<?php 
ALTER TABLE `bb_propersoptions` ADD `description` VARCHAR(255) NOT NULL AFTER `proHoverTxt`;
ALTER TABLE `bb_propersoptions` ADD `min` INT(11) NOT NULL AFTER `description`;
ALTER TABLE `bb_propersoptions` ADD `max` INT(11) NOT NULL AFTER `min`;
/**/
CREATE TABLE `bb_prooptionadd` (
  `id` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `propersoptions_id` int(11) NOT NULL,
  `proOptionVal` int(11) NOT NULL,
  `option_value` varchar(155) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `bb_prooptionadd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;
COMMIT;

?>