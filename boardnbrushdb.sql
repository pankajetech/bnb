-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2019 at 12:25 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `boardnbrushdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `bb_booking`
--

CREATE TABLE `bb_booking` (
  `id` int(11) NOT NULL,
  `boUserId` int(11) NOT NULL,
  `cOrderNo` varchar(500) NOT NULL,
  `eventId` int(11) NOT NULL,
  `boEventName` varchar(225) NOT NULL,
  `boEventDate` date NOT NULL,
  `eventStTime` varchar(255) NOT NULL,
  `eventEndTime` varchar(255) NOT NULL,
  `eventLocationId` int(11) NOT NULL,
  `boProjects` varchar(225) NOT NULL,
  `boFullName` varchar(225) NOT NULL,
  `boPhone` varchar(50) NOT NULL,
  `boEmail` varchar(225) NOT NULL,
  `boSitBy` varchar(255) NOT NULL,
  `boAddress` text NOT NULL,
  `boAccommodations` varchar(225) NOT NULL,
  `boAccommodationsNotes` varchar(225) NOT NULL,
  `transactionId` int(11) NOT NULL,
  `promocode` varchar(255) NOT NULL,
  `promoName` varchar(255) NOT NULL,
  `proApply` tinyint(1) NOT NULL,
  `proTax` tinyint(1) NOT NULL,
  `discType` tinyint(1) NOT NULL,
  `discAmt` decimal(10,2) NOT NULL,
  `totalTickets` int(11) NOT NULL,
  `isTax` tinyint(1) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `totalAmt` decimal(10,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0->Completed,2->Cancel,3->Need Attention,4->Change Project Pending',
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `bookingCreationDate` datetime NOT NULL,
  `bookingUpdationDate` datetime NOT NULL,
  `ipaddr` text NOT NULL,
  `isReminderEmail` int(11) NOT NULL DEFAULT '0',
  `isMobile` tinyint(1) NOT NULL DEFAULT '0',
  `deviceName` varchar(500) NOT NULL,
  `ClBkId` int(11) NOT NULL,
  `CleintEVid` int(11) NOT NULL,
  `ClientDbTbl` varchar(100) NOT NULL,
  `ClCuId` int(11) NOT NULL,
  `isupdated` tinyint(1) NOT NULL,
  `isupdatedTP` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_booking`
--

INSERT INTO `bb_booking` (`id`, `boUserId`, `cOrderNo`, `eventId`, `boEventName`, `boEventDate`, `eventStTime`, `eventEndTime`, `eventLocationId`, `boProjects`, `boFullName`, `boPhone`, `boEmail`, `boSitBy`, `boAddress`, `boAccommodations`, `boAccommodationsNotes`, `transactionId`, `promocode`, `promoName`, `proApply`, `proTax`, `discType`, `discAmt`, `totalTickets`, `isTax`, `tax`, `totalAmt`, `status`, `isDeleted`, `bookingCreationDate`, `bookingUpdationDate`, `ipaddr`, `isReminderEmail`, `isMobile`, `deviceName`, `ClBkId`, `CleintEVid`, `ClientDbTbl`, `ClCuId`, `isupdated`, `isupdatedTP`) VALUES
(1, 0, '001-N719Y6', 4, 'template_one', '2019-04-24', '12:00 AM', '12:30 AM', 360, '1', 'test test', '(999) 999-9999', 'testpankaj@yopmail.com', '', '', '', '', 0, 'cp121', '', 0, 0, 1, '200.00', 1, 1, '-2.70', '0.00', 0, 0, '2019-03-26 00:26:54', '2019-03-26 00:27:24', '::1', 0, 0, 'desktop', 0, 0, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_booktcktinfo`
--

CREATE TABLE `bb_booktcktinfo` (
  `id` int(11) NOT NULL,
  `bookingId` int(11) NOT NULL,
  `boUserId` int(11) NOT NULL,
  `boTicketName` varchar(225) NOT NULL,
  `boTicketPrice` varchar(225) NOT NULL,
  `boProjectId` int(11) NOT NULL,
  `boTicketProjectName` varchar(225) NOT NULL,
  `boTicketSku` varchar(225) NOT NULL,
  `boTicketfullname` varchar(500) NOT NULL,
  `boTicketEmailAddr` varchar(500) NOT NULL,
  `boTicketSitBy` varchar(225) NOT NULL,
  `boTicketProjectSize` varchar(500) NOT NULL,
  `boTicketProjectImage` text NOT NULL,
  `ClTctBoid` int(11) NOT NULL,
  `CleintTctId` int(11) NOT NULL,
  `ClBkId` int(11) NOT NULL,
  `ClientDbTbl` varchar(100) NOT NULL,
  `isupdated` tinyint(1) NOT NULL,
  `isupdatedTP` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_booktcktinfo`
--

INSERT INTO `bb_booktcktinfo` (`id`, `bookingId`, `boUserId`, `boTicketName`, `boTicketPrice`, `boProjectId`, `boTicketProjectName`, `boTicketSku`, `boTicketfullname`, `boTicketEmailAddr`, `boTicketSitBy`, `boTicketProjectSize`, `boTicketProjectImage`, `ClTctBoid`, `CleintTctId`, `ClBkId`, `ClientDbTbl`, `isupdated`, `isupdatedTP`) VALUES
(1, 1, 0, 'tq1', '65', 1, 'testingpankaj', '', 'test test', 'testpankaj@yopmail.com', 'testing', '', 'pid!a315d4bd71b0b7b50b7510ff4f367d59.jpg', 0, 0, 0, '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_booktcktpersinfo`
--

CREATE TABLE `bb_booktcktpersinfo` (
  `id` int(11) NOT NULL,
  `bookingId` int(11) NOT NULL,
  `bookingTicketId` int(11) NOT NULL,
  `boPersOptName` varchar(225) NOT NULL,
  `boPersOptVal` varchar(225) NOT NULL,
  `boPersOptType` varchar(125) NOT NULL COMMENT '1=>Text,2=>Date,3=>multiLine text',
  `boPersOptHoverTxt` varchar(225) NOT NULL,
  `boPersOptTypeFormat` varchar(125) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_booktcktpersinfo`
--

INSERT INTO `bb_booktcktpersinfo` (`id`, `bookingId`, `bookingTicketId`, `boPersOptName`, `boPersOptVal`, `boPersOptType`, `boPersOptHoverTxt`, `boPersOptTypeFormat`) VALUES
(1, 1, 1, 'Testingbutton', 'DSDS', '1', 'f', 'All Caps'),
(2, 1, 1, 'Label Date', '12-12-12', '2', 'dd', 'MM-DD-YY'),
(3, 1, 1, 'Sdfsf', 'Lllllllllll', '3', '', 'Title Case');

-- --------------------------------------------------------

--
-- Table structure for table `bb_calcolor`
--

CREATE TABLE `bb_calcolor` (
  `id` int(11) NOT NULL,
  `caName` varchar(255) NOT NULL,
  `caCode` varchar(255) NOT NULL,
  `caTitle` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_calcolor`
--

INSERT INTO `bb_calcolor` (`id`, `caName`, `caCode`, `caTitle`, `status`) VALUES
(1, 'Dim gray', '#717171', 'Open Workshop', 0),
(2, 'Silver', '#c4c2c2', 'Private Workshop', 0),
(3, 'Dark gray', '#8c8c8c', 'Make & Take', 0),
(4, 'Moderate orange', '#c19b43', 'Fundraiser', 0),
(5, 'Black', '#000000', 'Closed', 0),
(6, 'Very dark gray', '#474747', 'Grand Opening', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_crondata`
--

CREATE TABLE `bb_crondata` (
  `id` int(11) NOT NULL,
  `eventId` int(11) NOT NULL,
  `totalVacantSeats` int(11) NOT NULL,
  `bookedTicket` int(11) NOT NULL,
  `cookieId` text NOT NULL,
  `isBooked` int(11) NOT NULL DEFAULT '0' COMMENT '0->Not Booked,1->Booked',
  `createdDate` datetime NOT NULL,
  `stdTimeZone` varchar(255) NOT NULL,
  `createdDateTimeStr` bigint(20) DEFAULT NULL,
  `actualDT1stIns` datetime NOT NULL,
  `isUpdated` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_crondata`
--

INSERT INTO `bb_crondata` (`id`, `eventId`, `totalVacantSeats`, `bookedTicket`, `cookieId`, `isBooked`, `createdDate`, `stdTimeZone`, `createdDateTimeStr`, `actualDT1stIns`, `isUpdated`) VALUES
(18551, 1, 12, 1, '89ubl8n50q4g12290dgsspit8k', 0, '2019-03-23 05:13:25', 'America/Chicago', 1553336005, '2019-03-23 04:58:25', 0),
(18554, 1, 12, 2, 'omflph401vf57mn50rgngomohk', 0, '2019-03-26 00:05:42', 'America/Chicago', 1553576742, '2019-03-25 23:45:06', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_event`
--

CREATE TABLE `bb_event` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `studioId` text NOT NULL,
  `evTempId` int(11) NOT NULL,
  `slug` text NOT NULL,
  `evTempName` varchar(500) NOT NULL,
  `evType` int(11) NOT NULL,
  `evCalenderColor` varchar(50) NOT NULL,
  `evCalenderColorLabel` text NOT NULL,
  `evImage` text NOT NULL,
  `evDate` date NOT NULL,
  `evStTime` varchar(50) NOT NULL,
  `evEndTime` varchar(50) NOT NULL,
  `evLocationId` int(11) NOT NULL,
  `evTitle` varchar(225) NOT NULL,
  `evFullDescription` text NOT NULL,
  `evDesAddition` text NOT NULL,
  `evAccessCode` varchar(225) NOT NULL,
  `evScheduleType` tinyint(1) NOT NULL,
  `evSPubDate` date NOT NULL,
  `evSPubTime` varchar(255) NOT NULL,
  `evsDays` varchar(50) NOT NULL,
  `evScheduleHrs` varchar(50) NOT NULL,
  `evRegEnd` varchar(255) NOT NULL,
  `evNoOfTickets` varchar(50) NOT NULL,
  `evBookTicket` varchar(50) NOT NULL DEFAULT '0',
  `evIsClose` tinyint(1) NOT NULL DEFAULT '0',
  `evStatus` tinyint(1) NOT NULL DEFAULT '0',
  `evTypeStatus` enum('Enabled','Disabled') NOT NULL DEFAULT 'Enabled',
  `isCancel` tinyint(1) NOT NULL DEFAULT '0',
  `isPublish` tinyint(1) NOT NULL DEFAULT '0',
  `createDate` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `evFullimageUrl` text NOT NULL,
  `CleintEVid` int(11) NOT NULL,
  `ClientDbTbl` varchar(100) NOT NULL,
  `isupdated` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_event`
--

INSERT INTO `bb_event` (`id`, `userid`, `studioId`, `evTempId`, `slug`, `evTempName`, `evType`, `evCalenderColor`, `evCalenderColorLabel`, `evImage`, `evDate`, `evStTime`, `evEndTime`, `evLocationId`, `evTitle`, `evFullDescription`, `evDesAddition`, `evAccessCode`, `evScheduleType`, `evSPubDate`, `evSPubTime`, `evsDays`, `evScheduleHrs`, `evRegEnd`, `evNoOfTickets`, `evBookTicket`, `evIsClose`, `evStatus`, `evTypeStatus`, `isCancel`, `isPublish`, `createDate`, `isDeleted`, `evFullimageUrl`, `CleintEVid`, `ClientDbTbl`, `isupdated`) VALUES
(1, 1, '360', 1, 'testingeventtitle', 'testing event', 2, '#474747', '12', '5b8cf94a415681eea83a73753a341da7.jpg', '2019-03-28', '12:00 AM', '12:20 AM', 360, 'testing event title', 'testing full event title', 'This is testing descriptions', '12', 1, '2019-03-25', '11:51 pm', '', '23', '', '12', '0', 0, 0, 'Enabled', 0, 0, '2019-03-23 01:45:09', 0, '', 0, '', 0),
(2, 1, '360', 1, 'testingeventtitle-360-15533235902', 'testing event', 2, '#c19b43', '23', '5b8cf94a415681eea83a73753a341da7.jpg', '2019-03-05', '12:10 AM', '1:50 AM', 360, 'testingeventtitle', 'testing full descriptions', 'fdff', '12', 1, '2019-03-01', '12:30 AM', '', '23', '', '12', '0', 0, 0, 'Enabled', 0, 0, '2019-03-23 01:46:30', 0, '', 0, '', 0),
(3, 1, '360', 1, 'testingtitle', 'testing event', 2, '#c19b43', '23', '5b8cf94a415681eea83a73753a341da7.jpg', '2019-03-31', '12:20 AM', '12:50 AM', 360, 'testing title', 'testing full descriptions', '', '56', 1, '2019-03-28', '2:00 AM', '', '23', '', '12', '0', 0, 0, 'Enabled', 0, 0, '2019-03-23 01:48:00', 0, '', 0, '', 0),
(4, 1, '360', 2, 'templateone', 'testingmarch23_1', 2, '#717171', 'OpenWorkshop', '360!cb6b2b9bc9e7fa394740f2982c534de3.jpg', '2019-04-24', '12:00 AM', '12:30 AM', 360, 'template_one', 'template_one template_one template_one template_one template_one template_one', '', '22', 1, '2019-03-25', '11:53 pm', '', '12', '', '10', '1', 0, 0, 'Enabled', 0, 0, '2019-03-23 05:40:44', 0, '', 0, '', 0),
(5, 1, '360', 2, 'templateone-360-15537498232', 'testingmarch23_1', 2, '#717171', 'OpenWorkshop', '5dacb10425724ee6667b8d0791af329b.jpg', '2019-03-28', '12:00 AM', '3:20 AM', 360, 'template_one', 'template_onetemplate_onetemplate_onetemplate_onetemplate_onetemplate_one', 'dddd', '12', 1, '2019-03-01', '5:40 AM', '', '12', '', '10', '0', 0, 0, 'Enabled', 0, 0, '2019-03-28 00:10:23', 0, '', 0, '', 0),
(6, 1, '360', 1, 'ddddddde', 'testing event', 2, '#c19b43', '23', '360!80ec1b5940a768552a0565cd13a57d82.jpg', '2019-04-02', '12:30 AM', '5:30 AM', 360, 'ddddddde', 'testing full descridptions', 'd', 'd', 1, '2019-04-01', '12:20 AM', '', '23', '', '12', '0', 0, 0, 'Enabled', 0, 0, '2019-04-16 05:40:54', 0, '', 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_eventtemplates`
--

CREATE TABLE `bb_eventtemplates` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `evTempName` varchar(500) NOT NULL,
  `evTempImage` text NOT NULL,
  `evType` varchar(255) NOT NULL,
  `evTempTitle` varchar(255) NOT NULL,
  `evTempDescription` text NOT NULL,
  `evCalColor` varchar(255) NOT NULL,
  `evCalLabel` text NOT NULL,
  `evScheduleHrs` varchar(50) NOT NULL,
  `evNoOfTickets` varchar(50) NOT NULL,
  `evisStatus` tinyint(1) NOT NULL,
  `evisArchive` tinyint(1) NOT NULL,
  `evTypeStatus` enum('Enabled','Disabled') NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `createDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_eventtemplates`
--

INSERT INTO `bb_eventtemplates` (`id`, `userid`, `evTempName`, `evTempImage`, `evType`, `evTempTitle`, `evTempDescription`, `evCalColor`, `evCalLabel`, `evScheduleHrs`, `evNoOfTickets`, `evisStatus`, `evisArchive`, `evTypeStatus`, `isDeleted`, `createDate`) VALUES
(1, 1, 'testing event', '5b8cf94a415681eea83a73753a341da7.jpg', '2', 'testing title', 'testing full descriptions', '#c19b43', '23', '23', '12', 0, 0, 'Enabled', 0, '2019-03-23 00:50:36'),
(2, 1, 'testingmarch23_1', '5dacb10425724ee6667b8d0791af329b.jpg', '2', 'template_one', 'template_onetemplate_onetemplate_onetemplate_onetemplate_onetemplate_one', '#717171', 'OpenWorkshop', '12', '10', 0, 0, 'Enabled', 0, '2019-03-23 05:33:31'),
(3, 1, 'tev1', 'dc69c8bdc82a50222174089b090d0630.jpg', '2', 'dd', 'ddd', '#717171', 'dd', '43', '43', 0, 0, 'Enabled', 0, '2019-03-27 00:48:51');

-- --------------------------------------------------------

--
-- Table structure for table `bb_eventtype`
--

CREATE TABLE `bb_eventtype` (
  `id` int(11) NOT NULL,
  `etName` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_eventtype`
--

INSERT INTO `bb_eventtype` (`id`, `etName`, `status`) VALUES
(1, 'Public Workshop', 0),
(2, 'Private Workshop', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_event_bkp`
--

CREATE TABLE `bb_event_bkp` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `studioId` text NOT NULL,
  `evTempId` int(11) NOT NULL,
  `slug` text NOT NULL,
  `evTempName` varchar(500) NOT NULL,
  `evType` int(11) NOT NULL,
  `evCalenderColor` varchar(50) NOT NULL,
  `evCalenderColorLabel` text NOT NULL,
  `evImage` text NOT NULL,
  `evDate` date NOT NULL,
  `evStTime` varchar(50) NOT NULL,
  `evEndTime` varchar(50) NOT NULL,
  `evLocationId` int(11) NOT NULL,
  `evTitle` varchar(225) NOT NULL,
  `evFullDescription` text NOT NULL,
  `evDesAddition` text NOT NULL,
  `evAccessCode` varchar(225) NOT NULL,
  `evScheduleType` tinyint(1) NOT NULL,
  `evSPubDate` date NOT NULL,
  `evSPubTime` varchar(255) NOT NULL,
  `evsDays` varchar(50) NOT NULL,
  `evScheduleHrs` varchar(50) NOT NULL,
  `evRegEnd` varchar(255) NOT NULL,
  `evNoOfTickets` varchar(50) NOT NULL,
  `evBookTicket` varchar(50) NOT NULL DEFAULT '0',
  `evIsClose` tinyint(1) NOT NULL DEFAULT '0',
  `evStatus` tinyint(1) NOT NULL DEFAULT '0',
  `evTypeStatus` enum('Enabled','Disabled') NOT NULL DEFAULT 'Enabled',
  `isCancel` tinyint(1) NOT NULL DEFAULT '0',
  `isPublish` tinyint(1) NOT NULL DEFAULT '0',
  `createDate` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `evFullimageUrl` text NOT NULL,
  `CleintEVid` int(11) NOT NULL,
  `ClientDbTbl` varchar(100) NOT NULL,
  `isupdated` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bb_evoptions`
--

CREATE TABLE `bb_evoptions` (
  `id` int(11) NOT NULL,
  `opName` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_evoptions`
--

INSERT INTO `bb_evoptions` (`id`, `opName`, `status`) VALUES
(1, 'Project Class', 0),
(2, 'Ticket Limit', 0),
(3, 'Purchase Limit', 1),
(4, 'Minimum Purchase', 1),
(5, 'Manual Ticket', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_evstdrestrictions`
--

CREATE TABLE `bb_evstdrestrictions` (
  `id` int(11) NOT NULL,
  `evID` int(11) NOT NULL,
  `rsOptionVal` int(11) NOT NULL,
  `rsType` varchar(255) NOT NULL,
  `rsTypeVal` varchar(255) NOT NULL,
  `rsQVal` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bb_evticketpackage`
--

CREATE TABLE `bb_evticketpackage` (
  `id` int(11) NOT NULL,
  `evtempID` int(11) NOT NULL,
  `eventID` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `studioId` text NOT NULL,
  `urole` int(11) NOT NULL,
  `evTicketName` varchar(255) NOT NULL,
  `evTicketPrice` decimal(10,0) NOT NULL,
  `evTicketButtonLabel` varchar(255) NOT NULL,
  `isManual` tinyint(1) NOT NULL DEFAULT '0',
  `evTicketLImit` varchar(255) NOT NULL,
  `evSeatValue` varchar(50) NOT NULL,
  `evTicketOptionId` int(11) NOT NULL,
  `evTicketCatIds` text NOT NULL,
  `evTicketTypeIds` text NOT NULL,
  `evTicketProjectIds` text NOT NULL,
  `CleintEVid` int(11) NOT NULL,
  `CleintTctId` int(11) NOT NULL,
  `ClientDbTbl` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_evticketpackage`
--

INSERT INTO `bb_evticketpackage` (`id`, `evtempID`, `eventID`, `userid`, `studioId`, `urole`, `evTicketName`, `evTicketPrice`, `evTicketButtonLabel`, `isManual`, `evTicketLImit`, `evSeatValue`, `evTicketOptionId`, `evTicketCatIds`, `evTicketTypeIds`, `evTicketProjectIds`, `CleintEVid`, `CleintTctId`, `ClientDbTbl`) VALUES
(1, 1, 0, 1, '', 1, 'testing', '1200', 'testing', 0, '', '1', 8, '8,7', '', '', 0, 0, ''),
(2, 1, 1, 1, '360', 3, 'testing', '65', 'testing', 0, '', '1', 2, '', '', '', 0, 0, ''),
(3, 1, 2, 1, '360', 3, 'testing', '1', 'testing', 0, '', '1', 8, '8,7', '', '', 0, 0, ''),
(4, 1, 3, 1, '360', 3, 'testing', '1', 'testing', 0, '', '1', 8, '8,7', '', '', 0, 0, ''),
(5, 2, 0, 1, '', 1, 'tq1', '65', 'TEQ1', 0, '', '1', 2, '1,0', '', '', 0, 0, ''),
(6, 2, 4, 1, '360', 3, 'tq1', '80', 'TEQ1', 0, '', '1', 10, '4,5', '', '', 0, 0, ''),
(7, 3, 0, 1, '', 1, 'd', '40', 'dd', 0, '', '1', 6, '15', '', '', 0, 0, ''),
(8, 3, 0, 1, '', 1, '444', '0', 'dd', 0, '', '1', 8, '', '', '', 0, 0, ''),
(9, 2, 5, 1, '360', 3, 'tq1', '65', 'TEQ1', 0, '', '1', 9, '17,20,21,22,16,23', '', '', 0, 0, ''),
(10, 1, 6, 1, '360', 3, 'testing', '1', 'testing', 0, '', '1', 8, '8,7', '', '', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `bb_evticketpackageoptions`
--

CREATE TABLE `bb_evticketpackageoptions` (
  `id` int(11) NOT NULL,
  `evTempId` int(11) NOT NULL,
  `evTicketPackageId` int(11) NOT NULL,
  `evOptionVal` int(11) NOT NULL,
  `evType` varchar(255) NOT NULL,
  `evTypeVal` varchar(255) NOT NULL,
  `evPurTypeId` int(11) NOT NULL,
  `evQVal` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `urole` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bb_proactivitylog`
--

CREATE TABLE `bb_proactivitylog` (
  `id` int(11) NOT NULL,
  `lgUserId` int(11) NOT NULL,
  `lgProId` int(11) NOT NULL,
  `lgDescription` text NOT NULL,
  `lgDate` date NOT NULL,
  `lgTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_proactivitylog`
--

INSERT INTO `bb_proactivitylog` (`id`, `lgUserId`, `lgProId`, `lgDescription`, `lgDate`, `lgTime`) VALUES
(1, 1, 1, 'Project published', '2019-03-23', '04:21:38'),
(2, 1, 1, 'Project modified and published', '2019-03-23', '04:22:44'),
(3, 1, 1, 'Project modified and published', '2019-03-23', '04:52:37'),
(4, 1, 2, 'Project drafted', '2019-03-23', '05:35:48'),
(5, 1, 1, 'Project modified and published', '2019-03-25', '23:14:40'),
(6, 1, 1, 'Project modified and published', '2019-03-25', '23:41:31'),
(7, 1, 3, 'Project scheduled', '2019-03-27', '00:50:45'),
(8, 1, 5, 'Project scheduled', '2019-03-27', '02:21:01'),
(9, 1, 6, 'Project scheduled', '2019-03-27', '02:24:07'),
(10, 1, 7, 'Project published', '2019-03-27', '02:26:13'),
(11, 1, 8, 'Project scheduled', '2019-03-28', '00:02:01'),
(12, 1, 9, 'Project published', '2019-03-28', '00:06:53'),
(13, 1, 14, 'Project published', '2019-03-28', '01:59:51'),
(14, 1, 15, 'Project scheduled', '2019-03-28', '02:07:48'),
(15, 1, 15, 'Project modified and scheduled', '2019-03-28', '05:22:37'),
(16, 1, 16, 'Project scheduled', '2019-03-28', '05:24:54'),
(17, 1, 16, 'Project modified and scheduled', '2019-03-28', '05:25:54'),
(18, 1, 16, 'Project modified and scheduled', '2019-03-28', '05:39:00'),
(19, 1, 16, 'Project modified and scheduled', '2019-03-28', '05:56:18'),
(20, 1, 16, 'Project modified and scheduled', '2019-03-28', '05:56:59'),
(21, 1, 16, 'Project modified and scheduled', '2019-03-28', '06:02:00'),
(22, 1, 17, 'Project scheduled', '2019-03-28', '23:22:28'),
(23, 1, 18, 'Project scheduled', '2019-03-28', '23:57:04'),
(24, 1, 19, 'Project published', '2019-03-29', '00:01:49'),
(25, 1, 19, 'Project modified and published', '2019-03-29', '01:05:15'),
(26, 1, 19, 'Project modified and published', '2019-03-29', '01:30:21'),
(27, 1, 19, 'Project modified and published', '2019-03-29', '02:20:43'),
(28, 1, 19, 'Project modified and published', '2019-03-29', '02:21:22'),
(29, 1, 20, 'Project scheduled', '2019-03-29', '03:54:37'),
(30, 1, 20, 'Project modified and scheduled', '2019-03-29', '04:53:37'),
(31, 1, 20, 'Project modified and scheduled', '2019-03-29', '04:55:27'),
(32, 1, 20, 'Project modified and scheduled', '2019-03-29', '04:55:53'),
(33, 1, 15, 'Project modified and published', '2019-03-29', '05:02:21'),
(34, 1, 20, 'Project modified and scheduled', '2019-03-29', '05:04:44'),
(35, 1, 20, 'Project modified and scheduled', '2019-03-29', '05:05:03'),
(36, 1, 20, 'Project modified and scheduled', '2019-03-29', '05:05:16'),
(37, 1, 20, 'Project modified and scheduled', '2019-03-29', '05:05:33'),
(38, 1, 20, 'Project modified and scheduled', '2019-03-29', '05:05:52'),
(39, 1, 20, 'Project modified and scheduled', '2019-03-29', '05:06:04'),
(40, 1, 20, 'Project modified and scheduled', '2019-03-29', '05:06:18'),
(41, 1, 20, 'Project modified and scheduled', '2019-03-29', '05:24:44'),
(42, 1, 20, 'Project modified and scheduled', '2019-03-29', '05:25:24'),
(43, 1, 20, 'Project modified and scheduled', '2019-03-29', '05:26:35'),
(44, 1, 20, 'Project modified and scheduled', '2019-03-29', '05:28:28'),
(45, 1, 21, 'Project published', '2019-03-29', '05:47:00'),
(46, 1, 21, 'Project modified and published', '2019-03-29', '06:01:50'),
(47, 1, 20, 'Project modified and published', '2019-03-31', '22:59:35'),
(48, 1, 22, 'Project scheduled', '2019-04-02', '00:48:03'),
(49, 1, 26, 'Project scheduled', '2019-04-02', '01:26:28'),
(50, 1, 27, 'Project drafted', '2019-04-02', '01:31:53'),
(51, 1, 28, 'Project scheduled', '2019-04-02', '01:56:08'),
(52, 1, 28, 'Project modified and scheduled', '2019-04-02', '06:00:05'),
(53, 1, 28, 'Project modified and scheduled', '2019-04-02', '06:01:05'),
(54, 1, 28, 'Project modified and scheduled', '2019-04-02', '06:02:13'),
(55, 1, 28, 'Project modified and scheduled', '2019-04-02', '06:02:49'),
(56, 1, 28, 'Project modified and scheduled', '2019-04-02', '06:03:51'),
(57, 1, 32, 'Project scheduled', '2019-04-02', '06:59:28'),
(58, 1, 33, 'Project scheduled', '2019-04-02', '07:06:23'),
(59, 1, 33, 'Project modified and scheduled', '2019-04-02', '07:08:30'),
(60, 1, 34, 'Project scheduled', '2019-04-02', '07:12:13'),
(61, 1, 34, 'Project modified and scheduled', '2019-04-03', '01:55:29'),
(62, 1, 34, 'Project modified and scheduled', '2019-04-03', '01:56:21'),
(63, 1, 34, 'Project modified and scheduled', '2019-04-03', '01:59:17'),
(64, 1, 34, 'Project modified and scheduled', '2019-04-03', '03:44:23'),
(65, 1, 34, 'Project modified and scheduled', '2019-04-03', '03:46:21'),
(66, 1, 34, 'Project modified and scheduled', '2019-04-03', '03:53:15'),
(67, 1, 34, 'Project modified and scheduled', '2019-04-03', '04:08:14'),
(68, 1, 34, 'Project modified and scheduled', '2019-04-03', '04:14:59'),
(69, 1, 34, 'Project modified and scheduled', '2019-04-03', '04:16:18'),
(70, 1, 34, 'Project modified and scheduled', '2019-04-03', '04:19:23'),
(71, 1, 34, 'Project modified and scheduled', '2019-04-03', '04:20:22'),
(72, 1, 34, 'Project modified and scheduled', '2019-04-03', '04:25:54'),
(73, 1, 34, 'Project modified and scheduled', '2019-04-03', '05:03:45'),
(74, 1, 34, 'Project modified and scheduled', '2019-04-03', '05:09:13'),
(75, 1, 34, 'Project modified and scheduled', '2019-04-03', '06:44:57'),
(76, 1, 34, 'Project modified and scheduled', '2019-04-03', '06:48:48'),
(77, 1, 34, 'Project modified and scheduled', '2019-04-03', '06:51:18'),
(78, 1, 34, 'Project modified and scheduled', '2019-04-03', '06:51:51'),
(79, 1, 34, 'Project modified and scheduled', '2019-04-03', '07:00:59'),
(80, 1, 34, 'Project modified and scheduled', '2019-04-03', '07:23:34'),
(81, 1, 34, 'Project modified and scheduled', '2019-04-03', '22:58:58'),
(82, 1, 34, 'Project modified and scheduled', '2019-04-03', '23:00:53'),
(83, 1, 34, 'Project modified and scheduled', '2019-04-03', '23:44:42'),
(84, 1, 34, 'Project modified and scheduled', '2019-04-03', '23:54:27'),
(85, 1, 34, 'Project modified and scheduled', '2019-04-03', '23:55:10'),
(86, 1, 34, 'Project disabled', '2019-04-04', '00:52:35'),
(87, 1, 35, 'Project scheduled', '2019-04-04', '01:25:59'),
(88, 1, 36, 'Project scheduled', '2019-04-04', '01:30:50'),
(89, 1, 37, 'Project scheduled', '2019-04-04', '01:38:01'),
(90, 1, 38, 'Project scheduled', '2019-04-04', '01:39:34'),
(91, 1, 39, 'Project scheduled', '2019-04-04', '01:40:07'),
(92, 1, 42, 'Project scheduled', '2019-04-04', '01:47:15'),
(93, 1, 43, 'Project scheduled', '2019-04-04', '01:53:18'),
(94, 1, 44, 'Project scheduled', '2019-04-04', '01:54:28'),
(95, 1, 45, 'Project scheduled', '2019-04-04', '02:00:58'),
(96, 1, 46, 'Project scheduled', '2019-04-04', '02:01:44'),
(97, 1, 47, 'Project scheduled', '2019-04-04', '02:04:07'),
(98, 1, 47, 'Project modified and scheduled', '2019-04-04', '03:39:17'),
(99, 1, 47, 'Project modified and scheduled', '2019-04-04', '03:48:19'),
(100, 1, 47, 'Project modified and scheduled', '2019-04-04', '03:53:55'),
(101, 1, 47, 'Project modified and scheduled', '2019-04-04', '03:57:10'),
(102, 1, 47, 'Project modified and scheduled', '2019-04-04', '03:57:39'),
(103, 1, 47, 'Project modified and scheduled', '2019-04-04', '03:58:07'),
(104, 1, 47, 'Project modified and scheduled', '2019-04-04', '03:58:39'),
(105, 1, 47, 'Project modified and scheduled', '2019-04-04', '03:59:05'),
(106, 1, 47, 'Project modified and scheduled', '2019-04-04', '03:59:17'),
(107, 1, 47, 'Project modified and scheduled', '2019-04-04', '03:59:33'),
(108, 1, 47, 'Project modified and scheduled', '2019-04-04', '04:00:06'),
(109, 1, 47, 'Project modified and scheduled', '2019-04-04', '04:00:22'),
(110, 1, 48, 'Project published', '2019-04-04', '05:30:57'),
(111, 1, 48, 'Project modified and published', '2019-04-04', '05:31:43'),
(112, 1, 48, 'Project modified and published', '2019-04-04', '05:35:08'),
(113, 1, 48, 'Project modified and published', '2019-04-04', '05:37:53'),
(114, 1, 49, 'Project published', '2019-04-04', '05:50:30'),
(115, 1, 49, 'Project modified and published', '2019-04-04', '05:51:36'),
(116, 1, 50, 'Project scheduled', '2019-04-04', '05:55:34'),
(117, 1, 49, 'Project modified and published', '2019-04-04', '23:07:18'),
(118, 1, 55, 'Project scheduled', '2019-04-11', '01:58:42'),
(119, 1, 56, 'Project scheduled', '2019-04-11', '02:01:40'),
(120, 1, 65, 'Project scheduled', '2019-04-11', '02:16:05'),
(121, 1, 66, 'Project scheduled', '2019-04-11', '02:18:51'),
(122, 1, 66, 'Project modified and scheduled', '2019-04-11', '06:26:40'),
(123, 1, 66, 'Project modified and scheduled', '2019-04-11', '06:28:43'),
(124, 1, 66, 'Project modified and scheduled', '2019-04-11', '23:03:11'),
(125, 1, 66, 'Project modified and scheduled', '2019-04-11', '23:06:50'),
(126, 1, 66, 'Project modified and scheduled', '2019-04-11', '23:07:05'),
(127, 1, 66, 'Project modified and scheduled', '2019-04-11', '23:07:24'),
(128, 1, 67, 'Project scheduled', '2019-04-12', '02:17:48'),
(129, 1, 67, 'Project modified and scheduled', '2019-04-12', '05:40:53'),
(130, 1, 67, 'Project modified and scheduled', '2019-04-12', '05:43:29'),
(131, 1, 67, 'Project modified and scheduled', '2019-04-12', '05:43:45'),
(132, 1, 67, 'Project modified and scheduled', '2019-04-12', '05:47:33'),
(133, 1, 67, 'Project modified and scheduled', '2019-04-12', '05:47:44'),
(134, 1, 67, 'Project modified and scheduled', '2019-04-12', '06:16:17'),
(135, 1, 67, 'Project modified and scheduled', '2019-04-12', '06:19:50'),
(136, 1, 68, 'Project scheduled', '2019-04-12', '23:03:39'),
(137, 1, 69, 'Project published', '2019-04-13', '01:24:03'),
(138, 1, 70, 'Project scheduled', '2019-04-16', '01:59:25'),
(139, 1, 71, 'Project scheduled', '2019-04-16', '02:01:59'),
(140, 1, 72, 'Project scheduled', '2019-04-16', '02:07:02'),
(141, 1, 73, 'Project scheduled', '2019-04-16', '02:08:04'),
(142, 1, 74, 'Project scheduled', '2019-04-16', '02:09:22'),
(143, 1, 75, 'Project scheduled', '2019-04-16', '02:09:52'),
(144, 1, 76, 'Project scheduled', '2019-04-16', '02:12:17'),
(145, 1, 82, 'Project scheduled', '2019-04-16', '03:47:58'),
(146, 1, 83, 'Project scheduled', '2019-04-16', '03:55:06'),
(147, 1, 84, 'Project scheduled', '2019-04-16', '03:56:35'),
(148, 1, 86, 'Project scheduled', '2019-04-16', '04:01:15'),
(149, 1, 87, 'Project scheduled', '2019-04-16', '04:04:15'),
(150, 1, 88, 'Project scheduled', '2019-04-16', '04:07:22'),
(151, 1, 91, 'Project scheduled', '2019-04-16', '04:13:46'),
(152, 1, 93, 'Project scheduled', '2019-04-16', '04:24:25'),
(153, 1, 94, 'Project scheduled', '2019-04-16', '04:27:44'),
(154, 1, 128, 'Project scheduled', '2019-04-16', '06:49:41'),
(155, 1, 129, 'Project scheduled', '2019-04-16', '06:53:08'),
(156, 1, 130, 'Project scheduled', '2019-04-16', '06:53:58'),
(157, 1, 131, 'Project scheduled', '2019-04-16', '06:55:56'),
(158, 1, 132, 'Project scheduled', '2019-04-17', '00:48:14'),
(159, 1, 132, 'Project modified and scheduled', '2019-04-17', '04:07:40'),
(160, 1, 132, 'Project modified and scheduled', '2019-04-17', '04:10:16'),
(161, 1, 132, 'Project modified and scheduled', '2019-04-17', '04:18:55'),
(162, 1, 132, 'Project modified and scheduled', '2019-04-17', '04:32:51'),
(163, 1, 132, 'Project modified and scheduled', '2019-04-17', '04:33:33'),
(164, 1, 132, 'Project modified and scheduled', '2019-04-17', '04:34:13'),
(165, 1, 132, 'Project modified and scheduled', '2019-04-17', '04:40:32'),
(166, 1, 132, 'Project modified and scheduled', '2019-04-17', '04:41:50'),
(167, 1, 132, 'Project modified and scheduled', '2019-04-17', '04:43:06'),
(168, 1, 132, 'Project modified and scheduled', '2019-04-17', '04:53:32'),
(169, 1, 132, 'Project modified and scheduled', '2019-04-17', '04:55:37'),
(170, 1, 132, 'Project modified and scheduled', '2019-04-17', '05:13:02'),
(171, 1, 132, 'Project modified and scheduled', '2019-04-17', '05:15:09'),
(172, 1, 133, 'Project scheduled', '2019-04-17', '05:17:33'),
(173, 1, 133, 'Project modified and scheduled', '2019-04-17', '05:18:15'),
(174, 1, 133, 'Project modified and scheduled', '2019-04-17', '05:18:52'),
(175, 1, 133, 'Project modified and scheduled', '2019-04-17', '05:20:38'),
(176, 1, 133, 'Project modified and scheduled', '2019-04-17', '05:22:29'),
(177, 1, 134, 'Project scheduled', '2019-04-17', '05:32:07'),
(178, 1, 135, 'Project scheduled', '2019-04-18', '04:51:50'),
(179, 1, 136, 'Project scheduled', '2019-04-18', '04:53:56'),
(180, 1, 137, 'Project published', '2019-04-18', '04:58:04'),
(181, 1, 137, 'Project modified and published', '2019-04-18', '05:39:57'),
(182, 1, 137, 'Project modified and published', '2019-04-18', '05:41:27'),
(183, 1, 138, 'Project scheduled', '2019-04-21', '23:47:06'),
(184, 1, 138, 'Project modified and scheduled', '2019-04-21', '23:54:15'),
(185, 1, 138, 'Project modified and scheduled', '2019-04-21', '23:54:25'),
(186, 1, 138, 'Project modified and scheduled', '2019-04-22', '00:00:33'),
(187, 1, 138, 'Project modified and scheduled', '2019-04-22', '00:06:04'),
(188, 1, 138, 'Project modified and scheduled', '2019-04-22', '00:06:27'),
(189, 1, 138, 'Project modified and scheduled', '2019-04-22', '00:10:29'),
(190, 1, 138, 'Project modified and scheduled', '2019-04-22', '00:10:53'),
(191, 1, 138, 'Project modified and scheduled', '2019-04-22', '00:12:13'),
(192, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:09:15'),
(193, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:09:45'),
(194, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:12:07'),
(195, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:12:36'),
(196, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:14:06'),
(197, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:14:52'),
(198, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:15:03'),
(199, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:16:58'),
(200, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:17:08'),
(201, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:18:24'),
(202, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:20:47'),
(203, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:21:21'),
(204, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:22:17'),
(205, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:22:29'),
(206, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:25:21'),
(207, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:25:42'),
(208, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:25:50'),
(209, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:26:50'),
(210, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:27:13'),
(211, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:27:21'),
(212, 1, 138, 'Project modified and scheduled', '2019-04-22', '01:27:29'),
(213, 1, 139, 'Project published', '2019-04-22', '01:51:00'),
(214, 1, 139, 'Project modified and published', '2019-04-22', '01:51:43'),
(215, 1, 139, 'Project modified and published', '2019-04-22', '02:28:46'),
(216, 1, 140, 'Project scheduled', '2019-04-22', '05:13:36'),
(217, 1, 140, 'Project modified and scheduled', '2019-04-22', '06:02:28'),
(218, 1, 140, 'Project modified and scheduled', '2019-04-22', '06:05:21'),
(219, 1, 140, 'Project modified and scheduled', '2019-04-22', '06:05:36'),
(220, 1, 140, 'Project modified and scheduled', '2019-04-22', '06:13:16'),
(221, 1, 140, 'Project modified and scheduled', '2019-04-22', '06:13:33'),
(222, 1, 140, 'Project modified and scheduled', '2019-04-22', '06:14:14'),
(223, 1, 140, 'Project modified and scheduled', '2019-04-22', '06:14:30'),
(224, 1, 140, 'Project modified and scheduled', '2019-04-22', '06:16:11'),
(225, 1, 140, 'Project modified and scheduled', '2019-04-22', '06:16:56'),
(226, 1, 140, 'Project modified and scheduled', '2019-04-22', '06:20:02'),
(227, 1, 140, 'Project modified and scheduled', '2019-04-22', '06:22:55'),
(228, 1, 140, 'Project modified and scheduled', '2019-04-22', '06:23:08'),
(229, 1, 140, 'Project modified and scheduled', '2019-04-22', '06:23:17'),
(230, 1, 140, 'Project modified and scheduled', '2019-04-22', '06:26:10'),
(231, 1, 140, 'Project modified and scheduled', '2019-04-22', '06:27:21'),
(232, 1, 141, 'Project scheduled', '2019-04-22', '06:28:59'),
(233, 1, 141, 'Project modified and scheduled', '2019-04-22', '06:29:24'),
(234, 1, 141, 'Project modified and scheduled', '2019-04-22', '06:43:00'),
(235, 1, 141, 'Project modified and scheduled', '2019-04-22', '06:43:34'),
(236, 1, 141, 'Project modified and scheduled', '2019-04-22', '06:44:51'),
(237, 1, 137, 'Project modified and published', '2019-04-22', '06:44:57'),
(238, 1, 137, 'Project modified and published', '2019-04-22', '06:45:07'),
(239, 1, 137, 'Project modified and published', '2019-04-22', '06:45:19'),
(240, 1, 137, 'Project modified and published', '2019-04-22', '06:45:40'),
(241, 1, 137, 'Project modified and published', '2019-04-22', '06:47:35'),
(242, 1, 137, 'Project modified and published', '2019-04-22', '06:47:58'),
(243, 1, 137, 'Project modified and published', '2019-04-22', '07:08:51'),
(244, 1, 137, 'Project modified and published', '2019-04-22', '07:09:04'),
(245, 1, 137, 'Project modified and published', '2019-04-22', '07:13:33');

-- --------------------------------------------------------

--
-- Table structure for table `bb_procategory`
--

CREATE TABLE `bb_procategory` (
  `id` int(11) NOT NULL,
  `proCatName` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_procategory`
--

INSERT INTO `bb_procategory` (`id`, `proCatName`, `status`) VALUES
(1, 'Family', 0),
(2, 'NEW!', 0),
(3, 'Bar', 0),
(4, 'Bathroom', 0),
(5, 'Children', 0),
(6, 'Christmas', 0),
(7, 'Halloween', 0),
(8, 'Hobby', 0),
(9, 'Holiday', 0),
(10, 'Kids', 0),
(11, 'Kitchen & Laundry', 0),
(12, 'Lake', 0),
(13, 'Most Popular', 0),
(14, 'Odds & Ends', 0),
(15, 'Pets', 0),
(16, 'Pool', 0),
(17, 'Porch', 0),
(18, 'Quotes', 0),
(19, 'Religious', 0),
(20, 'Seasonal', 0),
(21, 'Sports', 0),
(22, 'Team Building', 0),
(23, 'USA/World', 0),
(24, 'Wedding', 0),
(30, 'Wood Sign Category', 1),
(31, 'Test Category11', 1),
(32, 'fffff111', 1),
(33, 'Testing deep', 1),
(34, 'demo category', 1),
(35, 'demo category1', 1),
(36, 'fffff1112', 1),
(37, 'demo category', 1),
(38, 'demo category', 1),
(39, 'demo category1', 1),
(40, 'Office', 0),
(41, 'Test Category', 1),
(42, 'Make &#38; Take', 0),
(43, 'Time Crunch', 0),
(44, 'Specialty Workshop', 0),
(45, 'Licensed', 0),
(46, 'UGA', 0),
(47, 'AHS', 0),
(48, 'Teen', 0),
(49, 'testingcategorypankaj', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_proclassoptions`
--

CREATE TABLE `bb_proclassoptions` (
  `id` int(11) NOT NULL,
  `opName` varchar(255) NOT NULL,
  `opPrice` decimal(10,0) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `dType` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_proclassoptions`
--

INSERT INTO `bb_proclassoptions` (`id`, `opName`, `opPrice`, `status`, `dType`) VALUES
(1, 'Large', '85', 1, 2),
(2, 'Regular', '65', 0, 1),
(3, 'Medium', '35', 0, 2),
(4, 'Child', '25', 0, 4),
(5, 'Bonus', '15', 0, 8),
(6, 'Doormat', '40', 0, 5),
(7, 'Make & Take', '15', 0, 6),
(8, 'Custom', '0', 0, 7),
(9, 'Component Add-on', '65', 0, 3),
(10, 'Glass', '80', 0, 9);

-- --------------------------------------------------------

--
-- Table structure for table `bb_profineprintoptions`
--

CREATE TABLE `bb_profineprintoptions` (
  `id` int(11) NOT NULL,
  `proFinePrintName` varchar(225) NOT NULL,
  `status` tinyint(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_profineprintoptions`
--

INSERT INTO `bb_profineprintoptions` (`id`, `proFinePrintName`, `status`) VALUES
(1, 'Limited project availability, check with local studio', 0),
(3, 'Greenery options subject to availability', 0),
(4, 'Accessory options may vary, subject to availability', 0),
(6, 'custom new fine print', 1),
(7, 'Limited project availability, check with local studio. Additional fees may apply.', 0),
(9, 'Limited project and hardware availability, check with local studio', 0),
(10, 'Size subject to availability, check with local studio', 0),
(11, 'Limited hardware availability, check with local studio. Additional fees may apply.', 0),
(13, 'Licensed design, additional fees may apply', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_progallery`
--

CREATE TABLE `bb_progallery` (
  `id` int(11) NOT NULL,
  `proUserId` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `proImgName` varchar(255) NOT NULL,
  `tmp_name` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `proImgStatus` int(11) NOT NULL COMMENT '''featuredimage''=>1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_progallery`
--

INSERT INTO `bb_progallery` (`id`, `proUserId`, `proId`, `proImgName`, `tmp_name`, `created`, `modified`, `proImgStatus`) VALUES
(1, 1, 136, 'cid!a2d5f53047fc0fffcf8d97b6dd5d6835.jpg', '', '2019-04-18 09:53:17', '2019-04-18 15:23:17', 0),
(2, 1, 136, 'cid!7101cb2ada8ac00c81fd7f1270f1f72f.jpg', '', '2019-04-18 09:53:23', '2019-04-18 15:23:23', 0),
(3, 1, 136, 'cid!c31985f4f4268c513628aa0df8c557d2.jpg', '', '2019-04-18 09:53:29', '2019-04-18 15:23:29', 0),
(4, 1, 136, 'cid!41a48db70914e1c006e303d7aaa4aee7.jpg', '', '2019-04-18 09:53:34', '2019-04-18 15:23:34', 0),
(5, 1, 136, 'cid!5e7cbb6cf52987d39d0b779b21553285.jpg', '', '2019-04-18 09:53:40', '2019-04-18 15:23:40', 0),
(6, 1, 136, 'cid!296c90fb59cccecd808b1f2fb948c926.jpg', '', '2019-04-18 09:53:45', '2019-04-18 15:23:45', 0),
(7, 1, 136, 'cid!81e32f6cf3138ab6591536cb84b8fc97.jpg', '', '2019-04-18 09:53:51', '2019-04-18 15:23:51', 0),
(8, 1, 136, 'cid!5df756374b06802499f6dcd593a8f0e7.jpg', '', '2019-04-18 09:53:56', '2019-04-18 15:23:56', 0),
(9, 1, 137, 'cid!08959cf294a7186830d47c8753bd3c48.jpg', '', '2019-04-18 09:57:36', '2019-04-18 15:27:36', 0),
(10, 1, 137, 'cid!4452bba9a7febe9b62401a0eb443eb63.jpg', '', '2019-04-18 09:57:41', '2019-04-18 15:27:41', 0),
(11, 1, 137, 'cid!428496f59056181dc81cb4e586ca72f2.jpg', '', '2019-04-18 09:57:47', '2019-04-18 15:27:47', 0),
(21, 1, 137, 'cid!1bc0cf7f9d371a0bd1d131b7f871108f.jpg', '', '2019-04-18 10:41:21', '2019-04-18 16:11:21', 0),
(22, 1, 137, 'cid!576e5b63c3a30118af8c15a330b38b1c.jpg', '', '2019-04-18 10:41:27', '2019-04-18 16:11:27', 0),
(23, 1, 138, 'cid!5c6dc0b2fbf44124b0235ebd1bdd0156.jpg', '', '2019-04-22 04:46:30', '2019-04-22 10:16:30', 0),
(24, 1, 138, 'cid!746405f0abc6e5d222f0133e83b6bfe8.jpg', '', '2019-04-22 04:46:35', '2019-04-22 10:16:35', 0),
(25, 1, 138, 'cid!f0e1d0f77507908e4d6e657db0bf89d4.jpg', '', '2019-04-22 04:46:45', '2019-04-22 10:16:45', 0),
(28, 1, 138, 'cid!dfbadac6cc525ded2039fff349df4b08.jpg', '', '2019-04-22 04:47:06', '2019-04-22 10:17:06', 0),
(29, 1, 139, 'cid!2f39175b850fac773006f85fdb3e7294.jpg', '', '2019-04-22 06:50:54', '2019-04-22 12:20:54', 0),
(30, 1, 139, 'cid!2f5b973a782010e6935b5e3b1cdf06a9.jpg', '', '2019-04-22 06:51:00', '2019-04-22 12:21:00', 0),
(35, 1, 140, 'cid!5effa20304c8c24ba3d3ce2f3bfb51a4.jpg', '', '2019-04-22 11:27:16', '2019-04-22 16:57:16', 0),
(36, 1, 140, 'cid!e3fd32582e5b0d8fa90788164fbacca5.jpg', '', '2019-04-22 11:27:21', '2019-04-22 16:57:21', 0),
(38, 1, 141, 'cid!dc5df3a0c6ead64ab44da5e345ec23bd.jpg', '', '2019-04-22 11:44:36', '2019-04-22 17:14:36', 0),
(39, 1, 141, 'cid!8580895b56eece57d282144d0a7b5fd9.jpg', '', '2019-04-22 11:44:51', '2019-04-22 17:14:51', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_project`
--

CREATE TABLE `bb_project` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `proName` varchar(225) NOT NULL,
  `proTypeId` int(11) NOT NULL,
  `proSku` varchar(225) NOT NULL,
  `proClassId` int(11) NOT NULL,
  `proDetailedDesc` text NOT NULL,
  `proPrice` decimal(10,2) NOT NULL,
  `proPriCatId` varchar(225) NOT NULL,
  `proAltCat` text NOT NULL,
  `proSchType` varchar(225) NOT NULL DEFAULT '0',
  `proSchStDate` date NOT NULL,
  `proSchStTime` varchar(225) NOT NULL,
  `proSchStDtTime` datetime NOT NULL,
  `proSchDtOptional` int(11) NOT NULL DEFAULT '0' COMMENT '0->Not Optional,1->Optional',
  `proGalImg` text NOT NULL,
  `proFpPrintList` text NOT NULL,
  `proFpPrintCustom` text NOT NULL,
  `proSpecWi` varchar(225) NOT NULL,
  `proSpecHi` varchar(225) NOT NULL,
  `proSpecDe` varchar(225) NOT NULL,
  `proSpecWe` varchar(225) NOT NULL,
  `status` tinyint(11) NOT NULL DEFAULT '0' COMMENT '0->Active,1->DeActive,2->Draft,3->Archive',
  `isDeleted` tinyint(11) NOT NULL DEFAULT '0',
  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_project`
--

INSERT INTO `bb_project` (`id`, `userId`, `proName`, `proTypeId`, `proSku`, `proClassId`, `proDetailedDesc`, `proPrice`, `proPriCatId`, `proAltCat`, `proSchType`, `proSchStDate`, `proSchStTime`, `proSchStDtTime`, `proSchDtOptional`, `proGalImg`, `proFpPrintList`, `proFpPrintCustom`, `proSpecWi`, `proSpecHi`, `proSpecDe`, `proSpecWe`, `status`, `isDeleted`, `createdDate`, `updatedDate`) VALUES
(1, 1, 'testingpankaj', 11, '', 2, 'ffffffffffffffffffffff', '65.00', '1', '', 'By Schedule', '2019-03-04', '2:00 AM', '2019-03-04 02:00:00', 0, 'pid!a315d4bd71b0b7b50b7510ff4f367d59.jpg', '1', '', '4', '4', '4', '4', 0, 1, '2019-03-23 04:21:38', '2019-03-25 23:41:30'),
(2, 1, 'projectone', 2, '', 2, 'this is project one', '65.00', '1', '11', 'By Schedule', '2019-03-23', '05:35 am', '2019-03-23 05:35:00', 1, 'pid!33563198380ba83790d8efca4f823d99.jpg', '1', 'd', '12', '12', '12', '12', 2, 1, '2019-03-23 05:35:47', '2019-03-23 05:35:47'),
(3, 1, 'pone', 9, '', 10, '', '80.00', '5', '10', 'By Schedule', '2019-03-31', '12:00 AM', '2019-03-31 00:00:00', 0, 'pid!3c2cd8e57832a65f439071b680b53e3f.jpg', '1', '', '2', '3', '4', '5', 0, 1, '2019-03-27 00:50:45', '2019-03-27 00:50:45'),
(4, 1, 'test_proj_1', 12, '', 5, 'testing details', '15.00', '1', '10', 'By Schedule', '2019-03-31', '12:00 AM', '2019-03-31 00:00:00', 0, 'pid!0e3cf093c95d532196aaeb630415e1f8.jpg', '1', '', '12', '12', '12', '12', 0, 0, '2019-03-27 02:19:41', '2019-03-27 02:19:41'),
(5, 1, 'test_proj_1', 12, '', 5, 'testing details', '15.00', '1', '10', 'By Schedule', '2019-03-31', '12:00 AM', '2019-03-31 00:00:00', 0, 'pid!3035a3547f90abca50b3c70c32ccf6d4.jpg', '1', '', '12', '12', '12', '12', 0, 1, '2019-03-27 02:21:01', '2019-03-27 02:21:01'),
(6, 1, 'test_proj_1', 12, '', 5, 'testing details', '15.00', '1', '10', 'By Schedule', '2019-03-31', '12:00 AM', '2019-03-31 00:00:00', 0, 'pid!7a2c365f35ba6293aca13419341f95e2.jpg', '1', '', '12', '12', '12', '12', 0, 1, '2019-03-27 02:24:07', '2019-03-27 02:24:07'),
(7, 1, 'testone_project_1', 1, '', 2, 'this is testing', '65.00', '3', '10', 'By Schedule', '2019-03-27', '12:00 AM', '2019-03-27 00:00:00', 0, 'pid!aade9e82c055a427b405820172a62882.jpg', '1', '', '12', '12', '2', '5', 0, 1, '2019-03-27 02:26:13', '2019-03-27 02:26:13'),
(8, 1, 'testing project', 2, '', 10, 'testing project testing project testing projecttesting projecttesting projecttesting project', '80.00', '4', '10', 'By Schedule', '2019-03-29', '12:00 AM', '2019-03-29 00:00:00', 0, 'pid!03d81ebb942abbd5ee074e82be9e10e9.jpg', '1', 'ddd', '12', '2', '3', '4', 0, 1, '2019-03-28 00:02:01', '2019-03-28 00:02:01'),
(9, 1, 'testingproject_name', 11, '', 10, 'testing date testing datetesting datetesting datetesting date', '80.00', '5', '10', 'By Schedule', '2019-03-28', '12:00 AM', '2019-03-28 00:00:00', 0, 'pid!ec14903c54f2d6db34ab82b23d78c395.jpg', '1', 'dddd', '12', '2', '2', '2', 0, 1, '2019-03-28 00:06:53', '2019-03-28 00:06:53'),
(10, 1, 'testingone', 1, '', 3, 'this is testing one', '35.00', '4', '10,1,3', 'By Schedule', '2019-03-28', '12:00 AM', '2019-03-28 00:00:00', 0, 'pid!6b42b84ce4679a4d0722273242c9bc74.jpg', '1', 'testing entry', '12', '12', '12', '12', 0, 0, '2019-03-28 01:45:23', '2019-03-28 01:45:23'),
(11, 1, 'testingone', 1, '', 3, 'this is testing one', '35.00', '4', '10,1,3', 'By Schedule', '2019-03-28', '12:00 AM', '2019-03-28 00:00:00', 0, 'pid!c4b5313c279a8422f715fb8d95e3268e.jpg', '1', 'testing entry', '12', '12', '12', '12', 0, 0, '2019-03-28 01:46:14', '2019-03-28 01:46:14'),
(12, 1, 'testingproject2', 3, '', 10, 'ddd', '80.00', '5', '10', 'By Schedule', '2019-03-28', '2:00 AM', '2019-03-28 02:00:00', 0, 'pid!7021cc5c363e3057ed39f2b60ae24519.jpg', '1', 'd', '12', '12', '2', '3', 0, 0, '2019-03-28 01:53:18', '2019-03-28 01:53:18'),
(13, 1, 'testproject3', 12, '', 10, 'This is detailed description', '80.00', '5', '10,9', 'By Schedule', '2019-03-31', '1:00 AM', '2019-03-31 01:00:00', 0, 'pid!a9e4a5a554bff1d4157cd5a3d46c9dbc.jpg', '1', '', '12', '12', '1', '2', 0, 0, '2019-03-28 01:56:54', '2019-03-28 01:56:54'),
(14, 1, 'testproject3', 11, '', 5, 'dd', '15.00', '4', '10', 'By Schedule', '2019-03-28', '12:00 AM', '2019-03-28 00:00:00', 0, 'pid!f9ae5448f71bf888849dba4e6c7eac86.jpg', '1', '33', '12', '12', '12', '12', 0, 1, '2019-03-28 01:59:50', '2019-03-28 01:59:50'),
(15, 1, 'testing5', 2, '', 9, 'thiii', '65.00', '1', '10', 'By Schedule', '2019-03-29', '1:00 AM', '2019-03-29 01:00:00', 0, 'pid!cbed6b9ba5fbd8fb19e8558ce708c092.jpg', '1', 'd', '2', '3', '4', '5', 0, 0, '2019-03-28 02:07:48', '2019-03-29 05:02:20'),
(16, 1, 'project_1', 12, '', 4, 'ddd', '25.00', '5', '10', 'By Schedule', '2019-03-29', '1:00 AM', '2019-03-29 01:00:00', 0, 'pid!05131f513696eda1ab2a966f726fed90.jpg', '1', 'd', '2', '3', '3', '4', 0, 0, '2019-03-28 05:24:54', '2019-03-28 06:02:00'),
(17, 1, 'testingp1', 10, '', 5, 'testing project', '15.00', '4', '10', 'By Schedule', '2019-03-29', '12:00 AM', '2019-03-29 00:00:00', 0, 'pid!d8c72eabadca45aeb03988c2c16e7a2a.jpg', '1', 'd', '12', '3', '4', '5', 0, 0, '2019-03-28 23:22:28', '2019-03-28 23:22:28'),
(18, 1, 'testing', 10, '', 10, 'tet', '80.00', '3', '10', 'By Schedule', '2019-03-29', '12:00 AM', '2019-03-29 00:00:00', 0, 'pid!cdb5c2901870b56fc5afdf7ede443153.jpg', '1', 'd', '12', '12', '3', '4', 0, 0, '2019-03-28 23:57:04', '2019-03-28 23:57:04'),
(19, 1, 'testingmar19', 13, '', 5, 'dsdsdsds', '15.00', '2', '1,10', 'By Schedule', '2019-03-29', '12:00 AM', '2019-03-29 00:00:00', 0, 'pid!acddb23e64feab7bf24eca529cf6e7f0.jpg', '1', '', '12', '3', '4', '5', 0, 0, '2019-03-29 00:01:48', '2019-03-29 02:21:22'),
(20, 1, 'testdropdownvalues', 12, '', 3, 'this is testing', '35.00', '3', '10', 'By Schedule', '2019-03-29', '7:00 AM', '2019-03-29 07:00:00', 0, 'pid!eef549b7926acec754298637337c7a32.jpg', '1', '', '2', '4', '5', '6', 0, 0, '2019-03-29 03:54:36', '2019-03-31 22:59:34'),
(21, 1, 'testing', 1, '', 3, 'fff', '35.00', '1', '10', 'By Schedule', '2019-03-13', '1:00 AM', '2019-03-13 01:00:00', 0, 'pid!f44bb8f1b2bdcd5bd895fcfce7470ee2.jpg', '1', '', '23', '32', '32', '32', 0, 0, '2019-03-29 05:47:00', '2019-03-29 06:01:50'),
(22, 1, 'teting02Apr', 12, '', 6, 'This is testing description', '40.00', '4', '10,1', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!421887b94f1dfa01f24ebff079a344be.jpg', '1', '', '12', '33', '44', '55', 0, 0, '2019-04-02 00:48:02', '2019-04-02 00:48:02'),
(23, 1, 'testproject', 11, '', 10, 'fff', '80.00', '4', '10,1', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, 'pid!a70f2d868c85b5ae609436efb1deec15.jpg', '1', 'g', '2', '3', '4', '5', 0, 0, '2019-04-02 01:20:31', '2019-04-02 01:20:31'),
(24, 1, 'testproject', 11, '', 10, 'fff', '80.00', '4', '10,1', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, 'pid!5bc36f23b36d41d2a57bbdc17a5649f8.jpg', '1', 'g', '2', '3', '4', '5', 0, 0, '2019-04-02 01:22:48', '2019-04-02 01:22:48'),
(25, 1, 'testproject', 11, '', 10, 'fff', '80.00', '4', '10,1', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, 'pid!1699a4c96db081b7962ecf4f315bddf3.jpg', '1', 'g', '2', '3', '4', '5', 0, 0, '2019-04-02 01:25:52', '2019-04-02 01:25:52'),
(26, 1, 'testproject', 11, '', 10, 'fff', '80.00', '4', '10,1', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, 'pid!ddcfb7c72e914635fee056c191f4670a.jpg', '1', 'g', '2', '3', '4', '5', 0, 0, '2019-04-02 01:26:27', '2019-04-02 01:26:27'),
(27, 1, 'project_02', 12, '', 2, 'fff', '65.00', '1', '10,1', 'By Schedule', '2019-04-02', '01:31 am', '2019-04-02 01:31:00', 1, 'pid!a124c023a068543e2379b825d42ab53c.jpg', '1', 'd', '12', '22', '32', '42', 2, 0, '2019-04-02 01:31:53', '2019-04-02 01:31:53'),
(28, 1, 'testingproject_1', 11, '', 5, 'testing1', '15.00', '5', '1,10', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!58262f774f55d80275c331e01763075a.jpg', '1', '', '2', '3', '4', '5', 0, 0, '2019-04-02 01:56:07', '2019-04-02 06:03:51'),
(29, 1, 'testingproject_1', 12, '', 5, 'ddd', '15.00', '2', '10,1,7', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!2d1f71977ae7accec71ae6845e1971ea.jpg', '1', 'ffff', '1', '2', '3', '4', 0, 0, '2019-04-02 06:44:53', '2019-04-02 06:44:53'),
(30, 1, 'testingproject_1', 12, '', 5, 'ddd', '15.00', '2', '10,1,7', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!93ed13f55e944027bbbf5a7a841f2bb3.jpg', '1', 'ffff', '1', '2', '3', '4', 0, 0, '2019-04-02 06:45:52', '2019-04-02 06:45:52'),
(31, 1, 'testingproject_1', 12, '', 5, 'ddd', '15.00', '2', '10,1,7', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!dc96cd35dc64a1b1d5b019ba8f3c57db.jpg', '1', 'ffff', '1', '2', '3', '4', 0, 0, '2019-04-02 06:47:36', '2019-04-02 06:47:36'),
(32, 1, 'testingproject_1', 11, '', 6, 'testttttttttttttttt', '40.00', '5', '10,1,13', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!a7b7b846c94546166bc00a9e8d9d4ef8.jpg', '1', 'd', '2', '3', '4', '5', 0, 0, '2019-04-02 06:59:27', '2019-04-02 06:59:27'),
(33, 1, 'testingapr02_1', 11, '', 7, 'dsdsdsdsdsds', '15.00', '2', '1,10,49', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, 'pid!38d35f271aa1ae64919d072b36a606cd.jpg', '1', 'testing', '12', '14', '16', '18', 0, 0, '2019-04-02 07:06:23', '2019-04-02 07:08:29'),
(34, 1, 'testafterstrings', 11, '', 5, 'testafterstringtestafterstringtestafterstring', '15.00', '1', '1,5,10', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!f0f32619af367225ba8984b004414677.jpg', '1', '', '2', '3', '4', '5', 1, 0, '2019-04-02 07:12:13', '2019-04-04 00:52:35'),
(35, 1, 'testproject1', 12, '', 2, 'dddd', '65.00', '5', '1,10,15', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!c31eaa650abe2b52e9ae69fa944e4e1a.jpg', '1', 'fffffff', '2', '3', '4', '5', 0, 0, '2019-04-04 01:25:58', '2019-04-04 01:25:58'),
(36, 1, 'checkfor one', 13, '', 10, 'f', '80.00', '1', '10,1', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!4f5b6463f111973fe604d1f1219f2bd0.jpg', '1', 'fvfvv', '4', '5', '6', '7', 0, 0, '2019-04-04 01:30:50', '2019-04-04 01:30:50'),
(37, 1, 'ttt', 11, '', 5, 'tt', '15.00', '1', '10,1,13', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, 'pid!883ef20402d91c2fd73637c3c46ec238.jpg', '1', 'f', '1', '2', '3', '4', 0, 0, '2019-04-04 01:38:01', '2019-04-04 01:38:01'),
(38, 1, 'ttt', 11, '', 5, 'tt', '15.00', '1', '10,1,13', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, 'pid!7c3ef3594098d8b174ad137b2260fb26.jpg', '1', 'f', '1', '2', '3', '4', 0, 0, '2019-04-04 01:39:34', '2019-04-04 01:39:34'),
(39, 1, 'ttt', 11, '', 5, 'tt', '15.00', '1', '10,1,13', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, 'pid!2c3114396aee4134beb408987861066f.jpg', '1', 'f', '1', '2', '3', '4', 0, 0, '2019-04-04 01:40:06', '2019-04-04 01:40:06'),
(40, 1, 'ttt', 11, '', 5, 'tt', '15.00', '1', '10,1,13', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, 'pid!aa286f120846d358fb8d99551db0b153.jpg', '1', 'f', '1', '2', '3', '4', 0, 0, '2019-04-04 01:42:54', '2019-04-04 01:42:54'),
(41, 1, 'ttt', 11, '', 5, 'tt', '15.00', '1', '10,1,13', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, 'pid!3525cbb7724085ca6b62c6c032d22da0.jpg', '1', 'f', '1', '2', '3', '4', 0, 0, '2019-04-04 01:46:19', '2019-04-04 01:46:19'),
(42, 1, 'ttt', 11, '', 5, 'tt', '15.00', '1', '10,1,13', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, 'pid!ef6a5ebf5def5922aa3d3a3337d2eb8b.jpg', '1', 'f', '1', '2', '3', '4', 0, 0, '2019-04-04 01:47:15', '2019-04-04 01:47:15'),
(43, 1, 'finaladd_pro', 1, '', 2, 'ddddddddddd', '65.00', '5', '10,1,4', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!6c16d4e4b55be8ca6a38fe53dd6f970b.jpg', '1', 'd', '1', '2', '3', '4', 0, 0, '2019-04-04 01:53:18', '2019-04-04 01:53:18'),
(44, 1, 'finaladd_pro', 1, '', 2, 'ddddddddddd', '65.00', '5', '10,1,4', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!38e679f417d8b04e040c91b63c8f64b7.jpg', '1', 'd', '1', '2', '3', '4', 0, 0, '2019-04-04 01:54:28', '2019-04-04 01:54:28'),
(45, 1, 'testingproject1', 12, '', 3, 'dddddddddddddddddddddddddddddddd', '35.00', '4', '10,1,4', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!27a1a28c0d60eb6a7ee05d7ab6513c77.jpg', '1', 'ggggg', '4', '4', '5', '6', 0, 0, '2019-04-04 02:00:57', '2019-04-04 02:00:57'),
(46, 1, 'testingproject1', 12, '', 3, 'dddddddddddddddddddddddddddddddd', '35.00', '4', '10,1,4', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!d427ad56ad44ded011fde7bc2fd38c95.jpg', '1', 'ggggg', '4', '4', '5', '6', 0, 0, '2019-04-04 02:01:44', '2019-04-04 02:01:44'),
(47, 1, 'final_testing', 7, '', 2, 'gg', '65.00', '1', '4,10,11', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, 'pid!e868adde03b04c2738c31dc4bb6feff5.jpg', '1', 'g', '12', '3', '4', '5', 0, 0, '2019-04-04 02:04:07', '2019-04-04 04:00:22'),
(48, 1, 'testingdevloper_1', 3, '', 3, 'teeeeeeeeeeeeeee', '35.00', '1', '1,10,22', 'By Schedule', '2019-04-03', '3:00 AM', '2019-04-03 03:00:00', 0, 'pid!e4c4c5eb18e44b227998bfa5e71c9275.jpg', '1', 'rrrr', '12', '14', '16', '18', 0, 0, '2019-04-04 05:30:57', '2019-04-04 05:37:53'),
(49, 1, 'hghghg', 2, '', 9, 'jhg', '65.00', '1', '10,22,49', 'By Schedule', '2019-04-03', '4:00 AM', '2019-04-03 04:00:00', 0, 'pid!b2c8083d2596a0fbf17dbeaa593334d9.jpg', '1', '', '76', '76', '7', '8', 0, 0, '2019-04-04 05:50:29', '2019-04-04 23:07:18'),
(50, 1, 'iceice', 13, '', 10, 'asdadsd', '80.00', '1', '1,5,10,40', 'By Schedule', '2019-04-10', '3:00 AM', '2019-04-10 03:00:00', 0, 'pid!425359ac7322c21cf6f821be8f9084c5.jpg', '3', 'bcvbcvb', '45', '23', '45', '87', 0, 0, '2019-04-04 05:55:33', '2019-04-04 05:55:33'),
(51, 1, 'testing project', 12, '', 4, 'dddddddd', '25.00', '5', '10,1,23', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, 'pid!8da78a43de020afeb5e442ab045327c6.jpg', '1', 'd', '2', '3', '4', '5', 0, 0, '2019-04-11 01:22:26', '2019-04-11 01:22:26'),
(52, 1, 'testing project', 12, '', 4, 'dddddddd', '25.00', '5', '10,1,23', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, 'pid!e3f53516e1079ecbff708f1cdbc3fc25.jpg', '1', 'd', '2', '3', '4', '5', 0, 0, '2019-04-11 01:24:02', '2019-04-11 01:24:02'),
(53, 1, 'testing project', 12, '', 4, 'dddddddd', '25.00', '5', '10,1,23', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, 'pid!fb30ff3e725f2fffec895a37a721c44a.jpg', '1', 'd', '2', '3', '4', '5', 0, 0, '2019-04-11 01:35:54', '2019-04-11 01:35:54'),
(54, 1, 'testing project', 12, '', 4, 'dddddddd', '25.00', '5', '10,1,23', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, 'pid!102ee803739d059e69d04b2d44a2f24c.jpg', '1', 'd', '2', '3', '4', '5', 0, 0, '2019-04-11 01:38:11', '2019-04-11 01:38:11'),
(55, 1, 'testing project restrictions', 1, '', 5, 'ffffffffffff', '15.00', '4', '10,11,1', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!5ba476c6f475e994f25a327512251c04.jpg', '1', 'f', '2', '3', '4', '5', 0, 0, '2019-04-11 01:58:41', '2019-04-11 01:58:41'),
(56, 1, 'testingproject_apr11', 11, '', 5, 'testing description', '15.00', '1', '10,11,1', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!86b6e6a25ed03d344cf28fdc93861311.jpg', '1', 'ffffffffff', '2', '3', '4', '5', 0, 0, '2019-04-11 02:01:39', '2019-04-11 02:01:39'),
(57, 1, 'testingproject_apr11', 11, '', 5, 'testing description', '15.00', '1', '10,11,1', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!0c6d0576f826eecebe36d47182b80bca.jpg', '1', 'ffffffffff', '2', '3', '4', '5', 0, 0, '2019-04-11 02:03:51', '2019-04-11 02:03:51'),
(58, 1, 'testingproject_apr11', 11, '', 5, 'testing description', '15.00', '1', '10,11,1', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!0ffdbb14adc074d8cd0acd655b12cfa8.jpg', '1', 'ffffffffff', '2', '3', '4', '5', 0, 0, '2019-04-11 02:04:43', '2019-04-11 02:04:43'),
(59, 1, 'testingproject_apr11', 11, '', 5, 'testing description', '15.00', '1', '10,11,1', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!551cb676099e8b8bb3765136a2bf47ea.jpg', '1', 'ffffffffff', '2', '3', '4', '5', 0, 0, '2019-04-11 02:07:38', '2019-04-11 02:07:38'),
(60, 1, 'testingproject_apr11', 11, '', 5, 'testing description', '15.00', '1', '10,11,1', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!84322a2520fcf6e1d4b3f799b763e232.jpg', '1', 'ffffffffff', '2', '3', '4', '5', 0, 0, '2019-04-11 02:09:55', '2019-04-11 02:09:55'),
(61, 1, 'testingproject_apr11', 11, '', 5, 'testing description', '15.00', '1', '10,11,1', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!82800e861277c8f81590a32788cee32d.jpg', '1', 'ffffffffff', '2', '3', '4', '5', 0, 0, '2019-04-11 02:10:21', '2019-04-11 02:10:21'),
(62, 1, 'testingproject_apr11', 11, '', 5, 'testing description', '15.00', '1', '10,11,1', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!acd622637f18687e12d82fa640e8b860.jpg', '1', 'ffffffffff', '2', '3', '4', '5', 0, 0, '2019-04-11 02:10:41', '2019-04-11 02:10:41'),
(63, 1, 'testingproject_apr11', 11, '', 5, 'testing description', '15.00', '1', '10,11,1', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!264da748df582b6907f3f2edd14caa46.jpg', '1', 'ffffffffff', '2', '3', '4', '5', 0, 0, '2019-04-11 02:11:30', '2019-04-11 02:11:30'),
(64, 1, 'testingproject_apr11', 11, '', 5, 'testing description', '15.00', '1', '10,11,1', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, 'pid!433c5aabb33ab3c84499d7dc3da6d48b.jpg', '1', 'ffffffffff', '2', '3', '4', '5', 0, 0, '2019-04-11 02:12:30', '2019-04-11 02:12:30'),
(65, 1, 'Restrictionj option final', 13, '', 3, 'dddddddddddd', '35.00', '4', '10,1,6', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, 'pid!c9cc92f2fdd4a7d741859416a7fb254a.jpg', '1', '', '1', '2', '3', '4', 0, 0, '2019-04-11 02:16:05', '2019-04-11 02:16:05'),
(66, 1, 'testinreqfinal', 12, '', 2, 'h', '65.00', '5', '1,10,49', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, 'pid!61f9f576ea0cd95bf47285d17bca103b.jpg', '1', 'ggggggggggg', '3', '3', '4', '5', 0, 0, '2019-04-11 02:18:50', '2019-04-11 23:07:23'),
(67, 1, 'testprojectcategory', 3, '', 10, 'This is testing project categort', '80.00', '2', '10', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, 'pid!d1822e5263a20f9527068becad30e72b.jpg', '1', 'dddd', '4', '4', '4', '5', 0, 0, '2019-04-12 02:17:47', '2019-04-12 06:19:50'),
(68, 1, '13Aprsaturday', 3, '', 9, 'Testing descriptiopn', '65.00', '1', '10,1,9', 'By Schedule', '2019-04-13', '1:00 AM', '2019-04-13 01:00:00', 0, 'pid!06543af914c440fd339f7f3a09a0e4fe.jpg', '1', 'sssssssssssssssssss', '3', '4', '5', '6', 0, 0, '2019-04-12 23:03:38', '2019-04-12 23:03:38'),
(69, 1, 'testing project 12', 2, '', 2, 'ddddddddddd', '65.00', '3', '11,1', 'By Schedule', '2019-04-13', '1:00 AM', '2019-04-13 01:00:00', 0, 'pid!9612e324129c1a4c3c025f7ad91a129f.jpg', '1', 'testing', '12', '2', '2', '3', 0, 0, '2019-04-13 01:24:02', '2019-04-13 01:24:02'),
(70, 1, 'tesss', 10, '', 7, 'ffff', '15.00', '5', '10,1,18', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'sss', '3', '4', '5', '6', 0, 0, '2019-04-16 01:59:25', '2019-04-16 01:59:25'),
(71, 1, 'tesss', 10, '', 7, 'ffff', '15.00', '5', '10,1,18', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'sss', '3', '4', '5', '6', 0, 0, '2019-04-16 02:01:59', '2019-04-16 02:01:59'),
(72, 1, 'tesss', 10, '', 7, 'ffff', '15.00', '5', '10,1,18', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'sss', '3', '4', '5', '6', 0, 0, '2019-04-16 02:07:02', '2019-04-16 02:07:02'),
(73, 1, 'tesss', 10, '', 7, 'ffff', '15.00', '5', '10,1,18', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'sss', '3', '4', '5', '6', 0, 0, '2019-04-16 02:08:04', '2019-04-16 02:08:04'),
(74, 1, 'tesss', 10, '', 7, 'ffff', '15.00', '5', '10,1,18', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'sss', '3', '4', '5', '6', 0, 0, '2019-04-16 02:09:21', '2019-04-16 02:09:21'),
(75, 1, 'tesss', 10, '', 7, 'ffff', '15.00', '5', '10,1,18', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'sss', '3', '4', '5', '6', 0, 0, '2019-04-16 02:09:51', '2019-04-16 02:09:51'),
(76, 1, 'testingprojectname', 10, '', 5, 'testing description', '15.00', '3', '10,1', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'dddd', '2', '3', '4', '5', 0, 0, '2019-04-16 02:12:16', '2019-04-16 02:12:16'),
(77, 1, 'testingprojectname', 10, '', 5, 'testing description', '15.00', '3', '10,1', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'dddd', '2', '3', '4', '5', 0, 0, '2019-04-16 02:18:50', '2019-04-16 02:18:50'),
(78, 1, 'testingprojectname', 10, '', 5, 'testing description', '15.00', '3', '10,1', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'dddd', '2', '3', '4', '5', 0, 0, '2019-04-16 02:26:03', '2019-04-16 02:26:03'),
(79, 1, 'testingprojectname', 2, '', 5, 'g', '15.00', '1', '10,1', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 's', '2', '3', '4', '5', 0, 0, '2019-04-16 02:29:33', '2019-04-16 02:29:33'),
(80, 1, 'testte', 11, '', 5, 'te', '15.00', '4', '10,1', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, '', '1', 'd', '2', '3', '4', '5', 0, 0, '2019-04-16 03:43:07', '2019-04-16 03:43:07'),
(81, 1, 'testte', 11, '', 5, 'te', '15.00', '4', '10,1', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, '', '1', 'd', '2', '3', '4', '5', 0, 0, '2019-04-16 03:45:19', '2019-04-16 03:45:19'),
(82, 1, 'testing hhh', 3, '', 5, 'ggggggggggg', '15.00', '5', '10,1,3', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, '', '1', 'd', '2', '3', '4', '5', 0, 0, '2019-04-16 03:47:45', '2019-04-16 03:47:45'),
(83, 1, 'testing projectname', 12, '', 7, 'ffff', '15.00', '4', '10,1,18', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 's', '2', '3', '4', '5', 0, 0, '2019-04-16 03:54:57', '2019-04-16 03:54:57'),
(84, 1, 'testing projectname', 12, '', 7, 'ffff', '15.00', '4', '10,1,18', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 's', '2', '3', '4', '5', 0, 0, '2019-04-16 03:56:24', '2019-04-16 03:56:24'),
(85, 1, 'testing projectname', 12, '', 7, 'ffff', '15.00', '4', '10,1,18', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 's', '2', '3', '4', '5', 0, 0, '2019-04-16 03:58:15', '2019-04-16 03:58:15'),
(86, 1, 'testing projectname', 12, '', 7, 'ffff', '15.00', '4', '10,1,18', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 's', '2', '3', '4', '5', 0, 0, '2019-04-16 04:01:14', '2019-04-16 04:01:14'),
(87, 1, 'testing projectname', 12, '', 7, 'ffff', '15.00', '4', '10,1,18', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 's', '2', '3', '4', '5', 0, 0, '2019-04-16 04:04:02', '2019-04-16 04:04:02'),
(88, 1, 'ds', 2, '', 5, 'ds', '15.00', '5', '10,1,5', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, '', '1', 'sssssss', '1', '2', '3', '4', 0, 0, '2019-04-16 04:07:12', '2019-04-16 04:07:12'),
(89, 1, 'ds', 2, '', 5, 'ds', '15.00', '5', '10,1,5', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, '', '1', 'sssssss', '1', '2', '3', '4', 0, 0, '2019-04-16 04:09:19', '2019-04-16 04:09:19'),
(90, 1, 'ddd', 11, '', 5, 'ddddddddddddd', '15.00', '5', '10,1,18', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'dffff', '2', '3', '4', '5', 0, 0, '2019-04-16 04:11:55', '2019-04-16 04:11:55'),
(91, 1, 'ddd', 11, '', 5, 'ddddddddddddd', '15.00', '5', '10,1,18', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'dffff', '2', '3', '4', '5', 0, 0, '2019-04-16 04:13:23', '2019-04-16 04:13:23'),
(92, 1, 'ddd', 11, '', 5, 'ddddddddddddd', '15.00', '5', '10,1,18', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'dffff', '2', '3', '4', '5', 0, 0, '2019-04-16 04:20:57', '2019-04-16 04:20:57'),
(93, 1, 'ddd', 11, '', 5, 'ddddddddddddd', '15.00', '5', '10,1,18', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'dffff', '2', '3', '4', '5', 0, 0, '2019-04-16 04:24:01', '2019-04-16 04:24:01'),
(94, 1, 'ddd', 11, '', 5, 'ddddddddddddd', '15.00', '5', '10,1,18', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'dffff', '2', '3', '4', '5', 0, 0, '2019-04-16 04:27:20', '2019-04-16 04:27:20'),
(95, 1, 'ttt', 11, '', 5, 'ttt', '15.00', '1', '10', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, '', '', '', '3', '4', '5', '6', 0, 0, '2019-04-16 04:31:04', '2019-04-16 04:31:04'),
(96, 1, 'ttt', 11, '', 5, 'ttt', '15.00', '1', '10', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, '', '', '', '3', '4', '5', '6', 0, 0, '2019-04-16 04:32:50', '2019-04-16 04:32:50'),
(97, 1, 'ttt', 11, '', 5, 'ttt', '15.00', '1', '10', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, '', '1', 'ddd', '3', '4', '5', '6', 0, 0, '2019-04-16 04:35:05', '2019-04-16 04:35:05'),
(98, 1, 'ddssd', 12, '', 5, 'ddd', '15.00', '3', '10,1,5', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, '', '1', 'd', '1', '2', '3', '4', 0, 0, '2019-04-16 04:38:34', '2019-04-16 04:38:34'),
(99, 1, 'ddssd', 12, '', 5, 'ddd', '15.00', '3', '10,1,5', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, '', '1', 'd', '1', '2', '3', '4', 0, 0, '2019-04-16 04:41:35', '2019-04-16 04:41:35'),
(100, 1, 'testtt', 11, '', 5, 'gg', '15.00', '1', '10,1', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, '', '1', 'dddddddddddddddd', '1', '2', '3', '4', 0, 0, '2019-04-16 04:55:49', '2019-04-16 04:55:49'),
(101, 1, 'ttttt', 11, '', 5, 'fff', '15.00', '4', '10,1,13', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, '', '1', 's', '1', '2', '3', '4', 0, 0, '2019-04-16 05:00:28', '2019-04-16 05:00:28'),
(102, 1, 'ttttt', 11, '', 5, 'fff', '15.00', '4', '10,1,13', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, '', '1', 's', '1', '2', '3', '4', 0, 0, '2019-04-16 05:02:39', '2019-04-16 05:02:39'),
(103, 1, 'ttttt', 11, '', 5, 'fff', '15.00', '4', '10,1,13', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, '', '1', 's', '1', '2', '3', '4', 0, 0, '2019-04-16 05:08:17', '2019-04-16 05:08:17'),
(104, 1, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '10,1', 'By Schedule', '2019-04-29', '1:00 AM', '2019-04-29 01:00:00', 0, '', '1', 'sss', '3', '4', '5', '6', 0, 0, '2019-04-16 05:16:00', '2019-04-16 05:16:00'),
(105, 1, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '10,1', 'By Schedule', '2019-04-29', '1:00 AM', '2019-04-29 01:00:00', 0, '', '1', 'sss', '3', '4', '5', '6', 0, 0, '2019-04-16 05:19:22', '2019-04-16 05:19:22'),
(106, 1, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '10,1', 'By Schedule', '2019-04-29', '1:00 AM', '2019-04-29 01:00:00', 0, '', '1', 'sss', '3', '4', '5', '6', 0, 0, '2019-04-16 05:22:25', '2019-04-16 05:22:25'),
(107, 1, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '10,1', 'By Schedule', '2019-04-29', '1:00 AM', '2019-04-29 01:00:00', 0, '', '1', 'sss', '3', '4', '5', '6', 0, 0, '2019-04-16 05:23:11', '2019-04-16 05:23:11'),
(108, 1, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '', 'By Schedule', '2019-04-16', '05:25 am', '2019-04-16 05:25:00', 1, '', '1', 'sss', '3', '4', '5', '6', 2, 0, '2019-04-16 05:25:18', '2019-04-16 05:25:18'),
(109, 1, 'ttttt', 11, '', 5, 'fff', '15.00', '4', '10,1,13', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, '', '1', 's', '1', '2', '3', '4', 0, 0, '2019-04-16 05:31:10', '2019-04-16 05:31:10'),
(110, 1, 'ttttt', 11, '', 5, 'fff', '15.00', '4', '10,1,13', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, '', '1', 's', '1', '2', '3', '4', 0, 0, '2019-04-16 05:33:24', '2019-04-16 05:33:24'),
(111, 1, 'ttttt', 11, '', 5, 'fff', '15.00', '4', '10,1,13', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, '', '1', 's', '1', '2', '3', '4', 0, 0, '2019-04-16 05:35:45', '2019-04-16 05:35:45'),
(112, 1, 'ddd', 3, '', 9, 'ff', '65.00', '4', '10,1', 'By Schedule', '2019-04-29', '12:00 AM', '2019-04-29 00:00:00', 0, '', '1', 'd', '1', '2', '3', '3', 0, 0, '2019-04-16 05:37:25', '2019-04-16 05:37:25'),
(113, 1, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '', 'By Schedule', '2019-04-16', '05:48 am', '2019-04-16 05:48:00', 1, '', '1', 'sss', '3', '4', '5', '6', 2, 0, '2019-04-16 05:48:24', '2019-04-16 05:48:24'),
(114, 99, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '', 'By Schedule', '2019-04-16', '05:52 am', '2019-04-16 05:52:00', 1, '', '1', 'sss', '3', '4', '5', '6', 2, 0, '2019-04-16 05:52:08', '2019-04-16 05:52:08'),
(115, 99, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '', 'By Schedule', '2019-04-16', '05:53 am', '2019-04-16 05:53:00', 1, '', '1', 'sss', '3', '4', '5', '6', 2, 0, '2019-04-16 05:53:31', '2019-04-16 05:53:31'),
(116, 99, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '', 'By Schedule', '2019-04-16', '05:54 am', '2019-04-16 05:54:00', 1, '', '1', 'sss', '3', '4', '5', '6', 2, 0, '2019-04-16 05:54:37', '2019-04-16 05:54:37'),
(117, 99, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '', 'By Schedule', '2019-04-16', '06:00 am', '2019-04-16 06:00:00', 1, '', '1', 'sss', '3', '4', '5', '6', 2, 0, '2019-04-16 06:00:43', '2019-04-16 06:00:43'),
(118, 99, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '', 'By Schedule', '2019-04-16', '06:08 am', '2019-04-16 06:08:00', 1, '', '1', 'sss', '3', '4', '5', '6', 2, 0, '2019-04-16 06:08:26', '2019-04-16 06:08:26'),
(119, 99, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '', 'By Schedule', '2019-04-16', '06:09 am', '2019-04-16 06:09:00', 1, '', '1', 'sss', '3', '4', '5', '6', 2, 0, '2019-04-16 06:09:59', '2019-04-16 06:09:59'),
(120, 99, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '', 'By Schedule', '2019-04-16', '06:10 am', '2019-04-16 06:10:00', 1, '', '1', 'sss', '3', '4', '5', '6', 2, 0, '2019-04-16 06:10:38', '2019-04-16 06:10:38'),
(121, 99, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '', 'By Schedule', '2019-04-16', '06:13 am', '2019-04-16 06:13:00', 1, '', '1', 'sss', '3', '4', '5', '6', 2, 0, '2019-04-16 06:13:46', '2019-04-16 06:13:46'),
(122, 99, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '', 'By Schedule', '2019-04-16', '06:14 am', '2019-04-16 06:14:00', 1, '', '1', 'sss', '3', '4', '5', '6', 2, 0, '2019-04-16 06:14:30', '2019-04-16 06:14:30'),
(123, 99, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '', 'By Schedule', '2019-04-16', '06:16 am', '2019-04-16 06:16:00', 1, '', '1', 'sss', '3', '4', '5', '6', 2, 0, '2019-04-16 06:16:15', '2019-04-16 06:16:15'),
(124, 99, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '', 'By Schedule', '2019-04-16', '06:22 am', '2019-04-16 06:22:00', 1, '', '1', 'sss', '3', '4', '5', '6', 2, 0, '2019-04-16 06:22:38', '2019-04-16 06:22:38'),
(125, 99, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '', 'By Schedule', '2019-04-16', '06:24 am', '2019-04-16 06:24:00', 1, '', '1', 'sss', '3', '4', '5', '6', 2, 0, '2019-04-16 06:24:49', '2019-04-16 06:24:49'),
(126, 99, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '', 'By Schedule', '2019-04-16', '06:25 am', '2019-04-16 06:25:00', 1, '', '1', 'sss', '3', '4', '5', '6', 2, 0, '2019-04-16 06:25:25', '2019-04-16 06:25:25'),
(127, 99, 'tesss', 10, '', 7, 'ffff', '15.00', '1', '', 'By Schedule', '2019-04-16', '06:26 am', '2019-04-16 06:26:00', 1, '', '1', 'sss', '3', '4', '5', '6', 2, 0, '2019-04-16 06:26:19', '2019-04-16 06:26:19'),
(128, 1, 'testprojectname', 12, '', 5, 'g', '15.00', '4', '10,1,13', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'ddddddd', '2', '3', '4', '5', 0, 0, '2019-04-16 06:49:41', '2019-04-16 06:49:41'),
(129, 1, 'testingprojectname', 12, '', 5, 'ffffffffff', '15.00', '1', '10,1,24', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, '', '1', 'ddd', '2', '3', '4', '5', 0, 0, '2019-04-16 06:52:36', '2019-04-16 06:52:36'),
(130, 1, 'testingprojectname', 12, '', 5, 'ffffffffff', '15.00', '1', '10,1,24', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, '', '1', 'ddd', '2', '3', '4', '5', 0, 0, '2019-04-16 06:53:27', '2019-04-16 06:53:27'),
(131, 1, 'checkimagetobucket', 12, '', 5, 'checkimagetobucket', '15.00', '1', '10,1,22', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'ddd', '1', '2', '3', '4', 0, 0, '2019-04-16 06:55:14', '2019-04-16 06:55:14'),
(132, 1, 'galleryone', 11, '', 5, 'fffffffffff', '15.00', '4', '1,10,22', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, '', '1', 'testing custom entry', '4', '5', '6', '7', 0, 0, '2019-04-17 00:47:56', '2019-04-17 05:15:08'),
(133, 1, 'final Gallery Image update', 4, '', 10, 'fffff', '80.00', '1', '1,10', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', '', '2', '3', '4', '5', 0, 0, '2019-04-17 05:17:05', '2019-04-17 05:30:02'),
(134, 1, 'dsds', 12, '', 4, 'dsds', '25.00', '5', '10', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '', '', '3', '4', '5', '5', 0, 0, '2019-04-17 05:31:54', '2019-04-17 05:31:54'),
(135, 1, 'testing project name', 9, '', 7, 'dddddddddddddddddddddddd', '15.00', '1', '10,1,22', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, '', '1,3,1,3', 'sssssss', '2', '3', '4', '5', 0, 0, '2019-04-18 04:51:01', '2019-04-18 04:51:01'),
(136, 1, 'testing project name', 9, '', 7, 'dddddddddddddddddddddddd', '15.00', '1', '10,1,22', 'By Schedule', '2019-04-30', '1:00 AM', '2019-04-30 01:00:00', 0, '', '1,3,1,3,1,3', 'sssssss', '2', '3', '4', '5', 0, 0, '2019-04-18 04:53:11', '2019-04-18 04:53:11'),
(137, 1, 'testprojectnameApr18', 1, '', 2, 'fffffffff', '65.00', '5', '1,10,48', 'By Schedule', '2019-04-01', '3:00 AM', '2019-04-01 03:00:00', 0, '', '1', 'dddddddddddd', '2', '2', '2', '2', 0, 0, '2019-04-18 04:57:30', '2019-04-22 07:13:33'),
(138, 1, 'testing', 12, '', 7, 'testing testig testing', '15.00', '4', '1,10', 'By Schedule', '2019-04-29', '12:00 AM', '2019-04-29 00:00:00', 0, '', '', 'ddd', '2', '3', '4', '5', 0, 0, '2019-04-21 23:46:24', '2019-04-22 01:27:29'),
(139, 1, 'testinf final res and aimgage', 1, '', 3, 'gg', '35.00', '5', '1,10', 'By Schedule', '2019-04-10', '12:00 AM', '2019-04-10 00:00:00', 0, '', '9', '', '2', '3', '4', '5', 0, 0, '2019-04-22 01:50:47', '2019-04-22 02:28:46'),
(140, 1, 'testing project dby pradeep', 1, '', 9, 'this is testing project by pradeep', '65.00', '5', '1,10', 'By Schedule', '2019-04-30', '12:00 AM', '2019-04-30 00:00:00', 0, '', '1', 'ddd', '2', '3', '4', '6', 0, 0, '2019-04-22 05:13:25', '2019-04-22 06:27:09'),
(141, 1, 'testing project', 12, '', 4, 'fffffff', '25.00', '5', '1,10,22', 'By Schedule', '2019-04-30', '2:00 AM', '2019-04-30 02:00:00', 0, '', '1', 'd', '2', '3', '4', '5', 0, 0, '2019-04-22 06:28:53', '2019-04-22 06:44:11');

-- --------------------------------------------------------

--
-- Table structure for table `bb_promoamounttype`
--

CREATE TABLE `bb_promoamounttype` (
  `id` int(11) NOT NULL,
  `amounttype` varchar(225) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_promoamounttype`
--

INSERT INTO `bb_promoamounttype` (`id`, `amounttype`, `status`) VALUES
(1, 'Fixed Amount', 0),
(2, 'Percentage', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_promoapplyto`
--

CREATE TABLE `bb_promoapplyto` (
  `id` int(11) NOT NULL,
  `applyto` varchar(225) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_promoapplyto`
--

INSERT INTO `bb_promoapplyto` (`id`, `applyto`, `status`) VALUES
(1, 'Order Total', 0),
(2, 'Each Ticket', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_promoavailability`
--

CREATE TABLE `bb_promoavailability` (
  `id` int(11) NOT NULL,
  `promoId` int(11) NOT NULL,
  `avAttrType` tinyint(1) NOT NULL,
  `avAttrVal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bb_promocode`
--

CREATE TABLE `bb_promocode` (
  `id` int(11) NOT NULL,
  `studioId` text NOT NULL,
  `promoName` varchar(255) NOT NULL,
  `promoCode` varchar(255) NOT NULL,
  `promoCodeUsed` int(11) NOT NULL DEFAULT '0',
  `promoDescription` text NOT NULL,
  `promoQty` int(11) NOT NULL,
  `promoIsApproRequired` tinyint(1) NOT NULL DEFAULT '0',
  `promoRApplyTo` varchar(125) NOT NULL,
  `promoRTaxType` varchar(125) NOT NULL,
  `promoRDiscountType` varchar(225) NOT NULL,
  `promoRDiscountVal` decimal(10,2) NOT NULL,
  `schStDate` date NOT NULL,
  `schStTime` varchar(125) NOT NULL,
  `schEndDate` date NOT NULL,
  `schEndTime` varchar(125) NOT NULL,
  `promoSchStDtTime` datetime NOT NULL,
  `promoSchEndDtTime` datetime NOT NULL,
  `eventAssociated` varchar(225) NOT NULL,
  `isDeleted` tinyint(11) NOT NULL DEFAULT '0',
  `status` tinyint(11) NOT NULL COMMENT '0=>Active,1=>DeActive,2=>Draft,3=>Archive',
  `promoCreatedBy` int(11) NOT NULL,
  `promoCreatedDate` date NOT NULL,
  `promoUpdatedBy` int(11) DEFAULT NULL,
  `promoUpdatedDate` date NOT NULL,
  `liveStudId` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_promocode`
--

INSERT INTO `bb_promocode` (`id`, `studioId`, `promoName`, `promoCode`, `promoCodeUsed`, `promoDescription`, `promoQty`, `promoIsApproRequired`, `promoRApplyTo`, `promoRTaxType`, `promoRDiscountType`, `promoRDiscountVal`, `schStDate`, `schStTime`, `schEndDate`, `schEndTime`, `promoSchStDtTime`, `promoSchEndDtTime`, `eventAssociated`, `isDeleted`, `status`, `promoCreatedBy`, `promoCreatedDate`, `promoUpdatedBy`, `promoUpdatedDate`, `liveStudId`, `coupon_id`) VALUES
(1, '360', 'cpone', 'cp121', 1, 'this is for 20 rs dicount', 12, 1, '2', '1', '1', '10.00', '2019-03-20', '12:00 AM', '2019-03-31', '3:00 AM', '2019-03-20 00:00:00', '2019-03-31 03:00:00', '1,3,4', 0, 0, 1, '2019-03-25', 1, '2019-03-26', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_promocode_bkp`
--

CREATE TABLE `bb_promocode_bkp` (
  `id` int(11) NOT NULL,
  `studioId` text NOT NULL,
  `promoName` varchar(255) NOT NULL,
  `promoCode` varchar(255) NOT NULL,
  `promoCodeUsed` int(11) NOT NULL DEFAULT '0',
  `promoDescription` text NOT NULL,
  `promoQty` int(11) NOT NULL,
  `promoIsApproRequired` tinyint(1) NOT NULL DEFAULT '0',
  `promoRApplyTo` varchar(125) NOT NULL,
  `promoRTaxType` varchar(125) NOT NULL,
  `promoRDiscountType` varchar(225) NOT NULL,
  `promoRDiscountVal` varchar(255) NOT NULL,
  `schStDate` date NOT NULL,
  `schStTime` varchar(125) NOT NULL,
  `schEndDate` date NOT NULL,
  `schEndTime` varchar(125) NOT NULL,
  `promoSchStDtTime` datetime NOT NULL,
  `promoSchEndDtTime` datetime NOT NULL,
  `eventAssociated` varchar(225) NOT NULL,
  `isDeleted` tinyint(11) NOT NULL DEFAULT '0',
  `status` tinyint(11) NOT NULL COMMENT '0=>Active,1=>DeActive,2=>Draft,3=>Archive',
  `promoCreatedBy` int(11) NOT NULL,
  `promoCreatedDate` date NOT NULL,
  `promoUpdatedBy` int(11) DEFAULT NULL,
  `promoUpdatedDate` date NOT NULL,
  `liveStudId` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bb_promoschedule`
--

CREATE TABLE `bb_promoschedule` (
  `id` int(11) NOT NULL,
  `promoId` int(11) NOT NULL,
  `schStDate` date NOT NULL,
  `schStTime` varchar(255) NOT NULL,
  `schEndDate` date NOT NULL,
  `schEndTime` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bb_promotaxtype`
--

CREATE TABLE `bb_promotaxtype` (
  `id` int(11) NOT NULL,
  `taxtype` varchar(225) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_promotaxtype`
--

INSERT INTO `bb_promotaxtype` (`id`, `taxtype`, `status`) VALUES
(1, 'Before Tax', 0),
(2, 'After Tax', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_prooptionadd`
--

CREATE TABLE `bb_prooptionadd` (
  `id` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `propersoptions_id` int(11) NOT NULL,
  `proOptionVal` int(11) NOT NULL,
  `option_value` varchar(155) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_prooptionadd`
--

INSERT INTO `bb_prooptionadd` (`id`, `proId`, `propersoptions_id`, `proOptionVal`, `option_value`, `created`, `modified`) VALUES
(14, 13, 0, 4, 'r1', '2019-03-28 10:39:00', '2019-03-28 10:39:00'),
(23, 16, 20, 5, 'c1', '2019-03-28 11:02:00', '2019-03-28 11:02:00'),
(24, 16, 20, 5, 'r1', '2019-03-28 11:02:00', '2019-03-28 11:02:00'),
(25, 16, 21, 4, 'c2', '2019-03-28 11:02:00', '2019-03-28 11:02:00'),
(26, 16, 21, 4, 'r2', '2019-03-28 11:02:00', '2019-03-28 11:02:00'),
(27, 17, 22, 5, 'op1', '2019-03-29 04:22:28', '2019-03-29 04:22:28'),
(28, 17, 23, 4, 'or1', '2019-03-29 04:22:28', '2019-03-29 04:22:28'),
(29, 18, 24, 4, 'or0', '2019-03-29 04:57:04', '2019-03-29 04:57:04'),
(30, 18, 24, 4, 'or1', '2019-03-29 04:57:04', '2019-03-29 04:57:04'),
(31, 18, 24, 4, 'or2', '2019-03-29 04:57:04', '2019-03-29 04:57:04'),
(32, 18, 25, 5, 'oc0', '2019-03-29 04:57:04', '2019-03-29 04:57:04'),
(33, 18, 25, 5, 'oc1', '2019-03-29 04:57:04', '2019-03-29 04:57:04'),
(34, 18, 25, 5, 'oc2', '2019-03-29 04:57:04', '2019-03-29 04:57:04'),
(86, 19, 52, 4, 'opr2', '2019-03-29 07:21:22', '2019-03-29 07:21:22'),
(87, 19, 53, 5, 'opc2', '2019-03-29 07:21:22', '2019-03-29 07:21:22'),
(88, 19, 53, 5, 'opc4', '2019-03-29 07:21:22', '2019-03-29 07:21:22'),
(89, 19, 54, 5, 'oc3', '2019-03-29 07:21:22', '2019-03-29 07:21:22'),
(90, 19, 54, 5, 'oc3_1', '2019-03-29 07:21:22', '2019-03-29 07:21:22'),
(109, 15, 63, 6, 'option1', '2019-03-29 10:02:21', '2019-03-29 10:02:21'),
(110, 15, 63, 6, 'option2', '2019-03-29 10:02:21', '2019-03-29 10:02:21'),
(111, 15, 64, 6, 'option1', '2019-03-29 10:02:21', '2019-03-29 10:02:21'),
(112, 15, 64, 6, 'option2', '2019-03-29 10:02:21', '2019-03-29 10:02:21'),
(113, 22, 95, 4, 'opt1', '2019-04-02 05:48:03', '2019-04-02 05:48:03'),
(114, 22, 96, 4, 'op2_0', '2019-04-02 05:48:03', '2019-04-02 05:48:03'),
(115, 26, 97, 4, 'op1', '2019-04-02 06:26:27', '2019-04-02 06:26:27'),
(116, 26, 98, 4, 'op2', '2019-04-02 06:26:27', '2019-04-02 06:26:27'),
(117, 27, 99, 4, 'op1', '2019-04-02 06:31:53', '2019-04-02 06:31:53'),
(151, 28, 110, 4, 'opup_1_1', '2019-04-02 11:03:51', '2019-04-02 11:03:51'),
(152, 28, 110, 4, 'opup_1_2', '2019-04-02 11:03:51', '2019-04-02 11:03:51'),
(153, 28, 110, 4, 'opup_1_3', '2019-04-02 11:03:51', '2019-04-02 11:03:51'),
(154, 28, 111, 4, 'op2_up_1', '2019-04-02 11:03:51', '2019-04-02 11:03:51'),
(155, 28, 111, 4, 'op2_up_2', '2019-04-02 11:03:51', '2019-04-02 11:03:51'),
(156, 28, 111, 4, 'op2_up_3', '2019-04-02 11:03:51', '2019-04-02 11:03:51'),
(157, 28, 111, 4, 'op2_up_4', '2019-04-02 11:03:51', '2019-04-02 11:03:51'),
(183, 35, 0, 0, 'op2', '2019-04-04 06:25:58', '2019-04-04 06:25:58'),
(184, 35, 0, 0, 'op3', '2019-04-04 06:25:59', '2019-04-04 06:25:59'),
(185, 35, 171, 0, 'option1', '2019-04-04 06:25:59', '2019-04-04 06:25:59'),
(186, 35, 171, 0, 'option2', '2019-04-04 06:25:59', '2019-04-04 06:25:59'),
(187, 35, 171, 0, 'option3', '2019-04-04 06:25:59', '2019-04-04 06:25:59'),
(188, 36, 173, 0, 'option2', '2019-04-04 06:30:50', '2019-04-04 06:30:50'),
(189, 36, 173, 0, 'option3', '2019-04-04 06:30:50', '2019-04-04 06:30:50'),
(190, 36, 0, 0, 'op3', '2019-04-04 06:30:50', '2019-04-04 06:30:50'),
(191, 36, 0, 0, 'op2', '2019-04-04 06:30:50', '2019-04-04 06:30:50'),
(192, 41, 176, 0, 'option1', '2019-04-04 06:46:20', '2019-04-04 06:46:20'),
(193, 41, 176, 0, 'option2', '2019-04-04 06:46:20', '2019-04-04 06:46:20'),
(194, 41, 176, 0, 'option3', '2019-04-04 06:46:20', '2019-04-04 06:46:20'),
(195, 42, 179, 0, 'option1', '2019-04-04 06:47:15', '2019-04-04 06:47:15'),
(196, 42, 179, 0, 'option2', '2019-04-04 06:47:15', '2019-04-04 06:47:15'),
(197, 42, 179, 0, 'option3', '2019-04-04 06:47:15', '2019-04-04 06:47:15'),
(198, 42, 180, 0, 'ttt', '2019-04-04 06:47:15', '2019-04-04 06:47:15'),
(199, 42, 180, 0, 'ttt1', '2019-04-04 06:47:15', '2019-04-04 06:47:15'),
(200, 42, 180, 0, 'tttt2', '2019-04-04 06:47:15', '2019-04-04 06:47:15'),
(201, 43, 183, 0, 'op3', '2019-04-04 06:53:18', '2019-04-04 06:53:18'),
(202, 43, 183, 0, 'op2', '2019-04-04 06:53:18', '2019-04-04 06:53:18'),
(203, 44, 186, 0, 'op3', '2019-04-04 06:54:28', '2019-04-04 06:54:28'),
(204, 44, 186, 0, 'op2', '2019-04-04 06:54:28', '2019-04-04 06:54:28'),
(205, 45, 189, 4, 'op3', '2019-04-04 07:00:57', '2019-04-04 07:00:57'),
(206, 45, 189, 4, 'op2', '2019-04-04 07:00:58', '2019-04-04 07:00:58'),
(207, 45, 190, 6, 'option1', '2019-04-04 07:00:58', '2019-04-04 07:00:58'),
(208, 45, 190, 6, 'option2', '2019-04-04 07:00:58', '2019-04-04 07:00:58'),
(209, 45, 190, 6, 'option3', '2019-04-04 07:00:58', '2019-04-04 07:00:58'),
(210, 46, 193, 4, 'op3', '2019-04-04 07:01:44', '2019-04-04 07:01:44'),
(211, 46, 193, 4, 'op2', '2019-04-04 07:01:44', '2019-04-04 07:01:44'),
(212, 46, 194, 6, 'option1', '2019-04-04 07:01:44', '2019-04-04 07:01:44'),
(213, 46, 194, 6, 'option2', '2019-04-04 07:01:44', '2019-04-04 07:01:44'),
(214, 46, 194, 6, 'option3', '2019-04-04 07:01:44', '2019-04-04 07:01:44'),
(264, 47, 234, 4, 'op1', '2019-04-04 09:00:22', '2019-04-04 09:00:22'),
(265, 47, 235, 6, 'option1', '2019-04-04 09:00:22', '2019-04-04 09:00:22'),
(273, 48, 243, 4, 'opt3', '2019-04-04 10:37:53', '2019-04-04 10:37:53'),
(274, 48, 243, 4, 'opt4', '2019-04-04 10:37:53', '2019-04-04 10:37:53'),
(281, 49, 248, 4, 'op4', '2019-04-04 10:51:36', '2019-04-04 10:51:36'),
(282, 49, 249, 6, 'option1', '2019-04-04 10:51:36', '2019-04-04 10:51:36'),
(283, 50, 252, 6, 'option1', '2019-04-04 10:55:33', '2019-04-04 10:55:33'),
(284, 50, 252, 6, 'option2', '2019-04-04 10:55:33', '2019-04-04 10:55:33'),
(285, 50, 252, 6, 'option3', '2019-04-04 10:55:34', '2019-04-04 10:55:34'),
(286, 51, 253, 4, 'op1', '2019-04-11 06:22:26', '2019-04-11 06:22:26'),
(287, 51, 253, 4, 'op2', '2019-04-11 06:22:26', '2019-04-11 06:22:26'),
(288, 51, 253, 4, 'op3', '2019-04-11 06:22:26', '2019-04-11 06:22:26'),
(289, 52, 255, 4, 'op1', '2019-04-11 06:24:02', '2019-04-11 06:24:02'),
(290, 52, 255, 4, 'op2', '2019-04-11 06:24:03', '2019-04-11 06:24:03'),
(291, 52, 255, 4, 'op3', '2019-04-11 06:24:03', '2019-04-11 06:24:03'),
(292, 53, 257, 4, 'op1', '2019-04-11 06:35:54', '2019-04-11 06:35:54'),
(293, 53, 257, 4, 'op2', '2019-04-11 06:35:54', '2019-04-11 06:35:54'),
(294, 53, 257, 4, 'op3', '2019-04-11 06:35:54', '2019-04-11 06:35:54'),
(295, 54, 259, 4, 'op1', '2019-04-11 06:38:11', '2019-04-11 06:38:11'),
(296, 54, 259, 4, 'op2', '2019-04-11 06:38:11', '2019-04-11 06:38:11'),
(297, 54, 259, 4, 'op3', '2019-04-11 06:38:11', '2019-04-11 06:38:11'),
(298, 55, 261, 4, 'op1', '2019-04-11 06:58:42', '2019-04-11 06:58:42'),
(299, 55, 261, 4, 'op2', '2019-04-11 06:58:42', '2019-04-11 06:58:42'),
(300, 56, 263, 4, 'op1', '2019-04-11 07:01:39', '2019-04-11 07:01:39'),
(301, 56, 263, 4, 'op2', '2019-04-11 07:01:39', '2019-04-11 07:01:39'),
(302, 56, 263, 4, 'op3', '2019-04-11 07:01:39', '2019-04-11 07:01:39'),
(303, 57, 265, 4, 'op1', '2019-04-11 07:03:51', '2019-04-11 07:03:51'),
(304, 57, 265, 4, 'op2', '2019-04-11 07:03:51', '2019-04-11 07:03:51'),
(305, 57, 265, 4, 'op3', '2019-04-11 07:03:51', '2019-04-11 07:03:51'),
(306, 58, 267, 4, 'op1', '2019-04-11 07:04:43', '2019-04-11 07:04:43'),
(307, 58, 267, 4, 'op2', '2019-04-11 07:04:43', '2019-04-11 07:04:43'),
(308, 58, 267, 4, 'op3', '2019-04-11 07:04:43', '2019-04-11 07:04:43'),
(309, 59, 269, 4, 'op1', '2019-04-11 07:07:38', '2019-04-11 07:07:38'),
(310, 59, 269, 4, 'op2', '2019-04-11 07:07:38', '2019-04-11 07:07:38'),
(311, 59, 269, 4, 'op3', '2019-04-11 07:07:38', '2019-04-11 07:07:38'),
(312, 60, 271, 4, 'op1', '2019-04-11 07:09:55', '2019-04-11 07:09:55'),
(313, 60, 271, 4, 'op2', '2019-04-11 07:09:55', '2019-04-11 07:09:55'),
(314, 60, 271, 4, 'op3', '2019-04-11 07:09:55', '2019-04-11 07:09:55'),
(315, 61, 273, 4, 'op1', '2019-04-11 07:10:21', '2019-04-11 07:10:21'),
(316, 61, 273, 4, 'op2', '2019-04-11 07:10:21', '2019-04-11 07:10:21'),
(317, 61, 273, 4, 'op3', '2019-04-11 07:10:21', '2019-04-11 07:10:21'),
(318, 62, 275, 4, 'op1', '2019-04-11 07:10:41', '2019-04-11 07:10:41'),
(319, 62, 275, 4, 'op2', '2019-04-11 07:10:41', '2019-04-11 07:10:41'),
(320, 62, 275, 4, 'op3', '2019-04-11 07:10:41', '2019-04-11 07:10:41'),
(321, 63, 277, 4, 'op1', '2019-04-11 07:11:30', '2019-04-11 07:11:30'),
(322, 63, 277, 4, 'op2', '2019-04-11 07:11:30', '2019-04-11 07:11:30'),
(323, 63, 277, 4, 'op3', '2019-04-11 07:11:31', '2019-04-11 07:11:31'),
(324, 64, 279, 4, 'op1', '2019-04-11 07:12:30', '2019-04-11 07:12:30'),
(325, 64, 279, 4, 'op2', '2019-04-11 07:12:30', '2019-04-11 07:12:30'),
(326, 64, 279, 4, 'op3', '2019-04-11 07:12:30', '2019-04-11 07:12:30'),
(327, 65, 281, 4, 'op1', '2019-04-11 07:16:05', '2019-04-11 07:16:05'),
(328, 65, 281, 4, 'op2', '2019-04-11 07:16:05', '2019-04-11 07:16:05'),
(329, 65, 281, 4, 'op3', '2019-04-11 07:16:05', '2019-04-11 07:16:05'),
(351, 66, 290, 4, 'op1', '2019-04-12 04:07:24', '2019-04-12 04:07:24'),
(352, 66, 290, 4, 'op2', '2019-04-12 04:07:24', '2019-04-12 04:07:24'),
(353, 66, 290, 4, 'op3', '2019-04-12 04:07:24', '2019-04-12 04:07:24'),
(389, 67, 312, 4, 'op1', '2019-04-12 11:19:50', '2019-04-12 11:19:50'),
(390, 67, 312, 4, 'op2', '2019-04-12 11:19:50', '2019-04-12 11:19:50'),
(391, 67, 312, 4, 'op3', '2019-04-12 11:19:50', '2019-04-12 11:19:50'),
(392, 67, 314, 6, 'option1', '2019-04-12 11:19:50', '2019-04-12 11:19:50'),
(393, 67, 314, 6, 'option2', '2019-04-12 11:19:50', '2019-04-12 11:19:50'),
(394, 69, 316, 4, 'op1', '2019-04-13 06:24:02', '2019-04-13 06:24:02'),
(395, 69, 316, 4, 'op2', '2019-04-13 06:24:03', '2019-04-13 06:24:03'),
(396, 69, 316, 4, 'op3', '2019-04-13 06:24:03', '2019-04-13 06:24:03'),
(415, 133, 411, 6, 'option1', '2019-04-17 10:30:02', '2019-04-17 10:30:02'),
(416, 133, 411, 6, 'option2', '2019-04-17 10:30:02', '2019-04-17 10:30:02'),
(417, 133, 411, 6, 'option3', '2019-04-17 10:30:02', '2019-04-17 10:30:02'),
(418, 135, 413, 4, 'op2', '2019-04-18 09:51:01', '2019-04-18 09:51:01'),
(419, 135, 413, 4, 'op3', '2019-04-18 09:51:01', '2019-04-18 09:51:01'),
(420, 136, 417, 4, 'op2', '2019-04-18 09:53:11', '2019-04-18 09:53:11'),
(421, 136, 417, 4, 'op3', '2019-04-18 09:53:11', '2019-04-18 09:53:11'),
(433, 137, 473, 4, '2', '2019-04-22 12:13:33', '2019-04-22 12:13:33');

-- --------------------------------------------------------

--
-- Table structure for table `bb_properattributes`
--

CREATE TABLE `bb_properattributes` (
  `id` int(11) NOT NULL,
  `proTypeName` varchar(255) NOT NULL,
  `proTypeStatus` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_properattributes`
--

INSERT INTO `bb_properattributes` (`id`, `proTypeName`, `proTypeStatus`) VALUES
(1, 'Text', 0),
(2, 'Date', 0),
(3, 'Multi-line Text', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_propersoptions`
--

CREATE TABLE `bb_propersoptions` (
  `id` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `proOptionVal` int(11) DEFAULT NULL,
  `proLabel` text,
  `options` text NOT NULL,
  `proHoverTxt` text,
  `description` text NOT NULL,
  `min` int(11) NOT NULL,
  `max` int(11) NOT NULL,
  `proOptVal` varchar(225) DEFAULT NULL,
  `proUserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_propersoptions`
--

INSERT INTO `bb_propersoptions` (`id`, `proId`, `proOptionVal`, `proLabel`, `options`, `proHoverTxt`, `description`, `min`, `max`, `proOptVal`, `proUserId`) VALUES
(1, 10, 4, 'lone', '', 'lhover', '', 0, 0, 'vertical', 1),
(2, 11, 4, 'lone', '', 'lhover', '', 0, 0, 'vertical', 1),
(3, 12, 4, 'lone', '', 'hone', '', 0, 0, 'vertical', 1),
(4, 13, 4, 'r1', '', 'rhover', '', 0, 0, 'vertical', 1),
(20, 16, 5, 'l1', '', 'h1', '', 0, 0, '', 1),
(21, 16, 4, 'l2', '', 'h2', '', 0, 0, '', 1),
(22, 17, 5, 'cl1', '', 'ho1', '', 0, 0, 'vertical', 1),
(23, 17, 4, 'rl1', '', 'rh1', '', 0, 0, 'vertical', 1),
(24, 18, 4, 'l0', '', 'ho', '', 0, 0, 'vertical', 1),
(25, 18, 5, 'l2', '', 'h2', '', 0, 0, 'vertical', 1),
(52, 19, 4, 'l1', '', 'h1', '', 0, 0, 'vertical', 1),
(53, 19, 5, 'l2', '', 'h2', '', 0, 0, 'horizontal', 1),
(54, 19, 5, 'l3', '', 'h3', '', 0, 0, '', 1),
(63, 15, 6, 'l1', '', 'h2', '', 0, 0, NULL, 1),
(64, 15, 6, 'l2', '', 'h1', '', 0, 0, NULL, 1),
(90, 21, 5, 'fd', '', 'fd', '', 0, 0, 'vertical', 1),
(91, 21, 2, 'fdfdfd', '', 'fdfdfdf', '', 0, 0, 'MM-DD-YY', 1),
(92, 21, 3, 'ds', '', 'ds', '', 0, 0, 'All Caps', 1),
(93, 20, 4, 'l1', '', 'h1', '', 0, 0, '', 1),
(94, 20, 6, 'label1', '', 'hover2', '', 0, 0, '', 1),
(95, 22, 4, 'lone', '', 'hone', '', 0, 0, 'vertical', 1),
(96, 22, 4, 'ltwo', '', 'dd', '', 0, 0, 'horizontal', 1),
(97, 26, 4, 'lon', '', 'hov1', 'des1', 12, 33, 'vertical', 1),
(98, 26, 4, 'l2', '', '', 'des2', 12, 22, 'vertical', 1),
(99, 27, 4, 'l1', '', 'h1', 'des1', 12, 33, 'vertical', 1),
(110, 28, 4, 'l1', '', 'h1', 'des1', 3, 5, NULL, 1),
(111, 28, 4, 'l2', '', 'htwo', 'des2', 2, 5, NULL, 1),
(168, 34, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(169, 34, 4, 'lsel1', '', 'hdfes1', '', 0, 0, NULL, 1),
(170, 35, 2, 'date_label', '', 'htext', '', 0, 0, NULL, 1),
(171, 35, 6, 'ldropdown', '', 'lhover', '', 0, 0, NULL, 1),
(172, 36, 1, 'ltext', '', 'htext', '', 0, 0, NULL, 1),
(173, 36, 6, 'ldrop', '', 'lhdtedrop', '', 0, 0, NULL, 1),
(174, 41, 1, 'ltext', '', 'htext', '', 0, 0, NULL, 1),
(175, 41, 2, 'ldate', '', 'hdate', '', 0, 0, NULL, 1),
(176, 41, 6, 'ldrop', '', 'lhover', '', 0, 0, NULL, 1),
(177, 42, 1, 'ltext', '', 'htext', '', 0, 0, NULL, 1),
(178, 42, 2, 'ldate', '', 'hdate', '', 0, 0, NULL, 1),
(179, 42, 6, 'ldrop', '', 'lhover', '', 0, 0, NULL, 1),
(180, 42, 4, 'l4', '', 'htext', 'dess', 3, 4, NULL, 1),
(181, 43, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(182, 43, 2, 'ldate', '', 'htext', '', 0, 0, 'MM-DD-YYYY', 1),
(183, 43, 4, 'select_drop', '', 'hover', 'drop', 2, 5, 'horizontal', 1),
(184, 44, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(185, 44, 2, 'ldate', '', 'htext', '', 0, 0, 'MM-DD-YYYY', 1),
(186, 44, 4, 'select_drop', '', 'hover', 'drop', 2, 5, 'horizontal', 1),
(187, 45, 3, 'lmulti', '', 'hmulti', '', 0, 0, 'All Caps', 1),
(188, 45, 2, 'ldate', '', 'hdate', '', 0, 0, 'MM-DD-YY', 1),
(189, 45, 4, 'lselectiom', '', 'hdesc', 'edesc', 3, 5, 'horizontal', 1),
(190, 45, 6, 'lde', '', 'hdrop', '', 0, 0, NULL, 1),
(191, 46, 3, 'lmulti', '', 'hmulti', '', 0, 0, 'All Caps', 1),
(192, 46, 2, 'ldate', '', 'hdate', '', 0, 0, 'MM-DD-YY', 1),
(193, 46, 4, 'lselectiom', '', 'hdesc', 'edesc', 3, 5, 'horizontal', 1),
(194, 46, 6, 'lde', '', 'hdrop', '', 0, 0, NULL, 1),
(233, 47, 3, 'lzeroup', '', 'hzero', '', 0, 0, 'Title Case', 1),
(234, 47, 4, 'g', '', 'ggggggg', 'fgfgfgfgfgfgfgfgfgfg', 6, 7, 'horizontal', 1),
(235, 47, 6, 'leb1', '', 'hovertest', '', 0, 0, NULL, 1),
(242, 48, 3, 'lone', '', 'hmultiline', '', 0, 0, 'All Caps', 1),
(243, 48, 4, 'l1', '', 'hover', 'description', 12, 22, 'horizontal', 1),
(250, 50, 1, 'dfsdfedfg', '', 'dfdsfsdfsd', '', 0, 0, 'All Lower Case', 1),
(251, 50, 3, 'dsfsdfd', '', 'fsdfdsfsd', '', 0, 0, 'All Caps', 1),
(252, 50, 6, 'fdfdsf', '', 'hg', '', 0, 0, NULL, 1),
(253, 51, 4, 'lsel', '', 'test', 'des', 1, 2, 'horizontal', 1),
(254, 51, 2, 'ldate', '', 'htesd', '', 0, 0, 'MM-DD-YY', 1),
(255, 52, 4, 'lsel', '', 'test', 'des', 1, 2, 'horizontal', 1),
(256, 52, 2, 'ldate', '', 'htesd', '', 0, 0, 'MM-DD-YY', 1),
(257, 53, 4, 'lsel', '', 'test', 'des', 1, 2, 'horizontal', 1),
(258, 53, 2, 'ldate', '', 'htesd', '', 0, 0, 'MM-DD-YY', 1),
(259, 54, 4, 'lsel', '', 'test', 'des', 1, 2, 'horizontal', 1),
(260, 54, 2, 'ldate', '', 'htesd', '', 0, 0, 'MM-DD-YY', 1),
(261, 55, 4, 'l1', '', 'htext', 'des1', 1, 2, 'vertical', 1),
(262, 55, 3, 'multilabel', '', 'htext', '', 0, 0, 'All Caps', 1),
(263, 56, 4, 'lsel', '', 'htext', 'teeestdecription', 1, 2, 'vertical', 1),
(264, 56, 3, 'lmulti', '', 'htext', '', 0, 0, 'All Caps', 1),
(265, 57, 4, 'lsel', '', 'htext', 'teeestdecription', 1, 2, 'vertical', 1),
(266, 57, 3, 'lmulti', '', 'htext', '', 0, 0, 'All Caps', 1),
(267, 58, 4, 'lsel', '', 'htext', 'teeestdecription', 1, 2, 'vertical', 1),
(268, 58, 3, 'lmulti', '', 'htext', '', 0, 0, 'All Caps', 1),
(269, 59, 4, 'lsel', '', 'htext', 'teeestdecription', 1, 2, 'vertical', 1),
(270, 59, 3, 'lmulti', '', 'htext', '', 0, 0, 'All Caps', 1),
(271, 60, 4, 'lsel', '', 'htext', 'teeestdecription', 1, 2, 'vertical', 1),
(272, 60, 3, 'lmulti', '', 'htext', '', 0, 0, 'All Caps', 1),
(273, 61, 4, 'lsel', '', 'htext', 'teeestdecription', 1, 2, 'vertical', 1),
(274, 61, 3, 'lmulti', '', 'htext', '', 0, 0, 'All Caps', 1),
(275, 62, 4, 'lsel', '', 'htext', 'teeestdecription', 1, 2, 'vertical', 1),
(276, 62, 3, 'lmulti', '', 'htext', '', 0, 0, 'All Caps', 1),
(277, 63, 4, 'lsel', '', 'htext', 'teeestdecription', 1, 2, 'vertical', 1),
(278, 63, 3, 'lmulti', '', 'htext', '', 0, 0, 'All Caps', 1),
(279, 64, 4, 'lsel', '', 'htext', 'teeestdecription', 1, 2, 'vertical', 1),
(280, 64, 3, 'lmulti', '', 'htext', '', 0, 0, 'All Caps', 1),
(281, 65, 4, 'lsel', '', '', 'dessel', 1, 2, 'vertical', 1),
(282, 65, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(290, 66, 4, 'lsel', '', '', 'ldes', 1, 2, 'vertical', 1),
(312, 67, 4, 'l1', '', 'htext', 'des1', 12, 22, 'vertical', 1),
(313, 67, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(314, 67, 6, 'ldrop', '', 'hhhh', '', 0, 0, NULL, 1),
(315, 68, 3, 'ltext', '', 'testhover', '', 0, 0, 'All Caps', 1),
(316, 69, 4, 'lone', '', 'hovertext', 'ldescription', 1, 2, 'vertical', 1),
(317, 70, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(318, 71, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(319, 72, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(320, 73, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(321, 74, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(322, 75, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(323, 76, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(324, 77, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(325, 78, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(326, 79, 3, 'ds', '', 'ds', '', 0, 0, 'All Caps', 1),
(327, 80, 1, 'd', '', 'f', '', 0, 0, 'All Caps', 1),
(328, 81, 1, 'd', '', 'f', '', 0, 0, 'All Caps', 1),
(329, 82, 1, 'f', '', 'f', '', 0, 0, 'All Caps', 1),
(330, 83, 1, 'ds', '', 'ds', '', 0, 0, 'All Caps', 1),
(331, 84, 1, 'ds', '', 'ds', '', 0, 0, 'All Caps', 1),
(332, 85, 1, 'ds', '', 'ds', '', 0, 0, 'All Caps', 1),
(333, 86, 1, 'ds', '', 'ds', '', 0, 0, 'All Caps', 1),
(334, 87, 1, 'ds', '', 'ds', '', 0, 0, 'All Caps', 1),
(335, 88, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(336, 89, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(337, 90, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(338, 91, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(339, 92, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(340, 93, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(341, 94, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(342, 95, 1, 'ff', '', 'ff', '', 0, 0, 'All Caps', 1),
(343, 96, 1, 'ff', '', 'ff', '', 0, 0, 'All Caps', 1),
(344, 97, 1, 'ff', '', 'ff', '', 0, 0, 'All Caps', 1),
(345, 98, 2, 'ds', '', 'ds', '', 0, 0, 'MM-DD-YY', 1),
(346, 99, 2, 'ds', '', 'ds', '', 0, 0, 'MM-DD-YY', 1),
(347, 100, 1, 'dd', '', 'dd', '', 0, 0, 'All Caps', 1),
(348, 101, 1, 'ds', '', 'ds', '', 0, 0, 'All Caps', 1),
(349, 102, 1, 'ds', '', 'ds', '', 0, 0, 'All Caps', 1),
(350, 103, 1, 'ds', '', 'ds', '', 0, 0, 'All Caps', 1),
(351, 104, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(352, 105, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(353, 106, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(354, 107, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(355, 108, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(356, 109, 1, 'ds', '', 'ds', '', 0, 0, 'All Caps', 1),
(357, 110, 1, 'ds', '', 'ds', '', 0, 0, 'All Caps', 1),
(358, 111, 1, 'ds', '', 'ds', '', 0, 0, 'All Caps', 1),
(359, 112, 3, 'testing', '', 'd', '', 0, 0, 'All Caps', 1),
(360, 113, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(361, 114, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 99),
(362, 115, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 99),
(363, 116, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 99),
(364, 117, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 99),
(365, 118, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 99),
(366, 119, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 99),
(367, 120, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 99),
(368, 121, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 99),
(369, 122, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 99),
(370, 123, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 99),
(371, 124, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 99),
(372, 125, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 99),
(373, 126, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 99),
(374, 127, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 99),
(375, 128, 1, 'dd', '', 'dd', '', 0, 0, 'All Caps', 1),
(376, 129, 1, 'f', '', 'htext', '', 0, 0, 'All Caps', 1),
(377, 130, 1, 'f', '', 'htext', '', 0, 0, 'All Caps', 1),
(378, 131, 1, 'd', '', 'd', '', 0, 0, 'All Caps', 1),
(397, 132, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(410, 133, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(411, 133, 6, 'lselection', '', 'hselection', '', 0, 0, NULL, 1),
(412, 135, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(413, 135, 4, 'lselected', '', 'htext', 'ldes', 2, 3, 'vertical', 1),
(414, 135, 1, 'ltext', '', 'htext', '', 0, 0, NULL, 1),
(415, 135, 4, 'lselected', '', 'htext', '', 0, 0, NULL, 1),
(416, 136, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(417, 136, 4, 'lselected', '', 'htext', 'ldes', 2, 3, 'vertical', 1),
(418, 136, 1, 'ltext', '', 'htext', '', 0, 0, NULL, 1),
(419, 136, 4, 'lselected', '', 'htext', '', 0, 0, NULL, 1),
(420, 136, 1, 'ltext', '', 'htext', '', 0, 0, NULL, 1),
(421, 136, 4, 'lselected', '', 'htext', '', 0, 0, NULL, 1),
(430, 139, 1, 'd', '', 'd', '', 0, 0, 'All Caps', 1),
(447, 140, 2, 'd', '', 'd', '', 0, 0, 'MM-DD-YY', 1),
(452, 141, 3, 'f', '', '', '', 0, 0, 'All Caps', 1),
(472, 137, 1, 'ltext', '', 'htext', '', 0, 0, 'All Caps', 1),
(473, 137, 4, 'lsel', '', 'htext', 'ldesc', 2, 3, 'vertical', 1),
(474, 137, 0, '', '', '', '', 0, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bb_prorestrictions`
--

CREATE TABLE `bb_prorestrictions` (
  `id` int(11) NOT NULL,
  `proID` int(11) NOT NULL,
  `rsOptionVal` int(11) NOT NULL COMMENT '2=>''studio'',1=>''coupon''',
  `rsCouponType` varchar(255) NOT NULL,
  `couponAmount` varchar(255) NOT NULL,
  `rsQVal` varchar(255) NOT NULL,
  `userId` varchar(155) NOT NULL COMMENT '''userid=>''studio location id'''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_prorestrictions`
--

INSERT INTO `bb_prorestrictions` (`id`, `proID`, `rsOptionVal`, `rsCouponType`, `couponAmount`, `rsQVal`, `userId`) VALUES
(1, 64, 2, '', '', '', '360,104'),
(2, 64, 1, 'amount', '120', '', '1'),
(3, 65, 2, '', '', '', '360,104,104'),
(4, 65, 1, 'amount', '12', '', '1'),
(12, 66, 2, '', '', '', '360'),
(13, 66, 1, 'percent', '12', '', '1'),
(28, 67, 1, 'amount', '22', '', '1'),
(29, 67, 2, '', '', '', '360,104'),
(30, 68, 2, '', '', '', '360,104'),
(31, 68, 1, 'percent', '23', '', '1'),
(32, 69, 1, 'percent', '12', '', '1'),
(33, 69, 2, '', '', '', '360,104'),
(34, 70, 2, '', '', '', '104,360'),
(35, 71, 2, '', '', '', '104,360'),
(36, 72, 2, '', '', '', '104,360'),
(37, 73, 2, '', '', '', '104,360'),
(38, 74, 2, '', '', '', '104,360'),
(39, 75, 2, '', '', '', '104,360'),
(40, 76, 1, 'percent', '3', '', '1'),
(41, 77, 1, 'percent', '3', '', '1'),
(42, 78, 1, 'percent', '3', '', '1'),
(43, 79, 2, '', '', '', '360'),
(44, 80, 1, 'percent', '3', '', '1'),
(45, 81, 1, 'percent', '3', '', '1'),
(46, 82, 1, 'percent', '4', '', '1'),
(47, 83, 1, 'percent', '33', '', '1'),
(48, 84, 1, 'percent', '33', '', '1'),
(49, 85, 1, 'percent', '33', '', '1'),
(50, 86, 1, 'percent', '33', '', '1'),
(51, 87, 1, 'percent', '33', '', '1'),
(52, 88, 1, 'percent', '3', '', '1'),
(53, 89, 1, 'percent', '3', '', '1'),
(54, 90, 1, 'percent', '555', '', '1'),
(55, 91, 1, 'percent', '555', '', '1'),
(56, 92, 1, 'percent', '555', '', '1'),
(57, 93, 1, 'percent', '555', '', '1'),
(58, 94, 1, 'percent', '555', '', '1'),
(59, 95, 2, '', '', '', '360,104'),
(60, 96, 2, '', '', '', '360,104'),
(61, 97, 2, '', '', '', '360,104'),
(62, 98, 1, 'percent', '2', '', '1'),
(63, 99, 1, 'percent', '2', '', '1'),
(64, 100, 1, 'percent', '2', '', '1'),
(65, 101, 1, 'percent', '3', '', '1'),
(66, 102, 1, 'percent', '3', '', '1'),
(67, 103, 1, 'percent', '3', '', '1'),
(68, 104, 2, '', '', '', ''),
(69, 105, 2, '', '', '', ''),
(70, 106, 2, '', '', '', ''),
(71, 107, 2, '', '', '', ''),
(72, 108, 2, '', '', '', ''),
(73, 109, 1, 'percent', '3', '', '1'),
(74, 110, 1, 'percent', '3', '', '1'),
(75, 111, 1, 'percent', '3', '', '1'),
(76, 113, 2, '', '', '', ''),
(77, 114, 2, '', '', '', ''),
(78, 115, 2, '', '', '', ''),
(79, 116, 2, '', '', '', ''),
(80, 117, 2, '', '', '', ''),
(81, 118, 2, '', '', '', ''),
(82, 119, 2, '', '', '', ''),
(83, 120, 2, '', '', '', ''),
(84, 121, 2, '', '', '', ''),
(85, 122, 2, '', '', '', ''),
(86, 123, 2, '', '', '', ''),
(87, 124, 2, '', '', '', ''),
(88, 125, 2, '', '', '', ''),
(89, 126, 2, '', '', '', ''),
(90, 127, 2, '', '', '', ''),
(91, 128, 1, 'percent', '2', '', '1'),
(92, 131, 2, '', '', '', '360'),
(111, 132, 1, 'percent', '5', '', '1'),
(124, 133, 1, 'percent', '12', '', '1'),
(125, 133, 2, '', '', '', '360,104'),
(126, 135, 2, '', '', '', '104,360'),
(127, 135, 1, 'percent', '12', '', '1'),
(128, 135, 2, '', '', '', '104,360'),
(129, 135, 1, 'percent', '12', '', '1'),
(130, 136, 2, '', '', '', '104,360'),
(131, 136, 1, 'percent', '12', '', '1'),
(132, 136, 2, '', '', '', '104,360'),
(133, 136, 1, 'percent', '12', '', '1'),
(134, 136, 2, '', '', '', '104,360'),
(135, 136, 1, 'percent', '12', '', '1'),
(200, 138, 1, 'percent', '2', '', ''),
(206, 139, 1, 'percent', '2', '', ''),
(207, 139, 2, '', '', '', ''),
(238, 140, 1, '1', '22', '', ''),
(239, 140, 2, '', '', '', '360,104'),
(246, 141, 1, '2', '12', '', ''),
(257, 137, 2, '', '', '', '360,104');

-- --------------------------------------------------------

--
-- Table structure for table `bb_prorestroptions`
--

CREATE TABLE `bb_prorestroptions` (
  `id` int(11) NOT NULL,
  `proTypeName` varchar(255) NOT NULL,
  `proTypeOptVal` text NOT NULL,
  `proTypeStatus` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_prorestroptions`
--

INSERT INTO `bb_prorestroptions` (`id`, `proTypeName`, `proTypeOptVal`, `proTypeStatus`) VALUES
(1, 'Promo Code', '1-Code,2-Discount,3-Loyalty Value', 0),
(2, 'Loyalty Value', '', 0),
(3, 'Studio', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_prospcoptions`
--

CREATE TABLE `bb_prospcoptions` (
  `id` int(11) NOT NULL,
  `opName` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_prospcoptions`
--

INSERT INTO `bb_prospcoptions` (`id`, `opName`, `status`) VALUES
(1, 'Board', 0),
(2, 'Support', 0),
(3, 'Screw eyes', 0),
(4, 'Wire', 0),
(5, 'Hanger', 0),
(6, 'Nails', 0),
(7, 'Other', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_prospcsizemeasure`
--

CREATE TABLE `bb_prospcsizemeasure` (
  `id` int(11) NOT NULL,
  `id_type` varchar(11) NOT NULL,
  `sizename` varchar(225) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `dType` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_prospcsizemeasure`
--

INSERT INTO `bb_prospcsizemeasure` (`id`, `id_type`, `sizename`, `status`, `dType`) VALUES
(1, '1', '4x5.25', 0, 7),
(2, '1', '4x12', 0, 9),
(3, '1', '4x17', 0, 11),
(4, '1', '4x24', 0, 12),
(5, '1', '4x48', 0, 13),
(6, '1', '6x4.75', 0, 14),
(7, '1', '6x6.75', 0, 15),
(8, '1', '6x16', 0, 16),
(9, '1', '6x24', 0, 17),
(10, '1', '6x32', 0, 18),
(11, '1', '6x48', 0, 19),
(12, '1', '8x16', 0, 20),
(13, '1', '8x24', 0, 21),
(14, '1', '8x32', 0, 22),
(15, '1', '8x48', 0, 23),
(16, '2', '7', 0, 37),
(17, '2', '8', 0, 38),
(18, '2', '12', 0, 39),
(19, '2', '16', 0, 40),
(20, '2', '19.2', 0, 41),
(21, '2', '20', 0, 42),
(22, '2', '24', 0, 43),
(23, '1', '10x24', 0, 24),
(24, '1', '10x48', 0, 26),
(25, '1', '10x26', 0, 25),
(26, '1', '10x60', 0, 27),
(27, '1', '12x12', 0, 28),
(28, '1', '12x12 ACX', 0, 29),
(29, '1', '12x16', 0, 30),
(30, '1', '12x24', 0, 31),
(31, '1', '12x32', 0, 32),
(32, '1', '12x48', 0, 33),
(33, '1', '2x12', 0, 1),
(34, '1', '2x14', 0, 2),
(35, '1', '2x18', 0, 3),
(36, '1', '2x26', 0, 4),
(37, '1', '2x34', 0, 5),
(38, '1', '2x50', 0, 6),
(39, '1', '18 Round', 0, 34),
(40, '1', '24 Round', 0, 35),
(41, '1', 'Prebuilt', 0, 36),
(42, '1', '4x14', 0, 10),
(43, '1', '4x8', 0, 8),
(44, '7', 'Cork 7\"', 0, 44);

-- --------------------------------------------------------

--
-- Table structure for table `bb_prospecifications`
--

CREATE TABLE `bb_prospecifications` (
  `id` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `specOptionVal` int(11) DEFAULT NULL,
  `specType` varchar(255) DEFAULT NULL,
  `specTypeVal` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_prospecifications`
--

INSERT INTO `bb_prospecifications` (`id`, `proId`, `userId`, `specOptionVal`, `specType`, `specTypeVal`) VALUES
(21, 16, 1, 7, '44', '2'),
(22, 17, 1, 6, '', '2'),
(23, 18, 1, 7, '44', '12'),
(28, 19, 1, 6, '', '12'),
(33, 15, 1, 7, '44', '4'),
(46, 21, 1, 7, '44', '3'),
(47, 20, 1, 7, '44', '12'),
(48, 22, 1, 7, '44', '12'),
(49, 26, 1, 7, '44', '12'),
(50, 26, 1, 6, '', '22'),
(51, 27, 1, 7, '44', '12'),
(57, 28, 1, 6, '', '2'),
(58, 32, 1, 6, '', '2'),
(60, 33, 1, 5, '', '2'),
(87, 34, 1, 7, '44', '3'),
(88, 35, 1, 7, '44', '12'),
(89, 36, 1, 7, '44', '66'),
(90, 37, 1, 7, '44', '12'),
(91, 38, 1, 7, '44', '12'),
(92, 39, 1, 7, '44', '12'),
(93, 42, 1, 7, '44', '12'),
(94, 43, 1, 6, '', '12'),
(95, 44, 1, 6, '', '12'),
(96, 45, 1, 7, '44', '12'),
(97, 46, 1, 7, '44', '12'),
(110, 47, 1, 5, '', '2'),
(114, 48, 1, 7, '44', '2'),
(117, 50, 1, 6, '', '6'),
(118, 50, 1, 3, '', '8'),
(119, 49, 1, 1, '33', '8'),
(120, 51, 1, 1, '33', '2'),
(121, 52, 1, 1, '33', '2'),
(122, 53, 1, 1, '33', '2'),
(123, 54, 1, 1, '33', '2'),
(124, 55, 1, 4, '', '3'),
(125, 56, 1, 6, '', '12'),
(126, 57, 1, 6, '', '12'),
(127, 58, 1, 6, '', '12'),
(128, 59, 1, 6, '', '12'),
(129, 60, 1, 6, '', '12'),
(130, 61, 1, 6, '', '12'),
(131, 62, 1, 6, '', '12'),
(132, 63, 1, 6, '', '12'),
(133, 64, 1, 6, '', '12'),
(134, 65, 1, 7, '44', '2'),
(142, 66, 1, 6, '', '4'),
(150, 67, 1, 6, '', '12'),
(151, 68, 1, 5, '', '12'),
(152, 69, 1, 1, '33', '2'),
(153, 70, 1, 5, '', '12'),
(154, 71, 1, 5, '', '12'),
(155, 72, 1, 5, '', '12'),
(156, 73, 1, 5, '', '12'),
(157, 74, 1, 5, '', '12'),
(158, 75, 1, 5, '', '12'),
(159, 76, 1, 6, '', '2'),
(160, 77, 1, 6, '', '2'),
(161, 78, 1, 6, '', '2'),
(162, 79, 1, 6, '', '2'),
(163, 80, 1, 5, '', '33'),
(164, 81, 1, 5, '', '33'),
(165, 82, 1, 6, '', '12'),
(166, 83, 1, 6, '', '3'),
(167, 84, 1, 6, '', '3'),
(168, 85, 1, 6, '', '3'),
(169, 86, 1, 6, '', '3'),
(170, 87, 1, 6, '', '3'),
(171, 88, 1, 6, '', '2'),
(172, 89, 1, 6, '', '2'),
(173, 90, 1, 5, '', '12'),
(174, 91, 1, 5, '', '12'),
(175, 92, 1, 5, '', '12'),
(176, 93, 1, 5, '', '12'),
(177, 94, 1, 5, '', '12'),
(178, 95, 1, 6, '', '2'),
(179, 96, 1, 6, '', '2'),
(180, 97, 1, 6, '', '2'),
(181, 98, 1, 6, '', '2'),
(182, 99, 1, 6, '', '2'),
(183, 100, 1, 6, '', '2'),
(184, 101, 1, 6, '', '2'),
(185, 102, 1, 6, '', '2'),
(186, 103, 1, 6, '', '2'),
(187, 104, 1, 5, '33', '12'),
(188, 105, 1, 5, '33', '12'),
(189, 106, 1, 5, '33', '12'),
(190, 107, 1, 5, '33', '12'),
(191, 108, 1, 5, '33', '12'),
(192, 109, 1, 6, '', '2'),
(193, 110, 1, 6, '', '2'),
(194, 111, 1, 6, '', '2'),
(195, 112, 1, 2, '16', '1'),
(196, 113, 1, 5, '33', '12'),
(197, 114, 99, 5, '33', '12'),
(198, 115, 99, 5, '33', '12'),
(199, 116, 99, 5, '33', '12'),
(200, 117, 99, 5, '33', '12'),
(201, 118, 99, 5, '33', '12'),
(202, 119, 99, 5, '33', '12'),
(203, 120, 99, 5, '33', '12'),
(204, 121, 99, 5, '33', '12'),
(205, 122, 99, 5, '33', '12'),
(206, 123, 99, 5, '33', '12'),
(207, 124, 99, 5, '33', '12'),
(208, 125, 99, 5, '33', '12'),
(209, 126, 99, 5, '33', '12'),
(210, 127, 99, 5, '33', '12'),
(211, 128, 1, 4, '', '3'),
(212, 129, 1, 6, '', '3'),
(213, 130, 1, 6, '', '3'),
(214, 131, 1, 6, '', '3'),
(233, 132, 1, 6, '', '5'),
(240, 133, 1, 3, '', '12'),
(241, 134, 1, 6, '', '4'),
(242, 135, 1, 6, '', '12'),
(243, 135, 1, 6, '', '12'),
(244, 136, 1, 6, '', '12'),
(245, 136, 1, 6, '', '12'),
(246, 136, 1, 6, '', '12'),
(279, 138, 1, 5, '', '12'),
(282, 139, 1, 5, '', '2'),
(299, 140, 1, 5, '', '5'),
(304, 141, 1, 6, '', '1'),
(313, 137, 1, 6, '', '2');

-- --------------------------------------------------------

--
-- Table structure for table `bb_protype`
--

CREATE TABLE `bb_protype` (
  `id` int(11) NOT NULL,
  `proTypeName` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `dType` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_protype`
--

INSERT INTO `bb_protype` (`id`, `proTypeName`, `status`, `dType`) VALUES
(1, 'Wood Sign', 0, 1),
(2, 'Wood Frame', 0, 2),
(3, 'Wood Box', 0, 3),
(4, 'Wood Tray', 0, 4),
(5, 'Double Sided', 0, 6),
(6, 'Clock', 0, 7),
(7, 'Special', 0, 8),
(8, 'Wood Shelf', 0, 5),
(9, 'Doormat', 0, 9),
(10, 'Team Building', 0, 10),
(11, 'Time Crunch', 0, 11),
(12, 'Tween', 0, 12),
(13, 'Glass and Wood Trivet', 0, 13),
(14, 'Glass and Wood Box', 0, 14);

-- --------------------------------------------------------

--
-- Table structure for table `bb_states`
--

CREATE TABLE `bb_states` (
  `state_id` int(11) NOT NULL,
  `abbr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bb_states`
--

INSERT INTO `bb_states` (`state_id`, `abbr`, `state_name`, `country_id`, `status`) VALUES
(1521, 'AL', 'Alabama', 224, 1),
(1522, 'AK', 'Alaska', 224, 1),
(1523, 'AZ', 'Arizona', 224, 1),
(1524, 'AR', 'Arkansas', 224, 1),
(1525, 'CA', 'California', 224, 1),
(1526, 'CO', 'Colorado', 224, 1),
(1527, 'CT', 'Connecticut', 224, 1),
(1528, 'DC', 'District of Columbia', 224, 1),
(1529, 'FL', 'Florida', 224, 1),
(1530, 'GA', 'Georgia', 224, 1),
(1531, 'HI', 'Hawaii', 224, 1),
(1532, 'ID', 'Idaho', 224, 1),
(1533, 'IL', 'Illinois', 224, 1),
(1534, 'IN', 'Indiana', 224, 1),
(1535, 'IA', 'Iowa', 224, 1),
(1536, 'KS', 'Kansas', 224, 1),
(1537, 'KY', 'Kentucky', 224, 1),
(1538, 'LA', 'Louisiana', 224, 1),
(1539, 'MD', 'Maryland', 224, 1),
(1540, 'MA', 'Massachusetts', 224, 1),
(1541, 'MI', 'Michigan', 224, 1),
(1542, 'MN', 'Minnesota', 224, 1),
(1543, 'MS', 'Mississippi', 224, 1),
(1544, 'MO', 'Missouri', 224, 1),
(1545, 'MT', 'Montana', 224, 1),
(1546, 'NE', 'Nebraska', 224, 1),
(1547, 'NV', 'Nevada', 224, 1),
(1548, 'NH', 'New Hampshire', 224, 1),
(1549, 'NJ', 'New Jersey', 224, 1),
(1550, 'NM', 'New Mexico', 224, 1),
(1551, 'NY', 'New York', 224, 1),
(1552, 'NC', 'North Carolina', 224, 1),
(1553, 'OH', 'Ohio', 224, 1),
(1554, 'OK', 'Oklahoma', 224, 1),
(1555, 'OR', 'Oregon', 224, 1),
(1556, 'PA', 'Pennsylvania', 224, 1),
(1557, 'RI', 'Rhode Island', 224, 1),
(1558, 'SC', 'South Carolina', 224, 1),
(1559, 'SD', 'South Dakota', 224, 1),
(1560, 'TN', 'Tennessee', 224, 1),
(1561, 'TX', 'Texas', 224, 1),
(1562, 'UT', 'Utah', 224, 1),
(1563, 'VA', 'Virginia', 224, 1),
(1564, 'WA', 'Washington', 224, 1),
(1565, 'WI', 'Wisconsin', 224, 1),
(1566, 'DE', 'Delaware', 224, 1),
(1567, 'ME', 'Maine', 224, 1),
(1568, 'ND', 'North Dakota', 224, 1),
(1569, 'VT', 'Vermont', 224, 1),
(1570, 'WV', 'West Virginia', 224, 1),
(1571, 'WY', 'Wyoming', 224, 1),
(1572, '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bb_studiolocation`
--

CREATE TABLE `bb_studiolocation` (
  `id` int(11) NOT NULL,
  `stNum` int(11) NOT NULL,
  `slug` text NOT NULL,
  `stName` varchar(500) NOT NULL,
  `stEmail` varchar(255) NOT NULL,
  `stStreetAddress` text NOT NULL,
  `stAddress1` varchar(500) NOT NULL,
  `stCity` varchar(255) NOT NULL,
  `stState` varchar(255) NOT NULL,
  `stZip` varchar(255) NOT NULL,
  `stSNouns` tinyint(1) NOT NULL DEFAULT '1',
  `lat` decimal(10,6) NOT NULL,
  `lng` decimal(10,6) NOT NULL,
  `stPhone` varchar(50) NOT NULL,
  `stAboutStudo` text NOT NULL,
  `stOwnerPic` text NOT NULL,
  `stBioDesc` text NOT NULL,
  `stOwner` varchar(500) NOT NULL,
  `stOwnerCInfo` varchar(500) NOT NULL,
  `stLocPolicies` text NOT NULL,
  `stEmailSignBlock` text NOT NULL,
  `stLocDirection` text NOT NULL,
  `stLogo` text NOT NULL,
  `stFbLink` text NOT NULL,
  `stInstaLink` text NOT NULL,
  `stTwtLink` text NOT NULL,
  `stPinLink` text NOT NULL,
  `stStatus` int(11) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `isActive` tinyint(1) NOT NULL DEFAULT '0',
  `ccode` varchar(50) NOT NULL,
  `isusa` varchar(50) NOT NULL,
  `stAuthInfo` text NOT NULL,
  `stimeZone` varchar(255) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` datetime NOT NULL,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` datetime NOT NULL,
  `stMetaTitle` text NOT NULL,
  `stMetaDesc` text NOT NULL,
  `isbtn` tinyint(1) NOT NULL DEFAULT '0',
  `ismigrated` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_studiolocation`
--

INSERT INTO `bb_studiolocation` (`id`, `stNum`, `slug`, `stName`, `stEmail`, `stStreetAddress`, `stAddress1`, `stCity`, `stState`, `stZip`, `stSNouns`, `lat`, `lng`, `stPhone`, `stAboutStudo`, `stOwnerPic`, `stBioDesc`, `stOwner`, `stOwnerCInfo`, `stLocPolicies`, `stEmailSignBlock`, `stLocDirection`, `stLogo`, `stFbLink`, `stInstaLink`, `stTwtLink`, `stPinLink`, `stStatus`, `isDeleted`, `isActive`, `ccode`, `isusa`, `stAuthInfo`, `stimeZone`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`, `stMetaTitle`, `stMetaDesc`, `isbtn`, `ismigrated`) VALUES
(104, 999, 'test-site', 'North Yonkers', 'admin@boardandbrush.com', '#11705 Jones Bridge Rd', '', 'Johns Creek', 'GA', '78906', 1, '34.065463', '-84.210615', '(000) 000-0000', '&#60;p&#62;Park Ridge is a picturesque suburb with small town charm located northwest of downtown Chicago. The Uptown shopping area of Park Ridge,where Board &#38;amp; Brush is located, also includes the Metra station, Public Library, historic Pickwick Theater and several charming parks where livemusical concerts and special events are held in the summer.The Park Ridge Studio is officially University of Wisconsin - Badger licensed, available for an additional cost of $10.&#60;/p&#62;', '2915de63ace1c5d5481581692e21f816.jpg', '&#60;h2&#62;Alissa &#38;amp; Ian Robinson&#60;/h2&#62;&#10;&#10;&#60;p&#62;When they first heard about Board &#38;amp; Brush Alissa and Ian worked downtown Chicago at an advertising agency and a commercial real estate company. Alissa fell in love with Board &#38;amp; Brush when she attended her first workshop. Her contagious excitement about the prospect of opening their own studio quickly latched on to Ian as well. In their pursuit of opening a studio, they were drawn in to the workings of the corporate office and found that their experience and ambition could help the Board &#38;amp; Brush corporate office to grow the brand across the country. While maintaining their corporate roles, Alissa and Ian are excited to have their own little piece of the dream in Park Ridge. They decided to open a Board &#38;amp; Brush because they wanted to give everyone a happy place to go. At B&#38;amp;b they can get away from daily life, celebrate with loved ones, or just experience something new while creating a personalized piece for their home that they will be proud to say they made. Alissa and Ian love to travel and have a bulldog named Ernie!&#60;/p&#62;&#10;&#10;&#60;h2&#62;CCatherine Hochfellner&#60;/h2&#62;&#10;&#10;&#60;p&#62;Catherine Hochfellner is the studio manager for the Park Ridge location. Cat previously worked as a Marketing Project Manager for an alumni travel operator in Chicago. She started her career there after graduating from Concordia University Chicago with a B.A. in Communications and Graphic Arts. She enjoys photography, music and traveling to a new destination once a year. She and her husband Karl were married in October of 2016 in Winfield, IL with a Blackhawk themed reception and have a sweet dog named Lord Stanley, after the Stanley Cup. She spends her time watching HGTV, movie nights with friends and spending time with Karl and Stanley. She can&#38;rsquo;t wait to meet everyone in the studio!&#60;/p&#62;&#10;&#10;&#60;p&#62;Park Ridge is a picturesque suburb with small town charm located northwest of downtown Chicago. The Uptown shopping area of Park Ridge, where Board &#38;amp; Brush is located, also includes the Metra station, Public Library, historic Pickwick Theater and several charming parks where live musical concerts and special events are held in the summer.&#60;/p&#62;&#10;&#10;&#60;p class=&#34;polctxt&#34;&#62;The Park Ridge Studio is officially &#60;strong&#62;University of Wisconsin - Badger&#60;/strong&#62; licensed, available for an additional cost of $10.&#60;/p&#62;', 'Alissa and Ian Robinson', '262-385-5443', 'Board &#38; Brush Park Ridge is a licensed establishment, beer and wine are available for purchase in the studio&#13;&#10;&#60;strong&#62;Please note,BYOB is not permitted.&#60;/strong&#62;', '', 'Board &#38; Brush Park Ridge is located by the corner of N Northwest Highway and Meacham Avenue across the street from Trader Joe’s and Scottrade.', '104-a449a955ec5fb747b8c991230fc1b287.png', 'https://www.facebook.com/fb', 'https://www.facebook.com/fb', 'https://www.facebook.com/fb', 'https://www.facebook.com/fb', 4, 0, 0, 'USA', '1', '7wd4P7tJzzK/246sW8fYZCD46yzH', 'America/Chicago', 1, '2019-01-16 10:55:13', 130, '2019-03-14 11:28:58', 'Test Website Meta Update', 'Test Website Meta Description', 1, 0),
(360, 1, 'test-pankaj', 'testing studio pankaj', 'dev@effectualtech.com', 'san', 'impirt', 'chandigarh', 'CA', '12223', 1, '30.733315', '76.779418', '(333) 333-3333', '&#60;p&#62;&#60;em&#62;dsddddddddddddddddddddddd&#60;/em&#62;&#60;/p&#62;', '78c010a26c6f194856479bfaf6e1751c.jpg', '&#60;p&#62;gfgfgfgfgfgf&#60;/p&#62;', 'test', 'dsdsds', 'ds', '', 'dsdsds', '360-0d0bee9c5ca47a7d744de58928d21573.jpg', 'http://facebook.com', 'http://fnsta.com', 'http://twitter.com', 'http://pintrest.com', 4, 0, 0, 'India', '0', '3v7L7Rb72ud/4v7Ft55T3rD7Mc6T', 'America/Chicago', 1, '2019-03-23 00:32:38', 99, '2019-03-25 23:57:21', 'Test meta title', 'test meta descriptions', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_studiostatus`
--

CREATE TABLE `bb_studiostatus` (
  `id` int(11) NOT NULL,
  `stName` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_studiostatus`
--

INSERT INTO `bb_studiostatus` (`id`, `stName`, `status`) VALUES
(1, 'Unpublished', 0),
(2, 'Listed', 0),
(3, 'Published', 0),
(4, 'Live', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_taxstudio`
--

CREATE TABLE `bb_taxstudio` (
  `id` int(11) NOT NULL,
  `studioLocId` text NOT NULL,
  `saleTax` varchar(25) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_taxstudio`
--

INSERT INTO `bb_taxstudio` (`id`, `studioLocId`, `saleTax`, `status`) VALUES
(1, '46', '70', 0),
(10, '287', '2.9', 0),
(11, '146', '2.2', 0),
(16, '104', '2.9', 0),
(17, '360', '2', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_timezonestudio`
--

CREATE TABLE `bb_timezonestudio` (
  `id` int(11) NOT NULL,
  `studioLocId` text NOT NULL,
  `stimezone` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bb_transactiondetails`
--

CREATE TABLE `bb_transactiondetails` (
  `id` int(11) NOT NULL,
  `orderType` varchar(11) NOT NULL DEFAULT '1' COMMENT '1=>create order, 2 =>refund order, 3=>void order, 4=>mark as paid, 5=>mark as pending,6=>update',
  `bookingId` int(11) DEFAULT NULL,
  `ptype` tinyint(1) NOT NULL COMMENT '0->Authorize.net,1->gift card,2->offline,3->paypal,4->manual',
  `cardholderName` varchar(255) NOT NULL,
  `caddress` text NOT NULL,
  `ccity` varchar(255) NOT NULL,
  `cstate` varchar(255) NOT NULL,
  `ccountry` varchar(255) NOT NULL,
  `czip` varchar(255) NOT NULL,
  `cphone` varchar(255) NOT NULL,
  `cemail` varchar(255) NOT NULL,
  `bookedAmount` decimal(10,2) DEFAULT NULL,
  `paymentStatus` varchar(225) DEFAULT NULL,
  `bookDate` datetime DEFAULT NULL,
  `refId` varchar(225) DEFAULT NULL,
  `resultCode` varchar(50) DEFAULT NULL,
  `messageCode` varchar(225) DEFAULT NULL,
  `messageText` text,
  `responseCode` varchar(225) DEFAULT NULL COMMENT '1=>Approved,2=>Declined,3=>Error,4=>Held for Review',
  `authCode` varchar(225) DEFAULT NULL,
  `avsResultCode` varchar(225) DEFAULT NULL,
  `cvvResultCode` varchar(225) DEFAULT NULL COMMENT 'M=>Match,N=>No Match,P=>Not Processed,S=>Should have been present,U=>Issuer unable to process request',
  `cavvResultCode` varchar(225) DEFAULT NULL,
  `transactionId` varchar(225) DEFAULT NULL,
  `refTransId` varchar(225) DEFAULT NULL COMMENT 'trans id of prev settled transaction',
  `transHash` varchar(225) DEFAULT NULL,
  `accountNumber` varchar(225) DEFAULT NULL,
  `accountType` varchar(225) DEFAULT NULL,
  `transRespMsgCode` varchar(225) DEFAULT NULL,
  `transRespMsgDescription` varchar(225) DEFAULT NULL,
  `transResErrorCode` varchar(225) DEFAULT NULL,
  `transResErrorText` varchar(225) DEFAULT NULL,
  `ClientDbTbl` text NOT NULL,
  `isupdated` tinyint(1) NOT NULL DEFAULT '0',
  `isDeleted` varchar(11) NOT NULL DEFAULT '0' COMMENT '0 => not deleted, 1 => deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_transactiondetails`
--

INSERT INTO `bb_transactiondetails` (`id`, `orderType`, `bookingId`, `ptype`, `cardholderName`, `caddress`, `ccity`, `cstate`, `ccountry`, `czip`, `cphone`, `cemail`, `bookedAmount`, `paymentStatus`, `bookDate`, `refId`, `resultCode`, `messageCode`, `messageText`, `responseCode`, `authCode`, `avsResultCode`, `cvvResultCode`, `cavvResultCode`, `transactionId`, `refTransId`, `transHash`, `accountNumber`, `accountType`, `transRespMsgCode`, `transRespMsgDescription`, `transResErrorCode`, `transResErrorText`, `ClientDbTbl`, `isupdated`, `isDeleted`) VALUES
(1, '1', 1, 4, 'test test', 'San Diego, San Diego', 'San Diego', 'California', 'United States', '92131', '(999) 999-9999', 'testpankaj@yopmail.com', '0.00', 'Completed', '2019-03-26 00:26:55', '', '', '', '', '', '', '', '', '', 'M-360-N719Y6', NULL, '', '', '', '', '', NULL, NULL, '', 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `bb_userroles`
--

CREATE TABLE `bb_userroles` (
  `id` int(11) NOT NULL,
  `roleName` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_userroles`
--

INSERT INTO `bb_userroles` (`id`, `roleName`, `status`) VALUES
(1, 'Corp. Super Admin', 1),
(2, 'Corp. Admin', 0),
(3, 'Studio User', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bb_users`
--

CREATE TABLE `bb_users` (
  `id` int(11) NOT NULL,
  `firstName` varchar(500) NOT NULL,
  `lastName` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `pwd` varchar(250) NOT NULL,
  `role` int(11) NOT NULL,
  `studioLocation` text NOT NULL,
  `isInvite` tinyint(1) NOT NULL DEFAULT '0',
  `isInvited` tinyint(1) NOT NULL DEFAULT '3',
  `createdDate` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  `ipaddr` varchar(255) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `isActive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bb_users`
--

INSERT INTO `bb_users` (`id`, `firstName`, `lastName`, `email`, `pwd`, `role`, `studioLocation`, `isInvite`, `isInvited`, `createdDate`, `createdBy`, `ipaddr`, `isDeleted`, `isActive`) VALUES
(1, 'B', 'B', 'bb@boardandbrush.com', 'e10adc3949ba59abbe56e057f20f883e', 1, '100', 3, 0, '2018-04-17 00:00:00', 0, '::1', 0, 0),
(99, 'Studio Login', 'asdf', 'studiologininternal@boardandbrush.com', 'e10adc3949ba59abbe56e057f20f883e', 3, '', 2, 0, '2018-08-11 10:53:15', 1, '127.0.0.1', 2, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bb_booking`
--
ALTER TABLE `bb_booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eventLocationId` (`eventLocationId`),
  ADD KEY `isDeleted` (`isDeleted`);

--
-- Indexes for table `bb_booktcktinfo`
--
ALTER TABLE `bb_booktcktinfo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookingId` (`bookingId`);

--
-- Indexes for table `bb_booktcktpersinfo`
--
ALTER TABLE `bb_booktcktpersinfo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookingId` (`bookingId`);

--
-- Indexes for table `bb_calcolor`
--
ALTER TABLE `bb_calcolor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_crondata`
--
ALTER TABLE `bb_crondata`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_event`
--
ALTER TABLE `bb_event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `evLocationId` (`evLocationId`);

--
-- Indexes for table `bb_eventtemplates`
--
ALTER TABLE `bb_eventtemplates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_eventtype`
--
ALTER TABLE `bb_eventtype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_event_bkp`
--
ALTER TABLE `bb_event_bkp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_evoptions`
--
ALTER TABLE `bb_evoptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_evstdrestrictions`
--
ALTER TABLE `bb_evstdrestrictions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_evticketpackage`
--
ALTER TABLE `bb_evticketpackage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_evticketpackageoptions`
--
ALTER TABLE `bb_evticketpackageoptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_proactivitylog`
--
ALTER TABLE `bb_proactivitylog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_procategory`
--
ALTER TABLE `bb_procategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_proclassoptions`
--
ALTER TABLE `bb_proclassoptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_profineprintoptions`
--
ALTER TABLE `bb_profineprintoptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_progallery`
--
ALTER TABLE `bb_progallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_project`
--
ALTER TABLE `bb_project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_promoamounttype`
--
ALTER TABLE `bb_promoamounttype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_promoapplyto`
--
ALTER TABLE `bb_promoapplyto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_promoavailability`
--
ALTER TABLE `bb_promoavailability`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_promocode`
--
ALTER TABLE `bb_promocode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_promocode_bkp`
--
ALTER TABLE `bb_promocode_bkp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_promoschedule`
--
ALTER TABLE `bb_promoschedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_promotaxtype`
--
ALTER TABLE `bb_promotaxtype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_prooptionadd`
--
ALTER TABLE `bb_prooptionadd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `propersoptions_id` (`propersoptions_id`);

--
-- Indexes for table `bb_properattributes`
--
ALTER TABLE `bb_properattributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_propersoptions`
--
ALTER TABLE `bb_propersoptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proId` (`proId`);

--
-- Indexes for table `bb_prorestrictions`
--
ALTER TABLE `bb_prorestrictions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_prorestroptions`
--
ALTER TABLE `bb_prorestroptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_prospcoptions`
--
ALTER TABLE `bb_prospcoptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_prospcsizemeasure`
--
ALTER TABLE `bb_prospcsizemeasure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_prospecifications`
--
ALTER TABLE `bb_prospecifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_protype`
--
ALTER TABLE `bb_protype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_states`
--
ALTER TABLE `bb_states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `bb_studiolocation`
--
ALTER TABLE `bb_studiolocation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_studiostatus`
--
ALTER TABLE `bb_studiostatus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_taxstudio`
--
ALTER TABLE `bb_taxstudio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_timezonestudio`
--
ALTER TABLE `bb_timezonestudio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_transactiondetails`
--
ALTER TABLE `bb_transactiondetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `isDeleted` (`isDeleted`),
  ADD KEY `bookingId` (`bookingId`);

--
-- Indexes for table `bb_userroles`
--
ALTER TABLE `bb_userroles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bb_users`
--
ALTER TABLE `bb_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bb_booking`
--
ALTER TABLE `bb_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bb_booktcktinfo`
--
ALTER TABLE `bb_booktcktinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bb_booktcktpersinfo`
--
ALTER TABLE `bb_booktcktpersinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bb_calcolor`
--
ALTER TABLE `bb_calcolor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `bb_crondata`
--
ALTER TABLE `bb_crondata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18555;

--
-- AUTO_INCREMENT for table `bb_event`
--
ALTER TABLE `bb_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `bb_eventtemplates`
--
ALTER TABLE `bb_eventtemplates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bb_eventtype`
--
ALTER TABLE `bb_eventtype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bb_event_bkp`
--
ALTER TABLE `bb_event_bkp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bb_evoptions`
--
ALTER TABLE `bb_evoptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bb_evstdrestrictions`
--
ALTER TABLE `bb_evstdrestrictions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bb_evticketpackage`
--
ALTER TABLE `bb_evticketpackage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `bb_evticketpackageoptions`
--
ALTER TABLE `bb_evticketpackageoptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bb_proactivitylog`
--
ALTER TABLE `bb_proactivitylog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=246;

--
-- AUTO_INCREMENT for table `bb_procategory`
--
ALTER TABLE `bb_procategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `bb_proclassoptions`
--
ALTER TABLE `bb_proclassoptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `bb_profineprintoptions`
--
ALTER TABLE `bb_profineprintoptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `bb_progallery`
--
ALTER TABLE `bb_progallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `bb_project`
--
ALTER TABLE `bb_project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `bb_promoamounttype`
--
ALTER TABLE `bb_promoamounttype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bb_promoapplyto`
--
ALTER TABLE `bb_promoapplyto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bb_promoavailability`
--
ALTER TABLE `bb_promoavailability`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bb_promocode`
--
ALTER TABLE `bb_promocode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bb_promocode_bkp`
--
ALTER TABLE `bb_promocode_bkp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bb_promoschedule`
--
ALTER TABLE `bb_promoschedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bb_promotaxtype`
--
ALTER TABLE `bb_promotaxtype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bb_prooptionadd`
--
ALTER TABLE `bb_prooptionadd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=434;

--
-- AUTO_INCREMENT for table `bb_properattributes`
--
ALTER TABLE `bb_properattributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bb_propersoptions`
--
ALTER TABLE `bb_propersoptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=475;

--
-- AUTO_INCREMENT for table `bb_prorestrictions`
--
ALTER TABLE `bb_prorestrictions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT for table `bb_prorestroptions`
--
ALTER TABLE `bb_prorestroptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bb_prospcoptions`
--
ALTER TABLE `bb_prospcoptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `bb_prospcsizemeasure`
--
ALTER TABLE `bb_prospcsizemeasure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `bb_prospecifications`
--
ALTER TABLE `bb_prospecifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=314;

--
-- AUTO_INCREMENT for table `bb_protype`
--
ALTER TABLE `bb_protype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `bb_states`
--
ALTER TABLE `bb_states`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1573;

--
-- AUTO_INCREMENT for table `bb_studiolocation`
--
ALTER TABLE `bb_studiolocation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=361;

--
-- AUTO_INCREMENT for table `bb_studiostatus`
--
ALTER TABLE `bb_studiostatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bb_taxstudio`
--
ALTER TABLE `bb_taxstudio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `bb_timezonestudio`
--
ALTER TABLE `bb_timezonestudio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bb_transactiondetails`
--
ALTER TABLE `bb_transactiondetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bb_userroles`
--
ALTER TABLE `bb_userroles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bb_users`
--
ALTER TABLE `bb_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
