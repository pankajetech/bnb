<?php include("includes/header2_inc.php");
if(!isset($_REQUEST["location"]) && $_REQUEST["location"]==""){ header("location: ".base_url_site);    exit; } 
$_COOKIE["locationId"]="";
$_COOKIE["eventId"]="";
$_COOKIE["remindTime"]="";
$_COOKIE["stdLocIds"]="";
$_COOKIE["stdEvntIds"]="";

if (isset($_COOKIE['locationId'])) {

    unset($_COOKIE['eventId']);
    unset($_COOKIE['locationId']);
	 unset($_COOKIE['remindTime']);
	  unset($_COOKIE['stdLocIds']);
	  unset($_COOKIE['stdEvntIds']);
	   setcookie('locationId', null, -1, '/');
	    setcookie('eventId', null, -1, '/');
		 setcookie('remindTime', null, -1, '/');
    setcookie('stdLocIds', null, -1, '/');
    setcookie('stdEvntIds', null, -1, '/');

}

$HTTP_USER_AGENT = strtolower($_SERVER['HTTP_USER_AGENT']);
$android = (bool) strpos($HTTP_USER_AGENT, 'android');
$iphone = !$android && ((bool) strpos($HTTP_USER_AGENT, 'iphone') || (bool) strpos($HTTP_USER_AGENT, 'ipod'));
$webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

$ipad = !$android && !$iphone && (bool) strpos($HTTP_USER_AGENT, 'ipad');
$ios = $iphone || $ipad;
$mobile = $android || $ios;
?>



<style>

.parallax{ min-height: 400px;  }
.hide-bodyscroll{ overflow-y:hidden; position:fixed;}
/*.fc-day-grid-event > .fc-content {
    white-space: normal;
    text-overflow: ellipsis;
    max-height:20px;
}
.fc-day-grid-event > .fc-content:hover {
    max-height:none!important; 
}*/

</style>

<!-- Middle section start-->

<?php 
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI];
$uri_path = parse_url($actual_link, PHP_URL_PATH);
$uri_segments = explode('/', $uri_path);
global $stLoca;
if(count($uri_segments)==3) {
$stLoca=$uri_segments[1];
} else if(count($uri_segments)==2) {
$stLoca=$uri_segments[0];
}


$locationinfo=getLocationIdbyslug($stLoca);
if((isset($locationinfo) && !empty($locationinfo)) && ($locationinfo['stStatus']==3 || $locationinfo['stStatus']==4)) {
$params = array('');
/*echo "SELECT * FROM `bb_event` where evLocationId='".$locationinfo['id']."' and evStatus=0 and isCancel=0 and evTypeStatus='Enabled' and isDeleted=0 order by evDate asc";
die();*/
$evlist = $db->rawQuery("SELECT id,evTitle,evDate,evType,evIsClose,isCancel,evSPubDate,evSPubTime,evStTime,evEndTime,evNoOfTickets,evBookTicket,evCalenderColorLabel,evScheduleHrs,evCalenderColor,evImage,slug FROM `bb_event` where evLocationId='".$locationinfo['id']."' and evStatus=0 and isCancel=0 and evTypeStatus='Enabled' and isDeleted=0 order by evDate asc", $params);

$i=1;
if(isset($evlist) && !empty($evlist)) {
$eventlist=array();
$eventcallist=array();
$curDate=strtotime(date('Y-m-d H:i:s'));
$evDt="";
$tol=0;
$evArr=array();
foreach ($evlist as $key=>$item) {
//$eventlist['id']=$item['id'];
$evIsClose=$item['evIsClose'];
$isCancel=$item['isCancel'];
//$eventlist['title']=$item['evTitle'];
$publishDate=strtotime(date('Y-m-d H:i:s',strtotime($item['evSPubDate']." ".$item['evSPubTime'])));
//$publishDate=strtotime($item['evSPubDate']." ".str_replace(" am","",$item['evSPubTime']));
$eventlist['textColor']="white";
$eventlist['id']=$item['id'];
$eventlist['evTitle']=html_entity_decode($item['evTitle'], ENT_QUOTES);
$eventlist['evEdate']=$item['evDate'];
$eventlist['evmonth']=date("D",strtotime($item['evDate']));
$eventlist['dte']=date("Y-m-d",strtotime($item['evDate']));
$eventlist['publishDate']=date('Y-m-d H:i',$publishDate);

/*$eventlist['evstart']=$item['evStTime'];
$eventlist['evend']=$item['evEndTime'];*/

if($item['evStTime'] != $item['evEndTime']){
	$evStTime1 = $item['evStTime'];
	$evEndTime1 = $item['evEndTime'];
} else{
	$evStTime1 = "00:00";
	$evEndTime1 =  "00:00";
}

if( strpos($evStTime1, ':00 ') !== false ) {
$evST=str_replace(':00 ','',$evStTime1);
} else {
$evST=$evStTime1;
}
$evST=str_replace(' ','',$evST);

if( strpos($evEndTime1, ':00 ') !== false ) {
$evET=str_replace(':00 ','',$evEndTime1);
} else {
$evET=$evEndTime1;
}
$evET=str_replace(' ','',$evET);

$evST = ltrim($evST,0);
$evET = ltrim($evET,0);
if($evST != $evET){
	$evST = $evST;
	$evET = $evET;
} else{ 
	$evST = "00:00";
	$evET =  "00:00";
}

$eventlist['evstart']= $evST;
$eventlist['evend']= $evET;

$eventlist['evseats']=$item['evNoOfTickets'];
$evseats=$item['evNoOfTickets']-$item['evBookTicket'];
//$evCalenderColorLabel=substrwords(html_entity_decode($item['evCalenderColorLabel'], ENT_QUOTES),34,'');
$evCalenderColorLabel=html_entity_decode($item['evCalenderColorLabel'], ENT_QUOTES);

if($evseats >0){
	//Check event has available seats from 'bb_crondata' table
	if(chkEvtHasAvailSeatsInCronTbl($item['id']) < 0){  //all seats vacant
		$evseats = $evseats;
	} 
	else if(chkEvtHasAvailSeatsInCronTbl($item['id']) == 0){ //all seats full
		$evseats = 0;
	} 
	else { //remaining seats
		$evseats = chkEvtHasAvailSeatsInCronTbl($item['id']);
	}
} else {
	$evseats = 0;
}

if($evseats<0) {
$eventlist['evbseats']=0;
$evseats1=0;
} else {
$eventlist['evbseats']=$evseats;
$evseats1=$evseats;
}

//$dttime=strtotime(trim($item['evDate']));
//$startDate1=date('Y-m-d',strtotime("23-06-2018"));
//echo date('d-m-Y h:i:s',$curDate).'--'.date('d-m-Y h:i:s',$startDate1)."--1<Br>";
$evsDays=0;
//echo $item['evDate']." ".$item['evStTime'];





$date = new DateTime($item['evDate']." ".$item['evStTime']);
$startDate = $date->format('Y-m-d H:i:s');
//echo date('d-m-Y h:i:s',$curDate).'--'.$startDate1."--1<Br>";

$startWDate=date('Y-m-d',strtotime($item['evDate']." ".$item['evStTime']));

$evDTime=strtotime(date('Y-m-d H:i:s',strtotime($item['evDate']." ".$item['evStTime'])));
$evBDTime=$item['evDate']." ".$item['evStTime'];
if(isset($item['evScheduleHrs']) && $item['evScheduleHrs']>0) {
$evsDays=strtotime("-".$item['evScheduleHrs']." hours",strtotime($evBDTime));
} else {
$evsDays=$evDTime;
}
//echo $startDate."--1<Br>";






//die();
if($curDate>=$publishDate) {
//echo date('Y-m-d H:i',$publishDate)."--1".$startDate."<Br>";
//$eventlist['end']=$item['evDate']." ".$item['evEndTime'];



/*echo date('d-m-Y h:i:s',$curDate).'--'.date('d-m-Y h:i:s',$evsDays);
die();*/
if($item['evType']==1) {

if($isCancel==1 || $evIsClose==1) {
$eventlist['start']=$startDate;
$eventlist['title']=$evCalenderColorLabel;
/* $eventlist['color']="#cecece"; */

if(isset($item['evCalenderColor']) && $item['evCalenderColor']!="") {
$eventlist['color']=$item['evCalenderColor'];
} else {
$eventlist['color']="#3a87ad";
}



$eventlist['cend']="1";
$cend="1";
} else {
if($curDate>=$evsDays) {
$eventlist['start']=$startDate;
$eventlist['title']=$evCalenderColorLabel;
/* $eventlist['color']="#cecece"; */
if(isset($item['evCalenderColor']) && $item['evCalenderColor']!="") {
$eventlist['color']=$item['evCalenderColor'];
} else {
$eventlist['color']="#3a87ad";
}
$eventlist['cend']="1";
$cend="1";
} else {
$eventlist['start']=$startDate;

if(isset($item['evCalenderColorLabel']) && $item['evCalenderColorLabel']!="") {
//$evCalenderColorLabel=substrwords(html_entity_decode($item['evCalenderColorLabel'], ENT_QUOTES),34,'');
$evCalenderColorLabel=html_entity_decode($item['evCalenderColorLabel'], ENT_QUOTES);
$eventlist['title']=$evCalenderColorLabel;
} else {
$eventlist['title']="Public Workshop";
}
if($evseats1<=0) {
/* $eventlist['color']="#cecece"; */
if(isset($item['evCalenderColor']) && $item['evCalenderColor']!="") {
$eventlist['color']=$item['evCalenderColor'];
} else {
$eventlist['color']="#3a87ad";
}
$eventlist['cend']="1";
$cend="1";
} else {
$eventlist['cend']="0";
$cend="0";
$eventlist['color']=$item['evCalenderColor'];
}
}
//$eventlist['rendering']='background';

}
}
if($item['evType']==2) {

if($isCancel==1 || $evIsClose==1) {
$eventlist['start']=$startDate;
$eventlist['title']=$evCalenderColorLabel;
/* $eventlist['color']="#cecece"; */
if(isset($item['evCalenderColor']) && $item['evCalenderColor']!="") {
$eventlist['color']=$item['evCalenderColor'];
} else {
$eventlist['color']="#3a87ad";
}
$eventlist['cend']="1";
$cend="1";
} else {

if($curDate>=$evsDays || $evIsClose==1 || $evseats==0) {
$eventlist['start']=$startDate;
$eventlist['title']=$evCalenderColorLabel;
/* $eventlist['color']="#cecece"; */
if(isset($item['evCalenderColor']) && $item['evCalenderColor']!="") {
$eventlist['color']=$item['evCalenderColor'];
} else {
$eventlist['color']="#3a87ad";
}
$eventlist['cend']="1";
$cend="1";
} else {
$eventlist['start']=$startDate;
if(isset($item['evCalenderColorLabel']) && $item['evCalenderColorLabel']!="") {
//$evCalenderColorLabel=substrwords(html_entity_decode($item['evCalenderColorLabel'], ENT_QUOTES),34,'');
$evCalenderColorLabel=html_entity_decode($item['evCalenderColorLabel'], ENT_QUOTES);
$eventlist['title']=$evCalenderColorLabel;
} else {
$eventlist['title']="Private Workshop";
}

if($evseats1<=0) {
/* $eventlist['color']="#cecece"; */
if(isset($item['evCalenderColor']) && $item['evCalenderColor']!="") {
$eventlist['color']=$item['evCalenderColor'];
} else {
$eventlist['color']="#3a87ad";
}
$eventlist['cend']="1";
$cend="1";
} else {
$eventlist['cend']="0";
$cend="0";
$eventlist['color']=$item['evCalenderColor'];
}
}
//$eventlist['rendering']='background';


}
}
//$eventlist['evimage']="/cpanel/uploads/events/100X100/".$item['evImage'];
//$eventlist['evimage']= dir_url."events/100X100/".$item['evImage'];
$eventlist['evimage']= dir_url."events/700X600/".$item['evImage'];
$eventlist['evS3image']= $item['evImage'];
//if($evDTime>=$curDate && $evseats>0 && $item['slug']!="" && $curDate<=$evsDays) {
//echo base_url_site.$_REQUEST["location"].'/events/'.$item['slug'];

				if($mobile) {
				$eventurl="";
				} else {
				$eventurl=base_url_site.$stLoca.'/events/'.$item['slug']."/";
				$eventlist['url']=$eventurl;
				$eventurl="";
				}
				/*} else {
				$eventlist['url']="";
				
				}*/
/*} else {
$eventlist['url']="";
}*/
$evArr[]=$item['evDate'];
if(in_array($item['evDate'],$evArr)  && $cend==1){
//if($item['evDate']==$eventlist['evEdate']  && $cend==1 && $curDate<=strtotime($item['evDate'])){
if($curDate>=strtotime($item['evDate'])){
$eventlist['cend']="1";
}

//echo $item['evDate']."--".$cend."<Br>";
//print_r($evArr);
}

//echo $item['evDate']."<Br>";
$eventlist['totEv']=$tol;




$eventcallist[]=$eventlist;


} if($curDate<=$publishDate && $publishDate>=$evDTime) {

//echo date('Y-m-d H:i',$publishDate)."--1".$startDate."<Br>";
/*echo date('d-m-Y h:i:s',$curDate).'--'.date('d-m-Y h:i:s',$evsDays);
die();*/
if($item['evType']==1) {

if($isCancel==1 || $evIsClose==1) {
$eventlist['start']=$startDate;
$eventlist['title']=$evCalenderColorLabel;
/* $eventlist['color']="#cecece"; */
if(isset($item['evCalenderColor']) && $item['evCalenderColor']!="") {
$eventlist['color']=$item['evCalenderColor'];
} else {
$eventlist['color']="#3a87ad";
}
$eventlist['cend']="1";
} else {
if($curDate>=$evsDays) {
$eventlist['start']=$startDate;

$eventlist['title']=$evCalenderColorLabel;
/* $eventlist['color']="#cecece"; */
if(isset($item['evCalenderColor']) && $item['evCalenderColor']!="") {
$eventlist['color']=$item['evCalenderColor'];
} else {
$eventlist['color']="#3a87ad";
}
$eventlist['cend']="1";
} else {
$eventlist['start']=$startDate;
if(isset($item['evCalenderColorLabel']) && $item['evCalenderColorLabel']!="") {
//$evCalenderColorLabel=substrwords(html_entity_decode($item['evCalenderColorLabel'], ENT_QUOTES),34,'');
$evCalenderColorLabel=html_entity_decode($item['evCalenderColorLabel'], ENT_QUOTES);
$eventlist['title']=$evCalenderColorLabel;
} else {
$eventlist['title']="Public Workshop";
}
if($evseats1<=0) {
/* $eventlist['color']="#cecece"; */
if(isset($item['evCalenderColor']) && $item['evCalenderColor']!="") {
$eventlist['color']=$item['evCalenderColor'];
} else {
$eventlist['color']="#3a87ad";
}
$eventlist['cend']="1";
} else {
$eventlist['cend']="0";
$eventlist['color']=$item['evCalenderColor'];
}
}
//$eventlist['rendering']='background';

}
}
if($item['evType']==2) {

if($isCancel==1 || $evIsClose==1) {
$eventlist['start']=$startDate;
$eventlist['title']=$evCalenderColorLabel;
/* $eventlist['color']="#cecece"; */
if(isset($item['evCalenderColor']) && $item['evCalenderColor']!="") {
$eventlist['color']=$item['evCalenderColor'];
} else {
$eventlist['color']="#3a87ad";
}
$eventlist['cend']="1";
} else {

if($curDate>=$evsDays) {
$eventlist['start']=$startDate;
$eventlist['title']=$evCalenderColorLabel;
/* $eventlist['color']="#cecece"; */
if(isset($item['evCalenderColor']) && $item['evCalenderColor']!="") {
$eventlist['color']=$item['evCalenderColor'];
} else {
$eventlist['color']="#3a87ad";
}
$eventlist['cend']="1";
} else {
$eventlist['start']=$startDate;
if(isset($item['evCalenderColorLabel']) && $item['evCalenderColorLabel']!="") {
//$evCalenderColorLabel=substrwords(html_entity_decode($item['evCalenderColorLabel'], ENT_QUOTES),34,'');
$evCalenderColorLabel=html_entity_decode($item['evCalenderColorLabel'], ENT_QUOTES);
$eventlist['title']=$evCalenderColorLabel;
} else {
$eventlist['title']="Private Workshop";
}
if($evseats1<=0) {
/* $eventlist['color']="#cecece"; */
if(isset($item['evCalenderColor']) && $item['evCalenderColor']!="") {
$eventlist['color']=$item['evCalenderColor'];
} else {
$eventlist['color']="#3a87ad";
}
$eventlist['cend']="1";
} else {
$eventlist['cend']="0";
$eventlist['color']=$item['evCalenderColor'];
}
}
//$eventlist['rendering']='background';


}
}
//$eventlist['evimage']="/cpanel/uploads/events/".$item['evImage'];
$eventlist['evimage']= dir_url."events/700X600/".$item['evImage'];
$eventlist['evS3image']= $item['evImage'];
if($mobile) {
$eventurl="";
} else {
$eventurl=base_url_site.$stLoca.'/events/'.$item['slug']."/";
				$eventlist['url']=$eventurl;
				$eventurl="";
}				
/*if($evDTime>=$curDate && $evseats>0 && $item['slug']!="" && $isCancel==0 && $curDate<=$evsDays) {
//echo base_url_site.$_REQUEST["location"].'/events/'.$item['slug'];
$eventlist['url']=base_url_site.$_REQUEST["location"].'/events/'.$item['slug'];
} else {
$eventlist['url']="";
}*/

$evArr[]=$item['evDate'];
//if(in_array($item['evDate'],$evArr)  && $cend==0){
if($item['evDate']==$eventlist['evEdate']  && $cend==1 && $curDate<=strtotime($item['evDate'])){

$eventlist['cend']="0";
//echo $item['evDate']."--".$cend."<Br>";
//print_r($evArr);
}

//echo $item['evDate']."<Br>";
$eventlist['totEv']=$tol;
$eventcallist[]=$eventlist;
}

}

}

/*echo "<pre>";
print_r($eventcallist);
die();*/

?>
<link href='<?php echo base_url_site?>css/fullcalendar.min.css' rel='stylesheet' />
<link href='<?php echo base_url_site?>css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<script src='<?php echo base_url_site?>js/lib/moment.min.js'></script>
<script src='<?php echo base_url_site?>js/fullcalendar.min.js'></script>
<script src='<?php echo base_url_site?>js/animation-script.js'></script>
<!-- Header end-->
<style>

  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }
  
  .footer{ margin-top:3px;}
  .expiredevent {background-color:#ebebeb !important; }
 /* .fc-past {background-color:#000; }*/
/*  .fc-content {
  font-size:9px;
  }*/
  
 .mob_active{display:none; float:left;}

   
  @media screen and (max-width:500px) {
	  /*.bnb-content-box p:not(:first-of-type){display:none;}*/
	   .dir_more{float:left; width:100% display:block;  padding-top:25px; background: -moz-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 22%, rgba(255,255,255,1) 100%); background: -webkit-linear-gradient(top, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 22%,rgba(255,255,255,1) 100%);background: linear-gradient(to bottom, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 22%,rgba(255,255,255,1) 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00ffffff', endColorstr='#ffffff',GradientType=0 ) }
	   
	   .mob_active{float:right; color:#4a90e2!important;display:block; text-align: right; margin-bottom:10px!important;}
  }

</style>


<?php
if($locationinfo['isbtn']==1) {
$m='05';
$curDte=date('Y-'.$m.'-01');
} else {
$curDte=date('Y-m-d');
}

?>

<script>

function escape_html(str) {
  
 if ((str===null) || (str===''))
       return false;
 else
   str = str.toString();
  
  var map = {
    '&': '&amp;',
	'<': '&lt;',
	'<': '&#60;',
	'>': '&gt;',
	'>': '&#62;',
	'"': '&quot;',
	"'": '&#039;'
  };

  return str.replace(/[&<>"']/g, function(m) { return map[m]; });
}


  $(document).ready(function() {
	  
	  //More and less
	  //$(".dir_more span").click(function(){
	  //	  $(".bnb-content-box p:not(:first-of-type)").slideToggle();	
      //});
	  
	  
	  

    $('#calendar').fullCalendar({
	   header: {
       left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek'
      },
	  timezone: '<?php echo $studioTimezone ;?>',
	  timeFormat: 'h(:mm)a',
	  //timeFormat: 'hh:mm a',
      defaultDate: '<?php echo $curDte?>',
     // navLinks: true, // can click day/week names to navigate views
      editable: true,
      eventLimit: true, // allow "more" link when too many events
	  dayRender: function (data, cell) {
			
			var today = new Date();
			
			var m = $.fullCalendar.moment(data);
			var dc=m.format("DD");
			var dm=m.format("MM");
			var dy=m.format("YYYY");
			//alert(dy);
			//alert(dm+"--"+today.getMonth());
			//alert(dc+"--"+today.getDate());
			if (dc == today.getDate() && dm == (today.getMonth()+1) && dy == (today.getFullYear())) {
		//	alert(dc);
			cell.css("background-color", "#f4c455");
			}
		
			},
      eventMouseover: function (data, event, view) {

//            tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#fff;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;color:#000;">' + '<div class="tooltip-img" style=""><img src="'+data.evimage+'" height="50" width="50"></div> <div  class="tooltip-day" style="float:left;margin-left:5px;">'+data.evmonth+' '+data.evstart+' - '+data.evend+'</div><div  class="tooltip-day" style="float:left;margin-left:5px;">Seats Available: ' + data.evbseats + '/<span>' + data.evseats + '</span></div> </div>';
/*if(data.color=="") {
var color="#3a87ad";
} else  {
var color=data.color;
}*/

  tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#fff;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;color:'+data.textColor+';"><div style=" width:100%; padding:5px 10px; margin-bottom:8px; line-height:21px; box-sizing:border-box; background-color:' + data.color + ';" class="tooltipevt-txt">'+escape_html(data.evTitle)+'</div>' + '<div class="tooltip-img" style=""><img src="'+data.evimage+'" height="50" width="50"></div> <div  class="tooltip-day" style="float:left;margin-left:5px;">'+data.evmonth+' '+data.evstart+' - '+data.evend+'</div><div  class="tooltip-day" style="float:left;margin-left:5px;">Seats Remaining: ' + data.evbseats + ' of ' + data.evseats + '</div> </div>';
  
   $("body").append(tooltip);
					$(this).mouseover(function (e) {
					$(this).css('z-index', 10000);
					$('.tooltiptopicevent').fadeIn('500');
					$('.tooltiptopicevent').fadeTo('10', 1.9);
					}).mousemove(function (e) {
					$('.tooltiptopicevent').css('top', e.pageY + 10);
					$('.tooltiptopicevent').css('left', e.pageX + 20);
					});






			

			
			

           


        },
        eventMouseout: function (data, event, view) {
            $(this).css('z-index', 8);

            $('.tooltiptopicevent').remove();

        },
        dayClick: function () {
		
            tooltip.hide()
        },
        eventResizeStart: function () {
            tooltip.hide()
        },
        eventDragStart: function () {
            tooltip.hide()
        },
        viewDisplay: function () {
            tooltip.hide()
        },
      events: <?php echo json_encode($eventcallist);?>,
	  <?php if($mobile) { ?>
	  eventClick: function(calEvent, jsEvent, view) {
	  
	   $.post("<?php echo base_url_site; ?>addon/ajaxGetDayEvents.php", {cevdate:calEvent.evEdate,slocId:'<?php echo $stLoca; ?>'}, function(response) {
		
						var data = JSON.parse(response);
						if(data.totevent==1) {
						window.location.href=data.eUrl;
						} else {
						$('.pop-mob').html(data.pmodel);
						$('.pop-mob').show();
						$('body').addClass('hide-bodyscroll');
						}
						
						//alert(response);
						
						
			});

		
		
        // change the border color just for fun
       // $(this).css('border-color', 'red');
		},
		
		 eventLimit: true,
		views: {
		month: {
		
		  eventLimit: 1,
		  eventLimitText:""
		},
		day: {
 		enable: 0
		}
		},	
	<?php } if($mobile) { ?>	
		  	eventLimitClick: function(cellInfo, jsEvent) {
	var m = $.fullCalendar.moment(cellInfo.date);
	var cdate=m.format("YYYY-MM-DD");

	 $.post("<?php echo base_url_site; ?>addon/ajaxGetDayEvents.php", {cevdate:cdate,slocId:'<?php echo $stLoca; ?>'}, function(response) {
		
						var data = JSON.parse(response);
						if(data.totevent==1) {
						window.location.href=data.eUrl;
						} else {
						$('.pop-mob').html(data.pmodel);
						$('.pop-mob').show();
						$('body').addClass('hide-bodyscroll');
						}
						
						//alert(response);
						
						
			});
	

	},		
	<?php }?>		  
	 /* eventRender: function (data, element, view) {
	  if(data.cend==1){
	  $('.fc-day[data-date="' + data.dte + '"]').addClass("expiredevent");
		 }
	  }*/
    });
	
	
	

  });

</script>







<?php 
$address="";
$address1="";
global $addressFooter;
global $addressMap;
$addressFooter="";
$addressMap="";
$addressFooter="<p>".$locationinfo['stName']." Studio<Br>---<br/>";
if(isset($locationinfo['stStreetAddress']) && $locationinfo['stStreetAddress']!="") {
$address.="<p>".$locationinfo['stStreetAddress'];
$address1=$locationinfo['stStreetAddress'];
$addressFooter.=$locationinfo['stStreetAddress'];
$addressMap.=$locationinfo['stStreetAddress'];
}
if(isset($locationinfo['stAddress1']) && $locationinfo['stAddress1']!="") {
$address.=" <br>".$locationinfo['stAddress1'];
$address1.=" <br>".$locationinfo['stAddress1'];
$addressFooter.=" <br>".$locationinfo['stAddress1'];
$addressMap.=", ".$locationinfo['stAddress1'];
}
if(isset($locationinfo['stCity']) && $locationinfo['stCity']!="") {
$address.=" <br>".$locationinfo['stCity'];
$address1.="<Br> ".$locationinfo['stCity'];
$addressFooter.="<Br> ".$locationinfo['stCity'];
$addressMap.=", ".$locationinfo['stCity'];
}
if(isset($locationinfo['stState']) && $locationinfo['stState']!="") {
$address.=", ".$locationinfo['stState'];
$address1.=", ".$locationinfo['stState'];
$addressFooter.=", ".$locationinfo['stState'];
$addressMap.=", ".$locationinfo['stState'];
}
if(isset($locationinfo['stZip']) && $locationinfo['stZip']!="") {
$address.=" ".$locationinfo['stZip']."</p>";
$address1.=" ".$locationinfo['stZip']."";
$addressFooter.=" ".$locationinfo['stZip']."";
$addressMap.=", ".$locationinfo['stZip'];
}
if(isset($locationinfo['stPhone']) && $locationinfo['stPhone']!="") {
$address.="<p> ".$locationinfo['stPhone']."</p>";
$phone=$locationinfo['stPhone'];
$addressFooter.="<br/><br/> ".$locationinfo['stPhone'];
}
$addressFooter.="</p>";
$addr = $address;
$addr1 = $address1;
?>




<!--CORPORATE EMAIL<br/>
<a href="#">hello@boardandbrush.com</a>-->
</p>

<script>

/* $(document).ready(function(){
  $(".wMore").click(function(){
    if ($(".workshop-content1").height() == 135) {
		//$(".workshop-content").css("height", ""+realHeight+"");
		$(".workshop-content1").animate({height: "100%"});
		$("#mtxt").text("Less");
		$(".maxer").hide();
		$(".miner").show();
	}else {
		//$(".workshop-content").css("height", "95px");
		$(".workshop-content1").animate({height: "135px"});
		$("#mtxt").text("More");
		$(".miner").hide();
		$(".maxer").show();
	}
	});  
}); */



</script>

<div class="loading" style="display:none;">Loading&#8230;</div>
<div class="middle-section-full">
<div class="middle-section-mid">
<div class="middle-section" style="background:#fff; padding-bottom:10px;">

<div class="booking-section">

<div class="booking-completed">
<div class="slash-line-top"></div>
<div id="animation1" class="target-scroll1 fade-in">
<h1 class="lakesight-font">Board & Brush</h1>
<div class="lakesight-second-head"><h2>Welcome to the <?php echo $locationinfo['stName'] ?><br>DIY Wood Sign Workshop!</h2></div>
</div>
<style type="text/css">
    span.read-more { cursor: pointer; color: red; }
    span.more { display: none;  }
	.read-more{margin-top: -28px; z-index: 20; position: relative;}
	.gradient-div{margin-top:-19px; position:relative; z-index:10;float:left; width:100%; padding:20px 0px; background: -moz-linear-gradient(top, rgba(255,255,255,0.2) 0%, rgba(255,255,255,0.2) 1%, rgba(255,255,255,1) 38%, rgba(255,255,255,1) 100%); background: -webkit-linear-gradient(top, rgba(255,255,255,0.2) 0%,rgba(255,255,255,0.2) 1%,rgba(255,255,255,1) 38%,rgba(255,255,255,1) 100%); background: linear-gradient(to bottom, rgba(255,255,255,0.2) 0%,rgba(255,255,255,0.2) 1%,rgba(255,255,255,1) 38%,rgba(255,255,255,1) 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#33ffffff', endColorstr='#ffffff',GradientType=0 );}
	</style>
<script>

$(function() {
        var limit = 146;
        var chars = $("#myDiv").text(); 
		//alert(chars);
        if (chars.length > limit) {
            var visiblePart = $("<span> "+ chars.substr(0, limit-1) +"</span>");
           // var dots = $("<span class='dots'>... </span>");
      		var hiddenPart = $("<span class='more'>"+ chars.substr(limit-1) +"</span>");
            var readMore = $('<div class="gradient-div"></div><div class="read-more " style="  display:inline-block; width:100%; font-size:17px; color:#5a90dc; font-style:normal; float:right;"><span style="float:right; cursor:pointer;" id="mtxt">More +</span></div>');
            readMore.click(function() {
               // $(this).prev().remove(); // remove dots
                //$(this).next().show();
                //$(this).remove(); // remove 'read more'
				if ($("#mtxt").text() === "More +") {
					$("#mtxt").text("Less -");
					$(".gradient-div").hide();
					$(".read-more").css({"margin-top": "0px"});
				}else {
					$("#mtxt").text("More +");
					$(".gradient-div").show();
					$(".read-more").css({"margin-top": "-28px"});
				}
				
				if ($(".more").css("display") == "none"){
					$(".more").show();
				}else{
					$(".more").hide();
				}
				
            });

            $("#myDiv").empty()
                .append(visiblePart)
              //  .append(dots)
                .append(hiddenPart)
				.append(readMore);
        }
    });
	
	
/* var readMore = $('<div class="workshop-license1 read_more"><span class="wMore" style="display:inline-block; font-size:17px; color:#5a90dc; font-style:normal; float:right;"><span id="mtxt">More</span> <span class="maxer">+</span><span class="miner" style="display:none;">-</span></span><br></div>');*/

</script>




<div class="bnb-content-box">
<div class="dmobile" id="myDiv" style="overflow:hidden;">
<?php if($locationinfo['stStatus']==4) {
	$calur="#Calendar-workshop";
} else {
$calur="javascript:void(0);";
}?>


<p>Are you looking for a night out with friends? Is your inner DIY itching for a fun new project without having to buy all your own supplies and make a mess at your house? Then <a href="<?php echo $calur ?>">BOOK A CLASS</a> at Board & Brush Creative Studio in <?php echo $locationinfo['stName'] ?>.
</p>
<p ><?php echo $locationinfo['stAboutStudo'] ?></p>



<?php /*?><div class="dir_more"><span class="mob_active">More+</span><p style="text-align:center; font-style:italic;"> <?php echo $locationinfo['stLocPolicies'] ?> </p></div><?php */?>
</div>


<div class="ddesktop"  style="overflow:hidden;">
<?php if($locationinfo['stStatus']==4) {
	$calur="#Calendar-workshop";
} else {
$calur="javascript:void(0);";
}?>


<p>Are you looking for a night out with friends? Is your inner DIY itching for a fun new project without having to buy all your own supplies and make a mess at your house? Then <a href="<?php echo $calur ?>">BOOK A CLASS</a> at Board & Brush Creative Studio in <?php echo $locationinfo['stName'] ?>.
</p>
<p ><?php $new_string1 = html_entity_decode($locationinfo['stAboutStudo']);
$new_string1 = preg_replace('/<span[^>]+\>|<\/span>/i', '', $new_string1); 

 echo stripSlash($new_string1); ?></p>



<?php /*?><div class="dir_more"><span class="mob_active">More+</span><p style="text-align:center; font-style:italic;"> <?php echo $locationinfo['stLocPolicies'] ?> </p></div><?php */?>
</div>


<p class="polctxt"> 
<?php //echo html_entity_decode($locationinfo['stLocPolicies']); 
$stLocPolicies = str_replace(PHP_EOL,"<br>",$locationinfo['stLocPolicies']);
$stLocPolicies1 = str_replace("&#13;&#10;","<br>",$stLocPolicies);
echo html_entity_decode($stLocPolicies1);
?> 
</p>


</div>
<div class="slash-line-bottom"></div>

<div class="upcoming-classis-box">
<?php if($locationinfo['stStatus']==4) {?>
<h3>Upcoming Classes</h3>

<?php echo getStudioEvents($locationinfo['id'],$locationinfo['slug']); ?>
<?php } else {?>
<span class="csoon">CLASSES COMING SOON - COME BACK OFTEN!</span>
<?php }?>
<?php if($locationinfo['stStatus']==4) {?>
<div class="ucb-link"><a href="#Calendar-workshop">more classes ></a></div>
<?php }?>
</div>

<div class="upcoming-classis-box-mob">
<?php if($locationinfo['stStatus']==4) {?>
<h3>Upcoming Classes</h3>

<?php echo getStudioEventsM($locationinfo['id'],$locationinfo['slug']); ?>
<?php } else {?>
<span class="csoon">CLASSES COMING SOON - COME BACK OFTEN!</span>
<?php }?>
<?php if($locationinfo['stStatus']==4) {?>
<div class="ucb-link"><a href="#Calendar-workshop">more classes ></a></div>
<?php }?>
</div>
</div>

<div class="side-bar">
<div class="contact-info">
<h2><?php echo $locationinfo['stName'].", ".$locationinfo['stState'];?> Studio</h2>
<?php echo $addr?>
<?php 
if($locationinfo['stEmail'] != ""){
	$parts=explode('@',$locationinfo['stEmail']);
	$partemail = $parts[0]."@<br/>".$parts[1];
}
?>
<p><a href="mailto:<?php echo strtolower($locationinfo['stEmail']) ?>"><?php echo $partemail; ?></a></p>
</div>
<div class="sidebar-nav">
<ul>
<?php if($locationinfo['stStatus']==4) {?>
<li><a href="#Calendar-workshop"><svg class="mk-svg-arrow" data-name="mk-icon-angle-right" data-cacheid="icon-5b62cc00bdc39" style=" height:14px; width: 5px; " xmlns="https://www.w3.org/2000/svg" viewBox="0 0 640 1792"><path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path></svg>Calendar- sign up for a class!</a></li>
<?php }?>
<li><a href="#location"><svg class="mk-svg-arrow" data-name="mk-icon-angle-right" data-cacheid="icon-5b62cc00bdc39" style=" height:14px; width: 5px; " xmlns="https://www.w3.org/2000/svg" viewBox="0 0 640 1792"><path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path></svg>Studio location</a></li>
<li><a href="#contact"><svg class="mk-svg-arrow" data-name="mk-icon-angle-right" data-cacheid="icon-5b62cc00bdc39" style=" height:14px; width: 5px; " xmlns="https://www.w3.org/2000/svg" viewBox="0 0 640 1792"><path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path></svg>Contact us</a></li>

</ul>
</div>
</div>





</div>

</div>
</div>
<?php if($locationinfo['stStatus']==4) {?>
<div id="Calendar-workshop" class="booking-section-box " data-parallax="scroll" data-image-src="<?php echo base_url_images; ?>wood-background1.jpg">
<div class="middle-section-mid">
<div class="middle-section">
<h2  id="animation2" class="target-scroll2">Click on a workshop below to register!</h2>
<?php if($locationinfo['isbtn']==1) { ?>
<style>


   

#calendar{position:relative;}
.view-events{backface-visibility: visible; background-color: #f4c455; font-size: 16px; text-transform: uppercase; border: none; position: absolute; top: 27px; width: 100%; }
.view-events a{padding: 4px 0 2px 0; float:left; color: #252525; width:100%; font-size:16px; font-family: 'Conv_Futura Condensed Medium';  font-weight:700;}
.fc-toolbar.fc-header-toolbar{margin-bottom:1.5em;}
.view-events a i{font-weight: bold; font-size: 18px;}
@media screen and (max-width:600px){
.view-events{top:64px;}
.view-events a{padding: 9px 0 8px 0;}
.fc-toolbar.fc-header-toolbar{margin-bottom:3em;}
}
</style>
<div id='calendar'>
<div class="view-events"><a href="https://boardandbrush.com/<?php echo $locationinfo['slug']; ?>#calendar"><b><i class="fa fa-angle-left"></i></b> Click here to see current events!</a></div>
</div>
<?php }else{?>
<div id='calendar'></div>
<?php } ?>
</div>
</div>
<!-- <div class="bg-color-layer"></div>-->
</div>
<?php }?>
<div class="bb-wine-paint-section">
<div class="middle-section-mid">
<div class="middle-section">
<div class="winepaint-box">
<div id="animation3" class="target-scroll3">
<h2>Board & Brush </h2>
<h3>Wine, Paint & Wood Signs</h3>
</div>
<div class="thissection-img"><img src="<?php echo base_url_images?>board-brush-creative-studio.jpg" alt="Section Image"></div>
<div class="section-content-box">
<p>At Board & Brush in <?php echo $locationinfo['stName'] ?>, we marry the joy of wine and paint with the classic timelessness of wooden signs. Our instructor led DIY wood sign workshops allow you to combine your creativity and willingness to learn new skills. We provide you with the instruction, materials and design for you to distress, sand, stain, paint, and customize your personalized piece of art!</p>
<?php if($locationinfo['stStatus']==4) {?>
<div class="centerTextBox"><a href="#Calendar-workshop">Check out our Calendar</a> to view upcoming wine, paint and wood sign workshops!</div>
<?php }?>
<div class="SectionCenterHeading"><h3 id="animation4" class="target-scroll4">Private Workshops, Birthday Parties, Bachelorette Parties, Corporate Events & More</h3></div>
<p>Whether you're looking for a fun way to celebrate your birthday, a distinctive evening out with a group of friends, or a unique relationship-building outing for your employees, look to our DIY wood sign workshops in <?php echo $locationinfo['stName'] ?>. Our space is uniquely set up to provide a warm, welcoming environment that allows everyone to feel comfortable and excited to create their own custom piece of decor.</p>
<div class="centerTextBox"><a href="#contact">Book Your Private Event Today!</a></div>
</div>
</div>
</div>
</div>
</div>
<?php
if(isset($locationinfo['stOwnerPic']) && $locationinfo['stOwnerPic']!=""){
			$imageurl=dir_url."studio/700X600/".$locationinfo['stOwnerPic'];
			$imageName=$locationinfo['stOwnerPic'];
			 }  else  {
			 $imageurl=base_url_images."placeholder.jpg";
			 $imageName="";
			 }

//echo $string =  preg_replace('/<[^>]*>/', '', $content);

$new_string = htmlspecialchars_decode($locationinfo['stBioDesc']);
$new_string = preg_replace('/<span[^>]+\>|<\/span>/i', '', $new_string); 
			 
?>			 
<div class="meet-board-section-box parallax" data-parallax="scroll" data-image-src="<?php echo base_url_images; ?>wood-background1.jpg">
<div class="middle-section-mid" style="max-width:1130px;">
<div class="middle-section">
<div class="meetBoardImgBox"><img src="<?php echo $imageurl?>" alt="<?php echo $locationinfo['stName'] ?>" title="<?php echo $locationinfo['stName'] ?>"></div>
<div class="meetBoardContentBox">
<h2 class="bbname">Meet Board & Brush <?php echo $locationinfo['stName'] ?> <?php if($locationinfo['stSNouns']==1) echo "Owners"; else echo "Owner";?></h2>
<!--<h3>Kimberly & Travis Hodges</h3>-->
<p><?php echo stripSlash($new_string);  ?></p>
<h3><b>Make Your Own Personalized Sign at Board & Brush <?php echo $locationinfo['stName'] ?></b></h3>
<p>Pick your preferred custom wood sign design, sign up for a DIY wood sign workshop that best works for you and your friends or family, and then show up for a fun, instructor led, hands-on experience.</p>
<p><?php echo html_entity_decode($locationinfo['stLocDirection']); ?></p>
<p>Have any questions? Please feel free to contact us via phone or email. We are happy to help and would love to see you in our wood sign workshop very soon!</p>
</div>
</div>
</div>
<!-- <div class="bg-color-layer"></div>-->
</div>

<div class="map-section-box" id="location">
<div class="middle-section-mid">
<div class="middle-section">
<div class="map-content-box">
<h2 id="animation5" class="target-scroll5">Board & Brush</h2>
<h3 id="animation6" class="target-scroll6" style="color:#020202;"><?php echo $addr1?></h3>
<h3 id="animation7" class="target-scroll7"><span><?php echo $phone?>  |  <a href="mailto:<?php echo strtolower($locationinfo['stEmail']) ?>"><?php echo strtolower($locationinfo['stEmail']) ?></a></span></h3>
</div>
</div>
</div>
<div class="section-map-box">
<div class="map-line-section"><img src="<?php echo base_url_images; ?>hr-line-map.jpg" /></div>
<div class="middle-section-mid">
<div class="middle-section">
<!--<div class="map-line-top"></div>-->
</div>
</div>
<style>
#map {
  width: 100%;
  height: 498px;
  -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
  filter: grayscale(100%);
}
</style>
<div class="main-map-img">

<div id="map2"><iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAwD4m6HlepJm2PMhA7KSchNvf7aLQRNgM&q=<?php echo urlencode(str_replace('#','+',$addressMap));?>&zoom=18" allowfullscreen> </iframe></div>

<!--<div id="map"></div>-->


</div>
<style>
.mail-form-box #cmsg{color: rgba(255,255,255,.7)!important;}
</style>

<div class="map-line-section"><img src="<?php echo base_url_images; ?>/hr-line-map.jpg" /></div>
<div class="middle-section-mid">
<div class="middle-section">
<!--<div class="map-line-top"></div>-->
</div>
</div>
</div>
</div>
<div class="mail-box-section parallax" id="contact" data-parallax="scroll" data-image-src="<?php echo base_url_images; ?>wood-background1.jpg">
<div class="middle-section-mid">
<div class="middle-section">
<div class="mail-box-center">
<h2 id="animation8" class="target-scroll8">Have a question? We'd love to hear from you!</h2>
<div class="mail-form-box">
<p id="cmsg" style="display:none;  background-color: rgba(0,107,36,.7); text-align:center; font-weight:bold;  padding: 4px 0; margin-bottom: 10px;"></p>
<form id="contactform" name="contactform">
<input type="hidden" name="stEmail" id="stEmail" value="<?php echo $locationinfo['stEmail']?>"/>
<input type="hidden" name="stName" id="stName" value="<?php echo $locationinfo['stName']?>"/>
<input type="hidden" name="location" id="location" value="<?php echo $_REQUEST["location"]?>" />
<div class="mail-row"><input type="text" name="txtName" id="txtName" placeholder="Your Name"/></div>
<div class="mail-row"><input type="text" name="txtPhone" id="txtPhone" placeholder="Your Phone Number"/></div>
<div class="mail-row"><input type="email" name="txtEmail" id="txtEmail" placeholder="Your Email"/></div>
<div class="mail-row"><textarea rows="4" name="txtMessage" id="txtMessage" placeholder="Your Message"></textarea></div>
<div class="mail-row"><input type="submit" name="btnSubmit" id="btnSubmit" value="Submit"/></div>
</form>
</div>
</div>
</div>
</div>
<!-- <div class="bg-color-layer"></div>-->
</div>
</div>

 
<!-- contact popup  -->


<div class="mk-quick-contact-wrapper">
			
		
		<div id="mk-quick-contact" class="quick-contact-anim" style="display: block;">
			<div class="mk-quick-contact-title">Contact Us</div>
			<p>Have a question for us here at the Studio? Drop us a line and we'll get back to you as soon as we can!</p>
			<form class="mk-contact-form" id="mk-contact-form" method="post" novalidate>
             <input type="hidden" name="stEmail1" id="stEmail1" value="<?php echo $locationinfo['stEmail']?>"/>
<input type="hidden" name="stName1" id="stName1" value="<?php echo $locationinfo['stName']?>"/>
				<input placeholder="Name*" required  name="contactname" id="contactname" class="text-input" value="" tabindex="961" type="text">
				<input data-type="email" required placeholder="Email*" id="contactemail" name="contactemail" class="text-input" value="" tabindex="962" type="email">
				<textarea placeholder="Message*" required id="contactcontent" name="contactcontent" class="textarea" tabindex="963"></textarea>
				
				<div class="btn-cont">
                    <button tabindex="964" class="mk-progress-button mk-contact-button accent-bg-color button" data-style="move-up">
                        <span class="mk-progress-button-content">Send</span>
                        <span class="mk-progress">
                            <span class="mk-progress-inner"></span>
                        </span>
                        <span class="state-success"><svg class="mk-svg-icon" data-name="mk-moon-checkmark" data-cacheid="icon-5b62f3354c9bc" xmlns="https://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M432 64l-240 240-112-112-80 80 192 192 320-320z"></path></svg></span>
                        <span class="state-error"><svg class="mk-svg-icon" data-name="mk-moon-close" data-cacheid="icon-5b62f3354cea7" xmlns="https://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M507.331 411.33l-.006-.005-155.322-155.325 155.322-155.325.006-.005c1.672-1.673 2.881-3.627 3.656-5.708 2.123-5.688.912-12.341-3.662-16.915l-73.373-73.373c-4.574-4.573-11.225-5.783-16.914-3.66-2.08.775-4.035 1.984-5.709 3.655l-.004.005-155.324 155.326-155.324-155.325-.005-.005c-1.673-1.671-3.627-2.88-5.707-3.655-5.69-2.124-12.341-.913-16.915 3.66l-73.374 73.374c-4.574 4.574-5.784 11.226-3.661 16.914.776 2.08 1.985 4.036 3.656 5.708l.005.005 155.325 155.324-155.325 155.326-.004.005c-1.671 1.673-2.88 3.627-3.657 5.707-2.124 5.688-.913 12.341 3.661 16.915l73.374 73.373c4.575 4.574 11.226 5.784 16.915 3.661 2.08-.776 4.035-1.985 5.708-3.656l.005-.005 155.324-155.325 155.324 155.325.006.004c1.674 1.672 3.627 2.881 5.707 3.657 5.689 2.123 12.342.913 16.914-3.661l73.373-73.374c4.574-4.574 5.785-11.227 3.662-16.915-.776-2.08-1.985-4.034-3.657-5.707z"></path></svg></span>
                    </button>
                </div>
				<!--<input id="security" name="security" value="ffb80e546d" type="hidden"><input name="_wp_http_referer" value="/glenmills/" type="hidden">				<input id="sh_id" name="sh_id" value="15" type="hidden"><input id="p_id" name="p_id" value="2342" type="hidden">	-->			
                <div class="contact-form-message clearfix"></div>  
                <div id="ctmsg" style="display:none; text-align:center; font-weight:bold;"></div>
			</form>
			<div class="bottom-arrow"></div>
		</div>
	</div>

<div class='msgbtn icon'><svg class="msg-icon" data-name="mk-icon-envelope" data-cacheid="icon-5b62d5b21c471" style=" height:20px; width: 20px; " xmlns="https://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path fill="#f1f1f1" d="M1792 710v794q0 66-47 113t-113 47h-1472q-66 0-113-47t-47-113v-794q44 49 101 87 362 246 497 345 57 42 92.5 65.5t94.5 48 110 24.5h2q51 0 110-24.5t94.5-48 92.5-65.5q170-123 498-345 57-39 100-87zm0-294q0 79-49 151t-122 123q-376 261-468 325-10 7-42.5 30.5t-54 38-52 32.5-57.5 27-50 9h-2q-23 0-50-9t-57.5-27-52-32.5-54-38-42.5-30.5q-91-64-262-182.5t-205-142.5q-62-42-117-115.5t-55-136.5q0-78 41.5-130t118.5-52h1472q65 0 112.5 47t47.5 113z"></path></svg></div>
</div>




<?php } else {?>
<script>
window.location.href="<?php echo base_url_site?>404/";
</script>
<?php }?>



<script type="text/javascript" src="<?php echo base_url_js; ?>jquery.maskedinput-1.3.1.min_.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
<?php /*?>function initialize() {
    var mapCanvas = document.getElementById("map");
    var mapOptions = {
        center: new google.maps.LatLng(<?php echo $locationinfo['lat'] ?>, <?php echo $locationinfo['lng'] ?>),
        zoom: 18,
		mapTypeControl: false,
   draggable: false,
   scaleControl: false,
   scrollwheel: false,
   navigationControl: false,
   streetViewControl: false,
   mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(mapCanvas, mapOptions);



var image = '<?php echo base_url_images?>BB-Map-Pin.png';

	 var marker = new google.maps.Marker({
          position: map.getCenter(),
          title: '<?php echo $locationinfo['stName'] ?>',
		icon: image,
          map: map
        });

}<?php */?>
</script>

<script type="text/javascript" src="<?php echo base_url_js; ?>jquery.maskedinput-1.3.1.min_.js"></script>

<script>
$(document).ready(function () {
//if($("#txtPhone").val()!="") {
//$("#txtPhone").mask("(999) 999-9999");
$("#txtPhone").mask("(999) 999-9999",{autoclear: false});

//}
$("#txtPhone").focus(function(){
 //alert("asdf");
 document.getElementById('txtPhone').style.pointerEvents = 'none';
});

$("#txtPhone").blur(function(){
 //alert("asdf");
 document.getElementById('txtPhone').style.pointerEvents = 'auto';
});
$("#contactform").validate({
                    rules: {
                        "txtName": {
                            required: true,
                        },
						"txtPhone": {
                            required: false,
							phoneUS: true,
							minlength: 14,
                        }, 
						"txtEmail": {
                            required: true,
                            email: true
                        }, 
						"txtMessage": {
                            required: true,
                        },
                    },
					errorPlacement: function(error,element) {
					return true;
					},

                    //perform an AJAX post to ajax.php
                    submitHandler: function() {
					jQuery('.loading').show(); 
					$.post("<?php echo base_url_site; ?>addon/ajaxContact.php", $("#contactform").serialize(), function(response) {
					var data = JSON.parse(response);
					jQuery('#txtName').val('');
					jQuery('#txtPhone').val('');
					jQuery('#txtEmail').val('');
					jQuery('#txtMessage').val('');
					jQuery('.loading').hide(); 
					if(data.result==0) {
					
					jQuery('#cmsg').css('color','#093');
					jQuery('#cmsg').html(data.msg);
					jQuery('#cmsg').show();
					} if(data.result==1) {
					jQuery('#cmsg').css('color','#F00');
					jQuery('#cmsg').html(data.msg);
					jQuery('#cmsg').show();
					}
					
					});	
                    },
                });


 });
 </script>
 
 
 <script>
$(document).ready(function () {

$("#mk-contact-form").validate({
                    rules: {
                        "contactname": {
                            required: true,
                        },
						"contactemail": {
                            required: true,
                            email: true
                        }, 
						"contactcontent": {
                            required: true,
                        },
                    },
					errorPlacement: function(error,element) {
					return true;
					},

                    //perform an AJAX post to ajax.php
                    submitHandler: function() {
					jQuery('.loading').show(); 
					$.post("<?php echo base_url_site; ?>addon/ajaxsideContact.php", $("#mk-contact-form").serialize(), function(response) {
					var data = JSON.parse(response);
					jQuery('#contactname').val('');
					
					jQuery('#contactemail').val('');
					jQuery('#contactcontent').val('');
					
					if(data.result==0) {
					
					jQuery('#ctmsg').css('color','#093');
					jQuery('#ctmsg').html(data.msg);
					jQuery('#ctmsg').show();
					jQuery('.mk-quick-contact-wrapper').delay( 2000 ).fadeOut("fast");
					} if(data.result==1) {
					jQuery('#ctmsg').css('color','#F00');
					jQuery('#ctmsg').html(data.msg);
					jQuery('#ctmsg').show();
					}
					jQuery('.loading').hide(); 
					});	
                    },
                });


 });
 </script>
 


<script>
$(document).ready(function(){ 

$(".msgbtn").click(function(){	
$(this).toggleClass("mkactive");	
if( $(this).hasClass("mkactive")){
	$(this).siblings(".mk-quick-contact-wrapper").fadeIn('fast');	
	
}
else
{
$(this).siblings(".mk-quick-contact-wrapper").fadeOut('fast');


	}
});
});
</script>


<script src='<?php echo base_url_js?>parallax.min.js'></script>
<script>
$(document).ready(function(){
	$('.parallax').parallax({
    naturalWidth: 600,
    naturalHeight: 400
  });
});
</script>






<!-- Middle section end-->


<?php
include("includes/footer-event_inc.php");
?>
