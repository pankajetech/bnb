<?php include("includes/header_inc.php");
unset($_SESSION["currentOrder"]);
unset($_SESSION["projectId"]);
unset($_SESSION['bookingInfo']);	
unset($_SESSION['promoc']);
unset($_SESSION['promoRApplyTo']);
unset($_SESSION['promoRDiscountType']);
unset($_SESSION['eventAssociated']);


?>

<!-- Middle section start-->





<div class="middle-section-full">
<div class="middle-section-mid">
<div class="middle-section">
<?php ?>
<div class="studio-scetion">
<!--<div class="studio-tab">
<ul>
<li class="selected"><a href="#">Find your studio</a><a class="mob-tab" href="#">1</a></li>
<li><a href="#">Choose a workshop</a><a class="mob-tab" href="#">2</a></li>
<li><a href="#">Personalize</a><a class="mob-tab" href="#">3</a></li>
<li><a href="#">Review & Book!</a><a class="mob-tab" href="#">4</a></li>
</ul>
</div>-->
<div class="studio-content rmv-padborder">
<!--<div class="studio-action">
<div class="gallery-back"><a href="#"><img src="images/squers.png"><span>back to Gallery</span></a></div>
<div class="cancel-project"><a href="#"><span>Cancel</span><svg version="1.1" id="Layer_1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><polygon fill="#333333" points="39.999,38.917 21.072,19.974 39.896,1.152 38.849,0.104 20.026,18.926 1.116,0 0.068,1.047 18.978,19.974 0,38.952 1.047,40 20.025,21.022 38.951,39.965 "/></svg></a></div>
</div>-->
<div class="studio-heading lakesight-font"><h2>Choose a Studio</h2></div>
<div class="studio-txt">Enter your Zip Code or City name to select your nearest Board & Brush</div>
<div class="zip-section">

<div class="zip-form zip-form-search">
<form name="frmMap" id="filterform" action="" method="post" class="search-btn4 search-map22">
<div class="pdf-row"><input class="pdf-text" name="txtSearch" id="txtSearch" type="text" /><label>Search Zip Code or City</label></div>
</form>
<div class="stdo-listing-section" id="stdo-listing-section">


</div>
</div>
<div class="map-zip map_wrapper"><div id="map_canvas" class="mapping" style="width:450px;height:500px;overflow:visible !important;"></div></div>

</div>



<div class="nearest-stdio">
<h2>Find your nearest studio from the list below to book your next workshop.</h2>
<div class="subhead-nearstdo">Studios marked with an * are coming soon!</div>
<div class="nearest-stdio-listing">
<?php
$params = array('');
$getLocations = $db->rawQuery("SELECT * FROM bb_studiolocation where isDeleted=0 and stStatus<>1 ", $params);

$params1 = array('');

$getLocations1 = $db->rawQuery("SELECT stState FROM bb_studiolocation  where isDeleted=0 and stStatus<>1 group by stState order by stState asc  ", $params1);






echo '<div class="nearest-stdio-column">';
$p=1;
foreach( $getLocations1 as $k=>$locationms ){
if($locationms['stState']!="") {
echo '<div class="nearest-stdio-outer">
<h3>'.$locationms['stState'].'</h3><div class="nearest-stdio-links">';
//echo "SELECT * FROM bb_studiolocation where stState like '%".$locationms['stState']."%' and  isDeleted=0 and stStatus<>1  order by stCity asc  ";
$getLocations2 = $db->rawQuery("SELECT * FROM bb_studiolocation where stState like '%".$locationms['stState']."%' and  isDeleted=0 and stStatus<>1  order by stCity asc  ", array(''));
foreach( $getLocations2 as $ky=>$locationmst ){
//$abbr=getAbbr($locationmst['stState']);
if($locationmst['stStatus']==4 || $locationmst['stStatus']==3) {
echo '<p><a href="'.base_url_site.$locationmst['slug'].'" style="text-decoration:none;">'.$locationmst['stName'].', '.$locationmst['stState'].'</a></p>';
}
if($locationmst['stStatus']==2) {
echo '<p>'.$locationmst['stName'].', '.$locationmst['stState'].'*</p>';
}
}
echo '</div>
</div>';

if($p%7==0) echo '</div><div class="nearest-stdio-column">';

}
$p++;

 }?>

</div>
</div>
</div>
<!--<div class="selected-project">
<div class="project-img"><img src="images/selected-project.jpg"></div>
<div class="project-desc">
Selected Project<br>
<span>Home - 16x24</span>
</div>
<div class="project-cost">
<p>C o s t</p>
<span>$</span>65
</div>
</div>-->

</div>

</div>
</div>
</div>

<?php


?>

  <script type="text/javascript">

jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyDF2l9JmpBPo_y0JdK8ZA-rxOZ9cudCb-g&callback=initialize";
    document.body.appendChild(script);
});





function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
	//var geocoder = new google.maps.Geocoder;
    var mapOptions = {
	center: new google.maps.LatLng(37.090240, -95.712891),
	zoom: 6,
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers
    var markers = [
	<?php /* For Each Location Create a Marker. */
if(isset($getLocations[0]['lat']) && isset($getLocations[0]['lng'])) {	
foreach( $getLocations as $k=>$locationm ){
if(($locationm['lat']!="" && $locationm['lng']!="")  && $locationm['isusa']==1) {
$address="";
if(isset($locationm['stStreetAddress']) && $locationm['stStreetAddress']!="") {
$address.=removeEXtraSpace($locationm['stStreetAddress']);
}
if(isset($locationm['stAddress1']) && $locationm['stAddress1']!="") {
$address.=" ".removeEXtraSpace($locationm['stAddress1']);
}
if(isset($locationm['stCity']) && $locationm['stCity']!="") {
$address.=",".removeEXtraSpace($locationm['stCity']);
}
if(isset($locationm['stState']) && $locationm['stState']!="") {
$address.=",".removeEXtraSpace($locationm['stState']);
}
if(isset($locationm['stZip']) && $locationm['stZip']!="") {
$address.=" ".removeEXtraSpace($locationm['stZip']);
}
$addr = $address;
$map_lat = $locationm['lat'];
$map_lng = $locationm['lng'];
$locationId = $locationm['id'];
?>
        ['<?php echo $addr?>', <?php echo $map_lat?>,<?php echo $map_lng?>],
<?php  } }
} else {?>
[37.090240, -95.712891]
<?php }?>
 //end foreach locations ?>    		
    ];
                        
    // Info Window Content
    var infoWindowContent = [
	<?php /* For Each Location Create a Marker. */
if(isset($getLocations[0]['lat']) && isset($getLocations[0]['lng'])) {		
foreach( $getLocations as $k=>$locationm1 ){
if(($locationm1['lat']!="" && $locationm1['lng']!="") && $locationm1['isusa']==1) {
$address="";
if(isset($locationm1['stStreetAddress']) && $locationm1['stStreetAddress']!="") {
$address.=$locationm1['stStreetAddress'];
}
if(isset($locationm1['stAddress1']) && $locationm1['stAddress1']!="") {
$address.=" ".$locationm1['stAddress1']."<Br>";
}
if(isset($locationm1['stCity']) && $locationm1['stCity']!="") {
$address.="".$locationm1['stCity'];
}
if(isset($locationm1['stState']) && $locationm1['stState']!="") {
$address.=",".$locationm1['stState'];
}
if(isset($locationm1['stZip']) && $locationm1['stZip']!="") {
$address.=" ".$locationm1['stZip'];
}
if(isset($locationm1['stPhone']) && $locationm1['stPhone']!="") {
$address.="<Br>Phone ".$locationm1['stPhone'];
}
$addr = $address;
$map_lat = $locationm1['lat'];
$studioName = $locationm1['stName'];
$map_lng = $locationm1['lng'];
$locationId = $locationm1['id'];?>	
        ['<div class="info_content">' +
        '<h3><?php echo $studioName?> Board & Brush</h3>' +
        '<p><?php echo $addr?></p>' +        '</div>'],
 <?php  } }
} else {?>
	['<div class="info_content"><h3>Board & Brush</h3><p>115 HILL STREET<br>Hartland, WI 53029</p></div>'],	
		<?php }
		//end foreach locations ?>      
    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    var response;
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);


		
		
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));
		
		

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
		
		
		
		
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(3);
        google.maps.event.removeListener(boundsListener);
    });
    
}

$(document ).ready(function() {

$('#removesuccess').click(function(){
	$('.successdiv').hide();
	<?php 
	$_SESSION["msg"]="";
	unset($_SESSION["msg"]);
	$_SESSION["ermsg"]="";
	unset($_SESSION["ermsg"]);
	?>
	
	});
	$('#removeerror').click(function(){
	$('.errordiv').hide();
	<?php 
	$_SESSION["msg"]="";
	unset($_SESSION["msg"]);
	$_SESSION["ermsg"]="";
	unset($_SESSION["ermsg"]);
	?>
	});	

$( ".search-map22" ).submit(function( event ) 
		{
          event.preventDefault();
		  
			var alldata=$("#filterform").serializeArray();	
			


			jQuery.ajax({

					url: "<?php echo base_url_site; ?>addon/get-location-map.php", 

					type: "post", //can be post or get

					data: alldata, 

					success: function(reponse)

					{

						jQuery( "#map_canvas" ).html(reponse);

						//fillter();

					}

				});

		});	

$( ".search-btn4" ).submit(function( event ) 
		{
          event.preventDefault();
		  
			var alldata=$("#filterform").serializeArray();	
			


			jQuery.ajax({

					url: "<?php echo base_url_site; ?>addon/get-location.php", 

					type: "post", //can be post or get

					data: alldata, 

					success: function(reponse)

					{

						jQuery( "#stdo-listing-section" ).html(reponse);

						//fillter();

					}

				});

		});			
});	
  </script> 


<!-- Middle section end-->

<?php
include("includes/footer_inc.php");
?>
