<?php ob_start();
include("includes/header3_inc.php");
//if(!isset($_SESSION['studioInfo']) && $_SESSION['studioInfo']=="") {
if(!isset($_SESSION['studioInfo']) && empty($_SESSION['studioInfo'][0]) ) {
	//remove record from cron table wrt cookieuserid,cookieeventslug,cookieLocationslug
	removeBookedTicketWRTCronTbl($_COOKIE['csid'],$_COOKIE['eventId'],$_COOKIE['locationId']);
	$_SESSION["msg"]="Your session has expired, please make your selections again";
	//header("location:".base_url_site);
	//header("location:".base_url_site.$_COOKIE['locationId'].'/events/'.$_COOKIE['eventId']);
	header("location:".base_url_site.$_COOKIE['locationId'].'/');
	exit;
} 
if(empty($_SESSION['currentOrder'])) {
	///$_SESSION["msg"]="Session Expired, Repeat process again";
	if(!isset($_SESSION['studioInfo']) && empty($_SESSION['studioInfo'][0]) ) {
		header("location:".base_url_site.$_COOKIE['locationId'].'/events/'.$_COOKIE['eventId']);
	} else {
		header("location:".base_url_site.$_SESSION['studioInfo'][0]['locationId'].'/events/'.$_SESSION['studioInfo'][0]['eventId']);
	}
	exit;
}
global $db;
?>
<!-- Header end-->
<!-- Middle section start-->
<style>
.crossIcon{position:absolute!important; z-index:1; right:-13px!important; top:-13px!important;}
.crossIcon img{width:25px;}
.editProject{color: #fff; margin-top: 10px; float: left;}


.footer{ display:none;}
.copy-right{ display:none;}

@media only screen and (max-width: 768px){
.goto-top{ display:none !important;}	
}

</style>
<div class="grey-header">
<div class="grey-header-mid">
<div class="studio-tab">
<ul>
<li><a href="javascript:void(0)">Choose a workshop</a><a class="mob-tab" href="#">1</a></li>
<li><a href="javascript:void(0);">Select a project</a><a class="mob-tab" href="#">2</a></li>
<li><a href="javascript:void(0);">Personalize</a><a class="mob-tab" href="#">3</a></li>
<li class="selected"><a href="javascript:void(0);">Review & Book!</a><a class="mob-tab" href="#">4</a></li>
</ul>
</div>
</div>
</div>


<div class="loading" style="display:none;">Loading&#8230;</div>
<div class="middle-section-full">
<div class="middle-section-mid">
<div class="middle-section">
<?php /*if(isset($_SESSION["msg"]) && $_SESSION["msg"]!="") {
if(isset($_SESSION["ermsg"]) && $_SESSION["ermsg"]==0) {
echo '<div class="successdiv" style="display:block;"><label>'.$_SESSION["msg"].'</label><span class="removesuccess" id="removesuccess">X</span></div>';
} if(isset($_SESSION["ermsg"]) && $_SESSION["ermsg"]==1) {
echo '<div class="errordiv"  style="display:block;"><label>'.$_SESSION["msg"].'</label><span class="removeerror" id="removeerror">X</span></div>';
}
}*/


//print_r($_SESSION);
//echo date('d-m-Y h:i:s');
$locationinfo=getLocationIdbyslug($_SESSION['studioInfo'][0]['locationId']);

if(isset($locationinfo) && !empty($locationinfo)) {


$eventinfo=getEventIdbyslug($_SESSION['studioInfo'][0]['eventId'],$locationinfo['id']);
if(isset($eventinfo) && !empty($eventinfo)) {

$evdate=$item['evDate'];
$evDay=date('d',strtotime($eventinfo['evDate']));
$evMonth=date('F',strtotime($eventinfo['evDate']));
$evWeekDay=date('D',strtotime($eventinfo['evDate']));
$evStTime=str_replace(":00 ","",$eventinfo['evStTime']);
$evEndTime=str_replace(":00","",$eventinfo['evEndTime']);

$evseats=$eventinfo['evNoOfTickets']-$eventinfo['evBookTicket'];

}
}

?>
<div class="studio-scetion">

<div class="studio-content rmv-padborder">
<div class="studio-action">
	<div class="gallery-back">
		<!--<a href="<?php //echo base_url_site.$_SESSION['studioInfo'][0]['locationId'];?>#Calendar-workshop">-->
		<a href="javascript:void(0);" class="chng-wrkshp">
		<img src="<?php echo base_url_site?>images/change-workshop.jpg">
		<span>Change Workshop</span>
		</a>
	</div>
	<div class="cancel-project">
		<!--<a href="<?php //echo base_url_site.$_SESSION['studioInfo'][0]['locationId'].'/events/'.$_SESSION['studioInfo'][0]['eventId'];?>"><span>Cancel</span>-->
		<a href="javascript:void(0);" class="cancelCronData"><span>Cancel</span>
		<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><polygon fill="#333333" points="39.999,38.917 21.072,19.974 39.896,1.152 38.849,0.104 20.026,18.926 1.116,0 0.068,1.047 18.978,19.974 0,38.952 1.047,40 20.025,21.022 38.951,39.965 "/></svg></a>
	</div>
</div>
<div class="errordiv tcs"  style="display:none;"><label></label><span class="removeerror" id="removeerror">X</span></div>
<div class="studio-heading lakesight-font"><h2 class="mob-heading">Review Your Reservation</h2></div>
<!--<div class="personal-detail-txt">Please be sure all your details are correct<br /> All of our materials are prepared 48 hours prior to the workshop</div>-->
<div class="personal-detail-txt">Please be sure your details are correct<br /> All of our materials are prepared prior to the workshop</div>
<?php



  $string=$locationinfo['stName'];
   // echo "string is ".$string."<br />";
    $length=strlen($string);
   // echo "total length is ".$length."<br/>";
    $length=round($length/2);
    //echo "middle string will be no ".$length." / the index number 3 which will be the fourh item in that string <br/>";
    //echo $string[$length-1];

/*echo $bookPrefix=substr($string,0,1).$string[$length-1].substr($locationinfo['stState'],0,1);
$digits = 5;
echo rand(pow(10, $digits-1), pow(10, $digits)-1);*/

$cartitems=OrderPriceCalc();

function sortByOrder($a, $b) {
    return $a['oid'] - $b['oid'];
}

function sortByOrder1($a, $b) {
    return key($a) - key($b);
}




if(isset($cartitems) && !empty($cartitems)) {
/*foreach ($cartitems as $key => $row) {
    $comt[$key]  = $row['oid'];
}
@array_multisort($cartitems, SORT_ASC, $comt);*/
usort($cartitems, 'sortByOrder');
}

$totalAmt=0;
foreach($cartitems as $key=>$val) {
if($val['pid']!=0) {
$totalAmt=$totalAmt+number_format($val['totalPrice'],0);
} else {
unset($_SESSION["currentOrder"][$key]);
}
}
?>
<div class="personal-detail">
<div class="personal-detail-left" id="ptotal">
<div class="personal-datetime">
<div class="personal-date"><p><?php echo $evMonth; ?></p><?php echo $evDay; ?></div>
<div class="personal-day"><?php echo $locationinfo['stName'].", ".$locationinfo['stState'];?><br /> <?php echo $evWeekDay; ?> <?php echo ltrim($evStTime, '0'); ?> - <?php echo ltrim($evEndTime, '0'); ?></div>

<div class="total">
<div class="workshop-cost" id="totalId">
<p>C o s t</p>
<span><?php echo SYMBOL;?></span><?php echo $totalAmt;?>
</div>
</div>
</div>
<?php 


$oderBookingInfo=$_SESSION['bookingInfo'];
$oderBookingInfo=multi_unique($oderBookingInfo);
//$oderBookingInfo = array_map("unserialize", array_unique(array_map("serialize", $oderBookingInfo)));
/*print_r($oderBookingInfo);
die();*/
usort($oderBookingInfo, 'sortByOrder1');
//print_r($oderBookingInfo);
$cartT=count($cartitems);
$evTlimit=array();

foreach($cartitems as $key=>$val) {
if($val['pid']!=0) {
	//Get Project Width,Height,Depth
	$val['sizename'] = ftechProjectDimension($val['pid']);
//echo $val['oid']."<br>";
$evTlimit[$val['tid']]+=$val['qty'];
?>
<div class="rv-cost">
<div class="review-cost" style="background-image:url(<?php echo dir_url;?>projects/<?php echo $val['proGalImg'];?>); background-position:center; background-repeat:no-repeat;">

<div class="choose-project">
	<input type="hidden" id="studioLocationSlug" value="<?php echo $_SESSION['studioInfo'][0]['locationId'];?>">
	<input type="hidden" id="persEvtId" value="<?php echo $eventinfo['id']; ?>">
	<input type="hidden" id="persCookieId" value="<?php if(isset($_COOKIE) && $_COOKIE['csid'] != "") { echo $_COOKIE['csid'];} ?>">
	
<span class="crossIcon"><img src="<?php echo base_url_site?>images/remove.png" alt="Remove" data-oid="<?php echo $val['oid']?>" data-pid="<?php echo $val['pid']?>" data-single-proj="1"  class="rproject cancelCronData"></span>
<div class="proleft">
<span><?php echo $val['proName'];?> <?php echo !empty($val['sizename'])?"<br><span class='projectsize'>".$val['sizename']:"";?></span></span><Br />
<a class="editProject" href="<?php echo base_url_site?>ticket-selected-project-gallery?eproid=<?php echo $val['pid'];?>&oid=<?php echo $val['oid'];?>&n=<?php echo $key;?>">change project</a>

</div>

<div class="costfor-user">
<div class="workshop-cost rvw-cost">
<p>C o s t</p>
<span><?php echo SYMBOL;?></span><?php echo number_format($val['evTicketPrice'],0);?>
</div>
</div>
</div>

<div class="bg-transperent"></div>

</div>
<div class="user-info">
<span class="uinfo1"><?php

 echo $oderBookingInfo[$key][$val['oid']]['fullname'];?></span><br />
<span class="uinfo2" style="font-size:19px"><?php echo $oderBookingInfo[$key][$val['oid']]['emailaddr'];?></span>
<?php 
$cRev = $db->rawQuery("select * from bb_propersoptions where proId='".$val['pid']."' order by id ", array(''));
if(count($cRev)>0){
	$p=1;
	echo '<br />';
	foreach($cRev as $key1=>$item){
		//if(isset($val[str_replace(" ","_",$item['proLabel'])])) {
		//echo ucwords($item['proLabel']).' <span class="uinfo3">'.$val[str_replace(" ","_",$item['proLabel'])].'</span> ';
		$proLabel = StringEscapeSlug($item['proLabel']);
		echo ucwords($item['proLabel']).' <span class="uinfo3">'.$val[$proLabel].'</span> ';
		if($p%2==0) {
			echo "<Br>";
		}
		//}
		$p++;
	}
}
//echo $key;
//echo $oderBookingInfo[$key][$key][$val['oid']]['siteby'];
?>


<?php echo (isset($oderBookingInfo[$key][$val['oid']]['siteby']) &&  $oderBookingInfo[$key][$val['oid']]['siteby']!="")?'<br />sit by <span class="uinfo4">'. $oderBookingInfo[$key][$val['oid']]['siteby']:"";?></span>
<div class="user-edit"><a href="javascript:void(0);" data-oid="<?php echo $val['oid']?>" data-hid="<?php echo $key;?>" class="editBook"><img src="
<?php echo base_url_site?>images/edit-personalize.png "/></a></div>
</div>

</div>
<?php }
} ?>
<div class="being-friend">
<ul>
<?php 
//print_r($evTlimit);
$params1=array();
$result1 = $db->rawQuery("SELECT id,evTicketName,evTicketPrice,evTicketButtonLabel,evTicketLImit FROM bb_evticketpackage WHERE  eventID='".$eventinfo['id']."'  order by id asc ", $params1);
if(isset($result1) && !empty($result1)) {
foreach($result1 as $k=>$tval) {
$evticketLmit=checkEventTicketLmit($eventinfo['id'],$tval['id']);
// || ($tval['evTicketLImit']=="" && $eventinfo['evNoOfTickets']!=$cartT)
//echo $evseats."--".$evticketLmit."--".$evTlimit[$tval['id']]."--".$tval['evTicketPrice']."<Br>";
// && $evseats>$evTlimit[$tval['id']]) || ($tval['evTicketLImit']=="" && $evseats!=$cartT) && $evseats!=$cartT
if(($evticketLmit>$evTlimit[$tval['id']] && $evseats>$evTlimit[$tval['id']] && $evseats!=$cartT) || ($tval['evTicketLImit']==""  && $evseats!=$cartT)) {

	//Check event has available seats from 'bb_crondata' table
	if(chkEvtHasAvailSeatsInCronTbl($eventinfo['id']) < 0 || chkEvtHasAvailSeatsInCronTbl($eventinfo['id']) > 0 ) {  //all seats vacant or have remaining seats
	?>
		<li class="eticket" id="<?php echo $eventinfo['id'];?>" data-ticketId="<?php echo $tval['id'];?>" style="cursor:pointer;"><a href="javascript:void(0);"><?php echo $tval['evTicketButtonLabel'];?> for <?php echo SYMBOL;?><?php echo number_format($tval['evTicketPrice'],0);?></a></li>
	<?php 
		}
	}
}
}?>
</ul>
</div>

</div>

<div class="personal-detail-right">
<div class="personal-deatil-form">
 <form name="frmReview" id="frmReview" action="<?php echo base_url_site?>checkout" method="post"> 
<h3 class="lakesight-font">– Apply Discount Code –</h3>
<div id="rmsg" style="margin:2%;"></div>
<div class="showProduct">
	<div class="first_button">
			<input type="text" class="remspace" name="txtPromo" id="txtPromo" value="<?php echo isset($_SESSION["promoc"])?$_SESSION["promoc"]:"";?>" />
			<div class="second_button">
		
		<button class="btnClickCoupon" <?php echo isset($_SESSION["promoc"])?"":"";?>>Apply code <span class="fa">&#xf105;</span></button>
	</div>
	</div>
	
</div>
<input type="hidden" name="chkPromo" id="chkPromo" value="0" />

<input type="submit" id="payId" name="btnPay" value="Pay and complete reservation" />



</form>
</div>



</div>
</div>




</div>

</div>
</div>
</div>



<!-- Middle section end-->

<script>
//setInterval(function(){ $('.successdiv').hide(); $('.errordiv').show(); $('.errordiv label').html("Hey, your session is ending");},300*1000);

$(document).on("blur",".remspace",function() {
var str = $(this).val();
var str = $.trim(str);
var newStr = str.replace(/\s+/g,' ').trim();
$(this).val(newStr);
});


$(document).ready(function() {
		if($('#txtPromo').val()) {
		$(".btnClickCoupon").fadeIn(800);
	}

$("#txtPromo").keyup(function(){
	if ($(this).val() && $(".btnClickCoupon").css("display") == "none"){
		$(".btnClickCoupon").fadeIn(800);
	}else if ($(this).val() == "" && $(".btnClickCoupon").css("display") == "inline-block"){
		$(".btnClickCoupon").fadeOut(800);
	}
});

 // window.history.pushState(null, "", window.location.reload());        
   /*     window.onpopstate = function() {
            window.history.pushState(null, "", window.location.href);
        };*/

	<?php if(isset($_SESSION["promoc"]) && $_SESSION["promoc"]!="") {?>
if($('#txtPromo').val()!="") {
//jQuery('.loading').show(); 
jQuery.ajax({
					url: "<?php echo base_url_site?>addon/ajax_promo.php", 
					type: "post", //can be post or get
					data: {txtPromo:$('#txtPromo').val()}, 
					success: function(response){
						var data=$.parseJSON(response); 
						jQuery('.loading').hide(); 
						if(data.result==0) {
						
						$('#rmsg').html();
						
						
						$('#rmsg').css('color','#FF0000');
						$('#rmsg').html(data.msg);
						/* if(data.msg!=""){
						$('#payId').prop('disabled', true);
						} */
						
						//alert("sdfsdf");
						} if(data.result==1) {
						$('#rmsg').html(data.msg);
						$('#totalId').html(data.totalamt);
						$('#rmsg').css('color','#009933');
						//$('#payId').prop('disabled', false);
						//$('#ptotal').html(data.promohtml);
						
						}
						jQuery('.loading').hide(); 
						
					
					}
				});	
}
<?php }?>
	/* $("#txtPromo").keypress(function(){
		$(".btnClickCoupon").removeAttr('disabled');

	}); */

 // window.history.pushState(null, "", window.location.reload());        
   /*     window.onpopstate = function() {
            window.history.pushState(null, "", window.location.href);
        };*/

$(document).on('click','.btnClickCoupon',function(){
          var promoVal = $("#txtPromo").val();
		  
          if(promoVal!="") {
//jQuery('.loading').show(); 
jQuery.ajax({
					url: "<?php echo base_url_site?>addon/ajax_promo.php", 
					type: "post", //can be post or get
					data: {txtPromo:promoVal}, 
					success: function(response){
						var data=$.parseJSON(response); 
						jQuery('.loading').hide(); 
						if(data.result==0) {
						
						$('#rmsg').html();
						
						
						$('#rmsg').css('color','#FF0000');
						$('#rmsg').html(data.msg);
						/* if(data.msg!=""){
						$('#payId').prop('disabled', true);
						} */
						
						//alert("sdfsdf");
						} if(data.result==1) {
						$('#rmsg').html(data.msg);
						$('#totalId').html(data.totalamt);
						$('#rmsg').css('color','#009933');
						//$('#payId').prop('disabled', false);
						//$('#ptotal').html(data.promohtml);
						
						}
						jQuery('.loading').hide(); 
						
					
					}
				});	
}
	return false;
});


$(document).on("click",".rproject",function() {
	oid=$(this).attr("data-oid");
	pid=$(this).attr("data-pid"); 
	jQuery('.loading').show(); 

	jQuery.ajax({
		url: "<?php echo base_url_site?>addon/ajax_removeProject.php", 
		type: "post", //can be post or get
		data: {ordID:oid,proID:pid}, 
		success: function(response){
			var data=$.parseJSON(response); 
			jQuery('.loading').hide(); 
			if(data.result==0) {
				window.location.reload();
				//console.log('remove-project');
			} 
			if(data.result==1) {
				$('#rmsg').html(data.msg);
				$('#rmsg').css('color','#009933');
				//$('#payId').prop('disabled', false);
				//$('#ptotal').html(data.promohtml);
			}
		}
	});	
});



$(document).on("click",".editBook",function() {
	oid=$(this).attr("data-oid");
	hid=$(this).attr("data-hid");
	
	window.location.href="<?php echo base_url_site?>personalize?oid="+oid+"&type=upd&n="+hid;
});

$(document).on("click",".eticket",function() {
	var cval=0;
	evid=$(this).attr("id");
	evTicketId=$(this).attr("data-ticketId");
	jQuery('.loading').show();
	$.post("<?php echo base_url_site; ?>addon/ajaxAddTicket.php", {evid:evid,evTicketId:evTicketId,type:1}, function(response) {
		var data = JSON.parse(response);
		//console.log('result==='+data.result);
		jQuery('.loading').hide();
		if(data.result==0) {
			window.location.href="<?php echo base_url_site?>ticket-selected-project-gallery";
		} 
		else if((data.result==2 || data.result==1 || data.result==3) && data.type==1) {
			window.location.href="<?php echo base_url_site?>review";
		} 
		else if( data.result==4) {
			$("#amterroridWrkShpFull").show();
		}
		else {
			window.location.href="<?php echo redirect_url?>";	
		}
	});
});

$('#removesuccess').click(function(){
	$('.successdiv').hide();
	<?php 
	$_SESSION["msg"]="";
	unset($_SESSION["msg"]);
	$_SESSION["ermsg"]="";
	unset($_SESSION["ermsg"]);
	?>
	
	});
	$('#removeerror').click(function(){
	$('.errordiv').hide();
	<?php 
	$_SESSION["msg"]="";
	unset($_SESSION["msg"]);
	$_SESSION["ermsg"]="";
	unset($_SESSION["ermsg"]);
	?>
	});	
});
</script>
<style type="text/css">

.first_button{
    width: 100%;
    float: left;
    border-bottom:1px solid #000;
    margin-bottom:20px;
}

.second_button{
    float: right;
    width: 30%;
}
.btnClickCoupon span{font-size:20px; font-weight:bold;}
.btnClickCoupon
{
display:none;
border-radius: 26px;
padding: 5px 0!important;
margin-bottom:5px;
box-shadow: 0px 3px 2px 0px #d7d7d7;
-webkit-box-shadow: 0px 3px 2px 0px #d7d7d7;
-moz-box-shadow: 0px 3px 2px 0px #d7d7d7;
text-align: center;
font-weight: bold;
white-space: pre-wrap;
background: #f4c455;
color: #2e2e2e;
line-height: normal;
font-stretch: normal;
letter-spacing: normal;
font-style: normal;font-size:16px;
border:none;
width:100%;
text-transform:uppercase;
font-family: 'droid_sansregular';
}

.btnClickCoupon:hover{background-color: #000!important; color: #fff!important;}

.btnClickCoupon[disabled="disabled"]{
background: #d7d7d7 !important;
color: #f1f1f1 !important;
} 

#txtPromo{
    border-bottom:none;
    width:64%;
    margin-bottom: 10px;
}

.showProduct{

}

@media screen and (max-width:1000px) {
	#txtPromo{width:60%;}
	.second_button{width:40%; }
}
@media screen and (max-width:768px) {
	#txtPromo{width:100%; border-bottom: 2px solid #999999; margin-bottom:0px;}
	.second_button{width:100%; text-align: center;}
	.first_button{border-bottom:none;}
	.btnClickCoupon{width:150px; font-size:18px; margin-top:25px; box-shadow: 0px 3px 5px 0px #d7d7d7; -webkit-box-shadow: 0px 3px 5px 0px #d7d7d7; -moz-box-shadow: 0px 3px 5px 0px #d7d7d7;}
	.btnClickCoupon span{font-size:21px;}
}


</style>
<!-- Footer section start-->
<?php
include("includes/workshop_full_popup.php");
include("includes/common_footer_inc.php");
include("includes/footer_inc.php");
?>