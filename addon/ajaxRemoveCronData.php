<?php
include("../includes/functions.php");
global $db;

$studioLocationSlug = $_POST["studioLocationSlug"]; //studio slug
$eventSlug = $_POST["eventSlug"]; //event slug
$persCookieId = $_POST["persCookieId"]; //cookie for cron table

$singleProjClk = $_POST["singleProjClk"]; //if single project click

$locationinfo = getLocationIdbyslug($studioLocationSlug);
if(isset($locationinfo) && !empty($locationinfo)) {
	//echo "<pre>locationinfo==";print_r($locationinfo);
	$stLocationId = $locationinfo['id'];
	$eventinfo = getEventIdbyslug1($eventSlug,$stLocationId);
	//echo "<pre>eventinfo==";print_r($eventinfo);die;
	if(isset($eventinfo) && !empty($eventinfo)) {
		$persEvtId = $eventinfo['id']; //event id
	}
}

//If Event is exist in cron tbl
$isExist = isExistEventInCronTbl($persEvtId,$persCookieId);
//echo "<pre>isExist==";print_r($isExist);die;
if($isExist){
	//Get Booked Ticket WRT Event and Cookie Id
	$bookedInfoWRTCOKIEUsr = getBkdTcktForCronWRTCookieUsr($persEvtId,$persCookieId);
	$bookedTicket = $bookedInfoWRTCOKIEUsr['bookedTicket'];
	
	if($singleProjClk ==1){ //if single project delete from review page
		$bookedUpdQty = $bookedTicket - 1;
		if($bookedUpdQty<0){ $bookedUpdQty = 0;} else { $bookedUpdQty = $bookedUpdQty;}
		$updEvInfoArr = array('bookedTicket'=>$bookedUpdQty);
		$db->where('eventId', $persEvtId);
		$db->where('cookieId',$persCookieId );
		$db->update("bb_crondata",$updEvInfoArr);
	} else{
		$bookedUpdQty = 0; //This will work at cancel click at each page
		if($bookedUpdQty<0){ $bookedUpdQty = 0;} else { $bookedUpdQty = $bookedUpdQty;}
		$db->where ('cookieId', $persCookieId);
		$db->where ('eventId', $persEvtId);
		$db->delete("bb_crondata");
	}
	
	$response['msg'] = "success";
	$response['eventSlug'] = $eventSlug;
	$response['studioLocationSlug'] = $studioLocationSlug;
	
	$_SESSION["msg"]= "";
	$_SESSION["ermsg"]=0;

	$response['result']=0;	
	$response['singleProjClk']= $singleProjClk;	
} 
else {
	$response['msg'] = "";
	//$response['eventSlug'] = "";
	//$response['studioLocationSlug'] = "";
	$response['eventSlug'] = $eventSlug;
	$response['studioLocationSlug'] = $studioLocationSlug;
	$response['singleProjClk']= $singleProjClk;	
}
echo json_encode($response);
?>
 