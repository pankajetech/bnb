<?php
include("../includes/functions.php");
global $db;

$params = array('');
$stWhere="";
$strHtml="";
$strHtml1="";
$fixedPrice=65;
if((isset($_REQUEST["evid"]) && $_REQUEST["evid"]!="") && (isset($_REQUEST["evTicketId"]) && $_REQUEST["evTicketId"]!="")) {
/*echo "SELECT * FROM bb_evticketpackage WHERE  eventID='".$_REQUEST["evid"]."' and id='".$_REQUEST["evTicketId"]."'  order by evTicketPrice desc ";
die();*/
$result1 = $db->rawQueryOne("SELECT * FROM bb_evticketpackage WHERE  eventID='".$_REQUEST["evid"]."' and id='".$_REQUEST["evTicketId"]."'  order by evTicketPrice desc ", $params);
$additional=$result1['evTicketPrice']-$fixedPrice;
$evTicketName=$result1['evTicketName'];
$totalP=$result1['evTicketPrice'];
$tid=$result1['id'];

$strHtml='<div class="prvevent-overlay2 dpg-pad">

<div class="cls-pp" id="clpm"><a href="javascript:void(0);"><span>Cancel</span><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><polygon fill="#333333" points="39.999,38.917 21.072,19.974 39.896,1.152 38.849,0.104 20.026,18.926 1.116,0 0.068,1.047 18.978,19.974 0,38.952 1.047,40 20.025,21.022 38.951,39.965 "></polygon></svg></a></div>


<div class="prvevent-wrapper2 dpg-pos">
<h2>Please Confirm</h2>
<p>The price of the ticket you\'ve chosen includes an additional fee of<br>'.SYMBOL.$additional.' for special hardware. By continuing, you acknowledge the total<br>cost of your workshop will be  '.SYMBOL.$totalP.'.</p>
<div class="prvevent-form2">

<form id="prvevt2" action="" method="post">
<div class="pdf-row" style="text-align:center;">
<button  class="prefreshwt eticket" type="button" id="'.$_REQUEST["evid"].'" data-ticketId="'.$tid.'">CONTINUE WITH '.$evTicketName.' FOR '.SYMBOL.$totalP.' <i class="fa fa-angle-right"></i></button>
</div>';

$result15 = $db->rawQuery("SELECT * FROM bb_evticketpackage WHERE  eventID='".$_REQUEST["evid"]."' and id<>'".$_REQUEST["evTicketId"]."'   order by id asc ", $params15);
if(isset($result15) && !empty($result15)) {
foreach($result15 as $k=>$tval) {
if($tval['evTicketOptionId']==9) {
$strHtml.='<div class="pdf-row" style="text-align:center;">
<button type="button" class="prefreshwt5 eticketspecial" id="'.$_REQUEST["evid"].'" data-ticketId="'.$tval['id'].'" style="cursor:pointer;">VIEW '.$tval['evTicketName'].' AT '.SYMBOL.$tval['evTicketPrice'].' <i class="fa fa-angle-right"></i></button>
</div>';
} else {
$strHtml.='<div class="pdf-row" style="text-align:center;">
<button type="button" class="prefreshwt5 eticket" id="'.$_REQUEST["evid"].'" data-ticketId="'.$tval['id'].'" style="cursor:pointer;">VIEW '.$tval['evTicketName'].' AT '.SYMBOL.$tval['evTicketPrice'].' <i class="fa fa-angle-right"></i></button>
</div>';
}
}

} else {
$strHtml.='<div class="pdf-row" style="text-align:center;">
<button type="button" class="prefreshwt5" id="nothanks">NO THANKS - SELECT ANOTHER WORKSHOP <i class="fa fa-angle-right"></i></button>
</div>';

}

$strHtml.='</form>

</div>

</div>
</div>
<style>

.cls-pp{position:absolute; right:30px; top:130px; opacity:0.7;}
.cls-pp a{text-decoration:none;}
.cls-pp a span{color: #4a4a4a; font-size:20px; position:relative; right:10px; top:-13px;}

.cls-pp:hover{opacity:1;}

.prefreshwt:hover {

    color: #fff;
    background-color: #000;
    box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.8);

}
.prefreshwt5:hover {

    color: #fff;
    background-color: #000 !important;
    box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.8);

}
.prefreshwt {

    border-radius: 50px;
    background-color: #ECECEC;
    font-size: 16px;
    border: none;
    padding: 7px 20px;
    margin-top: 0px;
    box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.3);
	

}

.prefreshwt5 {

    border-radius: 50px;
    background-color: #ececec !important;
    font-size: 16px;
    border: none;
    padding: 7px 20px;
    margin-top: 30px;
    box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.3);

}

@media screen and (max-width:1400px) {
	.prvevent-wrapper2{top:65px;}
	.cls-pp{top:28px;}
}

@media screen and (max-width:600px) {
	.cls-pp{top:10px; right:10px;}
}


</style>

';


	
	$response=array();
		$response['pmodel']=$strHtml;

		echo json_encode($response);
}
?>