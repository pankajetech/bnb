<?php
include("../includes/functions.php");
global $db;
$params = array('');
$stWhere="";
if(isset($_REQUEST["txtSearch"]) && $_REQUEST["txtSearch"]!="") {
$q=$_REQUEST["txtSearch"];
if(intval($q)){
$stWhere=" and stZip=".$q;
} else {
$stWhere=" and stCity like '%".$q."%'";
}
}
$strHtml="";

$getLocations = $db->rawQuery("SELECT * FROM bb_studiolocation where isDeleted=0 and stStatus<>1 $stWhere", $params);
if(isset($getLocations) && !empty($getLocations)){
foreach( $getLocations as $k=>$locationm1 ){
if($locationm1['lat']!="" && $locationm1['lng']!="") {
$address="";
if(isset($locationm1['stStreetAddress']) && $locationm1['stStreetAddress']!="") {
$address.=$locationm1['stStreetAddress'];
}
if(isset($locationm1['stAddress1']) && $locationm1['stAddress1']!="") {
$address.=" ".$locationm1['stAddress1']."<Br>";
}
if(isset($locationm1['stCity']) && $locationm1['stCity']!="") {
$address.="".$locationm1['stCity'];
}
if(isset($locationm1['stState']) && $locationm1['stState']!="") {
$address.=",".$locationm1['stState'];
}
if(isset($locationm1['stZip']) && $locationm1['stZip']!="") {
$address.=" ".$locationm1['stZip'];
}
if(isset($locationm1['stPhone']) && $locationm1['stPhone']!="") {
$address.="<Br>Phone ".$locationm1['stPhone'];
}
$addr = $address;
$map_lat = $locationm1['lat'];
$studioName = $locationm1['stName'];
$map_lng = $locationm1['lng'];
$locationId = $locationm1['id'];
if($locationm1['stStatus']==2){
$link="javascript:void(0);";
$studioStatus="<p><strong>Workshops coming soon</strong></p>";
} else if($locationm1['stStatus']==3 || $locationm1['stStatus']==4){
$link=base_url_site.$locationm1['slug'];
$studioStatus="";
} else {
$studioStatus="";
$link="javascript:void(0);";
}
$strHtml.='<div class="studio-listing">
<h2><a href="'.$link.'" style="text-decoration:none;">'.$studioName.' Board & Brush</a></h2>
<div class="stdo-addr">
'.$addr.$studioStatus.'
</div>
</div>
';
}
}
} else {
$strHtml.='<div class="studio-listing" style="color:#FF0000;text-align:center;font-weight:bold;">No Availbale Studio</div>';
}
echo $strHtml;
?>
 