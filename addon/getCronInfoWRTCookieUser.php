<?php
include("../includes/functions.php");
global $db;

$stdLocIds = $_POST["stdLocIds"]; //studio location id
$stdEvntIds = $_POST["stdEvntIds"]; //studio event id
$persCookieId = $_POST["persCookieId"]; //cookie for cron table

$isUpdated = $_POST["isUpdated"];


//If Event is exist in cron tbl
$isExist = isExistEventInCronTbl1($stdEvntIds,$persCookieId,$isUpdated);
//echo "<pre>isExist==";print_r($isExist);die;
if($isExist){
	//Get Booked Ticket WRT Event and Cookie Id
	$bookedInfoWRTCOKIEUsr = getBkdTcktForCronWRTCookieUsr1($stdEvntIds,$persCookieId,$isUpdated);
	$createdDate = $bookedInfoWRTCOKIEUsr['createdDate'];
	
	//$fifteenMinBeforeTimeStamp = strtotime("-5 minutes", strtotime($createdDate));
	
	$cron_exist_time_multiple_user = cron_exist_time_multiple_user; //from config file
	$fifteenMinBeforeTimeStamp = strtotime("- $cron_exist_time_multiple_user minutes", strtotime($createdDate));
	
	$fifteenMinBeforeDate = date('Y-m-d H:i:s', $fifteenMinBeforeTimeStamp); 
	
	/*echo $fifteenMinBeforeDate."----".$createdDate;
	die();*/
	$actualDT1stIns = $bookedInfoWRTCOKIEUsr['actualDT1stIns'];
	
	$response['actualDT1stIns'] = $actualDT1stIns;
	$response['entryDtTimeInCron'] = $createdDate;
	$response['actualDtTimeHittedInCron'] = $fifteenMinBeforeDate;
	$response['isUpdated'] = $isUpdated;
} else {
	$response['entryDtTimeInCron'] = '';
	$response['actualDtTimeHittedInCron'] = '';
	$response['actualDT1stIns'] = '';
	$response['isUpdated'] = '';
}

echo json_encode($response);
?>