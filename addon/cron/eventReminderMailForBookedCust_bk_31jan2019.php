<?php
include("../../includes/functions.php");
global $db;

/*$currentDate = time();

//72 hours before date
$threeDayBeforeCurrentDate = date('Y-m-d H:i:s', strtotime('-3 day', $currentDate)); 
$threDayBfreCrntDtTStamp = strtotime($threeDayBeforeCurrentDate);

$threeDay15MinBeforeCurrentDate = date('Y-m-d H:i:s', strtotime('-72 hours -15 min', $currentDate));
$threDay15MinBfreCrntDtTStamp = strtotime($threeDay15MinBeforeCurrentDate);*/

$sql = "SELECT now() as crntDt,(NOW() + INTERVAL 4320 Minute) as crntDtAft72,(NOW() + INTERVAL 4305 Minute) as crntDtbfr72,bk.id as bookingId,bk.boEmail,bk.boFullName,
bk.totalTickets,bk.cOrderNo,bk.promocode,bk.totalAmt,
et.id as eventId,et.evDate,et.evStTime,et.evEndTime,et.evLocationId,
et.evTitle,et.evNoOfTickets,et.evBookTicket,
TIMESTAMP(`evDate`, STR_TO_DATE(`evStTime`, '%h:%i %p')) as eventDtTimeStr
FROM `bb_booking` bk
Inner join `bb_event` et
on bk.eventId = et.id
WHERE bk.isDeleted=0 and (bk.status=0 OR bk.status=3) 
and et.evStatus=0 and et.isDeleted=0 and et.evIsClose=0 
and et.evTypeStatus='Enabled' and et.isCancel=0 and et.isPublish=0 and et.evBookTicket>0
and bk.isReminderEmail=0
and TIMESTAMP(`evDate`, STR_TO_DATE(`evStTime`, '%h:%i %p'))  <= (NOW() + INTERVAL 4320 Minute) 
AND TIMESTAMP(`evDate`, STR_TO_DATE(`evStTime`, '%h:%i %p'))  >=  (NOW() + INTERVAL 4305 Minute)
order by eventId";		
$cRev = $db->rawQuery($sql,array(''));

//and TIMESTAMP(`evDate`, STR_TO_DATE(`evStTime`, '%h:%i %p')) <= (NOW() - INTERVAL 3 DAY)
//and eventId=59552
//echo "<pre>cRev===";print_r($cRev);die;
if(count($cRev)>0){
	foreach($cRev as $key=>$val){
		$eventDtTimeTimeStamp = strtotime($val['eventDtTimeStr']);
		//if( ($eventDtTimeTimeStamp >= $threDay15MinBfreCrntDtTStamp) && ($eventDtTimeTimeStamp <= $threDayBfreCrntDtTStamp) )
		//{
			$eventId = $val['eventId'];
			/*echo "<br/>eventDtTimeTimeStamp==".$eventDtTimeTimeStamp;
			echo "<br/>threDayBfreCrntDtTStamp==".$threDayBfreCrntDtTStamp;
			echo "<br/>threDay15MinBfreCrntDtTStamp==".$threDay15MinBfreCrntDtTStamp;*/
			//echo "<br/>eventId==".$eventId;
			
			if( strpos($val['evStTime'], ':00 ') !== false ) {
				$evST=str_replace(':00 ','',$val['evStTime']);
			} else {
				$evST=$val['evStTime'];
			}

			if( strpos($val['evEndTime'], ':00 ') !== false ) {
				$evET=str_replace(':00 ','',$val['evEndTime']);
			} else {
				$evET=$val['evEndTime'];
			}
			
			//$eventId = $val['eventId'];
			$studioId = $val['evLocationId'];
			$bookingId = $val['bookingId'];
			
			//booking customer first name
			if( strpos($val["boFullName"], ' ') !== false ) {
				$custName1 = explode(" ",$val["boFullName"]);
				$custFirstName = $custName1[0];
			} else {
				$custFirstName = $val["boFullName"];
			}
			
			$locationinfo = getStudioInfoByStdId($studioId);
			//echo '<pre>locationinfo=='; print_r($locationinfo); 
			
			//Studio complete address
			$address="";
			if(isset($locationinfo['stStreetAddress']) && $locationinfo['stStreetAddress']!="") {
				$address.="".$locationinfo['stStreetAddress'];
			}
			if(isset($locationinfo['stAddress1']) && $locationinfo['stAddress1']!="") {
				$address.=", ".$locationinfo['stAddress1'];
			}
			if(isset($locationinfo['stCity']) && $locationinfo['stCity']!="") {
				$address.=", ".$locationinfo['stCity'];
			}
			if(isset($locationinfo['stState']) && $locationinfo['stState']!="") {
				$address.=" ".$locationinfo['stState']."<br/>";
			}
			if(isset($locationinfo['stZip']) && $locationinfo['stZip']!="") {
				$address.=" ".$locationinfo['stZip']."";
			}
			$addr = $address;
			
			//Get Booking Project info
			$cartitems = getBookingProjectInfo($bookingId);
			//echo '<pre>cartitems=='; print_r($cartitems);
			if(!empty($cartitems)) {
				$cartT = count($cartitems);
			}
			
			if(isset($locationinfo['stLocPolicies']) && $locationinfo['stLocPolicies']!="") {
				$stLocPolicies = $locationinfo['stLocPolicies']."<Br>";
			} else {
				$stLocPolicies = "";
			}
			
			//Studio Social Links
			if(isset($locationinfo["stFbLink"]) && $locationinfo["stFbLink"]!=""){
				$stFbLink = $locationinfo["stFbLink"];
			} else {
				$stFbLink = "www.facebook.com/boardandbrushcorporate/";
			}

			if(isset($locationinfo["stInstaLink"]) && $locationinfo["stInstaLink"]!=""){
				$stInstaLink = $locationinfo["stInstaLink"];
			} else {
				$stInstaLink = "www.instagram.com/explore/locations/1042276229137658/board-brush-creative-studio-corporate-site/";
			}

			if(isset($locationinfo["stTwtLink"]) && $locationinfo["stTwtLink"]!=""){
				$stTwtLink = $locationinfo["stTwtLink"];
			} else {
				$stTwtLink = "www.twitter.com/boardandbrushcs/";
			}

			if(isset($locationinfo["stPinLink"]) && $locationinfo["stPinLink"]!=""){
				$stPinLink = $locationinfo["stPinLink"];
			} else {
				$stPinLink = "www.pinterest.com/boardandbrush/";
			}
			
			if($val["totalTickets"]>1) {
				$rseats="s";
			} else {
				$rseats="";
			}
			if($val['evBookTicket']>1) { 
				$nseats="s"; 
				$helpingVerbForBookTickets = "are";
			} else if($val['evBookTicket']==1) { 
				$nseats=""; 
				$helpingVerbForBookTickets = "is";
			} else { 
				$nseats=""; 
				$helpingVerbForBookTickets = "are"; 
			}
			
			if( $val['evNoOfTickets'] >= $val['evBookTicket'] ){ 
				$remainingSeats = $val['evNoOfTickets']- $val['evBookTicket'];
				if($remainingSeats>1){
					$helpingVerbForRemainingSeats = "are";
				} else if($remainingSeats==1){
					$helpingVerbForRemainingSeats = "is";
				} else {
					$helpingVerbForRemainingSeats = "are";
				}
			} else { 
				$remainingSeats =  "0" ;
				$helpingVerbForRemainingSeats = "are";
			}

			$to = $val['boEmail']; //viplucmca@yahoo.co.in
			$replyStudioEmail = '<a href="mailto:'.$locationinfo["stEmail"].'">Email us ASAP</a>';
			
			$timestamp = strtotime($val["evDate"]);
			$day = date('l', $timestamp);
			
			$subject = $custFirstName.' see you '.$day.' at Board & Brush '.$locationinfo['stName'].'! - Seats:'.$val['totalTickets'].' ID: '.$val["cOrderNo"].'';
			$message = '
			<html>
			<head>
			<title>Board & Brush</title>
			<style>
				body{ background-color:#f1f1f1; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333; font-size:15px;}
				p,h1,h2,h3,h4,h5,h6{ margin:0; padding:0}
				.email-main{ max-width:680px; margin:auto;}
			</style>
			</head>
			<body>
			<div class="email-main" style="max-width:680px; margin:auto;">
				<table width="98%" cellpadding="0" cellspacing="0" border="0" style="padding:8px 1% 8px 1%; background-color:#eaeaea; width:680px; max-width:680px;">
				<tr>
				<td align="center" style="background-color:#FFFFFF; padding-top:15px; padding-bottom:15px;"><img width="300" src="'.base_url_images.'boardandbrush-logo-email.png" /></td>
				</tr>

				<tr>
				<td align="center" style="background-color:#FFFFFF;"><img width="97%" src="'.base_url_images.'hr-line-email.jpg" /></td>
				</tr>

				<tr>
				<td style="background-color:#FFFFFF;" height="10"></td></tr>
				<tr>

				<tr>
				<td style="background-color:#FFFFFF;">

				<table style=" width:100%; padding-top:15px; padding-bottom:15px; padding-left:2%; padding-right:2%;">
				
				<tr><td valign="top" colspan="3">
				Dear '.$custFirstName.',<br><br>
				This is a reminder for your reservation:
				<br></td></tr>
				<tr><td valign="top" colspan="3"><b>BOOKING ID:</b>'.$val["cOrderNo"].'</td></tr>
				<tr>
				<td valign="top" colspan="3">'.$val['evTitle'].'<br>
				'.date('m-d-Y',strtotime($val['evDate'])).' &ndash; '.ltrim($evST,0).' - '.ltrim($evET,0).'<br>
				'.$locationinfo["stName"].', '.$locationinfo['stState'].'<br>'.$addr.'<br><br>
				<b>Amount Paid:</b> '.SYMBOL.$val['totalAmt'] .' 
				<br /><br />
	
				<!--<b>Booking Item Detail:</b>-->
				</td>
				</tr>
				
				<tr><td valign="top" colspan="3">
				<b>IMPORTANT:</b><br>
				You provided the following information for your sign choice and personalization, this information will be used to customize your materials for the workshop. Please double check the below information and '.$replyStudioEmail.' with any corrections that were not provided at the time of booking.
				<a href="https://boardandbrush.com/privacy-policy/">Board & Brush Policies</a>
				</td>
				</tr>
				
				<tr><td valign="top" colspan="3">
				</td></tr>
				
				<tr>
				<td valign="top" colspan="3">

				<table width="100%" cellpadding="0" cellspacing="0" border="0">';
				if(!empty($cartitems)){
					foreach($cartitems as $key=>$valc2) {
						if($valc2['boProjectId']!=0) {
							//Get Project Width,Height,Depth
							$valc2['boTicketProjectSize'] = ftechProjectDimension($valc2['boProjectId']);
					
							$pim = dir_url."projects/700X600/".$valc2["boTicketProjectImage"];
							$message.='<tr style="background:url('.$pim.'); background-repeat:no-repeat; background-size:100%; background-position:center;">
							<td valign="top"  style="width:100%; height:25px; background-repeat:no-repeat; background-size:100%; background-position:center; padding-top:15px; padding-bottom:15px; padding-left:10px; font-size:25px; line-height:25px; color:#fff; text-transform: capitalize; background-color: rgba(0, 0, 0, .5);">
							'.$valc2['boTicketProjectName'];

							$message.=!empty($valc2['boTicketProjectSize'])?"<br><span style='font-size:20px;'>".$valc2['boTicketProjectSize']:"".'</span><Br>';
							$message.='</td>
							</tr>

							<tr>
							<td valign="top" style="width:100%; border:2px solid #4E4E4E; border-top:none; padding-bottom:10px; padding-top:10px; padding-left:10px; padding-right:10px;">
							<span style="font-size:22px; color:#4E4E4E;">'.$val['boFullName'].'</span><br>
							<span style="font-size:17px; color:#4E4E4E;">'.$val['boEmail'].'</span><br />';
							$personaliz="";
							$personalizB="";
							$cRev = $db->rawQuery("select * from bb_booktcktpersinfo where bookingId='".$bookingId."' and bookingTicketId='".$valc2['id']."' order by id ", array(''));
							if(count($cRev)>0){
								$p=1;
								//$personaliz.='<br />';
								if(isset($cRev) && !empty($cRev)){
									//$personalizB ="<Br>";
									foreach($cRev as $key=>$item){
										$personaliz.=ucwords($item['boPersOptName']).' <span style="font-size:22px; color:#4E4E4E;">'.$item['boPersOptVal'].'</span> ';
										if($p%2==0) {
											$personaliz.="<Br>";
										}
										$p++;
									}
								}
							} else {
								$personalizB="<Br>";
							}
							$message.=	$personaliz;		
							$message.=(isset($valc2['boTicketSitBy']) && $valc2['boTicketSitBy']!="")?$personalizB.'sit by <span style="font-size:22px; color:#4E4E4E;">'.$valc2['boTicketSitBy']:"";
							$message.='</tr>
							<tr><td height="10"></td></tr>';
						}
					}
				}
				if(isset($val['promocode']) && $val['promocode']!="") {
					//Get promo code details
					$promoDetails2 = ftechPromoDetailsReminderEmail($val['promocode']);
					if($promoDetails2["promoRApplyTo"]==2) { //Each Ticket
						$totalTick=" x ".$cartT;
					} else { //Order Total
						$totalTick="";
					}
		
					if($promoDetails2["promoRApplyTo"]==2) { //Each Ticket
						if($promoDetails2["promoRDiscountType"]==1) { //Fixed Amount
							$disV = SYMBOL.$promoDetails2["promoRDiscountVal"];
						} else { //Percentage
							$disV = $promoDetails2["promoRDiscountVal"].'%';
						}
					} else { //Order Total
						$disV = SYMBOL.number_format($val['totalAmt'],2);
					}
		
					$message.='<tr>
					<td style="width:100%; border:2px solid #4E4E4E; padding-bottom:10px; padding-top:10px; padding-left:10px; padding-right:10px;">
					Coupon Code – '.$disV.' '.$totalTick.' ('.$val['promocode'].')</span>
					</td>
					</tr>';
				}
				//Now there '.$helpingVerbForBookTickets.' '.$val['evBookTicket'].' seat'.$nseats.' reserved including yours, '.$remainingSeats.' '.$helpingVerbForRemainingSeats.' still available. --> 
					
				$message.='</table>
					</td>
					</tr>
					<tr>
					<td colspan="3">
					We look forward to seeing you!
					<br><br>
					Got Wood?!
					</td>
					</tr>

					<tr>
					<td colspan="3">
					<b>-------------------------------------------------------------------------------<br />
					Board & Brush '.$locationinfo['stName'].'<Br>
					'.$locationinfo['stStreetAddress'].' | '.$locationinfo['stPhone'].'<Br>
					'.$locationinfo['stEmail'].'<Br>
					<a href="'.base_url_site.$locationinfo['slug'].'">'.base_url_site.$locationinfo['slug'].'</a><Br>
					'.$stLocPolicies.'
					</b>
					<a href="'.$stFbLink.'"><img style="border:none;width:34px;" src="'.base_url_images.'fb1.png"></a>&nbsp;&nbsp;<a href="'.$stInstaLink.'"><img style="border:none;width:34px;" src="'.base_url_images.'inst1.png"></a>&nbsp;&nbsp;<a href="'.$stTwtLink.'"><img style="border:none;width:34px;" src="'.base_url_images.'twt1.png"></a>&nbsp;&nbsp;<a href="'.$stPinLink.'"><img style="border:none;width:34px;" src="'.base_url_images.'pin1.png"></a>
					
					</td>
					</tr>

					</table>

					</td>
					</tr>
					</table>
					<table width="98%" cellpadding="0" cellspacing="0" border="0" style="padding:0 1%; margin-top:10px; margin-bottom:10px;">
					<tr>
					<td align="center">Copyrighted Board & Brush, LLC. All Rights Reserved © '.date("Y").' </td>
					</tr>
					</table>

					</div>

					</body>
					</html>'; 
	
					//echo $message;
					//die("sdfsdfsdf3223");
					/*$headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
					$headers .= "From:".$locationinfo['stEmail']." \r\n";
					$header .= "Content-type: text/html\r\n";*/

				if( strpos($locationinfo['stName'], ',') !== false ) {
					$stN1=explode(',',$locationinfo['stName']);
					$stN=$stN1[0];
				} else {
					$stN=$locationinfo['stName'];
				}
				 
				$stName=$stN."  Board & Brush";
				$stEmail=$locationinfo['stEmail'];
					
				$headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
				$headers .= "From: ".$stName." <".$stEmail."> \r\n";
				$headers .= "Reply-To: ".$stName." <".$stEmail."> \r\n";
				$header .= "Content-type: text/html\r\n";
	
			if(mail ($to,$subject,$message,$headers)){
				$updBookingInfoArr = array('isReminderEmail' => 1);
				$db->where('id', $bookingId);
				$db->where('eventId', $eventId);
				$db->update("bb_booking",$updBookingInfoArr);
			}
		//}
	} //end foreach
} else {
	echo "No matching record found";
}
?>
 