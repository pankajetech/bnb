<?php
include("../../includes/functions.php");
global $db;

$currentDt = date('Y-m-d H:i:s');
$currentDtStr = strtotime($currentDt);

//Get Scheduled project which is in draft
$sql = "SELECT * FROM `bb_project` 
	where status=0 and proSchDtOptional=0 and isDeleted=0";
$cRev = $db->rawQuery($sql,array(''));

if(count($cRev)>0){
	foreach($cRev as $key=>$val){
		$proSchStDtTimeStr = strtotime($val['proSchStDtTime']);
		$projectId = $val['id'];
		if($proSchStDtTimeStr <= $currentDtStr){
			//Update those projects autopublish status
			$sqll = "update `bb_project` set status=0 where id='".$projectId."' ";
			$db->rawQueryOne($sqll, array(''));
			
			//Project history insertion in activity_log table
			/*$actionvar = "Scheduled Project Auto Published";
			$data = array (
				'lgUserId' => 1,
				'lgProId' => $projectId,
				'lgDescription' => $actionvar,
				'lgDate' => date('Y-m-d'),
				'lgTime' => date('H:i:s')
			);
			$db->insert("bb_proactivitylog",$data);*/
		}
	}
} else {
	return false;
}
?>
 