<?php
include("../includes/functions.php");
global $db;

$HTTP_USER_AGENT = strtolower($_SERVER['HTTP_USER_AGENT']);
$android = (bool) strpos($HTTP_USER_AGENT, 'android');
$iphone = !$android && ((bool) strpos($HTTP_USER_AGENT, 'iphone') || (bool) strpos($HTTP_USER_AGENT, 'ipod'));
$ipad = !$android && !$iphone && (bool) strpos($HTTP_USER_AGENT, 'ipad');
$ios = $iphone || $ipad;
$mobile = $android || $ios;

if($mobile) {
$ismobile=1;
} else {
$ismobile=0;
}

$devicename="";

if($android) { 
$devicename="android"; 
} else if($iphone) { 
$devicename="iphone"; 
} else if($ipad) { 
$devicename="ipad";
} else {
$devicename="desktop";
}




//echo "eventId===".$_SESSION['studioInfo'][0]['eventId']; 
if($_SESSION['studioInfo'][0]['eventId']!= "")
{
	$locationinfo = getLocationIdbyslug($_SESSION['studioInfo'][0]['locationId']);
	if(isset($locationinfo) && !empty($locationinfo)) {
		$eventDetail = getEventIdbyslug($_SESSION['studioInfo'][0]['eventId'],$locationinfo['id']);
		if(isset($eventDetail) && !empty($eventDetail)) {
			$evNoOfTickets = $eventDetail['evNoOfTickets'];
			$evBookTicket = $eventDetail['evBookTicket'];
		}
	}
	
	
	if($evNoOfTickets > $evBookTicket)
	{
	//if(isset($_POST['cardno']) && !empty($_POST['cardno'])) {

	$locationinfo=getLocationIdbyslug($_SESSION['studioInfo'][0]['locationId']);
	if(isset($locationinfo) && !empty($locationinfo)) {
		$eventinfo=getEventIdbyslug($_SESSION['studioInfo'][0]['eventId'],$locationinfo['id']);
		if(isset($eventinfo) && !empty($eventinfo)) {
			$evdate=$item['evDate'];
			$evDay=date('d',strtotime($eventinfo['evDate']));
			$evMonth=date('F',strtotime($eventinfo['evDate']));
			$evWeekDay=date('D',strtotime($eventinfo['evDate']));
			$evStTime=str_replace(":00 ","",$eventinfo['evStTime']);
			$evEndTime=str_replace(":00","",$eventinfo['evEndTime']);
		}
	}
	
	/*$string=$locationinfo['stName'];
	$length=strlen($string);
	$length=round($length/2);
	$digits = 5;
	$bookingNo=str_replace(",",substr($string,0,1).$string[$length-1]).substr($locationinfo['stState'],0,1).rand(pow(10, $digits-1), pow(10, $digits)-1);*/
	
	
	$pbookNo=strtoupper(random_code(6));
		
	if($locationinfo['stNum']<=9) {
		$bookingNo="00".$locationinfo['stNum']."-".$pbookNo;
	}
	if($locationinfo['stNum']>=10 && $locationinfo['stNum']<=99) {
		$bookingNo="0".$locationinfo['stNum']."-".$pbookNo;
	}
	if($locationinfo['stNum']>=100) {
		$bookingNo=$locationinfo['stNum']."-".$pbookNo;
	}

	$cartitems=OrderPriceCalc();
	//echo "<pre>cartitems===";print_R($cartitems);die;
	function sortByOrder($a, $b) {
		return $a['oid'] - $b['oid'];
	}

	function sortByOrder1($a, $b) {
		return key($a) - key($b);
	}
	
	if(isset($cartitems) && !empty($cartitems)) {
		/*foreach ($cartitems as $key => $row) {
			$comt[$key]  = $row['oid'];
		}
		@array_multisort($cartitems, SORT_ASC, $comt);*/
		usort($cartitems, 'sortByOrder');
	}

	$noofProject=count($cartitems);
	$cartT=count($cartitems);
	
	//echo "<pre>cartitems===";print_R($cartitems);die;
	if($cartT>0){
		$lineItemArr = array();
		foreach($cartitems as $cartTKey=>$cartTVal){
			$lineItemArr[$cartTKey]['itemId'] = $cartTVal['pid'];
			
			if($cartTVal['proName']!=""){
				if(strlen($cartTVal['proName'])>=10){
					$lineItemArr[$cartTKey]['name'] = substr($cartTVal['proName'],0,10);
				} else{
					$lineItemArr[$cartTKey]['name'] = $cartTVal['proName'];
				}
			} 
			
			if($cartTVal['proDetailedDesc']!=""){
				if(strlen($cartTVal['proDetailedDesc'])>=10){
					$lineItemArr[$cartTKey]['description'] = substr($cartTVal['proDetailedDesc'],0,10);
				} else{
					$lineItemArr[$cartTKey]['description'] = $cartTVal['proDetailedDesc'];
				}
			} else {
				$lineItemArr[$cartTKey]['description'] = substr($cartTVal['proName'],0,10);
			}
			$lineItemArr[$cartTKey]['quantity'] = $cartTVal['qty'];
			$lineItemArr[$cartTKey]['unitPrice'] = $cartTVal['totalPrice'];
		}
	}
	
	//echo "<pre>lineItemArr===";print_R($lineItemArr);die;

	if(intval($eventinfo['evNoOfTickets'])!=intval($eventinfo['evBookTicket'])) {
		$remainingTicket1=$eventinfo['evBookTicket']+$noofProject;
		if($remainingTicket1<0) {
			$errorMsg['result']=3;
			$_SESSION["msg"]="You can't add more event ticket package - it's limit";
			echo json_encode($errorMsg);
			exit;
		}
	}


	$tax=getTaxbyStudio($locationinfo['id']);
	$totalAmt=0;
	$totEvAmount=0;
	foreach($cartitems as $key=>$val) {
		if($val['pid']!=0) {
			if(isset($_SESSION['promoRApplyTo']) && $_SESSION['promoRApplyTo']!="") {
				$promoDetails1=ftechPromoDetails($_SESSION["promoc"]);
				if($promoDetails1['promoRApplyTo']==2) {
					if($promoDetails1["promoRTaxType"]==1) {
						if($promoDetails1['promoRDiscountType']==2) {
							$totEvAmount=$totEvAmount+($val['evTicketPrice']*($promoDetails1["promoRDiscountVal"]/100));
							$totalAmt1=$totalAmt1+($val['evTicketPrice']-($val['evTicketPrice']*($promoDetails1["promoRDiscountVal"]/100)))+(($val['evTicketPrice']-($val['evTicketPrice']*($promoDetails1["promoRDiscountVal"]/100)))*($tax/100));
							$taxAmt=$taxAmt+(($val['evTicketPrice']-($val['evTicketPrice']*($promoDetails1["promoRDiscountVal"]/100)))*($tax/100));
						} 
						if($promoDetails1['promoRDiscountType']==1) {
							$totEvAmount=$totEvAmount+$promoDetails1["promoRDiscountVal"];
							$totalAmt1=$totalAmt1+($val['evTicketPrice']-$promoDetails1["promoRDiscountVal"])+($val['evTicketPrice']-$promoDetails1["promoRDiscountVal"])*($tax/100);
							$taxAmt=$taxAmt+($val['evTicketPrice']-$promoDetails1["promoRDiscountVal"])*($tax/100);
						}
					}
					
					if($promoDetails1["promoRTaxType"]==2) {
						if($promoDetails1['promoRDiscountType']==2) {
							$totEvAmount=$totEvAmount+(($val['evTicketPrice']+($val['evTicketPrice']*($tax/100)))*($promoDetails1["promoRDiscountVal"]/100));
							$totalAmt1=$totalAmt1+(($val['evTicketPrice']+($val['evTicketPrice']*($tax/100)))-(($val['evTicketPrice']+($val['evTicketPrice']*($tax/100)))*($promoDetails1["promoRDiscountVal"]/100)));
							$taxAmt=$taxAmt+($val['evTicketPrice']*($tax/100));
							//	echo $totalAmt1."dfsdf".$tax;
						
							//$totalAmt1=$totalAmt1+($val['evTicketPrice']*($results["promoRDiscountVal"]/100));
						} 
						if($promoDetails1['promoRDiscountType']==1) {
							$totEvAmount=$totEvAmount+$promoDetails1["promoRDiscountVal"];
							$totalAmt1=$totalAmt1+(($val['evTicketPrice']+($val['evTicketPrice']*($tax/100)))-$promoDetails1["promoRDiscountVal"]);
							$taxAmt=$taxAmt+($val['evTicketPrice']*($tax/100));
						}
					}
				}
			}
		$totalAmt=$totalAmt+number_format($val['totalPrice'],0);
		}
	}
	
	$istax=0;
	
	if(isset($_SESSION['promoRApplyTo']) && $_SESSION['promoRApplyTo']!="") {
		$promoDiscount=0;
		$promoDetails=ftechPromoDetails($_SESSION["promoc"]);
		if($promoDetails["promoRApplyTo"]==1) {
			if($promoDetails["promoRDiscountType"]==1) {
				if($promoDetails["promoRTaxType"]==1) {
					$totEvAmount=$promoDetails["promoRDiscountVal"];
					$taxAmt=(($totalAmt-$totEvAmount)*($tax/100));
					$totalAmt1=$totalAmt1+($totalAmt-$totEvAmount)+(($totalAmt-$totEvAmount)*($tax/100));
				} 
				if($promoDetails["promoRTaxType"]==2) {
					$taxAmt=$taxAmt+($totalAmt*($tax/100));
					$totalAmt1=$totalAmt1+(($totalAmt+($totalAmt*($tax/100)))-$promoDetails["promoRDiscountVal"]);
					$totEvAmount=$promoDetails["promoRDiscountVal"];
				}
			} 
			if($promoDetails["promoRDiscountType"]==2) {
				if($promoDetails["promoRTaxType"]==1) {
					$totEvAmount=($totalAmt*$promoDetails["promoRDiscountVal"]/100);
					$totalAmt1=$totalAmt1+(($totalAmt-$totEvAmount)+(($totalAmt-$totEvAmount)*($tax/100)));
			
					$taxAmt=$taxAmt+(($totalAmt-$totEvAmount)*($tax/100));
				} 
				if($promoDetails["promoRTaxType"]==2) {
					$taxAmt=$taxAmt+($totalAmt*($tax/100));
					$totEvAmount=(($totalAmt+($totalAmt*($tax/100)))*$promoDetails["promoRDiscountVal"]/100);
					$totalAmt1=$totalAmt1+($totalAmt+($totalAmt*($tax/100)))-$totEvAmount;
				}
			}
		}
		
		
		if($promoDetails["promoRTaxType"]==1) {
			$istax=1;
		} 
		if($promoDetails["promoRTaxType"]==2) {
			$istax=2;
		}
	} 
	else {
		$taxAmt=$totalAmt*($tax/100);
		$totalAmt1=$totalAmt+$taxAmt;
	}

	$netAmt1=$totalAmt1;
	/*echo $netAmt1;
	die();*/
	if(intval($netAmt1)<=0) { $netAmt1="0.00"; } else { $netAmt1=$netAmt1; }
	  
	if(isset($taxAmt)  && $taxAmt!="0.00") $taxT=$taxAmt; else $taxT="0.00";
	$ntotalAmt=$netAmt1;
	$total      = str_replace(",","",$ntotalAmt);
	
	//$invoice    = substr(time(), 0, 6);
	if($locationinfo['stNum']<=9) {
		$invoice="00".$locationinfo['stNum']."-".$pbookNo;
	}
	if($locationinfo['stNum']>=10 && $locationinfo['stNum']<=99) {
		$invoice="0".$locationinfo['stNum']."-".$pbookNo;
	}
	if($locationinfo['stNum']>=100) {
		$invoice=$locationinfo['stNum']."-".$pbookNo;
	}

	$tax        = "0.00";
	$customerId = $invoice;
	$authInf=explode("/",$locationinfo['stAuthInfo']);
	$cusNameA=explode(' ', $_POST['namecard'], 2);

	if(isset($cusNameA[1]) && $cusNameA[1]!="") {
		$cFName=$cusNameA[0];
		$cLName=$cusNameA[1];
	} else {
		$cFName=$_POST['namecard'];
	}

	global $approval_code;
	global $avs_result;
	global $cvv_result;	

	//echo "<pre>xml==";print_r($xml);die;

	//if(isset($_POST['cardno']) && !empty($_POST['cardno']) && $xml->messages->resultCode!='') {
		$approval_code="";
		$avs_result="";
		$cvv_result="";
		/*echo $xml->messages->message->text;
		die();*/
		/*if($xml->isSuccessful()== "yes" || $xml->isSuccessful()== 1 || $xml->messages->message->text=="Successful.") {
			$approval_code  = $xml->transactionResponse[0]->authCode;
			$avs_result     = $xml->transactionResponse[0]->avsResultCode;
			$cvv_result     = $xml->transactionResponse[0]->cvvResultCode;
			$transaction_id = $xml->transactionResponse[0]->transId;
			
			$accountNumber  = $xml->transactionResponse[0]->accountNumber;
			//Added other transaction info by Vipul at 8Dec2017
			$responseCode  = $xml->transactionResponseadditional [0]->responseCode; //responseCode
			$cavvResultCode = $xml->transactionResponse[0]->cavvResultCode;//cavvResultCode
			$transHash = $xml->transactionResponse[0]->transHash;//transHash
			$accountType = $xml->transactionResponse[0]->accountType;//accountType
			$refId = $xml->refId;
			$resultCode = $xml->messages->resultCode;
			$messageCode = $xml->messages->message->code;
			$messageText = $xml->messages->message->text;

			$transRespMsgCode = $xml->transactionResponse[0]->messages->message->code;//transRespMsgCode
			$transRespMsgDescription = $xml->transactionResponse[0]->messages->message->description;//transRespMsgDescription
			*/
			$approval_code  = "";
			$avs_result     = "";
			$cvv_result     = "";
			$transaction_id = "M"."-".$locationinfo['id']."-".$pbookNo;
			$accountNumber  = "";
			$responseCode  = "";
			$cavvResultCode = "";
			$transHash = "";
			$accountType = "";
			$refId = "";
			$resultCode = "";
			$messageCode = "";
			$messageText = "";
			$transRespMsgCode = "";
			$transRespMsgDescription = "";
			
			$oderBookingInfo=$_SESSION['bookingInfo'];
			$oderBookingInfo=multi_unique($oderBookingInfo);
			usort($oderBookingInfo, 'sortByOrder1');
			if(isset($_SESSION['promoRApplyTo']) && $_SESSION['promoRApplyTo']!="") {
				$promoDetails2=ftechPromoDetails($_SESSION["promoc"]);
				$usedpromo=0;
				$promoCode=$promoDetails2['promoCode'];
				$discType=$promoDetails2['promoRDiscountType'];
				
				$usedpromo=$promoDetails2['promoCodeUsed'];
				$totalUsedPromo=$usedpromo+1;
				$discAmt=$totEvAmount;
				$data1 = array ('promoCodeUsed' => $totalUsedPromo);
				$db->where ('promoCode', $promoDetails2['promoCode']);
				$db->where ('studioId', $locationinfo['id']);
				$update = $db->update("bb_promocode",$data1);
			
				if($promoDetails2["promoIsApproRequired"]==1) {
					$status=3;
					$stCont="Pending";
				} else {
					$status=0;
					$stCont="Confirmed";
				}
			} 
			else {
				$promoCode="";
				$discType="";
				$discAmt="";
				$status=0;
				$stCont="Confirmed";
			}	

			$netAmt=$totalAmt1;
			if($promoDetails2["promoRApplyTo"]==2) {
				$totalTick=" x ".$cartT;
			} else {
				$totalTick="";
			}

			if(intval($netAmt)<=0) { $netAmt="0.00"; } else { $netAmt=$netAmt; }
			$bookingInfoData = array ('cOrderNo' => @$bookingNo,'eventId' => $eventinfo['id'],'boEventName' => $db->escape($eventinfo['evTitle']),'boEventDate' => $eventinfo['evDate'],'eventStTime' => $eventinfo['evStTime'],'eventEndTime' => $eventinfo['evEndTime'],'eventLocationId' => $locationinfo['id'],'boProjects' => $noofProject,'boFullName' => $_POST['namecard'],'boPhone' => $_POST['cphone'],'boEmail' => filter_var($_POST['cemail'], FILTER_SANITIZE_EMAIL),'transactionId' => (string)$transaction_id,'promocode' => @$promoCode,'discType' => @$discType,'discAmt' => @$discAmt,'totalTickets' => @$noofProject,'isTax' => @$istax,'tax' => @$taxAmt,'totalAmt' =>@$netAmt,'status' => @$status,'bookingCreationDate' => date('Y-m-d H:i:s'),'ipaddr' => get_ip_address(),'isMobile' =>$ismobile,'deviceName' =>$devicename);

			/*print_r($bookingInfoData);
			die();*/
			$insertBookId = $db->insert("bb_booking",$bookingInfoData);
			/*echo $insertBookId;
			echo "Last executed query was ". $db->getLastQuery();
			die();*/
			
			$availableSeats=0;
			$remainingTicket=0;	
			if($insertBookId){
				if(intval($eventinfo['evNoOfTickets'])>intval($eventinfo['evBookTicket'])) {
					// $eventinfo['id'];
					$remainingTicket=$eventinfo['evBookTicket']+$noofProject;
					$data1 = array ('evBookTicket' => $remainingTicket);
					$db->where ('id', $eventinfo['id']);
					$update = $db->update("bb_event",$data1);
					$availableSeats=$eventinfo['evNoOfTickets']-$remainingTicket;	
				}
			
				$orderinserted = "order is inserted ". $db->getLastQuery();
				foreach($cartitems as $key=>$valc) {
					if($valc['pid']!=0) {
						$bookingInfoTicketsData = array ('bookingId' => $insertBookId,'boTicketName' => $valc['evTicketName'],'boTicketPrice' => $valc['evTicketPrice'],'boProjectId' => $valc['pid'],'boTicketProjectName' => $valc['proName'],'boTicketSku' => $valc['proSku'],'boTicketfullname' => $oderBookingInfo[$key][$valc['oid']]['fullname'],'boTicketEmailAddr' => $oderBookingInfo[$key][$valc['oid']]['emailaddr'],'boTicketSitBy' => $oderBookingInfo[$key][$valc['oid']]['siteby'],'boTicketProjectSize' => $valc['sizename'],'boTicketProjectImage' => $valc['proGalImg']);
						$insertBookTId = $db->insert("bb_booktcktinfo",$bookingInfoTicketsData);
						$cRev = $db->rawQuery("select * from bb_propersoptions where proId='".$valc['pid']."' order by id ", array(''));
						if(count($cRev)>0){
							foreach($cRev as $key=>$item){
								//$bookingInfoTicketsPersonalData = array ('bookingId' => $insertBookId,'bookingTicketId' => $insertBookTId,'boPersOptName' => ucwords($item['proLabel']),'boPersOptVal' => $valc[str_replace(" ","_",$item['proLabel'])],'boPersOptType' => $item['proOptionVal'],'boPersOptHoverTxt' => $item['proHoverTxt'],'boPersOptTypeFormat' => $item['proOptVal']);
								$proLabel = StringEscapeSlug($item['proLabel']);
								$bookingInfoTicketsPersonalData = array ('bookingId' => $insertBookId,'bookingTicketId' => $insertBookTId,'boPersOptName' => ucwords($item['proLabel']),'boPersOptVal' => $valc[$proLabel],'boPersOptType' => $item['proOptionVal'],'boPersOptHoverTxt' => $item['proHoverTxt'],'boPersOptTypeFormat' => $item['proOptVal']);
								//print_r($bookingInfoTicketsPersonalData);
								//die();

								$insertBookTPId = $db->insert("bb_booktcktpersinfo",$bookingInfoTicketsPersonalData);
								//echo $db->getLastError();
								//echo "Last executed query was ". $db->getLastQuery();
								// die();
							}
						}
					}
				}
 
				$orderTransInfo = array (
					'bookingId' => $insertBookId,
					'ptype' => '4', //manual booking for zero payment
					'cardholderName' => isset($_POST['namecard'])?$db->escape(StringInputCleaner($_POST['namecard'])):"",
					'caddress' => isset($_POST['caddress'])?$db->escape(StringInputCleaner($_POST['caddress'])):"",
					'ccity' => isset($_POST['ccity'])?$db->escape(StringInputCleaner($_POST['ccity'])):"",
					'cstate' => isset($_POST['cstate'])?$db->escape(StringInputCleaner($_POST['cstate'])):"",
					'ccountry' => isset($_POST['ccountry'])?$db->escape(StringInputCleaner($_POST['ccountry'])):"",
					'czip' => isset($_POST['czip'])?$_POST['czip']:"",
					'cphone' => isset($_POST['cphone'])?$_POST['cphone']:"",
					'cemail' => isset($_POST['cemail'])?$_POST['cemail']:"",
					'bookedAmount' =>$netAmt,
					'paymentStatus' => 'Completed',
					'bookDate' =>date('Y-m-d H:i:s'),
					'refId' => (string)$refId,
					'resultCode' => (string)$resultCode,
					'messageCode' => (string)$messageCode,
					'messageText' => (string)$messageText,
					'responseCode' => (string)$responseCode,
					'authCode' => (string)$approval_code,
					'avsResultCode' => (string)$avs_result,
					'cvvResultCode' => (string)$cvv_result,
					'cavvResultCode' => (string)$cavvResultCode,
					'transactionId' => (string)$transaction_id,
					'transHash' => (string)$transHash,
					'accountNumber' => (string)$accountNumber,
					'accountType' =>(string)$accountType,
					'transRespMsgCode' =>(string)$transRespMsgCode,
					'transRespMsgDescription' =>(string)$transRespMsgDescription,
				);
				//print_r($orderTransInfo);
				//die();
				$insertBookTransactions = $db->insert("bb_transactiondetails",$orderTransInfo);
				
				/*if($insertBookTransactions){
					echo $insertBookTransactions;
					echo "trans-Last executed query was ". $db->getLastQuery();
				} else{
					echo "issue=".$db->getLastError();
				}
				die();*/
				/*echo $db->getLastError();
				echo "Last executed query was ". $db->getLastQuery();
				die();*/
				
				$address="";
				if(isset($locationinfo['stStreetAddress']) && $locationinfo['stStreetAddress']!="") {
					$address.="".$locationinfo['stStreetAddress'];
				}
				if(isset($locationinfo['stAddress1']) && $locationinfo['stAddress1']!="") {
					$address.=", ".$locationinfo['stAddress1'];
				}
				if(isset($locationinfo['stCity']) && $locationinfo['stCity']!="") {
					$address.=", ".$locationinfo['stCity'];
				}
				if(isset($locationinfo['stState']) && $locationinfo['stState']!="") {
					$address.=" ".$locationinfo['stState']."";
				}
				if(isset($locationinfo['stZip']) && $locationinfo['stZip']!="") {
					$address.=" ".$locationinfo['stZip']."";
				}
				/*if(isset($locationinfo['stPhone']) && $locationinfo['stPhone']!="") {
				$address.="<p> ".$locationinfo['stPhone']."</p>";
				}*/
				$addr = $address;
				if( strpos($_POST["namecard"], ' ') !== false ) {
					$custName1=explode(" ",$_POST['namecard']);
					$custName=$custName1[0];
				} else {
					$custName=$_POST['namecard'];
				}
				$to = $_POST['cemail'];
				  
				if($noofProject>1) $rseats="s"; else $rseats="";
				if($remainingTicket>1) { 
					$nseats="s"; 
					$helpingVerbForRemainingSeats = "are";
				} else if($remainingTicket==1) { 
					$nseats=""; 
					$helpingVerbForRemainingSeats = "is";
				} else { 
					$nseats=""; 
					$helpingVerbForRemainingSeats = "are";
				}

				$subject = 'Board & Brush '.$locationinfo["stName"].' Reservation '.$stCont.' - Seat'.$rseats.': '.$noofProject.' ID: '.$bookingNo.'';
				$message = '
					<html>
						<head>
							<title>Board & Brush</title>
							
							<style>
								body{ background-color:#f1f1f1; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333; font-size:15px;}
								p,h1,h2,h3,h4,h5,h6{ margin:0; padding:0}
								.email-main{ max-width:680px; margin:auto;}
								.sizeX {font-size: 16px;}
							</style>
							
						</head>
					   <body>

						<div class="email-main" style="max-width:680px; margin:auto;">
						<table width="98%" cellpadding="0" cellspacing="0" border="0" style="padding:8px 1% 8px 1%; background-color:#eaeaea; width:680px; max-width:680px;">
						<tr>
						<td align="center" style="background-color:#FFFFFF; padding-top:15px; padding-bottom:15px;"><img width="300" src="'.base_url_images.'boardandbrush-logo.png" /></td>
						</tr>

						<tr>
						<td align="center" style="background-color:#FFFFFF;"><img width="97%" src="'.base_url_images.'hr-line-email.jpg" /></td>
						</tr>

						<tr>
						<td style="background-color:#FFFFFF;" height="10"></td></tr>
						<tr>

						<!--
						<tr>
						<td  style="background-color:#FFFFFF;" align="center"><h2 style=" background-color:#FFFFFF; font-size:32px; font-weight:normal; color:#6ECD00;">Booking Summary</h2>
						</td>
						</tr>
						-->
						<tr>
						<td style="background-color:#FFFFFF;">

						<table style=" width:100%; padding-top:15px; padding-bottom:15px; padding-left:2%; padding-right:2%;">
						<tr>
						<td valign="top" colspan="3">
						Hello '.$custName.',<br />
						';

					if($status==3) {
						$message.= 'Your reservation at   '.$locationinfo["stName"].' Board & Brush, '.$locationinfo["stState"].' Studio is currently pending.';
					} else {
						$message.= 'You have successfully reserved '.$noofProject.' seat'.$rseats.' at the Board & Brush '.$locationinfo["stName"].', '.$locationinfo["stState"].' Studio!';
					}


					if( strpos($eventinfo['evStTime'], ':00 ') !== false ) {
						$evST=str_replace(':00 ','',$eventinfo['evStTime']);
					} else {
						$evST=$eventinfo['evStTime'];
					}

					if( strpos($eventinfo['evEndTime'], ':00 ') !== false ) {
						$evET=str_replace(':00 ','',$eventinfo['evEndTime']);
					} else {
						$evET=$eventinfo['evEndTime'];
					}
					$netAmt = number_format($netAmt,2);

					$message.='<br><br>
					<b>IMPORTANT:</b><br />
					If your sign selection and personalized information below is not correct or missing, we cannot prep your materials for the workshop. Please  <a href="mailto:'.$locationinfo["stEmail"].'">email us ASAP</a> with your information if you have not done so at time of booking.
					 <a href="'.Privacy_Policy_Url.'" target="_new">Board & Brush Policies</a> 
					<br /><br />

					</td></tr>

					<tr><td valign="top" colspan="3" style="font-size:18px;"><b>Summary</b></td></tr>

					<tr>
					<td valign="top" colspan="3">
					<b>BOOKING ID:</b> '.$bookingNo.'<br />
					'.$_POST['namecard'].'<br />
					'.$_POST['cemail'].' |  '.$_POST['cphone'].'<br />
					<br />
					'.$eventinfo['evTitle'].'<br>
					'.date('m-d-Y',strtotime($eventinfo['evDate'])).' &ndash; '.ltrim($evST,0).' - '.ltrim($evET,0).'<br>
					'.$locationinfo["stName"].', '.$locationinfo["stState"].'<Br>
					'.$addr.'
					<br><br>
					<b>Amount Paid:</b> '.SYMBOL.$netAmt.'<br><Br>

					<!--<b>Booking Item Detail:</b>-->
					</td>
					</tr>

					<tr>
					<td valign="top" colspan="3">

					<table width="100%" cellpadding="0" cellspacing="0" border="0">';
					foreach($cartitems as $key=>$valc2) {
						if($valc2['pid']!=0) {
							//Get Project Width,Height,Depth
							$valc2['sizename'] = ftechProjectDimension($valc2['pid']);
						
							$pim=dir_url."projects/700X600/".$valc2["proGalImg"];
							$message.='<tr style="background:url('.$pim.'); background-repeat:no-repeat; background-size:100%; background-position:center;">
							<td valign="top"  style="position:relative;  width:100%; height:25px; background-repeat:no-repeat; background-size:100%; background-position:center; padding-top:15px; padding-bottom:15px; padding-left:10px; font-size:25px; line-height:25px; text-transform: capitalize; color:#fff; background-color: rgba(0, 0, 0, .5);">'.$valc2['proName'];

							$message.=!empty($valc2['sizename'])?"<br><span style='font-size:20px;'>".$valc2['sizename']:"".'</span><Br>';
							$message.='</span></td>
							</tr>

							<tr>
							<td valign="top" style="width:100%; border:2px solid #4E4E4E; border-top:none; padding-bottom:10px; padding-top:10px; padding-left:10px; padding-right:10px;">
							<span style="font-size:22px; color:#4E4E4E;">'.$oderBookingInfo[$key][$valc2['oid']]['fullname'].'</span><br>
							<span style="font-size:17px; color:#4E4E4E;">'.$oderBookingInfo[$key][$valc2['oid']]['emailaddr'].'</span><br />';
							$personaliz="";
							$personalizB="";
							$cRev = $db->rawQuery("select * from bb_propersoptions where proId='".$valc2['pid']."' order by id ", array(''));
							if(count($cRev)>0){
								$p=1;
								//$personaliz.='<br />';
								if(isset($cRev) && !empty($cRev)){
									$personalizB="<Br>";
									foreach($cRev as $key=>$item){
										//$personaliz.=ucwords($item['proLabel']).' <span style="font-size:22px; color:#4E4E4E;">'.$valc2[str_replace(" ","_",$item['proLabel'])].'</span> ';
										$proLabel = StringEscapeSlug($item['proLabel']);
										$personaliz.=ucwords($item['proLabel']).' <span style="font-size:22px; color:#4E4E4E;">'.$valc2[$proLabel].'</span> ';
										if($p%2==0) {
										$personaliz.="<Br>";
										}
										$p++;
									}
								}
							} else {
								$personalizB="";
							}
							$message.=	$personaliz;		
							$message.=(isset($oderBookingInfo[$key][$valc2['oid']]['siteby']) && $oderBookingInfo[$key][$valc2['oid']]['siteby']!="")?$personalizB.'sit by <span style="font-size:22px; color:#4E4E4E;">'.$oderBookingInfo[$key][$valc2['oid']]['siteby']:"";
							$message.='</tr>
							<tr><td height="10"></td></tr>';
						}
					}
					if(isset($_SESSION['promoRApplyTo']) && $_SESSION['promoRApplyTo']!="") {
						if($promoDetails2["promoRApplyTo"]==2) {
							if($promoDetails2["promoRDiscountType"]==1) {
								$disV=SYMBOL.$promoDetails2["promoRDiscountVal"];
							} else {
								$disV=$promoDetails2["promoRDiscountVal"].'%';
							}
						} else {
							$disV=SYMBOL.number_format($totEvAmount,2);
						}
						$message.='<tr>
						<td style="width:100%; border:2px solid #4E4E4E; padding-bottom:10px; padding-top:10px; padding-left:10px; padding-right:10px;">
						Coupon - '.$disV.' ('.$_SESSION["promoc"].') '.$totalTick.' </span>
						</td>
						</tr>';
					}

					if(isset($locationinfo["stFbLink"]) && $locationinfo["stFbLink"]!=""){
						$stFbLink = $locationinfo["stFbLink"];
					} else {
						$stFbLink = "www.facebook.com/boardandbrushcorporate/";
					}
					if(isset($locationinfo["stInstaLink"]) && $locationinfo["stInstaLink"]!=""){
						$stInstaLink = $locationinfo["stInstaLink"];
					} else {
						$stInstaLink = "www.instagram.com/explore/locations/1042276229137658/board-brush-creative-studio-corporate-site/";
					}
					if(isset($locationinfo["stTwtLink"]) && $locationinfo["stTwtLink"]!=""){
						$stTwtLink = $locationinfo["stTwtLink"];
					} else {
						$stTwtLink = "www.twitter.com/boardandbrushcs/";
					}
					if(isset($locationinfo["stPinLink"]) && $locationinfo["stPinLink"]!=""){
						$stPinLink = $locationinfo["stPinLink"];
					} else {
						$stPinLink = "www.pinterest.com/boardandbrush/";
					}

					if(isset($locationinfo['stLocPolicies']) && $locationinfo['stLocPolicies']!="") {
						$stLocPolicies = "<em>".html_entity_decode($locationinfo['stLocPolicies'])."</em><Br>";
					}

					if($availableSeats>1) { 
						$helpingVerbForAvailableSeats = "are";
					} else if($availableSeats==1) { 
						$helpingVerbForAvailableSeats = "is";
					} else { 
						$helpingVerbForAvailableSeats = "are"; 
					}

					$message.='</table>

					</td>
					</tr>
					<tr>
					<td colspan="3">
					Now there '.$helpingVerbForRemainingSeats.' '.$remainingTicket.' seat'.$nseats.' reserved including yours, '.$availableSeats.' '.$helpingVerbForAvailableSeats.' still available.
					<br /><br />
					We look forward to seeing you soon!  Be sure to tell your friends!
					<br><br>
					Got Wood?!
					</td>
					</tr>
					<tr>
					<td colspan="3">
					-------------------------------------------------------------------------------<br />
					Board & Brush - '.$locationinfo['stName'].' <Br>
					'.$locationinfo['stStreetAddress'].' | '.$locationinfo['stPhone'].'<br>
					'.$locationinfo['stEmail'].'<br>
					<a href="'.live_site_mail_link_url.$locationinfo['slug'].'">'.live_site_mail_link_display.$locationinfo['slug'].'</a><Br>
					'.$stLocPolicies.'
					

					<a href="'.$stFbLink.'"><img src="'.base_url_site.'images/fb1.png" style="border:none;width:34px;"></a>&nbsp;&nbsp;<a href="'.$stInstaLink.'"><img src="'.base_url_site.'images/inst1.png" style="border:none;width:34px;"></a>&nbsp;&nbsp;<a href="'.$stTwtLink.'"><img src="'.base_url_site.'images/twt1.png" style="border:none;width:34px;" ></a>&nbsp;&nbsp;<a href="'.$stPinLink.'"><img src="'.base_url_site.'images/pin1.png" style="border:none;width:34px;"></a>
					</td>
					</tr>

					</table>

					</td>
					</tr>
					</table>
					<table width="98%" cellpadding="0" cellspacing="0" border="0" style="padding:0 1%; margin-top:10px; margin-bottom:10px;">
					<tr>
					<td align="center">Copyrighted Board & Brush, LLC. All Rights Reserved &copy; '.date('Y').'  </td>
					</tr>
					</table>

					</div>

					</body>
					</html>'; 
					
					if( strpos($locationinfo['stName'], ',') !== false ) {
						$stN1=explode(',',$locationinfo['stName']);
						$stN=$stN1[0];
					} else {
						$stN=$locationinfo['stName'];
					}
 
					//$stName = $stN."  Board & Brush";
					$stName = $stN;
					$stEmail=$locationinfo['stEmail'];
						
					/*echo $message;
					die("sdfsdfsdf3223");*/
					$headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
					//$headers .= "From: ".$stName." <".$stEmail."> \r\n";
					$headers .= "From: ".$stName." ".From_Name." <".From_Email."> \r\n";
					$headers .= "Reply-To: ".$stName." ".From_Name." <".$stEmail."> \r\n";
					$header .= "Content-type: text/html\r\n";

					$headers1  = "Content-type: text/html; charset=utf-8 \r\n"; 
					//$headers1 .= "From: ".$custName." <".$to."> \r\n";
					$headers1 .= "From: ".$stName." ".From_Name." <".From_Email."> \r\n";
					$headers1 .= "Reply-To: ".$custName." <".$to."> \r\n";
					$header1 .= "Content-type: text/html\r\n";

					
					mail($to,$subject,$message,$headers); //send mail to customer
					mail($locationinfo['stEmail'],$subject,$message,$headers1);	 //send mail to studio				
							
					//die("sdfasdf");
					//Cookie Data deleted
				if(isset($_COOKIE['csid']) && $_COOKIE['csid'] != ""){
					$eventId1 = $eventinfo['id'];
					$cokieId = $_COOKIE['csid'];
					$isExist = isExistEventInCronTbl($eventId1,$cokieId);
					if($isExist) { //delete data in bb_crondata tbl
						$db->where('eventId', $eventId1);
						$db->where('cookieId',$cokieId );
						$db->delete("bb_crondata");
					}
				}
			/*}
			else {
				$orderinserted = "order is not inserted ".$db->getLastError();
			}*/		
            
			$errorMsg['result']=0;
			$errorMsg['msg']= "Success";
			//$errorMsg['msg']= $xml->transactionResponse->messages->message->description;
			$errorMsg['bookingNo']=$bookingNo;
			$errorMsg['orderinserted'] = $orderinserted;

			unset($_SESSION["currentOrder"]);
			unset($_SESSION["projectId"]);
			unset($_SESSION['bookingInfo']);	
			unset($_SESSION['promoc']);
			unset($_SESSION['promoRApplyTo']);
			unset($_SESSION['promoRDiscountType']);
			unset($_SESSION['eventAssociated']);
			$_SESSION["blocId"]=$_SESSION['studioInfo'][0]['locationId'];
			$_SESSION["bNo"]=$bookingNo;
		} else { 
			$errorMsg['result']=1;
			$errorMsg['msg']= "Not Success";
			//$errorMsg['msg']=(string)$xml->transactionResponse->errors->error->errorText;
			$_SESSION["ermsg"]=1;
		}
		//die("sdfsdf");
		//echo json_encode($errorMsg);
	//}
//}
} // ( total seats > booked seats )
else  // (If total seats <= booked seats )
{  
	$errorMsg['result']=1;
	$errorMsg['msg']= "Event has no seat.You can't book anymore.";
}
echo json_encode($errorMsg);
}
?>