<?php
include("../includes/functions.php");
global $db;
$params = array('');
$stWhere="";
if(isset($_REQUEST["txtSearch"]) && $_REQUEST["txtSearch"]!="") {
$q=$_REQUEST["txtSearch"];
if(intval($q)){
$stWhere=" and stZip=".$q;
} else {
$stWhere=" and stCity like '%".$q."%'";
}
}
$getLocations = $db->rawQuery("SELECT * FROM bb_studiolocation where isDeleted=0 and stStatus<>1  $stWhere", $params);
?>
  <script type="text/javascript">

jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyDF2l9JmpBPo_y0JdK8ZA-rxOZ9cudCb-g&callback=initialize";
    document.body.appendChild(script);
});

function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
		center: new google.maps.LatLng(37.090240, -95.712891),
	zoom: 0,
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers
    var markers = [
	<?php /* For Each Location Create a Marker. */
if(isset($getLocations[0]['lat']) && isset($getLocations[0]['lng'])) {			
foreach( $getLocations as $k=>$locationm ){
if(($locationm['lat']!="" && $locationm['lng']!="") && $locationm['isusa']==1) {
$address="";
if(isset($locationm['stStreetAddress']) && $locationm['stStreetAddress']!="") {
$address.=removeEXtraSpace($locationm['stStreetAddress']);
}
if(isset($locationm['stAddress1']) && $locationm['stAddress1']!="") {
$address.=" ".removeEXtraSpace($locationm['stAddress1']);
}
if(isset($locationm['stCity']) && $locationm['stCity']!="") {
$address.=",".removeEXtraSpace($locationm['stCity']);
}
if(isset($locationm['stState']) && $locationm['stState']!="") {
$address.=",".removeEXtraSpace($locationm['stState']);
}
if(isset($locationm['stZip']) && $locationm['stZip']!="") {
$address.=" ".removeEXtraSpace($locationm['stZip']);
}
$addr = $address;
$map_lat = $locationm['lat'];
$map_lng = $locationm['lng'];
$locationId = $locationm['id'];
?>
        ['<?php echo $addr?>', <?php echo $map_lat?>,<?php echo $map_lng?>],
<?php  } } 

} else {?>
//['Board & Brush',37.090240, -95.712891]
		<?php }
		//end foreach locations ?>      	
    ];
                        
    // Info Window Content
    var infoWindowContent = [
	<?php /* For Each Location Create a Marker. */
if(isset($getLocations[0]['lat']) && isset($getLocations[0]['lng'])) {		
foreach( $getLocations as $k=>$locationm1 ){
if(($locationm1['lat']!="" && $locationm1['lng']!="") &&  $locationm1['isusa']==1) {
$address="";
if(isset($locationm1['stStreetAddress']) && $locationm1['stStreetAddress']!="") {
$address.=removeEXtraSpace($locationm1['stStreetAddress']);
}
if(isset($locationm1['stAddress1']) && $locationm1['stAddress1']!="") {
$address.=" ".removeEXtraSpace($locationm1['stAddress1'])."<Br>";
}
if(isset($locationm1['stCity']) && $locationm1['stCity']!="") {
$address.="".removeEXtraSpace($locationm1['stCity']);
}
if(isset($locationm1['stState']) && $locationm1['stState']!="") {
$address.=",".removeEXtraSpace($locationm1['stState']);
}
if(isset($locationm1['stZip']) && $locationm1['stZip']!="") {
$address.=" ".removeEXtraSpace($locationm1['stZip']);
}
if(isset($locationm1['stPhone']) && $locationm1['stPhone']!="") {
$address.="<Br>Phone ".removeEXtraSpace($locationm1['stPhone']);
}
if($locationm1['stStatus']==2){
$link="javascript:void(0);";
$studioStatus="<p><strong>Comming Soon</strong></p>";
} else if($locationm1['stStatus']==3 || $locationm1['stStatus']==4){
$link=base_url_site.$locationm1['slug'];
$studioStatus="";
} else {
$studioStatus="";
$link="javascript:void(0);";
}
$addr = $address;
$map_lat = $locationm1['lat'];
$studioName = $locationm1['stName'];
$map_lng = $locationm1['lng'];
$locationId = $locationm1['id'];?>	
        ['<div class="info_content">' +
        '<h3><?php echo $studioName?> Board & Brush</h3>' +
        '<p><?php echo $addr?></p><p><?php echo $studioStatus?></p>' +        '</div>'],
 <?php  } } 
} else {?>
	//['<div class="info_content"><h3>Board & Brush</h3></div>'],	

		<?php }
		//end foreach locations ?>          
    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(11);
        google.maps.event.removeListener(boundsListener);
    });
    
}


  </script> 





