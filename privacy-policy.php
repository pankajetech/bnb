<?php include("includes/header2_inc.php");

?>
<!-- Header end-->

<!-- Middle section start-->





<div class="middle-section-full">
<div class="middle-section-mid">
<div class="middle-section">

<!-- Policy html start -->
<h2 class="page-heading lakesight-font policyhead">Policies</h2>
<div class="policytxt">

<p><strong>Registration<br>
</strong>Project selection and customization information for your project is required at the time of booking on our registration form.&nbsp; Please be sure to double check your spelling and information to ensure your project stencil and materials will be ready for you at the start of the workshop.</p>
<p><strong>Workshop Cancellation Policy</strong><br>
Due to the custom nature of our studio, registration fees are non-refundable.&nbsp; Registration cancellations made at least 72 hours in advance of a workshop will be granted a credit for a future workshop. The credit is valid for up to one year from original scheduled workshop.</p>
<p>Due to staffing, we reserve the right to cancel workshops if attendance is below 4 attendees.</p>
<p><strong>Private Party Cancellation Policy</strong><br>
A deposit is due when booking your private party.&nbsp; Once the minimum number of participants is met, we will refund your initial deposit.&nbsp; If you are unable to meet the minimum, the deposit will be forfeited.&nbsp; If you choose to cancel your private party the deposit will not be refunded due to the nature of our studio and the demand for private party times on our calendar.</p>
<p>For private parties, we require all attendees to register and provide complete customization information for their project at least 72 hours in advance so we can prepare the materials for your party.</p>
<p><strong>Project Change Request</strong><br>
If you would like to make changes to your project within 72 hours of your workshop, we would be happy to accommodate your request for a $10 stencil recut fee, payable upon the start of your workshop. Changes made prior to 72 hours are done free of charge.</p>
<p><strong>Age Restriction</strong><br>
Due to the tools used, the stain, and our liquor license, infants and children under the age of 16 are not permitted in any of our Studios while a workshop is being held. If a participant brings an infant or child, they will be ask to rebook for a different workshop when they can attend ‘child’ free and enjoy the educational experience fully.</p>
<p>For children under the age of 16, we do offer the ability to book private children or teens parties! Contact your local studio for more information</p>
<p><strong>Safety Waiver</strong><br>
I understand that I will use and/or be exposed to drills, hammers, mallets, nails, sand paper, paint and low VOC oil based stains during Board and Brush workshops.&nbsp;&nbsp;I agree to be responsible for any damages to Board and Brush’s tools, equipment or facilities, to the extent and in the percentage I cause such damages.</p>
<p>I recognize there may be certain inherent risks in participating in Board and Brush workshops.&nbsp;&nbsp;I agree to take precautions and&nbsp;observe all Board and Brush policies, recommendations, directions, rules and warnings; whether provided orally or in writing. I assume full responsibility for personal injury to myself and my personal belongings.</p>
<p>I further agree to release and discharge Board and Brush for any injury, loss or damage that may arise out of my participation in Board and Brush workshops or my presence in Board and Brush facilities whether caused by me, Board and Brush or any third party.</p>
<p>I agree to indemnify and defend Board and Brush against all claims, causes of action, damages, judgments costs or expenses including reasonable attorney’s fees and other litigation costs which may in any way arise from my participation in Board and Brush workshops or my presence in Board and Brush facilities.</p>
<p>We love sharing photos of the beautiful work you do through social media and print advertising! If you would prefer not to share your photo, please opt out of the picture taking at the beginning of the workshop.</p>
<p>I HAVE READ THE BOARD AND BRUSH POLICIES AND THIS SAFETY WAIVER AND UNDERSTAND THEM.&nbsp; I FURTHER UNDERSTAND THAT BY CHECKING THE “ACCEPT POLICIES” BOX, I VOLUNTARILY SURRENDER CERTAIN LEGAL RIGHTS THAT I MIGHT OTHERWISE HAVE.</p>

<div class="policybotm">
<p style="text-align: center;"><b>We hope you love our creative studio, enjoyed your experience and our beautiful designs.&nbsp;&nbsp;</b></p>
<p style="text-align: center;"><b>Please be aware that these are all offered by Board &amp; Brush for the enjoyment of our customers, are protected by copyright and federal law, and any unauthorized use or copying without permission from Board &amp; Brush is expressly prohibited.</b></p>

</div>
</div>

<!-- Policy html end -->


</div>
</div>
</div>

<!-- Middle section end-->


<!-- Footer section start-->

<?php
include("includes/footer-event_inc.php");
?>