<?php include("includes/header_inc.php");
unset($_SESSION["currentOrder"]);
unset($_SESSION["projectId"]);
unset($_SESSION['bookingInfo']);	
unset($_SESSION['promoc']);
unset($_SESSION['promoRApplyTo']);
unset($_SESSION['promoRDiscountType']);
unset($_SESSION['eventAssociated']);
?>

<script>
$(document).ready(function(){
    $(".header").addClass("header404");
        
});
</script> 


<!-- Middle section start-->


<div class="middle-section-full">
<div class="middle-section-mid">
<div class="middle-section">

<div class="wrapper404">
<span class="not-found-title">WHOOPS!</span>
<span class="not-found-subtitle">It looks like you are Lost!
<br /><br />
We are eager to see you embrace your inner DIY master!</span>
<p class="not-found-searchfor"><a href="<?php echo base_url_site?>"><span>Search for another Studio Location and Book a Class!</span></a></p>

</div>

</div>
</div>
</div>

<!-- Middle section end-->




<?php
include("includes/footer_inc.php");
?>