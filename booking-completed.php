<?php ob_start();
//this i sjust to test to move updated code on server
include("includes/header2_inc.php");?>
<!-- Header      end-->

<!-- Middle section start-->

<?php
if(empty($_SESSION["bNo"])) {

$bookingInfoD1 = $db->rawQueryOne ("SELECT stlo.slug FROM `bb_booking` as bbi inner join bb_studiolocation as stlo on stlo.id=bbi.eventLocationId  where cOrderNo=? ", array($_REQUEST["bookingNo"]));
$stlslug=$bookingInfoD1['slug'];
if(isset($stlslug) && $stlslug!="") {
$stuioslug=$stlslug;
} else {
 $stuioslug=$_SESSION["studioLocaId"];
}

header("location:".base_url_site.$stuioslug);
exit;
}
//print_r($_SESSION);
if(isset($_REQUEST["bookingNo"]) && $_REQUEST["bookingNo"]!="") {
$bookingInfoD = $db->rawQueryOne ("SELECT * FROM `bb_booking` where cOrderNo=? ", array($_REQUEST["bookingNo"]));


$locationinfo=getLocationInfobyID($bookingInfoD['eventLocationId']);
if(isset($locationinfo) && !empty($locationinfo)) {
$evDay=date('d',strtotime($bookingInfoD['boEventDate']));
$evMonth=date('F',strtotime($bookingInfoD['boEventDate']));
$evWeekDay=date('D',strtotime($bookingInfoD['boEventDate']));
$evStTime=str_replace(":00 ","",$bookingInfoD['eventStTime']);
$evEndTime=str_replace(":00","",$bookingInfoD['eventEndTime']);

}
}
?>



<div class="middle-section-full">
<div class="middle-section-mid">
<div class="middle-section">

<div class="booking-section">

<div class="booking-completed">
<h1 class="lakesight-font">Booking Complete</h1>
<div class="slash-line"></div>
<h2 class="payment-successful"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="50px" height="50px" viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve"><g><circle fill="none" stroke="#6fcf00" stroke-width="2" stroke-miterlimit="10" cx="24.998" cy="25" r="24.001"/><path fill="#6fcf00" d="M40,16.649L22.15,34.972c0,0-0.816,0.787-1.608,0.787c-1.042,0-2.047-0.781-2.047-0.781l-8.862-7.328V24 l10.778,9L40,13V16.649z"/></g></svg> <span>Payment Successful</span></h2>

<div class="booking-total-complete">
<p class="totalcharge">total<br /> charged</p>
<p class="totalamt"><span class="dolr"><?php echo SYMBOL;?></span><?php
$totamt=explode(".",number_format($bookingInfoD['totalAmt'],2));



 echo $totamt[0];?><span class="decml"><?php echo $totamt[1];?></span></p>
<?php if(isset($bookingInfoD['tax']) && $bookingInfoD['tax']!="0.00") {?>
<p class="inctax">(<?php echo SYMBOL.number_format($bookingInfoD['tax'],2);?> tax included)</p>
<?php }?>
</div>



<div class="payment-content">
<span>We look forward to seeing you!</span> <br /> Please be sure your details are correct. If you have any questions<br> <a href="<?php echo base_url_site.$locationinfo['slug'];?>#contact">Contact us</a>
</div>
<div class="bookingid">Booking ID: <?php echo $_REQUEST["bookingNo"]; ?></div>
<div class="booking-detail">


<div class="personal-datetime">
<div class="personal-date"><p><?php echo $evMonth; ?></p><?php echo $evDay; ?></div>
<div class="personal-day"><?php echo $locationinfo['stName'].", ".$locationinfo['stState'];?><br /> <?php echo $evWeekDay; ?> <?php echo ltrim($evStTime, '0'); ?> - <?php echo ltrim($evEndTime, '0'); ?></div>
</div>

<?php

$bookingTInfoD = $db->rawQuery("SELECT * FROM `bb_booktcktinfo` where bookingId='".$bookingInfoD['id']."' ", array(''));
$u=1;
$tcart=count($bookingTInfoD);
foreach($bookingTInfoD as $key=>$bval) {
	//Get Project Width,Height,Depth
	$bval['boTicketProjectSize'] = ftechProjectDimension($bval['boProjectId']);
?>

<div class="review-cost" style="background-image:url(<?php echo dir_url;?>projects/<?php echo $bval['boTicketProjectImage'];?>); background-position:center; background-repeat:no-repeat;">

<div class="choose-project">
<div class="proleft">

<span><?php echo $bval['boTicketProjectName'];?> <?php echo !empty($bval['boTicketProjectSize'])?"<br><span class='projectsize'>".$bval['boTicketProjectSize']:"";?></span></span>
</div>
<div class="costfor-user">
<div class="workshop-cost comp-cost">
<p>C o s t</p>
<span><?php echo SYMBOL;?></span><?php echo number_format($bval['boTicketPrice'],0);?>

</div>
</div>
</div>

<div class="bg-transperent"></div>

</div>

<div class="user-info">
<span class="uinfo1"> <?php echo $bval['boTicketfullname'];?></span><br>
<span class="uinfo2" style="font-size:19px"> <?php echo $bval['boTicketEmailAddr'];?></span>
<?php
$cRev = $db->rawQuery("select * from bb_booktcktpersinfo where bookingId='".$bookingInfoD['id']."' and bookingTicketId='".$bval['id']."' order by id ", array(''));
if(count($cRev)>0){
	$p=1;
	echo '<br />';
	foreach($cRev as $key=>$item){
		//echo "+++".$item['boPersOptName']."===".$item['boPersOptVal'];
		echo ucwords($item['boPersOptName']).' <span class="uinfo3">'.$item['boPersOptVal'].'</span> ';
		if($p%2==0) {
			echo "<Br>";
		}
		$p++;
	}
}
?>			

<?php echo (isset($bval['boTicketSitBy']) && $bval['boTicketSitBy']!="")?'<br />sit by <span class="uinfo4">'.$bval['boTicketSitBy']:"";?></span>
<div class="user-edit"></div>
</div>
<?php 
$u++;
}
if(isset($bookingInfoD['promocode']) && $bookingInfoD['promocode']!="") {
$promoDetails=ftechPromoDetails($bookingInfoD['promocode']);
if($promoDetails["promoRApplyTo"]==2) {
$totalTick=" x ".$tcart;
		if($promoDetails["promoRDiscountType"]==2) {
		$promoval=$promoDetails["promoRDiscountVal"].'%';
		} else {
		$promoval=SYMBOL.$promoDetails["promoRDiscountVal"];
		}
} else {
$totalTick="";
	if($promoDetails["promoRDiscountType"]==2) {
		$promoval=$promoDetails["promoRDiscountVal"].'%';
		} else {
		$promoval=SYMBOL.$promoDetails["promoRDiscountVal"];
		}
}

echo '<div class="promo-box"><span>Coupon</span> – '.$promoval.' ('.$bookingInfoD['promocode'].') '.$totalTick.'</div>';
}

if(isset($locationinfo['stFbLink']) && $locationinfo['stFbLink']!="") {
$fblink=$locationinfo['stFbLink'];
} else {
$fblink="javascript:void(0);";
}
if(isset($locationinfo['stInstaLink']) && $locationinfo['stInstaLink']!="") {
$stInstaLink=$locationinfo['stInstaLink'];
} else {
$stInstaLink="javascript:void(0);";
}
?>


<div class="social-media">
<a href="<?php echo $fblink?>" target="_blank"><img src="<?php echo base_url_images?>fb-follow.jpg"/></a> <a href="<?php echo $stInstaLink?>" target="_blank"><img src="<?php echo base_url_images?>flw_inst.png" /></a>
</div>

</div>
</div>
<?php
$address="";
if(isset($locationinfo['stStreetAddress']) && $locationinfo['stStreetAddress']!="") {
$address.="<p>".$locationinfo['stStreetAddress'];
}
if(isset($locationinfo['stAddress1']) && $locationinfo['stAddress1']!="") {
$address.=" <br>".$locationinfo['stAddress1'];
}
if(isset($locationinfo['stCity']) && $locationinfo['stCity']!="") {
$address.=" <br>".$locationinfo['stCity'];
}
if(isset($locationinfo['stState']) && $locationinfo['stState']!="") {
$address.=", ".$locationinfo['stState'];
}
if(isset($locationinfo['stZip']) && $locationinfo['stZip']!="") {
$address.=" ".$locationinfo['stZip']."</p>";
}
if(isset($locationinfo['stPhone']) && $locationinfo['stPhone']!="") {
$address.="<p> ".$locationinfo['stPhone']."</p>";
}
$addr = $address;
?>
<div class="side-bar">
<div class="contact-info">
<h2><?php echo $locationinfo['stName'].", ".$locationinfo['stState'];?></h2>
<?php echo $addr;?>

<?php 
if($locationinfo['stEmail'] != ""){
	$parts=explode('@',$locationinfo['stEmail']);
	$partemail = $parts[0]."@<br/>".$parts[1];
}
?>

<p><a href="mailto:<?php echo $locationinfo['stEmail'];?>"><?php echo $partemail; ?></a></p>
</div>
<div class="sidebar-nav">
<ul>
<li><a href="<?php echo base_url_site.$locationinfo['slug'];?>#Calendar-workshop"><svg class="mk-svg-arrow" data-name="mk-icon-angle-right" data-cacheid="icon-5b62cc00bdc39" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 1792"><path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path></svg>Calendar- sign up for a class!</a></li>
<li><a href="<?php echo base_url_site.$locationinfo['slug'];?>#location"><svg class="mk-svg-arrow" data-name="mk-icon-angle-right" data-cacheid="icon-5b62cc00bdc39" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 1792"><path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path></svg>Studio location</a></li>
<li><a href="<?php echo base_url_site.$locationinfo['slug'];?>#contact"><svg class="mk-svg-arrow" data-name="mk-icon-angle-right" data-cacheid="icon-5b62cc00bdc39" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 1792"><path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path></svg>Contact us</a></li>
<!--<li><a href="#">My cart</a></li>
<li><a href="#">Checkout</a></li>-->
</ul>
</div>
</div>



</div>

</div>
</div>
</div>

<?php 
$address="";
$address1="";
global $addressFooter;
$addressFooter="";
$addressFooter="<p>".$locationinfo['stName']."<Br>---<br/>";
if(isset($locationinfo['stStreetAddress']) && $locationinfo['stStreetAddress']!="") {
$address.="<p>".$locationinfo['stStreetAddress'];
$address1=$locationinfo['stStreetAddress'];
$addressFooter.=$locationinfo['stStreetAddress'];
}
if(isset($locationinfo['stAddress1']) && $locationinfo['stAddress1']!="") {
$address.=" <br>".$locationinfo['stAddress1'];
$address1.=" <br>".$locationinfo['stAddress1'];
$addressFooter.=" <br>".$locationinfo['stAddress1'];
}
if(isset($locationinfo['stCity']) && $locationinfo['stCity']!="") {
$address.=" <br>".$locationinfo['stCity'];
$address1.="<Br> ".$locationinfo['stCity'];
$addressFooter.="<Br> ".$locationinfo['stCity'];
}
if(isset($locationinfo['stState']) && $locationinfo['stState']!="") {
$address.=", ".$locationinfo['stState'];
$address1.=", ".$locationinfo['stState'];
$addressFooter.=", ".$locationinfo['stState'];
}
if(isset($locationinfo['stZip']) && $locationinfo['stZip']!="") {
$address.=" ".$locationinfo['stZip']."</p>";
$address1.=" ".$locationinfo['stZip']."";
$addressFooter.=" ".$locationinfo['stZip']."";
}
if(isset($locationinfo['stPhone']) && $locationinfo['stPhone']!="") {
$address.="<p> ".$locationinfo['stPhone']."</p>";
$phone=$locationinfo['stPhone'];
$addressFooter.="<br/><br/> ".$locationinfo['stPhone'];
}
$addressFooter.="</p>";
$addr = $address;
$addr1 = $address1;
?>


<!-- Middle section end-->


<!-- Footer section start-->
<?php
include("includes/footer-event_inc.php");
unset($_SESSION["stdLocIds"]);
unset($_SESSION["stdEvntIds"]);
unset($_SESSION["studioInfo"]);
unset($_SESSION["locationId"]);
unset($_SESSION["eventId"]);
unset($_SESSION["evID"]);
unset($_SESSION["evTID"]);
unset($_SESSION["blocId"]);
unset($_SESSION["bNo"]);
?>