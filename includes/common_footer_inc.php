<div class="pop_up_overlay" style="display:none;">
	<div class="prvevent-overlay dpg-pad">
		<div class="prvevent-wrapper dpg-pos">
			<h2>Are you still there?</h2>
			<p>Your session is about to expire! <br />Please click continue to finalize your reservation or you may lose<br /> your space and selection to another guest.</p>
			<div class="prvevent-form">
				<form id="prvevt" action="" method="post">
					
					<div class="pdf-row" style="text-align:center;">
					<button type="button" id="prefresh5">CONTINUE <i class="fa fa-angle-right"></i></button>
					</div>
					<div class="pdf-row" style="text-align:center; margin-top:50px; font-size:35px;">
					<h1 id="clockhtag" class="countdown" style="font-weight:normal;"></h1>
                   
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
function removeBookedTicketWRTCronTbl(stdLocIds,stdEvntIds,persCookieId){
	//alert('rmv-cron='+stdLocIds+","+stdEvntIds+","+persCookieId);
	$.ajax({ 
		url : '<?php echo base_url_site;?>addon/removeBookedTicketWRTCronTbl.php',
		type: 'post',
		data: {stdLocIds:stdLocIds,stdEvntIds:stdEvntIds,persCookieId:persCookieId},
		success: function(data) {
			//alert('runclock');
			window.location.href= '<?php echo base_url_site.$locationId."/"?>';
		}
	});
} 
</script>
<?php
$date = new DateTime();
$bookedInfoWRTCOKIEUsr = getBkdTcktForCronWRTCookieUsrP($stdEvntIds,$_COOKIE['csid']);
$createdDate = $bookedInfoWRTCOKIEUsr['createdDate'];
$actualDT1stIns = $bookedInfoWRTCOKIEUsr['actualDT1stIns'];
$date2 = new DateTime($createdDate);
$date3 = new DateTime($actualDT1stIns);
$sesminutes=Session_time_enabled;
$sesTotalminutes=total_session_time;
$sessiontime=strtotime("+ $sesminutes minutes", strtotime($actualDT1stIns));
$sessiontotaltime=strtotime("+ $sesTotalminutes minutes", strtotime($actualDT1stIns));

$diffTotal = $sessiontotaltime - $date->getTimestamp(); 

if($date->getTimestamp()<=$sessiontime) {
	$diff = $date2->getTimestamp() - $date->getTimestamp();
} else { ?>
	<script>
		removeBookedTicketWRTCronTbl('<?php echo $stdLocIds?>','<?php echo $stdEvntIds?>','<?php echo $_COOKIE['csid'];?>');
	</script>
<?php 
} ?>
<script>
var stdLocIds = '<?php echo $stdLocIds;?>'; 
var stdEvntIds = '<?php echo $stdEvntIds;?>';
var persCookieId = '<?php echo $_COOKIE['csid'];?>';
function _timer(callback)
{
    var time = 0;     //  The default time of the timer
    var mode = 0;     //   Mode: count up or count down
    var status = 0;    //    Status: timer is running or stoped
    var timer_id;    //    This is used by setInterval function
    
    // this will start the timer ex. start the timer with 1 second interval timer.start(1000) 
    this.start = function(interval)
    {
        interval = (typeof(interval) !== 'undefined') ? interval : 1000;
 
        if(status == 0)
        {
            status = 1;
            timer_id = setInterval(function()
            {
                switch(mode)
                {
                    default:
                    if(time)
                    {
                        time--;
                        generateTime();
                        if(typeof(callback) === 'function') callback(time);
                    }
                    break;
                    
                    case 1:
                    if(time < 86400)
                    {
                        time++;
                        generateTime();
                        if(typeof(callback) === 'function') callback(time);
                    }
                    break;
                }
            }, interval);
        }
    }
    
    //  Same as the name, this will stop or pause the timer ex. timer.stop()
    this.stop =  function()
    {
        if(status == 1)
        {
            status = 0;
            clearInterval(timer_id);
        }
    }
    
    // Reset the timer to zero or reset it to your own custom time ex. reset to zero second timer.reset(0)
    this.reset =  function(sec)
    {
        sec = (typeof(sec) !== 'undefined') ? sec : 0;
        time = sec;
        generateTime(time);
    }
    
    // Change the mode of the timer, count-up (1) or countdown (0)
    this.mode = function(tmode)
    {
        mode = tmode;
    }
    
    // This methode return the current value of the timer
    this.getTime = function()
    {
        return time;
    }
    
    // This methode return the current mode of the timer count-up (1) or countdown (0)
    this.getMode = function()
    {
        return mode;
    }
    
    // This methode return the status of the timer running (1) or stoped (1)
    this.getStatus
    {
        return status;
    }
    
    // This methode will render the time variable to hour:minute:second format
    function generateTime()
    {
        var second = time % 60;
        var minute = Math.floor(time / 60) % 60;
        var hour = Math.floor(time / 3600) % 60;
        
        second = (second < 10) ? '0'+second : second;
        minute = (minute < 10) ? '0'+minute : minute;
        hour = (hour < 10) ? '0'+hour : hour;
		
		if(second<=60  && minute==1) {
		$('.countdown').show();
		$(".pop_up_overlay").show();
		
		}
		$('.countdown').html(minute  + ':' + second);
        
        $('div.timer span.second').html(second);
        $('div.timer span.minute').html(minute);
		
       // $('div.timer span.hour').html(hour);
    }
}
 

 
// example use
var timer;
 
$(document).ready(function(e) 
{
    timer = new _timer
    (
        function(time)
        {
		
		
		
            if(time == 0)
            {
                timer.stop();
				removeBookedTicketWRTCronTbl(stdLocIds,stdEvntIds,persCookieId);
               
            }
        }
    );


	timer.reset(<?php echo $diff?>);
    timer.start(1000);

<?php /*?>$.ajax({ 
	url : '<?php echo base_url_site;?>addon/getCronInfoWRTCookieUserP.php',
	type: 'post',
	data: {stdLocIds:stdLocIds,stdEvntIds:stdEvntIds,persCookieId:persCookieId},
	success: function(data) {
	var odata = $.parseJSON(data);	
    timer.reset(data.gSeconds);
    timer.start(1000);
	
	}
	});	<?php */?>
	
});


$(document).on("click","#prefresh5",function() {

$.ajax({ 
	url : '<?php echo base_url_site;?>addon/updCronDataForSpecificUserP.php',
	type: 'post',
	data: {stdLocIds:stdLocIds,stdEvntIds:stdEvntIds,persCookieId:persCookieId},
	success: function(data) {
	var odata = $.parseJSON(data);	
	timer.reset(0);
    timer.mode(0);
    timer.reset(odata.gSeconds);
    timer.start(1000);
		$(".pop_up_overlay").hide();
		$('.countdown').hide();
		$('.countdown').html('');
	
	}
});

});




</script>
<script>
function _timer1(callback1)
{
    var time1 = 0;     //  The default time of the timer
    var mode1 = 0;     //   Mode: count up or count down
    var status1 = 0;    //    Status: timer is running or stoped
    var timer_id1;    //    This is used by setInterval function
    
    // this will start the timer ex. start the timer with 1 second interval timer.start(1000) 
    this.start = function(interval1)
    {
        interval1 = (typeof(interval1) !== 'undefined') ? interval1 : 1000;
 
        if(status1 == 0)
        {
            status1 = 1;
            timer_id1 = setInterval(function()
            {
                switch(mode1)
                {
                    default:
                    if(time1)
                    {
                        time1--;
                        generateTime1();
                        if(typeof(callback1) === 'function') callback1(time1);
                    }
                    break;
                    
                    case 1:
                    if(time1 < 86400)
                    {
                        time1++;
                        generateTime1();
                        if(typeof(callback1) === 'function') callback1(time1);
                    }
                    break;
                }
            }, interval1);
        }
    }
    
    //  Same as the name, this will stop or pause the timer ex. timer.stop()
    this.stop =  function()
    {
        if(status1 == 1)
        {
            status1 = 0;
            clearInterval(timer_id1);
        }
    }
    
    // Reset the timer to zero or reset it to your own custom time ex. reset to zero second timer.reset(0)
    this.reset =  function(sec1)
    {
        sec1 = (typeof(sec1) !== 'undefined') ? sec1 : 0;
        time1 = sec1;
        generateTime1(time1);
    }
    
    // Change the mode of the timer, count-up (1) or countdown (0)
    this.mode1 = function(tmode1)
    {
        mode1 = tmode1;
    }
    
    // This methode return the current value of the timer
    this.getTime = function()
    {
        return time1;
    }
    
    // This methode return the current mode of the timer count-up (1) or countdown (0)
    this.getMode1 = function()
    {
        return mode1;
    }
    
    // This methode return the status of the timer running (1) or stoped (1)
    this.getStatus1
    {
        return status1;
    }
    
    // This methode will render the time variable to hour:minute:second format
    function generateTime1()
    {
        var second1 = time1 % 60;
        var minute1 = Math.floor(time1 / 60) % 60;
        var hour1 = Math.floor(time1 / 3600) % 60;
        
        second1 = (second1 < 10) ? '0'+second1 : second1;
        minute1 = (minute1 < 10) ? '0'+minute1 : minute1;
        hour1 = (hour1 < 10) ? '0'+hour1 : hour1;
		
		/*if(minute==10 && second<=50) {
		$('.countdown').show();
		$('.countdown').html(minute  + ':' + second);
		}*/
        
        $('div.timer1 span.second1').html(second1);
        $('div.timer1 span.minute1').html(minute1);
		$('div.timer1 span.hour1').html(hour1);
		
       // $('div.timer span.hour').html(hour);
    }
}


var timer1;
 
$(document).ready(function(e) 
{
    timer1 = new _timer1
    (
        function(time1)
        {
		
		
		
            if(time1 == 0)
            {
                timer1.stop();
                removeBookedTicketWRTCronTbl(stdLocIds,stdEvntIds,persCookieId);
            }
        }
    );
    timer1.reset(<?php echo $diffTotal;?>);
    timer1.start(1000);
});

</script>
<div class="timer" style="visibility:hidden;">
<span class="minute">00</span>:<span class="second">10</span>
</div>
<div class="timer1" style="visibility:hidden;">
<span class="hour1">00</span>:<span class="minute1">00</span>:<span class="second1">10</span>
</div>
