<script>
/*var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";
  
  var prevArrow = document.getElementsByClassName('lft-arr-calander');
  var nextArrow = document.getElementsByClassName('rght-arr-calander');
  prevArrow[0].style.display = "block";
  nextArrow[0].style.display = "block";
  if (slideIndex === 1) prevArrow[0].style.display = "none";
  if (slideIndex === x.length) nextArrow[0].style.display = "none";
    
}*/
</script>
<!-- Footer section start-->

<div class="footer">

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-75341734-1', 'auto');
ga('send', 'pageview');
</script>
<!-- Google Tag Manager -->
<script>
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KBC7L6N');
</script>
<!-- End Google Tag Manager -->

<div class="footer-middle">
<div class="ft-section-1">
<div class="ft-logo"><img src="<?php echo base_url_images?>footer-logo.png"></div>
<?php 
global $stLoca;
/*if(isset($_SESSION['studioInfo'][0]['locationId']) && $_SESSION['studioInfo'][0]['locationId']!="") {
$stLocaI=$_SESSION['studioInfo'][0]['locationId'];
} else {
$stLocaI=$stLoca;
}*/
$sinfof=getLocationIdbyslug($stLoca);
echo fgetSIcons($sinfof["id"])?>
</div>

<div class="ft-section-2">
<h3 class="ft-header">Board & Brush</h3>
<?php echo $addressFooter;?>
</div>

<div class="ft-section-3">
<h3 class="ft-header">QUICK LINKS</h3>

<ul>
<li><a href="<?php echo base_url_site;?>">HOME</a></li>
<li><a href="https://boardandbrush.com/about-wood-sign-workshops/">ABOUT</a></li>
<li><a href="https://boardandbrush.com/privacy-policy/">PRIVACY POLICY</a></li>
<li><a href="https://boardandbrush.com/faqs/">FAQs</a></li>
<li><a href="https://boardandbrush.com/studio-locations/">CONTACT US</a></li>
</ul>

</div>

 <div class="ft-section-4">
 <h3 class="ft-header">NEWSLETTER SIGN-UP</h3>
 <div class="newsletter-section">
 <div id="mc_embed_signup"> <form style="text-align: left;" action="//boardandbrush.us9.list-manage.com/subscribe/post?u=a60902170002f83462c8ac2da&amp;id=1c5264174c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate> <div id="mc_embed_signup_scroll"> <div class="mc-field-group"> <label for="mce-EMAIL" style="color:#ffffff;">Email Address <span class="asterisk">*</span></label> <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL"> </div> <div id="mce-responses" class="clear"> <div class="response" id="mce-error-response" style="display:none"></div> <div class="response" id="mce-success-response" style="display:none"></div> </div> <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups--> <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_a60902170002f83462c8ac2da_6b84e586b8" tabindex="-1" value=""></div> <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div> </div> </form> </div> <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script> <!--End mc_embed_signup-->
 </div>
 
  
 
  
 </div>

</div>
</div> 

<div class="copy-right">
<div class="copy-right-mid"> This website and all designs depicted on it are Copyrighted Board & Brush, LLC.  All Rights Reserved © 2016  |  Website Design by <a href="#">Matt Gerber Designs</a>  |  Studio Photography by Gretchen Bahr </div>
</div>




<div class="goto-top">
<a href="#" class="mk-go-top">
	<svg class="mk-svg-icon" data-name="mk-icon-chevron-up" data-cacheid="icon-5ad5bd90b9ef6" style=" height:16px; width: 16px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M1683 1331l-166 165q-19 19-45 19t-45-19l-531-531-531 531q-19 19-45 19t-45-19l-166-165q-19-19-19-45.5t19-45.5l742-741q19-19 45-19t45 19l742 741q19 19 19 45.5t-19 45.5z"></path></svg></a>
</div>
<!-- Footer section end -->



<!-- workshop modal window start -->


<!--<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>-->




<div class="pop-mob">
<div class="modal-overlay"></div>
<div class="modal">
<div class="workshop-modal">
<div class="workshop-modal-section">
<div class="modal-close"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><polygon fill="#d7d7d7" points="39.999,38.917 21.072,19.974 39.896,1.152 38.849,0.104 20.026,18.926 1.116,0 0.068,1.047 18.978,19.974 0,38.952 1.047,40 20.025,21.022 38.951,39.965 "/></svg></div>
<div class="modal-header">
<div class="modal-datetime">
<div class="modal-date"><p>January</p>31</div>
<div class="modal-day">
Thursday<br /> Hartland, Wisconsin
</div>
</div>
</div>

<div class="modal-date-box">
<img src="<?php echo base_url_images; ?>date-bg.jpg">
<div class="modal-date-content">
1pm - 4pm <br />Seats Available: 10/<small>30</small><br />
<span>Stephanie’s Birthday Party Workshop</span>
</div>


</div>

<div class="modal-date-box">
<img src="<?php echo base_url_images; ?>date-bg.jpg">

<div class="modal-date-content">
6pm - 9pm <br />Seats Available: 10/<small>24</small><br />
<span>Pick your Project Make any sign in our gallery!</span>
</div>

</div>
</div>
</div>
</div>
</div>


<script>
$(document).ready(function(){
$('.c-pop').click(function(){
$('.modal-overlay').fadeIn("fast");
$('.modal').fadeIn("fast");
});

$('.modal-close').click(function(){
$('.modal-overlay').fadeOut("fast");
$('.modal').fadeOut("fast");
});

//$('.modal-overlay').click(function(){
//$('.modal-overlay').fadeOut("fast");
//$('.modal').fadeOut("fast");
//});

});

</script> 



<!-- workshop modal window end -->






<script  type="text/javascript">
$(document).ready(function(){

	$('.goto-top').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 400);
    return false;
	});
	
	$(".goto-top").hide();
	

	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('.goto-top').fadeIn();
				
				
			} else {
				$('.goto-top').fadeOut();
			}
		});
		
		

	});
    });
	
</script>





<!--<a style="position:absolute; top:250px; right:50px; font-size:20px; text-decoration:none; border:1px solid blue; padding:10px;" href="choose-workshop-2.html">Next Page</a>-->
	

</body>
</html>
