<?php
error_reporting(E_ALL & ~E_NOTICE);
//ini_set('session.gc_maxlifetime', 5400); //2700
// each client should remember their session id for EXACTLY 45 min
//session_set_cookie_params(5400); 
session_start();
error_reporting(1);
require_once ('MysqliDb.php');
global $db;



function get_ip_address() {
	// check for shared internet/ISP IP
	if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
		return $_SERVER['HTTP_CLIENT_IP'];
	}
	// check for IPs passing through proxies
	if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		// check if multiple ips exist in var
		if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
			$iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
			foreach ($iplist as $ip) {
				if (validate_ip($ip))
					return $ip;
			}
		} else {
			if (validate_ip($_SERVER['HTTP_X_FORWARDED_FOR']))
				return $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
	}
	if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED']))
		return $_SERVER['HTTP_X_FORWARDED'];
	if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
		return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
	if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR']))
		return $_SERVER['HTTP_FORWARDED_FOR'];
	if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED']))
		return $_SERVER['HTTP_FORWARDED'];
	// return unreliable ip since all else failed
	return $_SERVER['REMOTE_ADDR'];
}
/**
 * Ensures an ip address is both a valid IP and does not fall within
 * a private network range.
 */
function validate_ip($ip) {
	if (strtolower($ip) === 'unknown')
		return false;
	// generate ipv4 network address
	$ip = ip2long($ip);
	// if the ip is set and not equivalent to 255.255.255.255
	if ($ip !== false && $ip !== -1) {
		// make sure to get unsigned long representation of ip
		// due to discrepancies between 32 and 64 bit OSes and
		// signed numbers (ints default to signed in PHP)
		$ip = sprintf('%u', $ip);
		// do private network range checking
		if ($ip >= 0 && $ip <= 50331647) return false;
		if ($ip >= 167772160 && $ip <= 184549375) return false;
		if ($ip >= 2130706432 && $ip <= 2147483647) return false;
		if ($ip >= 2851995648 && $ip <= 2852061183) return false;
		if ($ip >= 2886729728 && $ip <= 2887778303) return false;
		if ($ip >= 3221225984 && $ip <= 3221226239) return false;
		if ($ip >= 3232235520 && $ip <= 3232301055) return false;
		if ($ip >= 4294967040) return false;
	}
	return true;
}



$db = new MysqliDb (Array (
                'host' => 'localhost',
                'username' => 'root', 
                'password' => '',
                'db'=> 'boardnbrushdb',
                'port' => 3306,
                'prefix' => '',
                'charset' => 'utf8'));

/*$db = new MysqliDb (Array (
                'host' => 'localhost',
                'username' => 'root', 
                'password' => 'Shirt@123!',
                'db'=> 'shirtchamp_demo',
                'port' => 3306,
                'prefix' => '',
                'charset' => 'utf8'));*/

				
//$db = new MysqliDb ($mysqli);
$path=$_SERVER['DOCUMENT_ROOT'];
define('base_url_site','https://'.$_SERVER['HTTP_HOST'].'/bnb/');
define('redirect_url','https://'.$_SERVER['HTTP_HOST'].'/bnb/'.$_SERVER['REQUEST_URI']);
define('base_url_js','https://'.$_SERVER['HTTP_HOST'].'/bnb/js/');
define('base_url_css','https://'.$_SERVER['HTTP_HOST'].'/bnb/css/');
define('base_url_images','https://'.$_SERVER['HTTP_HOST'].'/bnb/images/');

define('base_url_popup','https://'.$_SERVER['HTTP_HOST'].'/bnb/pop-up/');
define('base_url_fontface','https://'.$_SERVER['HTTP_HOST'].'/bnb/fontface/');

//define('base_url_images','httpss://s3.amazonaws.com/shirtchamp/styleImages/');
define('dir_url','https://s3.amazonaws.com/cupload/uploads/');
define('dir_path',$path.'/cpanel/uploads/');
define('SYMBOL','$');
define('Session_time_enabled','90'); //45 min
define('total_session_time','90'); //45 min
define('cron_exist_time_multiple_user','15'); //15 min
//define('INTERVALTIME',' - INTERVAL 360 Minute'); //server interal time difference

define('live_site_mail_link_display','www.boardandbrush.com/');
define('live_site_mail_link_url','https://boardandbrush.com/');

if (!class_exists('S3'))require_once('S3.php');

//Some  bullshit to prevent php from throwing notices related to the s3 class 
// date_default_timezone_set('America/Los_Angeles');
// defined('CURL_SSLVERSION_TLSv1')   || define('CURL_SSLVERSION_TLSv1', 1);

// AWS access info
if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAIDJP7L7AROVO5ISQ');
if (!defined('awsSecretKey')) define('awsSecretKey', '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');

    
define('bucketName', 'cupload');
define('Server_Default_Timezone','America/Chicago');
define('Studio_Default_Timezone','America/Chicago');
define('Privacy_Policy_Url','https://boardandbrush.com/policies/');
define('From_Email','notifications@boadandbrush.com'); //admin mail
define('From_Name','Board & Brush'); //admin name
define('CFrom_Email','notifications@boardandbrush.com'); //admin contactus mail
define('CFrom_Name','Board & Brush'); //admin contactus name
?>
