<script>
$(document).on("click","#prefreshWrkShpFull",function() {
	//$("#amterroridWrkShpFull").hide();
	window.location.reload(); 
});
</script>

<div class="pop_up_overlay2" id="amterroridWrkShpFull" style="display:none;">
	<div class="prvevent-overlay2 dpg-pad">
	<div class="prvevent-wrapper2 dpg-pos">
		<h2 id="hmsg">Workshop Full</h2>
		<p id="ptextid">All seats for the workshop have been booked before you were able<br> to claim this seat. We apologize for any inconvenience.<br> Please check back later to see if another seat becomes available!</p>
		<div class="prvevent-form2">
			<form id="prvevt2" action="" method="post">
				<div class="pdf-row" style="text-align:center;">
					<button type="button" id="prefreshWrkShpFull">CONTINUE <i class="fa fa-angle-right"></i></button>
				</div>
			</form>
		</div>
	</div>
	</div>
</div>