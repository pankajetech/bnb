<?php
require_once ('config_inc.php');
//date_default_timezone_set('america/chicago');
//date_default_timezone_set('America/Los_Angeles');


// Get the IP address of the visitor so we can work with it later.
$ip = $_SERVER['REMOTE_ADDR'];
 
function slugify1($str) {
  // Convert to lowercase and remove whitespace
  $str = strtolower(trim($str));  

  // Replace high ascii characters
  $chars = array("�", "�", "�", "�");
  $replacements = array("ae", "oe", "ue", "ss");
  $str = str_replace($chars, $replacements, $str);
  $pattern = array("/(�|�|�|�)/", "/(�|�|�|�)/", "/(�|�|�|�)/");
  $replacements = array("e", "o", "u");
  $str = preg_replace($pattern, $replacements, $str);

  // Remove puncuation
  $pattern = array(":", "!", "?", ".", "/", "'");
  $str = str_replace($pattern, "", $str);

  // Hyphenate any non alphanumeric characters
  $pattern = array("/[^a-z0-9-]/", "/-+/");
  $str = preg_replace($pattern, "", $str);

  return $str;
} 


    function substrwords($text, $maxchar, $end='...') {
        if (strlen($text) > $maxchar || $text == '') {
            $words = preg_split('/\s/', $text);      
            $output = '';
            $i      = 0;
            while (1) {
                $length = strlen($output)+strlen($words[$i]);
                if ($length > $maxchar) {
                    break;
                } 
                else {
                    $output .= " " . $words[$i];
                    ++$i;
                }
            }
            $output .= $end;
        } 
        else {
            $output = $text;
        }
        return $output;
    }
 
function fnCheckEmpty($item) {
if(!empty($item) and $item!="") {
$item=$item;
} else {
$item="";
}
return $item;
}
 
function getExtension($str) 
{
$i = strrpos($str,".");
if (!$i) { return ""; }
$l = strlen($str) - $i;
$ext = substr($str,$i+1,$l);
return $ext;
}
//Here you can add valid file extensions. 
$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");


function checkQty($evid){
global $db;

$cRev = $db->rawQueryOne ("select evNoOfTickets,evBookTicket from bb_event where id=$evid  ", array(''));
return $cRev['evBookTicket'];

}

function checkEventTicketLmit($evid,$ticketId){
global $db;

$cRev = $db->rawQueryOne ("select evTicketLImit from bb_evticketpackage where id=$ticketId and eventID=$evid  ", array(''));
return $cRev['evTicketLImit'];

}

function getTaxbyStudio($stId){
global $db;

$cRev = $db->rawQueryOne ("select saleTax from bb_taxstudio where studioLocId='".$stId."'  ", array(''));
if(isset($cRev['saleTax']) && $cRev['saleTax']!="") {
return $cRev['saleTax'];
} else {
return "0.00";
}

}


function getAbbr($stname){
global $db;
//echo "select abbr from bb_states where state_name like '%".$stname."%'  ";
$cRev = $db->rawQueryOne ("select abbr from bb_states where state_name like '%".$stname."%'  ", array(''));
if(isset($cRev['abbr']) && $cRev['abbr']!="") {
return $cRev['abbr'];
} else {
return "";
}
}

function removeEXtraSpace($str) {
$str = preg_replace('/\s+/', ' ', $str);
return $str;
}
function getBoardDimensionByProjectId($proId){
global $db;
$cRev = $db->rawQueryOne ("select specType,specOptionVal from bb_prospecifications where proID=$proId and specOptionVal=1  ", array(''));
if(isset($cRev['specType']) && $cRev['specType']!="") {
$cRev1 = $db->rawQueryOne ("select id,sizename from bb_prospcsizemeasure where id='".$cRev['specType']."' and id_type='".$cRev['specOptionVal']."' and status=0  ", array(''));
return $cRev1;
} else {
return "";
}

}

function getFinePrints($profineprintId,$customFPrintsId){
	global $db;
	$list="";
	if($profineprintId != ""){
		$cRev = $db->rawQuery("SELECT * from `bb_profineprintoptions` WHERE id IN(".$profineprintId.") and status=0 order by id asc   ", array(''));
		if(count($cRev)>0){
			foreach($cRev as $key=>$item){
				//$list.='*'.htmlspecialchars($item['proFinePrintName'])."<Br>";
				$list.='*'.$item['proFinePrintName']."<Br>";
			}
		}
	} 
	if($customFPrintsId != ""){
		if(isset($customFPrintsId) && $customFPrintsId!="") {
			//$list.='*'.htmlspecialchars($customFPrintsId)."<Br>";
			$list.='*'.$customFPrintsId."<Br>";
		}	
	}
	else {
		//return "";
		$list.= "";
	}
	return $list;
}


/*function getLocationIdbyslug($slugname) {
global $db;
$sslug = $db->rawQueryOne ("SELECT * FROM `bb_studiolocation` where slug=? ", array($slugname));
return $sslug;
}*/

function getLocationIdbyslug($slugname) {
global $db;
$sslug = $db->rawQueryOne ("SELECT id,stNum,slug,stName,stEmail,lat,lng,stStreetAddress,stAddress1,stCity,stState,stZip,stSNouns,stPhone,stAboutStudo,stOwnerPic,stBioDesc,stOwner,stLocPolicies,stLocDirection,stLogo,stFbLink,stInstaLink,stTwtLink,stPinLink,stStatus,stAuthInfo,stimeZone,stMetaTitle,stMetaDesc,isbtn FROM `bb_studiolocation` where slug=? and isDeleted=0 ", array($slugname));
return $sslug;
}

function getEventIdbyslug($slugname,$studid) {
global $db;

$sslug = $db->rawQueryOne ("SELECT id,evTitle,evDate,evType,evIsClose,isCancel,evSPubDate,evSPubTime,evStTime,evEndTime,evNoOfTickets,evBookTicket,evCalenderColorLabel,evScheduleHrs,evCalenderColor,evImage,slug,evFullDescription,evDesAddition,evAccessCode FROM `bb_event` where slug = '".$slugname."' and evLocationId='".$studid."' and evStatus=0 and isCancel=0 and evTypeStatus='Enabled' and isDeleted=0 ", array(''));
/*print_r($sslug);
die();*/
return $sslug;
}

function getEventIdbyslug1($slugname,$stLocationId) {
	global $db;
	//$sslug = $db->rawQueryOne ("SELECT * FROM `bb_event` where studioId='".$stLocationId."' and slug=? ", array($slugname));
	$sslug = $db->rawQueryOne ("SELECT id,evTitle,evDate,evType,evIsClose,isCancel,evSPubDate,evSPubTime,evStTime,evEndTime,evNoOfTickets,evBookTicket,evCalenderColorLabel,evScheduleHrs,evCalenderColor,evImage,slug,evFullDescription,evDesAddition,evAccessCode FROM `bb_event` where evLocationId IN(".$stLocationId.") and slug=? and evStatus=0 and isCancel=0 and evTypeStatus='Enabled' and isDeleted=0", array($slugname));
	return $sslug;
}

function getEventIdbyslug1P($slugname,$stLocationId) {
	global $db;
	//$sslug = $db->rawQueryOne ("SELECT * FROM `bb_event` where studioId='".$stLocationId."' and slug=? ", array($slugname));
	$sslug = $db->rawQueryOne ("SELECT id,evTitle,evDate,evType,evIsClose,isCancel,evSPubDate,evSPubTime,evStTime,evEndTime,evNoOfTickets,evBookTicket,evCalenderColorLabel,evScheduleHrs,evCalenderColor,evImage,slug,evFullDescription,evDesAddition,evAccessCode FROM `bb_event` where evLocationId IN(".$stLocationId.") and slug=?  and isCancel=0 and evTypeStatus='Enabled' and isDeleted=0", array($slugname));
	return $sslug;
}

function getLocationInfobyID($id) {
	global $db;
	$sslug = $db->rawQueryOne ("SELECT * FROM `bb_studiolocation` where id=? ", array($id));
	return $sslug;
}

function getEventInfobyID($id) {
global $db;
$sslug = $db->rawQueryOne ("SELECT * FROM `bb_event` where slug=? ", array($id));
return $sslug;
}

function getStudioEvents($locationId,$slug) {
global $db;
$strEvents="";
$curDate=strtotime(date('Y-m-d H:i:s'));
//(evDate>='".date('Y-m-d')."' and  evDate<='".date('Y-m-d',strtotime("+5 days",strtotime(date('Y-m-d'))))."')
$sql="select id,evTitle,evDate,evType,evIsClose,isCancel,evSPubDate,evSPubTime,evStTime,evEndTime,evNoOfTickets,evBookTicket,evCalenderColorLabel,evScheduleHrs,evCalenderColor,evImage,slug from bb_event where evLocationId='".$locationId."' and evDate>='".date('Y-m-d')."' and isDeleted=0 and evStatus=0 and evTypeStatus='Enabled' and evIsClose=0 and isCancel=0 order by evDate asc  ";

$results =  $db->rawQuery($sql);
if(count($results)>0){
$k=1;
foreach($results as $key=>$item){
$publishDate=strtotime(date('Y-m-d H:i:s',strtotime($item['evSPubDate']." ".$item['evSPubTime'])));
$evdate=date('m/d/Y',strtotime($item['evDate']));
$evImage=$item['evImage'];
$evDay=date('d',strtotime($item['evDate']));
$evWeekDay=date('D',strtotime($item['evDate']));
$evStTime=str_replace(" ","",str_replace(":00","",$item['evStTime']));
$evEndTime=str_replace(" ","",str_replace(":00","",$item['evEndTime']));

$date = new DateTime($item['evDate']." ".$item['evStTime']);
$startDate = $date->format('Y-m-d H:i:s');
//echo date('d-m-Y h:i:s',$curDate).'--'.$startDate1."--1<Br>";

$startWDate=date('Y-m-d',strtotime($item['evDate']." ".$item['evStTime']));

$evDTime=strtotime(date('Y-m-d H:i:s',strtotime($item['evDate']." ".$item['evStTime'])));
$evBDTime=$item['evDate']." ".$item['evStTime'];
if(isset($item['evScheduleHrs']) && $item['evScheduleHrs']>0) {
$evsDays=strtotime("-".$item['evScheduleHrs']." hours",strtotime($evBDTime));
} else {
$evsDays=$evDTime;
}


if(isset($evImage) && $evImage!=""){
$imageurl=dir_url."events/700X600/".$evImage;
}  else  {
$imageurl=base_url_images."placeholder.jpg";
}
$evStTime1=ltrim($evStTime, '0'); 	
$evEndTime1=ltrim($evEndTime, '0');  
$evseats1=$item['evNoOfTickets']-$item['evBookTicket'];
$strEvents.='<div class="ucb-1">';
// && $curDate>=$evsDays && $evseats1>0


//if($evsDays>=$curDate) {
if($item['isCancel']==0 && $item['evIsClose']==0 && $evseats1>0 && $curDate>=$publishDate) {
if($evsDays>=$curDate) {
/*$strEvents.='<span>'.$evdate.'</span><br>
All Day<bR><strong>CLOSED</strong>';
} else {*/
if($k<=3) {
$strEvents.='<a class="ucb-img" href="'.base_url_site.$slug.'/events/'.$item['slug'].'"><div class="ucb-image"><img src="'.$imageurl.'" alt="image" width="214"></div></a>';
$strEvents.='<span>'.$evdate.'</span><br><span>'.$evStTime1.' - '.$evEndTime1.'</span><Br>';
$strEvents.='<a class="ucb-img" href="'.base_url_site.$slug.'/events/'.$item['slug'].'"><span>'.substrwords($item['evTitle'],55,'').'</span></a>';
$k++;
}
}
}
$strEvents.='</div>';
}
}
return $strEvents;
}






function getStudioEventsM($locationId,$slug) {
global $db;
$strEvents="";
$curDate=strtotime(date('Y-m-d H:i:s'));
//(evDate>='".date('Y-m-d')."' and  evDate<='".date('Y-m-d',strtotime("+5 days",strtotime(date('Y-m-d'))))."')
$sql="select id,evTitle,evDate,evType,evIsClose,isCancel,evSPubDate,evSPubTime,evStTime,evEndTime,evNoOfTickets,evBookTicket,evCalenderColorLabel,evScheduleHrs,evCalenderColor,evImage,slug from bb_event where evLocationId='".$locationId."' and evDate>='".date('Y-m-d')."' and isDeleted=0 and evStatus=0 and evTypeStatus='Enabled' and evIsClose=0 and isCancel=0 order by evDate asc  ";

$results =  $db->rawQuery($sql);
if(count($results)>0){
$k=1;
$strEvents.='<div class="date-wrapper mcevents">';
foreach($results as $key=>$item){
$publishDate=strtotime(date('Y-m-d H:i:s',strtotime($item['evSPubDate']." ".$item['evSPubTime'])));
$evdate=date('m/d/Y',strtotime($item['evDate']));
$evImage=$item['evImage'];
$evDay=date('d',strtotime($item['evDate']));
$evWeekDay=date('l',strtotime($item['evDate']));
$evStTime=str_replace(" ","",str_replace(":00","",$item['evStTime']));
$evEndTime=str_replace(" ","",str_replace(":00","",$item['evEndTime']));

$date = new DateTime($item['evDate']." ".$item['evStTime']);
$startDate = $date->format('Y-m-d H:i:s');
//echo date('d-m-Y h:i:s',$curDate).'--'.$startDate1."--1<Br>";

$startWDate=date('Y-m-d',strtotime($item['evDate']." ".$item['evStTime']));

$evDTime=strtotime(date('Y-m-d H:i:s',strtotime($item['evDate']." ".$item['evStTime'])));
$evBDTime=$item['evDate']." ".$item['evStTime'];
if(isset($item['evScheduleHrs']) && $item['evScheduleHrs']>0) {
$evsDays=strtotime("-".$item['evScheduleHrs']." hours",strtotime($evBDTime));
} else {
$evsDays=$evDTime;
}


if(isset($evImage) && $evImage!=""){
$imageurl=dir_url."events/700X600/".$evImage;
}  else  {
$imageurl=base_url_images."placeholder.jpg";
}
$evStTime1=str_replace(" ","",ltrim($evStTime, '0')); 	
$evEndTime1=str_replace(" ","",ltrim($evEndTime, '0'));  
$evseats1=$item['evNoOfTickets']-$item['evBookTicket'];

// && $curDate>=$evsDays && $evseats1>0


//if($evsDays>=$curDate) {
if($item['isCancel']==0 && $item['evIsClose']==0 && $evseats1>0 && $curDate>=$publishDate) {
if($evsDays>=$curDate) {
/*$strEvents.='<span>'.$evdate.'</span><br>
All Day<bR><strong>CLOSED</strong>';
} else {*/
if($k<=4) {
$strEvents.='<div class="date-box">';
$strEvents.='<a href="'.base_url_site.$slug.'/events/'.$item['slug'].'"><img src="'.$imageurl.'" alt="image" width="200">';
$strEvents.='<div class="datebox-overlay"></div>';
$strEvents.='<div class="date-content"><div class="day">'.$evWeekDay.'</div><div class="date">'.$evDay.'</div><div class="ampm">'.$evStTime1.' - '.$evEndTime1.'</div>';
$strEvents.='<div class="date-dsc"><div class="date-dsc-txt">'.substrwords($item['evCalenderColorLabel'],45,'').'</div></div></div>';
$strEvents.='</a></div>';
$k++;
}
}
}

}
$strEvents.='</div>';
}
return $strEvents;
}



function ftechETicketP($evid,$tid){
global $db;

$evetails = $db->rawQuery("select ev.evType,ev.evImage,ev.evDate,ev.evStTime,ev.evEndTime,ev.evLocationId,ev.evTitle,ev.evFullDescription,ev.evDesAddition,ev.evBookTicket,et.evTicketName,et.evTicketPrice,et.evTicketButtonLabel,et.isManual,et.evTicketLImit,et.evTicketOptionId,et.evTicketCatIds,et.evTicketTypeIds,et.evTicketProjectIds FROM bb_event as ev inner join bb_evticketpackage as et on ev.id=et.eventID where et.id = $tid and et.eventID = $evid ", array(''));
if(!empty($evetails)) {
return $evetails;
} else {
return 0;
}
}






function ftechProject($proId){
global $db;

$result3 = $db->rawQueryOne("SELECT * FROM bb_project WHERE  status=0 and isDeleted=0 and id=?  order by proName asc ", array($proId));
if(isset($result3) && !empty($result3)) {
return $result3;
} else {
return 0;
}
}

function ftechProPersonalize($proId){
global $db;
$cRev = $db->rawQuery("select * from bb_propersoptions where proId='".$proId."' order by id ", array(''));
			if(count($cRev)>0){
			return $cRev;
			}
}

function ftechPromoDetails($ccode){
global $db;
$wh="";
if(isset($_SESSION['studioInfo'][0]['locationId']) && $_SESSION['studioInfo'][0]['locationId']!="") {
$locationinfo=getLocationIdbyslug($_SESSION['studioInfo'][0]['locationId']);
$wh=" and studioId='".$locationinfo['id']."'";

}

$cRev = $db->rawQueryOne("select * from bb_promocode where promoCode='".$ccode."' $wh order by id ", array(''));
			if(count($cRev)>0){
			return $cRev;
			}
}

function fgetSIcons($locId){
global $db;
$socialIcons="";
$cRev = $db->rawQueryOne("select stFbLink,stInstaLink,stTwtLink,stPinLink from bb_studiolocation where id='".$locId."' order by id ", array(''));
			if(isset($cRev["stFbLink"]) && $cRev["stFbLink"]!=""){
			$stFbLink = $cRev["stFbLink"];
			} else {
			$stFbLink = "https://www.facebook.com/boardandbrushcorporate/";
			}
			if(isset($cRev["stInstaLink"]) && $cRev["stInstaLink"]!=""){
			$stInstaLink = $cRev["stInstaLink"];
			} else {
			$stInstaLink = "https://www.instagram.com/explore/locations/1042276229137658/board-brush-creative-studio-corporate-site/";
			}
			if(isset($cRev["stTwtLink"]) && $cRev["stTwtLink"]!=""){
			$stTwtLink = $cRev["stTwtLink"];
			} else {
			$stTwtLink = "https://twitter.com/boardandbrushcs/";
			}
			if(isset($cRev["stPinLink"]) && $cRev["stPinLink"]!=""){
			$stPinLink = $cRev["stPinLink"];
			} else {
			$stPinLink = "http://pinterest.com/boardandbrush/";
			}
$socialIcons='<div class="ft-social-icon">
<a href="'.$stFbLink.'" rel="nofollow"  target="_blank" alt="Follow Us on facebook" title="Follow Us on facebook"><svg class="mk-svg-icon" data-name="mk-jupiter-icon-facebook" data-cacheid="icon-5ad586a6c1f74" style=" height:32px; width: 32px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256-6.4c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm64.057 159.299h-49.041c-7.42 0-14.918 7.452-14.918 12.99v19.487h63.723c-2.081 28.41-6.407 64.679-6.407 64.679h-57.566v159.545h-63.929v-159.545h-32.756v-64.474h32.756v-33.53c0-8.098-1.706-62.336 70.46-62.336h57.678v63.183z"></path></svg></a>
<a href="'.$stInstaLink.'" rel="nofollow" class="builtin-icons custom large instagram-hover" target="_blank" alt="Follow Us on instagram" title="Follow Us on instagram"><svg class="mk-svg-icon" data-name="mk-jupiter-icon-instagram" data-cacheid="icon-5ad586a6c2196" style=" height:32px; width: 32px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 297.6c27.2 0 48-20.8 48-48s-20.8-48-48-48-48 20.8-48 48 20.8 48 48 48zm80-48c0 44.8-35.2 80-80 80s-80-35.2-80-80c0-8 0-12.8 3.2-19.2h-19.2v107.2c0 4.8 3.2 9.6 9.6 9.6h174.4c4.8 0 9.6-3.2 9.6-9.6v-107.2h-19.2c1.6 6.4 1.6 11.2 1.6 19.2zm-22.4-48h28.8c4.8 0 9.6-3.2 9.6-9.6v-28.8c0-4.8-3.2-9.6-9.6-9.6h-28.8c-4.8 0-9.6 3.2-9.6 9.6v28.8c0 6.4 3.2 9.6 9.6 9.6zm-57.6-208c-140.8 0-256 115.2-256 256s115.2 256 256 256 256-115.2 256-256-115.2-256-256-256zm128 355.2c0 16-12.8 28.8-28.8 28.8h-198.4c-9.6 0-28.8-12.8-28.8-28.8v-198.4c0-16 12.8-28.8 28.8-28.8h196.8c16 0 28.8 12.8 28.8 28.8v198.4z"></path></svg></a>
<a href="'.$stPinLink.'" rel="nofollow" class="builtin-icons custom large pinterest-hover" target="_blank" alt="Follow Us on pinterest" title="Follow Us on pinterest"><svg class="mk-svg-icon" data-name="mk-jupiter-icon-pinterest" data-cacheid="icon-5ad586a6c239f" style=" height:32px; width: 32px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256-6.4c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm25.508 327.119c-20.463 0-39.703-10.759-46.285-22.973 0 0-11.014 42.454-13.332 50.654-8.206 28.956-32.336 57.931-34.204 60.3-1.31 1.665-4.206 1.132-4.509-1.046-.518-3.692-6.671-40.229.567-70.031 3.638-14.973 24.374-100.423 24.374-100.423s-6.043-11.758-6.043-29.166c0-27.301 16.275-47.695 36.541-47.695 17.236 0 25.559 12.585 25.559 27.661 0 16.856-11.034 42.045-16.726 65.388-4.754 19.568 10.085 35.51 29.901 35.51 35.895 0 60.076-44.851 60.076-97.988 0-40.37-27.955-70.62-78.837-70.62-57.474 0-93.302 41.693-93.302 88.276 0 16.037 4.881 27.376 12.511 36.129 3.501 4.032 4 5.65 2.728 10.264-.929 3.396-2.993 11.566-3.874 14.802-1.261 4.669-5.144 6.335-9.488 4.613-26.458-10.512-38.802-38.716-38.802-70.411 0-52.337 45.394-115.119 135.43-115.119 72.351 0 119.955 50.911 119.955 105.569 0 72.294-41.325 126.306-102.241 126.306z"></path></svg></a>
<a href="'.$stTwtLink.'" rel="nofollow" class="builtin-icons custom large twitter-hover" target="_blank" alt="Follow Us on twitter" title="Follow Us on twitter"><svg class="mk-svg-icon" data-name="mk-jupiter-icon-twitter" data-cacheid="icon-5ad586a6c258e" style=" height:32px; width: 32px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256-6.4c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm146.24 258.654c-31.365 127.03-241.727 180.909-338.503 49.042 37.069 35.371 101.619 38.47 142.554-3.819-24.006 3.51-41.47-20.021-11.978-32.755-26.523 2.923-41.27-11.201-47.317-23.174 6.218-6.511 13.079-9.531 26.344-10.407-29.04-6.851-39.751-21.057-43.046-38.284 8.066-1.921 18.149-3.578 23.656-2.836-25.431-13.295-34.274-33.291-32.875-48.326 45.438 16.866 74.396 30.414 98.613 43.411 8.626 4.591 18.252 12.888 29.107 23.393 13.835-36.534 30.915-74.19 60.169-92.874-.493 4.236-2.758 8.179-5.764 11.406 8.298-7.535 19.072-12.719 30.027-14.216-1.257 8.22-13.105 12.847-20.249 15.539 5.414-1.688 34.209-14.531 37.348-7.216 3.705 8.328-19.867 12.147-23.872 13.593-2.985 1.004-5.992 2.105-8.936 3.299 36.492-3.634 71.317 26.456 81.489 63.809.719 2.687 1.44 5.672 2.1 8.801 13.341 4.978 37.521-.231 45.313-5.023-5.63 13.315-20.268 23.121-41.865 24.912 10.407 4.324 30.018 6.691 43.544 4.396-8.563 9.193-22.379 17.527-45.859 17.329z"></path></svg></a>
					

</div>';
return 	$socialIcons;		
}


function fgetLogo($locId){
global $db;
$cRev = $db->rawQueryOne("select stLogo from bb_studiolocation where id='".$locId."' order by id ", array(''));
			if(isset($cRev["stLogo"]) && $cRev["stLogo"]!=""){
			return dir_url."studio/logo/".$cRev["stLogo"];
			} else {
			return "";
			}
}

function OrderPriceCalc(){
$orderArr=array();
$j=0;

/*			  echo "<pre>";
	 print_r($_SESSION['currentOrder']);
	 die();*/
		 if(count($_SESSION['currentOrder'])>0){

		foreach($_SESSION['currentOrder'] as $k=>$val1){

				$data_products=ftechETicketP($val1['evid'],$val1['tid']);
				
				foreach($data_products as $k=>$val){
					$orderArr[$j]['evType'] =$val['evType'];		
					$orderArr[$j]['evImage'] =$val['evImage'];
					$orderArr[$j]['evDate'] =$val['evDate'];
					$orderArr[$j]['evStTime'] =$val['evStTime'];
					$orderArr[$j]['evEndTime'] =$val['evEndTime'];					
					$orderArr[$j]['evLocationId'] =$val['evLocationId'];		
					$orderArr[$j]['evFullDescription'] =$val['evFullDescription'];
					$orderArr[$j]['evDesAddition'] =$val['evDesAddition'];
					$orderArr[$j]['evBookTicket'] =$val['evBookTicket'];
					$orderArr[$j]['evTicketName'] =$val['evTicketName'];	
					$orderArr[$j]['evTicketPrice'] =$val['evTicketPrice'];
					$orderArr[$j]['evTicketButtonLabel'] =$val['evTicketButtonLabel'];
					$orderArr[$j]['isManual'] =$val['isManual'];	
					$orderArr[$j]['evTicketLImit'] =$val['evTicketLImit'];			
					$orderArr[$j]['evTicketOptionId'] =$val['evTicketOptionId'];	
					$orderArr[$j]['evTicketCatIds'] =$val['evTicketCatIds'];
					$orderArr[$j]['evTicketTypeIds'] =$val['evTicketTypeIds'];
					$orderArr[$j]['evTicketProjectIds'] =$val['evTicketProjectIds'];	
				}
 				$orderArr[$j]['totalPrice']=($orderArr[$j]['evTicketPrice']*$val1['qty']);
				$orderArr[$j]['qty']=$val1['qty'];
				$orderArr[$j]['evid']=$val1['evid'];
				$orderArr[$j]['tid']=$val1['tid'];
				$orderArr[$j]['oid']=$val1['oid'];
				$orderArr[$j]['pval']=$val1['pval'];
				$orderArr[$j]['sid']=$val1['sid'];
				if($val1['pid']!=0) {
				$orderArr[$j]['pid']=$val1['pid'];
				$data_projects=ftechProject($val1['pid']);
				$orderArr[$j]['proGalImg'] =$data_projects['proGalImg'];
				$orderArr[$j]['proName'] =$data_projects['proName'];
				$orderArr[$j]['proSku'] =$data_projects['proSku'];
				$proPersonalize=ftechProPersonalize($val1['pid']);
				foreach($proPersonalize as $key=>$item){
					//$orderArr[$j][str_replace(" ","_",$item['proLabel'])] =$val1[str_replace(" ","_",$item['proLabel'])];
					$proLabel = StringEscapeSlug($item['proLabel']);
					$orderArr[$j][$proLabel] = $val1[$proLabel];
				}
				$deminsion=getBoardDimensionByProjectId($val1['pid']);
				$orderArr[$j]['sizename'] =$deminsion['sizename'];
				$orderArr[$j]['proDetailedDesc'] =$data_projects['proDetailedDesc'];
				$orderArr[$j]['proGalImg'] =$data_projects['proGalImg'];
				
				} else {
				$orderArr[$j]['pid']=$val1['pid'];
				}
				
			$j++;	
				
		}
		}
	
/*					  echo "<pre>";
	 print_r($orderArr);
	 die();*/

	return $orderArr;	
}

function numToOrdinalWord($num)
		{
			$first_word = array('eth','First','Second','Third','Fouth','Fifth','Sixth','Seventh','Eighth','Ninth','Tenth','Elevents','Twelfth','Thirteenth','Fourteenth','Fifteenth','Sixteenth','Seventeenth','Eighteenth','Nineteenth','Twentieth');
			$second_word =array('','','Twenty','Thirty','Forty','Fifty');

			if($num <= 20)
				return $first_word[$num];

			$first_num = substr($num,-1,1);
			$second_num = substr($num,-2,1);

			return $string = str_replace('y-eth','ieth',$second_word[$second_num].'-'.$first_word[$first_num]);
		}


################ pagination function #########################################


function paginate_function($item_per_page, $current_page, $total_records, $total_pages)
{
    $pagination = '';
    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
        $pagination .= '<ul class="pagination">';
        
		$pglinks=($total_records/$item_per_page);
		
		$limitp=5;
		
        $right_links    = $current_page + 5; 
        $previous       = $current_page - 1; //previous link 
        $next           = $current_page + 1; //next link
        $first_link     = true; //boolean var to decide our first link



	

$ppage=0;
        
        if($current_page > 1){
			$previous_link = ($previous==0)? 1: $previous;
           // $pagination .= '<li class="firsts" data-page="1" title="First" style="cursor:pointer"><a href="javascript:void(0);" ><span class="fst">First</span> <span class="fst-arr"><i class="fa fa-angle-double-left"></i><span></a></li>'; //first link
            $pagination .= '<li data-page="'.$previous_link.'" style="cursor:pointer" class="prevs"><a href="javascript:void(0);"  title="Previous"><span class="pre">Previous</span><span class="prv-arr"><i class="fa fa-angle-left"></i></span></a></li>'; //previous link
			
			if($total_pages==$current_page) {
			$cnpage=($current_page-4);
			} else if($total_pages==($current_page+1)) {
			$cnpage=($current_page-3);
			} else {
			$cnpage=($current_page-2);
			}
			
                for($i = $cnpage; $i < $current_page; $i++){ //Create left-hand side links
                    if($i > 0){
                        $pagination .= '<li data-page="'.$i.'" title="Page'.$i.'" style="cursor:pointer"><a href="javascript:void(0);" >'.$i.'</a></li>';
						$ppage=$i;
                    }
                }   
            $first_link = false; //set first link to false
        }
        
        if($first_link){ //if current active page is first link
            $pagination .= '<li class="first active">'.$current_page.'</li>';
        }elseif($current_page == $total_pages){ //if it's the last active link
            $pagination .= '<li class="last active">'.$current_page.'</li>';
        }else{ //regular current link
            $pagination .= '<li class="active">'.$current_page.'</li>';
        }
           
		
			if($ppage==0) {
			$current_page1=$current_page+4;
			} else if($ppage==1) {
			$current_page1=$current_page+3;
			} else {
			$current_page1=$current_page+2;
			}		  
		        
        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
            if($i<=$total_pages){
			if($i<=$current_page1) {
                $pagination .= '<li data-page="'.$i.'" title="Page '.$i.'" style="cursor:pointer"><a href="javascript:void(0);" >'.$i.'</a></li>';
            }
			}
        }
        if($current_page < $total_pages){ 
				$next_link = ($i > $total_pages) ? $total_pages : $i;
                $pagination .= '<li data-page="'.($current_page+1).'" style="cursor:pointer" class="nexts"><a href="javascript:void(0);"  title="Next"><span class="nxt">Next</span><span class="rigt-arr"><i class="fa fa-angle-right"></i></span></a></li>'; //next link
               // $pagination .= '<li class="lasts" data-page="'.$total_pages.'" style="cursor:pointer"><a href="javascript:void(0);"  title="Last"><span class="lst">Last</span><span class="lst-arr"><i class="fa fa-angle-double-right"></i></span></a></li>'; //last link
        }
        
        $pagination .= '</ul>'; 
    }
    return $pagination; //return pagination links
}



/*function paginate_function($item_per_page, $current_page, $total_records, $total_pages)
{
    $pagination = '';
    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
        $pagination .= '<ul class="pagination">';
        
		$pglinks=($total_records/$item_per_page);
		
		
        $right_links    = $current_page + 7; 
        $previous       = $current_page - 1; //previous link 
        $next           = $current_page + 1; //next link
        $first_link     = true; //boolean var to decide our first link
        
        if($current_page > 1){
			$previous_link = ($previous==0)? 1: $previous;
            $pagination .= '<li class="firsts" data-page="1" title="First" style="cursor:pointer"><a href="javascript:void(0);" ><span class="fst">First</span> <span class="fst-arr"><i class="fa fa-angle-double-left"></i><span></a></li>'; //first link
            $pagination .= '<li data-page="'.$previous_link.'" style="cursor:pointer" class="prevs"><a href="javascript:void(0);"  title="Previous"><span class="pre">Previous</span><span class="prv-arr"><i class="fa fa-angle-left"></i></span></a></li>'; //previous link
                for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
				//for($i = 1; $i < $total_pages; $i++){ //Create left-hand side links
                    if($i > 0){
                        $pagination .= '<li data-page="'.$i.'" title="Page'.$i.'" style="cursor:pointer"><a href="javascript:void(0);" >'.$i.'</a></li>';
                    }
                }   
            $first_link = false; //set first link to false
        }
        
        if($first_link){ //if current active page is first link
            $pagination .= '<li class="first active">'.$current_page.'</li>';
        }elseif($current_page == $total_pages){ //if it's the last active link
            $pagination .= '<li class="last active">'.$current_page.'</li>';
        }else{ //regular current link
            $pagination .= '<li class="active">'.$current_page.'</li>';
        }
                
        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
            if($i<=$total_pages){
                $pagination .= '<li data-page="'.$i.'" title="Page '.$i.'" style="cursor:pointer"><a href="javascript:void(0);" >'.$i.'</a></li>';
            }
        }
        if($current_page < $total_pages){ 
				$next_link = ($i > $total_pages) ? $total_pages : $i;
                $pagination .= '<li data-page="'.($current_page+1).'" style="cursor:pointer" class="nexts"><a href="javascript:void(0);"  title="Next"><span class="nxt">Next</span><span class="rigt-arr"><i class="fa fa-angle-right"></i></span></a></li>'; //next link
                $pagination .= '<li class="lasts" data-page="'.$total_pages.'" style="cursor:pointer"><a href="javascript:void(0);"  title="Last"><span class="lst">Last</span><span class="lst-arr"><i class="fa fa-angle-double-right"></i></span></a></li>'; //last link
        }
        
        $pagination .= '</ul>'; 
    }
    return $pagination; //return pagination links
}*/


function getUpcommingEvents($locationId,$slug) {
global $db;
$strEvents="";
$curDate=strtotime(date('Y-m-d H:i:s'));
//(evDate>='".date('Y-m-d')."' and  evDate<='".date('Y-m-d',strtotime("+5 days",strtotime(date('Y-m-d'))))."')
$sql="select id,evTitle,evDate,evType,evIsClose,isCancel,evSPubDate,evSPubTime,evStTime,evEndTime,evNoOfTickets,evBookTicket,evCalenderColorLabel,evScheduleHrs,evCalenderColor,evImage,slug,evFullDescription,evDesAddition,evAccessCode from bb_event where evLocationId='".$locationId."' and evDate>='".date('Y-m-d')."' and isDeleted=0 and evStatus=0 and evTypeStatus='Enabled' and evIsClose=0 and isCancel=0 order by evDate asc limit 0,8 ";

$results =  $db->rawQuery($sql);
if(count($results)>0){
$strEvents.='<div class="event-right">
<div class="ev-project-add upevt-head">
<h3>Please select another workshop to book!</h3>

<div class="upcoming-evt">
<h2>- Upcoming Workshops -</h2>
<div class="upcoming-evt-box">';
$k=1;
foreach($results as $key=>$item){
$publishDate=strtotime(date('Y-m-d H:i:s',strtotime($item['evSPubDate']." ".$item['evSPubTime'])));
$evdate=date('m/d/Y',strtotime($item['evDate']));
$evImage=$item['evImage'];
$evDay=date('d',strtotime($item['evDate']));
$evWeekDay=date('F',strtotime($item['evDate']));
$evWeekDay1=date('l',strtotime($item['evDate']));
$evStTime=str_replace(" ","",str_replace(":00","",$item['evStTime']));
$evEndTime=str_replace(" ","",str_replace(":00","",$item['evEndTime']));

$date = new DateTime($item['evDate']." ".$item['evStTime']);
$startDate = $date->format('Y-m-d H:i:s');
//echo date('d-m-Y h:i:s',$curDate).'--'.$startDate1."--1<Br>";

$startWDate=date('Y-m-d',strtotime($item['evDate']." ".$item['evStTime']));

$evDTime=strtotime(date('Y-m-d H:i:s',strtotime($item['evDate']." ".$item['evStTime'])));
$evBDTime=$item['evDate']." ".$item['evStTime'];
if(isset($item['evScheduleHrs']) && $item['evScheduleHrs']>0) {
$evsDays=strtotime("-".$item['evScheduleHrs']." hours",strtotime($evBDTime));
} else {
$evsDays=$evDTime;
}

//substrwords($item['evTitle'],17,'')
if(isset($evImage) && $evImage!=""){
$imageurl=dir_url."events/700X600/".$evImage;
}  else  {
$imageurl=base_url_images."placeholder.jpg";
}
$evStTime1=ltrim($evStTime, '0'); 	
$evEndTime1=ltrim($evEndTime, '0');  
if($curDate>=$publishDate) {
if($evsDays>=$curDate) {
if($k<=4) {
$evseats=$item['evNoOfTickets']-$item['evBookTicket'];
if($evseats>0) {
$strEvents.='<div class="upevt-box" style="background-image:url('.$imageurl.');"><a href="'.base_url_site.$slug.'/events/'.$item['slug'].'">
<div class="upevt-overlay"></div>
<div class="upevt-content">
<div class="upevt-day">'.$evWeekDay1.'</div>
<div class="upevt-date">'.$evDay.'</div>
<div class="upevt-ampm">'.ltrim($evStTime1, '0').' - '.ltrim($evEndTime1, '0').'</div>
<div class="upevt-date-dsc-mobile"><div class="upevt-date-dsc-mobile-mid">'.substrwords($item['evCalenderColorLabel'],43,'').'</div></div>
<div class="upevt-date-dsc"><div class="upevt-date-dsc-mid">'.substrwords($item['evTitle'],55,'').'</div></div>
</div></a>
</div>';
$k++;
}
}
}
}

}
$strEvents.='<a class="view-calander" href="'.base_url_site.$slug.'#Calendar-workshop">View Calendar</a>

</div>
</div>



</div>
</div>';
}
return $strEvents;
}


function getEventInfo($evId){
	global $db;
	$cRev = $db->rawQueryOne("select id,evNoOfTickets,evBookTicket,slug from bb_event where id='".$evId."' ", array(''));
	if(count($cRev)>0){
		return $cRev;
	}
}

function isExistEventInCronTbl($evId,$cookieUserId) {
	global $db;
	$sql55 = "select id,eventId from bb_crondata where eventId ='".$evId."' and  cookieId ='".$cookieUserId."'";
	$cRev = $db->rawQueryOne($sql55,array(''));
	//echo "<pre>cRev=";print_R($cRev);die;
	if(count($cRev)>0){
		return true;
	} else {
		return false;
	}
}

function isExistEventInCronTbl1($evId,$cookieUserId,$isUpdated) {
 global $db;
 $sql55 = "select id,eventId from bb_crondata where bookedTicket>0 and eventId ='".$evId."' and  cookieId ='".$cookieUserId."' and isUpdated ='".$isUpdated."'";
 $cRev = $db->rawQueryOne($sql55,array(''));
 //echo "<pre>cRev=";print_R($cRev);die;
 if(count($cRev)>0){
  return true;
 } else {
  return false;
 }
}


function chkExistEventInCronTbl1($evId,$cookieUserId) {
 global $db;
 $sql55 = "select isUpdated from bb_crondata where bookedTicket>0 and eventId ='".$evId."' and  cookieId ='".$cookieUserId."'";
 $cRev = $db->rawQueryOne($sql55,array(''));
 //echo "<pre>cRev=";print_R($cRev);die;
 if(count($cRev)>0){
  return $cRev['isUpdated'];
 } else {
  return 0;
 }
}

//Get booked seats from 'bb_crondata' tbl for same user
function getBkdTcktForCronWRTCookieUsr($evId,$cookieUserId){
	global $db;
	$cRev = $db->rawQueryOne("select id,bookedTicket,createdDate,createdDateTimeStr from bb_crondata where eventId ='".$evId."' and  cookieId ='".$cookieUserId."' ", array(''));
	if(count($cRev)>0){
		return $cRev;
	}
}

//Get booked seats from 'bb_crondata' tbl for same user
function getBkdTcktForCronWRTCookieUsr1($evId,$cookieUserId,$isUpdated){
 global $db;
 $cRev = $db->rawQueryOne("select id,bookedTicket,createdDate,createdDateTimeStr,actualDT1stIns from bb_crondata where bookedTicket>0 and eventId ='".$evId."' and  cookieId ='".$cookieUserId."' and isUpdated ='".$isUpdated."' ", array(''));
 if(count($cRev)>0){
  return $cRev;
 }
}

//Get total booked seats from 'bb_crondata' tbl for same event
function getTotBkdTcktWRTEventId($evId){
	global $db;
	$cRev = $db->rawQuery("select bookedTicket from bb_crondata where eventId ='".$evId."' ", array(''));
	//echo "<pre>cRev==";print_r($cRev);die;
	if(count($cRev)>0){
		$tot = 0;
		foreach($cRev as $key=>$val){
			$tot += $val['bookedTicket'];
		}
	} else {
		$tot =0;
	}
	return $tot;
}
//Fetch Project dimension WRT ProjectID
function ftechProjectDimension($proId){
	global $db;
	$projDetails = $db->rawQueryOne("SELECT * FROM bb_project WHERE  status=0 and isDeleted=0 and id=?  order by proName asc ", array($proId));
	if(isset($projDetails) && !empty($projDetails)) {
		$proSpecWi = $projDetails['proSpecWi'];
		$proSpecHi = $projDetails['proSpecHi'];
		$proSpecDe = $projDetails['proSpecDe'];
		if($proSpecDe != ""){
			$sizeStr = $proSpecWi."<span class='sizeX'>x</span>".$proSpecHi."<span class='sizeX'>x</span>".$proSpecDe;
		} else {
			$sizeStr = $proSpecWi."<span class='sizeX'>x</span>".$proSpecHi."</b>";
		}
		return $sizeStr;
	} else {
		return 0;
	}
}

//Check event has available seats from 'bb_crondata' table
function chkEvtHasAvailSeatsInCronTbl($evId) {
	global $db;
	//Get total vacant seats from event table
	$sql1 = "select id,evNoOfTickets,evBookTicket
			from bb_event where id ='".$evId."'";
	$cRev1 = $db->rawQueryone($sql1,array(''));
	
	$evNoOfTickets = $cRev1['evNoOfTickets'];
	$evBookTicket = $cRev1['evBookTicket'];
	if($evNoOfTickets>$evBookTicket){
		$totalVacantSeats = $evNoOfTickets-$evBookTicket;
	} else {
		$totalVacantSeats = 0;
	}
	
	$sql = "select id,eventId,totalVacantSeats,bookedTicket
			from bb_crondata where eventId ='".$evId."' and  isBooked='0' ";
	$cRev = $db->rawQuery($sql,array(''));
	//echo "<pre>cRev=";print_R($cRev);die;
	if(count($cRev)>0){
		$totalBookedSeats = 0;
		foreach($cRev as $key=>$val){
			$totalBookedSeats += $val['bookedTicket'];
		}
		//$totalVacantSeats = $cRev[0]['totalVacantSeats'];
		if($totalBookedSeats >= $totalVacantSeats){
			return 0; //all seats booked
		} else {
			$remaingSeats = $totalVacantSeats - $totalBookedSeats;
			return $remaingSeats;
		}
	} else {
		return -1; //all seats vacant
	}
}

//remove record from cron table wrt cookieuserid,cookieeventslug,cookieLocationSlug
function removeBookedTicketWRTCronTbl($cookieUserId,$cookieEventSlug,$cookieStLocSlug) {
	global $db;
	$locationinfo = getLocationIdbyslug($cookieStLocSlug);
	if(isset($locationinfo) && !empty($locationinfo)) {
		$stLocationId = $locationinfo['id'];
		$eventinfo = getEventIdbyslug1($cookieEventSlug,$stLocationId);
		if(isset($eventinfo) && !empty($eventinfo)) {
			$cookieEvtId = $eventinfo['id'];
			$db->where ('cookieId', $cookieUserId);
			$db->where ('eventId', $cookieEvtId);
			$db->delete("bb_crondata");
		}
	}
}

//Function is used to get studio info
function getStudioInfoByStdId($studioId) {
	global $db;
	$params = array(''); 
	$sslug = $db->rawQueryOne ("SELECT * FROM `bb_studiolocation` where id='".$studioId."' ",$params);
	return $sslug;
}

//Function is used to get booking info
function getBookingProjectInfo($bookingId) {
	global $db;
	$params = array(''); 
	$sql = "SELECT * FROM `bb_booktcktinfo` where bookingId='".$bookingId."' ";
	$sslug = $db->rawQuery($sql,$params);
	return $sslug;
}

function ftechPromoDetailsReminderEmail($ccode){
	global $db;
	$cRev = $db->rawQueryOne("select * from bb_promocode where promoCode='".$ccode."' order by id ", array(''));
	if(count($cRev)>0){
		return $cRev;
	}
}

/*function cronTblAddUpdation($eventId){
	global $db;
	//Get event info
	$evInfo = getEventInfo($eventId);
	$eventTotalSeats = $evInfo['evNoOfTickets'];
	$eventTotalBookedSeats = $evInfo['evBookTicket'];
	
	$eventVacantSeats = $eventTotalSeats - $eventTotalBookedSeats;
	if($eventVacantSeats <0) {
		$eventVacantSeats =0;
	} else {
		$eventVacantSeats = $eventVacantSeats;
	}
	//Get total Booked Seats in Cron Tbl For Same EVent
	$totBkdTcktWRTEvent = getTotBkdTcktWRTEventId($eventId); 
	if($totBkdTcktWRTEvent < $eventVacantSeats){
		$cookieUserId = $_COOKIE['PHPSESSID'];
		setcookie("csid", $cookieUserId, time() + 3600 * 24*7, "/"); 
		//echo "<prE>COOKIE==";print_R($_COOKIE);die;
		
		$currentDt = date('Y-m-d H:i:s');
		$cron_exist_time_multiple_user = cron_exist_time_multiple_user; //from config file
		$currentDtAfter15MinFormat = strtotime("+ $cron_exist_time_multiple_user minutes", strtotime($currentDt));
		$currentDtAfter15Min  = date('Y-m-d H:i:s', $currentDtAfter15MinFormat);
		
		$evInfoArr = array(
			'eventId' =>       $evInfo['id'],
			'totalVacantSeats' =>  $eventVacantSeats,
			'bookedTicket' =>  $custOrder[0]['qty'],
			'cookieId' =>     $cookieUserId,
			'createdDate' =>  $currentDtAfter15Min,
			'createdDateTimeStr' =>  $currentDtAfter15MinFormat,
			'actualDT1stIns' =>  $currentDt
		);
			
		//First Check event is exist in bb_crondata tbl
		$isExist = isExistEventInCronTbl($eventId,$cookieUserId);
		//echo "<pre>isExist==";print_R($isExist);die;
		if(!$isExist) { //Insert data in bb_crondata tbl
			$insertCronId = $db->insert("bb_crondata",$evInfoArr);
		}
		else { //Update bb_crondata tbl for same event
			//Get booked seats for same user till now
			$bookedInfoWRTCOKIEUsr = getBkdTcktForCronWRTCookieUsr($eventId,$cookieUserId);
			$bookedTicket = $bookedInfoWRTCOKIEUsr['bookedTicket'];
			$bookedNewQty = $bookedTicket + $custOrder[0]['qty'];
			
			$updEvInfoArr = array('totalVacantSeats' => $eventVacantSeats,'bookedTicket'=>$bookedNewQty,'createdDate' => $currentDtAfter15Min,'createdDateTimeStr' => $currentDtAfter15MinFormat );
			$db->where('eventId', $evInfo['id']);
			$db->where('cookieId',$cookieUserId );
			$db->update("bb_crondata",$updEvInfoArr);
		}
		$dup=7777;
	} 
	else {
		return $dup=6;
	}
	return $dup;
}*/


///Prabhat
function getBkdTcktForCronWRTCookieUsrP($evId,$cookieUserId){
 global $db;
 $cRev = $db->rawQueryOne("select id,bookedTicket,createdDate,createdDateTimeStr,actualDT1stIns from bb_crondata where bookedTicket>0 and eventId ='".$evId."' and  cookieId ='".$cookieUserId."'", array(''));
 if(count($cRev)>0){
  return $cRev;
 }
}

//Generate Booking Id

function random_code($limit)
{
    return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
}

function genBookingId($studioId){

if($studioId<=9) {
$bookingId="00".$studioId."-".strtoupper(random_code(6));
}
if($studioId>=10 && $studioId<=99) {
$bookingId="0".$studioId."-".strtoupper(random_code(6));
}
if($studioId>=100) {
$bookingId=$studioId."-".strtoupper(random_code(6));
}
return $bookingId;
}



//PHP and mysql Injection functions

function StringInputCleaner($data)
{
	//remove space bfore and after
	$data = trim($data); 
	//remove slashes
	$data = stripslashes($data); 
	$data=(filter_var($data, FILTER_SANITIZE_STRING));
	return $data;
}


function IntegerInputCleaner($data)
{
	//remove space bfore and after
	$data = trim($data); 
	//remove slashes
	$data=(filter_var($data, FILTER_SANITIZE_NUMBER_INT));
	return $data;
}



function mysqlCleaner($data)
{
	$data= mysql_real_escape_string($data);
	$data= stripslashes($data);
	return $data;
	//or in one line code 
	//return(stripslashes(mysql_real_escape_string($data)));
}	

function makeASCII($char=0){
  return '&#'.ord($char).';';
}


function encodeEmail($email){
/*** check if the filter has a var ***/
if(filter_var($email, FILTER_VALIDATE_EMAIL) !== FALSE)
    {
    /*** split the email into single chars ***/
    $charArray = str_split($email);
    /*** apply a callback funcion to each array member ***/
    $encodedArray = filter_var($charArray, FILTER_CALLBACK, array('options'=>"makeASCII"));
    /*** put the string back together ***/
    $encodedString = implode('',$encodedArray);
    return $encodedString;
    }
else
  {
  return false;
  }
}

function StringEscapeSlug($slug){
	$unwanted_array = 
		array(
			'�'=>'S', '�'=>'s', '�'=>'Z', '�'=>'z', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'C', '�'=>'E', '�'=>'E',
			'�'=>'E', '�'=>'E', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'N', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'U',
			'�'=>'U', '�'=>'U', '�'=>'U', '�'=>'Y', '�'=>'B', '�'=>'Ss', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'c',
			'�'=>'e', '�'=>'e', '�'=>'e', '�'=>'e', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'o', '�'=>'n', '�'=>'o', '�'=>'o', '�'=>'o', '�'=>'o',
			'�'=>'o', '�'=>'o', '�'=>'u', '�'=>'u', '�'=>'u', '�'=>'y', '�'=>'b', '�'=>'y' 
		);
       
	$str = strtr($slug, $unwanted_array );
	$slug = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($str)));
	return $slug;
}
// for display on personalize label
function StringEscapeSlugDisplay($slug){
	$unwanted_array = 
		array(
			'�'=>'S', '�'=>'s', '�'=>'Z', '�'=>'z', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'C', '�'=>'E', '�'=>'E',
			'�'=>'E', '�'=>'E', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'N', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'U',
			'�'=>'U', '�'=>'U', '�'=>'U', '�'=>'Y', '�'=>'B', '�'=>'Ss', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'c',
			'�'=>'e', '�'=>'e', '�'=>'e', '�'=>'e', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'o', '�'=>'n', '�'=>'o', '�'=>'o', '�'=>'o', '�'=>'o',
			'�'=>'o', '�'=>'o', '�'=>'u', '�'=>'u', '�'=>'u', '�'=>'y', '�'=>'b', '�'=>'y' 
		);
       
	$str = strtr($slug, $unwanted_array );
	//$slug = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($str)));
	return $str;
}

function stripSlash($str) { //remove slashes function
	$output = preg_replace_callback("/(&#[0-9]+;)/", function($m) { return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES"); }, $str);

	//$str=preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i",'<$1$2>', $str);
	return $output;
}

//Ftech Project Class for no tax in gift cart case
function ftechProjClsByProjId($proId){
	global $db;
	$result3 = $db->rawQueryOne("SELECT proClassId FROM bb_project WHERE status=0 and isDeleted=0 and id=? ", array($proId));
	if(isset($result3) && !empty($result3)) {
		return $result3;
	} else {
		return 0;
	}
}


function getTimezoneDifference($studioTimezone,$serverTimezone){
	// This will return 10800 (3 hours) ...
	$offset = get_timezone_offset($studioTimezone,$serverTimezone);
	// or, if your server time is already set to 'America/New_York'...
	//$offset = get_timezone_offset($studioTimezone);
	// You can then take $offset and adjust your timestamp.
	//$offset_time = time() + $offset;
	$offset_time = $offset;
	return secondMinute($offset_time);
}

/**    Returns the offset from the origin timezone to the remote timezone, in seconds.
*    @param $remote_tz;
*    @param $origin_tz; If null the servers current timezone is used as the origin.
*    @return int;
*/
function get_timezone_offset($remote_tz, $origin_tz = null) {
    if($origin_tz === null) {
        if(!is_string($origin_tz = date_default_timezone_get())) {
            return false; // A UTC timestamp was returned -- bail out!
        }
    }
    $origin_dtz = new DateTimeZone($origin_tz);
    $remote_dtz = new DateTimeZone($remote_tz);
    $origin_dt = new DateTime("now", $origin_dtz);
    $remote_dt = new DateTime("now", $remote_dtz);
    $offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
    return $offset;
}

function secondMinute($seconds){
	/// get minutes
    $minResult = floor($seconds/60);
    /// if minutes is between 0-9, add a "0" --> 00-09
    if($minResult < 10){$minResult = 0 . $minResult;}
    /// return result
	return $minResult;
}


///Timezone code

if(empty($_SESSION['studioInfo'][0])) { //echo "ifff---".$stLoca;
$stLocaID=$_SESSION["stdLocIds"];
} else {  // echo "elseee---";
	$stLocaID=$_SESSION['studioInfo'][0]['locationId'];
}




if(isset($stLocaID) && $stLocaID!="") {
$pageinfoArr = getLocationIdbyslug($stLocaID);

if(!empty($pageinfoArr)) {
if(isset($pageinfoArr['stimeZone']) && $pageinfoArr['stimeZone']!= "" ){
	$studioTimezone = $pageinfoArr['stimeZone'];
} else {
	$studioTimezone = Studio_Default_Timezone; //studio default timezone
}


$serverTimezone = Server_Default_Timezone; //server default timezone
date_default_timezone_set($studioTimezone);
$timezoneDiffInMin = getTimezoneDifference($studioTimezone,$serverTimezone);
define('INTERVALTIME',' - INTERVAL '.intval($timezoneDiffInMin).' Minute'); //server interal time difference
}

} 


function multi_unique($src){
     $output = array_map("unserialize",
     array_unique(array_map("serialize", $src)));
   return $output;
}


?>