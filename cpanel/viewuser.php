<?php 
include('includes/header.php');
$menuClssUsers = "downarrow";
$menuSbClssUsers = "sub";
$menuSbClssUsers1 = "active";
$menuSbClssStyleUsers = "style='display:block;'";

if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 
if(isset($_SESSION['urole']) && $_SESSION['urole']!=2){ header("location: ".base_url_site."dashboard");    exit; } 

if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") {    
	$params = array('');
	$result = $db->rawQueryOne("SELECT * FROM bb_users WHERE id='".$_REQUEST["id"]."' ", $params);
	$ritems = (array)$result;
	if(!empty($ritems)) $ritems=$ritems; 
} else {
	$ritems=array();
	$ritems=$ritems;
}
?>
<!-- Header end-->
<style>
.token-input-list-facebook{ display:none; padding:7px 0px!important; box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box;}
</style>
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<script type="text/javascript" src="<?php echo base_url_js?>jquery.tokeninput.js"></script>
<link rel="stylesheet" href="<?php echo base_url_css?>token-input.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url_css?>token-input-facebook.css" type="text/css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>select-style.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>selectize.default.css">
<script type="text/javascript" src="<?php echo base_url_js?>selectize.js" /></script>
<!-- left nagivation end-->
<!-- left content area start-->
<div class="loading" style="display:none;">Loading&#8230;</div>
<div class="right-wrapper" id="frmChanged">
	<h1 class="pageheading">User-<?php echo @fnCheckEmpty($ritems['firstName']); ?></h1>
	<div class="form-area">
		<form role="form" method="post" action="" name="frmaddupUser" id="frmaddupUser">
			<input type="hidden" name="id" id="id" value="<?php echo $_REQUEST["id"]; ?>" />
			<input type="hidden" name="did" id="did" value="<?php echo $_REQUEST["did"]; ?>" />
			<?php 
			if((isset($_GET["stid"]) && $_GET["stid"]!="") && (!isset($_GET["did"]) && $_GET["did"]=="") && (!isset($_GET["id"]) && $_GET["id"]=="")) {?>
			<div class="input-row">
				<input type="checkbox" class="uexist" id="useExist" name="useExist" value="" onclick="javascript:valueChanged();" /> Use Existing User
			</div>
			<div class="input-row uEid" style="display:none;">
				<div class="control-group">
					<select id="txtExistU" name="txtExistU" class="left49">
						<option value="">Existing User</option>
							<?php echo fnMDropDownList("bb_users","id","email",@fnCheckEmpty($ritems['id']),"isActive=0 and isDeleted=0 and role=3");?>
					</select>
				</div>
				<script>
				$('#txtExistU').selectize();
				</script>
			</div>
			<?php }?>
			<div class="user-info">
				<div class="input-row">
					<div class="floatingBox left49"><input class="tempname" type="text" id="txtFirtName" name="txtFirtName" placeholder="" value="<?php echo @fnCheckEmpty($ritems['firstName']); ?>" /><label class="floating-lab">First Name</label></div>
					<div class="floatingBox right49"><input class="tempname" type="text" id="txtLastName" name="txtLastName" placeholder="" value="<?php echo @fnCheckEmpty($ritems['lastName']); ?>" /><label class="floating-lab">Last Name</label></div>
				</div>

				<div class="input-row">
					<div class="floatingBox left49"><input class="tempname" type="text" id="txtEmail" name="txtEmail" value="<?php echo @fnCheckEmpty($ritems['email']); ?>" <?php if(isset($_REQUEST["id"])) echo 'readonly="readonly"';?> placeholder="" /><label class="floating-lab">Email</label></div>
					<select class="right49 pdf-text tempname" id="isSend" name="isSend">
						<option value="">Select Invite</option>
						<option value="1" <?php if(isset($ritems['isInvite']) && $ritems['isInvite']==1) echo "selected"; ?>>Send Invite</option>
						<option value="2" <?php if(isset($ritems['isInvite']) && $ritems['isInvite']==2) echo "selected"; ?>>Save for Later</option>
					</select>
				</div>

				<div class="input-row">
					<?php 
					$roleid="";
					$stid="";
					if(isset($_GET["stid"]) && $_GET["stid"]!="") { 
						$roleid=3; 
						if(isset($_GET["id"]) && $_GET["id"]!="") { 
							$stid1=$_GET["stid"];
							$uinfo=getUserStudio($_GET["id"],$stid1);
							//print_r($uinfo);
							//die();
							if($uinfo) {
								$stid=$uinfo['studioLocation'].",".$stid1;
							}
						} else {
							$stid=$_GET["stid"]; 
							$strDis='readonly="readonly"';
						}
					} 
					else { 
						$roleid=@fnCheckEmpty($ritems['role']); 
						$stid=@fnCheckEmpty($ritems['studioLocation']); $strDis=''; 
					} ?>
				
					<select class="left49 pdf-text tempname" id="lstRole" name="lstRole">
						<option value="">Select User Type</option>
						<?php echo fnDropDownList("bb_userroles","id","roleName",$roleid,"");?>
					</select>
					<?php
					if(isset($stid) && $stid!="") { 
						$stlist = $db->rawQuery("SELECT id, stName FROM `bb_studiolocation` where id IN(".$stid.") and isDeleted=0 order by id asc", $params);
						$studiolist=array();
						$studiojlist=array();
						foreach ($stlist as $key=>$item) {
							$studiolist['id']=$item['id'];
							$studiolist['stName']=$item['stName'];
							$studiojlist[]=$studiolist;
						}
					}
					?>
					<!--<div class="floatingBox left49">-->
					<input type="text" class="right49" id="lstLocations" name="lstLocations"  placeholder="Studio Locations" />
					<!--</div>-->
					<script type="text/javascript">
					$(document).ready(function() {
						$("#lstLocations").tokenInput("addon/lstlocations.php", {
						propertyToSearch: "stName",
							theme: "facebook",
							tokenDelimiter: ",",
							preventDuplicates: true,
							<?php  if(isset($stid) && $stid!="") { ?> 
							prePopulate: <?php echo json_encode($studiojlist);?>
						<?php }?>	
						});
					});
					</script>
					
				</div>
				
			<input type="hidden" name="txtChanged" id="txtChanged" value="0" />
		</form>
		</div>
	</div>

<!-- left content area end-->

<script>
function valueChanged() {
    if($(".uexist").is(":checked"))   
        $(".uEid").show();
    else
        $(".uEid").hide();
}

$(document).ready(function() {
	$("#lstRole").on('change',function(){
		var sid="";
		sid=$(this).val();
		if(sid==3){
			$(".token-input-list-facebook").show();
			$("#token-input-lstLocations").hide();
			$(".token-input-delete-token-facebook").hide();
		} else {
			$(".token-input-list-facebook").hide();
		}
	});
	
	<?php if((isset($_GET["stid"]) && $_GET["stid"]!="") || (isset($roleid) && $roleid!="")) {?>
	$('#lstRole').change();
	<?php }?>

	<?php 
	//Disabled Class
	if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "" ) { ?>
		$('.tempname').attr('disabled', true);
		$('.tempname').css("background", "#e7e7e7");
	<?php 
	} ?>
	
 });
</script>
<?php
include('includes/footer.php');
?>