<?php 
include('includes/header.php');
$menuClssProject = "downarrow";
$menuSbClssProject = "sub";
$menuSbClssStyleProject = "style='display:block;'";
$menuClssProjecttem2 = "active";
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php"); exit; } 

if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") {  
	$params = array('');
	$result = $db->rawQueryOne("SELECT * FROM  bb_project WHERE id='".$_REQUEST["id"]."' ", $params);
	$ritems = (array)$result;
	if(!empty($ritems)) {
		$ritems = $ritems;
	}
	
	//Project History 
	$result1 = $db->rawQuery("SELECT * FROM  bb_proactivitylog WHERE lgProId='".$_REQUEST["id"]."' order by id desc", $params);
	$ritems1 = (array)$result1;
	
	$projectHistoryLogArr = array();
	if(!empty($ritems1)) {
		foreach($ritems1 as $key=>$val) {
		    //Get user name
			$result11 = $db->rawQuery("SELECT firstName FROM  bb_users WHERE id='".$val['lgUserId']."'", $params);
			$ritems11 = (array)$result11;
			$projectHistoryLogArr[] = "User ". $result11[0]['firstName']." ". $val['lgDescription']." on ". date('m/d/Y', strtotime($val['lgDate']))." at ".$val['lgTime'];
		}
	}
	$projectHistoryLogTxt = implode("\n",$projectHistoryLogArr);
	
	/*get project gallery code*/
	$resultGallerProduct = $db->rawQuery("SELECT * FROM  bb_progallery WHERE proId='".$_REQUEST["id"]."' order by id desc", $params);
	$ritemsGallery = (array)$resultGallerProduct;
	
	/*end of project galeery code */
} else {
	$ritems = array();
	$ritems = $ritems;
}
?>

<style>
.ph2_selection_row{float:left; width:100%; margin-bottom:15px;}
.ph2_selection-left{width:19%; float:left;}
.widthnew5{width:60px;}
.radioCls{margin:16px 0px;}
.append-row_1{float:left; width:100%;}
.remove_element{width:240px; border:1px solid #9e9e9e; display:inline-block; margin-right:20px; display:inline-block; /* border: 1px solid #9e9e9e; */ border-radius:5px; margin-bottom:15px;}
.remove_element .select2{width:100%!important;}
.remove_element input[type="text"]{border:none; width: 200px; }
.remove_element a{font-size: 19px; color: red; float:right; margin-right:10px; padding:10px 0px;}
.remove_element a:hover{font-size: 19px; color: red;}
.widthnew20{margin-right:20px;}
.mul_select_row{float:left; width:100%; margin-top:15px;}
.m_select_left{float:left; width:20%;}
.m_select_left select{margin-left:30px; width:177px;}
.m_select_right{float:right; width:90%;}
.m_select_right ul{ width:89%; float:left; margin:0px; padding:0px; border-radius:5px; border: 1px solid #9e9e9e; list-style:none;}
.m_select_right ul li{width:100%; padding:5px; display:inline-block; box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box;}
.m_select_right label{float:left; margin-right:10px;}
.select2-container--default .select2-selection--single{border:none!important;}
.removeImg{
	cursor:pointer;
}
<!-- galery css code -->
.multiSelectImage{float:left; margin-top:23px;}
.multiSelectImage i{color: #6fcf00; font-size: 50px;}
#result{display:inline-block; float:left;}
#result div{display:inline-block; width:200px; margin-right:15px;}
#result .thumbnail{width:100%;height:80%}

.grid li {
		line-height: 80px;
		float: left;
	    width: 100px;
        height: 120px;
		text-align: center;
		position:relative;
		border:none;
		list-style: none
	}
	
	.grid li a{position:absolute; right:0px; top:-8px;}
	.grid li a img{width:20px;}
.restrictionContainer{float:left; width:100%;}
.restriction-row{float:left; width:100%;}
.restriction_attr_selection{float:left; width:100%; margin-right:2%; margin-bottom:15px;}
.res_attr{    width: 21%!important;
    margin-bottom: 0px;
    margin-right: 10px;}
.res_close_btn{float: left;
    width: 7%;
    text-align: center;
    padding: 10px 0px;}
	
	.resstu_close_btn{
    width: 7%;
    text-align: center;
    padding: 10px 3px;}
	
.res_select_type, .specific_code_option, .specific_studio_list, .specific_studio_option, .specific_studio_name{margin-right:20px; margin-bottom:15px; width:19%; display:inline-block;}
.specific_studio_name{border-radius:5px; border: 1px solid #9e9e9e;}
.specific_studio_name input[type="text"]{border:none; width:87%;}
.res_amount{width:15%; margin-right:20px; margin-bottom:15px;}
.PC_add, .PS_add{color:#4a4a4a; text-decoration:none;}
.PC_add i, .PS_add i{color: #6fcf00; font-size: 17px;}

.ph2_tableEv{width:100%; border-collapse: collapse; border: 1px solid #9e9e9e;}
.ph2_tableEv th{background-color:#434343 !important;font-weight: bold; padding: 12px 10px; font-size: 16px; text-align: left; background-color: #434343; color: #fff;}
.ph2_tableEv td{padding:5px;}
.tb_img img{width:100px;}
.tb_anc{margin-right:10px;}
.tb_Price input[type="text"]{width:150px;}

.rpseb .select2{border:1px solid #9e9e9e; border-radius:5px;}

<!-- end of galeery code -->
</style>

<!-- Header end-->
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<!-- left nagivation end-->
<script type="text/javascript" src="<?php echo base_url_js?>additional-methods.js" /></script>
<!-- Date Time Picker Validation-->
<link href="<?php echo base_url_css?>datepicker/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url_js?>datepicker/jquery-ui.js"></script>
<link href="<?php echo base_url_css?>jquery.timepicker.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url_js?>jquery.timepicker.js"></script>

<!-- ToolTip Type Validation-->
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>bootstrap-3.3.5.min.css">
<script type="text/javascript" src="<?php echo base_url_js?>bootstrap-3.3.5.min.js"/></script>
<script type="text/javascript" src="<?php echo base_url_js?>jquery-validate.bootstrap-tooltip.min.js"/></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>select2.min.css">
<script type="text/javascript" src="<?php echo base_url_js?>select2.min.js" /></script>
<!-- for galeery image code -->

<!-- end of code -->

<script type="text/javascript" src="<?php echo base_url_js?>jquery.tokeninput.js"></script>
<script src="<?php echo base_url_js; ?>zepto.min.js"></script>
<script src="<?php echo base_url_js; ?>zepto.dragswap.js"></script>
<link rel="stylesheet" href="<?php echo base_url_css?>token-input.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url_css?>token-input-facebook.css" type="text/css" />
<!-- left content area start-->
<div class="loading" style="display:none;">Loading&#8230;</div>
<div class="right-wrapper">


<!-- end of studio selected -->
	<?php 
	if(isset($_REQUEST["id"]) && $_REQUEST["id"]!="") { ?>
		<h1 class="pageheading">Edit Project</h1>
	<?php } else {?>
		<h1 class="pageheading">Add Project</h1>
	<?php }?>
	<?php 
	if(isset($_REQUEST["id"]) && $_REQUEST["id"]!="") { ?>
	<div class="add-project-msg"></div> 
	<?php }?>
	<div class="form-area">
	<form role="form" method="post" action="" enctype="multipart/form-data" id="addUpdProjFrm">
	<!-- studio selected id  -->
		
		<div class="bnt-row">
			<div class="enbdis">
				<?php 
				if(isset($_REQUEST["id"]) && $_REQUEST["id"]!="") {  //Active
					if($ritems['status'] == 0){ //Active?>
						<button class="enbl-btn actionEvent" id="disableAction" alt="click for disable" title="click for disable">Disable</button>
					<?php 
					} else if($ritems['status'] == 1) { //Deactive ?>
						<button class="enbl-btn actionEvent" id="enableAction" alt="click for enable" title="click for enable">Enable</button>
					<?php } else { ?>
						<button class="enbl-btn actionEvent" id="disableAction" alt="click for disable" title="click for disable">Disable</button>
					<?php
					}
				} 
				if(!isset($_REQUEST["id"])){ // add case?>
					<button class="dsbl-btn actionEvent" id="enableAction" disabled alt="After save, you can use this" title="After save, you can use this">Enable</button>
				<?php 
				}?>
			</div>
			<?php 
			//edit case
			if(isset($_REQUEST["id"]) && $_REQUEST["id"]!="") { 
				if($ritems['status'] == 3){ //Archive?>
					<button class="yellow-btn actionEvent" id="unarchiveAction" alt="click for Un-archive" title="click for Un-archive">Un-Archive</button>
				<?php 
				} else { ?>
					<button class="yellow-btn actionEvent" id="archiveAction" alt="click for archive" title="click for archive" >Archive</button>
				<?php 
				} 
			} 
			if(!isset($_REQUEST["id"])){ //add case?>
				<button class="dsbl-btn actionEvent" id="archiveAction" disabled alt="After save, you can use this" title="After save, you can use this">Archive</button>
			<?php 
			}?>
		</div>
		
		<div class="bnt-row">
			<div class="btn-left">
				<?php 
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { //update case?>
					<button class="yellow-btn actionEvent" id="updateAction" type="submit">Update Changes</button>
				<?php } else { //add case?>
					<button class="yellow-btn actionEvent" id="addAction" type="submit">Save</button>
				<?php } ?>
			</div>
			<div class="btn-right">
				<button type="reset" id="btnCancel" class="grey-btn btn-mrg-right">Cancel</button>
				<?php 
				//edit case
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="" ) { ?>
				    <button class="grey-btn btn-mrg-right actionEvent" id="updatedraftAction" type="submit">Save Draft</button>
					<button class="grey-btn actionEvent" id="updatepreviewAction" type="submit">Preview</button>
				<?php 
				} else { //add case ?>
					<button class="grey-btn btn-mrg-right actionEvent" id="draftAction" type="submit">Save Draft</button>
					<button class="grey-btn actionEvent" id="previewAction" type="submit">Preview</button>
				<?php }?>
			</div>
		</div>
		
		<div class="template-info">
			<div class="input-row">
			    <?php 
				//edit case
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { 
					if($_REQUEST["type"] == "draft") { //edit case wit draft?>
					<input type="hidden" name="actionType" value="draftAction" id="actionType">
				<?php 
					} 
					else { //edit case?>
					<input type="hidden" name="actionType" value="updateAction" id="actionType">
				<?php 
					}
				} 
				if(!isset($_REQUEST["id"])) { //add case?>
					<input type="hidden" name="actionType" value="" id="actionType">
				<?php } ?>
				
				<input type="hidden" name="userId" value="<?php echo $_SESSION['usrid'];?>">
				<?php 
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { ?>
					<input type="hidden" name="projectId" id="projectId" value="<?php echo $_REQUEST["id"];?>">
				<?php } 
				?>
				<div class="floatingBox tempname"><input class="tempname" type="text" name="proName" id="proName" placeholder="" value="<?php echo fnCheckEmpty($ritems['proName']);?>" /><label class="floating-lab">Project Name</label></div>
				<select class="eventtype tempname" name="proTypeId" id="proTypeId">
					<option value="">Project Type</option>
					<?php echo fnDropDownListDType("bb_protype","id","proTypeName","dType",@fnCheckEmpty($ritems['proTypeId']),"");?>
				</select>
			</div>

			<div class="input-row">
				<select class="eventtype tempname" name="proClassId" id="proClassId">
					<option value="">Project Class</option>
					<?php echo fnDropDownListDType("bb_proclassoptions","id","opName","dType",@fnCheckEmpty($ritems['proClassId']),"");?>
				</select>
				<input class="sku tempname" type="text" name="proSku" id="proSku" placeholder="SKU" value="<?php echo fnCheckEmpty($ritems['proSku']);?>" style="display:none;"/>
			</div>
		</div>
		
		<div class="workshop-section removeMargin">
			<!--<h2 class="section-heading">Workshop</h2>-->
			<div class="input-row" style="margin-top:10px;">
				<div class="floatingBox left70"><textarea class="left70 tempname" placeholder="" name="proDetailedDesc" id="proDetailedDesc"><?php echo fnCheckEmpty($ritems['proDetailedDesc']);?></textarea><label class="floating-lab">Project Detailed Description</label></div>
				<?php 
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { ?>
				<div class="floatingBox right28"><input class="right28 tempname" type="text" placeholder="" name="proPrice" id="proPrice" value="<?php echo "$".fnCheckEmpty($ritems['proPrice']);?>" readonly/><label class="floating-lab">Price</label></div>
				<?php } else {?>
				<div class="floatingBox right28"><input class="right28 tempname" type="text" placeholder="" name="proPrice" id="proPrice" value="<?php echo fnCheckEmpty($ritems['proPrice']);?>" readonly/><label class="floating-lab">Price</label></div>
				<?php }?>
			</div>

			<div class="input-row">
				<div class="relative-div width30">
					<div class="control-group">
						<select id="answers" name="proPriCatId">
							<option value="">Primary Category</option>
								<?php echo fnDropDownListForPrimaryCat("bb_procategory","id","proCatName",@fnCheckEmpty($ritems['proPriCatId']),"");?>
						</select>
					</div>
					<script>
					<?php 
					//Edit Case with project is booked
					if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "" ) { ?>
						//$("#answers").select2({ width: '300px',disabled: true });
						$("#answers").select2({ width: '300px'});
					<?php 
					} else { ?>
						$("#answers").select2({ width: '300px' });
					<?php }?>
					$(document).on('change','#answers',function(value){ 
						if (value != '') {
							$(this).valid();
						}
						$(".select2-selection span").attr('title', '');
					});
					//studio_select
					$(document).on('change','.studio_select',function(value){
					      	$(this).next('span').remove();
						 //$(this).parent('div').hide();
						var idDiv = $(this).parent().attr('id');
						//alert(idDiv);
						var value = $(this).val();
						var val2=  $(".studio_select_val").val();
						//update by pankaj
						if(val2==""){
						  $(".studio_select_val").val(value);
						  
						}
						else {
							var val3 = val2+','+value;
							$(".studio_select_val").val(val3);
							
						}
						
							
							
						if (value != '') {
							$(this).valid();
						}

						
					$.ajax({
					url: "<?php echo base_url_site;?>/addon/listStudioOption",
					type: "post",
					data : {value:value},
					success: function (response) {
					var jsonData = $.parseJSON(response);
					var optionText  = jsonData.optionText; 
					//alert(optionText);
					$(".addCF").before(optionText);
				


					}
					});
						
						
					});
					
					//code_select
					$(document).on('change','.code_select',function(value){
						$(".rpseb").hide();
						$(this).next('span').remove();
					    //var divId = $(this).parent('div').attr('id');
					     // alert(divId);
						 //$(this).parent('div').hide();
						var idDiv = $(this).parent().attr('id');
						//alert(idDiv);
						var value = $(this).val();
						var val2=  $(".code_select_val").val();
						//alert(val2);
						//update by pankaj
						if(val2==""){
						  $(".code_select_val").val(value);
						  
						}
						else {
							var val3 = val2+','+value;
							$(".code_select_val").val(val3);
							
						}
						if (value != '') {
							$(this).valid();
						}
						
					$.ajax({
					url: "<?php echo base_url_site;?>/addon/listCodeOption",
					type: "post",
					data : {value:value},
					success: function (response) {
					var jsonData = $.parseJSON(response);
					var optionText  = jsonData.optionText; 
					$(".addCode").before(optionText);
					}
					});
						
						//$(".select2-selection span").attr('title', '');
					});
					</script>
					
					
					
				</div>
			</div>
			<style>
			.error ~ .select2-container{border: 1px solid red;}
			.valid ~ .select2-container{border: 1px solid green;}
			.add-project-msg{float:right; color:#6fcf00; font-weight:bold; display:none;}
			.prs-opt-singletxt{float:left; width:100%; margin-top:15px;}
			.prs-right-small input[type="radio"], .prs-right input[type="radio"]{margin-left:2.5%;}
			</style>
				
			<?php
			$params = array();
			if(isset($_REQUEST["id"]) and $_REQUEST["id"]!=""  ){  //edit case 
				if($ritems['proAltCat'] != "") { //if alt category is not blank
					$sqlQry = "SELECT id, proCatName FROM `bb_procategory` where id IN(".$ritems['proAltCat'].") and status=0 order by id asc";
				} 
				else { //if alt category is blank
					$sqlQry = "SELECT id, proCatName FROM `bb_procategory` where status=0 order by id asc";
				}
			} else { // add case 
				$sqlQry = "SELECT id, proCatName FROM `bb_procategory` where status=0 order by id asc";
			}
			$altcatlist = $db->rawQuery($sqlQry,$params);
			$altcatlistArr = array();
			$altcatjlistArr = array();
			foreach ($altcatlist as $key=>$item) {
				$altcatlistArr['id'] = $item['id'];
				$altcatlistArr['proCatName'] = $item['proCatName'];
				$altcatjlistArr[] = $altcatlistArr;
			}?>
			
			<div class="input-row">
            <span>Alt Categories</span>
            <div class="alcatrow">
               <input type="text" id="proAltCat11" name="proAltCat" class="tempname" placeholder="Alt Categories">
				</div>
			</div>
			
			<script type="text/javascript">
			$(".token-input-list-facebook").show();
			$("#proAltCat11").tokenInput("addon/altCategoryList.php", {
				hintText: "Please search alt category",
				propertyToSearch: "proCatName",
				theme: "facebook",
				tokenDelimiter: ",",
				preventDuplicates: true,
				<?php if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="" and $ritems['proAltCat'] != "" ){ ?> 	
					prePopulate: <?php echo json_encode($altcatjlistArr);?>
				<?php }?>	
			});
			
			
			</script>
			<style>
			ul.token-input-list-facebook { border:none; width:98%;margin-left:0px;}
			</style>
		</div>
		
		<div class="publishing-section">
			<h2 class="section-heading">Publishing</h2>
			<div class="input-row" id="projAddSchMore">
				<a href="javascript:void(0);" class="add-cat mrgtopleft0"><i class="fa fa-plus-circle" ></i><span>Add Schedule</span></a>
			</div>
			<?php 
			if(!isset($_REQUEST["id"])) { ?>
				<div class="proj_add_schedule_dynamic_content">
					<div class="prs-opt-row" id="proschli_0">
						<div class="prs-select" id="proschx_0">
							<select class="personalization-opt" name="proSchType">
								<option value="By Schedule" selected>By Schedule</option>
							</select>
						</div>
						<div class="prs-right" id="proschy_0">
                        
							<div class="floatingBox widthnew30"><input type="text" class="widthnew30 input_date" name="proSchStDate" id="proSchStDate" placeholder="" value="" autocomplete="off"/><label class="floating-lab">MM/DD/YYYY</label></div>
							<div class="floatingBox widthnew30"><input type="text" class="widthnew30 timepicker remspace" name="proSchStTime" autocomplete="off" id="proSchStTime" placeholder=""><label class="floating-lab">Start Time</label></div>
						</div>
					</div>
				</div>
			<?php 
			} else { ?>
				<div class="proj_add_schedule_dynamic_content">
					<div class="prs-opt-row" id="proschli_0">
						<div class="prs-select" id="proschx_0">
							<select class="personalization-opt tempname" name="proSchType">
								<option value="By Schedule" selected>By Schedule</option>
							</select>
						</div>
						<div class="prs-right" id="proschy_0">
							<div class="floatingBox widthnew30"><input type="text" class="widthnew30 input_date tempname" autocomplete="off" name="proSchStDate" id="proSchStDate" value="<?php if($ritems['proSchDtOptional'] !=1) { echo date("m/d/Y",strtotime($ritems['proSchStDate'])); } else { echo "";}?>" placeholder="" />
<label class="floating-lab">MM/DD/YYYY</label></div>
							<div class="floatingBox widthnew30"><input type="text" class="widthnew30 timepicker tempname" name="proSchStTime" id="proSchStTime" value="<?php if($ritems['proSchDtOptional'] !=1) { echo fnCheckEmpty($ritems['proSchStTime']);} else { echo "";}?>" placeholder=""  autocomplete="off"/><label class="floating-lab">Start Time</label></div>
						</div>
					</div>
				</div>
			<?php 
			}?>
			
			<div class="input-row">
				<div class="floatingBox textareafull"><textarea class="textareafull tempname" placeholder="" readonly><?php echo $projectHistoryLogTxt;?></textarea><label class="floating-lab">User: Created: Date Time</label></div>
			</div>
		</div>
		<style>
		#img-upload{position:absolute; top:-500px; display:none; }
		#upload-label{width:52px; height:60px; position:absolute; left:0; right:0; top:-50%; bottom:-50%; margin:auto;}
		.gallery-img {position:relative; display:block; height:auto; min-height:160px; }
		.gallery-img label{display:table-cell; vertical-align:middle; position:relative; z-index:10;}
		.gallery-img label i{display:table-cell; vertical-align:middle;}
		.gallery-img img{width:100%; position:relative; left:0px; margin:auto;}
		.gallery-img:hover #upload-label{visibility:visible!important;}
		#ui-datepicker-div, #ui-timepicker-div{z-index:1080!important;}
		</style>
		
		  <!-- code for gallery images -->
		<!------  gallery section area Start   ---->
	<?php 
	//pr($ritemsGallery);
	 	if(count($ritemsGallery)>0){
			$imageurl= s3_url_uimages."uploads/projects/700X600/".$ritems['proComGalImg'];
			$imageName = $ritems['proComGalImg'];
			echo '<input type="hidden" name="isfile" id="isfile" value="1">';
		}  
		else  {
			$imageurl=base_url_images."placeholder.jpg";
			$imageName="";
			echo '<input type="hidden" name="isfile" id="isfile" value="0">';
		} 
		?>
		
<div class="gallery-section">
	<h2 class="section-heading">Gallery</h2>
	<input type="hidden" name="flpup" value="<?php echo $imageName;?>" />
	<div class="ruff-img">
		<img src="<?php echo $imageurl?>" alt="" />
	</div>
		<style>
			.ruff-img{display:none; position:absolute; left:10000px;}
		</style>

<section>
	<div id="result">
		
		
		<ul class="sortable grid" id="imgul">
				<?php if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") {
					
						$ingg=s3_url_uimages."uploads/projects/700X600/".$imageName;			
		if(count($ritemsGallery)==0){		
				echo "<li id='imggal1' draggable='true'><a href='javascript:void(0);' class='remattr' imgid='".$compgalleryst[0]['id']."'><img src='images/cross-icon.png' /></a><img class='thumbnail' src='".$ingg."' id='ImgGal' name='ImgGal[]' alt='".$imageName."' ></li>";
		    }
			foreach($ritemsGallery as $galleryImg)
			{
				$ing=s3_url_uimages."uploads/projects/700X600/".$galleryImg['proImgName'];
				echo "<li id='imggal1' draggable='true'><a href='javascript:void(0);' name='".$galleryImg['proImgName']."' class='remattr' imgid='".$compimg['id']."'><img src='images/cross-icon.png' /></a><img class='thumbnail' src='".$ing."' id='ImgGal' name='ImgGal[]' alt='".$galleryImg['proImgName']."' ></li>";
				
			}
		
		}
		else
		{
			echo '<li id="img"><img  class="thumbnail" src="'.$imageurl.'"></li>';
			
		}
		
		?>	
		</ul>
	<input type="hidden" name="delimg" value="" id="delimg" />
	</div>
	<label class="multiSelectImage" for="files"><i class="fa fa-plus-circle"></i><input class="fileinput" id="files" name="files" style="visibility:hidden;" placeholder type="file"  multiple/></label>
</section>
</div>

<!------  end of code gallery section  ---->
		
		<div class="findprint-section">
			<h2 class="section-heading">Fine Print</h2>
			<div class="fndleft">
				<?php //edit case
				if(isset($_REQUEST["id"])) { 
					$searchString = ',';
					if( strpos($ritems['proFpPrintList'], $searchString) !== false ) {
						$checked_arr = explode(",",$ritems['proFpPrintList']);
					} else {
					  $checked_arr[] = $ritems['proFpPrintList'];
					}
				}
				$params = array('');
				$results = $db->rawQuery("SELECT id,proFinePrintName FROM  bb_profineprintoptions where status=0 order by id ASC", $params);
				if(count($results)>0){
					foreach($results as $val) { 
					    //edit case
						if(isset($_REQUEST["id"])) { 
							$checked = "";
							if(in_array($val["id"],$checked_arr)){
								$checked = "checked";
							}
						} ?>
						<div class="input-row">
							<input type="checkbox" value="<?php echo $val["id"];?>" name="proFpPrintList[]" class="proFpPrintListCls" style="margin-right:5px;" <?php if(isset($_REQUEST["id"])) { echo $checked;}?>/><?php echo $val["proFinePrintName"];?>
						</div>
					<?php }
				} ?>
				
			</div>
			<div class="fndright">
				<div class="input-row">
					<div class="floatingBox tareacustom" style="margin-top:5px;"><textarea class="tareacustom tempname" name="proFpPrintCustom" id="proFpPrintCustom" placeholder="" ><?php echo @fnCheckEmpty($ritems['proFpPrintCustom']);?></textarea><label class="floating-lab">Custom Entry</label></div>
				</div>
			</div>
		</div>
		 <!-- added new html by pradep -->
		 <div class="personalization-options-section">
			<h2 class="section-heading">Personalization Options</h2>
					
			<?php //Edit Case
			if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "") { ?>					<?php 
				$proPersitems = fetchTblDataByIdForProjectSection_1("bb_propersoptions","bb_prooptionadd","proId",$_REQUEST["id"]);
				//echo '<pre>';print_r($proPersitems);echo "</pre>"; 
				 
				if(!empty($proPersitems) && is_array($proPersitems)) {
					$count_add_1=0;
					foreach($proPersitems as $key=>$val){
						    //pr($val);
							$option_value = explode(",",$val['optionValues']); 
					?>
				<div class="prs-opt-row" id="perslistli_<?php echo $key;?>">
					<div class="prs-select" id="perslistx_<?php echo $key;?>">
				
					<a href="javascript:void(0);"><i class="fa fa-times-circle removeSpecificRow" style="font-size:19px;color:red;"></i></a>
					<select class="personalization-opt tempname" name="proOptionVal[]" id="proOptionVal<?php echo $key;?>">
						<option value="">Personalization Options</option>
						<option value="1" <?php if ($val['proOptionVal'] == 1) echo ' selected="selected"'; ?>>Text</option>
						<option value="2" <?php if ($val['proOptionVal'] == 2) echo ' selected="selected"'; ?>>Date</option>
						<option value="3" <?php if ($val['proOptionVal'] == 3) echo ' selected="selected"'; ?>>Multi-line Text</option>
						<option value="4" <?php if ($val['proOptionVal'] == 4) echo ' selected="selected"'; ?>>Selections Groups</option>
						<option value="6" <?php if ($val['proOptionVal'] == 6) echo ' selected="selected"'; ?>>Dropdown List</option>
					</select>
					</div>
				
					<div class="prs-right" id="perslisty_<?php echo $key;?>"> 
						<label>Label<input style="width:89%;" type="text" name="proLabel[]" id="proLabel<?php echo $key;?>" placeholder="Enter label" value="<?php echo fnCheckEmpty($val['proLabel']);?>" class="uniquelabel remspace"/>
						</label>
						<?php if ($val['proOptionVal'] == 1 || $val['proOptionVal'] == 3){ ?>
								<label><input type="radio" class="radioCls" name="proOptVal_<?php echo $key;?>[]" value="All Caps" <?php if($val['proOptVal']== 'All Caps') { echo "checked";}?>>All Caps</label>
								<label><input type="radio" class="radioCls" name="proOptVal_<?php echo $key;?>[]" value="All Lower Case" <?php if($val['proOptVal']== 'All Lower Case') { echo "checked";}?>>All Lower Case</label>
								<label><input type="radio" class="radioCls" name="proOptVal_<?php echo $key;?>[]" value="Title Case" <?php if($val['proOptVal']== 'Title Case') { echo "checked";}?>>Title Case</label>
							<?php } ?>
							
							<?php if ($val['proOptionVal'] == 2){ ?>
								<label><input type="radio" class="radioCls" name="proOptVal_<?php echo $key;?>[]" value="MM-DD-YY" <?php if($val['proOptVal']== 'MM-DD-YY') { echo "checked";}?>>MM-DD-YY</label>
								<label><input type="radio" class="radioCls" name="proOptVal_<?php echo $key;?>[]" value="MM-DD-YYYY" <?php if($val['proOptVal']== 'MM-DD-YYYY') { echo "checked";}?>>MM-DD-YYYY</label>
								<label><input type="radio" class="radioCls" name="proOptVal_<?php echo $key;?>[]" value="MM-YY" <?php if($val['proOptVal']== 'MM-YY') { echo "checked";}?>>MM-YY</label>
								<label><input type="radio" class="radioCls" name="proOptVal_<?php echo $key;?>[]" value="YYYY" <?php if($val['proOptVal']== 'YYYY') { echo "checked";}?>>YYYY</label>
								<label><input type="radio" class="radioCls" name="proOptVal_<?php echo $key;?>[]" value="YY" <?php if($val['proOptVal']== 'YY') { echo "checked";}?>>YY</label>
							<?php } ?>
					
					</div>
				<div class="prs-opt-singletxt" id="perslistz_<?php echo $key;?>"> <?php if($val['proOptionVal']==4){
						?>
					<div class="ph2_selection_row">
						<div class="ph2_selection-left">
							<label>Min 
								<input type="text" style="margin-right:15px;" class="widthnew5 min_class" id="min<?php echo $key;?>" value="<?php echo $val['min']; ?>" name="min_<?php echo $key;?>[]" placeholder="min">
							</label> 
							<label>Max 
								<input type="text" class="widthnew5 max_class" id="max<?php echo $key;?>" value="<?php echo $val['max']; ?>" name="max_<?php echo $key;?>[]" placeholder="max"> 
							</label>
						</div>
						<div class="prs-right-single-text">
							<label>Description <input type="text" class="remspace" id="proHoverTxt<?php echo $key; ?>" value="<?php echo $val['description']; ?>" name="description_<?php echo $key;?>[]" placeholder="Enter Description"> </label>
						</div>
					</div>
				<?php } ?>
					<div class="ph2_selection_row">
						<div class="ph2_selection-left">
						<?php 
							 
							$checkvaluesHorizontal="";
							$checkvaluesVertical="";
							if($val['proOptVal']=='vertical'){
								$checkvaluesVertical='checked="checked"';
								$checkvaluesHorizontal="";
								
							}
							else if($val['proOptVal']=='horizontal'){
								$checkvaluesHorizontal='checked="checked"';
								$checkvaluesVertical="";
							}
							if($val['proOptionVal']==4){
						?>
							<label style="margin-right:25px; float:left; margin-top:12px;">
								<input <?php echo $checkvaluesVertical; ?> type="radio" class="proOptVal_<?php echo $key; ?> radioCls" name="proOptVal_<?php echo $key; ?>[]" value="vertical">Vertical
							</label> 
							<label style=" float:left; margin-top:12px;"> 
							<input <?php echo $checkvaluesHorizontal; ?> type="radio" class="proOptVal_<?php echo $key; ?> radioCls" name="proOptVal_<?php echo $key; ?>[]" value="horizontal">Horizontal</label>
							<?php } ?>
						</div>
					<div class="prs-right-single-text">
						<label>Hover Text <input type="text" class="remspace" id="proHoverTxt<?php echo $key; ?>" value="<?php echo $val['proHoverTxt']; ?>" name="proHoverTxt[]" placeholder="Enter hover text"> 
						</label>
					</div>
					</div>
				</div>
					<?php 
						 
						if(!empty($val['optionValues'])){?>
						<div class="append-row_1" id="<?php echo $key;?>">
							<label>Options</label>
						<?php if($val['proOptionVal']==6){?>
						<select data-placeholder="Choose option..." class="chosen-select" name="option_per_<?php echo $key; ?>[]" multiple tabindex="4">
					<?php 
					
					$option_list_array=['option1'=>'option1','option2'=>'option2','option3'=>'option3'];
					//pr($option_value);
					if(!empty($option_list_array) && is_array($option_list_array)) {
							$count_add_drop=0;
					foreach($option_list_array as $val){
							//echo "value is".;
					if($val==$option_value[$count_add_drop]){
						$sel="selected='selected'";
					}
					else {
						$sel="";
					}
					?>
						<option value="<?php echo $val  ?>"  <?php echo $sel; ?>><?php echo $val  ?></option>
						
						
							<?php $count_add_drop++;} } ?>
							</select>
						<?php } else {?>
							<?php 
								
							if($option_value) {
							$count_add=0;
							
					foreach($option_value as $val){ 
						 
					?>
						<div class="remove_element"><input type="text" placeholder="option1" id="option_per<?php echo $count_add; ?>" value="<?php echo $val; ?>"  name="option_per_<?php echo $key; ?>[<?php echo $count_add; ?>]" class="widthnew20"/>
						<a href="javascript:void(0);"><i class="fa fa-times-circle removeSpecificRow_1"></i></a>
						
						
						</div>
						
						<?php 
							$count_add++;
						} 
						
						} ?>
						
							<a id="projSpecAddMore_1"  style="color: #6fcf00; " href="javascript:void(0);"><i style="font-size: 20px;" class="fa fa-plus-circle" ></i>Add Option</a>
						<?php } ?>
					
					
						
					</div>
						<?php } ?>
				
					
					
					
				</div>
				<?php 
					} 
					} 
				} 
				
				?><?php 
			//Add Case
			if(!isset($_REQUEST["id"]) ){ ?>
				<div class="prs-opt-row" id="perslistli_0">
					<div class="prs-select" id="perslistx_0">
						<a href="javascript:void(0);"><i class="fa fa-times-circle" style="font-size:19px;color:red;display:none;"></i></a>
						<!--<a href="javascript:void(0);"><i class="fa fa-times-circle removeSpecificRow" style="font-size:19px;color:red;"></i></a>-->
						<select class="personalization-opt tempname" name="proOptionVal[]" id="proOptionVal0" >
							<option value="">Personalization Options</option>
							<option value="1">Text</option>
							<option value="3">Multi-line Text</option>
							<option value="2">Date</option>
							<option value="4">Selection Groups</option>
							<option value="6">Dropdown List</option>
							
							
						</select>
					</div>
					<div class="prs-right" id="perslisty_0" style="display:none;">
						<label>Label<input type="text" name="proLabel[]" class="uniquelabel tempname remspace" id="proLabel0" placeholder="Enter label" value=""/></label>
						<label><input type="radio" class="radioCls" name="proOptVal_0[]" value="All Caps">All Caps</label>
						<label><input type="radio" class="radioCls" name="proOptVal_0[]" value="All Lower Case">All Lower Case</label>
						<label><input type="radio" class="radioCls" name="proOptVal_0[]" value="Title Case">Title Case</label>
						<input type="hidden" name="proOptValHidden[]" id="proOptValHidden0" value=""/>
					</div>
					<div class="prs-opt-singletxt" id="perslistz_0" style="display:none;">
						<div class="prs-right-single-text">
							<label>Hover Text<input type="text" class="tempname remspace" name="proHoverTxt[]" id="proHoverTxt0" value="" placeholder="Enter hover text"/></label>
						</div>
					</div>
				</div>
				<div class="personalize-dynamic-content"></div>
			<?php 
			} else {?> 
				<div class="personalize-dynamic-content"></div>
			<?php }?>
			<!--Edit Case-->
			<div class="insert-ticket"><a href="javascript:void(0);"><i id="personalizationAddMore" class="fa fa-plus-circle"></i></a></div>
		</div>
		 <!-- end of code for html -->
		 <!-- code start for restrictions -->
		<!-- restrication section pradeep html -->

				<div class="last_template-full_section">
					<h2 class="section-heading">Restrictions</h2>
					  <?php 
				//edit case
				   $studio_select_val="";
				   $res_count=0;
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { 
					$proRestitems = getRestrications("bb_prorestrictions","proId",$_REQUEST["id"]);
						
					?>
					<div class="restrictionContainer">
						<?php if(!empty($proRestitems)) {
							
							foreach($proRestitems as $values){
								//
								//pr($values);
								$selCoupon="";
								$selCoupon="";
								$selCouponFix="";
								$selCouponPer="";
								$selCouponCode="";
								if($values['rsOptionVal']==1){
									$selCoupon='selected="selected"';
									$selStudio ='';
								}
								else if($values['rsOptionVal']==2){
									$selStudio='selected="selected"';
									$selCoupon ='';
								}
								if($values['rsCouponType']==1){
									$selCouponFix='selected="selected"';
									$selCouponPer ='';
									$selCouponCode='';
								}
								else if($values['rsCouponType']==2){
									$selCouponPer='selected="selected"';
									$selCouponFix ='';
									$selCouponCode='';
								}
								else if($values['rsCouponType']==3){
									$selCouponPer='';
									$selCouponFix ='';
									$selCouponCode='selected="selected"';
									
								}
							?>

					<div class="restriction-row">
						<?php 
						//pr($values);
						if($values['rsOptionVal']==1){
							
						?>
						<div class="restriction_attr_selection">
							<a href="javascript:void(0);" class="res_close_btn"><img src="images/cross-icon.png"></a>
						<select name="resOptionVal[]"  class="res_attr valid" aria-invalid="false"><option value="0">Select Option</option>
							<option value="1"   <?php echo $selCoupon; ?> >Coupon</option>
						</select>
							<select  class="res_select_type valid" id="restopt" name="resCouponType[]" aria-invalid="false"><option value="0">Option</option><option <?php echo $selCouponFix; ?> value="1">Fixed Amount</option><option <?php echo $selCouponPer; ?> value="2">Percent</option>
							<option <?php echo $selCouponCode; ?> value="3">Code</option>
							</select>
							<?php if(!empty($values['code_id'])){
						?>
					
							<?php 
							$code_select_val=  $values['code_id'];
							$explode_Select_val = explode(",",$code_select_val);
							$count_up_restrictions= count($explode_Select_val);
							$count_up_restrictions++;
							$count_remaining = getCodeList_remaining($values['code_id']);
							if($count_remaining>0){
							?>
						
							<select id="code_res_select_<?php echo $count_up_restrictions; ?>" class="code_select" name="code_<?php echo $count_up_restrictions; ?>[0]">
							
							<?php echo getCodeList_2($values['code_id']);?>
							<script type="text/javascript">
								$("#code_res_select_<?php echo $count_up_restrictions ?>").select2({ width: '20%' });
							</script>
							</select>
							
							<?php } else { ?>
							<span id="code_res_select_<?php echo $count_up_restrictions; ?>"></span>
							<span id="codwwwe_res_select_<?php echo $count_up_restrictions; ?>"></span>
							
							
							<?php } ?>
							
								<a href="javascript:void(0);" class="addCode" style="color:#6fcf00;font-size:25px;margin-left: 3px;"><i class="fa fa-plus-circle"></i></a>
						
						<div class="code_attr_selection">

							<?php 
							$code_select_val=  $values['code_id'];
							//$code_select_val="";
							$explodeCodeId = explode(",",$values['code_id']);
							if(!empty($explodeCodeId)){
							foreach($explodeCodeId as $valueIds){
							//echo $valueIds;
							$getCodename = getCodeNameFromId('bb_code','id',$valueIds);

							?>
							<div class="remove_element">
							<input type="text"  value="<?php echo $getCodename[0]['code'] ?>">
							<a href="javascript:void(0);" id="<?php echo $valueIds; ?>"><i class="fa fa-times-circle removeSpecificRow_2"></i></a>
							</div>

							<?php }
							}
							?>
							
							</div>
			
							<?php } ?>
							
						
							<?php  if($values['rsCouponType']==2 || $values['rsCouponType']==1){ ?>
							<div id="t1" style="float: right;width: 46%;"><input type="text" name="amount[]" class="widthnew30  remspace " id="Percentpr" placeholder="" value="<?php echo !empty($values['couponAmount'])?$values['couponAmount']:'' ?>">
							</div>
							<?php }
							
						if(!empty($values['code_id'])){
						?>
							


					 <?php  }  ?>

		
						
						</div>
				
					<?php } else {

						if(!empty($values['userId'])){
						?>
						<div class="restriction_attr_selection"><a href="javascript:void(0);" class="res_close_btn"><img src="images/cross-icon.png"></a>
						<select  class="res_attr valid"  name="resOptionVal[]" aria-invalid="false">
								<option value="0">Select Option</option>
									<option  <?php echo $selStudio; ?> value="2">Studio</option>
							</select>
							<?php 
							$studio_user_select_val=  $values['user_id'];
							$explode_Select_user_val = explode(",",$studio_user_select_val);
							$count_up_user_id= count($explode_Select_user_val);
							$count_up_user_id++; ?>
						<select id="studio_res_select_<?php echo $count_up_user_id; ?>" class="studio_select" name="studioid_<?php echo $count_up_user_id; ?>[0]"><?php echo getStudioList();?></select>
						<a href="javascript:void(0);" class="addCF" style="color:#6fcf00;font-size:25px;margin-left: 3px;"><i class="fa fa-plus-circle"></i></a>
						<script type="text/javascript">
						   $("#studio_res_select_<?php echo $count_up_user_id; ?>").select2({ width: '20%' });
						</script>
							<?php 
							$studio_select_val= $values['userId'];
							
							
							$explodeStudioId = explode(",",$values['userId']);
							if(!empty($explodeStudioId)){
								foreach($explodeStudioId as $valueIds){
									//echo $valueIds;
								$getStudiosIds = getStudioNameFromId('bb_studiolocation','id',$valueIds);
								//pr($getStudiosIds);
							?>
						<div class="remove_element">
							<input type="text"  value="<?php echo $getStudiosIds[0]['name'] ?>">
							<a href="javascript:void(0);" id="<?php echo $valueIds; ?>"><i class="fa fa-times-circle removeSpecificRow_1"></i></a>
						</div>
						
						<?php }
							}
						?>
	</div>


					 <?php  }  ?>
						</div>

					<?php  $res_count++;
				}  } ?>
				</div>
				<?php 
				} ?>
				</div>
				
					
					
				<?php  } else { ?>
					
					<div class="restrictionContainer">
					</div>
				 <?php }?>
					<div class="insert-ticket"><a class="restriction_addOpt" href="javascript:void(0);"><i class="fa fa-plus-circle"></i></a>
					</div>
				</div>
				<!-- end of code -->
<input type="hidden" value="<?php echo !empty($studio_select_val)?$studio_select_val:'' ?>" name="studio_select_val" class="studio_select_val">
<input type="hidden" value="<?php echo !empty($code_select_val)?$code_select_val:'' ?>" name="code_select_val" class="code_select_val">
			
 
		 <!-- end of code of restrictions -->
		
		
		<div class="last_template-full_section">
			<h2 class="section-heading">Specifications</h2>
			<div class="prs-opt-row">
				<div class="specification-btn-row">
					<div class="floatingBox"><input type="text" name="proSpecWi" class="tempname" id="proSpecWi" placeholder="" value="<?php echo fnCheckEmpty($ritems['proSpecWi']);?>"><label class="floating-lab">Width</label></div>
					<div class="floatingBox"><input type="text" name="proSpecHi" class="tempname" id="proSpecHi" placeholder="" value="<?php echo fnCheckEmpty($ritems['proSpecHi']);?>"><label class="floating-lab">Height</label></div>
					<div class="floatingBox"><input type="text" name="proSpecDe" class="tempname" id="proSpecDe" placeholder="" value="<?php echo fnCheckEmpty($ritems['proSpecDe']);?>"><label class="floating-lab">Depth</label></div>
					<div class="floatingBox"><input type="text" name="proSpecWe" class="tempname" id="proSpecWe" placeholder="" value="<?php echo fnCheckEmpty($ritems['proSpecWe']);?>"><label class="floating-lab">Weight</label></div>
				</div>
			</div>

			<div class="prs-opt-row">
				<div class="specification-list-row">
					<ul>
					<?php //Edit Case
					if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "") {?>
						<?php 
						$proSpecitems = fetchTblDataByIdForProjectSection("bb_prospecifications","proID",$_REQUEST["id"]);
						
						if(!empty($proSpecitems) && is_array($proSpecitems)) {
							foreach($proSpecitems as $key=>$val){ 
							?>
						<li id="listli_<?php echo $key;?>">
							<div id="listx_<?php echo $key;?>" class="specification-list">
								<a href="javascript:void(0);"><i class="fa fa-times-circle remove" style="font-size:19px;color:red;<?php if($key==0) {?>display:none; <?php }?>"></i></a>
								<select class="specification-list_dropdown unique tempname" name="specOptionVal[]" id="specOptionVal<?php echo $key;?>">
									<option value="">Specification options</option>
									<?php echo fnDropDownList("bb_prospcoptions","id","opName",$val["specOptionVal"],"");?>
								</select>
							</div>
							
							<?php 
							$options = fnDropDownList2ndSpecificationOptionsdType("bb_prospcsizemeasure","id","sizename","id_type","dType",$val["specOptionVal"],$val["specType"]);
							if($options != ""){
							?>
								<div id="listy_<?php echo $key;?>" class="specification-list">
									<select class="specification-list_dropdown100 uniqueBoard tempname" name="specType[]" id="specType<?php echo $key;?>" >
										<?php echo fnDropDownList2ndSpecificationOptionsdType("bb_prospcsizemeasure","id","sizename","id_type","dType",$val["specOptionVal"],$val["specType"]);?>
									</select>
								</div>
							<?php 
							}  
							else { ?>
								<div id="listy_<?php echo $key;?>" class="specification-list" style="display:none;">
									<select class="specification-list_dropdown100 uniqueBoard tempname" name="specType[]" id="specType<?php echo $key;?>" >
										<option value="">select option</option>
									</select>
								</div>
							<?php }?>
							
							
							<div id="listz_<?php echo $key;?>" class="specification-list-qty" <?php if($val["specOptionVal"] == 4){ echo "style='width:120px;'";} else { echo "style='width:100px;'";}?>>
								<label>
								<?php 
								if($val["specOptionVal"] == 4){  //specification type= wire
									echo "Length";
								} else {
									echo "Qty";
								} ?>
								<input type="text" class="tempname" placeholder="" name="specTypeVal[]" value="<?php echo $val["specTypeVal"];?>"  id="specTypeVal<?php echo $key;?>" required></label>
							</div>
						</li>
					<?php 
							} //end foreach
						} // end if
						else { ?>
							<li id="listli_0">
								<div id="listx_0" class="specification-list">
									<a href="javascript:void(0);"><i class="fa fa-times-circle" style="font-size:19px;color:red;display:none;"></i></a>
									<select class="specification-list_dropdown unique" name="specOptionVal[]" id="specOptionVal0" >
										<option value="">Specification options</option>
										<?php echo fnDropDownList("bb_prospcoptions","id","opName","","");?>
									</select>
								</div>
								
								<div id="listy_0" class="specification-list" style="display:none;">
									<select class="specification-list_dropdown100 uniqueBoard" name="specType[]" id="specType0" >
										<option value="">select option</option>
									</select>
								</div>
								<div id="listz_0" class="specification-list-qty" style="display:none;">
									<label>Qty<input type="text" placeholder="" name="specTypeVal[]" id="specTypeVal0" ></label>
								</div>
							</li>	
						<?php
						} //end else 
					}?>
					
					<?php 
					//Add Case
					if(!isset($_REQUEST["id"]) ){ ?>
						<li id="listli_0">
							<div id="listx_0" class="specification-list">
								<a href="javascript:void(0);"><i class="fa fa-times-circle" style="font-size:19px;color:red;display:none;"></i></a>
								<select class="specification-list_dropdown unique" name="specOptionVal[]" id="specOptionVal0" >
									<option value="">Specification options</option>
									<?php echo fnDropDownList("bb_prospcoptions","id","opName","","");?>
								</select>
							</div>
							
							<div id="listy_0" class="specification-list" style="display:none;">
								<select class="specification-list_dropdown100 uniqueBoard" name="specType[]" id="specType0" >
								</select>
							</div>
							<div id="listz_0" class="specification-list-qty" style="display:none;">
								<label>Qty<input type="text" placeholder="" name="specTypeVal[]" id="specTypeVal0" ></label>
							</div>
						</li>
					<?php }?>
					</ul>
				</div>
			</div>
			
			<!--Edit Case-->
			<div class="insert-ticket"><a href="javascript:void(0);"><i class="fa fa-plus-circle" id="projSpecAddMore"></i></a></div>
		</div>
	</form>
	</div>
</div>
<!-- left content area end-->
<script>
$(document).on("click","#btnCancel",function() { 
	<?php if($_REQUEST["id"] != ''){ //edit  case ?>
		var status = '<?php echo $ritems['status'];?>';
		if(status == '2'){  //draft
			window.location.href="<?php echo base_url_site ?>projectlist?type=draft";
		}
		if(status == '3'){ //archive
			window.location.href="<?php echo base_url_site ?>projectlist?type=archive";
		} 
		else if(status == '0' || status == '1'){ //active or inactive
			window.location.href="<?php echo base_url_site ?>projectlist";
		}
	<?php } else { //add  case?>
		window.location.href="<?php echo base_url_site ?>projectlist";
	<?php }?>
});

$(document).on('click','.enbl-btn',function(){
	$(this).toggleClass('dsbl-btn');
	$(this).toggleClass('enbl-btn');
});

/********Image Upload Section *********
*************************************************
*****************************************/
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#blah').attr('src', e.target.result);
		};
		reader.readAsDataURL(input.files[0]);
		//$("#upload-label").css({'visibility' : 'hidden'});
	}
}
<?php if($_REQUEST["id"] != ''){?>
	var fetchProjId = '<?php echo $_REQUEST["id"];?>';
<?php } else {?>
	var fetchProjId = '';
<?php }?>


/********Add schedule Section *********
*************************************************
*****************************************/
$(document).on("click",".input_date",function(){
	$(this).datepicker({showOn:"focus",autoclose:true,changeMonth: true,changeYear: true, yearRange: '1950:2050'}).focus();
	$(this).attr("autocomplete", "off");
	$(this).attr("value", "");
	$(this).datepicker({minDate: "0",showOn:"focus",autoclose:true,changeMonth: true,changeYear: true, yearRange: '1950:2050'}).on("change", function() {
        $(this).valid(); 
	});
});

$(document).on("change",".input_date",function(){
	if($(this).val() != " "){
		$(this).siblings("label").addClass("float");
	}
});

/*$(document).on("click",".timepicker",function(){
	$(this).attr("value", " ");
});*/

$('.timepicker').timepicker({
    timeFormat: 'h:mm p',
    interval: 60,
    minTime: '12am',
    maxTime: '11:00pm',
    defaultTime: 'minTime',
    startTime: '12:00am',
    dynamic: false,
    dropdown: true,
    scrollbar: true,
});

//floating label for time picker
$(document).on("click","#proSchStTime",function(){
	$(".ui-timepicker-container").addClass("schs");
});

$(document).on("click",".schs .ui-menu-item",function(){
	$("#proSchStTime").siblings("label").addClass("float");
});	

$(document).on("click",".remspace",function() {
	var str = $(this).val();
	var str = $.trim(str);
	var newStr = str.replace(/\s+/g,' ').trim();
	$(this).val(newStr);
});

/*$('#proSchStTime').on('changeTime', function() {
$(document).on("changeTime","#proSchStTime",function() {
		//$('#onselectTarget').text($(this).val());
		var a1 = $(this).val();
		console.log('a@@@@='+a1);
	});*/


$.validator.addMethod("existDateThanTime", function(value, element, param) { 
	if($(param).val()== "" &&  value == "") {
		return true;
	} else if($(param).val()!= "" &&  value != "") {
		return true;
	} else if( $(param).val()!= "" &&  value == ""){
		return false;
	} 
},'Please enter a valid time.');


$.validator.addMethod("checkTimeFormat",function(a,b){
	if(a!=""){
		//var aasss = /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(\ ?[AP]M)$/i.test(a);
		return this.optional(b) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(\ ?[AP]M)$/i.test(a);
	} else {
		return true;
	}
},"Please enter a valid time.");



/********Project Personalization Section *********
*************************************************
*****************************************/

/*Project Personalization Clone*/
<?php //Edit Case
if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "") { 

    $proPersitems = fetchTblDataByIdForProjectSection("bb_propersoptions","proId",$_REQUEST["id"]);
    $proRestrictions = getRestrications("bb_propersoptions","proId",$_REQUEST["id"]);

	if(!empty($proPersitems) && is_array($proPersitems)) {?>
		var perfieldId = '<?php echo count($proPersitems)-1;?>';
		//var resfieldId = '<?php echo count($proRestitems)-1; ?>';
	<?php 
	}  else { // if no data ?>
		var perfieldId = 0;
		var resfieldId = 0;
	<?php
	}

	if(!empty($proRestrictions) && is_array($proRestrictions)) {?>
		var resfieldId = '<?php echo count($proRestrictions)-1;?>';
		//var resfieldId = '<?php echo count($proRestitems)-1; ?>';
	<?php 
	}  else { // if no data ?>
		var perfieldId = 0;
		var resfieldId = 0;
	<?php
	}

} else { //add Case?>
	var perfieldId = 0;
	var resfieldId = 0;
<?php 
} ?>

/*Tooltip respositioning when screen is changed*/
$.fn.tooltip.Constructor.prototype.recalculatePosition = function(){
    var $tip = this.tip()
    if($tip.is(':visible')){
      var placement = typeof this.options.placement == 'function' ?
        this.options.placement.call(this, $tip[0], this.$element[0]) :
        this.options.placement

      var autoToken = /\s?auto?\s?/i
      var autoPlace = autoToken.test(placement)
      if (autoPlace) placement = placement.replace(autoToken, '') || 'top'

      $tip.addClass(placement)

      var pos          = this.getPosition()
      var actualWidth  = $tip[0].offsetWidth
      var actualHeight = $tip[0].offsetHeight

      if (autoPlace) {
        var orgPlacement = placement
        var viewportDim = this.getPosition(this.$viewport)

        placement = placement == 'bottom' && pos.bottom + actualHeight > viewportDim.bottom ? 'top'    :
                    placement == 'top'    && pos.top    - actualHeight < viewportDim.top    ? 'bottom' :
                    placement == 'right'  && pos.right  + actualWidth  > viewportDim.width  ? 'left'   :
                    placement == 'left'   && pos.left   - actualWidth  < viewportDim.left   ? 'right'  :
                    placement

        $tip.removeClass(orgPlacement).addClass(placement)
      }

      var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight)
      this.applyPlacement(calculatedOffset, placement)
    }
 }
 
$('#personalizationAddMore').on('click', function() {
	
	perfieldId++;
	$('.personalize-dynamic-content').append('<div class="prs-opt-row" id="perslistli_'+perfieldId+'"><div class="prs-select" id="perslistx_'+perfieldId+'"><a href="javascript:void(0);"><i class="fa fa-times-circle removeSpecificRow" style="font-size:19px;color:red"></i></a><select class="personalization-opt" name="proOptionVal[]" id="proOptionVal'+perfieldId+'"><option value="">Personalization Options</option><option value="1">Text</option><option value="2">Date</option><option value="3">Multi-line Text</option><option value="4">Selection Group</option><option value="6">Dropdown List</option></select></div></div>');
	$('#proSpecWi, #proSpecHi, #proSpecDe, #proSpecWe, .specification-list_dropdown, .personalization-opt').tooltip('recalculatePosition');
	return false;
});


//code started for restrications functionalities
$('#restricationAddMore').on('click', function() {
	resfieldId++;
		
		//alert("res field is"+resfieldId);
		 var field_result = resfieldId-1;
		 //alert(field_result);
         var optionVal ='';
		 var value0 = $("#resOptionVal0").val();
		 var value1 = $("#resOptionVal1").val();
			//alert(value0);
			//alert(value1);

			
			
			//if(resfieldId<=2){

		 if(typeof(value1)=='undefined' && typeof(value2)=='undefined'){
					optionVal +='<option value="">Restrications options</option>';
					optionVal+='<option value="1">coupon</option>';
					optionVal+='<option value="2">studio</option>';
					
					    $('.restrication-dynamic-content').append('<div class="res-opt-row" id="reslistli_'+resfieldId+'"><div class="res-select" id="reslistx_'+resfieldId+'"><a href="javascript:void(0);"><i class="fa fa-times-circle removeSpecificRow_restrict" style="font-size:19px;color:red"></i></a><select class="restrications-opt uniquelabel" name="resOptionVal[]" id="resOptionVal'+resfieldId+'">'+optionVal+'</select></div></div>');
						$('#proSpecWi, #proSpecHi, #proSpecDe, #proSpecWe, .specification-list_dropdown, .personalization-opt').tooltip('recalculatePosition');
			}
			else if(value1==1 && typeof(value0)=='undefined'){
					optionVal +='<option value="">Restrications options</option>';
					optionVal+='<option value="2">studio</option>';
					    $('.restrication-dynamic-content').append('<div class="res-opt-row" id="reslistli_'+resfieldId+'"><div class="res-select" id="reslistx_'+resfieldId+'"><a href="javascript:void(0);"><i class="fa fa-times-circle removeSpecificRow_restrict" style="font-size:19px;color:red"></i></a><select class="restrications-opt uniquelabel" name="resOptionVal[]" id="resOptionVal'+resfieldId+'">'+optionVal+'</select></div></div>');
						$('#proSpecWi, #proSpecHi, #proSpecDe, #proSpecWe, .specification-list_dropdown, .personalization-opt').tooltip('recalculatePosition');
			}
			else if(value1==2 && typeof(value0)=='undefined'){
					optionVal +='<option value="">Restrications options</option>';
					optionVal+='<option value="1">coupon</option>';
					    $('.restrication-dynamic-content').append('<div class="res-opt-row" id="reslistli_'+resfieldId+'"><div class="res-select" id="reslistx_'+resfieldId+'"><a href="javascript:void(0);"><i class="fa fa-times-circle removeSpecificRow_restrict" style="font-size:19px;color:red"></i></a><select class="restrications-opt uniquelabel" name="resOptionVal[]" id="resOptionVal'+resfieldId+'">'+optionVal+'</select></div></div>');
						$('#proSpecWi, #proSpecHi, #proSpecDe, #proSpecWe, .specification-list_dropdown, .personalization-opt').tooltip('recalculatePosition');
			}
		    else if(value0==2 && typeof(value1)=='undefined'){
					optionVal +='<option value="">Restrications options</option>';
					optionVal+='<option value="1">coupon</option>';
					    $('.restrication-dynamic-content').append('<div class="res-opt-row" id="reslistli_'+resfieldId+'"><div class="res-select" id="reslistx_'+resfieldId+'"><a href="javascript:void(0);"><i class="fa fa-times-circle removeSpecificRow_restrict" style="font-size:19px;color:red"></i></a><select class="restrications-opt uniquelabel" name="resOptionVal[]" id="resOptionVal'+resfieldId+'">'+optionVal+'</select></div></div>');
						$('#proSpecWi, #proSpecHi, #proSpecDe, #proSpecWe, .specification-list_dropdown, .personalization-opt').tooltip('recalculatePosition');
			}
			else if(value0==1 && typeof(value1)=='undefined'){
					optionVal +='<option value="">Restrications options</option>';
					optionVal+='<option value="2">studio</option>';
					    $('.restrication-dynamic-content').append('<div class="res-opt-row" id="reslistli_'+resfieldId+'"><div class="res-select" id="reslistx_'+resfieldId+'"><a href="javascript:void(0);"><i class="fa fa-times-circle removeSpecificRow_restrict" style="font-size:19px;color:red"></i></a><select class="restrications-opt uniquelabel" name="resOptionVal[]" id="resOptionVal'+resfieldId+'">'+optionVal+'</select></div></div>');
						$('#proSpecWi, #proSpecHi, #proSpecDe, #proSpecWe, .specification-list_dropdown, .personalization-opt').tooltip('recalculatePosition');
			}
			 else if(value0==1 && value1==2){
					optionVal +='<option value="">Restrications options</option>';
					
					    $('.restrication-dynamic-content').append('<div class="res-opt-row" id="reslistli_'+resfieldId+'"><div class="res-select" id="reslistx_'+resfieldId+'"><a href="javascript:void(0);"><i class="fa fa-times-circle removeSpecificRow_restrict" style="font-size:19px;color:red"></i></a><select class="restrications-opt uniquelabel" name="resOptionVal[]" id="resOptionVal'+resfieldId+'">'+optionVal+'</select></div></div>');
						$('#proSpecWi, #proSpecHi, #proSpecDe, #proSpecWe, .specification-list_dropdown, .personalization-opt').tooltip('recalculatePosition');
			}

			//}
			
			//alert(optionVal);
			
		 
		
		
		
		
		
	
   
  
	return false;
});

//end of code

/*Project Personalization Cloning Remove*/
$(document).on('click','.removeSpecificRow',function(){
	var id =$(this).parent().parent().attr('id');
	var parseSplit =id.split("_");
	//$(this).parent().parent().attr('id','perslistli_'+1);
	perfieldId--;
	//alert(id);
	//return false;
	$(this).parent().parent().parent().remove();
	$('#proSpecWi, #proSpecHi, #proSpecDe, #proSpecWe, .specification-list_dropdown, .personalization-opt').tooltip('recalculatePosition');
	return false;
});

/*remove code for restrictions drop down */

$(document).on('click','.removeSpecificRow_restrict',function(){
	var id =$(this).parent().parent().attr('id');
	var parseSplit =id.split("_");
	resfieldId--;
	var val =$(this).parent().next().val();
	

	if(val==2){
		$(".studio_select_val").val('');
	}
	//alert(val);
	//return false;
	$(this).parent().parent().parent().remove();
	$('#proSpecWi, #proSpecHi, #proSpecDe, #proSpecWe, .specification-list_dropdown, .personalization-opt').tooltip('recalculatePosition');
	return false;
});
/*end of code */

/*Project Personalization options list fetch at 1st drop down value change */
$(document).on('change','.personalization-opt',function(){
	var opt_value = ($(this).val()); 
	var persDivId = $(this).parent().parent().attr("id");
	var persDivIdArr = persDivId.split("_");
	var persSpecId = persDivIdArr[1];
	
	if (opt_value == 1) { //Text
		$(this).parent().siblings().remove(); 
		$(this).parent().after('<div class="prs-right" id="perslisty_'+persSpecId+'"><label>Label<input type="text" placeholder="Enter label" id="proLabel'+persSpecId+'" value="" name="proLabel[]" class="uniquelabel remspace"/></label><label><input type="radio" name="proOptVal_'+persSpecId+'[]" class="proOptVal_'+persSpecId+' radioCls" value="All Caps">All Caps</label><label><input type="radio" name="proOptVal_'+persSpecId+'[]" class="proOptVal_'+persSpecId+' radioCls" value="All Lower Case">All Lower Case</label><label><input type="radio" name="proOptVal_'+persSpecId+'[]" class="proOptVal_'+persSpecId+' radioCls" value="Title Case">Title Case</label><input type="hidden" name="proOptValHidden[]" id="proOptValHidden0" value=""/> </div><div class="prs-opt-singletxt" id="perslistz_'+persSpecId+'"><div class="prs-right-single-text"><label>Hover Text<input type="text" class="remspace" name="proHoverTxt[]" id="proHoverTxt'+persSpecId+'" value="" placeholder="Enter hover text"/></label></div></div>');
	}else if (opt_value == 2) { //Date
		$(this).parent().siblings().remove();
		$(this).parent().after('<div class="prs-right-small" id="perslisty_'+persSpecId+'"><label>Label<input placeholder="Enter label" type="text" id="proLabel'+persSpecId+'" value="" name="proLabel[]" class="uniquelabel remspace"></label><label><input type="radio"  class="proOptVal_'+persSpecId+' radioCls" name="proOptVal_'+persSpecId+'[]" value="MM-DD-YY">MM-DD-YY</label><label><input type="radio" class="proOptVal_'+persSpecId+' radioCls" name="proOptVal_'+persSpecId+'[]" value="MM-DD-YYYY">MM-DD-YYYY</label><label><input type="radio" class="proOptVal_'+persSpecId+' radioCls" name="proOptVal_'+persSpecId+'[]" value="MM-YY">MM-YY</label><label><input type="radio" class="proOptVal_'+persSpecId+' radioCls" name="proOptVal_'+persSpecId+'[]" value="YYYY">YYYY</label><label><input type="radio" class="proOptVal_'+persSpecId+' radioCls" name="proOptVal_'+persSpecId+'[]" value="YY">YY</label><input type="hidden" name="proOptValHidden[]" id="proOptValHidden0" value=""/></div><div class="prs-opt-singletxt" id="perslistz_'+persSpecId+'"><div class="prs-right-single-text"><label>Hover Text<input type="text" class="remspace" name="proHoverTxt[]" id="proHoverTxt'+persSpecId+'" value="" placeholder="Enter hover text"/></label></div></div>');
	}else if (opt_value == 3) { //Multi Line
		$(this).parent().siblings().remove();
		$(this).parent().after('<div class="prs-right" id="perslisty_'+persSpecId+'"><label>Label<input type="text" placeholder="Enter label" id="proLabel'+persSpecId+'" value="" name="proLabel[]" class="uniquelabel remspace"/></label><label><input type="radio" class="proOptVal_'+persSpecId+' radioCls" name="proOptVal_'+persSpecId+'[]" value="All Caps">All Caps</label><label><input type="radio" class="proOptVal_'+persSpecId+' radioCls" name="proOptVal_'+persSpecId+'[]" value="All Lower Case">All Lower Case</label><label><input type="radio" class="proOptVal_'+persSpecId+' radioCls" name="proOptVal_'+persSpecId+'[]" value="Title Case">Title Case</label><input type="hidden" name="proOptValHidden[]" id="proOptValHidden0" value=""/></div><div class="prs-opt-singletxt" id="perslistz_'+persSpecId+'"><div class="prs-right-single-text"><label>Hover Text<input type="text" class="remspace" id="proHoverTxt'+persSpecId+'" value="" name="proHoverTxt[]" placeholder="Enter hover text"/></label></div></div>');
	}
	else if (opt_value == 4){  //added by developer on mar 26 2019
		$(this).parent().siblings().remove();
		$(this).parent().after('<div class="prs-right" id="perslisty_'+persSpecId+'"> <label>Label <input style="width:89%;" type="text" placeholder="Enter label" id="proLabel'+persSpecId+'" value="" name="proLabel[]" class="uniquelabel remspace"/> </label> </div><div class="prs-opt-singletxt" id="perslistz_'+persSpecId+'"> <div class="ph2_selection_row"><div class="ph2_selection-left"><label>Min <input type="text" style="margin-right:15px;" class="widthnew5 min_class" id="min'+persSpecId+'" value="" name="min_'+persSpecId+'[]" placeholder="min"/> </label> <label>Max <input type="text" class="widthnew5 max_class" id="max'+persSpecId+'" value="" name="max_'+persSpecId+'[]" placeholder="max"/> </label></div><div class="prs-right-single-text"><label>Description <input type="text" class="remspace" id="proHoverTxt'+persSpecId+'" value="" name="description_'+persSpecId+'[]" placeholder="Enter Description"/> </label></div></div><div class="ph2_selection_row"><div class="ph2_selection-left"><label style="margin-right:25px; float:left; margin-top:12px;"> <input type="radio" class="proOptVal_'+persSpecId+' radioCls" name="proOptVal_'+persSpecId+'[]" value="vertical">Vertical</label> <label style=" float:left; margin-top:12px;"> <input type="radio" class="proOptVal_'+persSpecId+' radioCls" name="proOptVal_'+persSpecId+'[]" value="horizontal">Horizontal</label></div><div class="prs-right-single-text"><label>Hover Text <input type="text" class="remspace" id="proHoverTxt'+persSpecId+'" value="" name="proHoverTxt[]" placeholder="Enter hover text"/> </label></div></div></div><div class="append-row_1" id="'+persSpecId+'"> <label>Options</label> <input style="margin-bottom:15px;" type="text" placeholder="option1" id="option_per'+persSpecId+'" name="option_per_'+persSpecId+'['+persSpecId+']" class="widthnew20"/><a id="projSpecAddMore_1"  style="color: #6fcf00; " href="javascript:void(0);"><i style="font-size: 20px;" class="fa fa-plus-circle" ></i>Add Option</a></div>');
		
	}
	else if (opt_value == 5){  //added by developer on mar 26 2019
		$(this).parent().siblings().remove();
		$(this).parent().after('<div class="prs-right" id="perslisty_'+persSpecId+'"><label>Label<input type="text" placeholder="Enter label" id="proLabel'+persSpecId+'" value="" name="proLabel[]" class="uniquelabel remspace"/></label><label><input type="radio" class="proOptVal_'+persSpecId+' radioCls" name="proOptVal_'+persSpecId+'[]" value="vertical">Vertical</label><label><input type="radio" class="proOptVal_'+persSpecId+' radioCls" name="proOptVal_'+persSpecId+'[]" value="horizontal">Horizontal</label></div><div class="prs-opt-singletxt" id="perslistz_'+persSpecId+'"><div class="prs-right-single-text"><label>Hover Text<input type="text" class="remspace" id="proHoverTxt'+persSpecId+'" value="" name="proHoverTxt[]" placeholder="Enter hover text"/></label></div></div><div class="append-row_1" id="'+persSpecId+'"><label>Options</label><input type="text" placeholder="option1" id="option_per_'+persSpecId+'"  name="option_per_'+persSpecId+'['+persSpecId+']" class="widthnew20"/><a href="javascript:void(0);"><i class="fa fa-plus-circle" id="projSpecAddMore_1"></i></a></div>');
	}
	else if (opt_value == 6){  //added by developer on mar 26 2019
		$(this).parent().siblings().remove();
		$(this).parent().after('<div class="prs-right" id="perslisty_'+persSpecId+'"><label>Label<input style="width:88%;" type="text" placeholder="Enter label" id="proLabel'+persSpecId+'" value="" name="proLabel[]" class="uniquelabel remspace"/></label></div><div class="prs-opt-singletxt" id="perslistz_'+persSpecId+'"><div class="prs-right-single-text"><label>Hover Text<input type="text" class="remspace" id="proHoverTxt'+persSpecId+'" value="" name="proHoverTxt[]" placeholder="Enter hover text"/></label></div></div><div class="mul_select_row"><div class="m_select_right"><label>Option : </label><select data-placeholder="Choose option..." class="chosen-select" name="option_per_'+persSpecId+'[]" multiple tabindex="4"><option value=""></option><option value="option1">option1</option><option value="option2">option2</option><option value="option3">option3</option></select></div></div>');
		$(".chosen-select").chosen({no_results_text: "Oops, nothing found!"}); 
	}
	else {
		$(this).parent().siblings().remove();
	}
	$('#proSpecWi, #proSpecHi, #proSpecDe, #proSpecWe, .specification-list_dropdown, .personalization-opt').tooltip('recalculatePosition');
	
	/*$("#proOptionVal"+persSpecId).rules("add", { required: function(element){
		if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction"){ 
		   return $('#proOptionVal'+persSpecId).val() == "" ;
		} else {
			return false;
		}
	}});*/
	
	$("#proLabel"+persSpecId).rules("add", { required: function(element){
		if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction" || $("#actionType").val() == 'enableAction' || $("#actionType").val() == 'unarchiveAction'){ 
		   return $('#proLabel'+persSpecId).val() == "" ;
		} else {
			return false;
		}
	}});
	
	/*$("#proHoverTxt"+persSpecId).rules("add", { required: function(element){
		if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction"){ 
		   return $('#proHoverTxt'+persSpecId).val() == "" ;
		} else {
			return false;
		}
	}});*/
	
	$(".proOptVal_"+persSpecId).rules("add", { required: function(element){
		if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction" || $("#actionType").val() == 'enableAction' || $("#actionType").val() == 'unarchiveAction'){ 
		   var isChecked = $('.proOptVal_'+persSpecId).attr('checked')?true:false;
		   if(!isChecked)
		   return true;
		} else {
			return false;
		}
	}});
});

// restrications code 
//$(".")

$(document).on('change','.restrications-opt',function(){
		
	 var opt_value = $(this).val();
	 var persDivId_res = $(this).parent().parent().attr("id");
	 var persDivIdArrRes = persDivId_res.split("_");
	 var resSpecId = persDivIdArrRes[1];
	 //alert(opt_value);
	 if(opt_value!=""){
		$("#restricationAddMore").css('display','block');
	 }
	 else {
		 $("#restricationAddMore").css('display','none');
	 }
	
	
		//alert(resSpecId);
	if (opt_value == 1) { //Text
		$(this).parent().siblings().remove(); 
		$(this).parent().after('<div class="res-main-parent" id="'+resSpecId+'"><div class="res-right-child res-select" id="restrication_'+resSpecId+'"><select class="restrication-opt-more" name="resCouponType[]" id="resOptionVal_'+resSpecId+'"><option value="amount">Fixed Amount</option><option value="percent">Percent</option></select></div></div>');
	}
	else if (opt_value == 2) { //Text
		$(this).parent().siblings().remove();
		$(this).parent().after('<div class="remove_element" id="'+resSpecId+'"><select id="studio_select_'+resSpecId+'" class="studio_select" name="studioid_'+resSpecId+'[0]"><?php echo getStudioList();?></select></div><a id="projSpecAddMore_studio" class="addmore_'+resSpecId+'" style="color: #6fcf00; " href="javascript:void(0);"><i style="font-size: 20px;" class="fa fa-plus-circle"></i>Add Option</a>');
		$("#studio_select_"+resSpecId).select2({ width: '100%' });
		
	}
	else {
			$(this).parent().siblings().remove();
	}
	
});

//code for restriction more options
//$(".restrication-opt-more")
$(document).on('change','.restrication-opt-more',function(){
	var id = $(this).parent().parent().attr("id");
	var value = $(this).val();
	if(value=="percent" || value=="amount" ){
		$(this).parent().next().remove();
		$(this).parent().after('<div class="res-right-child-1  prs-select" id="'+id+'"><input id="amount_'+id+'" type="text" name="amount[]" ></div>');
	}
	
	else  {
		$(this).parent().next().remove();
	}
});
//end of code 
//end of restrictions


$(document).on("change",".radioCls",function(){ 
    var val123 = $(this).val();
	$(this).parent().parent().find("#proOptValHidden0").val(val123)
});

/********Project Specification Section *********
*************************************************
*****************************************/

/*Project Specification Clone*/
<?php //Edit Case
if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "") {
	$proSpecitems = fetchTblDataByIdForProjectSection("bb_prospecifications","proID",$_REQUEST["id"]);
	$proSpecitems_options = fetchTblDataByIdForProjectSection_count("bb_prooptionadd","proID",$_REQUEST["id"]);
	//echo "count is".count($proSpecitems_options);
	if(!empty($proSpecitems) && is_array($proSpecitems)) { ?>
		var fieldId = '<?php echo count($proSpecitems)-1;?>';
		var fieldId_1 = '<?php echo count($proSpecitems_options);?>';
		var fieldId_2 = '<?php echo count($proRestitems); ?>';
<?php 
	} else {?>
		var fieldId = 0;
		var fieldId_2 = 0; 
	<?php
	}
} else { //add Case?>
	var fieldId = 0;
	var fieldId_1 = 0;
	var fieldId_2 = 0; //for add more studion
<?php 
} ?>

//alert('fieldId='+fieldId);
$('#projSpecAddMore').on('click', function() {
	fieldId++;
	getProjSpec1stOptList(); //For 1st Dropdown
	//getProjSpec2ndOptList(fieldId); //For 2nd Dropdown
	$('.specification-list-row ul').append('<li id="listli_'+fieldId+'"><div class="specification-list" id="listx_'+fieldId+'"><a href=""><i class="fa fa-times-circle remove" style="font-size:19px;color:red"></i></a><select class="specification-list_dropdown specDropdownAddedTime unique" name="specOptionVal[]" id="specOptionVal'+fieldId+'"></select></div><div class="specification-list" id="listy_'+fieldId+'" style="display:none;"><select class="specification-list_dropdown100 uniqueBoard" name="specType[]" id="specType'+fieldId+'"></select></div><div class="specification-list-qty" id="listz_'+fieldId+'" style="display:none;"><label>Qty<input placeholder="" type="text" name="specTypeVal[]" id="specTypeVal'+fieldId+'"></label></div></li>');
	$("#specOptionVal"+fieldId).rules("add", { required: function(element){
		if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction" || $("#actionType").val() == 'enableAction' || $("#actionType").val() == 'unarchiveAction'){ 
		   return $('#specOptionVal'+fieldId).val() == "" ;
		} else {
			return false;
		}
	}});
	return false; 
});



/*Project Specification Cloning Remove*/
$('.specification-list-row').on('click', '.remove', function() {
	$(this).parent().parent().parent().remove();
	$('#proSpecWi, #proSpecHi, #proSpecDe, #proSpecWe, .specification-list_dropdown, .personalization-opt').tooltip('recalculatePosition');
    return false; 
});

//for option under Personolizations options
 $(document).on('click','#projSpecAddMore_1',function(){
     
	fieldId_1++;
	var persDivId = $(this).parent().attr("id");
	//alert(persDivId);
	//alert(persDivId);
	//return false;
	//var persDivIdArr = persDivId.split("_");
	//var persSpecId = persDivIdArr[1];
	//option_select //<a href="javascript:void(0);"><i class="fa fa-times-circle removeSpecificRow_1" ></i></a>
	$(this).before('<div class="remove_element"><input type="text" name="option_per_'+persDivId+'['+fieldId_1+']"><a href="javascript:void(0);"><i class="fa fa-times-circle removeSpecificRow_1"></i></a></div>');
	return false;
	
});

//for options under restrications
$(document).on('click','#projSpecAddMore_studio',function(){
		
	var persDivId = $(this).parent().attr("id");
	//alert(persDivId);
	var persDivIdArrRes = persDivId.split("_");
	var resSpecId = persDivIdArrRes[1];
	var studio_id='';
	studio_id +=$(".studio_select_val").val();
	var classid = $(this).attr('class');
	fieldId_2++;
	
			var optionListHtml = "";

		$.ajax({
        url: "<?php echo base_url_site;?>/addon/listStudio",
        type: "post",
        data : {studio_id:studio_id},
        success: function (response) {
			var jsonData = $.parseJSON(response);
			var optionListLength  = jsonData.optionList.length; 
			
			
			  if(optionListLength > 0) {
			     optionListHtml= "<option value=''>Select Studios</option>";
				for(var k=0;k<optionListLength; k++){
					
				    optionListHtml += "<option value="+jsonData.optionList[k].id+">"+jsonData.optionList[k].stName+'_'+jsonData.optionList[k].stNum+"</option>";
					//alert(optionListHtml);
				}
				$("."+classid).before('<div class="remove_element remove_select" id="'+resSpecId+'"><select id="studio_select_more_'+persDivId+fieldId_2+'" class="studio_select" name="studioid_'+persDivId+'['+fieldId_2+']">'+optionListHtml+'</select></div>');
	$("#studio_select_more_"+persDivId+fieldId_2).select2({ 
	width: '300px' });
			}
			else {
				
			     //optionListHtml= "<option value=''>Select Studios</option>";
				
				//$("."+classid).before('<div class="remove_element remove_select"><select id="studio_select_more_'+persDivId+fieldId_2+'" class="studio_select" name="studioid_'+persDivId+'['+fieldId_2+']">'+optionListHtml+'</select></div>');
	//$("#studio_select_more_"+persDivId+fieldId_2).select2({ 
	//width: '300px' });
			
			} 
			
			
		}
    });
	
		
	
	

	
	
	
	

	
	
	return false;
});
//end of code

$(document).on('click','.removeSpecificRow_1',function(){
    
	var removeItem = $(this).parent().attr('id');
	var classs = $(this).parent().parent().hide();
	// $(this).parent('div').hide();
	var stringvalueArray = $(".studio_select_val").val();
	//alert(stringvalueArray);
	//alert(removeItem);
	var finalValRemove = removeValue(stringvalueArray, removeItem);
	//alert(finalValRemove);
	$(".studio_select_val").val(finalValRemove);
	//alert("value is"+aryWithoutSeven);
	//return false;
	$(".studio_select_val").val(aryWithoutSeven);
    //$("#projSpecAddMore_studio").trigger('click');
	//$(".select2-selection span").remove();
	//$(".select2").remove();
	$(this).parent().parent().remove();
	$(".remove_select").remove();
    return false; 
}); 

//code to remove code 
$(document).on('click','.removeSpecificRow_2',function(){
	
     var id = $(this).parent().attr('id');
	 var stringvalueArray = $(".code_select_val").val();
	 //var stringvalueArray1 = stringvalueArray.split(",");
	//alert(stringvalueArray1);
	// return false;
		
	 var removeItem = $(this).parent().attr('id');
	 
	
	var finalValRemove = removeValue(stringvalueArray, removeItem);
	//alert(finalValRemo);
	$(".code_select_val").val(finalValRemove);
	
	
	
    //return false;
	var classs = $(this).parent().parent().hide();
	
	//return false;
	
    //$("#projSpecAddMore_studio").trigger('click');
	//$(".select2-selection span").remove();
	//$(".select2").remove();
	$(this).parent().parent().remove();
	$(".remove_select").remove();
    return false; 
});  
function removeValue(list, value) {
		return list.replace(new RegExp(",?" + value + ",?"), function(match) {
		var first_comma = match.charAt(0) === ',',
		second_comma;

		if (first_comma &&
		(second_comma = match.charAt(match.length - 1) === ',')) {
		return ',';
		}
		return '';
		});
	}
/*Project specification 1st Dropdown options list*/
function getProjSpec1stOptList(){ 
	$.ajax({
        url: "<?php echo base_url_site;?>getProjSpec1stOptList",
        type: "post",
        data : {tblName:'bb_prospcoptions',fldName1:'id',fldName2:'opName'},
        success: function (response) {
            var jsonData = $.parseJSON(response); 
			var optionListLength  = jsonData.optionList.length; 
			//var optionListHtml = "";
			if(optionListLength > 0) {
			    var optionListHtml = "<option value=''>Specification options</option>";
				for(var k=0;k<optionListLength; k++){
				    optionListHtml += "<option value="+jsonData.optionList[k].id+">"+jsonData.optionList[k].opName+"</option>";
				}
				$(".specification-list-row ul li:last-child .specDropdownAddedTime").html(optionListHtml);
			}
		}
    });
}

/*Project specification 2nd Dropdown options list at page load */
<?php //Edit Case
if(!isset($_REQUEST["id"])) { ?>
	getProjSpec2ndOptList(fieldId); 
<?php }?>
function getProjSpec2ndOptList(fieldId){ 
	$.ajax({
        url: "<?php echo base_url_site;?>getProjSpec2ndOptList",
        type: "post",
        data : {tblName:'bb_prospcsizemeasure',fldName1:'id',fldName2:'sizename',fldName3:1},
        success: function (response) {
            var jsonData = $.parseJSON(response); 
			var optionListLength  = jsonData.optionList.length;
			if(optionListLength > 0) {
			    var optionListHtml = "";
				for(var k=0;k<optionListLength; k++){
				    optionListHtml += "<option value="+jsonData.optionList[k].id+">"+jsonData.optionList[k].sizename+"</option>";
				}
				$('.specification-list_dropdown').parent().parent().find("#listy_"+fieldId+" .specification-list_dropdown100").html(optionListHtml);
			}
		}
    });
}

/*Project specification 2nd Dropdown options list fetch at 1st drop down value change */
$(document).on('change','.specification-list_dropdown',function(){  
	var fldName3Val = $(this).val();
	var liId = $(this).parent().parent().attr('id');
	var liIdArr  = liId.split("_");
	
	$('.specification-list_dropdown').parent().parent().find("#listy_"+liIdArr[1]+" .specification-list_dropdown100").html("");
	if(fldName3Val != '') {
		$.ajax({
			url: "<?php echo base_url_site;?>getProjSpec2ndOptList",
			type: "post",
			data : {tblName:'bb_prospcsizemeasure',fldName1:'id',fldName2:'sizename',fldName3:fldName3Val},
			success: function (response) {
				var jsonData = $.parseJSON(response); 
				var optionListLength  = jsonData.optionList.length;
				var optionListHtml = "";
				if(optionListLength > 0) {
					for(var k=0;k<optionListLength; k++){
						optionListHtml += "<option value="+jsonData.optionList[k].id+">"+jsonData.optionList[k].sizename+"</option>";
					}
		
					$('.specification-list_dropdown').parent().parent().find("#listy_"+liIdArr[1]).css("display","block");
					$('.specification-list_dropdown').parent().parent().find("#listz_"+liIdArr[1]).css("display","block");
					
					$('.specification-list_dropdown').parent().parent().find("#listy_"+liIdArr[1]+" .specification-list_dropdown100").css("display","block");
					$('.specification-list_dropdown').parent().parent().find("#listy_"+liIdArr[1]+" .specification-list_dropdown100").html(optionListHtml);
				
				} else {
					$('.specification-list_dropdown').parent().parent().find("#listy_"+liIdArr[1]).css("display","block");
					$('.specification-list_dropdown').parent().parent().find("#listz_"+liIdArr[1]).css("display","block");
					optionListHtml = "<option value=''></option>";
					$('.specification-list_dropdown').parent().parent().find("#listy_"+liIdArr[1]+" .specification-list_dropdown100").css("display","none");
					$('.specification-list_dropdown').parent().parent().find("#listy_"+liIdArr[1]+" .specification-list_dropdown100").html(optionListHtml);
				}
			}
		});
	} 
	else {
		$('.specification-list_dropdown').parent().parent().find("#listy_"+liIdArr[1]).css("display","none");
		$('.specification-list_dropdown').parent().parent().find("#listz_"+liIdArr[1]).css("display","none");
	}
	
	$("#specOptionVal"+liIdArr[1]).rules("add", { required: function(element){
		if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction" || $("#actionType").val() == 'enableAction' || $("#actionType").val() == 'unarchiveAction'){ 
		   return $('#specOptionVal'+liIdArr[1]).val() == "" ;
		} else {
			return false;
		}
	}});
	if(fldName3Val == 1 || fldName3Val == 2) { // if options are  board,support
		$("#specType"+liIdArr[1]).rules("add", { required: function(element){
			if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction" || $("#actionType").val() == 'enableAction' || $("#actionType").val() == 'unarchiveAction'){ 
			   return $('#specType'+liIdArr[1]).val() == "" ;
			} else {
				return false;
			}
		}});
	}
	if(fldName3Val == 3 || fldName3Val == 4 || fldName3Val == 5 || fldName3Val == 6) { // if options are screw,wire,hanger,nails
	  $("#specType"+liIdArr[1]).rules("remove","required");
	}
	
	$("#specTypeVal"+liIdArr[1]).rules("add", { required: function(element){
		if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction" || $("#actionType").val() == 'enableAction' || $("#actionType").val() == 'unarchiveAction'){ 
		   return $('#specTypeVal'+liIdArr[1]).val() == "" ;
		} else {
			return false;
		}
	},maxlength:4,digits:true});
	
	
	if(fldName3Val == 4){
		var label_text = $("#listz_"+liIdArr[1]+" label").html();
		$("#listz_"+liIdArr[1]+" label").html( label_text.replace("Qty", "Length") );
		$("#listz_"+liIdArr[1]).css("width","120px");
	} 
	else {
		var label_text = $("#listz_"+liIdArr[1]+" label").html();
		$("#listz_"+liIdArr[1]+" label").html( label_text.replace("Length", "Qty") );
		$("#listz_"+liIdArr[1]).css("width","100px");
	}
});


/*Get Project Price on the basis of Project class change */
$(document).on('change','#proClassId',function(){ 
	var proClassIdVal = $(this).val();
	$.ajax({
        url: "<?php echo base_url_site;?>getProjClsPrice",
        type: "post",
        data : {tblName:'bb_proclassoptions',fldName1:'id',fldName2:'opPrice',fldName3:proClassIdVal},
        success: function (response) {
			var jsonData = $.parseJSON(response); 
			var projectPrice  = jsonData.opPrice;
			$("#proPrice").val("$"+projectPrice);
			$("#proPrice").next().addClass("float");
		}
    });
});

$.validator.addMethod("lettersonlys", function(value, element) {
  return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
}, "Please enter only letters");

/*$.validator.addMethod("letnumonlys", function(value, element) {
  return this.optional(element) || /^[a-zA-Z0-9]*$/.test(value); 
}, "Please enter letters or numbers only"); */

//Add method for validating duplicate specification options
$.validator.addMethod("unique", function(value, element) {  
    var parentForm = $(element).closest('form'); 
    var timeRepeated = 0;
    if (value != '' && value != '1') {
		$(parentForm.find('.specification-list_dropdown')).each(function () {
			if ($(this).val() === value) {
                timeRepeated++;
            }
        });
    }
    return timeRepeated === 1 || timeRepeated === 0;
}, "Duplicate option selected");

$.validator.addMethod("uniqueBoard", function(value, element) {
	var parentForm = $(element).closest('form');
	var timeRepeated = 0;
	var parentBoxValue = $(element).parent().siblings().find('.specification-list_dropdown').val();
	//alert(parentBoxValue);
	if (parentBoxValue == 1) {
		$(parentForm.find('.uniqueBoard')).each(function () {
			if ($(this).val() === value) {
                timeRepeated++;
            }
        });
    }
	return timeRepeated === 1 || timeRepeated === 0;
}, "Duplicate Board Sizes selected");

//Add method for validating duplicate personalization label for same options
$.validator.addMethod("uniquelabel", function(value, element) {  
    var parentForm = $(element).closest('form'); 
    var timeRepeated = 0;
	value = $.trim(value);
	if (value != '') { 
	    var value1 = value;
			value1 = $.trim(value1);
	    $(parentForm.find('.uniquelabel')).each(function () {
		    var str = $(this).val();
			str = $.trim(str);
			if (str === value1 ) {
				timeRepeated++;
            }
        });
    }
    return timeRepeated === 1 || timeRepeated === 0;
}, "Duplicate label selected");

$(document).on('click','.actionEvent',function(){ 
	var actionEventId = $(this).attr('id'); 
	$("#actionType").val(actionEventId);
});

$(document).on('change', '#img-upload', function(){
	var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
	if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
		$('#blah').css('border', '2px solid red');
	}else{
		$('#blah').css('border', 'none');
	}
});

<?php 
//Edit Case
if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "") { ?>
	var projectId = $("#projectId").val();
<?php 
} else { // Add Case ?>
	var projectId = "";
<?php }?>

<?php 
//Edit Case  when preview is true
if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "" && isset($_REQUEST["preview"]) && $_REQUEST["preview"] == "true" ) { ?>
	$('.tempname').attr('disabled', true);
<?php 
} else { // Add Case ?>
	$('.tempname').attr('disabled', false);
<?php }?>

$.validator.prototype.checkForm = function() {
    //overriden in a specific page
    this.prepareForm();
    for (var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
        if (this.findByName(elements[i].name).length !== undefined && this.findByName(elements[i].name).length > 1) {
            for (var cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
                this.check(this.findByName(elements[i].name)[cnt]);
            }
        } else {
            this.check(elements[i]);
        }
    }
    return this.valid();
};

$.validator.addMethod('minImageWidth', function(value, element, minWidth) {
	var ruff_Img_Wdith = $(element).parent().parent().parent().siblings('.ruff-img').width();
	return (ruff_Img_Wdith || 0) >= minWidth;
}, function(minWidth, element) {
	var imageWidth = $(element).data('imageWidth');
	return (imageWidth)
		//? ("Your image's width must be greater than or equal to " + minWidth + "px")
		? ("Size must exceed 700x600")
		: "Selected file is not an image.";
});

$.validator.addMethod('minImageHeight', function(value, element, minHeight) {
	var ruff_Img_Height = $(element).parent().parent().parent().siblings('.ruff-img').height();
	return (ruff_Img_Height || 0) >= minHeight;
}, function(minHeight, element) {
	//var imageHeight = $(element).data('imageHeight'); 
	var imageHeight = '202';
	return (imageHeight)
		//? ("Your image's height must be greater than or equal to " + minHeight + "px")
		? ("Size must exceed 700x600")
		: "Selected file is not an image.";
});

/*$.validator.addMethod("checkDateFormat",function(value, element) {
    return value.match(/^(0[1-9]|1[012])[\/|-](0[1-9]|[12][0-9]|3[01])[\/|-](19\d\d|2\d\d\d)$/);
},"Please enter a date in the format mm/dd/yyyy");*/


$.validator.addMethod('checkDateFormat', function(value, element, param) {
	if(value != '' ){
		return this.optional(element) || String(value).match(/^(0[1-9]|1[012])[\/|-](0[1-9]|[12][0-9]|3[01])[\/|-](19\d\d|2\d\d\d)$/);
	} else {
		return true;
	}
}, 'Please enter a date in the format mm/dd/yyyy');

$.validator.addMethod("noSpace", function(value, element) { 
  //return value.indexOf(" ") < 0 && value != "";
  return $.trim(value) != "";
}, "No space please and don't leave it empty");

var Resized_Image_Width_M =  '<?php echo Resized_Image_Width_M;?>' ;
var Resized_Image_Height_M =  '<?php echo Resized_Image_Height_M;?>' ;
$.validator.addMethod("minStrict",function(value,el,param){
	return value>param ;
});

$(document).on("keyup",".max_class",function(){
	var value =$(this).attr('id').match(/\d/g);
	
    var minId =$("#min"+value).val();
	
	$(this).rules("add",{
		required:true,
		number:true,
		min:function(){
					return parseInt(minId);
		},
		
		
	});	
});
/* $(".min_class").each(function(){

	$(this).rules("add",{
		required:true,
		
		
	});	
	
	
}); */
//code start for gallery images section
var Resized_Image_Width_M =  '<?php echo Resized_Image_Width_M;?>' ;
var Resized_Image_Height_M =  '<?php echo Resized_Image_Height_M;?>' ;
var form_datapush = new FormData(); 
        var number = 0;

        /* WHEN YOU UPLOAD ONE OR MULTIPLE FILES */
        $(document).on('change','#files',function(){
			len_files = $("#files").prop("files").length;
            for (var i = 0; i < len_files; i++) {
                var file_data = $("#files").prop("files")[i];
                form_datapush.append(file_data.name, file_data);
                number++;
               $('.sortable').children("#img").remove();
                $('.sortable').append("<li id='imggal1'><a href='javascript:void(0);' class='remattr'><img src='images/cross-icon.png' /></a><img class='thumbnail' src='"+URL.createObjectURL(event.target.files[i])+"' id='ImgGal' name='ImgGal[]' alt='"  +  file_data.name  + "'></li>");
				
			    }
              			  
		});
	$(document).on("click",".remattr",function() {
		     if (this.hasAttribute("imgid")) {
				 var st;
				  st=$('#delimg').val();
				  if(st=="")
				  {
					st =$(this).attr('imgid');
				  }
				  else
				  {
					  st =st+','+ $(this).attr('imgid');
				  }
                   $('#delimg').val(st);
                    
					} 
//alert($('#delimg').val());
		      
	         var filename = $(this).siblings('#ImgGal').attr('alt');

			form_datapush.delete($(this).siblings('#ImgGal').attr('alt'));
			
			
			 if($(this).parent().parent().parent().find('#imgul').children("li").length == 1)
			 {
				
				 $(this).parent().parent().parent().find('#imgul').append('<li id="img"><img  class="thumbnail" src="<?php echo base_url_images."placeholder.jpg";?>"></li>');
				 
			 }
			
			 
	         $(this).parent().remove();
			 
			
			
			 
});


$.validator.addMethod("checkimage", function(value, element) { 
return $('.sortable li').first().find('#ImgGal').attr('alt'); 
}, "Please Upload Image");
		
//end of code


var validator = $("form[id='addUpdProjFrm']").validate({
	//ignore: ':hidden > #img-upload',       
    onkeyup: false,
    onblur :true,
    ignore: 'input[type=hidden],.select2-input, .select2-focusser',
	rules: {
	    proName: { 
			required :true,maxlength: 40,noSpace: true
			/*,remote: {
				url: '<?php echo base_url_site; ?>addon/ajaxValidateProjectName',
				type: "post",
				data: { projectId:projectId}
			}*/
		},
		proTypeId: { required :true},
		//proSku: { required :true,maxlength: 15,letnumonlys: true },
		proClassId: { required :true},
		proPriCatId: { required :true},
		"min_0[]": {
                required: true,
				number:true,
               
                
            },
		"max_0[]": {
				required: true,
				number:true,
				 min:function(){
					return parseInt($("#min0").val());
				}
			},		
		proSchStDate: {
		    /*required: function(element){
				if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction"){ 
					return true;
				} else {
					return false;
				}
			},date: true,checkDateFormat: true,*/
			checkDateFormat: true,
		},
		proSchStTime: {
			checkTimeFormat: true,
			existDateThanTime:"#proSchStDate"
			/*required: function(element){
				if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction"){ 
					return true;
				} else {
					return false;
				}
			},*/
		},
		proGalImg:{
            required: function(element){
			    if(($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction" || $("#actionType").val()== "enableAction" || $("#actionType").val() == 'unarchiveAction' ) && $("#isfile").val()==0){ 
				    return true;
				} else {
					return false;
				}
			},extension: "png|jpeg|jpg|gif",minImageWidth: Resized_Image_Width_M,minImageHeight: Resized_Image_Height_M,
		},
		
		/*"proOptionVal[]": { 
			required: function(element){
				if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction"){ 
					return $('#proOptionVal0').val() == "";
                } else {
					return false;
				}
			}
		},
		"proLabel[]": { 
			required: function(element){
				//if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction"){ 
				if($("#actionType").val()== "updateAction"){ 
					return true;
                } else {
					return false;
				}
			}
		},
		"proHoverTxt[]": { 
			required: function(element){
				//if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction"){ 
				if($("#actionType").val()== "updateAction"){ 
					return true;
                } else {
					return false;
				}
			}
		},*/
		proSpecWi: {
		    required: function(element){
				if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction" || $("#actionType").val()== "enableAction" || $("#actionType").val() == 'unarchiveAction'){ 
					return true;
				} else {
					return false;
				}
			},maxlength: 4, digits: true
		},
		proSpecHi: {
		    required: function(element){
				if($("#actionType").val()== "addAction"|| $("#actionType").val()== "updateAction" || $("#actionType").val()== "enableAction" || $("#actionType").val() == 'unarchiveAction'){ 
					return true;
				} else {
					return false;
				}
			},maxlength: 4, digits: true
		},
	
		"specOptionVal[]": { 
			required: function(element){
				if($("#actionType").val()== "addAction"|| $("#actionType").val()== "updateAction" || $("#actionType").val()== "enableAction" || $("#actionType").val() == 'unarchiveAction'){ 
					return $('#specOptionVal0').val() == "";
                } else {
					return false;
				}
			}
		},
		"specTypeVal[]": { 
			required: function(element){
				if($("#actionType").val()== "addAction"|| $("#actionType").val()== "updateAction" || $("#actionType").val()== "enableAction" || $("#actionType").val() == 'unarchiveAction'){ 
					return true;
                } else {
					return false;
				}
			},maxlength:4,digits:true,
		},
		
	},
	// Specify validation error messages
	messages: {
	    proPriCatId: {required: "Please select primary category"},
		proName: {required: "Please enter project name", remote: "Project name is already exist"},
		proTypeId: {required: "Please enter project type"},
		//proSku: {required:"Please enter project sku"},
		proClassId: {required: "Please enter project class"},
		//proDetailedDesc: "Please enter project description",
		//proSchStDate: {required: "Please enter schedule date", date: "Please enter valid date"},
		//proSchStTime: {required: "Please enter schedule time"},
		proGalImg: {required: "Project Image is required",extension: "Image must be JPEG or PNG or JPG or GIF type" },
		
		/*"proOptionVal[]": { required: "Select option" },
		"proLabel[]": { required: "Select label" },
		"proHoverTxt[]": { required: "Select hover text" },*/
		
		proSpecWi: { required :"Enter width"},
		proSpecHi: { required :"Enter height"},
		/*proSpecDe: { required :"Enter depth"},
		proSpecWe: { required :"Enter weight"},*/
		"specOptionVal[]": { required: "Select option" },
		"specTypeVal[]": { required: "Enter value" },
		//"proFpPrintList[]": { required: "Select atleast one option" }
	},
	
	// in the "action" attribute of the form when valid
	submitHandler: function(form) { 
	    $('.loading').show(); 
		var dataf = new FormData();
		var form_data = $('#addUpdProjFrm').serializeArray();
		$.each(form_data, function() {
			form_datapush.append(this.name, this.value);
			$('.tb_img').append(this.name, this.value);
		});
		form_datapush.append('firstimg',$('.sortable li').first().find('#ImgGal').attr('alt'));
		
		
		
		//alert(proGalImg);
		//return false;

		/*var files = $("#img-upload")[0].files;
		if (files.length > 0) {
			dataf.append("proGalImg", files[0]);
		} else {
			dataf.append("proGalImg", "");
		}*/
		//alert(dataf);
		//save add project
		if($("#actionType").val() == 'addAction' || $("#actionType").val() == 'draftAction'|| $("#actionType").val() == 'previewAction' ){
			$.ajax({
				url : '<?php echo base_url_site; ?>addon/ajaxSaveAddProject',
				type : 'POST', // Assuming creation of an entity
				mimeType: "multipart/form-data",
				contentType : false, // To force multipart/form-data
				data : form_datapush,
				processData : false,
				success : function(response) {
				   $('.loading').hide(); 
					var odata = $.parseJSON(response); 
					setInterval(function(){ $('.project-msg-add').hide(); }, 5000);
					var status = odata.status;
					if(status == '2') {
					   window.location.href = "<?php echo base_url_site;?>projectlist?type=draft";
					} 
					else if(status == '5') {
						var last_insert_id = odata.last_insert_id; //alert(last_insert_id);
					    window.open("<?php echo base_url_site?>projectpreview?id="+last_insert_id);
					} 
					else {
						window.location.href = "<?php echo base_url_site;?>projectlist";
					}    
				}
			});
		}
		else if($("#actionType").val() == 'updateAction' || $("#actionType").val() == 'updatedraftAction' || $("#actionType").val() == 'updatepreviewAction' ){
			$.ajax({
				url : '<?php echo base_url_site; ?>addon/ajaxSaveUpdateProject',
				type : 'POST', // Assuming creation of an entity
				mimeType: "multipart/form-data",
				contentType : false, // To force multipart/form-data
				data : form_datapush,
				processData : false,
				success : function(response) {
				     $('.loading').hide(); 
					  var odata = $.parseJSON(response); 
					setInterval(function(){ $('.project-msg-add').hide(); }, 5000);
					var status = odata.status; //alert(status);
					if(status == '2') {
					   window.location.href = "<?php echo base_url_site;?>projectlist?type=draft";
					} 
					else if(status == '5') { 
						var last_insert_id = odata.last_insert_id;
					    window.open("<?php echo base_url_site?>projectpreview?id="+last_insert_id);
					} 
					else {
						window.location.href = "<?php echo base_url_site;?>projectlist";
					} 
				}
			});
		}
		
		else if($("#actionType").val() == 'enableAction' || $("#actionType").val() == 'disableAction' || $("#actionType").val() == 'archiveAction' || $("#actionType").val() == 'unarchiveAction'){
			$.ajax({
				url : '<?php echo base_url_site; ?>addon/ajaxChangeStatusProject',
				type : 'POST', // Assuming creation of an entity
				mimeType: "multipart/form-data",
				contentType : false, // To force multipart/form-data
				data : dataf,
				processData : false,
				success : function(response) {
					var odata = $.parseJSON(response); 
					
					var status = odata.status; //alert(status);
					if(status == '3') {
					   window.location.href = "<?php echo base_url_site;?>projectlist?type=archive";
					} else {
						window.location.href = "<?php echo base_url_site;?>projectlist";
					}
				}
			});
		}
	}
});
$(document).on('click','.remattr',function(){
	var idImg = $(this).attr('name');
	//alert(idImg);
		$.ajax({
				url : '<?php echo base_url_site; ?>addon/deleteGaImage',
				type : 'POST', // Assuming creation of an entity
				//mimeType: "multipart/form-data",
				//contentType : false, // To force multipart/form-data
				data :{idImg:idImg},
				//processData : false,
				success : function(response) {
					var odata = $.parseJSON(response); 
					
					var status = odata.status; 
					//alert(status);
					
				}
			});
	$(this).parent().remove();
	$(this).remove();
});

	
//for zoom effect
	$(function() {
       Zepto('.sortable').dragswap({
            element : 'li',
			dropAnimation: true  
		});
        
		
	});


<!-------  Restrictions   jquery start ------->  
$(document).ready(function(){
	
	
	$(document).on("click", ".res_close_btn", function(){
		//$(this).parents(".restriction-row").remove();
		$(this).parent().parent().remove();
		$(".res_attr").attr("id", "p_1");
		$(".restrictionContainer").siblings(".insert-ticket").show();
		
		
	});
	
	<!-----    crr.empty(); crr.append('<option attributevalue="" value="">Project Options</option>'+datas);          ------>
	
  $(document).on("click", ".restriction_addOpt", function(){
	var i=1;
	var j=0;
	var st=$(this);
	
	st.parents('.last_template-full_section').find(".res_attr").each(function() {
		j++;
		
	});
	
		if(j==2)
		{
			return true;
		}
	
	st.parents('.last_template-full_section').find(".res_attr").each(function() {
	var str=$(this); 
	
	i=2;
	scontent3=$(this).find("option:selected").val();
	//alert(scontent3);
	//return false;
 switch(scontent3) {
	
 case '0':
 
 
  break;
  case "1":
  
   str.empty();
   str.append('<option value="0">Select Option</option><option value="1" selected>Coupon</option>');
  st.parent().parent().children(".restrictionContainer").append('<div class="restriction-row"><div class="restriction_attr_selection"><a href="javascript:void(0);" class="res_close_btn"><img src="images/cross-icon.png"></a><select class="res_attr" name="resOptionVal[]"><option value="0">Select Option</option><option value="2">Studio</option></select></div></div>');
 
		break;
		 case "2":
    str.empty();
  str.append('<option value="0">Select Option</option><option value="2"  selected>Studio</option>');
   
   st.parent().parent().children(".restrictionContainer").append('<div class="restriction-row"><div class="restriction_attr_selection"><a href="javascript:void(0);" class="res_close_btn"><img src="images/cross-icon.png"></a><select class="res_attr"  name="resOptionVal[]"><option value="0">Select Option</option><option value="1">Coupon</option></select></div></div>');
 
		break;
   default:
   
   break;
		}
 
   		 
				 
	 });
		
		
		 if(i==1)
	{
	 $(this).parents(".last_template-full_section").children(".restrictionContainer").append('<div class="restriction-row"><div class="restriction_attr_selection"><a href="javascript:void(0);" class="res_close_btn"><img src="images/cross-icon.png"></a><select name="resOptionVal[]" class="res_attr"><option value="0">Select Option</option><option value="1">Coupon</option><option value="2">Studio</option></select></div></div>');
		
	}
		 
		 
		 
		
	});
	
		
	$(document).on("click",".addCF",function() {

		//code for copu
		var persDivId = $(this).prev().prev().attr("id");
			//alert(persDivId);
		var persDivIdArrRes = persDivId.split("_");
		var resSpecId = persDivIdArrRes[1];
		var studio_id='';
		studio_id +=$(".studio_select_val").val();
		var classid = $(this).attr('class');
		//alert(classid)
		fieldId_2++;

		var optionListHtml = "";

		$.ajax({
		url: "<?php echo base_url_site;?>/addon/listStudio",
		type: "post",
		data : {studio_id:studio_id},
		success: function (response) {
		var jsonData = $.parseJSON(response);
		var optionListLength  = jsonData.optionList.length; 


		if(optionListLength > 0) {
		optionListHtml= "<option value=''>Select Studios</option>";
		for(var k=0;k<optionListLength; k++){

		optionListHtml += "<option value="+jsonData.optionList[k].id+">"+jsonData.optionList[k].stName+'_'+jsonData.optionList[k].stNum+"</option>";
		//alert(optionListHtml);
		}
		$("."+classid).before('<span id="studio_select_more_ww'+persDivId+fieldId_2+'"></span><select id="studio_select_more_'+persDivId+fieldId_2+'" class="studio_select" name="studio[]">'+optionListHtml+'</select>');
		$("#studio_select_more_"+persDivId+fieldId_2).select2({ 
		width: '300px' });


		}
		else {

		//optionListHtml= "<option value=''>Select Studios</option>";

		//$("."+classid).before('<div class="remove_element remove_select"><select id="studio_select_more_'+persDivId+fieldId_2+'" class="studio_select" name="studioid_'+persDivId+'['+fieldId_2+']">'+optionListHtml+'</select></div>');
		//$("#studio_select_more_"+persDivId+fieldId_2).select2({ 
		//width: '300px' });

		} 


		}
		});
		//end of code

		
	
	
	//$(this).remove();	
	});
	
	
	// studio code start 
	$(document).on("click",".addCode",function() {

		//code for copu
		var persDivId = $(this).prev().prev().attr("id");
			//alert(persDivId);
		var persDivIdArrRes = persDivId.split("_");
		var resSpecId = persDivIdArrRes[1]; 
		var code_id=$.trim($(".code_select_val").val());
		//alert(code_id);
		var classid = $(this).attr('class');
		
		//var actionType = $("#actionType").val();
		
	 	/* $(".code_select_val").each(function(){
		  		$(".code_select_val").rules("add",{
					required:true
				});
		  });*/
			



		var optionListHtml = "";

		$.ajax({
		url: "<?php echo base_url_site;?>/addon/listCode",
		type: "post",
		data : {code_id:code_id},
		success: function (response) {
		var jsonData = $.parseJSON(response);
		var optionListLength  = jsonData.optionList.length; 
		

		if(optionListLength > 0) {
		optionListHtml= "<option value=''>Select Code</option>";
		for(var k=0;k<optionListLength; k++){

		optionListHtml += "<option value="+jsonData.optionList[k].id+">"+jsonData.optionList[k].code+"</option>";
		//alert(optionListHtml);
		}
		$("."+classid).before('<span id="code_select_more_ww'+persDivId+fieldId_2+'"></span><select id="code_select_more_'+persDivId+fieldId_2+'" class="code_select" name="studio[]">'+optionListHtml+'</select>');
		$("#code_select_more_"+persDivId+fieldId_2).select2({ 
		width: '300px' });


		}
		else {

		

		} 


		}
		});
		//end of code

		
	
	
	//$(this).remove();	
	});
	 
	//end of code
	
	
	
	
	$(document).on("change",".res_attr",function() {
		var resSpecId=0;          
		
	option=$(this).children('option:selected').text();
		switch(option) {
 case 'Coupon':
 $(this).nextAll().remove();
 $(this).parent().append('<select class="res_select_type" id="restopt" name="resCouponType[]"><option value="0">Option</option><option value="1">Fixed Amount</option><option value="2">Percent</option><option value="3">Code</option></select>');
 
  break;
  case 'Studio':
  $(this).nextAll().remove();
  $(this).parent().append('<select id="studio_res_select_'+resSpecId+'" class="studio_select" name="studioid_'+resSpecId+'[0]"><?php echo getStudioList();?></select><a href="javascript:void(0);" class="addCF" style="color:#6fcf00;font-size:25px;margin-left: 3px;"><i class="fa fa-plus-circle"></i></a>');
  $("#studio_res_select_"+resSpecId).select2({ width: '20%' });
		break;
   default:
   $(this).nextAll().remove();
   
   break;
		}
		
		
		
	});
	
	
	
	$(document).on('click',".resstu_close_btn",function() {
		
		$(this).next().remove();
		$(this).remove();
		
	});
	
	
	$(document).on("change",".res_select_type",function() {
		
		var resSpecId=0;
		
		option=$(this).children('option:selected').text();
		switch(option) {
 case 'Option':
 
 $(this).siblings("#t1").remove();
 
 
  break;
  case 'Fixed Amount':
  
		$(this).siblings("#t1").remove();
  
		$(this).parent().append('<div id="t1" style="float: right;width: 46%;" ><input type="text" name="amount[]" class="widthnew30 timepicker remspace decimal-only" id="fixprice" placeholder="" value=""></div>');
  
   break;
  case 'Percent':
  
        $(this).siblings("#t1").remove();
		$(this).parent().append('<div id="t1" style="float: right;width: 46%;" ><input type="text" name="amount[]" class="widthnew30 timepicker remspace number-only" id="Percentpr" placeholder="" value=""></div>');
	break;
	case 'Code':
  
	 $(this).siblings("#t1").remove();
     $(this).parent().append('<div id="t1" class="rpseb" style="display:inline-block; width: 22%; margin-right:15px;" ><select id="code_res_select_'+resSpecId+'" class="code_select widthnew30" name="code_'+resSpecId+'[0]"><?php echo getCodeList();?></select></div><a href="javascript:void(0);" class="addCode" style="color:#6fcf00;font-size:25px;margin-left: 3px;"><i class="fa fa-plus-circle"></i></a>');
	 $("#code_res_select_"+resSpecId).select2({ width: '100%' });
	break;
	
	default:
	
   $(this).siblings("#t1").remove();
   break;
		}
	});
		
	
					
		
});

	
<!-------  Restrictions   jquery end  ------->   
</script>

<style type="text/css">
.image-upload{
	color: red;
	font-size: 25px;
}
.image-upload>input {
  display: none;
}
.img_icon{
	margin:10px;
	float:left;
}
</style>

<?php
include('includes/footer.php');
?>