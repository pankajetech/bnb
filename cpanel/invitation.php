<?php include("includes/functions.php");?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>B&B</title>
<link href="<?php echo base_url_css?>style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url_css?>font-awesome.min.css">
<script src="<?php echo base_url_js?>jquery-1.12.4.js"></script>
<script src="<?php echo base_url_js?>jquery.validate-1.14.0.min.js"></script>
</head>

<body>
<div class="loading" style="display:none;">Loading&#8230;</div>
<div class="login-mid">
<div class="login-wrapper">
<div class="login-logo"><img src="<?php echo base_url_images?>login-logo.png" /></div>
<h2 class="login-heading">Set Password</h2>
<div class="login-content">
<div id="mail-status" class="event-msg"></div>


<form role="form" method="post" id="loginForm">
<input type="hidden" name="eid" id="eid" value="<?php echo $_REQUEST["eid"]; ?>">
<input class="form-control" placeholder="Password" name="password" type="password" id="password">
<input class="form-control" placeholder="Confirm Password" id="confirmpassword" name="confirmpassword" type="password" value="">
<input type="submit" value="Submit" id="btnS" name="btnS"/>
</form>
</div>

</div>
</div>

</body>
</html>
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>bootstrap-3.3.5.min.css">
    <script type="text/javascript" src="<?php echo base_url_js?>bootstrap-3.3.5.min.js" /></script>
    <script type="text/javascript" src="<?php echo base_url_js?>jquery-validate.bootstrap-tooltip.js" /></script>
<script>
 $(document).ready(function () {
$("#loginForm").validate({
           rules: {
               password: { 
                 required: true,
                    minlength: 6,
                    maxlength: 10,

               } , 

                   confirmpassword: { 
                    equalTo: "#password",
                     minlength: 6,
                     maxlength: 10
               }


           },
     messages:{
         password: { 
                 required:"The password is required"

               },
			   
			   
     },
	  submitHandler: function() {
					$("#loading").show();
					jQuery.ajax({
					url: "<?php echo base_url_site?>addon/ajaxSetPwd", 
					type: "post", //can be post or get
					data: {uid:$('#eid').val(),pwd:$('#password').val()}, 
					success: function(response){
					var data=$.parseJSON(response);
					$("#mail-status").html("");
					if(data.result==0) {
					$("#btnS").hide();					
					$("#mail-status").html("Successfully set your account password, you can login now - <a href='<?php echo base_url_site?>'>Login</a> ");
				
					} 

					}

					});
                    },

});



});


</script>
<style>
#password-error{ border: none !important;
    color: red;
    width: 100%;
    float: left;
    margin-top: -13px;
    margin-bottom: 10px;}
#confirmpassword-error{ border: none !important;
    color: red;
    width: 100%;
    float: left;
    margin-top: -13px;
    margin-bottom: 10px;}
</style>

