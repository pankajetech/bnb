<?php
include("includes/functions.php");
global $db;
$tbl = $_POST["tblName"];
$col1 = $_POST["fldName1"];
$col2 = $_POST["fldName2"];
$col3 = $_POST["fldName3"];
$col4 = $_POST["fldName4"];

$options="";
	
$sql = "SELECT $col1,$col2,evLocationId,evDate,evStTime,evSPubDate,evSPubTime,
		TIMESTAMP(`evSPubDate`, STR_TO_DATE(`evSPubTime`, '%h:%i %p')) as sortpubdtTime 
		FROM $tbl 
		WHERE evStatus=0 and isDeleted=0 and evIsClose=0 
		and evTypeStatus='Enabled' and isCancel=0 and isPublish=0 
		and id !=$col3 and evLocationId=$col4 
		and TIMESTAMP(`evSPubDate`, STR_TO_DATE(`evSPubTime`, '%h:%i %p')) <= (NOW() - ".INTERVALTIME.") and TIMESTAMP(`evDate`, STR_TO_DATE(`evStTime`, '%h:%i %p')) >= (NOW() - ".INTERVALTIME.")
		order by evDate ASC";
		
$results =  $db->rawQuery($sql);
$response = array();
if(count($results)>0){
	foreach($results as $key=>$val){
		$day = date("D",strtotime($val['evDate']));
		$evStTimeFormated = $val['evStTime'];
		if(!empty($evStTimeFormated)){
			$evStTimeFormatedArr = explode(" ",$evStTimeFormated);
			$evStTime1Arr = explode(":",$evStTimeFormatedArr[0]);
			
			if($evStTime1Arr[1] == "00" || $evStTime1Arr[1] == "0"){
				$evStTime1Str = $evStTime1Arr[0];
				$evStTime1StrForAMPM =  str_replace("m","",$evStTimeFormatedArr[1]);
				$eventFGinalFormatedStr = $evStTime1Str.$evStTime1StrForAMPM."&nbsp;&nbsp;&nbsp;";
			} else{
				$evStTime1Str = $evStTimeFormatedArr[0];
				$evStTime1StrForAMPM =  str_replace("m","",$evStTimeFormatedArr[1]);
				$eventFGinalFormatedStr = $evStTime1Str.$evStTime1StrForAMPM;
			}
		} 
		else {
			$eventFGinalFormatedStr = "";
		}
		$evDateFormated = date("m/d/Y",strtotime($val['evDate']));
		$results[$key]['evDateTimeFormated'] =  $day."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$eventFGinalFormatedStr."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$evDateFormated."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$val['evTitle'];
	}
	//echo "<pre>results==";print_r($results);die;
	$response["optionList"] = $results;
}	else {
	$response["optionList"] = array();
}
echo json_encode($response);
die;
?>