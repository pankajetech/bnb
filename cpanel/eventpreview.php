<?php 
session_start();
include('includes/functions.php');



if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 

if(isset($_REQUEST["evid"]) and $_REQUEST["evid"]!="") {    
//$numOrders = $result['Count'];
$params = array('');
$result = $db->rawQueryOne("SELECT * FROM bb_eventtemplates WHERE id='".$_REQUEST["evid"]."' ", $params);
$ritems = (array)$result;

		if(!empty($ritems)) { 
		$ritems=$ritems; 
		
			$params1 = array('');
			$result1 = $db->rawQuery("SELECT * FROM bb_evticketpackage WHERE evtempID='".$_REQUEST["evid"]."' and eventID=0  order by id asc ", $params1);
			//$ritems1 = (array)$result1;
		
		}
} else {
$ritems=array();
$ritems=$ritems;
}



	 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>B&B</title>
<link href="<?php echo base_url_css?>style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url_css?>font-awesome.min.css">




</head>
<!-- left content area start-->

<div class="right-wrapper" style="width:100%;">
<h1 class="pageheading">Events Template - <?php echo @fnCheckEmpty($ritems['evTempName']); ?></h1>
<div class="form-area">
<form role="form" method="post" action="" enctype="multipart/form-data" name="frmaddupEventTemp" id="frmaddupEventTemp">


<div class="template-info">
<div class="template-info-left">
<div class="input-row">
<input class="tempname remspace" type="text" name="evTempName" id="evTempName" placeholder="Event Template Name" value="<?php echo @fnCheckEmpty($ritems['evTempName']); ?>" disabled="disabled" />
<select class="eventtype" name="evType" id="evType" disabled="disabled">
<option value="">Event Type</option>
<?php echo fnDropDownList("bb_eventtype","id","etName",@fnCheckEmpty($ritems['evType']),"");?>
</select>
</div>
<div class="input-row"><input class="textfull remspace" disabled="disabled" type="text" name="evTempTitle" id="evTempTitle" placeholder="Event Title" value="<?php echo @fnCheckEmpty($ritems['evTempTitle']); ?>" /></div>
<div class="input-row"><textarea class="textareafull remspace" disabled="disabled" name="evTempDescription" id="evTempDescription" placeholder="Event Full Description"><?php echo @fnCheckEmpty($ritems['evTempDescription']); ?></textarea></div>
<div class="input-row">
<select class="clendar-color" name="evCalColor" id="evCalColor" disabled="disabled">
<option value="">Calendar Color</option>
<?php echo fnDropDownList("bb_calcolor","caCode","caName",@fnCheckEmpty($ritems['evCalColor']),"");?>
</select>
<input class="clendar-lavel remspace" type="text" disabled="disabled" name="evCalLabel" id="evCalLabel" placeholder="Calendar Label" value="<?php echo @fnCheckEmpty($ritems['evCalLabel']); ?>" />
</div>

</div>
<?php if(isset($ritems['evTempImage']) && $ritems['evTempImage']!=""){
			$imageurl=s3_url_uimages."uploads/events/".$ritems['evTempImage'];
	
			$imageName=$ritems['evTempImage'];
			 }  else  {
			 //$imageurl=base_url_site."images/placeholder.jpg";
			 $imageurl=base_url_images."placeholder.jpg";
			 $imageName="";
			 }
			 ?>
			<input type="hidden" name="flpup" value="<?php echo $imageName;?>" />
		<div class="gallery-img">
            <label id="upload-label" style="display:none;"><i class="fa fa-plus-circle"></i><input onchange="readURL(this);" name="evTempImage" id="img-upload" type="file"/></label>
           <img id="blah" src="<?php echo $imageurl?>" alt="" />
            </div>
</div>

<div class="schedule-section">
<h2 class="section-heading">Schedule</h2>
<div class="schedule-row"><span>Registration End</span><input class="tempname remspace" disabled="disabled" name="evScheduleHrs" id="evScheduleHrs" type="text" value="<?php echo @fnCheckEmpty($ritems['evScheduleHrs']); ?>" /> <span>Hours before event</span></div>
</div>

<div class="seats-section">
<h2 class="section-heading">Seats & Ticket Packages</h2>
<div class="input-row"><input class="numseats remspace" type="text" disabled="disabled" placeholder="Number of seats" name="evNoOfTickets" id="evNoOfTickets" value="<?php echo @fnCheckEmpty($ritems['evNoOfTickets']); ?>" /></div>
<div id="customFields">
<input type="hidden" id="totalTicketPackId" name="totalTicketPackId" value="<?php echo @countTicketP($_REQUEST["evid"])?>" />
<input type="hidden" id="rdids" name="rdids" value="" />

<?php 
$h=1;
if(isset($result1) && !empty($result1)) {
foreach($result1 as $k=>$tval) {

if($tval['isManual']==1) $ischk='checked="checked"'; else $ischk='';

?>



<div class="ticket-wrapper">
<div class="ticket-left">

<input type="hidden" name="ticketId" id="ticketId" value="<?php echo $h?>">
<input type="hidden" name="ticketIdT<?php echo $h?>" id="ticketIdT" value="<?php echo $tval['id']?>">
</div>
<div class="ticket-right" id="t_<?php echo $h?>">
<div class="ticket-right-row"><input class="ticket-name-small remspace" disabled="disabled" type="text" placeholder="Ticket Name" name="evTicketName<?php echo $h?>" id="evTicketName<?php echo $h?>"  value="<?php echo @fnCheckEmpty($tval['evTicketName']); ?>" /><input class="ticket-price-small2 remspace number-only" type="text" disabled="disabled" placeholder="Seat Value" name="evTicketSeatVal<?php echo $h?>" id="evTicketSeatVal<?php echo $h?>" value="<?php echo @fnCheckEmpty($tval['evSeatValue']); ?>" /><input class="ticket-price tprice" type="text" placeholder="Price" name="evTicketPrice<?php echo $h?>" id="evTicketPrice<?php echo $h?>" disabled="disabled" value="<?php echo @fnCheckEmpty($tval['evTicketPrice']); ?>" /></div>
<div class="ticket-right-row">
<input class="ticket-name-small remspace" type="text" disabled="disabled" placeholder="Add button label (eg 'being a friend for') (limit space)" name="evTicketButtonLabel<?php echo $h?>" id="evTicketButtonLabel<?php echo $h?>" value="<?php echo @fnCheckEmpty($tval['evTicketButtonLabel']); ?>" /><input class="ticket-price-small number-only" disabled="disabled" type="text" id="ticketLimit<?php echo $h?>" name="ticketLimit<?php echo $h?>" <?php echo $isdis?> placeholder="Ticket Value" value="<?php echo @fnCheckEmpty($tval['evTicketLImit']); ?>" /></div>
<div id="customSFields">

<?php if($tval['isManual']==0) {?>
<div class="ticket-right-row" id="to_<?php echo $h?>" >
<div class="append-row">
<div class="append-row-close">
<?php if($h!=1) {?>
<a href="javascript:void(0);" class="remSCF"><i class="fa fa-times-circle" style="font-size:19px;color:red"></i></a>
<?php }?>
</div>
<div class="append-row-element">
<div class="proOptions">
<select class="template_pe_dropdown proOpt" id="lstPProjectOptions" name="lstPProjectOptionsT<?php echo $h?>" disabled="disabled">
<option value="">Options</option>
<?php echo fnDropDownListProOptions("bb_proclassoptions","id","opName","opPrice",@fnCheckEmpty($tval['evTicketOptionId']),"");?>
</select>
<?php 
$cattemp="";
$typetemp="";
$protemp="";
$cattempIds="";
$typetempIds="";
$protempIds="";


if(isset($tval['evTicketCatIds']) && !empty($tval['evTicketCatIds'])) {
$cattempIds=$tval['evTicketCatIds'];
//$cattemp="Categories - ";
				$oids=explode(",",$tval['evTicketCatIds']);
						foreach($oids as $co=>$oval) {
						$cattemp.='<div class="template_pe_dropdown-box"><input placeholder="'.getNamebyPara("bb_procategory","proCatName","id",$oval).'" type="text" readonly></div>';
						}
}

if(isset($tval['evTicketTypeIds']) && !empty($tval['evTicketTypeIds'])) {
$typetempIds=$tval['evTicketTypeIds'];
//$typetemp="Types - ";
				$oids=explode(",",$tval['evTicketTypeIds']);
						foreach($oids as $co=>$oval) {
								$typetemp.='<div class="template_pe_dropdown-box"><input placeholder="'.getNamebyPara("bb_protype","proTypeName","id",$oval).'" type="text" readonly></div>';
								}
}

if(isset($tval['evTicketProjectIds']) && !empty($tval['evTicketProjectIds'])) {
$protempIds=$tval['evTicketProjectIds'];
//$protemp="Projects - ";
				$oids=explode(",",$tval['evTicketProjectIds']);
							foreach($oids as $co=>$oval) {
								$protemp.='<div class="template_pe_dropdown-box"><input placeholder="'.getNamebyPara("bb_project","proName","id",$oval).'" type="text" readonly></div>';
								}
}



?>

 <?php echo $cattemp; ?>

<?php echo $typetemp; ?>

<?php echo $protemp; ?>

</div>


</div>
</div>


</div>
<?php } else {?>
<div class="ticket-right-row" id="to_<?php echo $h?>" style="display:none;">
<div class="append-row">
<div class="append-row-close"></div>
<div class="append-row-element">
<div class="proOptions">
<select class="template_pe_dropdown proOpt" id="lstPProjectOptions<?php echo $h?>" disabled="disabled" name="lstPProjectOptionsT<?php echo $h?>">
<option value="">Options</option>
<?php echo fnDropDownListProOptions("bb_proclassoptions","id","opName","opPrice","","");?>
</select>
<a href="javascript:void(0);" class="add-cat padd" id="1"><i class="fa fa-plus-circle"></i><span>Add Category<input type="hidden" id="catids" name="catidsT<?php echo $h?>" value="" /></span></a>
<a href="javascript:void(0);" class="add-cat padd" id="2"><i class="fa fa-plus-circle"></i><span>Add Type<input type="hidden" id="typeids" name="typeidsT<?php echo $h?>" value="" /></span></a>
<a href="javascript:void(0);" class="add-cat padd" id="3"><i class="fa fa-plus-circle"></i><span>Add Project<input type="hidden" id="projectids" name="projectidsT<?php echo $h?>" value="" /></span></a>
</div>


</div>
</div>


</div>
<?php }?>
</div>

</div>


</div>

<?php
$h++;
}
 } else { ?>
<div class="ticket-wrapper" style="display:none;">
<div class="ticket-left"><input type="hidden" class="tcount" name="ticketId" id="ticketId" value="1"></div>
<div class="ticket-right" id="t_1">
<div class="ticket-right-row"><input class="ticket-name-small remspace" disabled="disabled" type="text" placeholder="Ticket Name" name="evTicketName1" id="evTicketName1" /><input class="ticket-price-small2 remspace number-only" type="text" disabled="disabled" placeholder="Seat Value" name="evTicketSeatVal1" id="evTicketSeatVal1" value="1" readonly="readonly" /><input class="ticket-price tprice" disabled="disabled" type="text" placeholder="Price" name="evTicketPrice1" id="evTicketPrice1" /></div>
<div class="ticket-right-row"><input class="ticket-name-small remspace" type="text" disabled="disabled" placeholder="Add button label (eg 'being a friend for') (limit space)" name="evTicketButtonLabel1" id="evTicketButtonLabel1" /><input class="ticket-price-small number-only" type="text" id="ticketLimit1" disabled="disabled" name="ticketLimit1" placeholder="Ticket Value" value="" /><input type="hidden" class="ticket-check iscmanul" id="isCManual1" name="isCManual1" value="0" /><input type="checkbox" class="ticket-check ismanul" id="isManual1" name="isManual1" value="0" disabled="disabled" /> Manual Ticket</div>
<div id="customSFields">
<div class="ticket-right-row" id="to_1">
<div class="append-row">
<div class="append-row-close"></div>
<div class="append-row-element">
<div class="proOptions">
<select class="template_pe_dropdown proOpt" id="lstPProjectOptions1" name="lstPProjectOptionsT1" disabled="disabled">
<option value="">Options</option>
<?php echo fnDropDownListProOptions("bb_proclassoptions","id","opName","opPrice","","");?>
</select>

</div>


</div>
</div>


</div>
</div>
<!--<div class="insert-row"><a href="javascript:void(0);" class="addSCF"><i class="fa fa-plus-circle"></i></a></div>-->
</div>


</div>
<?php }?>
</div>


</form>
</div>

</div>
</div>

<!-- left content area end-->


</body>
</html>
