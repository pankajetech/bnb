<?php
require_once ('config_inc.php');
// Get the IP address of the visitor so we can work with it later.
$ip = $_SERVER['REMOTE_ADDR'];
 
function fnCheckEmpty($item) {
if(!empty($item) and $item!="") {
$item=$item;
} else {
$item="";
}
return $item;
}
 
function getExtension($str) 
{
$i = strrpos($str,".");
if (!$i) { return ""; }
$l = strlen($str) - $i;
$ext = substr($str,$i+1,$l);
return $ext;
}
//Here you can add valid file extensions. 
$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");


function upload_image_to_s3logo($md5filename,$filename,$tmpfilename,$fldname,$resizefldname,$flg,$flpup){

    //Grab this class and place it in a file called S3.php in the same directory
    //  https://github.com/tpyo/amazon-s3-php-class/blob/master/S3.php
 $s3 = new S3(awsAccessKey, awsSecretKey);  

    //Change "bountify" to the name of folder for where you want your images 
    $uploadName = $fldname;



$fileName = $filename;
$fileTempName = $tmpfilename;
$ext =  @getExtension($fileName);

  
$fileNameOrigional = $md5filename;


$ext = strtolower($ext);
if($ext=="jpg" || $ext=="jpeg") {	
$image = imagecreatefromjpeg($fileTempName);
}
if($ext=="gif") {	
$image = imagecreatefromgif($fileTempName);
}
if($ext=="png") {	
$image = imagecreatefrompng($fileTempName);
}
if($ext=="bmp") {	
$image = @imagecreatefrombmp($fileTempName);
}

switch($ext){
      case 'bmp': $ContentType = "image/bmp"; break;
      case 'gif':  $ContentType = "image/gif"; break;
      case 'jpg':  $ContentType = "image/jpg"; break;
      case 'png':  $ContentType = "image/png"; break;
      default : $ContentType = "image/jpeg";
    } 

$thumb = imagecreatetruecolor( $thumb_width, $thumb_height );


    //Determining content type based on filenames, its a quick hack but mostly accurate 
    $type = strtolower(substr(strrchr($filename,"."),1));
    if($type == 'jpeg') $type = 'jpg';
    if($type == 'JPEG') $type = 'jpg';
    if($type == 'JPG') $type = 'jpg';
	 if($type == 'jpg') $type = 'jpg';
    if($type == 'GIF') $type = 'gif';
    if($type == 'BMP') $type = 'bmp';
    if($type == 'PNG') $type = 'png';
	if($type == 'png') $type = 'png';
	
	


	
if ($s3->putObjectFile($fileTempName, bucketName, $fldname.$resizefldname.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ)) {
if(isset($flpup) && $flpup) {
$re=$s3->deleteObject(bucketName, $fldname.$flpup);
}
unlink($fldname.$fileName);
}	
	
	
  
}





function upload_image_to_s3($md5filename,$filename,$tmpfilename,$fldname,$resizefldname,$flg,$flpup){

    //Grab this class and place it in a file called S3.php in the same directory
    //  https://github.com/tpyo/amazon-s3-php-class/blob/master/S3.php
 $s3 = new S3(awsAccessKey, awsSecretKey);  

    //Change "bountify" to the name of folder for where you want your images 
    $uploadName = $fldname;



$fileName = $filename;
$fileTempName = $tmpfilename;
$ext =  @getExtension($fileName);
$fileNameOrigional = $md5filename;
$ext = strtolower($ext);
if($ext=="jpg" || $ext=="jpeg") {	
$image = imagecreatefromjpeg($fileTempName);
}
if($ext=="gif") {	
$image = imagecreatefromgif($fileTempName);
}
if($ext=="png") {	
$image = imagecreatefrompng($fileTempName);
}
if($ext=="bmp") {	
$image = @imagecreatefrombmp($fileTempName);
}



$new_width_m =  700; 

$max_width = $new_width_m;
$width = imagesx($image);
$height = imagesy($image);


					if($width < $max_width)
					$newwidth = $width;
					else
					$newwidth = $max_width; 
					
					$divisor = $width / $newwidth;
					$newheight = floor( $height / $divisor);
					
					
					$thumb_width = $newwidth;
					$thumb_height = $newheight;






$original_aspect = $width / $height;
$thumb_aspect = $thumb_width / $thumb_height;

if ( $original_aspect >= $thumb_aspect )
{
   // If image is wider than thumbnail (in aspect ratio sense)
   $new_height = $thumb_height;
   $new_width = $width / ($height / $thumb_height);
}
else
{
   // If the thumbnail is wider than the image
   $new_width = $thumb_width;
   $new_height = $height / ($width / $thumb_width);
}
	

switch($ext){
      case 'bmp': $ContentType = "image/bmp"; break;
      case 'gif':  $ContentType = "image/gif"; break;
      case 'jpg':  $ContentType = "image/jpg"; break;
      case 'png':  $ContentType = "image/png"; break;
      default : $ContentType = "image/jpeg";
    } 

$thumb = imagecreatetruecolor( $thumb_width, $thumb_height );


    //Determining content type based on filenames, its a quick hack but mostly accurate 
    $type = strtolower(substr(strrchr($filename,"."),1));
    if($type == 'jpeg') $type = 'jpg';
    if($type == 'JPEG') $type = 'jpg';
    if($type == 'JPG') $type = 'jpg';
	 if($type == 'jpg') $type = 'jpg';
    if($type == 'GIF') $type = 'gif';
    if($type == 'BMP') $type = 'bmp';
    if($type == 'PNG') $type = 'png';
	 if($type == 'png') $type = 'png';
	
$chkevent=explode("!",$fileNameOrigional);	

switch ($type) {
		case 'jpg':
			
			if(isset($resizefldname) && $resizefldname=="logo/") {
			
			imagecopyresampled($thumb,$image,0,0,0,0,$thumb_width,$thumb_height, 755,747);
			imagejpeg($thumb,sPath.$fldname.$resizefldname.$fileNameOrigional.'.'. $type);
			
			$file_url1=$fldname.$resizefldname.$fileNameOrigional.'.'. $type;
			$url_parts1 = parse_url($file_url1);
			$full_path1 = sPath . substr($url_parts1['path'], 0);
			/*echo $fldname.$resizefldname.$flg.$fileNameOrigional.'.'. $ext;
			die();*/
			$s3->putObjectFile($full_path1, bucketName, $fldname.$resizefldname.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ);
			unlink($full_path1);
			
				if(isset($flpup) && $flpup) {
				$re=$s3->deleteObject(bucketName, $fldname.$resizefldname.$flpup);
				}
			
			
			
			} else {
			
			
			
			imagecopyresampled($thumb,$image,0 - ($new_width - $thumb_width) / 2,0 - ($new_height - $thumb_height) / 2,0, 0,$new_width, $new_height,$width, $height);
			imagejpeg($thumb, sPath.$fldname.$resizefldname.$fileNameOrigional.'.'. $type, 127);
			
			$file_url=$fldname.$resizefldname.$fileNameOrigional.'.'. $type;
			$url_parts = parse_url($file_url);
			$full_path = sPath . substr($url_parts['path'], 0);
	
			$s3->putObjectFile($full_path, bucketName, $fldname.$resizefldname.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ);
			unlink($full_path);
			
			if((isset($flpup) && $flpup!="") && empty($chkevent[0])) {
				$re=$s3->deleteObject(bucketName, $fldname.$resizefldname.$flpup);
				}
			
			}
			
			
			
			
	
		/*	if(isset($flg) && $flg!="") {
			

			imagecopyresampled($thumb,$image,0,0,0,0,$thumb_width,$thumb_height, 100,100);
			imagejpeg($thumb,sPath.$fldname.$flg.$fileNameOrigional.'.'. $type);
			
			$file_url1=$fldname.$flg.$fileNameOrigional.'.'. $type;
			$url_parts1 = parse_url($file_url1);
			$full_path1 = sPath . substr($url_parts1['path'], 0);

			$s3->putObjectFile($full_path1, bucketName, $fldname.$flg.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ);
			unlink($full_path1);
			
				if(isset($flpup) && $flpup) {
				$re=$s3->deleteObject(bucketName, $fldname.$flg.$flpup);
				}
			
			}
			*/
			break;

		case 'gif':
		//case "gif";
			if(isset($resizefldname) && $resizefldname=="logo/") {
			
			imagecopyresampled($thumb,$image,0,0,0,0,$thumb_width,$thumb_height, 755,747);
			imagegif($thumb,sPath.$fldname.$resizefldname.$fileNameOrigional.'.'. $type);
			
			$file_url1=$fldname.$resizefldname.$fileNameOrigional.'.'. $type;
			$url_parts1 = parse_url($file_url1);
			$full_path1 = sPath . substr($url_parts1['path'], 0);
			/*echo $fldname.$resizefldname.$flg.$fileNameOrigional.'.'. $ext;
			die();*/
			$s3->putObjectFile($full_path1, bucketName, $fldname.$resizefldname.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ);
			unlink($full_path1);
			if(isset($flpup) && $flpup) {
				$re=$s3->deleteObject(bucketName, $fldname.$resizefldname.$flpup);
				}
			
			
			} else {
		
		
			imagecopyresampled($thumb,$image,0 - ($new_width - $thumb_width) / 2,0 - ($new_height - $thumb_height) / 2,0, 0,$new_width, $new_height,$width, $height);
			imagegif($thumb, sPath.$fldname.$resizefldname.$fileNameOrigional.'.'. $type, 127);
		
			$file_url=$fldname.$resizefldname.$fileNameOrigional.'.'. $type;
			$url_parts = parse_url($file_url);
			$full_path = sPath . substr($url_parts['path'], 0);
			//echo $full_path;
			//die();
			$s3->putObjectFile($full_path, bucketName, $fldname.$resizefldname.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ);
			unlink($full_path);
			
			if((isset($flpup) && $flpup!="") && empty($chkevent[0])) {
				$re=$s3->deleteObject(bucketName, $fldname.$resizefldname.$flpup);
				}
			}
			
		/*	if(isset($flg) && $flg!="") {
			imagecopyresampled($thumb,$image,0 - ($new_width - $thumb_width) / 2,0 - ($new_height - $thumb_height) / 2,0, 0,100, 100,$width, $height);
			imagegif($thumb,sPath.$fldname.$flg.$fileNameOrigional.'.'. $type);
			
			$file_url1=$fldname.$flg.$fileNameOrigional.'.'. $type;
			$url_parts1 = parse_url($file_url1);
			$full_path1 = sPath . substr($url_parts1['path'], 0);

			$s3->putObjectFile($full_path1, bucketName, $fldname.$flg.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ);
			unlink($full_path1);
			
			if(isset($flpup) && $flpup) {
				$re=$s3->deleteObject(bucketName, $fldname.$flg.$flpup);
				}
			
			}*/
			
			

			break;

		case 'png':
		//case "png";
		if(isset($resizefldname) && $resizefldname=="logo/") {
			
			imagecopyresampled($thumb,$image,0,0,0,0,$thumb_width,$thumb_height, 755,747);
			imagepng($thumb,sPath.$fldname.$resizefldname.$fileNameOrigional.'.'. $type,8);
			
			$file_url1=$fldname.$resizefldname.$fileNameOrigional.'.'. $type;
			$url_parts1 = parse_url($file_url1);
			$full_path1 = sPath . substr($url_parts1['path'], 0);
			/*echo $fldname.$resizefldname.$flg.$fileNameOrigional.'.'. $ext;
			die();*/
			$s3->putObjectFile($full_path1, bucketName, $fldname.$resizefldname.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ);
			unlink($full_path1);
			if(isset($flpup) && $flpup) {
				$re=$s3->deleteObject(bucketName, $fldname.$resizefldname.$flpup);
				}
			
			
			} else {
		
			imagecopyresampled($thumb,$image,0 - ($new_width - $thumb_width) / 2,0 - ($new_height - $thumb_height) / 2,0, 0,$new_width, $new_height,$width, $height);
		
			imagepng($thumb, sPath.$fldname.$resizefldname.$fileNameOrigional.'.'. $type, 8);
			
			$file_url=$fldname.$resizefldname.$fileNameOrigional.'.'. $type;
			$url_parts = parse_url($file_url);
			$full_path = sPath . substr($url_parts['path'], 0);
			
			$s3->putObjectFile($full_path, bucketName, $fldname.$resizefldname.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ);
			unlink($full_path);
			
			if((isset($flpup) && $flpup!="") && empty($chkevent[0])) {
				$re=$s3->deleteObject(bucketName, $fldname.$resizefldname.$flpup);
				}
			
			}
			


			break;

		/*default:
			$imageProcess = 0;
			break;*/
	}

	
if ($s3->putObjectFile($fileTempName, bucketName, $fldname.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ)) {
if((isset($flpup) && $flpup!="") && empty($chkevent[0])) {
$re=$s3->deleteObject(bucketName, $fldname.$flpup);
}
unlink($fldname.$fileName);
}	
	
	
  
}


////upload project s3 code
function upload_image_to_s3project($md5filename,$filename,$tmpfilename,$fldname,$resizefldname,$flg,$flpup){

    //Grab this class and place it in a file called S3.php in the same directory
    //  https://github.com/tpyo/amazon-s3-php-class/blob/master/S3.php
 $s3 = new S3(awsAccessKey, awsSecretKey);  

    //Change "bountify" to the name of folder for where you want your images 
    $uploadName = $fldname;


$fileName = $filename;
$fileTempName = $tmpfilename;
$ext =  @getExtension($fileName);

$fileNameOrigional = $md5filename;


$thumb_width = 248;
$thumb_height = 213;


$ext = strtolower($ext);
if($ext=="jpg" || $ext=="jpeg") {	
$image = imagecreatefromjpeg($fileTempName);
}
if($ext=="gif") {	
$image = imagecreatefromgif($fileTempName);
}
if($ext=="png") {	
$image = imagecreatefrompng($fileTempName);
}
if($ext=="bmp") {	
$image = @imagecreatefrombmp($fileTempName);
}


$width = imagesx($image);
$height = imagesy($image);

$original_aspect = $width / $height;
$thumb_aspect = $thumb_width / $thumb_height;

if ( $original_aspect >= $thumb_aspect )
{
   // If image is wider than thumbnail (in aspect ratio sense)
   $new_height = $thumb_height;
   $new_width = $width / ($height / $thumb_height);
}
else
{
   // If the thumbnail is wider than the image
   $new_width = $thumb_width;
   $new_height = $height / ($width / $thumb_width);
}

$thumb = imagecreatetruecolor( $thumb_width, $thumb_height );



$type = strtolower(substr(strrchr($filename,"."),1));
if($type == 'jpeg') $type = 'jpg';
if($type == 'JPEG') $type = 'jpg';
if($type == 'JPG') $type = 'jpg';
if($type == 'jpg') $type = 'jpg';
if($type == 'GIF') $type = 'gif';
if($type == 'BMP') $type = 'bmp';
if($type == 'PNG') $type = 'png';
if($type == 'png') $type = 'png';

$chkevent=explode("!",$fileNameOrigional);	


switch ($type) {
		case 'jpg':

			imagecopyresampled($thumb,$image,0 - ($new_width - $thumb_width) / 2,0 - ($new_height - $thumb_height) / 2,0, 0,$new_width, $new_height,$width, $height);
			imagejpeg($thumb, sPath.$fldname.$resizefldname.$fileNameOrigional.'.'. $type, 80);
		
			$file_url=$fldname.$resizefldname.$fileNameOrigional.'.'. $type;
			$url_parts = parse_url($file_url);
			$full_path = sPath . substr($url_parts['path'], 0);
			/*echo $full_path;
			die();*/
			$s3->putObjectFile($full_path, bucketName, $fldname.$resizefldname.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ);
			unlink($full_path);
			
			if((isset($flpup) && $flpup!="") && empty($chkevent[0])) {
				$re=$s3->deleteObject(bucketName, $fldname.$resizefldname.$flpup);
				}
		
		break;

		case 'gif':
			imagecopyresampled($thumb,$image,0 - ($new_width - $thumb_width) / 2,0 - ($new_height - $thumb_height) / 2,0, 0,$new_width, $new_height,$width, $height);
			imagegif($thumb, sPath.$fldname.$resizefldname.$fileNameOrigional.'.'. $type, 80);
		
			$file_url=$fldname.$resizefldname.$fileNameOrigional.'.'. $type;
			$url_parts = parse_url($file_url);
			$full_path = sPath . substr($url_parts['path'], 0);
			//echo $full_path;
			//die();
			$s3->putObjectFile($full_path, bucketName, $fldname.$resizefldname.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ);
			unlink($full_path);
			
			if((isset($flpup) && $flpup!="") && empty($chkevent[0])) {
				$re=$s3->deleteObject(bucketName, $fldname.$resizefldname.$flpup);
				}
		
		break;

		case 'png':
		
			imagecopyresampled($thumb,$image,0 - ($new_width - $thumb_width) / 2,0 - ($new_height - $thumb_height) / 2,0, 0,$new_width, $new_height,$width, $height);
		
			imagepng($thumb, sPath.$fldname.$resizefldname.$fileNameOrigional.'.'. $type, 8);
			
			$file_url=$fldname.$resizefldname.$fileNameOrigional.'.'. $type;
			$url_parts = parse_url($file_url);
			$full_path = sPath . substr($url_parts['path'], 0);
			
			$s3->putObjectFile($full_path, bucketName, $fldname.$resizefldname.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ);
			unlink($full_path);
			
			if((isset($flpup) && $flpup!="") && empty($chkevent[0])) {
				$re=$s3->deleteObject(bucketName, $fldname.$resizefldname.$flpup);
				}
		
		break;
}		


	
if ($s3->putObjectFile($fileTempName, bucketName, $fldname.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ)) {
if((isset($flpup) && $flpup!="") && empty($chkevent[0])) {
$re=$s3->deleteObject(bucketName, $fldname.$flpup);
}
unlink($fldname.$fileName);
}	
	
	
  
}




function upload_image_to_s3event($md5filename,$filename,$tmpfilename,$fldname,$resizefldname,$flg,$flpup){

    //Grab this class and place it in a file called S3.php in the same directory
    //  https://github.com/tpyo/amazon-s3-php-class/blob/master/S3.php
 $s3 = new S3(awsAccessKey, awsSecretKey);  

    //Change "bountify" to the name of folder for where you want your images 
    $uploadName = $fldname;


$fileName = $filename;
$fileTempName = $tmpfilename;
$ext =  @getExtension($fileName);

$fileNameOrigional = $md5filename;


$thumb_width = 248;
$thumb_height = 213;


$ext = strtolower($ext);
if($ext=="jpg" || $ext=="jpeg") {	
$image = imagecreatefromjpeg($fileTempName);
}
if($ext=="gif") {	
$image = imagecreatefromgif($fileTempName);
}
if($ext=="png") {	
$image = imagecreatefrompng($fileTempName);
}
if($ext=="bmp") {	
$image = @imagecreatefrombmp($fileTempName);
}


$width = imagesx($image);
$height = imagesy($image);

$original_aspect = $width / $height;
$thumb_aspect = $thumb_width / $thumb_height;

if ( $original_aspect >= $thumb_aspect )
{
   // If image is wider than thumbnail (in aspect ratio sense)
   $new_height = $thumb_height;
   $new_width = $width / ($height / $thumb_height);
}
else
{
   // If the thumbnail is wider than the image
   $new_width = $thumb_width;
   $new_height = $height / ($width / $thumb_width);
}

$thumb = imagecreatetruecolor( $thumb_width, $thumb_height );



$type = strtolower(substr(strrchr($filename,"."),1));
if($type == 'jpeg') $type = 'jpg';
if($type == 'JPEG') $type = 'jpg';
if($type == 'JPG') $type = 'jpg';
if($type == 'jpg') $type = 'jpg';
if($type == 'GIF') $type = 'gif';
if($type == 'BMP') $type = 'bmp';
if($type == 'PNG') $type = 'png';
if($type == 'png') $type = 'png';

$chkevent=explode("!",$fileNameOrigional);	


switch ($type) {
		case 'jpg':

			imagecopyresampled($thumb,$image,0 - ($new_width - $thumb_width) / 2,0 - ($new_height - $thumb_height) / 2,0, 0,$new_width, $new_height,$width, $height);
			imagejpeg($thumb, sPath.$fldname.$resizefldname.$fileNameOrigional.'.'. $type, 80);
		
			$file_url=$fldname.$resizefldname.$fileNameOrigional.'.'. $type;
			$url_parts = parse_url($file_url);
			$full_path = sPath . substr($url_parts['path'], 0);
			/*echo $full_path;
			die();*/
			$s3->putObjectFile($full_path, bucketName, $fldname.$resizefldname.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ);
				unlink($full_path);
			
			if((isset($flpup) && $flpup!="") && empty($chkevent[0])) {
				$re=$s3->deleteObject(bucketName, $fldname.$resizefldname.$flpup);
				}
		
		break;

		case 'gif':
			imagecopyresampled($thumb,$image,0 - ($new_width - $thumb_width) / 2,0 - ($new_height - $thumb_height) / 2,0, 0,$new_width, $new_height,$width, $height);
			imagegif($thumb, sPath.$fldname.$resizefldname.$fileNameOrigional.'.'. $type, 80);
		
			$file_url=$fldname.$resizefldname.$fileNameOrigional.'.'. $type;
			$url_parts = parse_url($file_url);
			$full_path = sPath . substr($url_parts['path'], 0);
			//echo $full_path;
			//die();
			$s3->putObjectFile($full_path, bucketName, $fldname.$resizefldname.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ);
			unlink($full_path);
			
			if((isset($flpup) && $flpup!="") && empty($chkevent[0])) {
				$re=$s3->deleteObject(bucketName, $fldname.$resizefldname.$flpup);
				}
		
		break;

		case 'png':
		
			imagecopyresampled($thumb,$image,0 - ($new_width - $thumb_width) / 2,0 - ($new_height - $thumb_height) / 2,0, 0,$new_width, $new_height,$width, $height);
		
			imagepng($thumb, sPath.$fldname.$resizefldname.$fileNameOrigional.'.'. $type, 8);
			
			$file_url=$fldname.$resizefldname.$fileNameOrigional.'.'. $type;
			$url_parts = parse_url($file_url);
			$full_path = sPath . substr($url_parts['path'], 0);
			
			$s3->putObjectFile($full_path, bucketName, $fldname.$resizefldname.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ);
			unlink($full_path);
			
			if((isset($flpup) && $flpup!="") && empty($chkevent[0])) {
				$re=$s3->deleteObject(bucketName, $fldname.$resizefldname.$flpup);
				}
		
		break;
}		


	
if ($s3->putObjectFile($fileTempName, bucketName, $fldname.$fileNameOrigional.'.'. $type, S3::ACL_PUBLIC_READ)) {
if((isset($flpup) && $flpup!="") && empty($chkevent[0])) {
$re=$s3->deleteObject(bucketName, $fldname.$flpup);
}
unlink($fldname.$fileName);
}	
	
	
  
}




function slugify1($str) {
  // Convert to lowercase and remove whitespace
  $str = strtolower(trim($str));  

  // Replace high ascii characters
  $chars = array("�", "�", "�", "�");
  $replacements = array("ae", "oe", "ue", "ss");
  $str = str_replace($chars, $replacements, $str);
  $pattern = array("/(�|�|�|�)/", "/(�|�|�|�)/", "/(�|�|�|�)/");
  $replacements = array("e", "o", "u");
  $str = preg_replace($pattern, $replacements, $str);

  // Remove puncuation
  $pattern = array(":", "!", "?", ".", "/", "'");
  $str = str_replace($pattern, "", $str);

  // Hyphenate any non alphanumeric characters
  $pattern = array("/[^a-z0-9-]/", "/-+/");
  $str = preg_replace($pattern, "", $str);

  return $str;
}

function check_slug($tblname,$slug, $id) {
global $db;
	$invalidSlug = true;
	$count = 1;
	/*echo $slug;
	die();*/
$stwh="";
if(isset($id) && $id!="") {
$stwh=" and id<>".$id;
}
	
	while ($invalidSlug) {
		
		$sslug = $db->rawQuery ("SELECT id FROM $tblname where slug='".$slug."' $stwh", array(''));

		if (count($sslug) > 0) {
			$count++;
			if ($count == 2) {
				$slug = $slug.'-'.$count;
				break;
			} else {
				$slug++;
			}
			//continue;
		} else {
			$invalidSlug = false;
		}
	}
	return $slug;
}


function check_slug_event($tblname,$slug,$id,$studioId,$slug_old,$evTitle) {
	global $db;
$unwanted_array = array(    '�'=>'S', '�'=>'s', '�'=>'Z', '�'=>'z', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'C', '�'=>'E', '�'=>'E',
                            '�'=>'E', '�'=>'E', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'N', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'U',
                            '�'=>'U', '�'=>'U', '�'=>'U', '�'=>'Y', '�'=>'B', '�'=>'Ss', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'c',
                            '�'=>'e', '�'=>'e', '�'=>'e', '�'=>'e', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'o', '�'=>'n', '�'=>'o', '�'=>'o', '�'=>'o', '�'=>'o',
                            '�'=>'o', '�'=>'o', '�'=>'u', '�'=>'u', '�'=>'u', '�'=>'y', '�'=>'b', '�'=>'y' );
							
 $str = strtr($slug, $unwanted_array );
 
 // $slug = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($str)));
  
  $slug = slugify1(trim(strtolower($str)));

	$stwh="";
	$stwh1="";
	if(isset($id) && $id!="") {
	//$stwh=" and id<>".$id;
	$stwh1=" and id=".$id;
	}

	$stwh.=" and studioId=".$studioId."";
$chk=0;	
if(isset($id) && $id!="") {

//and evTitle='".$evTitle."'
$chkq=$db->rawQueryOne("SELECT slug FROM $tblname WHERE slug='".$slug_old."' and evStatus=0 and studioId='".$studioId."'", array(''));
if(isset($chkq['slug']) && !empty($chkq['slug'])) {
$chk=1;
} else {
$chk=0;
}
}


	
if($chk==0) {
 $sql = "SELECT slug FROM $tblname WHERE slug like '%".$slug."%' $stwh";	

 $sslug = $db->rawQuery ($sql, array(''));

 if($sslug)
 {
  $total_row = $db->count;
  if($total_row >0)
  {

   foreach($sslug as $kr=>$row)
   {
    $data[] = $row['slug'];
   }
   
   if(in_array($slug, $data))
   {
   
    $count = $total_row;
    while( in_array( ($slug . '-' . ++$count ), $data) );
    $slug = $slug . '-'.$studioId.'-' .time(). ($count);
	

   } else {
  
   if(isset($slug_old) && $slug_old!="") {
$slug = $slug_old;
} else {
$slug = $slug. '-'.$studioId.'-' .time();
}
   }
  }
 }	
} else {
if(isset($slug_old) && $slug_old!="") {
$slug = $slug_old;
} else {
$slug = $slug. '-'.$studioId.'-' .time();
}

}	
/*echo $slug."--".$total_row;

die();	*/							
return $slug;	
	
}	



/*function check_slug_event($tblname,$slug,$id,$studioId) {
	global $db;
	$invalidSlug = true;
	$count = 1;
	
	$stwh="";
	$stwh1="";
	if(isset($id) && $id!="") {
	$stwh=" and id<>".$id;
	$stwh1=" and id=".$id;
	}
	$stwh=" and studioId=".$studioId." and isDeleted=0";

	
		$sql="SELECT id FROM $tblname where slug='".$slug."' $stwh";
		$sslug = $db->rawQuery ($sql, array(''));
		//echo $sslug1['slug']."==".$slug;
		if (count($sslug) > 0) {
				$slug = $slug."-".$studioId."-".mt_rand();
				
		
		} else {
		$slug = $slug;
			
		}

	return $slug;
}
*/
/*function check_slug_event($tblname,$slug,$id,$studioId) {
	global $db;
	$invalidSlug = true;
	$count = 1;
	
	$stwh="";
	if(isset($id) && $id!="") {
	$stwh=" and id<>".$id;
	}
	$stwh=" and studioId=".$studioId." and isDeleted=0";
	while ($invalidSlug) {
		$sql="SELECT id FROM $tblname where slug='".$slug."' $stwh";
		echo $sql;
		die();
		$sslug = $db->rawQuery ($sql, array(''));
		if (count($sslug) > 0) {
			$count++;
			if ($count == 2) {
				$slug = $slug.'-'.$count;
			} else {
				$slug++;
			}
			continue;
		} else {
			$invalidSlug = false;
		}
	}
	echo $slug."-".$id."-".$studioId."-".$tblname;
	die();
	return $slug;
}*/


function chkSlugExit($tblname,$slugname,$id) {
global $db;
$stwh="";
if(isset($id) && $id!="") {
$stwh=" and id<>".$id;
}

$sslug = $db->rawQueryOne ("SELECT COUNT(*) AS NumHits FROM $tblname where slug='".$slugname."' $stwh", array(''));
if(count($sslug)>0) {
$numHits = $sslug['NumHits'];
return ($numHits > 0) ? ($slugname . '-' . $numHits) : $slug;
} else {
return false;
}
}

function fnDate($date) {
$dt=explode("-",$date);
return $dt[2]."-".$dt[1]."-".$dt[0];
}


function fnDropDownList($tbl,$col1,$col2,$id,$val) {
global $db;
$options="";	
	$sql="SELECT $col1,$col2 FROM $tbl WHERE status=0 order by id ASC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){

	foreach($results as $val) {
		if($id==$val[$col1]) {
		$options.='<option value="'.$val[$col1].'" selected>'.$val[$col2].'</option>';
		} else {
		$options.='<option value="'.$val[$col1].'">'.$val[$col2].'</option>';
		}
	}
	return $options;
	}	else {
	return $options;
	}
}

function fnDropDownListDType($tbl,$col1,$col2,$col3,$id,$val) {
	global $db;
	$options="";	
	$sql="SELECT $col1,$col2 FROM $tbl WHERE status=0 order by $col3 ASC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){
		foreach($results as $val) {
			if($id==$val[$col1]) {
				$options.='<option value="'.$val[$col1].'" selected>'.$val[$col2].'</option>';
			} else {
				$options.='<option value="'.$val[$col1].'">'.$val[$col2].'</option>';
			}
		}
		return $options;
	}	else {
		return $options;
	}
}

function fnDropDownListC($tbl,$col1,$col2,$id,$val1) {
global $db;
$options="";	
	$sql="SELECT $col1,$col2 FROM $tbl WHERE status=0 order by id ASC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){

	foreach($results as $val) {
		if(strtolower($id)==strtolower($val[$col1])) {
		//if(strcmp(strtolower($id),strtolower($val[$col1]))) {
		$options.='<option value="'.$val[$col1].'" selected>'.$val[$col2].'</option>';
		} else {
		$options.='<option value="'.$val[$col1].'">'.$val[$col2].'</option>';
		}
	}
	return $options;
	}	else {
	return $options;
	}
}

function fnMDropDownList($tbl,$col1,$col2,$id,$val) {
global $db;
$stlo=array();
if(isset($id) && $id!="") {
$stlo=explode(",",$id);
}
$options="";	
	$sql="SELECT $col1,$col2 FROM $tbl WHERE $val order by id ASC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){

	foreach($results as $val) {
		if(in_array($val[$col1],$stlo)) {
		$options.='<option value="'.$val[$col1].'" selected>'.$val[$col2].'</option>';
		} else {
		$options.='<option value="'.$val[$col1].'">'.$val[$col2].'</option>';
		}
	}
	return $options;
	}	else {
	return $options;
	}
}

function fnMDropDownListST($tbl,$col1,$col2,$id,$val,$idval) {
global $db;
$stlo=array();
if(isset($id) && $id!="") {
$stlo=explode(",",$id);
}
$options="";	
	$sql="SELECT $col1,$col2 FROM $tbl WHERE $val order by id ASC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){

	foreach($results as $val) {
		if($idval==$val[$col1]) {
		$options.='<option value="'.$val[$col1].'" selected>'.$val[$col2].'</option>';
		} else {
		$options.='<option value="'.$val[$col1].'">'.$val[$col2].'</option>';
		}
	}
	return $options;
	}	else {
	return $options;
	}
}

function fnDropDownListProOptions($tbl,$col1,$col2,$col3,$id,$val) {
	global $db;
	$options="";	
	$sql="SELECT $col1,$col2,$col3 FROM $tbl WHERE status=0 order by id ASC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){

	foreach($results as $val) {
		if($id==$val[$col1]) {
		$options.='<option value="'.$val[$col1].'" selected>'.$val[$col2].' - '.$val[$col3].'</option>';
		} else {
		$options.='<option value="'.$val[$col1].'">'.$val[$col2].' - '.$val[$col3].'</option>';
		}
	}
	return $options;
	}	else {
	return $options;
	}
}

function fnDropDownListProOptionsdType($tbl,$col1,$col2,$col3,$id,$val) {
	global $db;
	$options="";	
	$sql="SELECT $col1,$col2,$col3 FROM $tbl WHERE status=0 order by dType ASC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){

	foreach($results as $val) {
		if($id==$val[$col1]) {
		$options.='<option itemprcie="'.$val[$col3].'" value="'.$val[$col1].'" selected>'.$val[$col2].'</option>';
		} else {
		$options.='<option itemprcie="'.$val[$col3].'" value="'.$val[$col1].'">'.$val[$col2].'</option>';
		}
	}
	return $options;
	}	else {
	return $options;
	}
}


function fnDropDownListProOptionswhere($tbl,$col1,$col2,$where,$id,$orderby) {
global $db;
$options="";	

if($tbl=="bb_project") {
$curDate=strtotime(date('Y-m-d H:i:s'));
	$sql="SELECT $col1,$col2,proSchType,proSchStDate,proSchStTime,proSchStDtTime FROM $tbl $where $orderby";
} else {
	$sql="SELECT $col1,$col2 FROM $tbl $where $orderby";
}	
	/*echo $sql;
	die();*/
	$results =  $db->rawQuery($sql);
	if(count($results)>0){

	foreach($results as $val) {
if($tbl=="bb_project") {	
$publishDate=strtotime(date('Y-m-d H:i:s',strtotime($val['proSchStDate']." ".$val['proSchStTime'])));

if($curDate>=$publishDate) {
		if($id==$val[$col1]) {
		$options.='<option value="'.checkSpace($val[$col1]).'" selected>'.checkSpace($val[$col2]).'</option>';
		} else {
		$options.='<option value="'.checkSpace($val[$col1]).'">'.checkSpace($val[$col2]).'</option>';
		}
}	
} else {
			if($id==$val[$col1]) {
			$options.='<option value="'.checkSpace($val[$col1]).'" selected>'.checkSpace($val[$col2]).'</option>';
			} else {
			$options.='<option value="'.checkSpace($val[$col1]).'">'.checkSpace($val[$col2]).'</option>';
			}
}
}
	
	return $options;
	}	else {
	return $options;
	}
	
	
}



function getStudioName($ids) {
global $db;
$options="";	
//and stStatus=4
	$sql="SELECT stName FROM bb_studiolocation WHERE id IN($ids)  order by id ASC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){
	foreach($results as $kv=>$val) {
		$options.=$val['stName'].", ";
	}
	return $options;
	}	else {
	return $options;
	}
}

function getNamebyPara($tblname,$gcol,$col,$val){
	global $db;
	$taxinfo = $db->rawQueryOne("SELECT $gcol FROM $tblname WHERE $col = '".$val."'", array(''));
	return $taxinfo[$gcol];
}

function getStudioSug($locationId){
global $db;
$taxinfo = $db->rawQueryOne("SELECT slug FROM bb_studiolocation WHERE id = ? ", array($locationId));
return $taxinfo['slug'];
}

function getNamebyParadType($tblname,$gcol,$col,$val){
	global $db;
	$taxinfo = $db->rawQueryOne("SELECT $gcol FROM $tblname WHERE $col = '".$val."' order by dType ASC", array(''));
	return $taxinfo[$gcol];
}

function countTicketPOptions($tempid,$ptkid){
global $db;
$optinfo = $db->rawQueryOne("SELECT count(id) as totalTOpt FROM bb_evticketpackageoptions WHERE evTempId = '".$tempid."' and evTicketPackageId = '".$ptkid."' ", array(''));
return $optinfo['totalTOpt'];
}

function countTicketP($tempid,$evId,$studioID){
global $db;
$stwh="";
if(isset($_SESSION["urole"]) && $_SESSION["urole"]==3) {
if(isset($evId) && $evId!="") {
$stwh="  eventID=".$evId;
}
if(isset($studioID) && $studioID!="") {
$stwh.=" and studioId IN(".$studioID.")";
}
if(isset($tempid) && $tempid!="") {
$stwh.=" evtempID = '".$tempid."'";
$stwh.=" and eventID = 0 ";
}

} 
if(isset($_SESSION["urole"]) && ($_SESSION["urole"]==1 || $_SESSION["urole"]==2) ) {
		if(isset($_SESSION["studid"]) && $_SESSION["studid"]!="") {
				if(isset($evId) && $evId!="") {
				$stwh="  eventID=".$evId;
				if(isset($studioID) && $studioID!="") {
				$stwh.=" and studioId IN(".$studioID.")";
				}
				} else {
				$stwh=" evtempID = '".$tempid."'";
				}
				
		} else {
		$stwh=" evtempID = '".$tempid."'";
		}
}


$optinfo = $db->rawQueryOne("SELECT count(id) as totalTP FROM bb_evticketpackage WHERE $stwh ", array(''));
return $optinfo['totalTP'];
}

/*function countTicketPOptions($tempid,$ptkid){
global $db;
$optinfo = $db->rawQueryOne("SELECT count(id) as totalTOpt FROM bb_evticketpackageoptions WHERE evTempId = '".$tempid."' and evTicketPackageId = '".$ptkid."' ", array(''));
return $optinfo['totalTOpt'];
}*/



//check ticket paackage options exist or not
function checkTicketPackageOPt($tempid,$ptkid,$optid){
global $db;

$optinfo = $db->rawQueryOne("SELECT id FROM bb_evticketpackageoptions WHERE evtempID = '".$tempid."' and evTicketPackageId = '".$ptkid."' and id = '".$optid."' ", array(''));
if(isset($optinfo['id'])) {
return true;
} else {
return false;
}
}

//check ticket paackage exist or not
function checkTicketPackage($tempid,$ptkid,$evntid){
global $db;
$stwh="";
if(isset($_SESSION["urole"]) && $_SESSION["urole"]==3) {
if(isset($evntid) && $evntid!="") {
$stwh=" and eventID=".$evntid;
}
}
if(isset($_SESSION["urole"]) && ($_SESSION["urole"]==1 || $_SESSION["urole"]==2 ) ) {
		if(isset($_SESSION["studid"]) && $_SESSION["studid"]!="") {
		
		$stwh=" and eventID = '".$evntid."'  ";
		} else {
		$stwh=" and evtempID = '".$tempid."'  ";
		}

}
/*echo "SELECT id FROM bb_evticketpackage WHERE  id = '".$ptkid."' $stwh ";
die();*/
$optinfo = $db->rawQueryOne("SELECT id FROM bb_evticketpackage WHERE  id = '".$ptkid."' $stwh ", array(''));
if(isset($optinfo['id'])) {
return true;
} else {
return false;
}

}

function checkUserEvent($evid,$studid){
global $db;

$optinfo = $db->rawQueryOne("SELECT id FROM bb_event WHERE evLocationId IN(".$studid.") and id = '".$evid."' ", array(''));
 
if(isset($optinfo['id'])) {
return true;
} else {
return false;
}

}

function fnDbDate($dt) {
$dte=explode("/",$dt);
return $dte[2]."-".$dte[0]."-".$dte[1];
}

function fnDisplayDate($dt) {
$dte=explode("-",$dt);
return $dte[1]."/".$dte[2]."/".$dte[0];
}


function getEvTemptypeCol($evid){
global $db;
$optinfo = $db->rawQueryOne("SELECT id,evTempName,evType,evCalColor,evCalLabel FROM bb_eventtemplates WHERE  id = '".$evid."' ", array(''));
return $optinfo;
}

function getUserStudio($uid,$studloc){
global $db;
/*echo "SELECT id,studioLocation FROM bb_users WHERE  id = '".$uid."' and studioLocation IN(".$studloc.") ";
die();*/

$uinfo = $db->rawQueryOne("SELECT id,studioLocation FROM bb_users WHERE  id = '".$uid."'  ", array(''));
$studiolocinfo=explode(",",$uinfo['studioLocation']);
if((isset($uinfo) && !empty($uinfo)) &&  in_array($studloc,$studiolocinfo)) {
return true;
} else {
return $uinfo;
}

}

function countEvents($locationsId){
global $db;
$optinfo = $db->rawQueryOne("SELECT count(id) as totalEvent FROM bb_event WHERE  evLocationId = '".$locationsId."' and evStatus=0 and isDeleted=0 and isCancel=0 ", array(''));
return $optinfo;
}


function countPBookingEventTicket($eventId){
global $db;
$optinfo = $db->rawQueryOne("SELECT count(id) as totalP FROM bb_booking WHERE eventId = '".$eventId."' and status = 3", array(''));
if(isset($optinfo['totalP']) && $optinfo['totalP']!="") {
return $optinfo['totalP'];
} else {
return 0;
}
}

function countPBookingEventTicketListing($eventId,$locId){
global $db;
$optinfo = $db->rawQueryOne("SELECT count(id) as totalP FROM bb_booking WHERE eventId = '".$eventId."' and eventLocationId='".$locId."' and status = 3", array(''));
if(isset($optinfo['totalP']) && $optinfo['totalP']!="") {
return $optinfo['totalP'];
} else {
return 0;
}
}

function getStates($country_id){
	global $db;	
	$sql="SELECT * FROM `bb_states` WHERE country_id=$country_id and status=1 order by state_name ASC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){
		return $results;
	}	else {
		return false;
	}
}

/* *************************************************
***************************************************
Start Code By Vipul For Project Section at 31may2018 
***************************************************
***************************************************
***************************************************
*/

/*Get check box List*/
function fnCheckBoxList($tbl,$col1,$col2,$id,$val) {
	global $db;
	$options="";	
	$sql="SELECT $col1,$col2 FROM $tbl WHERE status=0 order by id ASC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){
		foreach($results as $val) {
			if($id==$val[$col1]) {
				$options .= '<div class="input-row"><input type="checkbox" checked value="'.$val[$col1].'" name="proFpPrintList[]" class="proFpPrintList" id="proFpPrintList'.$val[$col1].'" style="margin-right:5px;"/>'.$val[$col2].'</div>';
			} else {
				$options .= '<div class="input-row"><input type="checkbox" value="'.$val[$col1].'" name="proFpPrintList[]" class="proFpPrintList" id="proFpPrintList'.$val[$col1].'" style="margin-right:5px;"/>'.$val[$col2].'</div>';
			}
		}
		return $options;
	}else {
		return $options;
	}
}

function fnDropDownListForPrimaryCat($tbl,$col1,$col2,$id,$val) {
	//echo $id.",".$val;
	global $db;
	$options = "";	
	$sql = "SELECT $col1,$col2 FROM $tbl WHERE status=0 order by id ASC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){
	foreach($results as $val) {
		//echo $val[$col2];
		if($id==$val[$col1]) {
		$options.='<option value="'.$val[$col1].'" selected>'.$val[$col2].'</option>';
		} else {
		$options.='<option value="'.$val[$col1].'">'.$val[$col2].'</option>';
		}
	}
	return $options;
	}	else {
	return $options;
	}
}

function getFieldvalueById($tbl,$col1,$id) {
	global $db;
	$options="";	
	$sql="SELECT $col1 FROM $tbl WHERE id=$id";
	$results =  $db->rawQueryOne($sql);
	return $results[$col1];
}

function fetchTblDataById($tbl,$col1,$col2,$id1,$id2) {
	global $db;
	$params = array('');
	$proPersresult = $db->rawQuery("SELECT * FROM  $tbl WHERE $col1='".$id1."' and $col2='".$id2."' ", $params);
	$proPersitems = (array)$proPersresult;
	return $proPersitems;
}


function fnDropDownList2ndSpecificationOptions($tbl,$col1,$col2,$col3,$id,$val1) {
	global $db;
	$options="";	
	$sql="SELECT $col1,$col2 FROM $tbl WHERE status=0 and $col3=$id order by id ASC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){
		foreach($results as $val) {
			if($val1==$val[$col1]) {
				$options.='<option value="'.$val[$col1].'" selected>'.$val[$col2].'</option>';
			} else {
				$options.='<option value="'.$val[$col1].'">'.$val[$col2].'</option>';
			}
		}
		return $options;
	}	else { 
		return $options;
	}
}


function fnDropDownList2ndSpecificationOptionsdType($tbl,$col1,$col2,$col3,$col4,$id,$val1) {
	global $db;
	$options="";	
	$sql="SELECT $col1,$col2 FROM $tbl WHERE status=0 and $col3=$id order by $col4 ASC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){
		foreach($results as $val) {
			if($val1==$val[$col1]) {
				$options.='<option value="'.$val[$col1].'" selected>'.$val[$col2].'</option>';
			} else {
				$options.='<option value="'.$val[$col1].'">'.$val[$col2].'</option>';
			}
		}
		return $options;
	}	else { 
		return $options;
	}
}

function fnDropDownListForEventList($tbl,$col1,$col2,$col3,$col4,$col5,$id,$val,$type) {
	global $db;
	$options="";	
	//echo $sql1 = "SELECT $col1,$col2,$col3,$col4 FROM $tbl WHERE evStatus=0 and isDeleted=0 and  evLocationId IN(".$col5.") order by $col3 ASC";
	$sql = "SELECT $col1,$col2,$col3,$col4,
			TIMESTAMP(`evDate`, STR_TO_DATE(`evStTime`, '%h:%i %p')) as  sortTime
			FROM $tbl 
			WHERE evStatus=0 and isDeleted=0 and  
			evLocationId IN(".$col5.") 
			order by sortTime asc";
	
	$results =  $db->rawQuery($sql);
	if(count($results)>0){
		foreach($results as $val) {
			$day = date("D",strtotime($val['evDate']));
			$evStTimeFormated = $val['evStTime'];
			if(!empty($evStTimeFormated)){
				$evStTimeFormatedArr = explode(" ",$evStTimeFormated);
				$evStTime1Arr = explode(":",$evStTimeFormatedArr[0]);
				
				if($evStTime1Arr[1] == "00" || $evStTime1Arr[1] == "0"){
					$evStTime1Str = $evStTime1Arr[0];
					$evStTime1StrForAMPM =  str_replace("m","",$evStTimeFormatedArr[1]);
					$eventFGinalFormatedStr = $evStTime1Str.$evStTime1StrForAMPM."&nbsp;&nbsp;";
				} else{
					$evStTime1Str = $evStTimeFormatedArr[0];
					$evStTime1StrForAMPM =  str_replace("m","",$evStTimeFormatedArr[1]);
					$eventFGinalFormatedStr = $evStTime1Str.$evStTime1StrForAMPM;
				}
			} 
			else {
				$eventFGinalFormatedStr = "";
			}
		
			$evDateFormated = date("m/d/Y",strtotime($val['evDate']));
			$evDateTimeFormated =  $day."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$eventFGinalFormatedStr."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$evDateFormated."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$val['evTitle'];
			if($id==$val[$col1]) {
				$options.='<option value="'.$val[$col1].'" selected>'.$evDateTimeFormated.'</option>';
			} else {
				$options.='<option value="'.$val[$col1].'">'.$evDateTimeFormated.'</option>';
			}
		}
		//echo "<pre>options==";print_R($options);die;
		return $options;
	}	else {
		return $options;
	}
}

function getSelEventListAtPromoEditPage($eventArr,$stId) {
	if(!empty($eventArr)){
		if(count($eventArr)>1){
			$eventArr1 = implode(",",$eventArr);
		} else{
			$eventArr1 = $eventArr[0];
		}
	}
	global $db;
	$sql = "SELECT id,evTitle,evDate,evStTime, 
		TIMESTAMP(`evDate`, STR_TO_DATE(`evStTime`, '%h:%i %p')) as sortTime 
		FROM bb_event 
		WHERE evStatus=0 and isDeleted=0 and evLocationId IN(".$stId.") 
		and id IN(".$eventArr1.") order by sortTime asc ";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){
		$repo = array();
		foreach($results as $key=>$val){
			$repo[] = $val['id'];
		}
		//echo "<pre>";print_r($results);
		//echo "<pre>repo";print_r($repo);
		return $repo;
	}	else {
		return false;
	}
}


/* Get counter of need of attention type booking in left side menu */
function CntOfNeedAttentionBooking() {
	global $db;
	$params = array('');
	if(isset($_SESSION["urole"]) && $_SESSION["urole"]==3) {
		$sql = "SELECT * FROM `bb_booking` where eventLocationId IN(".$_SESSION["studioIds"].") and isDeleted=0 and status=3 order by id desc";
	} else {
		$sql = "SELECT * FROM `bb_booking` where eventLocationId IN(".$_SESSION["stidloc"].") and isDeleted=0 and status=3 order by id desc";
	}
	$bookingLists = $db->rawQuery($sql,$params);
	if(!empty($bookingLists)){
		$cntBooking = count($bookingLists);
	} else {
		$cntBooking = '';
	}
	return $cntBooking;
}

/* Get project id from booking id from 'bb_booktcktinfo' tbl */
function getProjectIdFromTicketInfoTbl($bookingId,$bookTicketUniQueId) {
	global $db;
	$params = array('');
	$result = $db->rawQueryone("SELECT boProjectId FROM bb_booktcktinfo WHERE bookingId='".$bookingId."' and id='".$bookTicketUniQueId."' ", $params);
	$ritems = (array)$result;
	if(!empty($ritems)) {
		$boProjectId = $ritems['boProjectId'];
	} else {
		$boProjectId = '';
	}
	return $boProjectId;
}

/* *************************************************
***************************************************
End code By Vipul For Project Section at 31may2018 
***************************************************
***************************************************
***************************************************
*/

/* *************************************************
start code By Vipul For Prep report at event detail page at 5july2018 
***************************************************
*/
//Get studio location city wrt event id
function getStudioCityWRTEventId($eventId){
	global $db;
	$params = array('');
	$sql = "SELECT st.stName 
				FROM `bb_studiolocation` as st
				inner join `bb_event` as ev
				on ev.studioId = st.id
				where ev.id='".$eventId."'";
	$result2 = $db->rawQueryOne($sql, $params);
	$ritems2 = (array)$result2;
	if(isset($ritems2) && !empty($ritems2)) {
		$stCity = $ritems2['stName'];
	} else {
		$stCity = 'NA';
	}
	return $stCity;
}

//Get event detail wrt event id
function getEventDetailWRTEventId($eventId){
	global $db;
	$params = array('');
	$result2 = $db->rawQueryOne("SELECT bo.id,bo.evTitle,bo.evDate,bo.evStTime,bo.evNoOfTickets,
			bo.evBookTicket FROM `bb_event` as bo where bo.id='".$eventId."'", $params);
	$ritems2 = (array)$result2;
	if(isset($ritems2) && !empty($ritems2)) {
		return $ritems2;
	} else {
		return array();
	}
}

//Get ticket names wrt booking id
function getTicketNameWRTBookingId($ticketid){
	global $db;
	$params = array('');
	$result2 = $db->rawQueryOne("SELECT boTicketName FROM bb_booktcktinfo WHERE id='".$ticketid."'", $params);
	$ritems2 = (array)$result2;
	if(isset($ritems2) && !empty($ritems2)) {
		return $ritems2['boTicketName'];
	} else {
		$ticketname = 'NA';
		return $ticketname;
	}
}

//Get project personalization info wrt booking id,boooking ticket id
function getTicketProjectPersonalizeInfoWRTBookingId($bookingId,$bookingTicketId){
	global $db;
	$params = array('');
	$sql= "SELECT boPersOptName,boPersOptVal,bookingTicketId FROM bb_booktcktpersinfo WHERE bookingId='".$bookingId."' and bookingTicketId='".$bookingTicketId."' ";
	$result = $db->rawQuery($sql, $params);
	$ritems = (array)$result;
	if(isset($ritems) && !empty($ritems)) {
		$QueryString = array();
		foreach ($ritems as $key => $val){
			$QueryString[] = $val['boPersOptName'].': '.$val['boPersOptVal'];
		}
	}
	return $QueryString;
}


//Get project specification option  type
function getProjectSpecType($optionType){
	global $db;
	$params = array('');
	$sql= "SELECT opName FROM bb_prospcoptions WHERE id='".$optionType."'";
	$result = $db->rawQueryone($sql, $params);
	$ritems = (array)$result;
	if(isset($ritems) && !empty($ritems)) {
		return $ritems['opName'];
	}
}

//Get project specification size
function getProjectSpecSize($sizeId){
	global $db;
	$params = array('');
	$sql= "SELECT sizename FROM bb_prospcsizemeasure WHERE id='".$sizeId."' ";
	$result = $db->rawQueryone($sql, $params);
	$ritems = (array)$result;
	if(isset($ritems) && !empty($ritems)) {
		return $ritems['sizename'];
	}
}

//Get project specification info wrt project id,
function getprojectSpecInfoWRTBookingId($projectId){
	global $db;
	$params = array('');
	$sql= "SELECT specOptionVal,specType,specTypeVal FROM bb_prospecifications WHERE proID='".$projectId."'";
	$result = $db->rawQuery($sql, $params);
	$ritems = (array)$result;
	if(isset($ritems) && !empty($ritems)) {
		$QueryString = array();
		foreach ($ritems as $key => $val){
			if($val['specOptionVal'] == 1 || $val['specOptionVal'] == 2){
				$QueryString['supplies'][] = getProjectSpecSize($val['specType']);
			} else {
				$QueryString['supplies'][] = getProjectSpecType($val['specOptionVal']);
			}
			$QueryString['qty'][] = $val['specTypeVal'];
		}
	}
	return $QueryString;
}


function getprojectSpecInfoForWoodSummary($projectId){
	global $db;
	$params = array('');
	$sql= "SELECT specOptionVal,specType,specTypeVal FROM bb_prospecifications WHERE proID='".$projectId."'";
	$result = $db->rawQuery($sql, $params);
	$ritems = (array)$result;
	if(isset($ritems) && !empty($ritems)) {
		$QueryString = array();
		$i=0;
		foreach ($ritems as $key => $val){
			if($val['specOptionVal'] == 1 || $val['specOptionVal'] == 2){
				$QueryString['supplies'][] = getProjectSpecSize($val['specType']);
			} else {
				$QueryString['supplies'][] = getProjectSpecType($val['specOptionVal']);
			}
			$QueryString['qty'][] = $val['specTypeVal'];
		}
	}
	return $QueryString;
}

//Get wood plan summary
function getWoodPlanSummaryWRTEventId($eventId){
	//echo "eventId=".$eventId;
	global $db;
	$params = array('');
	$sql = "SELECT botckt.boProjectId FROM `bb_booking` as bo 
		    right join `bb_booktcktinfo` as botckt
		    on bo.id = botckt.bookingId
		    where bo.eventId='".$eventId."' and bo.isDeleted=0 and (bo.status=0 OR bo.status=3)";
	$result = $db->rawQuery($sql,$params);
	$ritems = (array)$result;
	//echo "<pre>ritems=";print_r($ritems);
	if(isset($ritems) && !empty($ritems)) {
		$QueryString1 = array();
		$QueryStringFinal = array();
		foreach ($ritems as $key => $val){
			//echo "boProjectId=".$val['boProjectId'];
			$rr = getprojectSpecInfoForWoodSummary($val['boProjectId']);
			//echo "<pre>rr=";print_r($rr);
			//echo "cnt==".count($rr['supplies']);
			for($j=0;$j<=count($rr['supplies']);$j++){
				if($rr['supplies'][$j] != ''){
					$QueryString1['event1'][]  = $rr['supplies'][$j];
					$QueryString1['budget1'][]  = $rr['qty'][$j];
				}
			}
		}
		//echo "<pre>QueryString1=";print_r($QueryString1);
		for($i=0;$i<count($QueryString1['event1']); $i++){
			$QueryStringFinal[$i]['event'] = $QueryString1['event1'][$i];
			$QueryStringFinal[$i]['budget'] = $QueryString1['budget1'][$i];
		}
		//echo "<pre>QueryStringFinal=";print_r($QueryStringFinal);die;
		$sum = array_reduce($QueryStringFinal, function ($a, $b) {
			isset($a[$b['event']]) ? $a[$b['event']]['budget'] += $b['budget'] : $a[$b['event']] = $b;  
			return $a;
		});
		return array_values($sum);
	}
}

//check project is booked or not in any booking  from 'bb_booktcktinfo' tbl
function isProjectBookedInAnyTicket($projectId){
	global $db;
	$params = array('');
	$sql = "SELECT botckt.bookingId,botckt.boProjectId,bo.*
		FROM  bb_booktcktinfo as botckt
		right join `bb_booking` as bo
		on botckt.bookingId = bo.id
		WHERE botckt.boProjectId='".$projectId."' and bo.isDeleted=0 and (bo.status=0 || bo.status=3 || bo.status=4)
		group by  botckt.bookingId";
	$result = $db->rawQuery($sql,$params);
	$ritems = (array)$result;
	if(!empty($ritems) && count($ritems)>=1) {
		$isProjectBooked = true;
	} else {
		$isProjectBooked = false;
	}
	return $isProjectBooked;
}

function fetchTblDataByIdForProjectSection($tbl,$col1,$id1) {
 global $db;
 $params = array('');
 $proPersresult = $db->rawQuery("SELECT * FROM  $tbl WHERE $col1='".$id1."' ", $params);
 $proPersitems = (array)$proPersresult;
 return $proPersitems;
}

//count no of records
function fetchTblDataByIdForProjectSection_count($tbl,$col1,$id1){
 global $db;
 $params = array('');
 $proPersresult = $db->rawQuery("SELECT * FROM  $tbl WHERE $col1='".$id1."' ", $params);
 $proPersitems = (array)$proPersresult;
 return $proPersitems;
}
//end of record


//Function is used to get associated event
function getAssociatedEventListWRTUser($eventId,$studioId){
	//echo $eventId.",".$studioId;
	global $db;
	$params = array('');
	$eventAssociatedArr = array();
	//echo $sql = "SELECT id FROM  bb_event WHERE id='".$eventId."' and evLocationId='".$studioId."' ";
	$sql = "SELECT id FROM  bb_event WHERE id='".$eventId."' and evLocationId IN(".$studioId.") ";
	$result = $db->rawQueryOne($sql,$params);
	$ritems = (array)$result;
	if(!empty($ritems)) {
		$eventAssociatedArr = $ritems['id'];
		return  $eventAssociatedArr;
	} else {
		return false;
	}
}

//Function is used to get promo studio location Id
function getStdLocationIdWRTPromoId($promoId){
	global $db;
	$params = array('');
	$sql = "SELECT studioId FROM  bb_promocode WHERE id='".$promoId."'";
	$result = $db->rawQueryOne($sql,$params);
	$ritems = (array)$result;
	if(!empty($ritems)) {
		return $ritems['studioId'];
	} else {
		return false;
	}
}

//Function is used to get locationId wrt to eventId
function getEventLocIdWRTEventId($eventId){
	//echo $eventId;
	global $db;
	$params = array('');
	$sql = "SELECT evLocationId FROM bb_event WHERE id='".$eventId."'"; //die;
	$result = $db->rawQueryOne($sql,$params);
	$ritems = (array)$result;
	if(!empty($ritems)) {
		return $ritems['evLocationId'];
	} else {
		return false;
	}
}

//Function is used to resizing image
function resizeImage($resourceType,$image_width,$image_height,$resizeWidth,$resizeHeight) {
    $imageLayer = imagecreatetruecolor($resizeWidth,$resizeHeight);
    imagecopyresampled($imageLayer,$resourceType,0,0,0,0,$resizeWidth,$resizeHeight, $image_width,$image_height);
    return $imageLayer;
}

//Function is used to decrase seats on cancel,reject,delete booking
function decreaseSeatsFromBookedEvent($bookingId){
	global $db;
	$params = array('');
	//Get EventId, no. of booked ticket From bookingId
	$result = $db->rawQueryOne("SELECT eventId,totalTickets FROM bb_booking WHERE id='".$bookingId."' ", $params);
	$ritems = (array)$result;
	if(!empty($ritems)) {
		$eventId = $ritems['eventId'];
		//Get Total Ticket Booked In Booking
		$totalBookedTicketInAnyBooking = abs($ritems['totalTickets']);
		//echo $bookingId."====".$eventId."=====".$totalBookedTicketInAnyBooking;
		if($eventId !=''){
			$result1 = $db->rawQueryOne("SELECT evBookTicket FROM bb_event WHERE id='".$eventId."' ", $params);
			$ritems1 = (array)$result1;
			//echo "<prE>ritems1===";print_r($ritems1);
			if(!empty($ritems1)) {
				//Get Total Ticket Booked In Event
				$totalBookedTicketInAnyEvent = abs($ritems1['evBookTicket']); 
				$remainingBookedTicketAfterDeletion = $totalBookedTicketInAnyEvent - $totalBookedTicketInAnyBooking;
				$remainingBookedTicketAfterDeletion = abs($remainingBookedTicketAfterDeletion); 
				/*echo "totalBookedTicketInAnyEvent:".$totalBookedTicketInAnyEvent."<br/>";
				echo "totalBookedTicketInAnyBooking:".$totalBookedTicketInAnyBooking."<br/>";
				echo "remainingBookedTicketAfterDeletion:".$remainingBookedTicketAfterDeletion."<br/>";
				die;*/
				
				//Update Remaining booked ticket in event after cancel,reject,delete
				$data1 = array ( "evBookTicket"=>$remainingBookedTicketAfterDeletion);
				$db->where ('id',$eventId);
				$update1 = $db->update("bb_event",$data1);
				/*if($update1){
					echo "done".$db->getLastQuery();
				} else{
					echo "not done".$db->getLastError();
				}*/
			}
		}
	}
}

//Function is used to increase seats on cancel,reject,delete booking
function increaseSeatsFromBookedEvent($bookingId){
	global $db;
	$params = array('');
	//Get EventId, no. of booked ticket From bookingId
	$result = $db->rawQueryOne("SELECT eventId,totalTickets FROM bb_booking WHERE id='".$bookingId."' ", $params);
	$ritems = (array)$result;
	if(!empty($ritems)) {
		$eventId = $ritems['eventId'];
		//Get Total Ticket Booked In Booking
		$totalBookedTicketInAnyBooking = abs($ritems['totalTickets']);
		if($eventId !=''){
			$result1 = $db->rawQueryOne("SELECT evBookTicket FROM bb_event WHERE id='".$eventId."' ", $params);
			$ritems1 = (array)$result1;
			if(!empty($ritems1)) {
				//Get Total Ticket Booked In Event
				$totalBookedTicketInAnyEvent = abs($ritems1['evBookTicket']); 
				$updatedBookedTicketAfterDeletion = $totalBookedTicketInAnyEvent + $totalBookedTicketInAnyBooking;
				$updatedBookedTicketAfterDeletion = abs($updatedBookedTicketAfterDeletion); 
				//Update booked ticket in event after approve
				$data1 = array ( "evBookTicket"=>$updatedBookedTicketAfterDeletion);
				$db->where ('id',$eventId);
				$update1 = $db->update("bb_event",$data1);
			}
		}
	}
}

//Function is used to get specification Board size WRT SpecType
function getSpecBoardSizeWRTSpecType($specType){
	global $db;
	$params = array(''); 
	$sql = "SELECT sizename FROM bb_prospcsizemeasure WHERE id='".$specType."'";
	$result = $db->rawQueryOne($sql,$params);
	$ritems = (array)$result;
	//echo "<pre>ritems###==";print_r($ritems);
	if(!empty($ritems)) {
		return $ritems['sizename'];
	} else {
		return false;
	}
}

//Function is used to get specification Board size WRT projectId
function getSpecBoardSizeWRTProjId($projectId){
	global $db;
	$params = array(''); 
	$sql = "SELECT a1.proId,a1.specOptionVal,a1.specType
	FROM bb_prospecifications as a1
	LEFT JOIN bb_prospcsizemeasure a2 
	ON a1.specOptionVal = a2.id_type 
	WHERE a2.status=0 and a1.proId='".$projectId."' and a1.specOptionVal=1 group by a1.proId";
	
	$result = $db->rawQueryone($sql,$params);
	$ritems = (array)$result;
	//echo "<pre>ritems==";print_r($ritems);
	if(!empty($ritems)) {
		$size = getSpecBoardSizeWRTSpecType($ritems['specType']);
		return $size;
	} else {
		return false;
	}
}

//Function is used to get studio info
function getStudioInfoByStdId($studioId) {
	global $db;
	$params = array(''); 
	$sslug = $db->rawQueryOne ("SELECT * FROM `bb_studiolocation` where id='".$studioId."' ",$params);
	return $sslug;
}

//Function is used to get booking info
function getBookingInfo($bookingId) {
	global $db;
	$params = array(''); 
	$sslug = $db->rawQueryOne ("SELECT * FROM `bb_booking` where id='".$bookingId."' ",$params);
	return $sslug;
}

//Function is used to get booking info
function getBookingProjectInfo($bookingId) {
	global $db;
	$params = array(''); 
	$sql = "SELECT * FROM `bb_booktcktinfo` where bookingId='".$bookingId."' ";
	$sslug = $db->rawQuery($sql,$params);
	return $sslug;
}

//Function is used to get booked event info
function getBookedEventInfo($eventId) {
	global $db;
	$params = array(''); 
	$sql = "SELECT * FROM `bb_event` where id='".$eventId."' ";
	$sslug = $db->rawQueryOne($sql,$params);
	return $sslug;
}

//Function is used to get project info from event ticket packae project id 
function getProjInfoWRTTicketPackProId($projId) {
	global $db;
	$params = array(''); 
	$sql = "SELECT id,proName,proSku,proGalImg FROM `bb_project` where id='".$projId."' ";
	$sslug = $db->rawQueryOne($sql,$params);
	return $sslug;
}

function getProjPersonalizeList($projId) {
	global $db;
	$params = array(''); 
	$sql = "SELECT a1.id,a1.proOptionVal,a1.proLabel,a1.proHoverTxt,a1.proOptVal
		FROM `bb_propersoptions` as a1
		WHERE a1.proId='".$projId."' ";
	$sslug = $db->rawQuery($sql,$params);
	return $sslug;
}

function cntEventBookedSeats($evtId) {
	global $db;
	$params = array(''); 
	$sql = "SELECT evBookTicket FROM `bb_event` WHERE id='".$evtId."' ";
	$result = $db->rawQueryone($sql,$params);
	$ritems = (array)$result;
	if(!empty($ritems)) {
		return $ritems['evBookTicket'];
	} else {
		return 0;
	}
}

function updateDecreaseSeatsFromOldEvent($updTotSeatsInNewEvent1,$oldEventId) {
	global $db;
	$data85 = array ('evBookTicket' => $updTotSeatsInNewEvent1);	
	$db->where ('id', $oldEventId);
	$db->update("bb_event",$data85);
}

function updateIncreaseSeatsInNewEvent($updTotSeats,$newEventId) {
	global $db;
	$data75 = array ('evBookTicket' => $updTotSeats);	
	$db->where ('id', $newEventId);
	$db->update("bb_event",$data75);
}

//Function is used to get totalticket from bb_booking tbl
function getTotalTicketWRTBookingId($bookingId) {
	global $db;
	$params1 = array('');
	$result78 = $db->rawQueryone("SELECT totalTickets FROM bb_booking WHERE id='".$bookingId."' ", $params1);
	$ritems78 = (array)$result78;
	if(!empty($ritems78)) {
		$totalSeatsAsPerOldEventID = $ritems78['totalTickets'];
	} else{
		$totalSeatsAsPerOldEventID = 0;
	}
	return $totalSeatsAsPerOldEventID;
}

//Function is used to get booking billing address,city,state from bb_booking tbl
function getBookingTransactioninfo($bookingId) {
	global $db;
	$params = array(''); 
	$sql = "SELECT a1.id,a1.caddress,a1.ccity,a1.cstate,a1.ccountry,a1.czip
		FROM `bb_transactiondetails` as a1
		WHERE a1.bookingId='".$bookingId."' "; 
	$sslug = $db->rawQueryone($sql,$params);
	return $sslug;
}


//Get event info wrt event id
function getEventinfoWRTEvntId($eventId){
	global $db;
	$params = array('');
	$result2 = $db->rawQueryone("SELECT bo.id,bo.evTitle,bo.evDate,bo.evStTime,bo.evNoOfTickets,
			bo.evBookTicket FROM `bb_event` as bo where bo.id='".$eventId."'", $params);
	$ritems2 = (array)$result2;
	if(isset($ritems2) && !empty($ritems2)) {
		$result = array();
		$evTitle = html_entity_decode($ritems2['evTitle'], ENT_QUOTES, 'UTF-8');
					
		//$result['Event Name'] = $ritems2['evTitle'];
		$result['Event Name'] = $evTitle;
		
		$result['Event Date'] = date('l, n-j-Y',strtotime($ritems2['evDate'])); 
		$result['Event Start Time'] = $ritems2['evStTime']; 
		$result['Total Seats'] = $ritems2['evBookTicket'];
		$result['Total Projects'] = $ritems2['evBookTicket'];
		return $result;
	} else {
		return array();
	}
}

//Get booking info wrt event id
function getBookinginfoWRTEvntId($eventId){
	global $db;
	$params = array('');
	$sql = "SELECT bo.boAccommodationsNotes,bo.cOrderNo,bo.id as bookingIdVal,bo.eventLocationId,bo.boProjects,bo.boFullName,
		bo.boPhone,bo.boEmail,bo.bookingCreationDate,bo.totalTickets,
		botckt.boTicketProjectName,botckt.boProjectId,botckt.id as bookingTicketId,botckt.boTicketProjectImage,botckt.boTicketSitBy
		FROM `bb_booking` as bo 
		right join `bb_booktcktinfo` as botckt
		on bo.id = botckt.bookingId
		where bo.eventId='".$eventId."' and bo.isDeleted=0 and (bo.status=0 OR bo.status=3)";
		
	/*$sql = "SELECT bo.cOrderNo,bo.id as bookingIdVal,bo.eventLocationId,bo.boProjects,bo.boFullName,
		bo.boPhone,bo.boEmail,bo.bookingCreationDate,bo.totalTickets,
		botckt.boTicketProjectName,botckt.boProjectId,botckt.id as bookingTicketId,botckt.boTicketProjectImage,botckt.boTicketSitBy
		FROM `bb_booking` as bo 
		right join `bb_booktcktinfo` as botckt
		on bo.id = botckt.bookingId
		where bo.eventId='".$eventId."' and (bo.status=0 OR bo.status=3)";*/
		
	$results =  $db->rawQuery($sql,$params);
	if(isset($results) && !empty($results)) {
		//$repo = array();
		//echo "<pre>results=";print_R($results); die;
		$numItems = count($results);
		$kk=2;
		$target_array1 = array();
		for($zz=0;$zz<$numItems;$zz++) {
			if($zz==0){
				$results[$zz]['seatNo'] = 1;
			}
			if($zz>0){
				$currentIterationBookingId = $results[$zz]['cOrderNo']; //booking id of current iteration
				$oldIterationBookingId = $results[$zz-1]['cOrderNo']; //booking id of last iteration 
				if($currentIterationBookingId == $oldIterationBookingId ){
					$target_array1[] = $zz;
					$results[$zz]['seatNo'] = $kk;
					$kk++;
				} else {
					$results[$zz]['seatNo'] = 1;
					$kk=2;
				} 
			}
		}
		foreach($target_array1 as $key1=>$val1){
			$results[$val1]['cOrderNo'] = '';
			$results[$val1]['boPhone'] = '';
			$results[$val1]['boEmail'] = '';
			$results[$val1]['bookingCreationDate'] = '';	
		}
		//echo "<pre>results=";print_R($results); die;
		/*foreach($results as $key=>$val){
			$repo[$key]['Customer'] =  $val['boFullName'];
			$repo[$key]['Contact'] =  $val['boPhone'];
			$repo[$key]['Contact'] =  $val['boPhone'];
		}*/
		
		return $results;
	} else {
		return array();
	}
}

function ftechPromoDetails($ccode){
	global $db;
	$cRev = $db->rawQueryOne("select * from bb_promocode where promoCode='".$ccode."' order by id ", array(''));
	if(count($cRev)>0){
		return $cRev;
	}
}

//Get project names realted with booking
function getTicketProjectName($bookingid){
	global $db;
	$params = array('');
	$result2 = $db->rawQuery("SELECT boTicketProjectName FROM bb_booktcktinfo WHERE bookingId='".$bookingid."'", $params);
	$ritems2 = (array)$result2;
	if(isset($ritems2) && !empty($ritems2)) {
		$boTicketProjectNameArr = array();
		foreach($ritems2 as $key2=>$val2){
			$boTicketProjectNameArr[] = $val2['boTicketProjectName'];
		}
		if(!empty($boTicketProjectNameArr) ){
			if(count($boTicketProjectNameArr)>1){
				$boTicketProjectNameStr = implode(",",$boTicketProjectNameArr);
			} else {
				$boTicketProjectNameStr = implode(",",$boTicketProjectNameArr);
				$boTicketProjectNameStr = rtrim($boTicketProjectNameStr,",");
			}
		}
	} else {
		$boTicketProjectNameArr = array();
		$boTicketProjectNameStr = "NA";
	}
	return $boTicketProjectNameStr;
}

//Get project personalization info realted with booking
function getTicketProjectPersonalizeInfo($bookingid){
	global $db;
	$params = array('');
	$result3 = $db->rawQuery("SELECT boPersOptName,boPersOptVal,bookingTicketId FROM bb_booktcktpersinfo WHERE bookingId='".$bookingid."'", $params);
	$ritems3 = (array)$result3;
	if(isset($ritems3) && !empty($ritems3)) {
		$boTicketPersOptionArr = array();
		foreach($ritems3 as $key3=>$val3){
			$boTicketPersOptionArr[$val3['bookingTicketId']][$val3['boPersOptName']] = $val3['boPersOptVal'];
			$boTicketPersOptionArr[$val3['bookingTicketId']]['ticketname'] = getTicketName($val3['bookingTicketId']);
		}
		$QueryString = '';
		foreach ($boTicketPersOptionArr as $Key => $Value){
			//$QueryString .= 'Ticket'.$Key.'=>';
			$QueryString .= $Value['ticketname'].'=>';
			foreach ($Value as $Key1 => $Value1){
				if($Key1 != 'ticketname'){
					$QueryString .= $Key1 . '=' . $Value1.',';
				}
			}
			$QueryString = rtrim($QueryString,',');
			$QueryString .= '<br/>';
		}
	} else {
		$boTicketPersOptionArr = array();
		$QueryString = "NA";
	}
	return $QueryString;
}

//Get booking payment gateway
function getPaymentGateway($bookingid){
	global $db;
	$params = array('');
	$result5 = $db->rawQueryone("SELECT ptype FROM bb_transactiondetails WHERE bookingId='".$bookingid."'", $params);
	$ritems5 = (array)$result5;
	if(isset($ritems5) && !empty($ritems5)) {
		$ptype = $ritems5['ptype'];
		if($ptype == '0') {
			$paymentgateway = "Authorize .net";
		} else if($ptype == '1') {
			$paymentgateway = "Gift Card";
		} else if($ptype == '2') {
			$paymentgateway = "Offline";
		} else if($ptype == '3') {
			$paymentgateway = "Paypal";
		} else if($ptype == '4') {
			$paymentgateway = "None";
		}
	} else {
		$paymentgateway = "NA";
	}
	return $paymentgateway;
}
//Get ticket names realted with booking
function getTicketName($ticketid){
	global $db;
	$params = array('');
	$result2 = $db->rawQueryOne("SELECT boTicketName FROM bb_booktcktinfo WHERE id='".$ticketid."'", $params);
	$ritems2 = (array)$result2;
	if(isset($ritems2) && !empty($ritems2)) {
		return $ritems2['boTicketName'];
	} else {
		$ticketname = 'NA';
		return $ticketname;
	}
}

/* strpos that takes an array of values to match against a string
 * note the stupid argument order (to match strpos)
 */
function strpos_arr_check($haystack, $needle) {
    $accommodList = preg_split('/\r\n|\r|\n/', $haystack);
	//echo "<pre>accommodList==";print_r($accommodList);
	
	$accommodList_str_separated = implode("!@#$,", $accommodList);
	//echo "<pre>accommodList_str_separated==";print_r($accommodList_str_separated);
	//$match = 'zip:';
	if(stripos($accommodList_str_separated, $needle) !== false) {
		$used1 = explode($needle,$accommodList_str_separated);
		//echo "<pre>used1==";print_r($used1);
		$used11 = explode('!@#$',$used1[1]);
		//echo "<pre>used11==";print_r($used11);
		$czip = $used11[0];
	} else  {
		$czip = '';
	}	
	return $czip;	
}

//Get Booking info wrt event location id
function getBookinginfoWRTBookingId($eventLocId,$bookingType){
	//echo $eventLocId.",".$bookingType;die;
	$stwhre = "";
	if(isset($bookingType) && $bookingType =="needatten") { 
		$orderby =" order by id desc";
		$stwhre.= "  and isDeleted=0 and status=3 ";
	}
	else if(isset($bookingType) && $bookingType =="future") {
		$orderby=" order by bookingCreationDate desc";
		$stwhre.= "  and isDeleted=0 and boEventDate >= CURDATE() ";
	}
	else if(isset($bookingType) && $bookingType =="today") { 
		$orderby=" order by bookingCreationDate desc";
		$stwhre.= "  and isDeleted=0 and boEventDate = CURDATE() ";
	}
	else if(isset($bookingType) && $bookingType =="tomorrow") { 
		$orderby=" order by bookingCreationDate desc";
		$stwhre.= "  and isDeleted=0 and date(boEventDate) = date_add(CURDATE(), interval 1 day) "; 
	}
	else if(isset($bookingType) && $bookingType =="past") { 
		$orderby=" order by bookingCreationDate desc";
		$stwhre.= "  and isDeleted=0 and boEventDate < CURDATE() ";
	}
	else if(isset($bookingType) && $bookingType =="trash") { 
		$orderby=" order by id desc";
		$stwhre.= "  and isDeleted=1";
	}
	else {
		$orderby =" order by id desc";
		$stwhre.= "  and isDeleted=0 ";
	} 
	 
	global $db;
	$params = array('');
	
	$sql = "SELECT * FROM `bb_booking` where eventLocationId IN(".$eventLocId.") $stwhre $orderby";
	$result2 = $db->rawQuery($sql, $params);
	$ritems2 = (array)$result2;
	if(isset($ritems2) && !empty($ritems2)) {
		//echo "<pre>ritems2===";print_r($ritems2);die;
		$result = array();
		foreach($ritems2 as $key=>$val){
			$bookingStatus = getBookingStatus($val['id']);
			if(isset($val['bookingCreationDate']) && $val['bookingCreationDate']!=""){ 
				$bookingCreationDate =  date("m/d/Y",strtotime($val['bookingCreationDate']));
			} else { 
				$bookingCreationDate =  "NA";
			}
			
			if(isset($val['boFullName']) && $val['boFullName']!=""){ 
				$boFullName = $val['boFullName']; 
			} else { 
				$boFullName =  "NA";
			}
			
			if(isset($val['boEmail']) && $val['boEmail']!=""){ 
				$boEmail = $val['boEmail'];
			} else { 
				$boEmail = "NA";
			}
			
			if(isset($val['boPhone']) && $val['boPhone']!=""){ 
				$boPhone = $val['boPhone']; 
			} else { 
				$boPhone = "NA";
			}
			if(isset($val['boProjects']) && $val['boProjects']!=""){ 
				$totalSeats =  $val['boProjects']; 
			} else { 
				$totalSeats = "NA";
			}
			
			$project = getTicketProjectName($val['id']);
			$personalizations = getTicketProjectPersonalizeInfo($val['id']);
			if(isset($val['promocode']) && $val['promocode']!=""){ 
				$promocode = $val['promocode']; 
			} else { 
				$promocode = "NA";
			}
			
			if(isset($val['totalAmt']) && $val['totalAmt']!=""){ 
				$totalAmt =  "$".$val['totalAmt']; 
			} else { 
				$totalAmt = "NA";
			}
			
			$paymentGateway = getPaymentGateway($val['id']);
			$tansInfo = getBookingTransactioninfo($val['id']);
			//echo "<pre>tansInfo===";print_r($tansInfo);
			if(isset($tansInfo) && !empty($tansInfo)){
				$caddress =  $tansInfo['caddress'];
				$ccity = $tansInfo['ccity'];
				$cstate = $tansInfo['cstate'];
				$czip = $tansInfo['czip'];
			} else {
				if($val['boAddress'] != ""){
					$caddress = $val['boAddress'];
				} else {
					$caddress =  "NA";
				}
				
				if($val['boAccommodationsNotes']!= ""){
					$czip = strpos_arr_check($val['boAccommodationsNotes'],'Zip:');
					$ccity = strpos_arr_check($val['boAccommodationsNotes'],'City:');
					$cstate = strpos_arr_check($val['boAccommodationsNotes'],'State:');
				} else {
					$czip = "NA";
					$ccity = "NA";
					$cstate = "NA";
				}
			}
			if(isset($val['boEventDate']) && $val['boEventDate']!=""){ 
				$boEventDate =  date("m/d/Y",strtotime($val['boEventDate']));
			} else { 
				$boEventDate =  "NA";
			}
			$result[$key]['Status'] = $bookingStatus;
			$result[$key]['BookingID'] = $val['cOrderNo'];
			$result[$key]['BookingDate'] = $bookingCreationDate; 
			$result[$key]['CustomerName'] = $boFullName;
			$result[$key]['CustomerEmail'] = $boEmail;
			$result[$key]['CustomerPhone'] = $boPhone;
			$result[$key]['Seats'] = $totalSeats;
			$result[$key]['Project'] = $project;
			$result[$key]['Personalizations'] = $personalizations;
			$result[$key]['PromoCode'] = $promocode;
			$result[$key]['Total'] = $totalAmt;
			$result[$key]['Gateway'] = $paymentGateway;
			$result[$key]['Address'] = $caddress;
			$result[$key]['City'] = $ccity;
			$result[$key]['State'] = $cstate;
			$result[$key]['ZipCode'] = $czip;
			$result[$key]['EventDate'] = $boEventDate;
		}
		//echo "<pre>result=";print_R($result); die;
		return $result;
	} else {
		return array();
	}
}

//Get Booking info wrt event location id
function getBookinginfoWRTBookingId1($eventLocId,$bookingType,$schStDate,$schEndDate){
	$stwhre = "";
	$stwhre1 = "";
	if(isset($bookingType) && $bookingType =="needatten") { 
		$orderby =" order by bookingCreationDate asc";
		$stwhre.= "  and isDeleted=0 and status=3 ";
	}
	else if(isset($bookingType) && $bookingType =="future") {
		$orderby =" order by bookingCreationDate asc";
		$stwhre.= "  and isDeleted=0 and boEventDate >= CURDATE() ";
	}
	else if(isset($bookingType) && $bookingType =="today") { 
		$orderby =" order by bookingCreationDate asc";
		$stwhre.= "  and isDeleted=0 and boEventDate = CURDATE() ";
	}
	else if(isset($bookingType) && $bookingType =="tomorrow") { 
		$orderby =" order by bookingCreationDate asc";
		$stwhre.= "  and isDeleted=0 and date(boEventDate) = date_add(CURDATE(), interval 1 day) "; 
	}
	else if(isset($bookingType) && $bookingType =="past") { 
		$orderby =" order by bookingCreationDate asc";
		$stwhre.= "  and isDeleted=0 and boEventDate < CURDATE() ";
	}
	else if(isset($bookingType) && $bookingType =="trash") { 
		$orderby =" order by bookingCreationDate asc";
		$stwhre.= "  and isDeleted=1";
	}
	else {
		$orderby =" order by bookingCreationDate asc";
		$stwhre.= "  and isDeleted=0";
	} 
	$schStDate1 = date('Y-m-d',strtotime($schStDate));
    $schEndDate1 = date('Y-m-d',strtotime($schEndDate));
	
	$stwhre1.= "  and DATE(bookingCreationDate)  BETWEEN '".$schStDate1."' AND '".$schEndDate1."' "; 
	global $db;
	$params = array('');
	
	$sql = "SELECT * FROM `bb_booking` where eventLocationId IN(".$eventLocId.") $stwhre $stwhre1 $orderby";
	$result2 = $db->rawQuery($sql, $params);
	$ritems2 = (array)$result2;
	if(isset($ritems2) && !empty($ritems2)) {
		//echo "<pre>ritems2===";print_r($ritems2);die;
		$result = array();
		foreach($ritems2 as $key=>$val){
			$bookingStatus = getBookingStatus($val['id']);
			if(isset($val['bookingCreationDate']) && $val['bookingCreationDate']!=""){ 
				$bookingCreationDate =  date("m/d/Y",strtotime($val['bookingCreationDate']));
			} else { 
				$bookingCreationDate =  "NA";
			}
			
			if(isset($val['boFullName']) && $val['boFullName']!=""){ 
				$boFullName = $val['boFullName']; 
			} else { 
				$boFullName =  "NA";
			}
			
			if(isset($val['boEmail']) && $val['boEmail']!=""){ 
				$boEmail = $val['boEmail'];
			} else { 
				$boEmail = "NA";
			}
			
			if(isset($val['boPhone']) && $val['boPhone']!=""){ 
				$boPhone = $val['boPhone']; 
			} else { 
				$boPhone = "NA";
			}
			if(isset($val['boProjects']) && $val['boProjects']!=""){ 
				$totalSeats =  $val['boProjects']; 
			} else { 
				$totalSeats = "NA";
			}
			
			$project = getTicketProjectName($val['id']);
			$personalizations = getTicketProjectPersonalizeInfo($val['id']);
			if(isset($val['promocode']) && $val['promocode']!=""){ 
				$promocode = $val['promocode']; 
			} else { 
				$promocode = "NA";
			}
			
			if(isset($val['totalAmt']) && $val['totalAmt']!=""){ 
				$totalAmt =  "$".$val['totalAmt']; 
			} else { 
				$totalAmt = "NA";
			}
			
			$paymentGateway = getPaymentGateway($val['id']);
			$tansInfo = getBookingTransactioninfo($val['id']);
			//echo "<pre>tansInfo===";print_r($tansInfo);
			
			if(isset($tansInfo) && !empty($tansInfo)){
				$caddress =  $tansInfo['caddress'];
				$ccity = $tansInfo['ccity'];
				$cstate = $tansInfo['cstate'];
				$czip = $tansInfo['czip'];
			} else {
				if($val['boAddress'] != ""){
					$caddress = $val['boAddress'];
				} else {
					$caddress =  "NA";
				}
				
				if($val['boAccommodationsNotes']!= ""){
					$czip = strpos_arr_check($val['boAccommodationsNotes'],'Zip:');
					$ccity = strpos_arr_check($val['boAccommodationsNotes'],'City:');
					$cstate = strpos_arr_check($val['boAccommodationsNotes'],'State:');
				} else {
					$czip = "NA";
					$ccity = "NA";
					$cstate = "NA";
				}
			}
			
			if(isset($val['boEventDate']) && $val['boEventDate']!=""){ 
				$boEventDate =  date("m/d/Y",strtotime($val['boEventDate']));
			} else { 
				$boEventDate =  "NA";
			}
			$result[$key]['Status'] = $bookingStatus;
			$result[$key]['BookingID'] = $val['cOrderNo'];
			$result[$key]['BookingDate'] = $bookingCreationDate; 
			$result[$key]['CustomerName'] = $boFullName;
			$result[$key]['CustomerEmail'] = $boEmail;
			$result[$key]['CustomerPhone'] = $boPhone;
			$result[$key]['Seats'] = $totalSeats;
			$result[$key]['Project'] = $project;
			$result[$key]['Personalizations'] = $personalizations;
			$result[$key]['PromoCode'] = $promocode;
			$result[$key]['Total'] = $totalAmt;
			$result[$key]['Gateway'] = $paymentGateway;
			$result[$key]['Address'] = $caddress;
			$result[$key]['City'] = $ccity;
			$result[$key]['State'] = $cstate;
			$result[$key]['ZipCode'] = $czip;
			$result[$key]['EventDate'] = $boEventDate;
		}
		//echo "<pre>result=";print_R($result); die;
		return $result;
	} else {
		return array();
	}
}

//Get booking Status
function getBookingStatus($bookingid){
	global $db;
	$params = array('');
	$result5 = $db->rawQueryone("SELECT status,id FROM bb_booking WHERE id='".$bookingid."'", $params);
	$ritems5 = (array)$result5;
	if(isset($ritems5) && !empty($ritems5)) {
		$status = $ritems5['status'];
		if($status == '0'){ 
			$bookingStatus = "Completed"; 
		} else if($status == '1'){ 
			$bookingStatus = "Unapproved"; 
		} else if($status == '2'){ 
			$bookingStatus = "Cancelled"; 
		} else if($status == '3'){ 
			$bookingStatus = "Needs Attention"; 
		} else if($status == '4'){ 
			$bookingStatus = "Change Project"; 
		} else if($status == '5'){ 
			$bookingStatus = "Rejected"; 
		}
	} 
	return $bookingStatus;
}

//Fetch Project dimension WRT ProjectID
function ftechProjectDimension($proId){
	global $db;
	$projDetails = $db->rawQueryOne("SELECT * FROM bb_project WHERE  status=0 and isDeleted=0 and id=?  order by proName asc ", array($proId));
	if(isset($projDetails) && !empty($projDetails)) {
		$proSpecWi = $projDetails['proSpecWi'];
		$proSpecHi = $projDetails['proSpecHi'];
		$proSpecDe = $projDetails['proSpecDe'];
		if($proSpecDe != ""){
			$sizeStr = $proSpecWi."<span class='sizeX'>x</span>".$proSpecHi."<span class='sizeX'>x</span>".$proSpecDe;
		} else {
			$sizeStr = $proSpecWi."<span class='sizeX'>x</span>".$proSpecHi."</b>";
		}
		return $sizeStr;
	} else {
		return 0;
	}
}


/**
* Create excel by from ajax request
*/
function xlscreation_ajax($eventLocId,$bookingType,$schStDate,$schEndDate) {
	require_once SITEPATH . 'PHPExcel/Classes/PHPExcel.php';
	
	$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
	$cacheSettings = array( 'memoryCacheSize' => '8MB', 'cacheTime' => '1000');
	PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
	
	/*$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
	$cacheSettings = array( 'memoryCacheSize' => '8MB');
	PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);*/

	$objPHPExcel = new PHPExcel(); 
	$objPHPExcel->getProperties()
			->setCreator("user")
			->setLastModifiedBy("user")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

	// Set the active Excel worksheet to sheet 0
	$objPHPExcel->setActiveSheetIndex(0); 
		
	//Initialise the Excel row number
	$rowCount = 0; 

	// Sheet cells
	$cell_definition = array(
		'A' => 'Status',
		'B' => 'BookingID',
		'C' => 'BookingDate',
		'D' => 'CustomerName',
		'E' => 'CustomerEmail',
		'F' => 'CustomerPhone',
		'G' => 'Seats',
		'H' => 'Project',
		'I' => 'Personalizations',
		'J' => 'PromoCode',
		'K' => 'Total',
		'L' => 'Gateway',
		'M' => 'Address',
		'N' => 'City',
		'O' => 'State',
		'P' => 'ZipCode',
		'Q' => 'EventDate',
	);
	//Build headers
	foreach( $cell_definition as $column => $value ) {
		$objPHPExcel->getActiveSheet()->setCellValue( "{$column}1", $value ); 
	}
	//Get Booking info wrt event location id
	$eventdetails = getBookinginfoWRTBookingId1($eventLocId,$bookingType,$schStDate,$schEndDate);
	//echo "<pre>eventdetails==";print_R($eventdetails);die;
	$rowCount =0;
	//Build cells
	while( $rowCount < count($eventdetails) ) {
		$cell=$rowCount+2;
		foreach( $cell_definition as $column => $value ) {
			$objPHPExcel->getActiveSheet()->setCellValue($column.$cell, $eventdetails[$rowCount][$value] ); 
		}
		$rowCount++; 
	} 
	ob_end_clean();
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); //Excel2007
	$saveExcelToLocalFile = saveExcelToLocalFileBookingDownload($objWriter,$eventLocId,$schStDate,$schEndDate);
	
	$response = array(
		 'success' => true,
		 'filename' => $saveExcelToLocalFile['filename'],
		 'url' => $saveExcelToLocalFile['filePath']
	);
	echo json_encode($response);
	die();
}

function saveExcelToLocalFileBookingDownload($objWriter,$eventLocId,$schStDate,$schEndDate) {
	$stdInfo = getStudioInfoByStdId($eventLocId);
	$stdName = $stdInfo['stName'];
	$schStDate1 = date('m-d-Y',strtotime($schStDate));
    $schEndDate1 = date('m-d-Y',strtotime($schEndDate));
	$fileName = $stdName.$schStDate1."to".$schEndDate1.".xls";
	//make sure you have permission to write to directory
    $filePath = SITEPATH .'reports/'.$fileName;
	
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'.$fileName.'"');
	header('Cache-Control: max-age=0');
	ob_end_clean();
    $objWriter->save($filePath);
	$data = array(
    	'filename' => $fileName,
    	'filePath' => $filePath
	);
	
	/*if(file_exists($filePath)) { 
		unlink($filePath);
	} */
    return $data;
}

/* *************************************************
end code By Vipul For Prep report at event detail page at 5july2018 
***************************************************
*/




//PHP and mysql Injection functions

function StringInputCleaner($data)
{
	//remove space bfore and after
	$data = trim($data); 
	//remove slashes
	$data = stripslashes($data); 
	$data=(filter_var($data, FILTER_SANITIZE_SPECIAL_CHARS));
	return $data;
}


function IntegerInputCleaner($data)
{
	//remove space bfore and after
	$data = trim($data); 
	//remove slashes
	$data=(filter_var($data, FILTER_SANITIZE_NUMBER_INT));
	return $data;
}



function mysqlCleaner($data)
{
	$data= mysql_real_escape_string($data);
	$data= stripslashes($data);
	return $data;
	//or in one line code 
	//return(stripslashes(mysql_real_escape_string($data)));
}	

function makeASCII($char=0){
  return '&#'.ord($char).';';
}


function encodeEmail($email){
/*** check if the filter has a var ***/
if(filter_var($email, FILTER_VALIDATE_EMAIL) !== FALSE)
    {
    /*** split the email into single chars ***/
    $charArray = str_split($email);
    /*** apply a callback funcion to each array member ***/
    $encodedArray = filter_var($charArray, FILTER_CALLBACK, array('options'=>"makeASCII"));
    /*** put the string back together ***/
    $encodedString = implode('',$encodedArray);
    return $encodedString;
    }
else
  {
  return false;
  }
}


function checkSpace($str) { //Temp function
return trim($str);
}

function StringEscapeSlug($slug){
	$unwanted_array = 
		array(
			'�'=>'S', '�'=>'s', '�'=>'Z', '�'=>'z', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'C', '�'=>'E', '�'=>'E',
			'�'=>'E', '�'=>'E', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'N', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'U',
			'�'=>'U', '�'=>'U', '�'=>'U', '�'=>'Y', '�'=>'B', '�'=>'Ss', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'c',
			'�'=>'e', '�'=>'e', '�'=>'e', '�'=>'e', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'o', '�'=>'n', '�'=>'o', '�'=>'o', '�'=>'o', '�'=>'o',
			'�'=>'o', '�'=>'o', '�'=>'u', '�'=>'u', '�'=>'u', '�'=>'y', '�'=>'b', '�'=>'y' 
		);
       
	$str = strtr($slug, $unwanted_array );
	$slug = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($str)));
	return $slug;
}



function getTimezoneDifference($studioTimezone,$serverTimezone){
	// This will return 10800 (3 hours) ...
	$offset = get_timezone_offset($studioTimezone,$serverTimezone);
	// or, if your server time is already set to 'America/New_York'...
	//$offset = get_timezone_offset($studioTimezone);
	// You can then take $offset and adjust your timestamp.
	//$offset_time = time() + $offset;
	$offset_time = $offset;
	return secondMinute($offset_time);
}

/**    Returns the offset from the origin timezone to the remote timezone, in seconds.
*    @param $remote_tz;
*    @param $origin_tz; If null the servers current timezone is used as the origin.
*    @return int;
*/
function get_timezone_offset($remote_tz, $origin_tz = null) {
    if($origin_tz === null) {
        if(!is_string($origin_tz = date_default_timezone_get())) {
            return false; // A UTC timestamp was returned -- bail out!
        }
    }
    $origin_dtz = new DateTimeZone($origin_tz);
    $remote_dtz = new DateTimeZone($remote_tz);
    $origin_dt = new DateTime("now", $origin_dtz);
    $remote_dt = new DateTime("now", $remote_dtz);
    $offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
    return $offset;
}

function secondMinute($seconds){
	/// get minutes
    $minResult = floor($seconds/60);
    /// if minutes is between 0-9, add a "0" --> 00-09
    if($minResult < 10){$minResult = 0 . $minResult;}
    /// return result
	return $minResult;
}

function getStudioTimeZone($stid) {
	global $db;
$chkq=$db->rawQueryOne("SELECT stimezone FROM bb_studiolocation WHERE id='".$stid."'", array(''));
if(isset($chkq['stimezone']) && !empty($chkq['stimezone'])) {
return $chkq['stimezone'];
} else {
return "America/Chicago";
}
}



if(isset($_REQUEST["stId"]) && $_REQUEST["stId"]!="") {
$_SESSION["dstId"]=$_REQUEST["stId"];
} else {
$_SESSION["dstId"]=$_SESSION["dstId"];
}



global $deflttimezone;
if(!empty($_SESSION["dstId"])){
	$dttimezone=getStudioTimeZone($_SESSION["dstId"]);
	/*echo $dttimezone;
	die();*/
	$deflttimezone=$dttimezone;
	date_default_timezone_set($dttimezone);
} 
else {
	$deflttimezone=Server_Default_Timezone;
	date_default_timezone_set(Server_Default_Timezone);
}

$serverTimezone = Server_Default_Timezone; //server default timezone
$timezoneDiffInMin = getTimezoneDifference($deflttimezone,$serverTimezone);
define('INTERVALTIME',' - INTERVAL '.intval($timezoneDiffInMin).' Minute'); //server interal time difference



function fnMultiStudioTitle($stid) {
// && $_SESSION["ms"]==1
if((isset($stid) && $stid!="") || (isset($_REQUEST["stId"]) && $_REQUEST["stId"]!="")) {
return ''.getNamebyPara("bb_studiolocation","stName","id",$stid).' ';
} else {
return '';
}

}


//new function added by pankaj on mar 28 2019 for phase 2
function fetchTblDataByIdForProjectSection_1($tbl,$tb2,$col1,$id1) {
 global $db;
 $params = array('');
 $sql_query ="SELECT $tbl.*,GROUP_CONCAT(option_value)  as optionValues FROM  $tbl LEFT JOIN $tb2 on $tbl.id=$tb2.propersoptions_id  WHERE $tbl.$col1='".$id1."' GROUP BY bb_propersoptions.id";
 $proPersresult = $db->rawQuery($sql_query, $params);
 $proPersitems = (array)$proPersresult;
 return $proPersitems;
}
function getCodeList() {
global $db;
$options='<option value="" selected>Select Code</option>';	
//and stStatus=4
	$sql="SELECT id,code FROM bb_code  order by id DESC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){
	foreach($results as $kv=>$val) {
		//$options[$val['id']]=$val['code'];
		$options.='<option value="'.$val['id'].'">'.$val['code'].'</option>';
	}
	
	//pr($options);
	return $options;
	}	else {
	return $options;
	}
	
}

function getCodeList_2($id) {
global $db;
$options='<option value="" selected>Select Code</option>';	
//and stStatus=4
	 $sql="SELECT id,code FROM bb_code   WHERE id NOT IN ($id)   order by id DESC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){
	foreach($results as $kv=>$val) {
		//$options[$val['id']]=$val['code'];
		$options.='<option value="'.$val['id'].'">'.$val['code'].'</option>';
	}
	
	//pr($options);
	return $options;
	}	else {
	return $options;
	}
	
}

function getCodeList_remaining($id) {
global $db;

//and stStatus=4
	 $sql="SELECT id,code FROM bb_code   WHERE id NOT IN ($id)   order by id DESC";
	 $results =  $db->rawQuery($sql);
	 return count($results);
	
}
//functio to get code list
function getCodeList_1($id) {
	global $db;
	$sql="SELECT id,code FROM bb_code   WHERE id NOT IN ($id)   order by id DESC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){
	  return $results;
	} 
	else {
		return false;
	}
}

function getCodeList_all() {
	global $db;
	$sql="SELECT id,code FROM bb_code    order by id DESC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){
	  return $results;
	} 
	else {
		return false;
	}
}



function getStudioList() {
global $db;
$options='<option value="">Select Studio</option>';	
//and stStatus=4
	$sql="SELECT id,stName,stNum,stEmail FROM bb_studiolocation  order by id DESC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){
	foreach($results as $kv=>$val) {
		$options[$val['id']]=$val['stName'].'_'.$val['stNum'];
		$options.='<option value="'.$val['id'].'">'.$val['stName'].'_'.$val['stNum'].'</option>';
	}
	
	//pr($options);
	return $options;
	}	else {
	return $options;
	}
	
}


function getStudioList_1($id) {
	global $db;
	$sql="SELECT id,stName,stNum,stEmail FROM bb_studiolocation WHERE id NOT IN ($id)   order by id DESC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){
	  return $results;
	} 
	else {
		return false;
	}
}
function getAllStudioList_1($id) {
global $db;
	$sql="SELECT id,stName,stNum,stEmail FROM bb_studiolocation   order by id DESC";
	$results =  $db->rawQuery($sql);
	if(count($results)>0){
	  return $results;
	} 
	else {
		return false;
	}
}

function getRestrications($tbl,$col1,$id1) {
 global $db;
 $params = array('');
 $sql_query ="SELECT *  FROM  $tbl   WHERE $tbl.$col1='".$id1."'";
 $proPersresult = $db->rawQuery($sql_query, $params);
 $proPersitems = (array)$proPersresult;
 return $proPersitems;
}
function getStudioNameFromId($tbl,$col1,$id) {
 global $db;
 $params = array('');
 $sql_query ="SELECT CONCAT(stName,'_',stNum) as name  FROM  $tbl   WHERE $tbl.$col1='".$id."' order by id DESC";
 $proPersresult = $db->rawQuery($sql_query, $params);
 $proPersitems = (array)$proPersresult;
 return $proPersitems;

}

function getCodeNameFromId($tbl,$col1,$id) {
 global $db;
 $params = array('');
 $sql_query ="SELECT code  FROM  $tbl   WHERE $tbl.$col1='".$id."' order by id DESC";
 $proPersresult = $db->rawQuery($sql_query, $params);
 $proPersitems = (array)$proPersresult;
 return $proPersitems;

}
function changeTimeTo12HourFormat($getTime){
	   $date = new DateTime($getTime);
	   return $date->format('h:i a') ;
 }
function pr($array){
	echo "<pre>";
	 print_r($array);
	echo "</pre>"; 

}
//end of function




?>