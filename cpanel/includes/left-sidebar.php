<div class="left-navigation">
<ul>
	<?php 
	if( isset($_SESSION["urole"]) && ($_SESSION["urole"]==1 || $_SESSION["urole"]==2) ) {?>
	<li>
		<a class="down <?php echo $menuClssStudio; ?>" href="javascript:void(0);"><img src="<?php echo base_url_images;?>booking-icon.png" /> Locations <i class="fa fa-angle-right"></i></a>
		<ul class="submenu <?php echo $menuSbClssStudio; ?>" <?php echo $menuSbClssStyleStudio; ?>>
			<?php if( isset($_SESSION["urole"]) && $_SESSION["urole"]==1){ ?>
			<li class="<?php echo $menuSbClssStudio2?>"><a href="<?php echo base_url_site?>addupdstudio">Add Location</a></li>
			<?php }?>
			<li class="<?php echo $menuSbClssStudio1?>"><a href="<?php echo base_url_site?>studiolocs">All</a></li>
			<!--<li><a href="#">Favourites</a></li>-->
		</ul>
	</li>
	<?php 
	}?>
	<?php 
	if( isset($_SESSION["urole"]) && $_SESSION["urole"]==3 ) {
	if( isset($_SESSION["studioIds"]) && $_SESSION["studioIds"]!="" ) {
	$Studios=explode(",",$_SESSION['studioIds']);
	$totalStudio=count($Studios);
	
	if($totalStudio>1) {
	if(isset($_SESSION["curRole"]) && !empty($_SESSION["curRole"])) {
	$llink="edit-locationinfo";
	} else {
	$llink="edit-locationinfo";
	}
	
	?>
	<li>
		<a class="down <?php echo $menuClssStudio; ?>" href="javascript:void(0);"><img src="<?php echo base_url_images;?>booking-icon.png" /> Locations <i class="fa fa-angle-right"></i></a>
		<ul class="submenu <?php echo $menuSbClssStudio; ?>" <?php echo $menuSbClssStyleStudio; ?>>
            <?php 
			$params = array('');
			$slist = $db->rawQuery("SELECT * FROM `bb_studiolocation` where id IN(".$_SESSION["studioIds"].") and (stStatus=4 or stStatus=3 or stStatus=1 or stStatus=2) and isDeleted=0 order by id asc", $params);
			if(isset($slist) && !empty($slist)) {
			foreach ($slist as $key=>$item2) {
			if($_SESSION["stId"]==$item2['id']) {
			echo '<li class="active"><a href="'.base_url_site.$llink.'?stId='.$item2['id'].'">'.$item2['stName'].'</a></li>';
			} else {
			echo '<li><a href="'.base_url_site.$llink.'?stId='.$item2['id'].'">'.$item2['stName'].'</a></li>';
			}
			}
			}
			?>
			<!--<li><a href="#">Favourites</a></li>-->
		</ul>
	</li>
	<?php 
	} else {
	if(isset($_SESSION["curRole"]) && !empty($_SESSION["curRole"])) {
	$llink1="edit-locationinfo";
	} else {
	$llink1="edit-locationinfo";
	}
	echo '<li>
		<a class="down '.$menuClssStudio.'" href="'.base_url_site.$llink1.'?stId='.$_SESSION["studioIds"].'"><img src="'.base_url_images.'booking-icon.png" /> Location <i class="fa fa-angle-right"></i></a></li>';
	}
	}
	}?>
    	
    
    
	<li><a class="down <?php echo $menuClssEvents; ?>" href="javascript:void(0);"><img src="<?php echo base_url_images;?>event-icon.png" /> Events <i class="fa fa-angle-right"></i></a> 
		<ul class="submenu <?php echo $menuSbClssEvents; ?>" <?php echo $menuSbClssStyleEvents; ?>>
			<?php if(isset($_SESSION["urole"]) && ($_SESSION["urole"]==1 || $_SESSION["urole"]==2) ) {?>
				<li class="alleventtemp"><a href="<?php echo base_url_site?>eventtemplist">Event Templates</a></li>
				<li class="<?php echo $menuSbClssA1Events?>"><a href="<?php echo base_url_site?>addupdeventtemp">Add Event Template</a></li>
				<li class="draft"><a href="<?php echo base_url_site?>eventtemplist?estatus=draft">Drafts</a></li>
				<li class="archive"><a href="<?php echo base_url_site?>eventtemplist?estatus=archive">Archive</a></li>
			<?php }?>
			
			<?php if( (isset($_SESSION["stidloc"]) && $_SESSION["stidloc"]!="") &&  (isset($_SESSION["urole"]) && ($_SESSION["urole"]==1 || $_SESSION["urole"]==2) ) ) {?>
				<li class="<?php echo $menuClssEvents1; ?> stnamecls"><a href="<?php echo base_url_site?>edit-locationinfo?studid=<?php echo $_SESSION["stidloc"];?>"><strong><?php echo getNamebyPara("bb_studiolocation","stName","id",$_SESSION["stidloc"]);?></strong></a></li>
				<li class="allevent"><a href="<?php echo base_url_site?>eventlist?studid=<?php echo $_SESSION["stidloc"];?>">Events</a></li>
				<li class="<?php echo $menuSbClssEvents1?>"><a href="<?php echo base_url_site?>addupdevent?studid=<?php echo $_SESSION["stidloc"];?>">Add Event</a></li>
				<li class="current"><a href="<?php echo base_url_site?>eventlist?studid=<?php echo $_SESSION["stidloc"];?>&estatus=current">Current</a></li>
				<!--<li><a href="#">Booked</a></li>
				<li><a href="#">Needs Attention</a></li>-->
				<li class="past"><a href="<?php echo base_url_site?>eventlist?studid=<?php echo $_SESSION["stidloc"];?>&estatus=past">Past</a></li>
				<li class="draft1"><a href="<?php echo base_url_site?>eventlist?studid=<?php echo $_SESSION["stidloc"];?>&estatus=draft">Drafts</a></li>
				<?php /*?><li><a href="<?php echo base_url_site?>eventlist?studid=<?php echo $_SESSION["stidloc"];?>&estatus=schedule">Schedule</a></li><?php */?>
				<li class="trash"><a href="<?php echo base_url_site?>eventlist?studid=<?php echo $_SESSION["stidloc"];?>&estatus=trash">Trash</a></li>
			<?php } 
			
			if(isset($_SESSION["urole"]) && $_SESSION["urole"]==3) {?>
				<li class="allevent"><a href="<?php echo base_url_site?>eventlist">Events</a></li>
				<li class="<?php echo $menuSbClssEvents1?>"><a href="<?php echo base_url_site?>addupdevent">Add Event</a></li>
				<li class="current"><a href="<?php echo base_url_site?>eventlist?estatus=current">Current</a></li>
				<!--<li><a href="#">Booked</a></li>
				<li><a href="#">Needs Attention</a></li>-->
				<li class="past"><a href="<?php echo base_url_site?>eventlist?estatus=past">Past</a></li>
				<li class="draft1"><a href="<?php echo base_url_site?>eventlist?estatus=draft">Drafts</a></li>
				<?php /*?><li><a href="<?php echo base_url_site?>eventlist?estatus=schedule">Schedule</a></li><?php */?>
				<li class="trash1"><a href="<?php echo base_url_site?>eventlist?estatus=trash">Trash</a></li>
			<?php }?>
		</ul>
	</li>

	<?php 
	if((isset($_SESSION["stidloc"]) && $_SESSION["stidloc"]!="") &&  (isset($_SESSION["urole"]) && ($_SESSION["urole"]==1 || $_SESSION["urole"]==2) )) {?>
		<li><a class="down <?php echo $menuClssBookingTemp;?>" href="javascript:void(0);"><img src="<?php echo base_url_images;?>booking-icon.png" />Bookings <i class="fa fa-angle-right"></i></a>
			<ul class="submenu <?php echo $menuSbClssBookingTemp;?>" <?php echo $menuSbClssStyleBookingTemp;?>>
				<li class="allbooking"><a href="<?php echo base_url_site?>bookinglist">All</a></li>
				<!--<li class="customer"><a href="<?php //echo base_url_site?>bookinglist?type=customer">Customers</a></li>-->
				<li class="needatten"><a href="<?php echo base_url_site?>bookinglist?type=needatten">Needs Attention <?php echo "<span style='float:right;'>".CntOfNeedAttentionBooking()."</span>"; ?></a></li>
				<li class="current"><a href="<?php echo base_url_site?>bookinglist?type=future">Future</a></li>
				<li class="today"><a href="<?php echo base_url_site?>bookinglist?type=today">Today</a></li>
				<li class="tomorrow"><a href="<?php echo base_url_site?>bookinglist?type=tomorrow">Tomorrow</a></li>
				<li class="past"><a href="<?php echo base_url_site?>bookinglist?type=past">Past</a></li>
				<li class="trash"><a href="<?php echo base_url_site?>bookinglist?type=trash">Trash</a></li>
			</ul>
		</li>
	<?php 
	}

	if(isset($_SESSION["urole"]) && $_SESSION["urole"]==3) {?>
		<li><a class="down <?php echo $menuClssBookingTemp;?>" href="javascript:void(0);"><img src="<?php echo base_url_images;?>booking-icon.png" />Bookings <i class="fa fa-angle-right"></i></a>
			<ul class="submenu <?php echo $menuSbClssBookingTemp;?>" <?php echo $menuSbClssStyleBookingTemp;?>>
				<li class="allbooking"><a href="<?php echo base_url_site?>bookinglist">All</a></li>
				<!--<li class="customer"><a href="<?php //echo base_url_site?>bookinglist?type=customer">Customers</a></li>-->
				<li class="needatten"><a href="<?php echo base_url_site?>bookinglist?type=needatten">Needs Attention <?php echo "<span style='float:right;'>".CntOfNeedAttentionBooking()."</span>"; ?></a></li>
				<li class="current"><a href="<?php echo base_url_site?>bookinglist?type=future">Future</a></li>
				<li class="today"><a href="<?php echo base_url_site?>bookinglist?type=today">Today</a></li>
				<li class="tomorrow"><a href="<?php echo base_url_site?>bookinglist?type=tomorrow">Tomorrow</a></li>
				<li class="past"><a href="<?php echo base_url_site?>bookinglist?type=past">Past</a></li>
				<li class="trash"><a href="<?php echo base_url_site?>bookinglist?type=trash">Trash</a></li>
			</ul>
		</li>
	<?php }?> 

	<?php 
	if((isset($_SESSION["stidloc"]) && $_SESSION["stidloc"]!="") &&  (isset($_SESSION["urole"]) && ($_SESSION["urole"]==1 || $_SESSION["urole"]==2) )) {?>
		<li><a class="down <?php echo $menuClssPromocode;?>" href="javascript:void(0);"><img src="<?php echo base_url_images;?>promo-icon.png" /> Coupon Codes <i class="fa fa-angle-right"></i></a>
			<ul class="submenu <?php echo $menuSbClssPromocode;?>" <?php echo $menuSbClssStylePromocode;?>>
				<li class="allPromocode"><a href="<?php echo base_url_site?>promocodelist?studid=<?php echo $_SESSION["stidloc"];?>">Coupon Codes</a></li>
				<li class="<?php echo $menuClssPromocodetem2;?>"><a href="<?php echo base_url_site?>addupdpromocode?studid=<?php echo $_SESSION["stidloc"];?>">Create Coupon Code</a></li>
				<li class="archivePromocode"><a href="<?php echo base_url_site?>promocodelist?studid=<?php echo $_SESSION["stidloc"];?>&type=archive">Archive</a></li>
				<li class="currentPromocode"><a href="<?php echo base_url_site?>promocodelist?studid=<?php echo $_SESSION["stidloc"];?>&type=current">Current</a></li>
				<li class="expirePromocode"><a href="<?php echo base_url_site?>promocodelist?studid=<?php echo $_SESSION["stidloc"];?>&type=expired">Expired</a></li>
			</ul>
		</li>
	<?php 
	}
	
	if(isset($_SESSION["urole"]) && $_SESSION["urole"]==3) {?>
	<li><a class="down <?php echo $menuClssPromocode;?>" href="javascript:void(0);"><img src="<?php echo base_url_images;?>promo-icon.png" /> Coupon Codes <i class="fa fa-angle-right"></i></a>
		<ul class="submenu <?php echo $menuSbClssPromocode;?>" <?php echo $menuSbClssStylePromocode;?>>
			<li class="allPromocode"><a href="<?php echo base_url_site?>promocodelist">Coupon Codes</a></li>
			<li class="<?php echo $menuClssPromocodetem2;?>"><a href="<?php echo base_url_site?>addupdpromocode">Create Coupon Code</a></li>
			<li class="archivePromocode"><a href="<?php echo base_url_site?>promocodelist?type=archive">Archive</a></li>
			<li class="currentPromocode"><a href="<?php echo base_url_site?>promocodelist?type=current">Current</a></li>
			<li class="expirePromocode"><a href="<?php echo base_url_site?>promocodelist?type=expired">Expired</a></li>
		</ul>
	</li>
	<?php }?> 


	<!--<li><a class="down"href="#"><img src="images/info-icon.png" /> Location Info <i class="fa fa-angle-right"></i></a></li>-->
	<!--<li><a class="down" href="javascript:void(0);"><img src="images/report-icon.png" /> Reports <i class="fa fa-angle-right"></i></a></li>-->

	<li><a class="down <?php echo $menuClssProject; ?>" href="javascript:void(0);"><img src="<?php echo base_url_images;?>project-icon.png" /> Projects <i class="fa fa-angle-right"></i></a>
		<ul class="submenu <?php echo $menuSbClssProject; ?>" <?php echo $menuSbClssStyleProject; ?>>
		<?php 
		if(isset($_SESSION["urole"])) { 
			if( $_SESSION["urole"]==1 || $_SESSION["urole"]==2 || $_SESSION["urole"]==3 ) {?>
				<li class="allProject"><a href="<?php echo base_url_site?>projectlist">All</a></li>
			<?php }
			if( $_SESSION["urole"]==1 || $_SESSION["urole"]==2 ) {?>
			    <li class="<?php echo $menuClssProjecttem2;?>"><a href="<?php echo base_url_site?>addupdproject">Add Project</a></li>
				<li class="draftProject"><a href="<?php echo base_url_site?>projectlist?type=draft">Drafts</a></li>
				<li class="archiveProject"><a href="<?php echo base_url_site?>projectlist?type=archive">Archive</a></li>
				<li class="<?php echo $menuClssProjecttem5;?>"><a href="<?php echo base_url_site?>categorylist">Categories</a></li>
				<li class="<?php echo $menuClssProjecttem6;?>"><a href="<?php echo base_url_site?>projectschlist">Schedule</a></li>
			<?php } 
			if($_SESSION["urole"]==3) {?>
				<li class="<?php echo $menuClssProjecttem7;?>"><a href="<?php echo base_url_site?>projectltrellist">Latest Releases</a></li>
				<!--<li class="<?php //echo $menuClssProjecttem7;?>"><a href="<?php //echo base_url_site?>projectltrellist">NEW</a></li>-->
			
			<?php }
		}?>
		</ul>
	</li>

	<?php if(isset($_SESSION["urole"]) && ( $_SESSION["urole"]==1 || $_SESSION["urole"]==2 ) ) {
	if(isset($_SESSION["urole"]) && ( $_SESSION["urole"]==1 && $_SESSION["stidloc"]!="") ) {
	$urlPre='javascript:void(0);';
	} else {
	$urlPre='javascript:void(0);';
	}
	}
	?>
    <li><a class="down <?php echo $menuClssSetting; ?>" href="javascript:void(0);"><img src="<?php echo base_url_images;?>setting-icon.png" /> Settings <i class="fa fa-angle-right"></i></a>
		<ul class="submenu <?php echo $menuSbClssSetting; ?>" <?php echo $menuSbClssStyleSetting; ?>>
				<?php if(isset($_SESSION["urole"]) && ( $_SESSION["urole"]==1 || $_SESSION["urole"]==2  || !empty($_SESSION["curRole"])) ) {?>  
			<li class="<?php echo $menuClssSetting1;?>"><a href="<?php echo base_url_site?>projectFinePrintlist">Global Settings</a></li>
		<?php }?>
			<?php 
			if((isset($_SESSION["stId"]) && $_SESSION["stId"]!="") &&  (isset($_SESSION["urole"]) && ($_SESSION["urole"]==1 || $_SESSION["urole"]==2 || !empty($_SESSION["curRole"])) &&  $_SESSION["urole"]==3)) { ?>
				<?php /*?><li class="<?php echo $menuClssFinePrint2;?>"><a href="<?php echo base_url_site?>studioSaleTax">Global Setting</a></li><?php */?>
				<li class="<?php echo $menuClssSetting2;?>"><a href="<?php echo base_url_site?>studioSaleTax?studid=<?php echo $_SESSION["stId"];?>"><?php echo getNamebyPara("bb_studiolocation","stName","id",$_SESSION["stId"]);?> Settings</a></li>
                <li class="<?php echo $menuClssTzone2;?>"><a href="<?php echo base_url_site?>studioTimeZone?studid=<?php echo $_SESSION["stId"];?>"><?php echo getNamebyPara("bb_studiolocation","stName","id",$_SESSION["stId"]);?> Time Zone Settings</a></li>
			<?php }
			if(isset($_SESSION["urole"]) && $_SESSION["urole"]==3   && empty($_SESSION["curRole"])) {?>
				<li class="<?php echo $menuClssFinePrint2;?>"><a href="<?php echo base_url_site?>studioSaleTax">Studio Settings</a></li>
                 <li class="<?php echo $menuClssTzone2;?>"><a href="<?php echo base_url_site?>studioTimeZone">Studio Time Zone Settings</a></li>
			<?php }?>
		</ul>
	</li>

 <?php if(isset($_SESSION["urole"]) && ($_SESSION["urole"]==3 || $_SESSION["urole"]==2|| $_SESSION["urole"]==1)) {?>
	<li><a class="down <?php echo $menuClssFinePrint; ?>" href="javascript:void(0);"><img src="<?php echo base_url_images;?>setting-icon.png" /> Preferences <i class="fa fa-angle-right"></i></a>
		<ul class="submenu <?php echo $menuSbClssFinePrint; ?>" <?php echo $menuSbClssStyleFinePrint; ?>>
       
        <li class="<?php echo $menuClssFinePrint1;?>"><a href="<?php echo base_url_site.'usersetting';?>">User Password Management</a></li>
       
         
			
		</ul>
	</li>
	 <?php }?>

	<?php 
	if( isset($_SESSION["urole"]) && ($_SESSION["urole"]==1 || $_SESSION["urole"]==2) ) {?>
	<li><a class="down <?php echo $menuClssUsers; ?>" href="javascript:void(0);"><img src="<?php echo base_url_images;?>booking-icon.png" /> Users <i class="fa fa-angle-right"></i></a>
		<ul class="submenu <?php echo $menuSbClssUsers; ?>" <?php echo $menuSbClssStyleUsers; ?>>
			<?php if( isset($_SESSION["urole"]) && $_SESSION["urole"]==1){ ?>
			<li class="<?php echo $menuSbClssUsers2?>"><a href="<?php echo base_url_site?>addupduser">Add User</a></li>
			<?php }?>	
			<li class="<?php echo $menuSbClssUsers1?>"><a href="<?php echo base_url_site?>users">Users</a></li>
		</ul>
	</li>
	<?php }?>
</ul>
</div>