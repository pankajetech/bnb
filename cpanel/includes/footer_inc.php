
<script>
/*var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";
  
  var prevArrow = document.getElementsByClassName('lft-arr-calander');
  var nextArrow = document.getElementsByClassName('rght-arr-calander');
  prevArrow[0].style.display = "block";
  nextArrow[0].style.display = "block";
  if (slideIndex === 1) prevArrow[0].style.display = "none";
  if (slideIndex === x.length) nextArrow[0].style.display = "none";
    
}*/
</script>
<!-- Footer section start-->

<div class="footer">
<div class="footer-middle">
<div class="ft-section-1">
<div class="ft-logo"><img src="<?php echo base_url_site?>images/footer-logo.png"></div>
<div class="ft-social-icon">
<a href="https://www.facebook.com/boardandbrushcorporate/" rel="nofollow"  target="_blank" alt="Follow Us on facebook" title="Follow Us on facebook"><svg class="mk-svg-icon" data-name="mk-jupiter-icon-facebook" data-cacheid="icon-5ad586a6c1f74" style=" height:32px; width: 32px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256-6.4c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm64.057 159.299h-49.041c-7.42 0-14.918 7.452-14.918 12.99v19.487h63.723c-2.081 28.41-6.407 64.679-6.407 64.679h-57.566v159.545h-63.929v-159.545h-32.756v-64.474h32.756v-33.53c0-8.098-1.706-62.336 70.46-62.336h57.678v63.183z"></path></svg></a>
<a href="https://www.instagram.com/explore/locations/1042276229137658/board-brush-creative-studio-corporate-site/" rel="nofollow" class="builtin-icons custom large instagram-hover" target="_blank" alt="Follow Us on instagram" title="Follow Us on instagram"><svg class="mk-svg-icon" data-name="mk-jupiter-icon-instagram" data-cacheid="icon-5ad586a6c2196" style=" height:32px; width: 32px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 297.6c27.2 0 48-20.8 48-48s-20.8-48-48-48-48 20.8-48 48 20.8 48 48 48zm80-48c0 44.8-35.2 80-80 80s-80-35.2-80-80c0-8 0-12.8 3.2-19.2h-19.2v107.2c0 4.8 3.2 9.6 9.6 9.6h174.4c4.8 0 9.6-3.2 9.6-9.6v-107.2h-19.2c1.6 6.4 1.6 11.2 1.6 19.2zm-22.4-48h28.8c4.8 0 9.6-3.2 9.6-9.6v-28.8c0-4.8-3.2-9.6-9.6-9.6h-28.8c-4.8 0-9.6 3.2-9.6 9.6v28.8c0 6.4 3.2 9.6 9.6 9.6zm-57.6-208c-140.8 0-256 115.2-256 256s115.2 256 256 256 256-115.2 256-256-115.2-256-256-256zm128 355.2c0 16-12.8 28.8-28.8 28.8h-198.4c-9.6 0-28.8-12.8-28.8-28.8v-198.4c0-16 12.8-28.8 28.8-28.8h196.8c16 0 28.8 12.8 28.8 28.8v198.4z"></path></svg></a>
<a href="http://pinterest.com/boardandbrush" rel="nofollow" class="builtin-icons custom large pinterest-hover" target="_blank" alt="Follow Us on pinterest" title="Follow Us on pinterest"><svg class="mk-svg-icon" data-name="mk-jupiter-icon-pinterest" data-cacheid="icon-5ad586a6c239f" style=" height:32px; width: 32px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256-6.4c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm25.508 327.119c-20.463 0-39.703-10.759-46.285-22.973 0 0-11.014 42.454-13.332 50.654-8.206 28.956-32.336 57.931-34.204 60.3-1.31 1.665-4.206 1.132-4.509-1.046-.518-3.692-6.671-40.229.567-70.031 3.638-14.973 24.374-100.423 24.374-100.423s-6.043-11.758-6.043-29.166c0-27.301 16.275-47.695 36.541-47.695 17.236 0 25.559 12.585 25.559 27.661 0 16.856-11.034 42.045-16.726 65.388-4.754 19.568 10.085 35.51 29.901 35.51 35.895 0 60.076-44.851 60.076-97.988 0-40.37-27.955-70.62-78.837-70.62-57.474 0-93.302 41.693-93.302 88.276 0 16.037 4.881 27.376 12.511 36.129 3.501 4.032 4 5.65 2.728 10.264-.929 3.396-2.993 11.566-3.874 14.802-1.261 4.669-5.144 6.335-9.488 4.613-26.458-10.512-38.802-38.716-38.802-70.411 0-52.337 45.394-115.119 135.43-115.119 72.351 0 119.955 50.911 119.955 105.569 0 72.294-41.325 126.306-102.241 126.306z"></path></svg></a>
<a href="https://twitter.com/boardandbrushcs" rel="nofollow" class="builtin-icons custom large twitter-hover" target="_blank" alt="Follow Us on twitter" title="Follow Us on twitter"><svg class="mk-svg-icon" data-name="mk-jupiter-icon-twitter" data-cacheid="icon-5ad586a6c258e" style=" height:32px; width: 32px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256-6.4c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm146.24 258.654c-31.365 127.03-241.727 180.909-338.503 49.042 37.069 35.371 101.619 38.47 142.554-3.819-24.006 3.51-41.47-20.021-11.978-32.755-26.523 2.923-41.27-11.201-47.317-23.174 6.218-6.511 13.079-9.531 26.344-10.407-29.04-6.851-39.751-21.057-43.046-38.284 8.066-1.921 18.149-3.578 23.656-2.836-25.431-13.295-34.274-33.291-32.875-48.326 45.438 16.866 74.396 30.414 98.613 43.411 8.626 4.591 18.252 12.888 29.107 23.393 13.835-36.534 30.915-74.19 60.169-92.874-.493 4.236-2.758 8.179-5.764 11.406 8.298-7.535 19.072-12.719 30.027-14.216-1.257 8.22-13.105 12.847-20.249 15.539 5.414-1.688 34.209-14.531 37.348-7.216 3.705 8.328-19.867 12.147-23.872 13.593-2.985 1.004-5.992 2.105-8.936 3.299 36.492-3.634 71.317 26.456 81.489 63.809.719 2.687 1.44 5.672 2.1 8.801 13.341 4.978 37.521-.231 45.313-5.023-5.63 13.315-20.268 23.121-41.865 24.912 10.407 4.324 30.018 6.691 43.544 4.396-8.563 9.193-22.379 17.527-45.859 17.329z"></path></svg></a>
					<style>
						</style>

</div>
</div>

<div class="ft-section-2">
<h3 class="ft-header">Board & Brush</h3>
<p>
CORPORATE OFFICE<br/>
---<br/>
115 HILL STREET<br/>
HARTLAND, WI 53029
<br/><br/>
CORPORATE EMAIL<br/>
<a href="#">hello@boardandbrush.com</a>
</p>
</div>

<div class="ft-section-3">
<h3 class="ft-header">QUICK LINKS</h3>

<ul>
<li><a href="#">HOME</a></li>
<li><a href="#">ABOUT</a></li>
<li><a href="#">PRIVACY POLICY</a></li>
<li><a href="#">FAQs</a></li>
<li><a href="#">NEWSLETTER SIGN-UP</a></li>
<li><a href="#">CONTACT YOUR STUDIO</a></li>
</ul>

</div>

 <div class="ft-section-4">
 <h3 class="ft-header">LATEST NEWS</h3>
 <div class="ft-latest-news">
 <a href="#">Board & Brush Mechanicsburg, PA is NOW OPEN!</a>
 <div class="news-date">March 24, 2018</div>
 </div>
 
  <div class="ft-latest-news">
 <a href="#">Board & Brush Mechanicsburg, PA is NOW OPEN!</a>
 <div class="news-date">March 24, 2018</div>
 </div>
 
  <div class="ft-latest-news">
 <a href="#">Board & Brush Mechanicsburg, PA is NOW OPEN!</a>
 <div class="news-date">March 24, 2018</div>
 </div>
 </div>

</div>
</div> 

<div class="copy-right">
<div class="copy-right-mid"> This website and all designs depicted on it are Copyrighted Board & Brush, LLC.  All Rights Reserved © 2016  |  Website Design by <a href="#">Matt Gerber Designs</a>  |  Studio Photography by Gretchen Bahr </div>
</div>




<div class="goto-top">
<a href="#" class="mk-go-top">
	<svg class="mk-svg-icon" data-name="mk-icon-chevron-up" data-cacheid="icon-5ad5bd90b9ef6" style=" height:16px; width: 16px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M1683 1331l-166 165q-19 19-45 19t-45-19l-531-531-531 531q-19 19-45 19t-45-19l-166-165q-19-19-19-45.5t19-45.5l742-741q19-19 45-19t45 19l742 741q19 19 19 45.5t-19 45.5z"></path></svg></a>
</div>
<!-- Footer section end -->



<!-- workshop modal window start -->


<!--<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>-->




<div class="pop-mob">
<div class="modal-overlay"></div>
<div class="modal">
<div class="workshop-modal">
<div class="workshop-modal-section">
<div class="modal-close"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><polygon fill="#d7d7d7" points="39.999,38.917 21.072,19.974 39.896,1.152 38.849,0.104 20.026,18.926 1.116,0 0.068,1.047 18.978,19.974 0,38.952 1.047,40 20.025,21.022 38.951,39.965 "/></svg></div>
<div class="modal-header">
<div class="modal-datetime">
<div class="modal-date"><p>January</p>31</div>
<div class="modal-day">
Thursday<br /> Hartland, Wisconsin
</div>
</div>
</div>

<div class="modal-date-box">
<img src="images/date-bg.jpg">
<div class="modal-date-content">
1pm - 4pm <br />Seats Available: 10/<small>30</small><br />
<span>Stephanie’s Birthday Party Workshop</span>
</div>


</div>

<div class="modal-date-box">
<img src="images/date-bg.jpg">

<div class="modal-date-content">
6pm - 9pm <br />Seats Available: 10/<small>24</small><br />
<span>Pick your Project Make any sign in our gallery!</span>
</div>

</div>
</div>
</div>
</div>
</div>


<script>
$(document).ready(function(){
$('.c-pop').click(function(){
$('.modal-overlay').fadeIn("fast");
$('.modal').fadeIn("fast");
});

$('.modal-close').click(function(){
$('.modal-overlay').fadeOut("fast");
$('.modal').fadeOut("fast");
});

//$('.modal-overlay').click(function(){
//$('.modal-overlay').fadeOut("fast");
//$('.modal').fadeOut("fast");
//});

});

</script> 



<!-- workshop modal window end -->






<script  type="text/javascript">
$(document).ready(function(){

	$('.goto-top').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 400);
    return false;
	});
	
	$(".goto-top").hide();
	

	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('.goto-top').fadeIn();
				
				
			} else {
				$('.goto-top').fadeOut();
			}
		});
		
		

	});
    });
	
</script>





<!--<a style="position:absolute; top:250px; right:50px; font-size:20px; text-decoration:none; border:1px solid blue; padding:10px;" href="choose-workshop-2.html">Next Page</a>-->
	

</body>
</html>
