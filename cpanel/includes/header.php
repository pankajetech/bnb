<?php
include("includes/functions.php");

if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>B&B</title>
<!--<link href="<?php //echo base_url_site?>css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php //echo base_url_site?>/css/font-awesome.min.css">
<script src="<?php //echo base_url_site?>js/jquery-1.12.4.js"></script>
<script src="<?php //echo base_url_site?>js/jquery.validate-1.14.0.min.js"></script>
<script src="<?php //echo base_url_site?>js/main.js"></script>-->


<link href="<?php echo base_url_css?>style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url_css?>chosen.css">
<link rel="stylesheet" href="<?php echo base_url_css?>font-awesome.min.css">
<script src="<?php echo base_url_js?>jquery-1.12.4.js"></script>
<script src="<?php echo base_url_js?>jquery.validate-1.14.0.min.js"></script>
<script src="<?php echo base_url_js?>main.js"></script>


<script>
$(document).ready(function(){

$(".rslogin").on('click',function(){
jQuery('.loading').show();
var stid=$(this).attr('id');

$.post("<?php echo base_url_site; ?>addon/ajaxReturnLogin.php", {studioId:stid}, function(response) {
	var data = JSON.parse(response);
	if(data.result==0) {
		jQuery('.loading').hide();
		window.location.href="<?php echo base_url_site; ?>studiolocs";
	}	
	 });


});

	$(".floatingBox").each(function(){
		if ($(this).children('input, textarea').val()) {
			$(this).children('.floating-lab').addClass('float');
		}else{
			$(this).children('.floating-lab').removeClass('float');
		}
	});
	
	$(document).on("focus",".floatingBox input",function() {
		$(this).siblings('.floating-lab').addClass('float');
	});
	
	$(document).on("blur",".floatingBox input",function() {
		if (!$(this).val()) {
			$(this).siblings('.floating-lab').removeClass('float');
		}
	});
	
	$(document).on("focus",".floatingBox textarea",function() {
		$(this).siblings('.floating-lab').addClass('float');
	});
	
	$(document).on("blur",".floatingBox textarea",function() {
		if (!$(this).val()) {
			$(this).siblings('.floating-lab').removeClass('float');
		}
	});
});
</script>
<style>
.rpBS {
    float: right;
    width: 80%;
    text-align: center;
    color: #ffe500;
    font-size: 16px;
    margin-top: 17px;
}

.rpBS a {
    text-decoration: none;
    color: #00c805;
}
</style>
</head>

<body>

<!-- Header start-->

<div class="header">
<div class="logo"><a href="#"><!--<img src="<?php //echo base_url_site?>images/boardandbrush-logo.png" />--> <img src="<?php echo base_url_images?>boardandbrush-logo.png" /></a></div>
<?php if(isset($_SESSION["curRole"]) && $_SESSION["curRole"]!="") {
if($_SESSION["curRole"]==1) $urole="Super";
if($_SESSION["curRole"]==2) $urole="Corp";
?>
<div class="rpBS">You are currently in Studio Admin view for studio <?php echo getNamebyPara("bb_studiolocation","stName","id",$_SESSION["studioIds"]);?>. <a href="javascript:void(0);" class="rslogin">Click here</a> to return to <?php echo $urole?> Admin view.</div>
<?php }
//print_r($_SESSION);

 if($_SESSION["curRole"]=="") {?>
<div class="username"><ul><li><a href="#"><?php if(isset($_SESSION['displayname'])){ ?><?php echo $_SESSION['displayname'] ?><?php }?> <i class="fa fa-angle-down"></i></a>

<ul>
<li><a href="<?php echo base_url_site?>editprofile">Edit Profile</a></li>
<li><a href="<?php echo base_url_site?>logout">Logout</a></li>
</ul>

</li></ul>
<?php }?>
</div>
</div>