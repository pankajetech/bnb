<?php
include('includes/header.php');
$menuClssBookingTemp = "downarrow";
$menuSbClssBookingTemp = "sub";
$menuSbClssStyleBookingTemp = "style='display:block;'";
//$menuClssBookingTemp3 = "active";

if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php"); exit; } 

/* strpos that takes an array of values to match against a string
 * note the stupid argument order (to match strpos)
 */
/*function strpos_arr_check($haystack, $needle) {
    $accommodList = preg_split('/\r\n|\r|\n/', $haystack);
	//echo "<pre>accommodList==";print_r($accommodList);
	
	$accommodList_str_separated = implode("!@#$,", $accommodList);
	//echo "<pre>accommodList_str_separated==";print_r($accommodList_str_separated);
	//$match = 'zip:';
	if(stripos($accommodList_str_separated, $needle) !== false) {
		$used1 = explode($needle,$accommodList_str_separated);
		//echo "<pre>used1==";print_r($used1);
		$used11 = explode('!@#$',$used1[1]);
		//echo "<pre>used11==";print_r($used11);
		$czip = $used11[0];
	} else  {
		$czip = '';
	}	
	return $czip;	
}*/

if(isset($_REQUEST["CR"]) and $_REQUEST["CR"]!="") {    
	$params = array('');
	$result = $db->rawQueryOne("SELECT * FROM bb_booking WHERE boEmail='".base64_decode($_REQUEST["CR"])."' and id='".$_REQUEST["bookingId"]."' and eventId <>0", $params);
	$ritems = (array)$result;
	//echo "<pre>ritems";print_r($ritems);die;
	if(!empty($ritems)) {
		
		$tansInfo = getBookingTransactioninfo($ritems['id']);
		//echo "<pre>tansInfo==";print_r($tansInfo);die;
		
		if($tansInfo['caddress'] != ''){
			$boAddress = $tansInfo['caddress'];
		} else {
			$boAddress = $ritems['boAddress'];
		}
		
		if($tansInfo['czip'] != ''){
			$czip = $tansInfo['czip'];
		} else {
			$czip = strpos_arr_check($ritems['boAccommodationsNotes'],'Zip:');
		}
		
		if($tansInfo['ccity'] != ''){
			$ccity = $tansInfo['ccity'];
		} else {
			$ccity = strpos_arr_check($ritems['boAccommodationsNotes'],'City:');
		}
		
		if($tansInfo['cstate'] != ''){
			$cstate = $tansInfo['cstate'];
		} else {
			$cstate = strpos_arr_check($ritems['boAccommodationsNotes'],'State:');
		}
	}
} else {
	$ritems = array();
	$ritems = $ritems;
}
?>
<!-- Header end-->
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<!-- left nagivation end-->
<!-- ToolTip Type Validation-->
<script type="text/javascript" src="<?php echo base_url_js?>additional-methods.js" /></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>bootstrap-3.3.5.min.css">
<script type="text/javascript" src="<?php echo base_url_js?>bootstrap-3.3.5.min.js" /></script>
<script type="text/javascript" src="<?php echo base_url_js?>jquery-validate.bootstrap-tooltip.min.js" /></script>
<script type="text/javascript" src="<?php echo base_url_js?>jquery.maskedinput-1.3.1.min_.js" /></script>
<script>
	var type = '<?php echo $_REQUEST["type"];?>';
	if(type == 'customer') {
		$('.customer').addClass("active");
	} 
	else if(type == 'needatten') {
		$('.needatten').addClass("active");
	} 
	else if(type == 'future') {
		$('.current').addClass("active");
	} 
	else if(type == 'today') {
		$('.today').addClass("active");
	} 
	else if(type == 'tomorrow') {
		$('.tomorrow').addClass("active");
	} 
	else if(type == 'past') {
		$('.past').addClass("active");
	} 
	else if(type == '') { //all boking list
		$('.allbooking').addClass("active");
	}
	</script>
<!-- left content area start-->
<div class="loading" style="display:none;">Loading&#8230;</div>

<!-- left nagivation end-->
<!-- left content area start-->
<div class="right-wrapper">
	<h1 class="pageheading"><?php echo fnMultiStudioTitle($_SESSION["stId"]);?>Customer Record</h1>
	<script>
	var type = '<?php echo $_REQUEST["type"];?>';
	if(type == 'customer') {
		$('.customer').addClass("active");
	} 
	else if(type == 'needatten') {
		$('.needatten').addClass("active");
	} 
	
	else if(type == 'current') {
		$('.current').addClass("active");
	} 
	else if(type == 'today') {
		$('.today').addClass("active");
	} 
	else if(type == 'tomorrow') {
		$('.tomorrow').addClass("active");
	} 
	else if(type == 'past') {
		$('.past').addClass("active");
	} 
	else if(type == '') { //all boking list
		$('.allbooking').addClass("active");
	}
	</script>
	<div class="form-area">
	<form role="form" method="post" action="" id="addUpdCustRecordFrm">
		<div class="bnt-row">
			<div class="btn-left">
				<button class="yellow-btn actionEvent" id="updateAction" type="submit">Update Changes</button>
			</div>
			<div class="btn-right"> 
				<button type="reset" id="btnCancel" class="grey-btn btn-mrg-right">Cancel</button>
				<button type="reset" id="btnCancel" class="grey-btn">Exit</button>
			</div>
		</div>
		
		<div class="template-info">
			<input type="hidden" name="actionType" value="updateAction" id="actionType">
			<input type="hidden" name="userId" value="<?php echo $_SESSION['usrid'];?>">
			<input type="hidden" name="bookingId" id="bookingId" value="<?php echo $ritems["id"];?>">
			<input type="hidden" name="pageType" id="pageType" value="<?php echo $_REQUEST["type"];?>">
			
			<div class="input-row">
				<div class="floatingBox width32"><input class="width32" type="text" placeholder="" name="boFullName" value="<?php echo fnCheckEmpty($ritems['boFullName']);?>" /><label class="floating-lab">Full Name</label></div>
				<div class="floatingBox width32" style="margin-right:0px;float:right;"><input class="width32" style="margin-right:0px; float:right;" type="text" placeholder="" name="boProjects" value="<?php echo fnCheckEmpty($ritems['boProjects']);?>" readonly /><label class="floating-lab">#Projects</label></div>
			</div>
			<div class="input-row">
            	<div class="floatingBox width32" style="float:left;"><input class="width32 tempname" type="text" placeholder="" name="boEmail" value="<?php echo fnCheckEmpty($ritems['boEmail']);?>" readonly/><label class="floating-lab">Email</label></div>
				<div class="floatingBox width32" style="float:left;"><input class="width32" type="text" placeholder="" id="boPhone" name="boPhone" value="<?php echo fnCheckEmpty($ritems['boPhone']);?>" /><label class="floating-lab">Phone</label></div>
			
			</div>

			<div class="input-row">
				<div class="floatingBox textfull">
					<!--<input class="textfull" type="text" placeholder="" name="boAddress" value="<?php //echo fnCheckEmpty($ritems['boAddress']);?>" />-->
					<input class="textfull" type="text" placeholder="" name="caddress" value="<?php echo $boAddress;?>"/>
					<label class="floating-lab">Billing Address</label>
				</div>
			</div>
			
			<div class="input-row">
				<div class="floatingBox width33"><input class="width33" type="text" placeholder="" name="ccity" id="ccity" value="<?php echo $ccity;?>"/><label class="floating-lab">City</label></div>
				<div class="floatingBox width33"><input class="width33" type="text" placeholder="" name="cstate" id="cstate" value="<?php echo $cstate;?>"/><label class="floating-lab">State</label></div>
				<div class="floatingBox width33" style="margin-right:0px;"><input class="width33" style="margin-right:0px;" type="text" placeholder="" name="czip" id="czip" value="<?php echo $czip;?>"/><label class="floating-lab">Zip</label></div>
			</div>
			
		</div>

		<div class="accommodaation">
			<h2 class="section-heading">Accommodation</h2>
			<div class="input-row">
				<div class="floatingBox"><input class="textfull" type="text" placeholder="" name="boAccommodations" value="<?php echo fnCheckEmpty($ritems['boAccommodations']);?>" /><label class="floating-lab">Accommodation</label></div>
			</div>
			<div class="input-row">
				<label class="note">Note</label>
				<textarea class="tarea90" name="boAccommodationsNotes"><?php echo fnCheckEmpty($ritems['boAccommodationsNotes']);?></textarea>
			</div>
		</div>
    </form>
		<div class="listing-wrapper">
		<h2 class="section-heading">Tickets</h2>
		<div class="listing-table">
		<table width="100%" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th>Event Date</th>
					<th>Event Name</th>
					<th># of Seats</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if(isset($_REQUEST["CR"]) and $_REQUEST["CR"]!="") {    
					$params = array('');
					$result4 = $db->rawQuery("SELECT * FROM bb_booking WHERE boEmail='".base64_decode($_REQUEST["CR"])."' and eventId !='' ", $params);
					$ritems4 = (array)$result4;
					//echo "<pre>ritems4=";print_r($ritems4);die;
					if(!empty($ritems4) && is_array($ritems4)) {
						foreach($ritems4 as $key4=>$val4){
							$boEventDate = $val4['boEventDate'];
							$boEventStTime = $val4['eventStTime'];
							$boEventName = $val4['boEventName'];
							$bobookingId = $val4['id'];
							$totalTickets = $val4['totalTickets'];
							$totalAmt = $val4['totalAmt'];
							
							//$result5 = $db->rawQueryOne("SELECT count(id) as totalSeats,sum(boTicketPrice) as totalPrice FROM bb_booktcktinfo WHERE bookingId='".$bobookingId."' ", $params);
							//$ritems5 = (array)$result5;
							//echo "<pre>ritems5=";print_r($ritems5);die;
						?>
						<tr>
							<td><?php echo $boEventDate." ".$boEventStTime;?></td>
							<td><?php echo $boEventName;?></td>
							<td><?php echo $totalTickets;?></td>
							<td><?php echo "$".$totalAmt;?></td>
						</tr>
					<?php
						}
					} 
					else { ?>
						<tr>
							<td colspan="4">No record found</td>
						</tr>
					<?php
					}
				}?>
			</tbody>
		</table>
		</div>
		</div>


		<div class="listing-wrapper">
		<h2 class="section-heading">Bookings & Transactions</h2>
		<div class="listing-table">
		<table width="100%" cellpadding="0" cellspacing="0">
			<thead>
			<tr>
				<th>Transaction ID</th>
				<th>Booking ID</th>
				<th>Amount</th>
				<th>Gateway</th>
				<th>Status</th>
			</tr>
			</thead>
			<tbody>
			<?php
			if(isset($_REQUEST["CR"]) and $_REQUEST["CR"]!="") {    
				$params = array('');
				$result1 = $db->rawQuery("SELECT transactionId,cOrderNo,id FROM bb_booking WHERE boEmail='".base64_decode($_REQUEST["CR"])."' ", $params);
				$ritems1 = (array)$result1;
				//echo "<pre>ritems1=";print_r($ritems1);die;
				if(!empty($ritems1) && is_array($ritems1)) {
					foreach($ritems1 as $key1=>$val1){
						$bookingId = $val1['id'];
						$cOrderNo = $val1['cOrderNo'];
						$result2 = $db->rawQuery("SELECT * FROM bb_transactiondetails WHERE bookingId='".$bookingId."' ", $params);
						$ritems2 = (array)$result2;
						//echo "<pre>ritems2=";print_r($ritems2);die;
						if(!empty($ritems2) && is_array($ritems2)) {
							foreach($ritems2 as $key2=>$val2){
							?>
							<tr>
								<td><?php if($val2['transactionId']!= ""){ echo $val2['transactionId'];} else { echo "NA";}?></td>
								<td><?php echo $val1['cOrderNo'];?></td>
								<td><?php echo "$".$val2['bookedAmount'];?></td>
								<td><?php echo getPaymentGateway($val2['bookingId']);?></td>
								<td><?php echo $val2['paymentStatus'];?></td>
							</tr>
							<?php
							}
						}
				    }
				} 
				else { 
				?>
					<tr>
						<td colspan="5">No record found</td>
					</tr>
				<?php
				}
			}
			?>
			</tbody>
		</table>
		</div>
		</div>


		<div class="listing-wrapper">
		<h2 class="section-heading">Promo Codes</h2>
		<div class="listing-table">
		<table width="100%" cellpadding="0" cellspacing="0">
			<thead>
			<tr>
			<th>Promo Name</th>
			<th>Promo Code</th>
			<th>Discount</th>
			<th>Uses</th>
			<th>Creator</th>
			</tr>
			</thead>
			<tbody>
			<?php
			if(isset($_REQUEST["CR"]) and $_REQUEST["CR"]!="") {    
				//echo "<pre>";print_r($_SESSION);
				$params = array('');
				$sql99 = "SELECT promocode,id FROM bb_booking WHERE boEmail='".base64_decode($_REQUEST["CR"])."' group by promocode";
				$result2 = $db->rawQuery($sql99, $params);
				$ritems2 = (array)$result2;
				//echo "<pre>ritems2=";print_r($ritems2);//die;
				if(!empty($ritems2) && is_array($ritems2)) {
					foreach($ritems2 as $key2=>$val2){
						$bookingId = $val2['id'];
						$promocode = $val2['promocode'];
						$sql555 = "SELECT * FROM bb_promocode WHERE promoCode='".$promocode."' and studioId='".$_SESSION['studioIds']."' " ;
						$result6 = $db->rawQuery($sql555,$params);
						$ritems6 = (array)$result6;
						//echo "<pre>ritems6=";print_r($ritems6);die; 
						if(!empty($ritems6) && is_array($ritems6)) {
							foreach($ritems6 as $key6=>$val6){
								//Get user name
								$result7 = $db->rawQuery("SELECT firstName FROM  bb_users WHERE id='".$val6['promoCreatedBy']."'", $params);
								$ritems7 = (array)$result7;
								if(isset($ritems7) && !empty($ritems7)) {
									$creator = $ritems7[0]['firstName'];
								}
						?>
							<tr>
								<td><?php echo $val6['promoName'];?></td>
								<td><?php echo $val6['promoCode'];?></td>
								<td><?php
									if(isset($val6['promoRDiscountVal']) && $val6['promoRDiscountVal']!=""){ 
										if($val6['promoRDiscountVal'] == '2'){ 
											echo $val6['promoRDiscountVal']."%";
										} else {
											echo "$".$val6['promoRDiscountVal'];
										}
									} else { 
										echo "NA";
									}
									?>
								</td>
								<td>
									<?php
									if(isset($val6['promoQty']) && $val6['promoQty']!=""){ 
										echo $val6['promoCodeUsed']."/".$val6['promoQty']; 
									}?>
								</td>
								<td>
								<?php if(isset($val6['promoCreatedBy']) && $val6['promoCreatedBy']!=""){ 
									echo $creator;
								}?>
								</td>
							</tr>
							<?php
							}
						}
				    }
				} 
				else { 
				?>
					<tr>
						<td colspan="5">No record found</td>
					</tr>
				<?php
				}
			}
			?>
			</tbody>
		</table>
		</div>
		</div>
	</div>
</div>
<!-- left content area end-->
<script>
$("#boPhone").mask("(999) 999-9999");
$(document).on('click','.actionEvent',function(){ 
	var actionEventId = $(this).attr('id');
	$("#actionType").val(actionEventId);
});

$(document).on("click","#btnCancel",function() { 
	var pageType = '<?php echo $_REQUEST['type'];?>';
	if(pageType != ''){
		var url = '<?php echo base_url_site;?>bookinglist?type='+pageType;
	} else {
		var url = '<?php echo base_url_site;?>bookinglist';
	}
	window.location = url;
});

<?php 
//When email is n request
if(isset($_REQUEST["CR"]) && $_REQUEST["CR"] != "" ) { ?>
	$('.tempname').attr('disabled', true);
<?php 
} ?>

$.validator.addMethod("zipcode", function(value, element) {
  return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
}, "Please provide a valid zipcode.");

$("form[id='addUpdCustRecordFrm']").validate({
	rules: {
	    boFullName: { required :true },
		//boPhone : { required: true, phoneUS: true },
		boPhone : { required: true},
		caddress: { required :true },
		ccity: { required :true },
		cstate: { required :true },
		czip: { required :true ,zipcode: true },
	},
	messages: {
		boFullName: { required : "Please enter customer name" },
		//boPhone : { required: "Please enter phone name", phoneUS: "Please enter valid phone name" },
		boPhone : { required: "Please enter phone name"},
		caddress: { required : "Please enter address" },
		ccity: { required : "Please enter city" },
		cstate: { required : "Please enter state" },
		czip: { required : "Please enter zip" },
	},
	submitHandler: function(form) { 
	    $('.loading').show(); 
		$.ajax({
			url : '<?php echo base_url_site; ?>addon/ajaxSaveCustomerRecordData',
			type : 'POST', 
			data :$('#addUpdCustRecordFrm').serialize(),
			success : function(response) {
				$('.loading').hide(); 
				var odata = $.parseJSON(response); 
				var pageType = odata.pageType;
				//alert('pageType=='+pageType);
				if(pageType == ''){
					window.location.href = "<?php echo base_url_site;?>bookinglist";
				} else {
					window.location.href = "<?php echo base_url_site;?>bookinglist?type="+pageType;
				}
			}
		});
	}
});

</script>
<?php
include('includes/footer.php');
?>