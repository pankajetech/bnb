<?php include('includes/header.php');
//header('Content-Type: text/html; charset=ISO-8859-1');

$menuClssEvents = "downarrow";
$menuSbClssEvents = "sub";
$menuSbClssAEvents = "active";
$menuSbClssStyleEvents = "style='display:block;'";

if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 

if(isset($_REQUEST["delid"]) and $_REQUEST["delid"]!="") { 
$dataD=array("isDeleted"=>1);
$db->where('id',$_REQUEST["delid"]);
$db->update('bb_eventtemplates',$dataD);

$_SESSION["msg"]="Successfully deleted event template";
header("location: ".base_url_site."eventtemplist");
exit;
}
	 ?>

<!-- Header end-->


<!-- left nagivation start-->

<?php include('includes/left-sidebar.php');?>
<link href="<?php echo base_url_css?>dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<!-- left nagivation end-->
<!-- left nagivation end-->

<!-- left content area start-->

<div class="right-wrapper">
<h1 class="pageheading">Event Templates
<?php 
if(isset($_REQUEST["estatus"]) && $_REQUEST["estatus"]!='') {
echo " - ".strtoupper($_REQUEST["estatus"]);
}
?>
</h1>
<div class="form-area">
<div class="listing-wrapper">
<div class="listing-table">
<?php

 if(isset($_SESSION['msg']) && $_SESSION['msg']!="") {?>
<script>
setInterval(function(){ $('.event-msg').hide(); }, 3000);
</script>
<div class="event-msg" style="display:block;"><?php echo $_SESSION['msg']?></div>
<?php }?>
<table width="100%" cellpadding="0" cellspacing="0" id="dataTables-example">
<thead>
<tr>
<th>Action</th>
<th>Name</th>
<th>Title</th>
<th>Type</th>
<th>Color</th>
<th>Scheduled Hrs</th>
<!--<th>Status</th>-->

</tr>
</thead>
<tbody>
<?php
$stwhre="";
if(isset($_REQUEST["estatus"]) && $_REQUEST["estatus"]=='archive') {
$stwhre.= " and evisArchive=1 ";
} else if(isset($_REQUEST["estatus"]) && $_REQUEST["estatus"]=='draft') {
$stwhre.= " and evisStatus=1 ";
} else {
$stwhre.= " and evisStatus=0 and  evisArchive=0 ";
}

$params = array('');
$evlist = $db->rawQuery("SELECT * FROM `bb_eventtemplates` where isDeleted=0 $stwhre order by id desc", $params);
$i=1;
if(isset($evlist) && !empty($evlist)) {

foreach ($evlist as $key=>$item) {

if(isset($item['id'])) { $evntid=$item['id']; } else { $evntid=$item['id']; }

if(isset($_SESSION['urole']) && ( $_SESSION['urole']==1 || $_SESSION['urole']==2 ) ) {
		$delstr='/ <a href="javascript:void(0);" class="del" id="'.$evntid.'">Delete</a>';
} else {
$delstr='';
}
/*if($item['evisStatus']=="0") {
$status='Active';
} else {
$status='InActive';
}*/

/*$stIsStudioLive='';
if($item['stIsStudioLive']==1) {
$stIsStudioLive='<a href="javascript:void(0);" class="clsLogin" id="'.$studid.'" data-status="1" alt="Click for In-Active" title="Click for In-Active"><img src="'.base_url_site.'/images/green-icon.png" height="10" width="10"></a>';
} else {
$stIsStudioLive='<a href="javascript:void(0);" class="clsLogin" id="'.$userid.'" data-status="0" alt="Click for Active" title="Click for Active"><img src="'.base_url_site.'/images/red-icon.png" height="10" width="10"></a>';
}*/




//<a href="viewuser.php?id='.$userid.'">View</a> /
if($i%2==0) {
echo '<tr class="even gradeC"><td class="center"> <a href="addupdeventtemp.php?evid='.$evntid.'">Edit</a> '.$delstr.'</td><td>'.checkSpace($item['evTempName']).'</td><td>'.checkSpace($item['evTempTitle']).'</td><td>'.getNamebyPara("bb_eventtype","etName","id",$item['evType']).'</td><td>'.getNamebyPara("bb_calcolor","caName","caCode",$item['evCalColor']).'</td><td>'.$item['evScheduleHrs'].'</td> </tr>';
} else {
echo '<tr class="odd gradeX"><td class="center"> <a href="addupdeventtemp.php?evid='.$evntid.'">Edit</a> '.$delstr.'</td><td>'.checkSpace($item['evTempName']).'</td><td>'.checkSpace($item['evTempTitle']).'</td><td>'.getNamebyPara("bb_eventtype","etName","id",$item['evType']).'</td><td>'.getNamebyPara("bb_calcolor","caName","caCode",$item['evCalColor']).'</td><td>'.$item['evScheduleHrs'].'</td> </tr>';
}
$i++;
}
}
?>   

</tbody>
</table>
</div>

</div>
</div>
</div>

<!-- left content area end-->
<script src="<?php echo base_url_css?>dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url_css?>dataTables/dataTables.bootstrap.js"></script>
<script>
 var type = '<?php echo $_REQUEST["estatus"];?>';
 if(type == 'draft') {
  $('.draft').addClass("active");
 } else if(type == 'archive') {
  $('.archive').addClass("active");
 } else if(type == '') {
  $('.alleventtemp').addClass("active");
 }
 
 $(document).on("click",".del",function() {
var delid=$(this).attr('id');

var dlg='<p>Would you like to delete event template</p>';

doConfirm(dlg, function yes() {
						window.location.href="<?php echo base_url_site?>eventtemplist?delid="+delid;
        }, function no() {
           
        });
		


});
 
$(document).ready(function() {
vtable = $('#dataTables-example').DataTable( {
dom: '<"elm-row"<"bubbleInfo"<"fl spage"<"paging"pl><"clear">><"fr page-text"i>>>',
pagingType: "full_numbers",
sortable: false,
paginate: true,
pageLength: 50,
info: true,
bSort: true,
bFilter: false,
"aaSorting": [],
"aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0 ] } ]
} );



});
</script>

<?php
include('includes/footer.php');
?>