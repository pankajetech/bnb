<?php 
session_start();
include("../includes/config_inc.php");
define('base_url_site1','https://'.$_SERVER['HTTP_HOST'].'/cpanel/migration-script/');
include('../includes/header.php'); ini_set('max_execution_time', 30000);

if(isset($_SESSION['urole']) && $_SESSION['urole'] != 1 ) { 
	header("location: ".base_url_site."dashboard"); exit;
}
?>
<script type="text/javascript">
var specialKeys = new Array();
specialKeys.push(8); //Backspace
function IsNumeric(e) {
	var keyCode = e.which ? e.which : e.keyCode
	var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
	document.getElementById("error").style.display = ret ? "none" : "inline";
	return ret;
}
</script>
<style type="text/css">
body {font-size: 9pt;font-family: Arial;}
</style>
	
</br></br></br></br></br></br></br>	
<?php
echo '<a href="'.base_url_site1.'studiolocs">Home</a>';
?>
</br></br>
<h1>Booking Update Transaction Script With state,City,Billing address </h1>
</br></br>	
<?php 
//Code of Booking Transaction Script
if (isset($_REQUEST['studioID']) and $_REQUEST['studioID']!= "" ){
	global $db;
	$buserdata = $db->rawQueryOne("select max(booking_id) as totals from bnbwp_testlive2.board_".$_REQUEST['studioID']."_em_bookings order by booking_id ", array(''));
	if(isset($buserdata) && !empty($buserdata)) {
		$totalbooking = $buserdata['totals'];
	}
	
	if((isset($_REQUEST["slimit"]) && $_REQUEST["slimit"]!="") && (isset($_REQUEST["elimit"]) && $_REQUEST["elimit"]!="")) {
		$slimit=$_REQUEST["slimit"];
		$elimit=$_REQUEST["elimit"];
	} else {
		$data = array ("isupdated" => 0);
		$db->where ('isupdated', 1);
		$db->update ('bb_transactiondetails', $data);
		$slimit=1;
		$elimit=1500;
		echo '<h2>Updating booking transaction script With state,City,Billing address </h2>';
	}
	if($slimit<=$totalbooking) {
		$sdql = "select bt.booking_id,bt.booking_meta,
		    b.ClBkId,b.id,b.status
			from `bb_booking` b 
			inner join bnbwp_testlive2.board_".$_REQUEST['studioID']."_em_bookings bt
			on b.ClBkId = bt.booking_id 
			where  b.ClientDbTbl ='board_".$_REQUEST['studioID']."'
			and (b.ClBkId>=$slimit and b.ClBkId<=$elimit)
			order by b.ClBkId ASC"; 
			
		$bookingup = $db->rawQuery($sdql,array(''));
		//echo count($bookingup);
		//echo "<pre>bookingup==";print_r($bookingup);
		if(isset($bookingup) && !empty($bookingup)) {
			$data2=array();
			foreach($bookingup as $k=>$v) {	
				$data = @unserialize($v['booking_meta']);
				if($data == false)	{
					//echo "</br>"; echo "FAILED here: ".$row['booking_id']; echo "</br>";
					$my_data =$v['booking_meta'];
					$fixed_serialized_data = preg_replace_callback ('!s:(\d+):"(.*?)";!',function($match) {
					return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";'; }, $my_data );
					$data = @unserialize($fixed_serialized_data );
					if($data == false) {
						$data=  @unserialize($my_data, ['allowed_classes' => false]);
						if($data == false) {
							//echo "</br>"; echo "FAILED RETURN here: ".$row['booking_id']; echo "</br>";
						}
					}
				}
				//$booking_id=$v['booking_id'];
				$booking_id = $v['id'];
				
				if(isset($data['registration']['dbem_city'])){
					$dbem_cityData = $data['registration']['dbem_city'];
					$dbem_city = str_replace("'", '', $dbem_cityData); 
				} else { $dbem_city = ""; }

				if(isset($data['registration']['dbem_state'])) {
					$dbem_stateData = $data['registration']['dbem_state'];
					$dbem_state = str_replace("'", '', $dbem_stateData); 
				} else { $dbem_state=""; }

				if(isset($data['registration']['dbem_zip'])) {
					$dbem_zip = $data['registration']['dbem_zip'];
				} else { $dbem_zip=""; }

				//echo  $booking_id."===".$dbem_city."===".$dbem_state."===".$dbem_zip; die;
				$data2 = array (
					"ccity" => $dbem_city,
					"cstate" => $dbem_state,
					"czip" => $dbem_zip,
					"isupdated" => 1);
				$db->where ('bookingId', $booking_id);
				$db->where ('ClientDbTbl', 'board_'.$_REQUEST['studioID']);
				$db->update ('bb_transactiondetails', $data2);
				
				/*echo "Last executed query was ". $db->getLastQuery();
				print_r($data2);
				die();*/
			    // bookin status != completed ten cne bookin status = need atention === ceck in stuidio 15 bartland
				$sdql1 = "select bbt.bookingId,bbt.paymentStatus
				from `bb_transactiondetails` bbt 
				where  `paymentStatus` != 'Completed' and bbt.ClientDbTbl ='board_".$_REQUEST['studioID']."'
				and bbt.bookingId = '".$booking_id."' "; 
				
				$bookingup1 = $db->rawQuery($sdql1,array(''));
				//echo count($bookingup1);
				//echo "<pre>bookingup1==";print_r($bookingup1);
				if(isset($bookingup1) && !empty($bookingup1)) {
					$data55 = array ("status" => 3); //Need attention
					$db->where ('id', $booking_id);
					$db->where ('ClientDbTbl', 'board_'.$_REQUEST['studioID']);
					$db->update ('bb_booking', $data55);
				}
			}
		}
		header("location:".base_url_site1."bookinguptransaction_step2.php?studioID=".$_REQUEST['studioID']."&slimit=".$elimit."&elimit=".($elimit+1500));	
		exit;
}	

if($elimit>$totalbooking) { 
	echo '<h2 style="color:#009933;">Updated booking transaction data with address,city state </h2>';
    header("location:".base_url_site1."booking.php?msg=usuccess");	
	exit;
}
/*echo ($elimit+1500);
die();*/
}	
?>	