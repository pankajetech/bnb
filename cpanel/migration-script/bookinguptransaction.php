<?php 
session_start();
include("../includes/config_inc.php");
define('base_url_site1','https://'.$_SERVER['HTTP_HOST'].'/cpanel/migration-script/');
include('../includes/header.php'); ini_set('max_execution_time', 30000);

if(isset($_SESSION['urole']) && $_SESSION['urole'] != 1 ) { 
	header("location: ".base_url_site."dashboard"); exit;
}

//$mysqli = new mysqli("localhost", "root", "", "boardnbrushdblatest");
$mysqli = new mysqli("localhost", "root", "Dev@123ET", "boardnbrushdb");
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}
?>

<script type="text/javascript">
var specialKeys = new Array();
specialKeys.push(8); //Backspace
function IsNumeric(e) {
	var keyCode = e.which ? e.which : e.keyCode
	var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
	document.getElementById("error").style.display = ret ? "none" : "inline";
	return ret;
}
</script>
<style type="text/css">
body { font-size: 9pt;font-family: Arial;}
</style>
</br></br></br></br></br></br></br>	
<?php
echo '<a href="'.base_url_site1.'studiolocs">Home</a>';
?>
</br></br>
<h1>Booking Transaction Migration Script</h1>
</br></br>	
<?php 
if(isset($_REQUEST['studioID']) and ($_REQUEST['studioID']) != ""  ) {
	$UpdateBooking = "
		INSERT INTO bb_transactiondetails (
			orderType,
			bookingId,
			cardholderName,
			caddress,
			cphone,
			cemail,
			bookedAmount,
			paymentStatus,
			bookDate,
			ClientDbTbl,
			transactionId,
			ptype         
		) SELECT
		1,
		b.id,
		b.boFullName, 
		b.boAddress,
		b.boPhone,
		b.boEmail,
		bt.transaction_total_amount,
		bt.transaction_status,
		bt.transaction_timestamp,
		'board_".$_REQUEST['studioID']."',
		bt.transaction_id,
		CASE transaction_gateway 
			WHEN 'authorize_aim' THEN 0 
			WHEN 'offline' THEN 2 
			WHEN 'paypal' THEN 3 
			END as transaction_gateway
		FROM `bb_booking` b 
		inner join bnbwp_testlive2.board_".$_REQUEST['studioID']."_em_transactions bt 
		on b.ClBkId=bt.booking_id 
		WHERE `ClientDbTbl`='board_".$_REQUEST['studioID']."' ";
		
	/*echo $UpdateBooking;
	die();*/
	if (mysqli_query($mysqli, $UpdateBooking)) {
		//echo $UpdateBooking;
		//header("location:".base_url_site."booking.php?msg=success&?tmsg=success");	
		//exit;
	} else { echo "Error Inserted record: " . mysqli_error($mysqli);}
} 
?>	