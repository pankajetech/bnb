<?php 
session_start();
include("../includes/config_inc.php");
define('base_url_site1','https://'.$_SERVER['HTTP_HOST'].'/cpanel/migration-script/');
include('../includes/header.php'); ini_set('max_execution_time', 30000);

if(isset($_SESSION['urole']) && $_SESSION['urole'] != 1 ) { 

header("location: ".base_url_site."dashboard"); exit;
}

?>
<script type="text/javascript">
var specialKeys = new Array();
specialKeys.push(8); //Backspace
function IsNumeric(e) {
	var keyCode = e.which ? e.which : e.keyCode
	var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
	document.getElementById("error").style.display = ret ? "none" : "inline";
	return ret;
}
</script>
<style type="text/css">
body { font-size: 9pt; font-family: Arial;}
</style>
</br></br></br></br></br></br></br>	
<?php
echo '<a href="'.base_url_site1.'studiolocs">Home</a>';
?>
</br></br>
<?php
echo 'Go to  <a href="'.base_url_site1.'events.php">Events Migration Script</a>';
?>
</br></br>
<?php
echo 'Go to  <a href="'.base_url_site1.'imageProcessing.php">Images Migration Script</a> after event migration.';
?>
</br></br>
<h1>Booking Migration Script</h1>
</br></br>	
<?php 
// Code of personal information
if (isset($_REQUEST['studioID']) and $_REQUEST['studioID']!= "" ){
	global $db;
	$buserdata = $db->rawQueryOne("select max(booking_id) as totals from bnbwp_testlive2.board_".$_REQUEST['studioID']."_em_bookings order by booking_id ", array(''));
	if(isset($buserdata) && !empty($buserdata)) {
		$totalbooking=$buserdata['totals'];
	}

	if((isset($_REQUEST["slimit"]) && $_REQUEST["slimit"]!="") && (isset($_REQUEST["elimit"]) && $_REQUEST["elimit"]!="")) {
		$slimit=$_REQUEST["slimit"];
		$elimit=$_REQUEST["elimit"];
	} else {
		$data = array ("isupdated" => 0);
		$db->where ('isupdated', 1);
		$db->update ('bb_booking', $data);
		$slimit=1;
		$elimit=1500;
		echo '<h2>Updating booking user data is in progress </h2>';
	}
	if($slimit<=$totalbooking) {
		//echo "select booking_id,booking_meta from bnbwp_testlive2.board_".$_REQUEST['studioID']."_em_bookings  where  (booking_id>$slimit and booking_id<=$elimit) order by booking_id ASC ";
		$bookingup = $db->rawQuery("select booking_id,booking_meta from bnbwp_testlive2.board_".$_REQUEST['studioID']."_em_bookings  where  (booking_id>=$slimit and booking_id<=$elimit)   order by booking_id ASC ", array(''));
		if(isset($bookingup) && !empty($bookingup)) {
			$data2=array();
			$data=array();
			foreach($bookingup as $k=>$v) {	
				$data = @unserialize($v['booking_meta']);
				if($data == false)	{
					//echo "</br>"; echo "FAILED here: ".$row['booking_id']; echo "</br>";
					$my_data =$v['booking_meta'];
					$fixed_serialized_data = preg_replace_callback ('!s:(\d+):"(.*?)";!',function($match) {
					return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";'; }, $my_data );
					$data = @unserialize($fixed_serialized_data );
					if($data == false) {
						//echo "</br>"; echo "FAILED AGAIN here: ".$row['booking_id']; echo "</br>";
						$data=  @unserialize($my_data, ['allowed_classes' => false]);
						if($data == false) {
							//echo "</br>"; echo "FAILED RETURN here: ".$row['booking_id']; echo "</br>";
						}
					}
				}
		
		
				if(isset($data['coupon'])) {
					$coupon_id=$data['coupon']['coupon_id'];
					$coupon_code=$data['coupon']['coupon_code'];
				} else {
					$coupon_id="";
					$coupon_code="";
				}
					
				$booking_id=$v['booking_id'];
				$accomdata="";
				if(isset($data['registration']['user_name'])) {
					$user_nameData=$data['registration']['user_name'];
					$user_name = str_replace("'", ' ', $user_nameData); 
				} else { $user_name=""; }

				if(isset($data['registration']['user_email'])){
					$user_email=$data['registration']['user_email'];
				} else { $user_email=""; }

				if(isset($data['registration']['dbem_address'])){ 				
					$dbem_addressData =$data['registration']['dbem_address'];
					$dbem_address = str_replace("'", '', $dbem_addressData); 
				} else { $dbem_address=""; }

				if(isset($data['registration']['dbem_city'])){
					$dbem_cityData=$data['registration']['dbem_city'];
					$dbem_city = str_replace("'", '', $dbem_cityData); 
					$dbem_city = "City: ".$dbem_city."\n"; 
				} else { $dbem_city=""; }

				if(isset($data['registration']['dbem_state'])) {
					$dbem_stateData=$data['registration']['dbem_state'];
					$dbem_state = str_replace("'", '', $dbem_stateData); 
					$dbem_state = "State: ".$dbem_state."\n"; 
				} else { $dbem_state=""; }

				if(isset($data['registration']['dbem_zip'])) {
					$dbem_zip=$data['registration']['dbem_zip'];
					$dbem_zip="Zip: ".$dbem_zip."\n"; 
				} else { $dbem_zip=""; }

				if(isset($data['registration']['dbem_country'])) {
					$dbem_countryData=$data['registration']['dbem_country'];
					$dbem_country = str_replace("'", '', $dbem_countryData); 
					$dbem_country = "Country:".$dbem_country."\n";
				} else { $dbem_country="";}

				if(isset($data['registration']['dbem_phone'])) 	{
					$dbem_phone=format_phone('us', $data['registration']['dbem_phone']);
				} else  { $dbem_phone=""; }

				if(isset($data['registration']['dbem_fax'])) {
					$dbem_fax=$data['registration']['dbem_fax'];
					$dbem_fax="Fax: ".$dbem_fax."\n";
				} else { $dbem_fax=""; }

				if(isset($data['booking']['type_your_sign_choice_here__from_above_'])) {
					$sign_choicenData= $data['booking']['type_your_sign_choice_here__from_above_'];
					$sign_choice = str_replace("'", '', $sign_choicenData); 
					if(strlen($sign_choice)>4){
						$sign_choice = "Sign Choice: ".$sign_choice."\n";
					}
				} else { $sign_choice=""; }
					
					
				/*if(isset($data['booking']['personalization'])) {
					$personalizationData= $data['booking']['personalization'];
					$personalization = str_replace("'", '', $personalizationData); 
					if(strlen($personalization)>4){
						$personalization = "Personalization: ".$personalization."\n";
					}
				} else { $personalization=""; }*/
				
				if(isset($data['booking']['personalization'])) { 
					$personalizationData= $data['booking']['personalization'];
					$personalization1 = str_replace("'", '', $personalizationData); 
					if(strlen($personalization1)>4){
						$personalization = "Personalization: ".$personalization1."\n";
					}
				} else { $personalization=""; }
					
				if(isset($data['booking']['booking_comment'])) { 
					$booking_commentData=$data['booking']['booking_comment'];
					$booking_comment = str_replace("'", '', $booking_commentData); 
					$booking_comment = "Booking Comment: ".$booking_comment."\n";
				} else { $booking_comment=""; }
						

				if(isset($data['gateway'])) {
					$gatewayData=$data['gateway'];
					$gateway= str_replace("'", '', $gatewayData); 
					$gateway = "Gateway: ".$gateway."\n";
				} else { $gateway=""; }

				//coupon section
				//$accomdata= $dbem_state."/".$dbem_zip."/". $dbem_country."/". $dbem_fax."/".$booking_comment."/".$gateway."/".$personalization;
				//$accomdata= $dbem_state.$dbem_zip.$dbem_country.$dbem_fax.$booking_comment.$gateway.$personalization;
				//$accomdata= $dbem_city.$dbem_state.$dbem_country.$dbem_zip.$dbem_fax.$booking_comment.$gateway.$sign_choice.$personalization;
				
				/*https://trello.com/c/wYICEhTE/84-wordpress-db-schema
				Order of fields in accommodation notes should be: Sign Choice, Personalization, Comment
				*/
				$accomdata = $sign_choice.$personalization.$booking_comment.$dbem_city.$dbem_state.$dbem_country.$dbem_zip.$dbem_fax.$gateway;
				$data2 = array ("cOrderNo" => $booking_id,"boFullName" => $user_name,"boPhone" => $dbem_phone,"boEmail" => $user_email,"boAddress" => $dbem_address,"boAccommodationsNotes" => $accomdata,"ClCuId" => $coupon_id,"promocode" => $coupon_code,"isupdated" => 1);
				
				$db->where ('ClBkId', $booking_id);
				$db->where ('ClientDbTbl', 'board_'.$_REQUEST['studioID']);
				$db->update ('bb_booking', $data2);
			
				/*echo "Last executed query was ". $db->getLastQuery();
				print_r($data2);
				die();*/
				///	print_r($data);
				//die();
			}
		}
		header("location:".base_url_site1."bookingupdateuserdata.php?studioID=".$_REQUEST['studioID']."&slimit=".$elimit."&elimit=".($elimit+1500));	
		exit;
	}	

	if($elimit>$totalbooking) { 
		echo '<h2 style="color:#009933;">Updated booking user data successfully </h2>';
		header("location:".base_url_site1."booking.php?msg=usuccess");	
		exit;
	}

	/*echo ($elimit+1500);
	die();*/
}	

function format_phone($country, $phone) {
  $function = 'format_phone_' . $country;
  if(function_exists($function)) {
    return $function($phone);
  }
  return $phone;
}
 
function format_phone_us($phone) {
  // note: making sure we have something
  if(!isset($phone{3})) { return ''; }
  // note: strip out everything but numbers 
  $phone = preg_replace("/[^0-9]/", "", $phone);
  $length = strlen($phone);
  switch($length) {
  case 7:
    return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
  break;
  case 10:
   return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
  break;
  case 11:
  return preg_replace("/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/", "$1($2) $3-$4", $phone);
  break;
  default:
    return $phone;
  break;
  }
}	
?>