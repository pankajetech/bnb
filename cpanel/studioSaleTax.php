<?php 
include('includes/header.php');
/*$menuClssFinePrint = "downarrow";
$menuSbClssFinePrint = "sub";
$menuSbClssStyleFinePrint = "style='display:block;'";
$menuClssFinePrint2 = "active";*/

$menuClssSetting = "downarrow";
$menuSbClssSetting = "sub";
$menuSbClssStyleSetting = "style='display:block;'";
$menuClssSetting2 = "active";


if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 
?>
<!-- Header end-->
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<!-- ToolTip Type Validation-->
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>bootstrap-3.3.5.min.css">
<script type="text/javascript" src="<?php echo base_url_js?>bootstrap-3.3.5.min.js" /></script>
<script type="text/javascript" src="<?php echo base_url_js?>jquery-validate.bootstrap-tooltip.min.js" /></script>

<style>
.dyanmicRadio input[type="checkbox"]{margin-right:15px; width:auto;}
.dynamicBox input[type="text"]{width:35%; margin-right:15px; margin-top:15px; float:none;}
.dynamicBox button{background-color: #6fcf00; font-size: 16px; color: #fff; border: none; border-radius: 5px; padding: 8px 17px;}
.dynamicBox button:focus {background-color: #62b700; transform: translateY(2px); }
.dynamicBox button:hover{background-color: #62b700;}
.studiochr-msg{color:#6fcf00; font-weight:bold; text-align:center;margin-bottom: 20px;}
</style>

<!-- left nagivation end-->
<!-- left content area start-->
<div class="right-wrapper">
	<h1 class="pageheading"><?php echo fnMultiStudioTitle($_SESSION["stId"]);?>Studio Charges Sales Tax</h1>
	<?php
	if(isset($_SESSION['msg']) && $_SESSION['msg']!="") {?>
		<script>
		setInterval(function(){ $('.studiochr-msg').hide(); }, 3000);
		</script>
		<div class="studiochr-msg" style="display:block;"><?php echo $_SESSION['msg'];?></div>
	<?php }?>
	<?php //echo "<pre>";print_r($_SESSION);?>
	<?php
	$params = array('');
	if($_SESSION["urole"] == 1 || $_SESSION["urole"] == 2){
		$sql = "SELECT * FROM `bb_taxstudio` where studioLocId IN(".$_SESSION["stidloc"].")";
	}
	if($_SESSION["urole"] == 3){
	if(isset($_SESSION["stId"]) && !empty($_SESSION["stId"])) {
     $locId=$_SESSION["stId"];
     } else {
     $locId=$_SESSION["studioIds"];
     }
		$sql = "SELECT * FROM `bb_taxstudio` where studioLocId IN(".$locId.") "; 
		//echo $sql;
		
	}
	$stdTax = $db->rawQueryone($sql,$params);
	if(isset($stdTax) && !empty($stdTax)) {
		//echo "<pre>stdTax==";print_R($stdTax);die;
		$saleTax = $stdTax["saleTax"];
		$status = $stdTax["status"];
		$studioLocId = $stdTax["studioLocId"];
	} ?>	
	<div class="form-area">
		<div class="event-listing-module">
			<div class="elm-control">
				<div class="pfp-outer">
					<form method="post" action="" role="form" id="studioTaxForm">
					    <?php
						if( isset($stdTax) && !empty($stdTax) ) { //update action?>
							<div class="dyanmicRadio">
								<input class="tempname saleTaxRateChkbox" type="checkbox" id="stdCharges" <?php if($status== "0") {echo "checked";}?>>
								Sales Tax Rate
							</div>
							<div class="dynamicBox">
								<span id="dollar_sign" style="inline-block;">%</span>
								<input name="taxAmount" id="taxAmount" maxlength="5"  class="tempname" type="text" value="<?php echo $saleTax ;?>">
								<input name="status" id="status" type="hidden" value="<?php echo $status;?>">
								<?php if($_SESSION["urole"] == 3 && !isset($_SESSION["curRole"])){ ?>
								<button type="button" class="pfp-save dsbl-btn" >Save</button>
								<?php } else { ?>
								<button type="submit" class="pfp-save" >Save</button>
								<?php }?>
							</div>
							<input name="updateaction" type="hidden" value="updateaction">
						<?php
						} 
						else { //add action ?>
							<div class="dyanmicRadio">
								<input type="checkbox" id="stdCharges">Studio Charges Sales Tax
							</div>
							<div class="dynamicBox">
								<span id="dollar_sign">%</span>
								<input name="taxAmount" id="taxAmount" maxlength="5"  type="text" value="">
								<?php if($_SESSION["urole"] == 3 && !isset($_SESSION["curRole"])){ ?>
								<button type="button" class="pfp-save dsbl-btn" >Save</button>
								<?php } else { ?>
								<button type="submit" class="pfp-save" >Save</button>
								<?php }?>
							</div> 
							<input name="addaction" type="hidden" value="addaction">
							<input name="status" id="status" type="hidden" value="0">
						<?php 
						}?>
						<input name="studioLocId" type="hidden" value="<?php echo $locId ;?>">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
<?php 
//user role = studio
if(isset($_SESSION) && $_SESSION["urole"] == "3" && !isset($_SESSION["curRole"])) { ?>
	$('.tempname').attr('disabled', true);
<?php 
} else { // Add Case ?>
	$('.tempname').attr('disabled', false);
<?php }?>

var saleTax = "<?php echo $saleTax ;?>";
//alert(saleTax);

if(saleTax == "" || saleTax == "0.00") {
	$('#taxAmount').hide();
	$('.pfp-save').hide();
	$('#dollar_sign').css("display","none");
} else {
	$('#taxAmount').show();
	$('#dollar_sign').css("display","inline-block");
	$('.pfp-save').show();
}

//Studio sales tax rate checkbox unchecked at edit page
var studioLocIds = "<?php echo $studioLocId ;?>";
$('.saleTaxRateChkbox').change(function() {
	if(!this.checked && saleTax != '') { 
		$.ajax({
			url : '<?php echo base_url_site; ?>addon/ajaxDelStudioTax',
			type : 'POST', 
			data :{studioLocIds: studioLocIds},
			success : function(response) {
				$('.loading').hide(); 
				var odata = $.parseJSON(response); 
				window.location.href = "<?php echo base_url_site;?>studioSaleTax";
			}
		});	
	}	
});

$('#stdCharges').change(function() {
	if(this.checked) {  
		$('#taxAmount').show();
		$('#dollar_sign').css("display","inline-block");
		$('.pfp-save').show();
		$('#status').val(0);
	} else { 
	    $('#taxAmount').hide();
		$('#dollar_sign').css("display","none");
		$('.pfp-save').hide();
		$('#taxAmount').val("");
		$('#status').val(1);
	}
});

<?php
//user role = studio
if(isset($_SESSION) && $_SESSION["urole"] == "3" && !isset($_SESSION["curRole"])) { ?>
	$('.pfp-save').hide();
	$('#stdCharges').addClass('tempname');
	$('.tempname').attr('disabled', true);
<?php }?>

$.validator.addMethod('decimal', function(value, element) {
  return this.optional(element) || /^((\d+(\\.\d{0,2})?)|((\d*(\.\d{1,2}))))$/.test(value);
}, "Please enter a sale tax, format 0.00");

$("form[id='studioTaxForm']").validate({
	rules: {
		taxAmount:{
			required: function(element){
				if( $("input#stdCharges:checked").length > 0 ) {
					return true;
				} else {
					return false;
				}
			},
			//digits: true,
			//maxlength:2
			decimal: true
		},
	},
	messages: {
		taxAmount: { required: "Enter Sales Tax percentage" },
	},
	submitHandler: function(form) {
	    $('.loading').show();
		$('.studiochr-msg').html("");
		$.ajax({
			url : '<?php echo base_url_site; ?>addon/ajaxSaveStudioTax',
			type : 'POST', 
			data :$('#studioTaxForm').serialize(),
			success : function(response) {
				$('.loading').hide(); 
				var odata = $.parseJSON(response); 
				window.location.href = "<?php echo base_url_site;?>studioSaleTax";
			}
		});	
	}
});
</script>
<!-- left content area end-->
<?php
include('includes/footer.php');
?>