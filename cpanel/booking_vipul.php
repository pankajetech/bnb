<?php include('includes/header.php'); ini_set('max_execution_time', 30000); 
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 

if(isset($_SESSION['urole']) && $_SESSION['urole'] != 1 ) { 
header("location: ".base_url_site."dashboard"); exit;
}

//$mysqli = new mysqli("localhost", "root", "", "boardnbrushdblatest");
$mysqli = new mysqli("localhost", "root", "Dev@123ET", "boardnbrushdb");
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}

?>

 <script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        function IsNumeric(e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            document.getElementById("error").style.display = ret ? "none" : "inline";
            return ret;
        }
    </script>
    <style type="text/css">
        body
        {
            font-size: 9pt;
            font-family: Arial;
        }
    </style>
	
</br></br></br></br></br></br></br>	
<?php
echo '<a href="'.base_url_site.'studiolocs">Home</a>';
?>
</br></br>
<?php
echo 'Go to  <a href="'.base_url_site.'events.php">Events Migration Script</a>';
?>
</br></br>


<?php
echo 'Go to  <a href="'.base_url_site.'imageProcessing.php">Images Migration Script</a> after event migration.';
?>
</br></br>


		<h1>Booking Migration Script</h1>
	
	</br></br>	

    <h2>Migrate Location ID </h2>
    <form action="" method="POST"> Location ID:
        <input type="text" name="studioID" id="studioID" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"  > 
		
        <input type="hidden" name="form_submitted" id="form_submitted" value="1" />
        <input type="submit" value="Submit">
		<span id="error" style="color: Red; display: none">* Input digits (0 - 9)</span>
    </form>


<?php 

if(isset($_REQUEST['form_submitted']) and ($_REQUEST['form_submitted']) == 1  ) {
	
	$id = $_POST['studioID'];
	$search_location = "SELECT id FROM bb_studiolocation where id=".$id;		
	$result = $mysqli->query($search_location);
	
	if($result->num_rows > 0) {
		
		$booking = "
		INSERT INTO bb_booking (
            totalAmt,
            status,
            tax,
			eventLocationId,

            boProjects,
            totalTickets,

            bookingCreationDate,
            bookingUpdationDate,

            ClBkId,
            CleintEVid,
            ClientDbTbl
      ) SELECT              
    
            booking_price,
            0,
            booking_taxes,
			'".$_POST['studioID']."',

            booking_spaces,
            booking_spaces,

            booking_date,
            booking_date,

            booking_id,
            event_id,
            'board_".$_POST['studioID']."'
           
    FROM bbwp.board_".$_POST['studioID']."_em_bookings
    ORDER BY booking_id ASC
	";
	// booking_status   - 0 For static values as status COMPLETED
	
		if (mysqli_query($mysqli, $booking)) {
			echo "</br>"; echo "Step 1 completed:"; echo "</br>";
			echo "<p>"; echo "Booking records migrated."; echo "</p>";	
			//step 1 - Seat booked:
		$booktcktinfo = "
		INSERT INTO bb_booktcktinfo (
            boTicketPrice,
			
            boProjectId,
            boTicketProjectName,
            boTicketProjectSize,
            boTicketProjectImage,

            ClTctBoid,
            CleintTctId,
            ClBkId,
            ClientDbTbl
    
            
      ) SELECT  
            
            ticket_booking_price,
            1,
            'P1',
            '12x32',
            '6_1534910444.jpg',
            
            ticket_booking_id,
            ticket_id,
            booking_id,
            'board_".$_POST['studioID']."'
            
    FROM bbwp.board_".$_POST['studioID']."_em_tickets_bookings
    ORDER BY booking_id ASC
		";		
		
		
			if (mysqli_query($mysqli, $booktcktinfo)) {

			 echo "</br>"; echo " Step 2 completed:"; echo "</br>";
			 echo "<p>"; echo "Ticket records migrated."; echo "</p>";	
			 
			    $UpdateBooking = "
				UPDATE bb_booking AS B,bb_event E SET
				
				B.eventId=E.id,
				B.boEventName=E.evTitle,
				B.boEventDate=E.evDate,
				B.eventStTime=E.evStTime,
				B.eventEndTime=E.evEndTime
								
				WHERE
				E.CleintEVid = B.CleintEVid and
				E.ClientDbTbl = B.ClientDbTbl and
				B.CleintEVid !='' and
				B.ClientDbTbl = 'board_".$_POST['studioID']."' and
				B.eventLocationId='".$_POST['studioID']."'
				";
				
				//echo $UpdateBooking;
				if (mysqli_query($mysqli, $UpdateBooking)) {
			 echo "</br>"; echo " Step 3 completed:"; echo "</br>";
			 echo "<p>"; echo "Booking records updated."; echo "</p>";	
					
					
					
					
		//step 4 - Seat booked:

					$BookingIds = "
					UPDATE bb_booktcktinfo as BT,  bb_booking AS B
					SET
					BT.bookingId = B.id        
					
					WHERE
					BT.ClBkId = B.ClBkId and
					BT.ClientDbTbl = B.ClientDbTbl and
					BT.ClientDbTbl =  'board_".$_POST['studioID']."'
					
					";
					if (mysqli_query($mysqli, $BookingIds)) {
			
					 echo "</br>"; echo " Step 4 completed:"; echo "</br>";
					 echo "<p>"; echo "Booking Ids records updated."; echo "</p>";	



						
		//step 5 - Seat booked:

					$UpdTicketName ="
					UPDATE `bb_booktcktinfo` SET boTicketName = (
					SELECT evTicketName FROM `bb_evticketpackage`
					WHERE
					bb_booktcktinfo.CleintTctId = bb_evticketpackage.CleintTctId AND
					bb_booktcktinfo.ClientDbTbl = bb_evticketpackage.ClientDbTbl AND
					bb_booktcktinfo.ClientDbTbl = 'bbwp.board_".$_POST['studioID']."'
					) WHERE bb_booktcktinfo.ClientDbTbl = 'board_".$_POST['studioID']."'
					";
			
						if (mysqli_query($mysqli, $UpdTicketName)) {
							 echo "</br>"; echo " Step 5 completed:"; echo "</br>";
							 echo "<p>"; echo "Ticket names updated."; echo "</p>";	
							 							 
				// Form for personal information			
							
				?>
				<form name="pernDetails" id="pernDetails" method="post" action="" >
					<input type="hidden" name="studioID" id="studioID" 
						value="<?php echo $_POST['studioID'];?>" >
						
					<input type="hidden" name="PersonStudioID" id="PersonStudioID" 
						value="<?php echo $_POST['studioID'];?>" >
					
					<input type="submit" name="submit" value="Update User Data">
				</form>
<h1><a href="<?php echo base_url_site?>bookingupdateuserdata?studioID=<?php echo $_POST['studioID'];?>">Update User Data</a></h1>
				<?php 
				// EOF Form for personal information							

							} else { echo "Error Inserted record:".mysqli_error($mysqli); }
					
					} 
			
				} else { echo "Error Inserted record: " . mysqli_error($mysqli);}
		
		
			} else { echo "Error Inserted record: " . mysqli_error($mysqli);}

				
		} else { echo "Error Inserted record: " . mysqli_error($mysqli); }
	
	
	} else { echo "Location Id is not migrated from source database."; }
	
} 

   


// Code of personal information
if ( (isset($_REQUEST['PersonStudioID']) and ($_REQUEST['PersonStudioID']) != ""  )  || 
		(isset($_REQUEST['StrtLimt']) and ($_REQUEST['StrtLimt']) != ""  ) )
			{
	
echo "<br><b>----------- Updating User data in batches of 2000 ------------------</b><br>";


	
	 $Search_records = "select count(booking_id) as totals from bbwp.board_".$_REQUEST['studioID']."_em_bookings order by booking_id ";
	     
	 // echo "</br>  ==========</br>";

	 $resultRecords = $mysqli->query($Search_records);
	 
	 $totalRows = mysqli_fetch_object($resultRecords);
	 
        $Total = $totalRows->totals;  //echo "</br>  ==========</br>";
	 
	 	$tot = (int)$Total;			//echo "</br>  ==========</br>";

		$Quotient = $tot/2000 ;     	//echo "</br>  ==========</br>";
		
		$RoundQuotient = round($Quotient);   //echo "</br>  ==========</br>";
	

	 
	 if($tot>2000) {
	 
	 	if($Quotient > $RoundQuotient ) {
			$steps = $RoundQuotient+1;
			} else {
				$steps = $RoundQuotient;
				}
	    //echo "Steps:", $steps;		
	 }

	 	if( ($tot>2000) and ($steps>0) )	 { 
		// Loops will be here
			for($i=1; $i<=$steps; $i++ ) {
					echo "</br>";
			//if(isset($_REQUEST['StrtLimt']) and $i>$_REQUEST['StrtLimt']  )	{
			?>	
			<form name="limits" id="limits" method="post">
				<input type="hidden" name="StrtLimt" value="<?php echo $i; ?>">
				<input type="hidden" name="studioID" value="<?php echo $_REQUEST['studioID']; ?>">
				<input type="submit" name="Limit" value="Part <?php echo $i;?>">
			</form>
  
			<!--<a href="?step=1"> Step <?php echo $i?></a>-->
			
						
			<?php 		 
		//}				
			
				} //end loop
		}
 
			
	//echo "User data updated";
} 

//Steps 
if (isset($_REQUEST['StrtLimt'])) {
	
	
	if($_REQUEST['StrtLimt'] == 2) {
		$StrtLimt = 2000;
		}
		else if($_REQUEST['StrtLimt'] == 3) {
		$StrtLimt = 4000;
		}
		else if($_REQUEST['StrtLimt'] == 4) {
		$StrtLimt = 6000;
		} 
		else if($_REQUEST['StrtLimt'] == 5) {
		$StrtLimt = 8000;
		} 
		else if($_REQUEST['StrtLimt'] == 6) {
		$StrtLimt = 10000;
		} else if($_REQUEST['StrtLimt'] == 7) {
		$StrtLimt = 12000;
		}  else if($_REQUEST['StrtLimt'] == 8) {
		$StrtLimt = 14000;
		} else if($_REQUEST['StrtLimt'] == 9) {
		$StrtLimt = 16000;
		} else if($_REQUEST['StrtLimt'] == 10) {
		$StrtLimt = 18000;
		} else if($_REQUEST['StrtLimt'] == 11) {
		$StrtLimt = 20000;
		} else if($_REQUEST['StrtLimt'] == 12) {
		$StrtLimt = 22000;
		} else if($_REQUEST['StrtLimt'] == 13) {
		$StrtLimt = 24000;
		} else {
			$StrtLimt = 0;
		}
	
	
	 $selectQueryC = "select booking_id,booking_meta from bbwp.board_".$_REQUEST['studioID']."_em_bookings order by booking_id ASC LIMIT ".$StrtLimt.", 2000 ";

		     
	//echo "</br>"; echo $selectQueryC; echo "</br>";	 
	
	$rows = mysqli_query($mysqli,$selectQueryC); 

			if (!$rows) { printf("Error: %s\n", mysqli_error($mysqli));  exit(); }
		
			$data2=array();
			$data=array();
			
					
			while ($row = mysqli_fetch_array($rows)) {
					
			
			$data = @unserialize($row['booking_meta']);
			
			
			//print_r($data);
			
			
			if($data == false)	{
				//echo "</br>"; echo "FAILED here: ".$row['booking_id']; echo "</br>";
				
			$my_data =$row['booking_meta'];

			$fixed_serialized_data = preg_replace_callback ('!s:(\d+):"(.*?)";!',function($match) {
	 
				return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";'; }, $my_data );

			$data = @unserialize($fixed_serialized_data );
				if($data == false) {
					//echo "</br>"; echo "FAILED AGAIN here: ".$row['booking_id']; echo "</br>";
					
					$data=  @unserialize($my_data, ['allowed_classes' => false]);
					

				if($data == false) {
					//echo "</br>"; echo "FAILED RETURN here: ".$row['booking_id']; echo "</br>";
					
					
				}



					
					
				}
				
			}		
					

					
					
					if(isset($data['coupon'])) {
						
						$coupon_id=$data['coupon']['coupon_id'];
						$coupon_code=$data['coupon']['coupon_code'];
						} else {
						$coupon_id="";
						$coupon_code="";
						}
					
					$booking_id=$row['booking_id'];
					
					$accomdata="";
					
					if(isset($data['registration']['user_name'])) {
						$user_nameData=$data['registration']['user_name'];
						$user_name = str_replace("'", ' ', $user_nameData); 
						} else { $user_name=""; }

					if(isset($data['registration']['user_email'])){
						$user_email=$data['registration']['user_email'];
						} else { $user_email=""; }

					if(isset($data['registration']['dbem_address'])){ 				
						
						$dbem_addressData =$data['registration']['dbem_address'];
						$dbem_address = str_replace("'", '', $dbem_addressData); 
						
						} else { $dbem_address=""; }

					if(isset($data['registration']['dbem_city'])){
						$dbem_cityData=$data['registration']['dbem_city'];
						$dbem_city = str_replace("'", '', $dbem_cityData); 
						$dbem_city = "City: ".$dbem_city."\n"; 
						
						} else { $dbem_city=""; }

					if(isset($data['registration']['dbem_state'])) {
						$dbem_stateData=$data['registration']['dbem_state'];
						$dbem_state = str_replace("'", '', $dbem_stateData); 
						$dbem_state = "State: ".$dbem_state."\n"; 
						} else { $dbem_state=""; }

					if(isset($data['registration']['dbem_zip'])) {
						$dbem_zip=$data['registration']['dbem_zip'];
						$dbem_zip="Zip: ".$dbem_zip."\n"; 
						
						} else { $dbem_zip=""; }

					if(isset($data['registration']['dbem_country'])) {
						$dbem_countryData=$data['registration']['dbem_country'];
						$dbem_country = str_replace("'", '', $dbem_countryData); 
						
						$dbem_country = "Country:".$dbem_country."\n";
						
						} else { $dbem_country="";}

					if(isset($data['registration']['dbem_phone'])) 	{
						
						$dbem_phone=format_phone('us', $data['registration']['dbem_phone']);
						
						} else  { $dbem_phone=""; }

					if(isset($data['registration']['dbem_fax'])) {
						$dbem_fax=$data['registration']['dbem_fax'];
						$dbem_fax="Fax: ".$dbem_fax."\n";
						} else { $dbem_fax=""; }

					if(isset($data['booking']['personalization'])) {
						$personalizationData= $data['booking']['personalization'];
						$personalization = str_replace("'", '', $personalizationData); 
							if(strlen($personalization)>4){
								$personalization = "Personalization: ".$personalization."\n";
								
								}
						} else { $personalization=""; }

					if(isset($data['booking']['booking_comment'])) { 
						$booking_commentData=$data['booking']['booking_comment'];
						$booking_comment = str_replace("'", '', $booking_commentData); 
						
						$booking_comment = "Booking Comment: ".$booking_comment."\n";
						
						} else { $booking_comment=""; }

					if(isset($data['gateway'])) {
						$gatewayData=$data['gateway'];
						$gateway= str_replace("'", '', $gatewayData); 
						$gateway = "Gateway: ".$gateway."\n";
						} else { $gateway=""; }

					//coupon section

						
						
						
					//$accomdata= $dbem_state."/".$dbem_zip."/". $dbem_country."/". $dbem_fax."/".$booking_comment."/".$gateway."/".$personalization;
					//$accomdata= $dbem_state.$dbem_zip.$dbem_country.$dbem_fax.$booking_comment.$gateway.$personalization;
					$accomdata= $dbem_city.$dbem_state.$dbem_country.$dbem_zip.$dbem_fax.$booking_comment.$gateway.$personalization;
					
					  
				 $UpdBookingData="UPDATE bb_booking SET 
					cOrderNo='".$booking_id."',
					boFullName='".$user_name."',
					boPhone='".$dbem_phone."',
					boEmail='".$user_email."',
					boAddress='".$dbem_address."',
					boAccommodationsNotes='".$accomdata."',
					ClCuId='".$coupon_id."',
					promocode='".$coupon_code."'
				
					WHERE bb_booking.ClBkId ='".$booking_id."' and bb_booking.ClientDbTbl='bbwp.board_".$_REQUEST['studioID']."' ";
					
					/*echo "</br>"; echo $UpdBookingData; echo "</br>";	 
					die();*/
					if (mysqli_query($mysqli, $UpdBookingData)) {
							//echo "</br>"; echo " Step 6: User data added"; echo "</br>";
							//echo "<br> : ".$booking_id; echo "</br>";
							
							/*
							We can define static value and can increment that by every records here.
							And after while statment, we can mentiond that these number of records are updated.
							*/
							 
					} else { 
					
					//echo "<br> : ".$UpdBookingData; echo "</br>";
					//echo "Error Inserted User data:".mysqli_error($mysqli); 
					
						}  
						  
		  } //while				
// EOF Code of personal information									
echo "<b>"," Part ", $_REQUEST['StrtLimt'] , " executed successfully. ","</b>"; echo "</br>";


} //if (isset($_REQUEST['StrtLimt']))   

	
	
	
function format_phone($country, $phone) {
  $function = 'format_phone_' . $country;
  if(function_exists($function)) {
    return $function($phone);
  }
  return $phone;
}
 
function format_phone_us($phone) {
  // note: making sure we have something
  if(!isset($phone{3})) { return ''; }
  // note: strip out everything but numbers 
  $phone = preg_replace("/[^0-9]/", "", $phone);
  $length = strlen($phone);
  switch($length) {
  case 7:
    return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
  break;
  case 10:
   return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
  break;
  case 11:
  return preg_replace("/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/", "$1($2) $3-$4", $phone);
  break;
  default:
    return $phone;
  break;
  }
}	


	
?>	
	
	