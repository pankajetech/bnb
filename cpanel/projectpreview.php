<?php 
include('includes/header.php');
$menuClssProject="active";
$menuSbClssProject="selected";
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php"); exit; } 

if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") {    
	$params = array('');
	$result = $db->rawQueryOne("SELECT * FROM  bb_project WHERE id='".$_REQUEST["id"]."' ", $params);
	$ritems = (array)$result;
	if(!empty($ritems)) {
		$ritems = $ritems;
	}
	//Project History 
	$result1 = $db->rawQuery("SELECT * FROM  bb_proactivitylog WHERE lgProId='".$_REQUEST["id"]."' order by id desc", $params);
	$ritems1 = (array)$result1;
	
	$projectHistoryLogArr = array();
	if(!empty($ritems1)) {
		foreach($ritems1 as $key=>$val) {
		    //Get user name
			$result11 = $db->rawQuery("SELECT firstName FROM  bb_users WHERE id='".$val['lgUserId']."'", $params);
			$ritems11 = (array)$result11;
			$projectHistoryLogArr[] = "User ". $result11[0]['firstName']." ". $val['lgDescription']." on ". date('m/d/Y', strtotime($val['lgDate']))." at ".$val['lgTime'];
		}
	}
	$projectHistoryLogTxt = implode("\n",$projectHistoryLogArr);
} else {
	$ritems = array();
	$ritems = $ritems;
}
?>
<!-- Header end-->
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<!-- left nagivation end-->

<!-- ToolTip Type Validation-->
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>bootstrap-3.3.5.min.css">
<script type="text/javascript" src="<?php echo base_url_js?>bootstrap-3.3.5.min.js" /></script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>select2.min.css">
<script type="text/javascript" src="<?php echo base_url_js?>select2.min.js" /></script>

<script type="text/javascript" src="<?php echo base_url_js?>jquery.tokeninput.js"></script>
<link rel="stylesheet" href="<?php echo base_url_css?>token-input.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url_css?>token-input-facebook.css" type="text/css" />
<!-- left content area start-->
<div class="loading" style="display:none;">Loading&#8230;</div>
<div class="right-wrapper">
	<h1 class="pageheading"><?php echo fnMultiStudioTitle($_SESSION["stId"]);?>Project-<?php echo $ritems['proName'];?></h1>
	<?php 
	if(isset($_REQUEST["id"]) && $_REQUEST["id"]!="") { ?>
	<div class="add-project-msg"></div> 
	<?php }?>
	<div class="form-area">
	<form role="form" method="post" action=""  id="addUpdProjFrm">
		<div class="template-info">
			<div class="input-row">
			    <?php 
				//edit case
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { 
					if($_REQUEST["type"] == "draft") { //edit case wit draft?>
					<input type="hidden" name="actionType" value="draftAction" id="actionType">
				<?php 
					} 
					else { //edit case?>
					<input type="hidden" name="actionType" value="updateAction" id="actionType">
				<?php 
					}
				} 
				if(!isset($_REQUEST["id"])) { //add case?>
					<input type="hidden" name="actionType" value="" id="actionType">
				<?php } ?>
				
				<input type="hidden" name="userId" value="<?php echo $_SESSION['usrid'];?>">
				<?php 
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { ?>
					<input type="hidden" name="projectId" id="projectId" value="<?php echo $_REQUEST["id"];?>">
				<?php } ?>
				<input class="tempname" type="text" name="proName" id="proName" placeholder="Project Name" value="<?php echo fnCheckEmpty($ritems['proName']);?>" />
				<select class="eventtype tempname" name="proTypeId" id="proTypeId" >
					<option value="">Project Type</option>
					<?php echo fnDropDownList("bb_protype","id","proTypeName",@fnCheckEmpty($ritems['proTypeId']),"");?>
				</select>
			</div>

			<div class="input-row">
				<select class="eventtype tempname" name="proClassId" id="proClassId">
					<option value="">Project Class</option>
					<?php echo fnDropDownList("bb_proclassoptions","id","opName",@fnCheckEmpty($ritems['proClassId']),"");?>
				</select>
				<input class="sku tempname" type="text" name="proSku" id="proSku" placeholder="SKU" value="<?php echo fnCheckEmpty($ritems['proSku']);?>" style="display:none;"/>
			</div>
		</div>
		
		<div class="workshop-section">
			<h2 class="section-heading">Workshop</h2>
			<div class="input-row">
				<textarea class="left70 tempname" placeholder="Project Detailed Description" name="proDetailedDesc" id="proDetailedDesc"><?php echo fnCheckEmpty($ritems['proDetailedDesc']);?></textarea>
				<?php 
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { ?>
				<input class="right28 tempname" type="text" placeholder="Price" name="proPrice" id="proPrice" value="<?php echo "$".fnCheckEmpty($ritems['proPrice']);?>" readonly/>
				<?php } else {?>
				<input class="right28 tempname" type="text" placeholder="Price" name="proPrice" id="proPrice" value="<?php echo fnCheckEmpty($ritems['proPrice']);?>" readonly/>
				<?php }?>
			</div>

			<div class="input-row">
				<div class="relative-div width30">
					<div class="control-group">
						<select id="answers" name="proPriCatId">
							<option value="">Primary Category</option>
							<?php echo fnDropDownListForPrimaryCat("bb_procategory","id","proCatName",@fnCheckEmpty($ritems['proPriCatId']),"");?>
						</select>
					</div>
					<script>
					$("#answers").select2({ width: '300px',disabled: true });
					</script>
				</div>
			</div>
			<style>
			.error ~ .select2-container{border: 1px solid red;}
			.valid ~ .select2-container{border: 1px solid green;}
			.add-project-msg{float:right; color:#6fcf00; font-weight:bold; display:none;}
			.prs-opt-singletxt{float:left; width:100%; margin-top:15px;}
			.prs-right-small input[type="radio"], .prs-right input[type="radio"]{margin-left:2.5%;}
			</style>
				
			<?php
			$params = array();
			if(isset($_REQUEST["id"]) and $_REQUEST["id"]!=""  ){  //edit case 
				if($ritems['proAltCat'] != "") { //if alt category is not blank
					$sqlQry = "SELECT id, proCatName FROM `bb_procategory` where id IN(".$ritems['proAltCat'].") and status=0 order by id asc";
				} 
				else { //if alt category is blank
					$sqlQry = "SELECT id, proCatName FROM `bb_procategory` where status=0 order by id asc";
				}
			} else { // add case 
				$sqlQry = "SELECT id, proCatName FROM `bb_procategory` where status=0 order by id asc";
			}
			$altcatlist = $db->rawQuery($sqlQry,$params);
			$altcatlistArr = array();
			$altcatjlistArr = array();
			foreach ($altcatlist as $key=>$item) {
				$altcatlistArr['id'] = $item['id'];
				$altcatlistArr['proCatName'] = $item['proCatName'];
				$altcatjlistArr[] = $altcatlistArr;
			}?>
			
			<div class="input-row">
				<div class="alcatrow">
				<input type="text" id="proAltCat11" name="proAltCat" class="tempname" placeholder="Alt Categories" >
				</div>
			</div>
			
			<script type="text/javascript">
			$(".token-input-list-facebook").show();
			$("#proAltCat11").tokenInput("addon/altCategoryList.php", {
				propertyToSearch: "proCatName",
				theme: "facebook",
				tokenDelimiter: ",",
				preventDuplicates: true,
				<?php if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="" and $ritems['proAltCat'] != "" ){ ?> 	
					prePopulate: <?php echo json_encode($altcatjlistArr);?>
				<?php }?>	
			});
			</script>
			<script>
			$("#token-input-proAltCat11").hide();
			$(".token-input-delete-token-facebook").hide();
			</script>
			<style>
			ul.token-input-list-facebook { border:none; width:98%;margin-left:0px;}
			</style>
		</div>
		
		<div class="publishing-section">
			<h2 class="section-heading">Publishing</h2>
			<div class="proj_add_schedule_dynamic_content">
				<div class="prs-opt-row" id="proschli_0">
					<div class="prs-select" id="proschx_0">
						<select class="personalization-opt tempname" name="proSchType">
							<option value="By Schedule" selected>By Schedule</option>
						</select>
					</div>
					<div class="prs-right" id="proschy_0">
						<input type="text" class="widthnew30 input_date tempname" name="proSchStDate" id="proSchStDate" value="<?php if($ritems['proSchDtOptional'] !=1) { echo date("m/d/Y",strtotime($ritems['proSchStDate'])); } else { echo "";}?>" placeholder="MM/DD/YYYY"/>
						<input type="text" class="widthnew30 input_time tempname" name="proSchStTime" id="proSchStTime" value="<?php if($ritems['proSchDtOptional'] !=1) { echo fnCheckEmpty($ritems['proSchStTime']); } else { echo ""; }?>" placeholder="Start Time" autocomplete="false"/>
					</div>
				</div>
			</div>
			<div class="input-row">
				<textarea class="textareafull tempname" placeholder="User: Created: Date Time" readonly><?php echo $projectHistoryLogTxt;?></textarea>
			</div>
		</div>
		<style>
		#img-upload{position:absolute; top:-500px; display:none; }
		.gallery-img {position:relative;}
		.gallery-img label{display:table-cell; vertical-align:middle; position:relative; z-index:10;}
		.gallery-img label i{}
		.gallery-img img{width:100%; position:absolute; left:0px;}
		.gallery-img:hover #upload-label{visibility:visible!important;}
		</style>
		<?php 
		if(isset($ritems['proGalImg']) && $ritems['proGalImg']!=""){
			$imageurl= s3_url_uimages."uploads/projects/".$ritems['proGalImg'];
			$imageName = $ritems['proGalImg'];
			echo '<input type="hidden" name="isfile" id="isfile" value="1">';
		}  
		else  {
			$imageurl=base_url_images."placeholder.jpg";
			$imageName="";
			echo '<input type="hidden" name="isfile" id="isfile" value="0">';
		}?>
		<input type="hidden" name="flpup" value="<?php echo $imageName;?>" />
		
		<div class="gallery-section">
			<h2 class="section-heading">Gallery</h2>
			<div class="gallery-img">
				<label id="upload-label">
					<input onchange="readURL(this);" name="proGalImg" id="img-upload" disabled="disabled" type="file"/>
				</label>
				<img id="blah" src="<?php echo $imageurl?>" alt="" />
			</div>
		</div>
		
		<div class="findprint-section">
			<h2 class="section-heading">Fine Print</h2>
			<div class="fndleft">
				<?php //edit case
				if(isset($_REQUEST["id"])) { 
					$searchString = ',';
					if( strpos($ritems['proFpPrintList'], $searchString) !== false ) { 
						$checked_arr = explode(",",$ritems['proFpPrintList']);
					}  else {
					  $checked_arr[] = $ritems['proFpPrintList'];
					}
				}
				$params = array('');
				$results = $db->rawQuery("SELECT id,proFinePrintName FROM  bb_profineprintoptions where status=0 order by id ASC", $params);
				if(count($results)>0){
					foreach($results as $val) { 
					    //edit case
						if(isset($_REQUEST["id"])) { 
							$checked = "";
							if(in_array($val["id"],$checked_arr)){
								$checked = "checked";
							}
						} ?>
						<div class="input-row">
							<input type="checkbox" disabled="disabled" value="<?php echo $val["id"];?>" name="proFpPrintList[]" class="proFpPrintListCls " style="margin-right:5px;" <?php if(isset($_REQUEST["id"])) { echo $checked;}?>/><?php echo $val["proFinePrintName"];?>
						</div>
					<?php 
					}
				} ?>
			</div>
			<div class="fndright">
				<div class="input-row">
					<textarea class="tareacustom tempname" name="proFpPrintCustom" id="proFpPrintCustom" placeholder="Custom Entry" ><?php echo @fnCheckEmpty($ritems['proFpPrintCustom']);?></textarea>
				</div>
			</div>
		</div>
		
		<div class="personalization-options-section">
			<h2 class="section-heading">Personalization Options</h2>
			<?php //Edit Case
			if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "") {?>
				<div class="personalize-dynamic-content">				
				<?php 
				//Get project status 
				$params8 = array('');
				$result8 = $db->rawQuery("SELECT userId FROM bb_project WHERE id='".$_REQUEST["id"]."' ", $params8);
				$ritems8 = (array)$result8;
				if(!empty($ritems8)){
					$currentuserId = $ritems8[0]['userId'];
				}
				$proPersitems = fetchTblDataByIdForProjectSection("bb_propersoptions","proId",$_REQUEST["id"]);
				if(!empty($proPersitems) && is_array($proPersitems)) {
					foreach($proPersitems as $key=>$val){ 
					?>
					<div class="prs-opt-row" id="perslistli_<?php echo $key;?>">
						<div class="prs-select" id="perslistx_<?php echo $key;?>">
							<a href="javascript:void(0);"><i class="fa fa-times-circle <?php if($key != 0){ ?> removeSpecificRow <?php }?>" style="font-size:19px;color:red;display:none;"></i></a>
							
							<select class="personalization-opt tempname" name="proOptionVal[]" id="proOptionVal<?php echo $key;?>">
								<option value="">Personalization Options</option>
								<option value="1" <?php if ($val['proOptionVal'] == 1) echo ' selected="selected"'; ?>>Text</option>
								<option value="2" <?php if ($val['proOptionVal'] == 2) echo ' selected="selected"'; ?>>Date</option>
								<option value="3" <?php if ($val['proOptionVal'] == 3) echo ' selected="selected"'; ?>>Multi-line</option>
							</select>
						</div>
						<style>
						.fixlblpreview .tempname{ float:none;}
						</style>
						<div class="prs-right fixlblpreview" id="perslisty_<?php echo $key;?>">
							<label>Label<input type="text" class="tempname" name="proLabel[]" id="proLabel<?php echo $key;?>" placeholder="Enter label" value="<?php echo fnCheckEmpty($val['proLabel']);?>"/></label>
							<?php if ($val['proOptionVal'] == 1 || $val['proOptionVal'] == 3){ ?>
								<label><input type="radio" disabled="disabled" name="proOptVal_<?php echo $key;?>[]" value="All Caps" <?php if($val['proOptVal']== 'All Caps') { echo "checked";}?>>All Caps</label>
								<label><input type="radio" disabled="disabled" name="proOptVal_<?php echo $key;?>[]" value="All Lower Case" <?php if($val['proOptVal']== 'All Lower Case') { echo "checked";}?>>All Lower Case</label>
								<label><input type="radio" disabled="disabled" name="proOptVal_<?php echo $key;?>[]" value="Title Case" <?php if($val['proOptVal']== 'Title Case') { echo "checked";}?>>Title Case</label>
							<?php } ?>
							
							<?php if ($val['proOptionVal'] == 2){ ?>
								<label><input type="radio" disabled="disabled" name="proOptVal_<?php echo $key;?>[]" value="MM-DD-YY" <?php if($val['proOptVal']== 'MM-DD-YY') { echo "checked";}?>>MM-DD-YY</label>
								<label><input type="radio" disabled="disabled" name="proOptVal_<?php echo $key;?>[]" value="MM-DD-YYYY" <?php if($val['proOptVal']== 'MM-DD-YYYY') { echo "checked";}?>>MM-DD-YYYY</label>
								<label><input type="radio" disabled="disabled" name="proOptVal_<?php echo $key;?>[]" value="MM-YY" <?php if($val['proOptVal']== 'MM-YY') { echo "checked";}?>>MM-YY</label>
								<label><input type="radio" disabled="disabled" name="proOptVal_<?php echo $key;?>[]" value="YYYY" <?php if($val['proOptVal']== 'YYYY') { echo "checked";}?>>YYYY</label>
								<label><input type="radio" disabled="disabled" name="proOptVal_<?php echo $key;?>[]" value="YY" <?php if($val['proOptVal']== 'YY') { echo "checked";}?>>YY</label>
							<?php } ?>
						</div>
						<div class="prs-opt-singletxt" id="perslistz_<?php echo $key;?>">
							<div class="prs-right-single-text">
								<label>Hover Text<input type="text" class="tempname" name="proHoverTxt[]" id="proHoverTxt<?php echo $key;?>" value="<?php echo fnCheckEmpty($val['proHoverTxt']);?>" placeholder="Enter hover text"/></label>
							</div>
						</div>
					</div>
				<?php
					}
				}?>
				</div>
			<?php 
			} ?>
		</div>
		
		<div class="last_template-full_section">
			<h2 class="section-heading">Specifications</h2>
			<div class="prs-opt-row">
				<div class="specification-btn-row">
					<input type="text" name="proSpecWi" class="tempname" id="proSpecWi" placeholder="Width" value="<?php echo fnCheckEmpty($ritems['proSpecWi']);?>">
					<input type="text" name="proSpecHi" class="tempname" id="proSpecHi" placeholder="Height" value="<?php echo fnCheckEmpty($ritems['proSpecHi']);?>">
					<input type="text" name="proSpecDe" class="tempname" id="proSpecDe" placeholder="Depth" value="<?php echo fnCheckEmpty($ritems['proSpecDe']);?>">
					<input type="text" name="proSpecWe" class="tempname" id="proSpecWe" placeholder="Weight" value="<?php echo fnCheckEmpty($ritems['proSpecWe']);?>">
				</div>
			</div>

			<div class="prs-opt-row">
				<div class="specification-list-row">
					<ul>
					<?php //Edit Case
					if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "") {?>
						<?php 
						//Get project status 
						$params8 = array('');
						$result8 = $db->rawQuery("SELECT userId FROM bb_project WHERE id='".$_REQUEST["id"]."' ", $params8);
						$ritems8 = (array)$result8;
						if(!empty($ritems8)){
							$currentuserId = $ritems8[0]['userId'];
						}
						$proSpecitems = fetchTblDataByIdForProjectSection("bb_prospecifications","proID",$_REQUEST["id"]);
						
						if(!empty($proSpecitems) && is_array($proSpecitems)) {
							foreach($proSpecitems as $key=>$val){ 
						?>
						<li id="listli_<?php echo $key;?>">
							<div id="listx_<?php echo $key;?>" class="specification-list">
								<a href="javascript:void(0);"><i class="fa fa-times-circle remove" style="font-size:19px;color:red;display:none;"></i></a>
								<select class="specification-list_dropdown unique tempname" name="specOptionVal[]" id="specOptionVal<?php echo $key;?>" required>
									<option value="">Specification options</option>
									<?php echo fnDropDownList("bb_prospcoptions","id","opName",$val["specOptionVal"],"");?>
								</select>
							</div>
							
							<div id="listy_<?php echo $key;?>" class="specification-list">
							<?php 
							$options = fnDropDownList2ndSpecificationOptions("bb_prospcsizemeasure","id","sizename","id_type",$val["specOptionVal"],$val["specType"]);
							if($options != ""){ ?>
								<select class="specification-list_dropdown100 tempname" name="specType[]" id="specType<?php echo $key;?>" required>
									<?php echo fnDropDownList2ndSpecificationOptions("bb_prospcsizemeasure","id","sizename","id_type",$val["specOptionVal"],$val["specType"]);?>
								</select>
							<?php } ?>
							</div>
							<div id="listz_<?php echo $key;?>" class="specification-list-qty">
								<label>Qty<input type="text" class="tempname" placeholder="3" name="specTypeVal[]" value="<?php echo $val["specTypeVal"];?>"  id="specTypeVal<?php echo $key;?>" required></label>
							</div>
						</li>
					<?php 
							}
						}
					}?>
					</ul>
				</div>
			</div>
		</div>
	</form>
	</div>
</div>
<!-- left content area end-->
<script>
/********Image Upload Section *********
*************************************************
*****************************************/
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#blah').attr('src', e.target.result);
		};
		reader.readAsDataURL(input.files[0]);
		$("#upload-label").css({'visibility' : 'hidden'});
	}
}

/********Project Specification Section *********
*************************************************
*****************************************/
/*Project specification 1st Dropdown options list*/
function getProjSpec1stOptList(){ 
	$.ajax({
        url: "<?php echo base_url_site;?>getProjSpec1stOptList",
        type: "post",
        data : {tblName:'bb_prospcoptions',fldName1:'id',fldName2:'opName'},
        success: function (response) {
            var jsonData = $.parseJSON(response); 
			var optionListLength  = jsonData.optionList.length; 
			if(optionListLength > 0) {
			    var optionListHtml = "<option value=''>Specification options</option>";
				for(var k=0;k<optionListLength; k++){
				    optionListHtml += "<option value="+jsonData.optionList[k].id+">"+jsonData.optionList[k].opName+"</option>";
				}
				$(".specification-list-row ul li:last-child .specDropdownAddedTime").html(optionListHtml);
			}
		}
    });
}

/*Project specification 2nd Dropdown options list at page load */
<?php //Edit Case
if(!isset($_REQUEST["id"])) { ?>
	getProjSpec2ndOptList(fieldId); 
<?php }?>
function getProjSpec2ndOptList(fieldId){ 
	$.ajax({
        url: "<?php echo base_url_site;?>getProjSpec2ndOptList",
        type: "post",
        data : {tblName:'bb_prospcsizemeasure',fldName1:'id',fldName2:'sizename',fldName3:1},
        success: function (response) {
            var jsonData = $.parseJSON(response); 
			var optionListLength  = jsonData.optionList.length;
			if(optionListLength > 0) {
			    var optionListHtml = "";
				for(var k=0;k<optionListLength; k++){
				    optionListHtml += "<option value="+jsonData.optionList[k].id+">"+jsonData.optionList[k].sizename+"</option>";
				}
				$('.specification-list_dropdown').parent().parent().find("#listy_"+fieldId+" .specification-list_dropdown100").html(optionListHtml);
			}
		}
    });
}

$(document).on('click','.actionEvent',function(){ 
	var actionEventId = $(this).attr('id');
	$("#actionType").val(actionEventId);
});
<?php 
//Edit Case  when preview is true
if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "" ) { ?>
	$('.tempname').attr('disabled', true);
<?php 
} else { // Add Case ?>
	$('.tempname').attr('disabled', false);
<?php }?>
</script>
<?php
include('includes/footer.php');
?>