<?php 
include('includes/header.php');
$menuClssProject = "downarrow";
$menuSbClssProject = "sub";
$menuSbClssStyleProject = "style='display:block;'";
$menuClssProjecttem5 = "active";
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 
?>
<!-- Header end-->
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<!-- left nagivation end-->
<!-- left content area start-->
<div class="right-wrapper">
	<h1 class="pageheading">Project Categories</h1>
	<?php
    if(isset($_SESSION['msg']) && $_SESSION['msg']!="") {?>
		<script>
		setInterval(function(){ $('.cat-msg').hide(); }, 3000);
		</script>
		<div class="cat-msg" style="display:block;">
			<?php echo $_SESSION['msg'];?>
		</div>
	<?php }?>
	<div class="category-del-msg" style="display:none;"></div>
	<style>
	.cat-msg,.category-del-msg{color:#6fcf00; font-weight:bold; text-align:center;margin-bottom: 20px;}
	</style>
	<div class="form-area">
		<div style="float:right;margin-bottom:15px;"><a href="<?php echo base_url_site;?>addupdcategory">Add Category</a></div>
		<div class="event-listing-module">
			<div class="elm-control">
				<div class="elm-row">
					<table class="elm-table" id="example">
						<thead>
						<tr>
							<th>Action</th>
							<!--<th>Status</th>-->
							<th>Category Name</th>
						</tr>
						</thead>
						<tbody>
						<?php
						$params = array('');
						$sql = "SELECT * FROM `bb_procategory` where status=0 order by id desc";
						$cats = $db->rawQuery($sql,$params);
						$categoryCnt = count($cats);
						if(isset($cats) && !empty($cats)) {
							foreach ($cats as $key=>$item) {
							?>
							<tr>
								<td>
									<a href="<?php echo base_url_site;?>addupdcategory?id=<?php echo $item['id'];?>">Edit</a>
									|
									<a href="javascript:void(0);" class="delCategory" customAtr="<?php echo $item['id'];?>">Delete</a>
								</td>
								<!--<td><?php //if($item['status'] == '0'){ echo "Active"; } else { echo "InActive"; }?></td>-->
								<td><?php if(isset($item['proCatName']) && $item['proCatName']!=""){ echo $item['proCatName']; } else { echo "NA";}?></td>
							</tr>
							<?php 
							}
						} ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<link href="<?php echo base_url_css?>dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<script src="<?php echo base_url_css?>dataTables/jquery.dataTables_project.js"></script>
<script src="<?php echo base_url_css?>dataTables/dataTables.bootstrap.js"></script>
<script>
$(document).ready(function(){ 
	vtable =  $('#example').DataTable( {
		dom: '<"elm-row"<"bubbleInfo"<"fl spage"<"paging"pl><"clear">><"fr page-text"i>>>',
		pagingType: "full_numbers",
		sortable: false,
		paginate: true,
		pageLength: 50,
		info: true,
		bSort: true,
		bFilter: true,
		"aaSorting": [],
		"aoColumnDefs": [ 
			{ "bSortable": false, "aTargets": [ 0 ] } ,
		]
	} );
	
	$(document).on('click','.delCategory',function(){  
		var categoryId = $(this).attr('customAtr');
		var elm = $(this);
		var dlg ='<p>Would you like to delete this category</p>';
		doConfirm(dlg, function yes() {
			$.ajax({
				url : '<?php echo base_url_site; ?>addon/ajaxDeleteCategory',
				type: 'post',
				data: {categoryId:categoryId},
				success: function(data) {
				    var odata = $.parseJSON(data);
					vtable.row( elm.parents('tr') ).remove().draw(false);
					$('html, body').animate({scrollTop: '0px'}, 300);//Scroll top
					$('.category-del-msg').css("display","block");
					$('.category-del-msg').html(odata.msg);
					setTimeout(function(){ 
						$('.category-del-msg').css("display","none");
						$('.category-del-msg').html(""); 
					}, 2000);
				},
				error: function() {
					alert('There has been an error.');
				}
			});
		}, function no() {
			return false;
		});
    });
});
</script>
<!-- left content area end-->
<?php
include('includes/footer.php');
?>