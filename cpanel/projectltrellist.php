<?php 
include('includes/header.php');
$menuClssProject = "downarrow";
$menuSbClssProject = "sub";
$menuSbClssStyleProject = "style='display:block;'";

$menuClssProjecttem7 = "active";
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 
?>
<!-- Header end-->
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<!-- left nagivation end-->
<!-- left content area start-->
<div class="right-wrapper">
	<h1 class="pageheading"><?php echo fnMultiStudioTitle($_SESSION["stId"]);?>Latest Releases</h1> <!--NEW-->
	<div class="form-area">
		<div class="event-listing-module">
			<div class="tabFilterRow"><ul></ul></div>
			<style>
			.tabFilterRow{float:left; width:100%;}
			.tabFilterRow ul{float:left; margin:0px; padding:0px; list-style:none; margin-bottom:15px;}
			.tabFilterRow ul li{display: inline-block; margin: 0 10px 10px 0px; padding: 10px 15px; color: #4a4a4a; text-decoration: none; border: 1px solid #c4c4c4; text-transform: uppercase; background-color: #fff; font-weight: 600; font-size: 13px; cursor: pointer;}
			.tabFilterRow ul li:hover { background-color: #333333; color: #fff; }
			.tabFilterRow ul li.active { background-color: #e1e1e1;}
			.tabFilterRow ul li.active:hover { background-color: #333333; color: #fff;}
			</style>
			<div class="elm-control">
				<div class="elm-row">
					<table class="elm-table" id="example">
						<thead>
						<tr>
							<th>Action</th>
							<th>Status</th>
							<th>Featured Image</th>
							<th>Project Name</th>
							<th>Project Class</th>
							<th>Project Type</th>
							<th>Categories</th>
							<!--<th>Sku</th>-->
							<th>Created Date</th>
							<th>Release Date Time </th>
						</tr>
						</thead>
						<tbody>
						<?php
						$params = array('');
						//Project With New Category
						//$sql = "SELECT * FROM `bb_project` where status=0 and isDeleted=0 and proSchDtOptional=0 and (proPriCatId=41 OR proAltCat=41) and proSchStDtTime >= NOW() order by proSchStDtTime desc";
						//$sql = "SELECT * FROM `bb_project` where status=0 and isDeleted=0 and proSchStDtTime >= NOW() order by proSchStDtTime desc";
						$sql = "SELECT * FROM `bb_project` where status=0 and isDeleted=0 and proSchDtOptional=0 and MONTH(proSchStDtTime) = MONTH(CURRENT_DATE()) order by proSchStDtTime desc";
						$projects = $db->rawQuery($sql,$params);
						if(isset($projects) && !empty($projects)) {
							foreach ($projects as $key=>$item) {
							?>
							<tr>
								<td><a href="<?php echo base_url_site;?>projectpreview?id=<?php echo $item['id'];?>">View</a></td> 
								<td>
								<?php 
								if($item['proSchDtOptional'] == 1){
									echo "Draft"; 
								} else {
									if(strtotime($item['proSchStDtTime'])<= strtotime("now")){
										echo "Published"; 
									} else {
										echo "Scheduled"; 
									}
								}?>
								</td>
								<td>
								<?php 
								if(isset($item['proGalImg']) && $item['proGalImg']!=""){?>
									<img src="<?php echo s3_url_uimages."uploads/projects/".$item['proGalImg'];?>" height="80" width="80"/>
								<?php
								} else { ?>
									<img src="<?php echo base_url_images."placeholder.jpg";?>" height="80" width="80"/>
								<?php }?>
								</td>
								<td><?php if(isset($item['proName']) && $item['proName']!=""){ echo $item['proName']; } else { echo "NA";}?></td>
								<td><?php if(isset($item['proClassId']) && $item['proClassId']!=""){ echo getFieldvalueById('bb_proclassoptions','opName',$item['proClassId']); } else { echo "NA";}?></td>
								<td><?php if(isset($item['proTypeId']) && $item['proTypeId']!=""){ echo getFieldvalueById('bb_protype','proTypeName',$item['proTypeId']); } else { echo "NA";}?></td>
								<td><?php if(isset($item['proPriCatId']) && $item['proPriCatId']!=""){ 
								echo getFieldvalueById('bb_procategory','proCatName',$item['proPriCatId']);
								} else { echo "NA";}?></td>
								<!--<td><?php //if(isset($item['proSku']) && $item['proSku']!=""){ echo $item['proSku']; } else { echo "NA";}?></td>-->
								<td><?php if(isset($item['createdDate']) && $item['createdDate']!=""){ echo date("m/d/Y",strtotime($item['createdDate'])); } else { echo "NA";}?></td>
								<td>
								<?php 
								if(isset($item['proSchStDate']) && $item['proSchStDate']!=""){ 
								echo date("m/d/Y",strtotime($item['proSchStDate'])) ." ".$item['proSchStTime']; } else { echo "NA";}?></td>
							</tr>
							<?php 
							}
						} ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<link href="<?php echo base_url_css?>dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<script src="<?php echo base_url_css?>dataTables/jquery.dataTables_project.js"></script>
<script src="<?php echo base_url_css?>dataTables/dataTables.bootstrap.js"></script>

<script>
$(document).ready(function(){ 
	vtable = $('#example').DataTable( {
		dom: '<"elm-row"<"bubbleInfo"<"fl spage"<"paging"pl><"clear">><"fr page-text"i>>>',
		pagingType: "full_numbers", //simple_numbers
		sortable: false,
		paginate: true,
		pageLength: 50,
		info: true,
		bSort: true,
		bFilter: true,
		"aaSorting": [],
		"aoColumnDefs": [ 
			{ "bSortable": false, "aTargets": [ 0 ] } ,
			{ "bSortable": false, "aTargets": [ 2 ] }
		]
	} );
});
</script>
<!-- left content area end-->
<?php
include('includes/footer.php');
?>