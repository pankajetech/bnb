<?php include('includes/header.php');
$menuClssEvents = "downarrow";
$menuSbClssEvents = "sub";
$menuSbClssStyleEvents = "style='display:block;'";

if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 

$params = array('');
//evStatus=0 and
//and evStatus=0
if(isset($_SESSION["urole"]) && $_SESSION["urole"]==1) {
$evlist = $db->rawQuery("SELECT * FROM `bb_event` where evLocationId='".$_SESSION["stId"]."' and isCancel=0 and  evTypeStatus='Enabled' and isDeleted=0 order by evDate asc", $params);
} else {
$evlist = $db->rawQuery("SELECT * FROM `bb_event` where evLocationId ='".$_SESSION["stId"]."' and isCancel=0  and isDeleted=0 and evTypeStatus='Enabled'  order by evDate asc", $params);
}


$i=1;
if(isset($evlist) && !empty($evlist)) {
$eventlist=array();
$eventcallist=array();
$curDate=strtotime(date('Y-m-d H:i:s'));
$evDt="";
$tol=0;
$evArr=array();
foreach ($evlist as $key=>$item) {
//$eventlist['id']=$item['id'];
$evIsClose=$item['evIsClose'];
$isCancel=$item['isCancel'];
//$eventlist['title']=$item['evTitle'];
$publishDate=strtotime(date('Y-m-d H:i:s',strtotime($item['evSPubDate']." ".$item['evSPubTime'])));
//$publishDate=strtotime($item['evSPubDate']." ".str_replace(" am","",$item['evSPubTime']));

$eventlist['id']=$item['id'];
$eventlist['evTitle']=$item['evTitle'];
$eventlist['evEdate']=$item['evDate'];
$eventlist['evmonth']=date("D",strtotime($item['evDate']));
$eventlist['dte']=date("Y-m-d",strtotime($item['evDate']));
$eventlist['publishDate']=date('Y-m-d H:i',$publishDate);

 
if($item['evStTime'] != $item['evEndTime']){
	$evStTime1 = $item['evStTime'];
	$evEndTime1 = $item['evEndTime'];
} else{
	$evStTime1 = "00:00";
	$evEndTime1 =  "00:00";
}

/*$eventlist['evstart']=$item['evStTime'];
$eventlist['evend']=$item['evEndTime'];*/

$eventlist['evstart']= $evStTime1;
$eventlist['evend']= $evEndTime1;

$eventlist['evseats']=$item['evNoOfTickets'];
$evseats=$item['evNoOfTickets']-$item['evBookTicket'];
$evCalenderColorLabel=$item['evCalenderColorLabel'];
if($evseats<0) {
$eventlist['evbseats']=0;
$evseats1=0;
} else {
$eventlist['evbseats']=$evseats;
$evseats1=$evseats;
}

//$dttime=strtotime(trim($item['evDate']));
//$startDate1=date('Y-m-d',strtotime("23-06-2018"));
//echo date('d-m-Y h:i:s',$curDate).'--'.date('d-m-Y h:i:s',$startDate1)."--1<Br>";
$evsDays=0;
//echo $item['evDate']." ".$item['evStTime'];





$date = new DateTime($item['evDate']." ".$item['evStTime']);
$startDate = $date->format('Y-m-d H:i:s');
//echo date('d-m-Y h:i:s',$curDate).'--'.$startDate1."--1<Br>";

$startWDate=date('Y-m-d',strtotime($item['evDate']." ".$item['evStTime']));

$evDTime=strtotime(date('Y-m-d H:i:s',strtotime($item['evDate']." ".$item['evStTime'])));
$evBDTime=$item['evDate']." ".$item['evStTime'];
if(isset($item['evScheduleHrs']) && $item['evScheduleHrs']>0) {
$evsDays=strtotime("-".$item['evScheduleHrs']." hours",strtotime($evBDTime));
} else {
$evsDays=$evDTime;
}
//echo $startDate."--1<Br>";






//die();
if($curDate>=$publishDate) {
//echo date('Y-m-d H:i',$publishDate)."--1".$startDate."<Br>";
//$eventlist['end']=$item['evDate']." ".$item['evEndTime'];



/*echo date('d-m-Y h:i:s',$curDate).'--'.date('d-m-Y h:i:s',$evsDays);
die();*/
if($item['evType']==1) {

if($isCancel==1 || $evIsClose==1) {
$eventlist['start']=$startDate;
$eventlist['title']=$evCalenderColorLabel;
$eventlist['color']="#cecece";
$eventlist['cend']="1";
$cend="1";
} else {
if($curDate>=$evsDays) {
$eventlist['start']=$startDate;
$eventlist['title']=$evCalenderColorLabel;
$eventlist['color']="#cecece";
$eventlist['cend']="1";
$cend="1";
} else {
$eventlist['start']=$startDate;

if(isset($item['evCalenderColorLabel']) && $item['evCalenderColorLabel']!="") {
$evCalenderColorLabel=$item['evCalenderColorLabel'];
$eventlist['title']=$evCalenderColorLabel;
} else {
$eventlist['title']="Public Workshop";
}
if($evseats1<=0) {
$eventlist['color']="#cecece";
$eventlist['cend']="1";
$cend="1";
} else {
$eventlist['cend']="0";
$cend="0";
$eventlist['color']=$item['evCalenderColor'];
}
}
//$eventlist['rendering']='background';

}
}
if($item['evType']==2) {

if($isCancel==1 || $evIsClose==1) {
$eventlist['start']=$startDate;
$eventlist['title']=$evCalenderColorLabel;
$eventlist['color']="#cecece";
$eventlist['cend']="1";
$cend="1";
} else {

if($curDate>=$evsDays || $evIsClose==1 || $evseats==0) {
$eventlist['start']=$startDate;
$eventlist['title']=$evCalenderColorLabel;
$eventlist['color']="#cecece";
$eventlist['cend']="1";
$cend="1";
} else {
$eventlist['start']=$startDate;
if(isset($item['evCalenderColorLabel']) && $item['evCalenderColorLabel']!="") {
$evCalenderColorLabel=$item['evCalenderColorLabel'];
$eventlist['title']=$evCalenderColorLabel;
} else {
$eventlist['title']="Private Workshop";
}

if($evseats1<=0) {
$eventlist['color']="#cecece";
$eventlist['cend']="1";
$cend="1";
} else {
$eventlist['cend']="0";
$cend="0";
$eventlist['color']=$item['evCalenderColor'];
}
}
//$eventlist['rendering']='background';


}
}

//$eventlist['evimage']="/cpanel/uploads/events/100X100/".$item['evImage'];
//$eventlist['evimage']= "https://s3.amazonaws.com/cupload/uploads/events/700X600/m_39_57519edc3fc437c8e3517314752313fe.jpg";
$eventlist['evimage']= s3_url_uimages."uploads/events/700X600/".$item['evImage'];

//if($evDTime>=$curDate && $evseats>0 && $item['slug']!="" && $curDate<=$evsDays) {
//echo base_url_site.$_REQUEST["location"].'/events/'.$item['slug'];

				//if($isCancel==0 || $evIsClose==0) {
				$eventurl=base_url_site."single-event?evid=".$item['id'];
				$eventlist['url']=$eventurl;
				$eventurl="";
				
				/*} else {
				$eventlist['url']="";
				
				}*/
/*} else {
$eventlist['url']="";
}*/
$evArr[]=$item['evDate'];
if(in_array($item['evDate'],$evArr)  && $cend==1){
//if($item['evDate']==$eventlist['evEdate']  && $cend==1 && $curDate<=strtotime($item['evDate'])){
if($curDate>=strtotime($item['evDate'])){
$eventlist['cend']="1";
}

//echo $item['evDate']."--".$cend."<Br>";
//print_r($evArr);
}

//echo $item['evDate']."<Br>";
$eventlist['totEv']=$tol;




$eventcallist[]=$eventlist;


} if($curDate<=$publishDate && $publishDate>=$evDTime) {

//echo date('Y-m-d H:i',$publishDate)."--1".$startDate."<Br>";
/*echo date('d-m-Y h:i:s',$curDate).'--'.date('d-m-Y h:i:s',$evsDays);
die();*/
if($item['evType']==1) {

if($isCancel==1 || $evIsClose==1) {
$eventlist['start']=$startDate;
$eventlist['title']=$evCalenderColorLabel;
$eventlist['color']="#cecece";
$eventlist['cend']="1";
} else {
if($curDate>=$evsDays) {
$eventlist['start']=$startDate;

$eventlist['title']=$evCalenderColorLabel;
$eventlist['color']="#cecece";
$eventlist['cend']="1";
} else {
$eventlist['start']=$startDate;
if(isset($item['evCalenderColorLabel']) && $item['evCalenderColorLabel']!="") {
$evCalenderColorLabel=$item['evCalenderColorLabel'];
$eventlist['title']=$evCalenderColorLabel;
} else {
$eventlist['title']="Public Workshop";
}
if($evseats1<=0) {
$eventlist['color']="#cecece";
$eventlist['cend']="1";
} else {
$eventlist['cend']="0";
$eventlist['color']=$item['evCalenderColor'];
}
}
//$eventlist['rendering']='background';

}
}
if($item['evType']==2) {

if($isCancel==1 || $evIsClose==1) {
$eventlist['start']=$startDate;
$eventlist['title']=$evCalenderColorLabel;
$eventlist['color']="#cecece";
$eventlist['cend']="1";
} else {

if($curDate>=$evsDays) {
$eventlist['start']=$startDate;
$eventlist['title']=$evCalenderColorLabel;
$eventlist['color']="#cecece";
$eventlist['cend']="1";
} else {
$eventlist['start']=$startDate;
if(isset($item['evCalenderColorLabel']) && $item['evCalenderColorLabel']!="") {
$evCalenderColorLabel=$item['evCalenderColorLabel'];
$eventlist['title']=$evCalenderColorLabel;
} else {
$eventlist['title']="Private Workshop";
}
if($evseats1<=0) {
$eventlist['color']="#cecece";
$eventlist['cend']="1";
} else {
$eventlist['cend']="0";
$eventlist['color']=$item['evCalenderColor'];
}
}
//$eventlist['rendering']='background';


}
}
//$eventlist['evimage']="/cpanel/uploads/events/".$item['evImage'];

$eventlist['evimage']= s3_url_uimages."uploads/events/".$item['evImage'];
$eventurl=base_url_site."single-event?evid=".$item['id'];
				$eventlist['url']=$eventurl;
				$eventurl="";
/*if($evDTime>=$curDate && $evseats>0 && $item['slug']!="" && $isCancel==0 && $curDate<=$evsDays) {
//echo base_url_site.$_REQUEST["location"].'/events/'.$item['slug'];
$eventlist['url']=base_url_site.$_REQUEST["location"].'/events/'.$item['slug'];
} else {
$eventlist['url']="";
}*/

$evArr[]=$item['evDate'];
//if(in_array($item['evDate'],$evArr)  && $cend==0){
if($item['evDate']==$eventlist['evEdate']  && $cend==1 && $curDate<=strtotime($item['evDate'])){

$eventlist['cend']="0";
//echo $item['evDate']."--".$cend."<Br>";
//print_r($evArr);
}

//echo $item['evDate']."<Br>";
$eventlist['totEv']=$tol;
$eventcallist[]=$eventlist;
}

}

}

/*print_r($eventcallist);
die();*/
	 ?>

<link href='<?php echo base_url_css?>fullcalendar.min.css' rel='stylesheet' />
<link href='<?php echo base_url_css?>fullcalendar.print.min.css' rel='stylesheet' media='print' />
<script src='<?php echo base_url_js?>lib/moment.min.js'></script>
<script src='<?php echo base_url_js?>fullcalendar.min.js'></script>
<!-- Header end-->
<style>

  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }
/*  .fc-content {
  font-size:9px;
  }*/

</style>

<!-- left nagivation start-->

<?php include('includes/left-sidebar.php');?>

<!-- left nagivation end-->
<!-- left nagivation end-->

<!-- left content area start-->
<script>

  $(document).ready(function() {

    $('#calendar').fullCalendar({
	   header: {
       left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
	  timezone: '<?php echo $deflttimezone; ?>',
	   timeFormat: 'h(:mm)a',
      defaultDate: '<?php echo date('Y-m-d')?>',
      navLinks: true, // can click day/week names to navigate views
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      eventMouseover: function (data, event, view) {

            tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#fff;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;color:#000;">' + '<div class="tooltip-img" style=""><img src="'+data.evimage+'" height="50" width="50"></div> <div  class="tooltip-day" style="float:left;margin-left:5px;">'+data.evmonth+' '+data.evstart+' - '+data.evend+'</div><div  class="tooltip-day" style="float:left;margin-left:5px;">Seats Available: ' + data.evbseats + '/<span>' + data.evseats + '</span></div> </div>';


            $("body").append(tooltip);
            $(this).mouseover(function (e) {
                $(this).css('z-index', 10000);
                $('.tooltiptopicevent').fadeIn('500');
                $('.tooltiptopicevent').fadeTo('10', 1.9);
            }).mousemove(function (e) {
                $('.tooltiptopicevent').css('top', e.pageY + 10);
                $('.tooltiptopicevent').css('left', e.pageX + 20);
            });


        },
        eventMouseout: function (data, event, view) {
            $(this).css('z-index', 8);

            $('.tooltiptopicevent').remove();

        },
        dayClick: function () {
            tooltip.hide()
        },
        eventResizeStart: function () {
            tooltip.hide()
        },
        eventDragStart: function () {
            tooltip.hide()
        },
        viewDisplay: function () {
            tooltip.hide()
        },
      events: <?php echo json_encode($eventcallist);?>,
	  eventClick: function(event) {
    if (event.url) {
        window.open(event.url, "_blank");
        return false;
    }
}
    });

  });

</script>
<style>



  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }

</style>
<div class="right-wrapper">
<h1 class="pageheading"><?php echo fnMultiStudioTitle($_SESSION["stId"]);?></h1>
<div class="form-area">
<div class="listing-wrapper">
<div class="listing-table">
<div class="elm-row mrgtom30"><a class="elm-calendar bluelink" href="<?php echo base_url_site?>eventlist?studid=<?php echo $_SESSION["stId"];?>">View List</a></div>
<div id='calendar'></div>
</div>

</div>
</div>
</div>

<!-- left content area end-->


<?php
include('includes/footer.php');
?>