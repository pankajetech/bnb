<?php include('includes/header.php');
$menuClssStudio = "downarrow";
$menuSbClssStudio = "sub";
$menuSbClssStudio2 = "active";
$menuSbClssStyleStudio = "style='display:block;'";
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 
if(isset($_SESSION['urole']) && $_SESSION['urole']!=1){ header("location: ".base_url_site."dashboard");    exit; } 

//echo "<pre>++++";print_r($_SESSION);die;

if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") {    
//$numOrders = $result['Count'];
$params = array('');
$result = $db->rawQueryOne("SELECT * FROM bb_studiolocation WHERE id='".$_REQUEST["id"]."' ", $params);
$ritems = (array)$result;

if(!empty($ritems)) $ritems=$ritems; 
} else {
$ritems=array();
$ritems=$ritems;
}


	 ?>

<!-- Header end-->


<!-- left nagivation start-->

<?php include('includes/left-sidebar.php');?>

<!-- left nagivation end-->

<!-- left content area start-->
<script type="text/javascript" src="<?php echo base_url_site?>editor/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url_site?>editor/ckfinder/ckfinder.js"></script>
<style>
.centertxt input[type="text"], .centertxt textarea{text-align:center;}
.gallery-img label {
display:none;
}
#img-upload1 {

    position: absolute;
    top: -500px;
    display: none;

}

.gallery-img {

    
    position: relative;
    display: block;
    height: auto;
    min-height: 100px;

}


.gallery-img label {

    width: 52px;
    height: 60px;
    position: absolute;
    left: 0;
    right: 0;
    top: -50%;
    bottom: -50%;
    margin: auto;
    z-index: 10;

}
.gallery-img img {

    width: 100%;
    position: relative;
    left: 0px;
    margin: auto;
    z-index: 9;

}
.boi-img .gallery-img{min-height:auto!important;}
</style>

<div class="loading" style="display:none;">Loading&#8230;</div>
<div class="right-wrapper">
<h1 class="pageheading"><?php if(isset($_REQUEST["id"]) && $_REQUEST["id"]!=""){ echo "Update"; } else echo "Add";?>   Studio</h1>
<div class="form-area">

 <form role="form" method="post" action="" enctype="multipart/form-data" name="frmaddupStudio" id="frmaddupStudio">
 <input type="hidden" name="sid" id="sid" value="<?php echo @fnCheckEmpty($ritems['id']); ?>" />

<div style="float:right; margin-bottom:10px;" id="btnStudio"><input type="submit" class="yellow-btn" id="btnSave" name="btnSave" value="Save" data-id="0" /></div>

<div class="location-info-module centertxt">

<div class="locationinfo-row">
<div class="locationinfo-left"><div class="floatingBox"><input type="text" placeholder="" name="stNumber" id="stNumber"  value="<?php echo @fnCheckEmpty($ritems['stNum']); ?>" style="width:40%;" class="number-only" data-val="<?php echo @fnCheckEmpty($ritems['stNum']); ?>">
<label class="floating-lab float">Studio Number</label><div id="cmsg2" style="display:none;"></div>
</div></div>
<div class="locationinfo-right"><select class="clendar-color" name="stStatus" id="stStatus" style="float:right;" >
            <option value="">Studio Status</option>
            <?php echo fnDropDownList("bb_studiostatus","id","stName",@fnCheckEmpty($ritems['stStatus']),"");?>
            </select>
</div>
</div>
<div class="locationinfo-row">
<div class="locationinfo-left" style="width:60%;"><div class="floatingBox"><input type="text" placeholder="" name="stName" id="stName"  value="<?php echo @fnCheckEmpty($ritems['stName']); ?>" >
<label class="floating-lab float">Studio Name</label>
</div></div>
<div class="locationinfo-right"  style="width:38%; margin-bottom:10px;"><div class="floatingBox"><input type="text" placeholder="" name="slug" id="slug"  value="<?php echo @fnCheckEmpty($ritems['slug']); ?>" ><div id="cmsg3" style="display:none;"></div>
<label class="floating-lab float">Page URL (<?php echo web_url_site; ?>)</label>
</div>
</div>

<div class="locationinfo-row">
<div class="locationinfo-left" style="width:60%;">
<div style="width:25%;">
<div class="location-image-upload-section">
<div class="location-image-upload" style=" width:80%;">
<div id="IMloder1" class="gallery-img location-img-upload" style="">
<label id="upload-label" <?php if(isset($_REQUEST["stId"]) && $_REQUEST["stId"]!="") {?> style="display:none !important;" <?php }?> class="upc"><i class="fa fa-plus-circle"></i><input onchange="readURLogo(this);" name="stLogo" id="img-upload1"  type="file"/></label>
<?php 
if(isset($ritems['stLogo']) && $ritems['stLogo']!=""){
			$imageurl=s3_url_uimages."uploads/studio/logo/".$ritems['stLogo'];
			$imageName=$ritems['stLogo'];
			$isVimage1=1;
			 }  else  {
			 //$imageurl=base_url_site."images/placeholder.jpg";
			 $imageurl=base_url_images."placeholder.jpg";
			 $imageName="";
			 $isVimage1=0;
	}
?>
<img id="blah1" src="<?php echo $imageurl;?>" alt="">
<input type="hidden" name="flpup1" id="flpup1" value="<?php echo $imageName;?>" />
</div>
</div>

</div>
</div><div style="width:75%; float:left;">
<strong>Studio Contact info</strong><Br /><Br />

<div style="float: left; ">
<div class="input-row" style="width: 49%; margin-right:2%;"><div class="floatingBox"><input class="textfull remspace" type="text" name="stStreetAddress"   id="stStreetAddress"  placeholder="" value="<?php echo @fnCheckEmpty($ritems['stStreetAddress']); ?>" /><label class="floating-lab">Address Line 1</label></div></div>

<div class="input-row" style="width: 49%;"><div class="floatingBox"><input class="textfull remspace" type="text" name="stAddress1"  id="stAddress1"  placeholder="" value="<?php echo @fnCheckEmpty($ritems['stAddress1']); ?>" /><label class="floating-lab">Address Line 2</label></div></div>

<div class="input-row" style="width: 49%; margin-right:2%;"><div class="floatingBox"><input class="textfull remspace" type="text" name="stCity" required  id="stCity"  placeholder="" value="<?php echo @fnCheckEmpty($ritems['stCity']); ?>" /><label class="floating-lab">City</label></div></div>

<div class="input-row" style="width: 49%;">
<select class="slectfull" name="stState" required  id="stState"  placeholder="Select an Option" style="padding:12px 10px;">
<option value="">Select State</option>
    <?php 
    $states=getStates("224");
    foreach($states as $k=>$v) {
        if($ritems['stState']==$v['abbr']) {
            echo '<option value="'.$v['abbr'].'" selected>'.$v['abbr'].' </option>';
        } else {
            echo '<option value="'.$v['abbr'].'">'.$v['abbr'].' </option>';
        }
    }
    ?> 
    </select>
</div>

<div class="input-row" style="width: 49%; margin-right:2%;"><div class="floatingBox"><input class="textfull number-only remspace" type="text" name="stZip" required  id="stZip"  placeholder="" value="<?php echo @fnCheckEmpty($ritems['stZip']); ?>" maxlength="5" /><label class="floating-lab">Zip</label></div></div>

<div class="input-row" style="width: 49%;"><div class="floatingBox"><input class="textfull" type="text" required  name="stPhone" id="stPhone" value="<?php echo @fnCheckEmpty($ritems['stPhone']); ?>" placeholder="" /><label class="floating-lab">Studio Phone</label></div></div>

<div class="input-row"><div class="floatingBox"><input class="textfull remspace" type="text" required  name="stEmail" id="stEmail" value="<?php echo @fnCheckEmpty($ritems['stEmail']); ?>" placeholder="" /><label class="floating-lab">Studio Email</label></div></div>                
</div>

</div>
</div>
<div class="locationinfo-right" style="width:38%;"><div class="locationinfo-row"><div style="width:57%; margin-right:1%; float:left;"><div class="floatingBox" style="margin-bottom:5px;">
            </select><input type="text" name="stOwner" id="stOwner" placeholder="" value="<?php echo @fnCheckEmpty($ritems['stOwner']); ?>"  style="width:90%;"><label class="floating-lab float">Owner</label></div></div>
			<div style="width:40%; float:left;">
				<select class="clendar-color" name="stSNouns" id="stSNouns" style="float:right; width:100%; margin-right:0px;">
					<option value="">Owner/Owner(s)</option>
					<option value="0" <?php if(isset($ritems['stSNouns']) && $ritems['stSNouns']=="0") echo "selected";?>>Owner</option>
					<option value="1"  <?php if( (isset($ritems['stSNouns']) && $ritems['stSNouns']=="1") || !isset($ritems['stSNouns']) ) {echo "selected"; }?>>Owners</option>
				</select>
			</div>
			</div>
<div class="locationinfo-row"><div class="floatingBox"><input type="text" placeholder="" value="<?php echo @fnCheckEmpty($ritems['stOwnerCInfo']); ?>" name="stContactInfo" id="stContactInfo" ><label class="floating-lab float">Owner Contact Info</label></div></div>
</div>
</div>



<div class="locationinfo-row" id="locDec" ><strong>Location Description</strong><Br />
<div class="wysiwyg-editor">
			<textarea name="stAboutStudo" id="stAboutStudo" rows="3" required data-placement="right"  placeholder="Please enter about studio."><?php echo @fnCheckEmpty($ritems['stAboutStudo']); ?></textarea>
			<script type="text/javascript">
            var editor = CKEDITOR.replace( 'stAboutStudo', {
            filebrowserBrowseUrl : 'editor/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl : 'editor/ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl : 'editor/ckfinder/ckfinder.html?type=Flash',
            filebrowserUploadUrl : 'editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl : 'editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl : 'editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
            });
            CKFinder.setupCKEditor( editor, '../' );
            </script>
			</div>
</div>
<div class="locationinfo-row" style="margin-top:20px;">
<div class="locationinfo-left"><div class="floatingBox"><textarea rows="2" placeholder="" name="stLPolicies" id="stLPolicies"  ><?php echo @fnCheckEmpty($ritems['stLocPolicies']); ?></textarea><label class="floating-lab float">Location Policies</label></div></div>
<?php /*?><div class="locationinfo-right"><div class="floatingBox">
<label class="floating-lab float11"><strong>Studio Email Signature Block</strong></label><Br /><Br />
<?php echo '
	<b>-------------------------------------------------------------------------------<br />
	<strong>Board & Brush - '.$ritems['stName'].'</strong><Br>
	'.$ritems['stStreetAddress'].' | '.$ritems['stPhone'].'<Br>
	'.$ritems['stEmail'].'<Br>
	<a href="'.web_url_site.$ritems['slug'].'">'.web_url_site.$ritems['slug'].'</a>
	</b>
	<br><br>
	<a href="'.$ritems['stFbLink'].'"><img style="border:none;width:34px;height:34px;" src="'.base_url_images.'facebook.svg"></a>&nbsp;&nbsp;<a href="'.$ritems['stInstaLink'].'"><img style="border:none;width:34px;height:34px;" src="'.base_url_images.'insta.svg"></a>&nbsp;&nbsp;<a href="'.$ritems['stTwtLink'].'"><img style="border:none;width:34px;height:34px;" src="'.base_url_images.'twt.svg"></a>&nbsp;&nbsp;<a href="'.$ritems['stPinLink'].'"><img style="border:none;width:34px;height:34px;" src="'.base_url_images.'pin.svg"></a>';?>
    
    
</div></div><?php */?>

<?php /*?><div class="locationinfo-right"><div class="floatingBox"><textarea rows="2" placeholder="" name="stESblock" id="stESblock" ><?php echo @fnCheckEmpty($ritems['stEmailSignBlock']); ?></textarea><label class="floating-lab float">Studio Email Signature Block</label></div></div><?php */?>
</div>

<div class="locationinfo-row">
<div class="location-image-upload-section" >
<div class="location-image-upload boi-img" style="width:30%;">
<div class="gallery-img location-img-upload">
<label id="upload-label" <?php if(isset($_REQUEST["stId"]) && $_REQUEST["stId"]!="") {?> style="display:none !important;" <?php }?> class="upc"><i class="fa fa-plus-circle"></i><input onchange="readURL(this);" name="stImage" id="img-upload"  type="file"/></label>
<?php 
if(isset($ritems['stOwnerPic']) && $ritems['stOwnerPic']!=""){
			$imageurl=s3_url_uimages."uploads/studio/700X600/".$ritems['stOwnerPic'];
			$imageName=$ritems['stOwnerPic'];
			$isVimage=1;
			 }  else  {
			 //$imageurl=base_url_site."images/placeholder.jpg";
			 $imageurl=base_url_images."placeholder.jpg";
			 $imageName="";
			 $isVimage=0;
	}
?>
<img id="blah" src="<?php echo $imageurl;?>" alt="">
<input type="hidden" name="flpup" id="flpup" value="<?php echo $imageName;?>" />
</div>
</div>

<div class="locationinfo-right" id="pid" style="width:65%;">
<strong>Personal Bio</strong><Br />
<div class="wysiwyg-editor">
			<textarea  name="stBioDesc" id="stBioDesc" rows="3" required  data-placement="bottom"  placeholder="Please enter studio description."><?php echo @fnCheckEmpty($ritems['stBioDesc']); ?></textarea>
			<script type="text/javascript">
            var editor1 = CKEDITOR.replace( 'stBioDesc', {
            filebrowserBrowseUrl : 'editor/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl : 'editor/ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl : 'editor/ckfinder/ckfinder.html?type=Flash',
            filebrowserUploadUrl : 'editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl : 'editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl : 'editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
            });
            CKFinder.setupCKEditor( editor1, '../' );
            </script>
			</div></div>
</div>


</div>

</div>


<div class="locationinfo-row"><div class="floatingBox"><input type="text" placeholder="" name="stLocationDir" id="stLocationDir" value="<?php echo @fnCheckEmpty($ritems['stLocDirection']); ?>" ><label class="floating-lab float">Location Direction</label></div></div>

<div class="locationinfo-row">
<div class="locationinfo-left"><div class="floatingBox"><input type="text" placeholder="" name="stFbLink" id="stFbLink"  value="<?php echo @fnCheckEmpty($ritems['stFbLink']); ?>"><label class="floating-lab float">Facebook</label></div></div>
<div class="locationinfo-right"><div class="floatingBox"><input type="text" placeholder=""  name="stInstaLink" id="stInstaLink"  value="<?php echo @fnCheckEmpty($ritems['stInstaLink']); ?>"><label class="floating-lab float">Instagram</label></div></div>
</div>

<div class="locationinfo-row">
<div class="locationinfo-left"><div class="floatingBox"><input type="text" placeholder="" name="stTwtLink" id="stTwtLink"   value="<?php echo @fnCheckEmpty($ritems['stTwtLink']); ?>"><label class="floating-lab float">Twitter</label></div></div>
<div class="locationinfo-right"><div class="floatingBox"><input type="text" placeholder="" name="stPinLink" id="stPinLink"   value="<?php echo @fnCheckEmpty($ritems['stPinLink']); ?>"><label class="floating-lab float">Pinterest</label></div></div>
</div>
<div class="sociallink-section">
        <h2 class="section-heading">Meta Data</h2>
		<div class="input-row"><div class="floatingBox"><input class="textfull" name="stMetaTitle" id="stMetaTitle" placeholder="" type="text" value="<?php echo @fnCheckEmpty($ritems['stMetaTitle']); ?>"><label class="floating-lab">Meta Title</label></div></div>
        <div class="input-row" style="margin-bottom:0;"><div class="floatingBox" style="margin-bottom:0;" ><textarea class="textareafull" name="stMetaDesc" id="stMetaDesc"><?php echo @fnCheckEmpty($ritems['stMetaDesc']); ?></textarea><label class="floating-lab">Meta Description</label>
        </div></div>
        
        
        </div>
      <div>&nbsp;</div>
<div class="locationinfo-row"><div class="floatingBox" style="margin-bottom:0px;"><input type="text" placeholder="" name="stPGatway" id="stPGatway" value="<?php echo @fnCheckEmpty($ritems['stAuthInfo']); ?>" ><label class="floating-lab float">Payment Gateway</label></div><br />
<span id="cmsg" style="display:none;"></span>
Authorize.net Gateway info eg - <strong>login id</strong>/<strong>transaction key</strong> (separated by forward slash <strong>/</strong>)</div>

</div>
<input type="hidden" name="isVimage" id="isVimage" value="<?php echo $isVimage;?>" />
<input type="hidden" name="isVimage1" id="isVimage1" value="<?php echo $isVimage1;?>" />
  <input class="width70" type="hidden" name="ccode" id="ccode" value="<?php echo @fnCheckEmpty($ritems['ccode']); ?>" />
      <input class="width70" type="hidden" name="isusa" id="isusa" value="<?php echo @fnCheckEmpty($ritems['isusa']); ?>" />
      <input class="width70" type="hidden" name="latitude" id="latitude" value="<?php echo @fnCheckEmpty($ritems['lat']); ?>" />
      <input class="width70" type="hidden" name="longitude" id="longitude" value="<?php echo @fnCheckEmpty($ritems['lng']); ?>" />  
</form>
</div>

</div>

   <link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>bootstrap-3.3.5.min.css">
    <script type="text/javascript" src="<?php echo base_url_js?>bootstrap-3.3.5.min.js" /></script>
    <script type="text/javascript" src="<?php echo base_url_js?>jquery-validate.bootstrap-tooltip.js" /></script>
      <script type="text/javascript" src="<?php echo base_url_js?>jquery.maskedinput-1.3.1.min_.js"></script>
      <script type="text/javascript" src="<?php echo base_url_js?>additional-methods.js" /></script>
 <script src="https://maps.google.com/maps/api/js?key=AIzaSyAwD4m6HlepJm2PMhA7KSchNvf7aLQRNgM"></script>    

<!-- left content area end-->
<script>
function showResult(result) {

    document.getElementById('latitude').value = result.geometry.location.lat();
    document.getElementById('longitude').value = result.geometry.location.lng();
	
	 var position = new google.maps.LatLng(result.geometry.location.lat(), result.geometry.location.lng());
	
	geocoder.geocode({'location': position}, function(results, status) {
        if(status === "OK"){
          var adrFormat = results[0].formatted_address;
          var adrLangth = adrFormat.split(", ").length - 1;
          var adrAry = adrFormat.split(", ");
          if(adrAry[adrLangth] == "USA"){
		   document.getElementById('isusa').value = 1;
		   document.getElementById('ccode').value = adrAry[adrLangth];
          	//alert('usa');
          } else {
		  	document.getElementById('isusa').value = 0;
		   document.getElementById('ccode').value = adrAry[adrLangth];
		  }
        }
        });
	
}

function getLatitudeLongitude(callback, address) {
    // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
    address = address || 'Ferrol, Galicia, Spain';
    // Initialize the Geocoder
    geocoder = new google.maps.Geocoder();
    if (geocoder) {
        geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                callback(results[0]);
            }
        });
    }
}

/*var button1 = document.getElementById('stZip');
var address="";
button1.addEventListener("blur", function () {
if($("#stZip").val()!="") {
if($("#stStreetAddress").val()!="") {
address=address+$("#stStreetAddress").val();
}
if($("#stAddress1").val()!="") {
address=address+" "+$("#stAddress1").val();
}
if($("#stCity").val()!="") {
address=address+","+$("#stCity").val();
}
if($("#stState").val()!="") {
address=address+","+$("#stState").val();
}
if($("#stZip").val()!="") {
address=address+" "+$("#stZip").val();
}
//alert(address);


   // var address = document.getElementById('address').value;
    getLatitudeLongitude(showResult, address)
}	
});*/

var oneElement = document.querySelector("#stStreetAddress");
var twoElement = document.querySelector("#stAddress1");
var threeElement = document.querySelector("#stCity");
var fourElement = document.querySelector("#stState");
var fiveElement = document.querySelector("#stZip");

oneElement.addEventListener("blur", doSomething, false);
twoElement.addEventListener("blur", doSomething, false);
threeElement.addEventListener("blur", doSomething, false);
fourElement.addEventListener("blur", doSomething, false);
fiveElement.addEventListener("blur", doSomething, false);

function doSomething(e) {
	var address="";
    var clickedItem = e.target.id;
    if($("#"+clickedItem).val()!="") {
		if($("#stStreetAddress").val()!="") {
			address=address+$("#stStreetAddress").val();
		}
		if($("#stAddress1").val()!="") {
			address=address+" "+$("#stAddress1").val();
		}
		if($("#stCity").val()!="") {
			address=address+","+$("#stCity").val();
		}
		if($("#stState").val()!="") {
			address=address+","+$("#stState").val();
		}
		if($("#stZip").val()!="") {
			address=address+" "+$("#stZip").val();
		}
		//alert(address);
		// var address = document.getElementById('address').value;
		getLatitudeLongitude(showResult, address)
	}
}


function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
				
                reader.onload = function (e) {
				$('#blah').attr('src', e.target.result);
				var image = new Image();
				image.src = e.target.result;
				
				image.onload = function () {
                    
						//Determine the Height and Width.
                        var height = this.height;
						//alert(height);
                        var width = this.width;
                        if (width <= 700) {
                           $("#isVimage").val(0);
						   //$("#upload-label").css({'visibility' : 'visible'});
                        } else {
						$("#isVimage").val(1);
						//$("#upload-label").css({'visibility' : 'hidden'});
						}
                };
				 };
                reader.readAsDataURL(input.files[0]);
				
            }
        }


function readURLogo(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
				
                reader.onload = function (e) {
				$('#blah1').attr('src', e.target.result);
				var image = new Image();
				image.src = e.target.result;
				
				image.onload = function () {
                    
						//Determine the Height and Width.
                        var height = this.height;
						//alert(height);
                        var width = this.width;

                        if (height <= 747 || width <= 755) {
                           $("#isVimage1").val(0);
						   //$("#upload-label1").css({'visibility' : 'visible'});
                        } else {
						$("#isVimage1").val(1);
						//$("#upload-label1").css({'visibility' : 'hidden'});
						}
                };
				 };
                reader.readAsDataURL(input.files[0]);
				
            }
        }



CKEDITOR.on('instanceReady', function () {
    $.each(CKEDITOR.instances, function (instance) {
        CKEDITOR.instances[instance].document.on("keyup", CK_jQ);
        CKEDITOR.instances[instance].document.on("paste", CK_jQ);
        CKEDITOR.instances[instance].document.on("keypress", CK_jQ);
        CKEDITOR.instances[instance].document.on("blur", CK_jQ);
        CKEDITOR.instances[instance].document.on("change", CK_jQ);
    });
});

function CK_jQ() {
    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
    }
}

function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // �-� CHECK MINUS, AND ONLY ONE.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // �.� CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

$('document').ready(function(){

$("#stNumber").on('change',function(){

var stNumPrev=$(this).attr("data-val");
var stNum=$(this).val();
var cur=$(this);
if(stNumPrev!=stNum) {
jQuery('.loading').show();
$.post("<?php echo base_url_site; ?>addon/chkStNumber.php", {stNum:stNum}, function(response) {
jQuery('.loading').hide();
var data = JSON.parse(response);
jQuery('#cmsg2').show();
if(data.result==0){
jQuery('#cmsg2').css('color','#093');
jQuery('#cmsg2').html("valid");
jQuery('#cmsg2').show();
} else {
jQuery('#cmsg2').css('color','#F00');
cur.val(stNumPrev);
jQuery('#cmsg2').html("Studio Number "+stNum+" already exist, please try another number");
jQuery('#cmsg2').show();

}

});
}

});


$("#slug").on('change',function(){

var slug=$(this).val();
var cur=$(this);

jQuery('.loading').show();
$.post("<?php echo base_url_site; ?>addon/chkSlug.php", {slug:slug,sid:'<?php echo @fnCheckEmpty($ritems['id']); ?>'}, function(response) {
jQuery('.loading').hide();
var data = JSON.parse(response);
jQuery('#cmsg3').show();
if(data.result==0){
jQuery('#cmsg3').css('color','#093');
jQuery('#cmsg3').html("valid");
jQuery('#cmsg3').show();
} else {
jQuery('#cmsg3').css('color','#F00');
jQuery('#cmsg3').html(""+slug+" already exist, please try another slug");
jQuery('#cmsg3').show();

}
});
});

$(document).on("blur","#stPGatway",function() {
var lkey = $(this).val();

jQuery('.loading').show(); 
if(lkey!="") {
$.post("<?php echo base_url_site; ?>addon/checkAuthLogin.php", {cklkey:lkey}, function(response) {
jQuery('.loading').hide();
var data = JSON.parse(response);
jQuery('#cmsg').show();
if(data.result==0){
jQuery('#cmsg').css('color','#093');
jQuery('#cmsg').html(data.msg+"<Br>");
jQuery('#cmsg').show();
jQuery('#btnSave').show();

} else {
jQuery('#cmsg').css('color','#F00');
jQuery('#cmsg').html(data.msg+"<Br>");
jQuery('#cmsg').show();
jQuery('#btnSave').hide();
}

});

} else {
jQuery('.loading').hide();
jQuery('#cmsg').hide();
jQuery('#btnSave').show();
}

});


$(document).on("blur",".remspace",function() {
var str = $(this).val();
var str = $.trim(str);
var newStr = str.replace(/\s+/g,' ').trim();
$(this).val(newStr);
});

$('.number-only').keypress(function (event) {
return isNumber(event, this)
});









	$.validator.addMethod("customemail", 
		function(value, element) {
			return /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
		}, 
		"Sorry, Ive enabled very strict email validation"
	);

	jQuery.validator.addMethod("zipUS", function(value, element) {
		return /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(value);
	}, "Please specify a valid US zip code.");


$.validator.addMethod('vImageLDim', function(value, ele) {

var isv=$("#isVimage1").val();
if(isv==1){
return true;
} else {
return false;
}	
	//return	ret;
		
}, "Width and Height must exceed 755 X 747.");

$.validator.addMethod('vImageDim', function(value, ele) {

var isv=$("#isVimage").val();
if(isv==1){
return true;
} else {
return false;
}	
	//return	ret;
		
}, "Width  must exceed 700.");

$("#stPhone").mask("(999) 999-9999");
$("#frmaddupStudio").validate({
			ignore: [],
			debug: false,
			rules: {
			stNumber:{
           	required: function(element){
			if(($("#stStatus").val()==3 || $("#stStatus").val()==4)) {
			return true;
			} else {
			return false;
			}
			}
         	},
			stName: {
			required: true, 
			remote: {
			url: "<?php echo base_url_site; ?>chkExist.php",
			type: "post",
			data:
			{
			ptitle: function()
			{
			return $('#frmaddupStudio :input[name="stName"]').val();
			},
			tblname: function()
			{
			return "bb_studiolocation";
			},
			coln: function()
			{
			return "stName";
			},
			colid: function()
			{
			return "<?php echo @fnCheckEmpty($ritems['id']); ?>";
			},
			isDeleted: function()
			{
			return "0";
			}
			}
			} 
			},
			stLogo:{
            required: function(element){
						if(($("#stStatus").val()==3 || $("#stStatus").val()==4)  && $("#isVimage1").val()==0 ) {
                         return true;
						 } else {
						 return false;
						 }
                        },extension: "png|jpeg|jpg|gif",//vImageLDim: true,

         	},
			slug:{
           	required: function(element){
			if(($("#stStatus").val()==3 || $("#stStatus").val()==4)) {
			return true;
			} else {
			return false;
			}
			}
         	},
			stImage:{
            required: function(element){
						if(($("#stStatus").val()==3 || $("#stStatus").val()==4)  && $("#isVimage").val()==0) {
                         return true;
						 } else {
						 return false;
						 }
                        },extension: "png|jpeg|jpg|gif",/*vImageDim: true,*/
/*
                         accept: "png|jpe?g|gif",
							filesize: "1048576",*/
         	},
			stEmail: {
			required: function(element){
			if(($("#stStatus").val()==3 || $("#stStatus").val()==4)) {
			return true;
			} else {
			return false;
			}
			},
			
			customemail:true,
			},
			stStreetAddress: {
			required: function(element){
			return ($("#stStatus").val()==3 || $("#stStatus").val()==4);
			}
			},
			/*stAddress1: {
			required: function(element){
			return ($("#stStatus").val()==3 || $("#stStatus").val()==4);
			}
			},*/
			stCity: {
			required: function(element){
			return ($("#stStatus").val()==3 || $("#stStatus").val()==4);
			}
			},
			stState: {
			required: function(element){
			return ($("#stStatus").val()==3 || $("#stStatus").val()==4);
			}
			},
			stZip: {
			required: function(element){
			return ($("#stStatus").val()==3 || $("#stStatus").val()==4);
			},
			 number: true,
			},
			stPhone: {
			required: function(element){
			return ($("#stStatus").val()==3 || $("#stStatus").val()==4);
			}
			},
			stOwner: {
			required: function(element){
			return ($("#stStatus").val()==3 || $("#stStatus").val()==4);
			}
			},
			stSNouns: {
			required: function(element){
			return ($("#stStatus").val()==3 || $("#stStatus").val()==4);
			}
			},
			stContactInfo: {
			required: function(element){
			return ($("#stStatus").val()==3 || $("#stStatus").val()==4);
			}
			},
			/*stLPolicies: {
			required: function(element){
			return ($("#stStatus").val()==3 || $("#stStatus").val()==4);
			}
			},*/
			/*stESblock: {
			required: function(element){
			return ($("#stStatus").val()==3 || $("#stStatus").val()==4);
			}
			},*/
			stLocationDir: {
			required: function(element){
			return ($("#stStatus").val()==3 || $("#stStatus").val()==4);
			}
			},
			stAboutStudo:{
            required: function(element){
						if(($("#stStatus").val()==3 || $("#stStatus").val()==4)) {
                         return true;
						 } else {
						 return false;
						 }
                        },

                         //minlength:10
         },

			stBioDesc:{
            required: function(element){
						if(($("#stStatus").val()==3 || $("#stStatus").val()==4)) {
                         return true;
						 } else {
						 return false;
						 }
                        },

                         //minlength:10
                    },
			stStatus:"required",
			stFbLink:{
			required: false,
			url: true
			},
			stInstaLink:{
			required: false,
			url: true
			},
			stTwtLink:{
			required: false,
			url: true
			},
			stPinLink:{
			required: false,
			url: true
			},
			/*stPGatway: {
			required: function(element){
			return ($("#stStatus").val()==3 || $("#stStatus").val()==4);
			}
			},*/
			
			
			},
			
			messages: {
					stName: {
					required: "Please enter studio name.",

					remote: jQuery.validator.format("{0} is already taken.")

					},
					stLogo: {required: "Studio logo is required",extension: "Image must be JPEG or PNG or JPG or GIF type" },
					slug : "Please enter slug",
					stImage: {required: "Studio Image is required",extension: "Image must be JPEG or PNG or JPG or GIF type" },
					stEmail : "Please enter valid email",
					stStreetAddress : "Please enter address line 1",
					//stAddress1 : "Please enter address",
					stCity : "Please enter city",
					stState : "Please enter state",
					stZip : "Please enter zip",
					stPhone : "Please enter phone",
					stOwner : "Please enter owner name",
					stContactInfo : "Please enter owner contact info",
					//stLPolicies : "Please enter policies",
					//stESblock : "Please enter email signature block",
					stLocationDir : "Please enter location direction",
					stAboutStudo : {
                        required:"Please enter about studio",
                        //minlength:"Please enter 10 characters"
                    },
					stBioDesc : {
                        required:"Please enter bio & description",
                        //minlength:"Please enter 10 characters"
                    },
					//stPGatway : "Please enter authorize.net payment gateway as per below instruction",
                },
				submitHandler: function(form) {
			jQuery('.loading').show(); 	
			$('#btnSave').attr("disabled", true); 
			//$("#stSNouns").attr("disabled", false); 
				
				var abtstu=editor.getData();
				var prbio=editor1.getData();
				//alert(editor.getData());
		
				var dataf = new FormData();
			var form_data = $('#frmaddupStudio').serializeArray();
           // alert (form_data); 
			
			$.each(form_data, function() {
			dataf.append(this.name, this.value);
			});

			dataf.append("stAboutStudo", abtstu);
			dataf.append("stBioDesc", prbio);
			
			var file_data = $("#img-upload").prop("files")[0];
			dataf.append("stImage", file_data) ;
			
			var file_data1 = $("#img-upload1").prop("files")[0];
			dataf.append("stLogo", file_data1) ;
			
			/*var files = $("#img-upload").get(0).files;
			if (files.length > 0) {
			dataf.append("stImage", files[0]);
			} else {
			dataf.append("stImage", "");
			}*/
		
			/*var files1 = $("#img-upload1").get(0).files;
			if (files1.length > 0) {
			dataf.append("stLogo", files1[0]);
			} else {
			dataf.append("stLogo", "");
			}*/
		//console.log(dataf);
		
		
			jQuery.ajax({
			url : '<?php echo base_url_site; ?>addon/ajaxUpdateStudio', 
			type : 'POST',              // Assuming creation of an entity
			mimeType: "multipart/form-data",
			contentType : false,        // To force multipart/form-data
			data : dataf,
			async: false,
       		cache: false,
			processData : false,
			success : function(response) {

						var data = JSON.parse(response);
						console.log(data);
						///$('.event-msg').html("");
						if(data.result==1){
						jQuery('.loading').hide(); 
						var id=data.sid;
						window.location.href="<?php echo base_url_site?>studiolocs";
						
						
						
						

						
						
						
						
						}
			
			
			}
    		});
				
				}

    		});

});
</script>

<?php
include('includes/footer.php');
?>
