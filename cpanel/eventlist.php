<?php 
include('includes/header.php');
$menuClssEvents = "downarrow";
$menuSbClssEvents = "sub";
$menuSbClssStyleEvents = "style='display:block;'";
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php"); exit; } 
if(isset($_REQUEST["delid"]) and $_REQUEST["delid"]!="") { 
	if(isset($_REQUEST["dtype"]) and $_REQUEST["dtype"]=="p") { 
		$db->where('id',$_REQUEST["delid"]);
		$db->delete('bb_event');
	} else if(isset($_REQUEST["dtype"]) and $_REQUEST["dtype"]=="r") { 
		$dataD=array("isDeleted"=>0);
		$db->where('id',$_REQUEST["delid"]);
		$db->update('bb_event',$dataD);
	} else {
		$dataD=array("isDeleted"=>1);
		$db->where('id',$_REQUEST["delid"]);
		$db->update('bb_event',$dataD);
	}
	$purl="";

	if(isset($_REQUEST["estatus"]) and $_REQUEST["estatus"]!="") { 
		$purl="?estatus=".$_REQUEST["estatus"];
	}

	$_SESSION["msg"]="Successfully deleted event";
	 if(isset($_REQUEST["dtype"]) and $_REQUEST["dtype"]=="r") { 
 $_SESSION["msg"]="Successfully restored event";
 } else {
 $_SESSION["msg"]="Successfully deleted event";
 }
	header("location: ".base_url_site."eventlist".$purl);
	exit;
	
}
?>
<!-- Header end-->
<link href="<?php echo base_url_css?>dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>

<!-- left nagivation end-->
<!-- left nagivation end-->
<!-- left content area start-->
<style>
.dataTables_processing { position:absolute; z-index: 1000; width:100%; top:40%;}
.loader-control{ position:relative;}
</style>
<div class="right-wrapper">
<h1 class="pageheading"><?php echo fnMultiStudioTitle($_SESSION["stId"]);?>Events
<?php 
if(isset($_REQUEST["estatus"]) && $_REQUEST["estatus"]!='') {
echo " - ".strtoupper($_REQUEST["estatus"]);
}
?>
</h1>
<div class="form-area">
<div class="listing-wrapper">
<div class="listing-table">
	<?php if(isset($_SESSION['msg']) && $_SESSION['msg']!="") {?>
	<script>
	setInterval(function(){ $('.event-msg').hide(); }, 3000);
	</script>
	<div class="event-msg" style="display:block;"><?php echo $_SESSION['msg']?></div>
	<?php }?>
	<?php
	if($_SESSION["curRole"]==1 || $_SESSION["curRole"]==2 || $_SESSION["urole"]==3) {?>
		<div class="elm-row mrgtom30" style="position: relative;z-index:99999;"><a class="elm-calendar bluelink" href="<?php echo base_url_site?>eventlistcal?studid=<?php echo $_SESSION["stId"];?>">View Calendar</a></div>
	<?php }?>
	
	<div id="download_img" style="text-align: center; position: fixed; display:none; z-index: 99999; left: 0; right: 0; top: 0; bottom: 0;" >
		<img src="<?php echo base_url_site;?>images/ajax-loader.gif"  style="position: relative;  top: 50%;"/ >
	</div>
	<div class="loader-control">
	<table width="100%" cellpadding="0" cellspacing="0" id="dataTables-example">
		<thead>
			<tr>
				<th>Action</th>
				<th>Status</th>
                <!--<th>View Event</th>-->
				<th>Featured Image</th>
				<th>Day of the Week</th>
				<th>Event Date</th>
				<th>Event Time</th>
				<th>Event Name</th>
				<th>Creator</th>
				<th>Bookings</th>
				<!--<th>Status</th>-->
				<?php if($_REQUEST["estatus"] == "trash"){ ?>
					<th></th>
				<?php } else {?>
					<th>Prep Report</th>
				<?php } ?>
			</tr>
		</thead>
	</table>
	</div>
</div>
</div>

</div>
</div>

<!-- left content area end-->
<script src="<?php echo base_url_css?>dataTables/jquery.dataTables.js"></script>
<script src="<?php echo base_url_css?>dataTables/dataTables.bootstrap.js"></script>
<script>
var type = '<?php echo $_REQUEST["estatus"];?>';
if(type == 'draft') {
  $('.draft1').addClass("active");
} else if(type == 'archive') {
  $('.archive').addClass("active");
}  else if(type == 'current') {
  $('.current').addClass("active");
}  else if(type == 'past') {
  $('.past').addClass("active");
}  else if(type == 'trash') {
  $('.trash1').addClass("active");
}  else if(type == '') {
  $('.allevent').addClass("active");
}

/*Function is used to export btn click*/
/*$('.exportBtn').on('click',function(event){ 
	var eventId = $(this).attr('customattr');
	window.location.href = "<?php echo base_url_site;?>bookingreport?eventId="+eventId ;
});	*/

$(document).on('click','.exportBtn1',function(){ 
	var SITEURL1 = "<?php echo base_url_site;?>";
	var eventId = $(this).attr('customattr');
	$('#download_img').show(); 
	//window.location.href = "<?php echo base_url_site;?>eventprepreport?eventId="+eventId ;
	$.ajax({
		type: "POST",
		url: "<?php echo base_url_site;?>eventprepreport?eventId="+eventId,
		success: function(msg){
			$('#download_img').hide(); 
			var data = JSON.parse(msg);
			//window.open(SITEURL1+"reports/"+data.filename, '_blank','width='+windowWidth+', height='+windowHeigth+'');
			window.open(SITEURL1+"reports/"+data.filename, '_self');
		}
	});
});	

$(document).ready(function() {
	vtable = $('#dataTables-example').DataTable( {
		"dom": '<"elm-row"<"bubbleInfo"<"fl spage"<"paging"pl><"clear">><"fr page-text"i>>>rt',
		"processing": true,
		"language": {
			processing: "<div style='text-align:center'><img src='<?php echo base_url_site;?>images/ajax-loader.gif'></div>",
		},
		"serverSide": true,
		"ajax":{
			url :"<?php echo base_url_site; ?>addon/event-grid-data", // json datasource
			data: function (d) {
                d.estatus = "<?php echo $_REQUEST["estatus"];?>";
            },
			type: "post",  //method,by default get
			error: function(){  //error handling
				$(".dataTables-example-error").html("");
				$("#dataTables-example").append('<tbody class="dataTables-example-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
				$("#dataTables-example_processing").css("display","none");
			}
		},
		<?php if($_REQUEST['estatus'] == ''){ ?>
			order: [[ 4, "desc" ]],
		<?php } if($_REQUEST['estatus'] == 'draft'){ ?>
			order: [[ 4, "desc" ]],
		<?php } if($_REQUEST['estatus'] == 'trash'){ ?>
			order: [[ 4, "desc" ]],
		<?php } if($_REQUEST['estatus'] == 'current'){ ?>
			order: [[ 4, "asc" ]],
		<?php } if($_REQUEST['estatus'] == 'past'){ ?>
			order: [[ 4, "desc" ]],
		<?php } ?>
		pagingType: "full_numbers",
		sortable: false,
		paginate: true,
		pageLength: 50,
		info: true,
		bSort: true,
		bFilter: false,
		"aaSorting": [],
		"aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0,2,3,7,8,9 ] } ]
	} );

	$(document).on('click','.del',function(){  
		var delid=$(this).attr('id');
		var dtype="";
		var durl="";
		var stxt="";
		dtype=$(this).attr('data-type');
		if(dtype=="p") {
			durl="&dtype="+dtype;
			stxt="permanent";
		} else if(dtype=="r") {
			stxt="restore";
			durl="&dtype="+dtype;
		} else {
			stxt="delete";
		}
		var dlg='<p>Would you like to '+stxt+' this event </p>';
		doConfirm(dlg, function yes() {
			//Delete all bookings
			delAllBookingsWRTEventId(delid,dtype);
			setTimeout(function(){ 
				window.location.href="<?php echo base_url_site?>eventlist?delid="+delid+durl+"&estatus=<?php echo $_REQUEST["estatus"];?>";
			}, 3000);
		}, function no() {
           
        });
	});

	//Function is used to delete all bookings on soft or hard delete the event
	function delAllBookingsWRTEventId(delid,dtype){
		$.ajax({
			url : '<?php echo base_url_site; ?>addon/ajaxDelAllBookingsWRTEventId',
			type: 'post',
			data: {eventId:delid,dtype:dtype},
			success: function(data) {
			},
			error: function() {
				alert('There has been an error.');
			}
		});
	}
});
</script>
<?php
include('includes/footer.php');
?>