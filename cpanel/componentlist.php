<?php 
include('includes/header.php');
$menuClssProject = "downarrow";
$menuSbClssProject = "sub";
$menuSbClssStyleProject = "style='display:block;'";

$menuClssProjecttem8 = "active";
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php"); exit; } 
?>
<!-- Header end-->
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<!-- left nagivation end-->
<!-- left content area start-->
<div class="right-wrapper">

<?php 
/*echo date_default_timezone_get();

$params = array('');
$sql = "SELECT NOW(),CURDATE()";
$crnt = $db->rawQuery($sql,$params);
echo "<pre>crnt==";print_r($crnt);*/
?>
<?php if( $_SESSION["urole"]==1 || $_SESSION["urole"]==2){ ?>
			
<h1>
	<button class="enbl-btn actionEvent" id="AddComponent" alt="Add Component" title="click for Add Component">Add Component</button>
</h1>	
<?php  }

else{
 ?>
<h1 class="pageheading"><?php echo fnMultiStudioTitle($_SESSION["stId"]);?>All</h1> <!--NEW-->

<?php } ?>
			
	<script>
	var type = '<?php echo $_REQUEST["type"];?>';
	if(type == 'draft') {
		$('.draftProject').addClass("active");
	} else if(type == 'archive') {
		$('.archiveProject').addClass("active");
	} else if(type == '') {
		$('.allProject').addClass("active");
	}
	</script>
	
	<style>
	.project-msg,.project-del-msg{color:#6fcf00; font-weight:bold; text-align:center;margin-bottom: 20px;}
	</style>
	<?php
	if(isset($_SESSION['msg']) && $_SESSION['msg']!="") {?>
		<script>
		setInterval(function(){ $('.project-msg').hide(); }, 3000);
		</script>
		<div class="project-msg" style="display:block;"><?php echo $_SESSION['msg'];?></div>
	<?php }?>
	<div class="project-del-msg" style="display:none;"></div>
	
	<div id="download_img" style="text-align: center; position: fixed; display:none; z-index: 99999; left: 0; right: 0; top: 0; bottom: 0;" >
		<img src="<?php echo base_url_site;?>images/ajax-loader.gif"  style="position: relative;  top: 50%;"/ >
	</div>
	<div class="form-area">
		<div class="event-listing-module">
			<div class="tabFilterRow"><ul></ul></div>
			<style>
			.tabFilterRow{float:left; width:100%;}
			.tabFilterRow ul{float:left; margin:0px; padding:0px; list-style:none; margin-bottom:15px;}
			.tabFilterRow ul li{display: inline-block; margin: 0 10px 10px 0px; padding: 10px 15px; color: #4a4a4a; text-decoration: none; border: 1px solid #c4c4c4; text-transform: uppercase; background-color: #fff; font-weight: 600; font-size: 13px; cursor: pointer;}
			.tabFilterRow ul li:hover { background-color: #333333; color: #fff; }
			.tabFilterRow ul li.active { background-color: #e1e1e1;}
			.tabFilterRow ul li.active:hover { background-color: #333333; color: #fff;}
			</style>
			
			<div class="elm-control">
				<div class="elm-row">
					<table class="elm-table" id="dataTables-example">
						<thead>
						<tr>
							<th>Status</th>
							<th>Featured Image</th>
							<th>Sku</th>
							<th>Component Name</th>
							<?php if( !($_SESSION['urole']==3))
		{?>
							<th>Optional</th>
							
		<?php  }?>
							<th>Price</th>
						</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<link href="<?php echo base_url_css?>dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<script src="<?php echo base_url_css?>dataTables/buttons/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url_css?>dataTables/buttons/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url_css?>dataTables/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url_css?>dataTables/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script>
$(document).ready(function(){ 


	vtable = $('#dataTables-example').DataTable( {
		"dom": '<"elm-row"<"bubbleInfo"<"fl spage"<"paging"pl><"clear">><"fr page-text"i>>>rt',
		"processing": true,
		"language": {
			processing: "<div style='text-align:center'><img src='<?php echo base_url_site;?>images/ajax-loader.gif'></div>",
		},
		"serverSide": true,
		"ajax":{
			url :"<?php echo base_url_site; ?>addon/component-grid-data.php", // json datasource
			type: "post",  //method,by default get
			error: function(){  //error handling
				$(".dataTables-example-error").html("");
				$("#dataTables-example").append('<tbody class="dataTables-example-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
				$("#dataTables-example_processing").css("display","none");
			}
			},
		order: [[ 3, "asc" ]],
		pagingType: "full_numbers",
		sortable: false,
		paginate: true,
		pageLength: 50,
		info: true,
		bSort: true,
		bFilter: false,
		"aaSorting": [],
		"aoColumnDefs": [  {
			<?php if(($_SESSION['urole']==3))
		{ ?>
							  "targets": [0,1],
							
		<?php  } else { ?>
        "targets": [0,1,4],
		
		<?php } ?>
        "orderable": false
} ],
		
	} );
	$(document).on('click','#ComPriceOption',function(){  
		
		var compid = $(this).attr('coid');
		var opval;
		var test;
		test=$(this);
	if(this.checked) { 
		opval=1; //set value in hidden
	} else { 
		opval=0; //set value in hidden field
	}
	
	var elm = $(this);
		var dlg ='<p>Would you like to change Price Optional value</p>';

		doConfirm(dlg, function yes() {
			$.ajax({
				url : '<?php echo base_url_site; ?>addon/ajaxchangePriceOptional.php',
				type: 'post',
				data: {opval:opval,compid:compid},
				success: function(data) {
					
				    var odata = $.parseJSON(data);
					vtable.row( elm.parents('tr') ).remove().draw(false);
					$('html, body').animate({scrollTop: '0px'}, 300);//Scroll top
					$('.project-del-msg').css("display","block");
					$('.project-del-msg').html(odata.msg);
					setTimeout(function(){ 
						$('.project-del-msg').css("display","none");
						$('.project-del-msg').html(""); 
					}, 2000);
				},
				error: function() {
					alert('There has been an error.');
				}
			});
			
			
		}, function no() {
			if(opval==1) { 
		            test.prop("checked", false);
	     } else { 
		     test.prop("checked", true);
		 }
			
			return false;
		});
	
	
});

$(document).on("click","#AddComponent",function() { 
	<?php if($_REQUEST["id"] != ''){ //edit  case ?>
		var status = '<?php echo $ritems['status'];?>';
		if(status == '0' || status == '1'){ //active or inactive
			window.location.href="<?php echo base_url_site ?>addupdcomponent";
		}
	<?php } else { //add  case?>
		window.location.href="<?php echo base_url_site ?>addupdcomponent";
	<?php }?>
});

	

	
	
	$(document).on("click", ".btCircle", function(){
		var ap;
		var spp;
		spp=$(this);
		var ids=($(this).attr('id'));
		$(this).parent().toggleClass("on");
		ap=$(this).attr('status');
		if(ap==0)
		{
			ap=1;
			$(this).attr('status',1);
		}
		else{
			
			ap=0;
			$(this).attr('status',0);
		}
		var elm = $(this);
		var dlg ='<p>Would you like to change component status </p>';
	//	doConfirm(dlg, function yes() {
			$.ajax({
				url : '<?php echo base_url_site; ?>addon/ajaxchangeStatusOptional.php',
				type: 'post',
				data: {ids:ids,ap:ap},
				success: function(data) {
					
				    var odata = $.parseJSON(data);
					vtable.row( elm.parents('tr') ).remove().draw(false);
					$('html, body').animate({scrollTop: '0px'}, 300);//Scroll top
					$('.project-del-msg').css("display","block");
					$('.project-del-msg').html(odata.msg);
					setTimeout(function(){ 
						$('.project-del-msg').css("display","none");
						$('.project-del-msg').html(""); 
					}, 2000);
				},
				error: function() {
					alert('There has been an error.');
				}
			});
			
			
		//}, function no() {
			
			
			
			//if(ap==1)
		//{
			
			//spp.attr('status',0);
		//}
		//else{
	
			//spp.attr('status',1);
		
		//}
			//spp.parent().toggleClass("on");
			
			
			
			
			//return false;
		//});
	
	
});



/////-----------------------------------------------------
	
	$(document).on("click", ".btCirclestu", function(){
		var ap;
		var spp;
		spp=$(this);
		var ids=($(this).attr('id'));
		var price=($(this).attr('price'));
		var flag=($(this).attr('flag'));
		
		$(this).parent().toggleClass("on");
		ap=$(this).attr('status');
		if(ap==0)
		{
			ap=1;
			$(this).attr('status',1);
		}
		else{
			
			ap=0;
			$(this).attr('status',0);
		}
		var elm = $(this);
		var dlg ='<p>Would you like to change component status </p>';
	//	doConfirm(dlg, function yes() {
			$.ajax({
				url : '<?php echo base_url_site; ?>addon/ajaxchangeStatusOptional.php',
				type: 'post',
				data: {ids:ids,ap:ap,sta:1,price:price,flag:flag},
				success: function(data) {
					
				    var odata = $.parseJSON(data);
					vtable.row( elm.parents('tr') ).remove().draw(false);
					$('html, body').animate({scrollTop: '0px'}, 300);//Scroll top
					$('.project-del-msg').css("display","block");
					$('.project-del-msg').html(odata.msg);
					setTimeout(function(){ 
						$('.project-del-msg').css("display","none");
						$('.project-del-msg').html(""); 
					}, 2000);
				},
				error: function() {
					alert('There has been an error.');
				}
			});
			
			//If category count=1 then remove the same category
			
			
		//}, function no() {
			
			//if(ap==1)
		//{
			
			//spp.attr('status',0);
		//}
		//else{
	
			//spp.attr('status',1);
		
		//}
			//spp.parent().toggleClass("on");
			
			//return false;
			
		//});
	
	
});

////-------------------------------------------------------
$(document).on("keypress",".number-only",function(e) {

 if ((e.which != 46 || self.val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {


return false;
}
});




$(document).on("blur",".compricesave",function() {
	var sts=$(this);
	if (!$.isNumeric(this.value))
        this.value = 0;
	    var priceupdate=1;
		var compid=($(this).attr('compid'));
		var flag=($(this).attr('flag'));
	    var price=$(this).val();
	
var elm = $(this);
		var dlg ='<p>Would you like to change Price value </p>';
		if(sts.attr('pic')!=$(this).val())
		{
		doConfirm(dlg, function yes() {
			$.ajax({
				url : '<?php echo base_url_site; ?>addon/ajaxchangeStatusOptional.php',
				type: 'post',
				data: {priceupdate:priceupdate,compid:compid,flag:flag,price:price},
				success: function(data) {
					 sts.val(price);
					 sts.attr('pic',price);
				    var odata = $.parseJSON(data);
					vtable.row( elm.parents('tr') ).remove().draw(false);
					$('html, body').animate({scrollTop: '0px'}, 300);//Scroll top
					$('.project-del-msg').css("display","block");
					$('.project-del-msg').html(odata.msg);
					setTimeout(function(){ 
						$('.project-del-msg').css("display","none");
						$('.project-del-msg').html(""); 
					}, 2000);
				},
				error: function() {
					alert('There has been an error.');
				}
			});
			


 }, function no() {
			 
			 sts.val(sts.attr('pic'));
			 
			 
		 });
		}
});




});
</script>
<!-- left content area end-->
<?php
include('includes/footer.php');
?>