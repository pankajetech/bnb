<?php 
include('includes/functions.php');
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: ".base_url_site."index.php");    exit; } 
global $db;
//$eventId = $_POST['eventId'];
$eventId = $_REQUEST['eventId'];

$strHTML = "";
$strHTML.='<div class="right-wrapper">
<table border="1">
<tr><th><h1 class="pageheading">Board & Brush - '.getStudioCityWRTEventId($eventId).'</h1></th></tr>
<tr><td>
<div class="form-area">
<form><div class="event-listing-module">
<div class="elm-control">
<div class="elm-row">';
	$results1 = getEventDetailWRTEventId($eventId);
	if(!empty($results1)){
		$strHTML.='<table class="elm-table firstTbl" border="1">
					<tr>
						<th>Event Name</th>
						<th>Event Date</th>
						<th>Event Start Time</th>
						<th>Total Seats</th>
						<th>Total Projects</th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
					<tr>
						<td align="center">'.$results1['evTitle'].'</td>
						<td align="center">'.date('l,n-j-Y',strtotime($results1['evDate'])).'</td>
						<td align="center">'.$results1['evStTime'].'</td>
						<td align="center">'.$results1['evBookTicket'].'</td>
						<td align="center">'.$results1['evBookTicket'].'</td>
						<td align="center"></td>
						<td align="center"></td>
						<td align="center"></td>
					</tr>
				</table>';
	}
	

		//Get booking  info
		$sql = "SELECT bo.cOrderNo,bo.id as bookingIdVal,bo.eventLocationId,bo.boProjects,bo.boFullName,
		bo.boPhone,bo.boEmail,bo.bookingCreationDate,bo.totalTickets,
		botckt.boTicketProjectName,botckt.boProjectId,botckt.id as bookingTicketId,botckt.boTicketProjectImage,botckt.boTicketSitBy
		FROM `bb_booking` as bo 
		right join `bb_booktcktinfo` as botckt
		on bo.id = botckt.bookingId
		where bo.eventId='".$eventId."' and bo.isDeleted=0 and (bo.status=0 OR bo.status=3)";
		
		$results =  $db->rawQuery($sql);
		//echo "<pre>results11=";print_R($results);die;
		if(count($results)>0){
			$strHTML.='
				<br><table class="elm-table" border="1">
				<tr>
					<th>Customer</th>
					<th>Contact</th>
					<th>Project Selection</th>
					<th>Personalization</th>
					<th>Seating Request</th>
					<th>Supplies</th>
					<th>Qty</th>
					<th>Sample</th>
				</tr>';
			
			$numItems = count($results);
			$kk=2;
			$target_array1 = array();
			for($zz=0;$zz<$numItems;$zz++) {
				if($zz==0){
					$results[$zz]['seatNo'] = 1;
				}
				if($zz>0){
					$currentIterationBookingId = $results[$zz]['cOrderNo']; //booking id of current iteration
					$oldIterationBookingId = $results[$zz-1]['cOrderNo']; //booking id of last iteration 
					if($currentIterationBookingId == $oldIterationBookingId ){
						$target_array1[] = $zz;
						$results[$zz]['seatNo'] = $kk;
						$kk++;
					} else {
						$results[$zz]['seatNo'] = 1;
						$kk=2;
					} 
				}
			}
			foreach($target_array1 as $key1=>$val1){
				$results[$val1]['cOrderNo'] = '';
				$results[$val1]['boPhone'] = '';
				$results[$val1]['boEmail'] = '';
				$results[$val1]['bookingCreationDate'] = '';	
			}
			//echo "<pre>results22=";print_R($results); die;
			foreach ($results as $key=>$row) { 
				$strHTML.='<tr>
				<td class="intabletd" colspan="8">
				<table class="intable" border="1">';
				
				$persInfoArr = getTicketProjectPersonalizeInfoWRTBookingId($row['bookingIdVal'],$row['bookingTicketId']);
				$persInfoArrCnt = count($persInfoArr);
				
				$specInfoArr = getprojectSpecInfoWRTBookingId($row['boProjectId']);
				$specInfoArrCnt = count($specInfoArr['supplies']);
				
				$strHTML.='<tr>
						<td>'.$row['boFullName'].'</td>
						<td>'.$row['boPhone'].'</td>
						<td>'.$row['boTicketProjectName'].'</td>
						<td>'.$persInfoArr[0].'</td>
						<td>'.$row['boTicketSitBy'].'</td>
						<td>'.$specInfoArr['supplies'][0].'</td>
						<td align="center">'.$specInfoArr['qty'][0].'</td>
						<td><img width="50" height="50" src="'.base_url_site.'/uploads/projects/'.$row['boTicketProjectImage'].'"/></td>
					</tr>
					
					<tr>
						<td>Seat '.$row['seatNo'].'</td>
						<td>'.$row['boEmail'].'</td>
						<td></td>
						<td>'.$persInfoArr[1].'</td>
						<td></td>
						<td>'.$specInfoArr['supplies'][1].'</td>
						<td align="center">'.$specInfoArr['qty'][1].'</td>
						<td></td>
					</tr>
					
					<tr>
						<td>'.$row['seatNo'].' of '.$row['totalTickets'].'</td>
						<td></td>
						<td></td>
						<td>'.$persInfoArr[2].'</td>
						<td></td>
						<td>'.$specInfoArr['supplies'][2].'</td>
						<td align="center">'.$specInfoArr['qty'][2].'</td>
						<td></td>
					</tr>
					
					<tr>
						<td>'.$row['cOrderNo'].'</td>
						<td></td>
						<td></td>
						<td>'.$persInfoArr[3].'</td>
						<td></td>
						<td>'.$specInfoArr['supplies'][3].'</td>
						<td align="center">'.$specInfoArr['qty'][3].'</td>
						<td></td>
					</tr>';
					if($row['bookingCreationDate'] != '' ) { 
							$bookingCreationDate = date('n-j-Y',strtotime($row['bookingCreationDate']) ) ;
						} else {
							$bookingCreationDate = '';
						}
					$strHTML.='<tr>
						<td align="left">'.$bookingCreationDate.'</td>
						<td></td>
						<td></td>
						<td>'.$persInfoArr[4].'</td>
						<td></td>
						<td>'.$specInfoArr['supplies'][4].'</td>
						<td align="center">'.$specInfoArr['qty'][4].'</td>
						<td></td>
					</tr>
					<tr height="1" fill="#000"></tr>';
					
				if($persInfoArrCnt >=5 || $specInfoArrCnt >=5){
					if($persInfoArrCnt > $specInfoArrCnt ){
						$counter = $persInfoArrCnt;
					} else if($persInfoArrCnt < $specInfoArrCnt) {
						$counter = $specInfoArrCnt;
					} else {
						$counter = $persInfoArrCnt;
					}
					for($i=5;$i<$counter;$i++){
						$strHTML.= '<tr>
							<td></td>
							<td></td>
							<td></td>
							<td>'.$persInfoArr[$i].'</td>
							<td></td>
							<td>'.$specInfoArr['supplies'][$i].'</td>
							<td>'.$specInfoArr['qty'][$i].'</td>
							<td></td>
						</tr>';
					}
				}
			$strHTML.='</table>
			</td>
		</tr>';
	} //end foreach
	} //end iff
	
$strHTML.='</table>
</div>
</div>
</div>';
	$woodSummaryTbl = getWoodPlanSummaryWRTEventId($eventId); 
	if(isset($woodSummaryTbl) && !empty($woodSummaryTbl) ) {
		
		$strHTML.='<div class="wood-plan-summary-box">
		<p><b>Wood Planning Summary :</b></p>
		<table class="wood-plan-summary" border="1" >
			<tr>';
		foreach($woodSummaryTbl as $key5=>$val5){
			$strHTML.='<th>'.$val5['event'].'</th>';
		}
		$strHTML.='</tr>';
		$strHTML.='<tr>';
		foreach($woodSummaryTbl as $key5=>$val5){
			$strHTML.='<td align="center">'.$val5['budget'].'</td>';
		}
		$strHTML.='</tr>';
	}
$strHTML.='</table>';
$strHTML.='<h4 style="width:100%;" align="center">Notes</h4>			
<p style="width:100%;" align="center">Studio focused report			<br>
Prefer this to be in a printable format on 8.5 X 11 paper if possible	<br>		
Wood Planning Summary (supply list) at the end of Registrations	</p>';
$strHTML.='</div>
</form>
</div>
</div>
</td></tr>
</table>
<!-- left content area end-->
<style>
.pfp-outer{float:left; width:100%;}
.pfp-outer ul{float:left; width:100%; margin:0; padding:0; list-style:none;}
.pfp-heading{float:left; width:100%; margin-bottom: 15px; background-color: #434343; font-size: 16px; padding: 8px 5px; text-align: left; color: #fff;}
.pfp-heading label{width:15%; margin-right:1.5%;}
.pfp-outer ul li{float:left; width:100%; margin-bottom:15px;}
.pfp-heading{float:left; width:100%;}
.width-70{float:left; width:70%;}
.width-70 input{width:100%;}
.intable{float:left; width:100%;}
.intable tr{background:transparent!important;}
.intable td{padding:5px;}

.wood-plan-summary-box{float:left; width:100%;}
.wood-plan-summary{width:100%; border-collapse:collapse; box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box;}
.wood-plan-summary tr td{padding:5px; text-align:center;}
.wood-plan-summary tr th{padding:5px; color:#fff; background:#434343;}
.wood-plan-summary tr:nth-child(odd){background:#e9e9e9;}
</style>';
$prepsheetFileName = 'Prepsheet_'.date("m-d-Y").'.xls';
$output = file_put_contents('export/'.$prepsheetFileName,$strHTML);

//download script
//$file = "event_prep_report.xls";
//$file = 'Prepsheet_'.date("m/d/Y").'.xls';
$download_path = 'export/'.$prepsheetFileName;
$file_to_download = $download_path; // file to be downloaded
header("Expires: 0");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");  header("Content-type: application/file");
header('Content-length: '.filesize($file_to_download));
header('Content-disposition: attachment; filename='.basename($file_to_download));
readfile($file_to_download);
unlink($file_to_download); //delete file from path
exit;
?>