<?php include('includes/header.php');
$menuClssStudio = "downarrow";
$menuSbClssStudio = "sub";
$menuSID = $_REQUEST["stId"];
$menuSbClssStudio1 = "active";

$menuSbClssStyleStudio = "style='display:block;'";

global $db;
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 
$_SESSION["studid"]=$_REQUEST["studid"];
$_SESSION["stidloc"]=$_REQUEST["studid"];
$_SESSION["stId"]=$_REQUEST["stId"];

$params = array('');

$result = $db->rawQueryOne("SELECT * FROM `bb_studiolocation` where (id='".$_REQUEST["studid"]."' or id='".$_REQUEST["stId"]."')  and isDeleted=0 order by id desc", $params);
$ritems = (array)$result;

		if(!empty($ritems)) { 
		$ritems=$ritems; 
		
		}

	 ?>

<!-- Header end-->


<!-- left nagivation start-->

<?php include('includes/left-sidebar.php');?>

<!-- left nagivation end-->

<!-- left content area start-->

<style>
.centertxt input[type="text"], .centertxt textarea{text-align:center;}
</style>


<div class="right-wrapper">
<h1 class="pageheading">Location Info</h1>
<div class="form-area">

<div class="location-info-module centertxt">

<div class="locationinfo-row"><input type="text" placeholder="Studio Name" readonly="readonly" value="<?php echo @fnCheckEmpty($ritems['stName']); ?>"></div>

<div class="locationinfo-row">
<div class="locationinfo-left">
<strong>Studio Contact info</strong><Br />
<?php if(isset($ritems['stStreetAddress']) && $ritems['stStreetAddress']!="") {
echo "<strong>Address :</strong> ".$ritems['stStreetAddress'];
}
if(isset($ritems['stAddress1']) && $ritems['stAddress1']!="") {
echo " ".$ritems['stAddress1'];
}
if(isset($ritems['stCity']) && $ritems['stCity']!="") {
echo ", ".$ritems['stCity'];
}
if(isset($ritems['stState']) && $ritems['stState']!="") {
echo ", ".$ritems['stState'];
}
if(isset($ritems['stZip']) && $ritems['stZip']!="") {
echo " ".$ritems['stZip'];
}
if(isset($ritems['stEmail']) && $ritems['stEmail']!="") {
echo "<Br><strong>Email :</strong> ".$ritems['stEmail'];
}
if(isset($ritems['stPhone']) && $ritems['stPhone']!="") {
echo "<Br><strong>Phone :</strong> ".$ritems['stPhone'];
}

?>
</div>
<div class="locationinfo-right"><div class="locationinfo-row"><input type="text" placeholder="Owner" value="Owner" readonly="readonly" value=""></div><input type="text" placeholder="Owner Contact Info" value="Owner Contact Info" readonly="readonly" value=""></div>
</div>

<div class="locationinfo-row" style="text-align:center;"><strong>Location Description</strong><Br />
<?php echo @fnCheckEmpty($ritems['stAboutStudo']); ?></div>

<div class="locationinfo-row">
<div class="locationinfo-left"><textarea rows="2" placeholder="Location Policies" readonly="readonly"></textarea></div>
<div class="locationinfo-right"><textarea rows="2" placeholder="Studio Email Signature Block" readonly="readonly"></textarea></div>
</div>

<div class="locationinfo-row">
<div class="location-image-upload-section">
<div class="location-image-upload">
<div class="gallery-img location-img-upload">
<?php 
if(isset($ritems['stOwnerPic']) && $ritems['stOwnerPic']!=""){
			$imageurl=base_url_site."uploads/studio/".$ritems['stOwnerPic'];
			$imageName=$ritems['stOwnerPic'];
			 }  else  {
			 //$imageurl=base_url_site."images/placeholder.jpg";
			 $imageurl=base_url_images."placeholder.jpg";
			 $imageName="";
	}
?>
<img id="blah" src="<?php echo $imageurl;?>" alt="">
</div>
</div>
<div class="location-personal-bio-section">
<strong>Personal Bio</strong><Br />
<?php echo @fnCheckEmpty($ritems['stBioDesc']); ?></div>
</div>
</div>


<div class="locationinfo-row"><input type="text" placeholder="Location Direction" value="Location Direction" readonly="readonly"></div>

<div class="locationinfo-row">
<div class="locationinfo-left"><input type="text" placeholder="Facebook"  readonly="readonly" value="<?php echo @fnCheckEmpty($ritems['stFbLink']); ?>"></div>
<div class="locationinfo-right"><input type="text" placeholder="Instagram"  readonly="readonly" value="<?php echo @fnCheckEmpty($ritems['stInstaLink']); ?>"></div>
</div>

<div class="locationinfo-row">
<div class="locationinfo-left"><input type="text" placeholder="Twitter"  readonly="readonly" value="<?php echo @fnCheckEmpty($ritems['stTwtLink']); ?>"></div>
<div class="locationinfo-right"><input type="text" placeholder="Pinterest"  readonly="readonly" value="<?php echo @fnCheckEmpty($ritems['stPinLink']); ?>"></div>
</div>

<div class="locationinfo-row"><input type="text" placeholder="Payment Gateway" value="Payment Gateway" readonly="readonly" value=""></div>

</div>

</div>

</div>

<!-- left content area end-->


<?php
include('includes/footer.php');
?>
