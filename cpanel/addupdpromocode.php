<?php 
include('includes/header.php');
$menuClssPromocode = "downarrow";
$menuSbClssPromocode = "sub";
$menuSbClssStylePromocode = "style='display:block;'";
$menuClssPromocodetem2 = "active";
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php"); exit; } 
//if(!isset($_SESSION['studid']) ){ header("location: index.php"); exit; } 
//echo "<pre>";print_r($_SESSION);
if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") {    
	$params = array('');
	//if($_SESSION['urole'] == 3){
	if(!isset($_SESSION['curRole']) ) {  //studio user
		$sql = "SELECT * FROM  bb_promocode WHERE id='".$_REQUEST["id"]."' and FIND_IN_SET(".$_SESSION["stId"].",`studioId`) ";
		//SELECT * FROM  bb_promocode WHERE id='".$_REQUEST["id"]."' ;
	} 
	else { //corp admin and corp user
		//$sql = "SELECT * FROM  bb_promocode WHERE id='".$_REQUEST["id"]."' and studioId IN(".$_SESSION["stidloc"].") ";
		$sql = "SELECT * FROM  bb_promocode WHERE id='".$_REQUEST["id"]."' and FIND_IN_SET(".$_SESSION["stId"].",`studioId`) ";
	}
	$result = $db->rawQueryOne($sql,$params);
	$ritems = (array)$result;
	if(!empty($ritems)) {
		$ritems = $ritems;
	}
} else {
	$ritems = array();
	$ritems = $ritems;
}
?>
<!-- Header end-->
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<!-- left nagivation end-->
<!-- Date Time Picker Validation-->
<link href="<?php echo base_url_css?>datepicker/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url_js?>datepicker/jquery-ui.js"></script>
<link href="<?php echo base_url_css?>jquery.timepicker.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url_js?>jquery.timepicker.js"></script>

<!-- ToolTip Type Validation-->
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>bootstrap-3.3.5.min.css">
<script type="text/javascript" src="<?php echo base_url_js?>bootstrap-3.3.5.min.js" /></script>
<script type="text/javascript" src="<?php echo base_url_js?>jquery-validate.bootstrap-tooltip.min.js" /></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>select2.min.css">
<script type="text/javascript" src="<?php echo base_url_js?>select2.min.js" /></script>
<!-- left content area start-->
<div class="loading" style="display:none;">Loading&#8230;</div>
<div class="right-wrapper">
	<h1 class="pageheading"><?php echo fnMultiStudioTitle($_SESSION["stId"]);?><?php if($_REQUEST["id"] != "") { echo "Coupon Code"; } else { echo "Create New Coupon Code"; } ?></h1>
	<?php //echo "<pre>";print_r($_SESSION);?>
	<div class="form-area">
		<form role="form" method="post" action="" id="addUpdPromoFrm">
			<div class="bnt-row">
				<div class="enbdis">
					<?php 
					if(isset($_REQUEST["id"]) && $_REQUEST["id"]!="") {  //Active
						if($ritems['status'] == 0){ //Active?>
							<button class="enbl-btn actionEvent" id="disableAction" alt="click for disable" title="click for disable">Disable</button>
						<?php 
						} else if($ritems['status'] == 1) { //Deactive ?>
							<button class="enbl-btn actionEvent" id="enableAction" alt="click for enable" title="click for enable">Enable</button>
						<?php } else { ?>
							<button class="enbl-btn actionEvent" id="disableAction" alt="click for disable" title="click for disable">Disable</button>
						<?php
						}
					} 
					if(!isset($_REQUEST["id"])){ // add case?>
						<button class="dsbl-btn actionEvent" id="enableAction" disabled alt="After save, you can use this" title="After save, you can use this">Enable</button>
					<?php 
					}?>
				</div>
				
				<?php 
				//edit case
				if(isset($_REQUEST["id"]) && $_REQUEST["id"]!="") { 
					if($ritems['status'] == 3){ //Archive?>
						<button class="yellow-btn actionEvent" id="enableAction" alt="click for Un-archive" title="click for Un-archive">Un-Archive</button>
					<?php 
					} else { ?>
						<button class="yellow-btn actionEvent" id="archiveAction" alt="click for archive" title="click for archive" >Archive</button>
					<?php 
					} 
				} 
				if(!isset($_REQUEST["id"])){ //add case?>
					<button class="dsbl-btn actionEvent" id="archiveAction" disabled alt="After save, you can use this" title="After save, you can use this">Archive</button>
				<?php 
				}?>
			</div>
			<div class="bnt-row">
				<div class="btn-left">
					<?php 
					if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { //update case
						if($_REQUEST["type"]== "archive"){ //update case in archive case
						?>
							<button class="yellow-btn actionEvent" id="updateArchiveAction" type="submit">Update Changes</button>
					
						<?php
						} 
						else { //update case in normal case
						?>
							<button class="yellow-btn actionEvent" id="updateAction" type="submit">Update Changes</button>
					
						<?php
						}
					?>
					<?php } else { //add case?>
						<button class="yellow-btn actionEvent" id="addAction" type="submit">Save</button>
					<?php } ?>
				</div>
				<div class="btn-right"> 
					<button type="reset" id="btnCancel" class="grey-btn btn-mrg-right">Cancel</button>
				</div>
				
			</div>

			<div class="promo-section-first">
				<?php 
				//edit case
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { ?>
					<input type="hidden" name="actionType" value="updateAction" id="actionType">
				<?php 
				} 
				if(!isset($_REQUEST["id"])) { //add case?>
					<input type="hidden" name="actionType" value="" id="actionType">
				<?php } ?>
				
				<?php 
				if(!isset($_SESSION['curRole']) ) {  //studio user?>
					<input type="hidden" name="userId" value="<?php echo $_SESSION['usrid'];?>">
				<?php } else { //corp user and corp admin?>
					<input type="hidden" name="userId" value="<?php echo $_SESSION['loggedin'];?>">
				<?php }?>
				
				<?php 
				if(!isset($_SESSION['curRole']) ) {  //studio user?>
					<input type="hidden" name="userRole" value="<?php echo $_SESSION['urole'];?>">
				<?php } else { //corp user and corp admin?>
					<input type="hidden" name="userRole" value="<?php echo $_SESSION['curRole'];?>">
				<?php }?>
				
				<?php 
				if(!isset($_SESSION['curRole']) ) {  //studio user 
					if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { //edit page?>
						<input type="hidden" name="studid" id="studid1" value="<?php echo fnCheckEmpty($ritems['studioId']);?>">
					<?php 
					} else { //add page?>
						<input type="hidden" name="studid" id="studid1" value="<?php echo $_SESSION['stId'];?>">
					<?php 
					}
				} 
				else { //corp user or admin
					if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { //edit page ?>
						<input type="hidden" name="studid" id="studid1" value="<?php echo fnCheckEmpty($ritems['studioId']);?>">
					<?php 
					} else { //add page?>
						<input type="hidden" name="studid" id="studid1" value="<?php echo $_SESSION['stId'];?>">
						<!--<input type="text" name="studid" id="studid1" value="<?php //echo $_SESSION['stId'];?>">-->
					<?php 
					}
				} 
				 
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { ?>
					<input type="hidden" name="promoId" id="promoId" value="<?php echo $_REQUEST["id"];?>">
				<?php } ?>
				
				<div class="promo-row">
					<div class="promo-row-70"><div class="floatingBox"><input type="text" name="promoName" id="promoName" placeholder="" value="<?php echo fnCheckEmpty($ritems['promoName']);?>"/><label class="floating-lab">Promotion Name</label></div></div>
					<div class="promo-row-30"><div class="floatingBox"><input type="text" name="promoCode" id="promoCode" placeholder="" value="<?php echo fnCheckEmpty($ritems['promoCode']);?>" /><label class="floating-lab">Code</label></div></div>
				</div>
				<div class="promo-row"><div class="promo-row-100"><div class="floatingBox"><input type="text" name="promoDescription" id="promoDescription" placeholder="" value="<?php echo fnCheckEmpty($ritems['promoDescription']);?>" /><label class="floating-lab">Description</label></div></div></div>
				<div class="promo-row">
					<div class="promo-row-10">
						<div class="floatingBox" style="width:75px;"><input type="text" placeholder="" name="promoQty" id="promoQty" value="<?php echo fnCheckEmpty($ritems['promoQty']);?>" /><label class="floating-lab">QTY</label></div>
						<?php if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { ?>
							<label><input type="checkbox" id="promoIsApproRequired" value="<?php echo fnCheckEmpty($ritems['promoIsApproRequired']);?>" <?php if(fnCheckEmpty($ritems['promoIsApproRequired'] == 1)){ echo "checked";}?>> Approval Required</label>
						<?php } else {?>
							<label><input type="checkbox" id="promoIsApproRequired" value="0" <?php if(fnCheckEmpty($ritems['promoIsApproRequired'] == 1)){ echo "checked";}?>> Approval Required</label>
						<?php } ?>
						
						<?php if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { ?>
							<input type="hidden" name="promoIsApproRequired" id="promoIsApproReqHidden" value="<?php if(fnCheckEmpty($ritems['promoIsApproRequired'] == 1)){ echo "1";} else {echo "0"; }?>"/>
						<?php } else {?>
							<input type="hidden" name="promoIsApproRequired" id="promoIsApproReqHidden" value="0"/>
						<?php }?>
					</div>
				</div>
			</div>

			<div class="promo-rule-section">
				<h2 class="section-heading">Rules</h2>
				<div class="promo-row">
					<div class="promo-row-select-label">
						<label>Apply To
						<?php if(!isset($_REQUEST["id"])) { $ritems['promoRApplyTo'] = 1;} ?>
						<select name="promoRApplyTo" id="promoRApplyTo">
							<option value="">-Select Option-</option>
							<?php echo fnDropDownList("bb_promoapplyto","id","applyto",@fnCheckEmpty($ritems['promoRApplyTo']),"");?>
						</select>
						</label>
					</div>
				</div>
				<div class="promo-row">
					<div class="promo-row-select-label">
						<label>Discount
						<select name="promoRDiscountType" id="promoRDiscountType">
							<option value="">-Select Option-</option>
							<?php echo fnDropDownList("bb_promoamounttype","id","amounttype",@fnCheckEmpty($ritems['promoRDiscountType']),"");?>
						</select>
						</label> 	
					</div>
					<div class="promo-row-select">
						<?php if(!isset($_REQUEST["id"])) { $ritems['promoRTaxType'] = 1;} ?>
						<select name="promoRTaxType" id="promoRTaxType">
							<option value="">-Select Option-</option>
							<?php echo fnDropDownList("bb_promotaxtype","id","taxtype",@fnCheckEmpty($ritems['promoRTaxType']),"");?>
						</select>
					</div>
					<div class="promo-row-select">
						<div class="floatingBox"><input type="text" name="promoRDiscountVal" id="promoRDiscountVal" placeholder="" value="<?php echo fnCheckEmpty($ritems['promoRDiscountVal']);?>"/><label class="floating-lab">Amount</label></div>
					</div>
				</div>
			</div>

			<div class="promo-schedule-section">
				<h2 class="section-heading">Schedule</h2>
				<div class="promo-row" style="margin-top:8px;">
					<div class="promo-row-select">
						<div class="floatingBox"><input type="text" name="schStDate" value="<?php if($ritems['schStDate'] != "" ) { echo date("m/d/Y",strtotime($ritems['schStDate'])); } else { echo "";} ?>" id="schStDate" placeholder="" autocomplete="off" /><label class="floating-lab">Start Date(MM/DD/YYYY)</label></div>
					</div>
					<div class="promo-row-select">
						<div class="floatingBox"><input type="text" name="schStTime" value="<?php echo fnCheckEmpty($ritems['schStTime']);?>" id="schStTime" class="timepicker remspace" placeholder="" autocomplete="off"/><label class="floating-lab">Start Time</label></div>
					</div>
					<div class="promo-row-select">
						<div class="floatingBox"><input type="text" name="schEndDate" value="<?php if($ritems['schEndDate'] != "" ) { echo date("m/d/Y",strtotime($ritems['schEndDate'])); } else { echo "";} ?>" id="schEndDate" placeholder="" autocomplete="off" /><label class="floating-lab">End Date(MM/DD/YYYY)</label></div>
					</div>
					<div class="promo-row-select">
						<div class="floatingBox"><input type="text" name="schEndTime" value="<?php echo fnCheckEmpty($ritems['schEndTime']);?>" id="schEndTime" class="timepicker remspace" placeholder="" autocomplete="off"/><label class="floating-lab">End Time</label></div>
					</div>
				</div>
			</div>
			
			<?php //Edit Case
			$eventAssociatedVal = fnCheckEmpty($ritems['eventAssociated']);
			//echo "<prE>eventAssociatedVal==";print_r($eventAssociatedVal);
			if( (isset($_REQUEST["id"]) && $_REQUEST["id"] != "") && $eventAssociatedVal != "" ) {  
			?>
			<div class="promo-schedule-section">
				<h2 class="section-heading">Availability</h2>
				<div class="promo-row">
					<ul class="event-dynamic-content">
					<?php
					if(!empty($eventAssociatedVal)){
						if( strpos($eventAssociatedVal, ',') !== false ) { 
							$eventAssociatedNewArr = explode(",",$eventAssociatedVal);
							//echo "<prE>eventAssociatedNewArr==";print_r($eventAssociatedNewArr);
							$eventAssociatedArr = array();
							$params = array('');
							foreach($eventAssociatedNewArr as $newKey=>$newVal){
								//Get associated event wrt studioid for corp admin
								if(!isset($_SESSION['curRole']) ) {  //studio user
									if(getAssociatedEventListWRTUser($newVal,$_SESSION['stId'])){
										$eventAssociatedArr[] = getAssociatedEventListWRTUser($newVal,$_SESSION['stId']);
									}
								} 
								else { //corp admin or corp user
									if(getAssociatedEventListWRTUser($newVal,$_SESSION['stId'])){
										$eventAssociatedArr[] = getAssociatedEventListWRTUser($newVal,$_SESSION['stId']);
									}
								}
							}
							//echo "<prE>eventAssociatedArr==";print_r($eventAssociatedArr);
							$diffArr= array_diff($eventAssociatedNewArr,$eventAssociatedArr);
							$diffArrStr = implode(",",$diffArr); 
							?>
							<input type="hidden" name="diffArrStr" value="<?php echo $diffArrStr;?>"/>
							<?php
						} 
						else {  
							//if user is corp admin
							if(!isset($_SESSION['curRole']) ) {  //studio user
								$eventAssociatedArr[0] = $eventAssociatedVal;
							} 
							else { //corp admin or corp user
								//Get associated event wrt studioid for corp admin
								if(getAssociatedEventListWRTUser($eventAssociatedVal,$_SESSION['stId'])){
									$eventAssociatedArr[0] = getAssociatedEventListWRTUser($eventAssociatedVal,$_SESSION['stId']);
								} else { 
									$eventAssociatedArr = array();
								}
							}
						}
						
						//echo "<pre>eventAssociatedArr=";print_r($eventAssociatedArr);//die;
						if(!empty($eventAssociatedArr)){ 
							$eventAssociatedArr1 = getSelEventListAtPromoEditPage($eventAssociatedArr,$_SESSION['stId']);
						    //echo "<pre>eventAssociatedArr1=";print_r($eventAssociatedArr1);
							foreach($eventAssociatedArr1 as $key=>$val) {?>
								<li id="li_<?php echo $key;?>">
									<div class="ticket-close-dropdown availevent30" >
										<a href="javascript:void(0);"><i class="fa fa-times-circle remove" style="font-size:19px;color:red"></i></a>
										<select class="template_pe_dropdown unique" name="eventAssociated[]" id="eventAssociated_<?php echo $key;?>" >
											<option value="">-Event Name-</option>
											<?php
											if(!isset($_SESSION['curRole']) ) {  //studio user
												echo fnDropDownListForEventList("bb_event","id","evTitle","evDate","evStTime",$_SESSION['stId'],$val,"",$_SESSION['urole']);
											} else { //corp admin or corp user
												echo fnDropDownListForEventList("bb_event","id","evTitle","evDate","evStTime",$_SESSION['stId'],$val,"",$_SESSION['curRole']);
											}?>
										</select>
									</div>
								</li>
							<?php
							}					
						}
					}  ?>
					</ul>
				</div>
				<div class="input-row"><div class="insert-row"><a href="javascript:void(0);"><i id="eventAssociatedAddMore" class="fa fa-plus-circle"></i></a></div></div>
			</div>
			<?php
			} 
			//Add Case
			if(!isset($_REQUEST["id"]) || ( (isset($_REQUEST["id"]) && $_REQUEST["id"] != "") && $eventAssociatedVal == "" ) ) {?>
				<div class="promo-schedule-section">
					<h2 class="section-heading">Availability</h2>
					<div class="promo-row">
						<ul class="event-dynamic-content">
						</ul>
					</div>
					<div class="input-row"><div class="insert-row"><a href="javascript:void(0);"><i id="eventAssociatedAddMore" class="fa fa-plus-circle"></i></a></div></div>
				</div>
			<?php 
			} ?>

			
		</form>
		<style type="text/css">
		.error ~ .select2-container{border: 1px solid red;}
		.valid ~ .select2-container{border: 1px solid green;}
		#ui-datepicker-div, #ui-timepicker-div{z-index:1080!important;}
		.disabled-select{ background-color:#d5d5d5;opacity:0.5;border-radius:3px;cursor:not-allowed;position:absolute;top:0;bottom:0;right:0;left:0;}
		.availevent30 {width : 80% !important}
		.event-dynamic-content li .ticket-close-dropdown a {padding:12px 0px;}
		</style>
	</div>
</div>
<!-- left content area end-->
<script>
$(document).on("click","#btnCancel",function() { 
	<?php if($_REQUEST["id"] != ''){ //edit  case ?>
		var status = '<?php echo $ritems['status'];?>';
		if(status == '3'){ //archive
			window.location.href="<?php echo base_url_site ?>promocodelist?type=archive";
		} 
		else if(status == '0' || status == '1'){ //active or inactive
			window.location.href="<?php echo base_url_site ?>promocodelist";
		}
	<?php } else { //add  case?>
		window.location.href="<?php echo base_url_site ?>promocodelist";
	<?php }?>
});
/*Promo is approval checkbox onchange event*/
$('#promoIsApproRequired').change(function() {
	if(this.checked) { 
		$('#promoIsApproReqHidden').val(1); //set value in hidden
	} else { 
		$('#promoIsApproReqHidden').val(0); //set value in hidden field
	}
});

function select2OnLoad(){ 
	$(".template_pe_dropdown").select2({ width: '600px' }); //dropdownAutoWidth : true, width: 'auto' ,allowClear: false 
}

function select3AtAddMore(){
	$('.event-dynamic-content li:last-child').find(".template_pe_dropdown").select2({ width: '600px' });
}

function readonly_select(objs, action) { 
	if (action===true)
        objs.prepend('<div class="disabled-select"></div>');
    else
        $(".disabled-select", objs.find('.select2')).remove();
}

$(document).on('change','.template_pe_dropdown',function(){
	var varRepeat = 0;
	$.validator.addMethod("unique", function(value, element) { 
        var parentForm = $(element).closest('form'); 
        var timeRepeated = 0;
			if (value != '') {
				$(parentForm.find('.template_pe_dropdown')).each(function () {
					if ($(this).val() === value) {
						timeRepeated++;
						varRepeat++;
					}
				});
			}
			
		return timeRepeated === 1 || timeRepeated === 0;
    }, "Duplicate option selected");
	$(this).valid();
	//$(".select2-selection span").attr('title', '');
	if ($(this).val() != ''){
		//alert('varRepeat='+varRepeat);
		if (varRepeat === 1 || varRepeat === 0){ 
			$(this).parents('li').find('.select2').prepend('<div class="disabled-select"></div>');
		}else {
			$(this).parents('li').find('.disabled-select').remove();
		}
	}
});

/*Event Asscociation Clone*/

<?php 
//Edit Case
if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "") { 
	$eventAssociatedVal = $ritems['eventAssociated'];
	if(!empty($eventAssociatedVal)){
		if( strpos($eventAssociatedVal, ',') !== false ) { 
			$eventAssociatedArr = explode(",",$eventAssociatedVal);
			$eventAssociatedArrCnt = count($eventAssociatedArr);
		} else {
			$eventAssociatedArr[0] = $eventAssociatedVal;
			$eventAssociatedArrCnt = 1;
		}
	}  else {
		$eventAssociatedArrCnt = 0;
	}?>
	var evtId = '<?php echo $eventAssociatedArrCnt;?>';
	//alert('evtId='+evtId);
<?php 
} else { // Add Case ?>
	var evtId = 0;
<?php }?>
<?php
//echo "<pre>";print_R($_SESSION);
//studio user 
//if($_SESSION['urole'] == 3){ 
if(!isset($_SESSION['curRole']) ) { ?>
	var studioIdAsPerUser =  '<?php echo $_SESSION['stId'];?>';
	var userRole = '<?php echo $_SESSION['urole'];?>';
<?php } else { ?>
	var studioIdAsPerUser = '<?php echo $_SESSION['stId'];?>';
	var userRole = '<?php echo $_SESSION['curRole'];?>';
<?php }?>

function promoDropDownListForEvent(){ 
	$.ajax({
        url: "<?php echo base_url_site;?>promoDropDownListForEvent",
        type: "post",
        data : {tblName:'bb_event',fldName1:'id',fldName2:'evTitle',fldName3:'evDate',fldName4:'evStTime',fldName5:studioIdAsPerUser, fldName6:"",fldName7:"",type:userRole},
        success: function (response) {
            var jsonData = $.parseJSON(response); 
			var optionListLength  = jsonData.optionList.length; 
			if(optionListLength > 0) {
			    var optionListHtml = "<option value=''>-Event Name-</option>";
				for(var k=0;k<optionListLength; k++){
					optionListHtml += "<option value="+jsonData.optionList[k].id+">"+jsonData.optionList[k].evDateTimeFormated+"</option>";
				}
				$('.event-dynamic-content li:last-child').find('.template_pe_dropdown').html(optionListHtml);
			}
		}
    });
}

$('#eventAssociatedAddMore').on('click', function() { 
    promoDropDownListForEvent();
	$('.event-dynamic-content').append('<li id="li_'+evtId+'"><div class="ticket-close-dropdown availevent30" ><a href="javascript:void(0);"><i class="fa fa-times-circle remove" style="font-size:19px;color:red"></i></a><select class="template_pe_dropdown unique" name="eventAssociated[]" id="eventAssociated_'+evtId+'" ><option value="">-Event Name-</option></select></div></li>');
	$('.event-dynamic-content li:last-child').find('.template_pe_dropdown').val('');
	$('.event-dynamic-content li:last-child').find('.template_pe_dropdown').attr('id', "eventAssociated_"+evtId);
	
	if($('.event-dynamic-content li:last-child').find('.template_pe_dropdown').hasClass('valid')){
		$('.event-dynamic-content li:last-child').find('.template_pe_dropdown').removeClass('valid');
	}
	if($('.event-dynamic-content li:last-child').find('.template_pe_dropdown').hasClass('error')){
		$('.event-dynamic-content li:last-child').find('.template_pe_dropdown').removeClass('error');
	}
	select3AtAddMore();  //reinitialize select2 on all .combobox
	evtId++;
});

select2OnLoad(); //selectize all .combobox

<?php 
//Edit Case select2 disabled
if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "") { 
	$eventAssociatedVal =  $ritems['eventAssociated'];
	if(!empty($eventAssociatedVal)){
		if( strpos($eventAssociatedVal, ',') !== false ) { 
			$eventAssociatedArr = explode(",",$eventAssociatedVal);
			$eventAssociatedArrCnt = count($eventAssociatedArr);
		} else {
			$eventAssociatedArr[0] = $eventAssociatedVal;
			$eventAssociatedArrCnt = 1;
		}
	} 
	if($eventAssociatedVal == ''){ ?>
		readonly_select($(".select2"), false);
	<?php 
	} else { ?>
		readonly_select($(".select2"), true);
	<?php
	}
} ?>



/*Event associated Cloning Remove*/
$('.event-dynamic-content').on('click', '.remove', function() { 
	$(this).parent().parent().parent().remove();
	return false; 
});

$(document).on("click","#schStDate",function(){
	$(this).datepicker({showOn:"focus",autoclose:true,changeMonth: true,changeYear: true, yearRange: '1950:2050' }).focus();
	$(this).attr("autocomplete", "off"); 
	//$(this).attr("value", " "); 
	$(this).datepicker({minDate: "0",showOn:"focus",changeMonth: true,changeYear: true, yearRange: '1950:2050'}).on("change", function() {
		$(this).valid(); 
    });
});

$(document).on("click","#schEndDate",function(){
	$(this).datepicker({showOn:"focus",autoclose:true,changeMonth: true,changeYear: true, yearRange: '1950:2050'}).focus();
	$(this).attr("autocomplete", "off");
	//$(this).attr("value", " ");
	$(this).datepicker({minDate: "0",showOn:"focus",changeMonth: true,changeYear: true, yearRange: '1950:2050'}).on("change", function() {
        $(this).valid(); 
    });
});

$(document).on("change","#schStDate",function(){
	if($(this).val() != " "){
		$(this).siblings("label").addClass("float");
	}
});

$(document).on("change","#schEndDate",function(){
	if($(this).val() != " "){
		$(this).siblings("label").addClass("float");
	}
});

$(document).on("click",".timepicker",function(){
	$(this).attr("value", " ");
});


// floating label for time picker
$(document).on("click","#schStTime",function(){
	$(".ui-timepicker-container").addClass("schs");
});


$(document).on("click",".schs .ui-menu-item",function(){
	$("#schStTime").siblings("label").addClass("float");
});	


$(document).on("click","#schEndTime",function(){
	$(".ui-timepicker-container").addClass("sche");
});


$(document).on("click",".sche .ui-menu-item",function(){
	$("#schEndTime").siblings("label").addClass("float");
});

$('.timepicker').timepicker({
    timeFormat: 'h:mm p',
    interval: 60,
    minTime: '12am',
    maxTime: '11:00pm',
    defaultTime: 'minTime',
    startTime: '12:00am',
    dynamic: false,
    dropdown: true,
    scrollbar: true
});

$.validator.addMethod("greaterThan",function(value, element, param) {
	var valUsed = $.trim(value);
	var paramVal = $.trim($(param).val());
	if(paramVal != ''){
		var dateParseVal = Date.parse(valUsed);
		var paramParseVal = Date.parse(paramVal);
        if(dateParseVal >= paramParseVal ){
            if( $('#schStDate').hasClass('error')) {
                $('#schStDate').removeClass('error')
                $('#schStDate').addClass('valid')
                $('#schStDate').next('.tooltip').remove();
            }
            return this.optional(element) || dateParseVal >= paramParseVal;
        } else {
            return false;
        }
    } else {
        return true;
    }
},'Must be greater than or equal to start date.');

$.validator.addMethod('lessThan', function(value, element, param) {
	var valUsed = $.trim(value);
	var paramVal = $.trim($(param).val());
	if(paramVal != ''){
		var dateParseVal = Date.parse(valUsed);
		var paramParseVal = Date.parse(paramVal);
		if(dateParseVal <= paramParseVal){
            if( $('#schEndDate').hasClass('error')) {
                $('#schEndDate').removeClass('error')
                $('#schEndDate').addClass('valid')
                $('#schEndDate').next('.tooltip').remove();
            }
            return this.optional(element) || dateParseVal <= paramParseVal;
        } else {
            return false;
        }
    } else {
        return true;
    }
}, 'Must be less than or equal to end date.');

function ConvertTimeformat(format, strUsed) {
	var str = $.trim(strUsed);
	if(str != ''){
		var hours = Number(str.match(/^(\d+)/)[1]);
		var minutes = Number(str.match(/:(\d+)/)[1]);
		var AMPM = str.match(/\s?([AaPp][Mm]?)$/)[1];
		var pm = ['P', 'p', 'PM', 'pM', 'pm', 'Pm'];
		var am = ['A', 'a', 'AM', 'aM', 'am', 'Am'];
		if (pm.indexOf(AMPM) >= 0 && hours < 12) hours = hours + 12;
		if (am.indexOf(AMPM) >= 0 && hours == 12) hours = hours - 12;
		var sHours = hours.toString();
		var sMinutes = minutes.toString();
		if (hours < 10) sHours = "0" + sHours;
		if (minutes < 10) sMinutes = "0" + sMinutes;
		if (format == '0000') {
			var txthr = (sHours + sMinutes);
		} else if (format == '00:00') {
		   var txthr = (sHours + ":" + sMinutes);
		} else {
			return false;
		}
		return txthr;
	}
}

$.validator.addMethod("greaterThanTime", function(value, element, param) { 
	var valUsed = $.trim(value);
	var paramVal = $.trim($(param).val());
	var schStDate = $.trim($('#schStDate').val());
	var schEndDate = $.trim($('#schEndDate').val());
	if( (schStDate == schEndDate) &&  schStDate != "" && schEndDate != ""){
		if(paramVal != ''){
			var schStDateArr = schStDate.split("/");
			
			var schStTimeFormat = ConvertTimeformat("00:00", valUsed);
			var schStTimeArr = schStTimeFormat.split(":");
			
			var schEndDateArr = schEndDate.split("/");
			
			var schEndTimeFormat = ConvertTimeformat("00:00", paramVal);
			var schEndTimeArr = schEndTimeFormat.split(":");
			
			//(year, month, day, hours, minutes, seconds, milliseconds)
			//subtract 1 from month because Jan is 0 and Dec is 11
			var startFullDateFormat = new Date(schStDateArr[0], (schStDateArr[1]-1), schStDateArr[2], schStTimeArr[0], schStTimeArr[1], 0, 0);
			//alert('startFullDateFormat=='+startFullDateFormat);
			var endFullDateFormat = new Date(schEndDateArr[0], (schEndDateArr[1]-1), schEndDateArr[2], schEndTimeArr[0], schEndTimeArr[1], 0, 0);
			//alert('endFullDateFormat=='+endFullDateFormat);
			
			if(startFullDateFormat > endFullDateFormat ){	
				return this.optional(element) || startFullDateFormat > endFullDateFormat;
			} else {
				return false;
			}
		}
		else {
			return true;
		}
	} 
	else {
		return true;
	}
},'Must be greater than start time.');

$.validator.addMethod('lessThanTime', function(value, element, param) {
	var valUsed = $.trim(value);
	var paramVal = $.trim($(param).val());
	var schStDate = $.trim($('#schStDate').val());
	var schEndDate = $.trim($('#schEndDate').val());
	if( (schStDate == schEndDate) &&  schStDate != "" && schEndDate != ""){
		if(paramVal != ''){
			var schStDateArr = schStDate.split("/");
			
			var schStTimeFormat = ConvertTimeformat("00:00", valUsed);
			var schStTimeArr = schStTimeFormat.split(":");
			
			var schEndDateArr = schEndDate.split("/");
			
			var schEndTimeFormat = ConvertTimeformat("00:00", paramVal);
			var schEndTimeArr = schEndTimeFormat.split(":");
			
			//(year, month, day, hours, minutes, seconds, milliseconds)
			//subtract 1 from month because Jan is 0 and Dec is 11
			var startFullDateFormat = new Date(schStDateArr[0], (schStDateArr[1]-1), schStDateArr[2], schStTimeArr[0], schStTimeArr[1], 0, 0);
			//alert('startFullDateFormat=='+startFullDateFormat);
			var endFullDateFormat = new Date(schEndDateArr[0], (schEndDateArr[1]-1), schEndDateArr[2], schEndTimeArr[0], schEndTimeArr[1], 0, 0);
			//alert('endFullDateFormat=='+endFullDateFormat);
			if(startFullDateFormat < endFullDateFormat ){	
				return this.optional(element) || startFullDateFormat < endFullDateFormat;
			} else {
				return false;
			}
		} else {
			return true;
		}
	} 
	else {
		return true;
	}
}, 'Must be less than to end time.');


$(document).on('click','.actionEvent',function(){ 
	var actionEventId = $(this).attr('id');
	$("#actionType").val(actionEventId);
});

<?php 
//Edit Case
if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "") { ?>
	var promoId = $("#promoId").val();
<?php 
} else { // Add Case ?>
	var promoId = "";
<?php }?>

$.validator.addMethod("maxLen", function (value, element, param) {
    //alert('element= ' + $(element).attr('name') + ' param= ' + param )
	if($("#promoRDiscountType").val()== "2"){ // for type = percentage
		if ($(element).val().length > param) {
			return false;
		} else {
			return true;
		}
	} else {
        return true;
    }
}, "Only 2 digit allowed when discount type = percentage.");

$.validator.addMethod("checkDateFormat",function(value, element) {
    return value.match(/^(0[1-9]|1[012])[\/|-](0[1-9]|[12][0-9]|3[01])[\/|-](19\d\d|2\d\d\d)$/);
},"Please enter a date in the format mm/dd/yyyy");

$.validator.prototype.checkForm = function() {
    //overriden in a specific page
    this.prepareForm();
    for (var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
        if (this.findByName(elements[i].name).length !== undefined && this.findByName(elements[i].name).length > 1) {
            for (var cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
                this.check(this.findByName(elements[i].name)[cnt]);
            }
        } else {
            this.check(elements[i]);
        }
    }
    return this.valid();
};

$.validator.addMethod('decimal', function(value, element) {
  return this.optional(element) || /^((\d+(\\.\d{0,2})?)|((\d*(\.\d{1,2}))))$/.test(value);
}, "Please enter a correct amount, format 0.00");

$.validator.addMethod("noSpace", function(value, element) { 
  return $.trim(value) != "";
}, "No space please and don't leave it empty");

$.validator.addMethod("qtyGreaterThanZero", function(value, element) { 
  return $.trim(value) != 0;
}, "Qty can't be zero");


/*$.validator.addMethod("existStartDateThanTime", function(value, element, param) { 
	if($(param).val()== "" &&  value == "") {
		return true;
	} else if($(param).val()!= "" &&  value != "") {
		return true;
	} else if( $(param).val()!= "" &&  value == ""){
		return false;
	} 
},'Please enter date.');

$.validator.addMethod("existEndDateThanTime", function(value, element, param) { 
	if($(param).val()== "" &&  value == "") {
		return true;
	} else if($(param).val()!= "" &&  value != "") {
		return true;
	} else if( $(param).val()!= "" &&  value == ""){
		return false;
	} 
},'Please enter date.');*/


$.validator.addMethod("checkStartTimeFormat",function(a,b){
	if(a!=""){
		return this.optional(b) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(\ ?[AP]M)$/i.test(a);
	} else {
		return true;
	}
},"Please enter valid time.");


$.validator.addMethod("checkEndTimeFormat",function(a,b){
	if(a!=""){
		return this.optional(b) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(\ ?[AP]M)$/i.test(a);
	} else {
		return true;
	}
},"Please enter valid time.");

$("form[id='addUpdPromoFrm']").validate({
	onkeyup: false,
    onblur :true,
	ignore: ".ignore, .select2-input", 
	rules: {
	    promoName: { required :true,noSpace: true},
		promoCode: { 
			required :true,
			minlength: 5,
			maxlength: 30,
			noSpace: true,
			remote: {
				url: '<?php echo base_url_site; ?>addon/ajaxValidatePromocode',
				type: "post",
				data: { promoId:promoId,studid:$("#studid1").val()}
			}
		},
		promoDescription: { required :true,noSpace: true},
		promoQty: { required :true,digits: true,qtyGreaterThanZero: true},
		promoRApplyTo: {
		    required: function(element){
				if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction"){ 
					return true;
				} else {
					return false;
				}
			},
		},
		promoRDiscountType: {
		    required: function(element){
				if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction"){ 
					return true;
				} else {
					return false;
				}
			},
		},
		promoRTaxType: {
		    required: function(element){
				if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction"){ 
					return true;
				} else {
					return false;
				}
			},
		},
		promoRDiscountVal: {
		    required: function(element){
				if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction"){ 
					return true;
				} else {
					return false;
				}
			},
			decimal: true
			//digits: true,maxLen: 2
		},
		schStDate: {
		    required: function(element){
				if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction"){ 
					return true;
				} else {
					return false;
				}
			},lessThan: "#schEndDate",checkDateFormat: true,
		},
		schStTime: {
		    required: function(element){
				if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction"){ 
					return true;
				} else {
					return false;
				}
			},
			lessThanTime: "#schEndTime",
			checkStartTimeFormat: true,
			//existStartDateThanTime:"#schStDate"
			//noSpace: true
		},
		schEndDate: {
		    required: function(element){
				if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction"){ 
					return true;
				} else {
					return false;
				}
			},greaterThan: "#schStDate",checkDateFormat: true,
		},
		schEndTime: {
		    required: function(element){ 
				if($("#actionType").val()== "addAction" || $("#actionType").val()== "updateAction"){ 
					return true;
				} else {
					return false;
				}
			},
			greaterThanTime:"#schStTime",
			checkEndTimeFormat: true,
			//existEndDateThanTime:"#schEndDate"
			//noSpace: true
		},
		"eventAssociated[]":{ required :true},
	},
	messages: {
	    promoName: {required: "Please enter promotion name"},
		promoCode: {
			required: "Please enter Coupon code",
			remote: "Coupon code is already exist",
			minlength: "Coupon code can not be less than 5",
			maxlength: "Coupon code can not be greater than 30"
		},
		promoDescription: {required:"Please enter a description"},
		promoQty: {required: "Please enter a quantity",digits: "Please enter only digits"},
		promoRApplyTo: {required: "Please select apply type"},
		promoRDiscountType: {required: "Please choose an option"},
		promoRTaxType: {required: "Please select tax type"},
		promoRDiscountVal: {required: "Please enter discount amount",digits: "Please enter only digits"},
		schStDate: {required: "Please enter schedule start date",date: "Please enter valid date"},
		schStTime: {required: "Please enter schedule start time"},
		schEndDate: {required: "Please enter schedule end date" ,date: "Please enter valid date"},
		schEndTime: {required: "Please enter schedule end time"},
		"eventAssociated[]": { required: "Select event" },
	},
	submitHandler: function(form) { 
	    $('.loading').show(); 
		$.ajax({
			url : '<?php echo base_url_site; ?>addon/ajaxSavePromocode',
			type : 'POST', 
			data :$('#addUpdPromoFrm').serialize(),
			success : function(response) {
				$('.loading').hide(); 
				var odata = $.parseJSON(response); 
				setInterval(function(){ $('.promo-msg-add').hide(); }, 5000);
				var status = odata.status;
				var studid = odata.studid;
				var userRole = odata.userRole;
				//alert('status='+status);
				//alert('studid='+studid);
				if(userRole == 3){
					if(status == '3') {
					   window.location.href = "<?php echo base_url_site;?>promocodelist?type=archive";
					} else {
						window.location.href = "<?php echo base_url_site;?>promocodelist";
					}
				} 
				else {
					if(status == '3') {
					    window.location.href = "<?php echo base_url_site;?>promocodelist?studid="+studid+"&type=archive";
					} else {
						window.location.href = "<?php echo base_url_site;?>promocodelist?studid="+studid;
					}
				}
			}
		});
	}
});

$(document).on("click",".remspace",function() {
	var str = $(this).val();
	var str = $.trim(str);
	var newStr = str.replace(/\s+/g,' ').trim();
	$(this).val(newStr);
});
</script>
<?php
include('includes/footer.php');
?>