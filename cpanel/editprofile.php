<?php include('includes/header.php');
$menuClssUsers = "downarrow";
$menuSbClssUsers = "sub";
$menuSbClssUsers2 = "active";
$menuSbClssStyleUsers = "style='display:block;'";

if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 
//if(isset($_SESSION['urole']) && $_SESSION['urole']!=1){ header("location: ".base_url_site."dashboard");    exit; } 


if(isset($_SESSION["usrid"]) and $_SESSION["usrid"]!="") {    
//$numOrders = $result['Count'];
$params = array('');
$result = $db->rawQueryOne("SELECT * FROM bb_users WHERE id='".$_SESSION["usrid"]."' ", $params);
$ritems = (array)$result;

if(!empty($ritems)) $ritems=$ritems; 
} else {
$ritems=array();
$ritems=$ritems;
}




	 ?>

<!-- Header end-->
<style>
.token-input-list-facebook{
display:none;
}
</style>

<!-- left nagivation start-->

<?php include('includes/left-sidebar.php');?>
<script type="text/javascript" src="<?php echo base_url_js?>jquery.tokeninput.js"></script>
    <link rel="stylesheet" href="<?php echo base_url_css?>token-input.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url_css?>token-input-facebook.css" type="text/css" />
<!-- left nagivation end-->
<!-- left nagivation end-->

<!-- left content area start-->
<div class="loading" style="display:none;">Loading&#8230;</div>
<div class="right-wrapper">
<h1 class="pageheading"><?php if(!isset($_SESSION["usrid"]))  echo "Add";  else  echo "Update" ?>  Profile</h1>
<div class="form-area">
 <form role="form" method="post" action="" enctype="multipart/form-data" name="frmaddupUser" id="frmaddupUser">
<input type="hidden" name="id" id="id" value="<?php echo $_SESSION["usrid"]; ?>" />
<?php

 if(isset($_SESSION['msg']) && $_SESSION['msg']!="") {?>
<script>
setInterval(function(){ $('.event-msg').hide(); }, 3000);
</script>
<div class="event-msg" style="display:block;"><?php echo $_SESSION['msg']?></div>
<?php }?>
<div class="user-info">
<div class="input-row">
<div class="floatingBox left49"><input class="" type="text" id="txtFirtName" name="txtFirtName" placeholder="" value="<?php echo @fnCheckEmpty($ritems['firstName']); ?>" /><label class="floating-lab float">First Name</label></div>
<div class="floatingBox right49"><input class="" type="text" id="txtLastName" name="txtLastName" placeholder="" value="<?php echo @fnCheckEmpty($ritems['lastName']); ?>" /><label class="floating-lab float">Last Name</label></div>
</div>

<div class="input-row">
<div class="floatingBox left49"><input class="" type="text" id="txtEmail" name="txtEmail" readonly="readonly" value="<?php echo @fnCheckEmpty($ritems['email']); ?>" <?php if(isset($_REQUEST["id"])) echo 'readonly="readonly"';?> placeholder="" /><label class="floating-lab float">Email</label></div>




<?php if(isset($_SESSION["usrid"]) && $_SESSION["usrid"]!="") { ?>

<div class="floatingBox right49"><input class="" type="password" id="txtRstPwd" name="txtRstPwd" placeholder="" value="" /><label class="floating-lab float">Reset Password</label></div>

<?php }?>

</div>



<div class="bottom-btn"><input style="padding-left:30px; padding-right:30px;" class="yellow-btn" id="btnSave" name="btnSave" type="submit" value="Save" /></div>

</form>
</div>

</div>

<!-- left content area end-->
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>bootstrap-3.3.5.min.css">
    <script type="text/javascript" src="<?php echo base_url_js?>bootstrap-3.3.5.min.js" /></script>
    <script type="text/javascript" src="<?php echo base_url_js?>jquery-validate.bootstrap-tooltip.js" /></script>
<script>
function valueChanged()
{
    if($(".uexist").is(":checked"))   
        $(".uEid").show();
    else
        $(".uEid").hide();
}
$(document).ready(function() {


$("#txtExistU").on('change',function(){
sid=$(this).val();
window.location.href="<?php echo base_url_site?>addupduser?stid=<?php echo $_REQUEST['stid'];?>&id="+sid;
 });
$("#lstRole").on('change',function(){
var sid="";
sid=$(this).val();
if(sid==3){
$(".token-input-list-facebook").show();
//$("#lstLocations").show();
} else {
$(".token-input-list-facebook").hide();
//$("#lstLocations").hide();
}

     });
<?php if((isset($_GET["stid"]) && $_GET["stid"]!="") || (isset($roleid) && $roleid!="")) {?>
$('#lstRole').change();
<?php }?>
	$.validator.addMethod("customemail", 
		function(value, element) {
			return /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
		}, 
		"Sorry, Ive enabled very strict email validation"
	);	 
$("#frmaddupUser").validate({
			ignore: [],
			debug: false,
			rules: {
			txtFirtName: "required",
			txtLastName:"required",
			txtRstPwd: {
							 minlength : 5
                        },

			},			
			messages: {
			txtFirtName : "Please enter first name",
			txtLastName : "Please enter last name",
			},
		submitHandler: function(form) {
		jQuery('.loading').show(); 
			var dataf = new FormData();
			var form_data = $('#frmaddupUser').serializeArray();
           // alert (form_data); 
			
			$.each(form_data, function() {
			dataf.append(this.name, this.value);
			});

			

			
			jQuery.ajax({
			url : '<?php echo base_url_site; ?>addon/ajaxSaveUser', 
			type : 'POST',              // Assuming creation of an entity
			mimeType: "multipart/form-data",
			contentType : false,        // To force multipart/form-data
			data : dataf,
			processData : false,
			success : function(response) {

						var data = JSON.parse(response);
						console.log(data);
						///$('.event-msg').html("");
						if(data.result==1){
						jQuery('.loading').hide(); 
						
						window.location.href="<?php echo base_url_site?>editprofile";
						
						
						}
			
			
			}
    		});

			
		
			
				
		}
		});	 
	 
 }); 
</script>

<?php
include('includes/footer.php');
?>