<?php 
include('includes/header.php');
$menuClssProject = "downarrow";
$menuSbClssProject = "sub";
$menuSbClssStyleProject = "style='display:block;'";
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php"); exit; } 
?>
<!-- Header end-->
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<!-- left nagivation end-->
<!-- left content area start-->
<div class="right-wrapper">

<?php 
/*echo date_default_timezone_get();

$params = array('');
$sql = "SELECT NOW(),CURDATE()";
$crnt = $db->rawQuery($sql,$params);
echo "<pre>crnt==";print_r($crnt);*/
?>
	<h1 class="pageheading">
	<?php 
	if(isset($_SESSION['urole']) &&  $_SESSION['urole']==3){
		echo fnMultiStudioTitle($_SESSION["stId"]);
	}
	//echo "<pre>"; print_r($_SESSION);
	if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="archive") { 
		$heading = "Archive Project Listing";
	} else if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="draft") { 
		$heading = "Draft Project Listing";
	} else {
		$heading = "All";
	} 
	echo $heading;
	?>
	</h1>
	<script>
	var type = '<?php echo $_REQUEST["type"];?>';
	if(type == 'draft') {
		$('.draftProject').addClass("active");
	} else if(type == 'archive') {
		$('.archiveProject').addClass("active");
	} else if(type == '') {
		$('.allProject').addClass("active");
	}
	</script>
	
	<style>
	.project-msg,.project-del-msg{color:#6fcf00; font-weight:bold; text-align:center;margin-bottom: 20px;}
	</style>
	<?php
	if(isset($_SESSION['msg']) && $_SESSION['msg']!="") {?>
		<script>
		setInterval(function(){ $('.project-msg').hide(); }, 3000);
		</script>
		<div class="project-msg" style="display:block;"><?php echo $_SESSION['msg'];?></div>
	<?php }?>
	<div class="project-del-msg" style="display:none;"></div>
	
	<div id="download_img" style="text-align: center; position: fixed; display:none; z-index: 99999; left: 0; right: 0; top: 0; bottom: 0;" >
		<img src="<?php echo base_url_site;?>images/ajax-loader.gif"  style="position: relative;  top: 50%;"/ >
	</div>
	<div class="form-area">
		<div class="event-listing-module">
			<div class="tabFilterRow"><ul></ul></div>
			<style>
			.tabFilterRow{float:left; width:100%;}
			.tabFilterRow ul{float:left; margin:0px; padding:0px; list-style:none; margin-bottom:15px;}
			.tabFilterRow ul li{display: inline-block; margin: 0 10px 10px 0px; padding: 10px 15px; color: #4a4a4a; text-decoration: none; border: 1px solid #c4c4c4; text-transform: uppercase; background-color: #fff; font-weight: 600; font-size: 13px; cursor: pointer;}
			.tabFilterRow ul li:hover { background-color: #333333; color: #fff; }
			.tabFilterRow ul li.active { background-color: #e1e1e1;}
			.tabFilterRow ul li.active:hover { background-color: #333333; color: #fff;}
			</style>
			
			<div class="elm-control">
				<div class="elm-row">
					<table class="elm-table" id="example">
						<thead>
						<tr>
							<th>Action</th>
							<th>Status</th>
							<th>Featured Image</th>
							<th>Project Name</th>
							<th>Project Class</th>
							<th>Project Type</th>
							<th>Categories</th>
							<!--<th>Sku</th>-->
							<th>Created Date</th>
							<th>Changed Date</th>
							<th>User (last activity)</th>
						</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<link href="<?php echo base_url_css?>dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<script src="<?php echo base_url_css?>dataTables/jquery.dataTables_project.js"></script>
<script src="<?php echo base_url_css?>dataTables/dataTables.bootstrap.js"></script>
<script>
$(document).ready(function(){ 
	var vtable = $('#example').DataTable( {
		initComplete: function () {
            this.api().columns( [6] ).every( function () {
                var column = this;
				
                var select = $('<ul><li class="active" customCls="">All</li></ul>').appendTo( $('.tabFilterRow').empty() );
				
				$(document).on('click', 'li', function(){
					var val = $.fn.dataTable.util.escapeRegex(
						$(this).attr('customCls')
					);
					var customClsVal = $.fn.dataTable.util.escapeRegex($(this).attr('customCls'));
					if(customClsVal == val){
						$('li').removeClass('active');
						$(this).addClass('active');
					} 
					//alert('val='+val);
					//column.search( val ? '^'+val+'$' : '', true, false ).draw();
					column.search( val ? val : '', true, false ).draw();
				} );
				
				$.ajax({
					url : '<?php echo base_url_site; ?>addon/ajaxShowAllCateg',
					type: 'post',
					data : {projType: "<?php echo $_REQUEST["type"];?>"},
					success: function(data) {
						var jsonData = $.parseJSON(data); 
						var catLength  = jsonData.catList.length; 
						if(catLength > 0) {
							for(var k=0;k<catLength; k++){
								select.append( '<li customCls="'+jsonData.catList[k].id+'">'+jsonData.catList[k].proCatName+'</li>' )
							}
						}
					}
				});
 
                /*column.data().unique().sort().each( function ( d, j ) {
					select.append( '<li customCls="'+d+'">'+d+'</li>' )
				} );*/
            } );
        },
		
		//dom: '<"elm-row"<"bubbleInfo"<"fl spage"<"paging"pl><"clear">><"fr page-text"i>>>',
		
		"dom": '<"elm-row"<"bubbleInfo"<"fl spage"<"paging"pl><"clear">><"fr page-text"i>>>rt',
		"processing": true,
		"language": {
			processing: "<div style='text-align:center'><img src='<?php echo base_url_site;?>images/ajax-loader.gif'></div>"
		},
		"serverSide": true,
		"ajax":{
			url :"<?php echo base_url_site; ?>addon/project-grid-data", // json datasource
			data: function ( d ) {
                d.type = "<?php echo $_REQUEST["type"];?>";
            },
			type: "post",  // method  , by default get
			error: function(){  // error handling
				$(".example-error").html("");
				$("#example").append('<tbody class="example-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
				$("#example_processing").css("display","none");
			}
		},
		
		pagingType: "full_numbers",
		sortable: false,
		paginate: true,
		pageLength: 50,
		info: true,
		bSort: true,
		
		<?php if($_REQUEST['type'] == ''){ ?>
			order: [[ 0, "desc" ]],
		<?php } if($_REQUEST['type'] == 'draft'){ ?>
			order: [[ 0, "desc" ]],
		<?php } if($_REQUEST['type'] == 'archive'){ ?>
			order: [[ 0, "desc" ]],
		<?php } ?>
		bFilter: true,
		"aaSorting": [],
		"aoColumnDefs": [ 
			{ "bSortable": false, "aTargets": [ 0 ] } ,
			{ "bSortable": false, "aTargets": [ 2 ] }
		]
	} );
	
	$(document).on('click','.delProject',function(){  
		var projectId = $(this).attr('customAtr');
		var elm = $(this);
		var dlg ='<p>Would you like to delete this Project</p>';
		doConfirm(dlg, function yes() {
			$.ajax({
				url : '<?php echo base_url_site; ?>addon/ajaxDeleteProject',
				type: 'post',
				data: {projectId:projectId},
				success: function(data) {
				    var odata = $.parseJSON(data);
					vtable.row( elm.parents('tr') ).remove().draw(false);
					$('html, body').animate({scrollTop: '0px'}, 300);//Scroll top
					$('.project-del-msg').css("display","block");
					$('.project-del-msg').html(odata.msg);
					setTimeout(function(){ 
						$('.project-del-msg').css("display","none");
						$('.project-del-msg').html(""); 
					}, 2000);
				},
				error: function() {
					alert('There has been an error.');
				}
			});
			
			//If category count=1 then remove the same category
			if ($("#example tbody tr").length == 1){
				$(".tabFilterRow ul li.active").remove();
			}
			
		}, function no() {
			return false;
		});
    });

});
</script>
<!-- left content area end-->
<?php
include('includes/footer.php');
?>