<?php 
include('includes/header.php');
$menuClssBookingTemp = "downarrow";
$menuSbClssBookingTemp = "sub";
$menuSbClssStyleBookingTemp = "style='display:block;'";
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php"); exit; } 
?>
<!-- Header end-->
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<link href="<?php echo base_url_site?>css/datepicker/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url_site?>js/datepicker/jquery-ui.js"></script>
<!-- ToolTip Type Validation-->
<!--<link type="text/css" rel="stylesheet" href="<?php //echo base_url_site?>css/bootstrap-3.3.5.min.css">-->
<script type="text/javascript" src="<?php echo base_url_site?>js/bootstrap-3.3.5.min.js" /></script>
<script type="text/javascript" src="<?php echo base_url_site?>js/jquery-validate.bootstrap-tooltip.min.js" /></script>

<link href="<?php echo base_url_css?>dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<script src="<?php echo base_url_css?>dataTables/buttons/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url_css?>dataTables/buttons/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url_css?>dataTables/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url_css?>dataTables/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script>
$(document).on("change","#schStDate",function(){
	if($(this).val() != " "){
		$(this).siblings("label").addClass("float");
	}
});

$(document).on("change","#schEndDate",function(){
	if($(this).val() != " "){
		$(this).siblings("label").addClass("float");
	}
});

$(document).on('click','.bookingApprovalEmail',function(){
	var customerEmail = $(this).attr("data-cus-email"); 
	var bookingId = $(this).attr("data-bookingId"); 
	var studioId = $(this).attr("data-studioId"); 
	var actionType = $(this).attr("data-actType"); 
	//alert(customerEmail+","+bookingId+","+studioId+","+actionType);
	$.ajax({
		url : '<?php echo base_url_site; ?>addon/ajaxBookingReConfirmationEmail',
		type : 'POST', 
		data :{customerEmail: customerEmail, bookingId: bookingId, studioId: studioId,actionType:actionType,pageTypeAct:'approveAttention'},
		success : function(response) {
			var odata = $.parseJSON(response); 
			var mailstatus = odata.mailstatus; 
			window.location.href = "<?php echo base_url_site;?>bookinglist?type=needatten";
			if(mailstatus == '1'){
				console.log('mail is successfully sent');
			} else {
				console.log('mail is not sent');
			}
		}
	});
});

$(document).on('click','.bookingRejectionEmail',function(){
	var customerEmail = $(this).attr("data-cus-email"); 
	var bookingId = $(this).attr("data-bookingId"); 
	var studioId = $(this).attr("data-studioId"); 
	var actionType = $(this).attr("data-actType"); 
	//alert(customerEmail+","+bookingId+","+studioId+","+actionType);
	$.ajax({
		url : '<?php echo base_url_site; ?>addon/ajaxBookingRejectionEmail',
		type : 'POST', 
		data :{customerEmail: customerEmail, bookingId: bookingId, studioId: studioId,actionType: actionType,pageTypeAct:'rejectAttention'},
		success : function(response) {
			var odata = $.parseJSON(response); 
			var mailstatus = odata.mailstatus;
			window.location.href = "<?php echo base_url_site;?>bookinglist?type=needatten";
			if(mailstatus == '1'){
				console.log('mail is successfully sent');
			} else {
				console.log('mail is not sent');
			}
		}
	});
});

$(document).ready(function(){ 
	var vtable = $('#booking-grid').DataTable( {
		"dom": '<"elm-row"<"bubbleInfo"<"fl spage"<"paging"pl><"clear">><"fr page-text"i>>>rt',
		"processing": true,
		"language": {
			processing: "<div style='text-align:center'><img src='<?php echo base_url_site;?>images/ajax-loader.gif'></div>"
		},
		"serverSide": true,
		"ajax":{
			url :"<?php echo base_url_site; ?>addon/booking-grid-data", // json datasource
			data: function ( d ) {
                d.type = "<?php echo $_REQUEST["type"];?>";
            },
			type: "post",  // method  , by default get
			error: function(){  // error handling
				$(".booking-grid-error").html("");
				$("#booking-grid").append('<tbody class="booking-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
				$("#booking-grid_processing").css("display","none");
			}
		},
		searching: false,
		pagingType: "simple_numbers",
		sortable: false,
		paginate: true,
		pageLength: 50,
		info: true,
		bSort: true,
		
		<?php if($_REQUEST['type'] == ''){ ?>
			order: [[ 0, "desc" ]],
		<?php } if($_REQUEST['type'] == 'needatten'){ ?>
			order: [[ 0, "desc" ]],
		<?php } if($_REQUEST['type'] == 'future'){ ?>
			order: [[ 3, "desc" ]],
		<?php } if($_REQUEST['type'] == 'today'){ ?>
			order: [[ 3, "desc" ]],
		<?php } if($_REQUEST['type'] == 'tomorrow'){ ?>
			order: [[ 3, "desc" ]],
		<?php } if($_REQUEST['type'] == 'past'){ ?>
			order: [[ 3, "desc" ]],
		<?php } if($_REQUEST['type'] == 'trash'){ ?>
			order: [[ 0, "desc" ]],
		<?php } ?>
		bFilter: true,
		"aoColumnDefs": [ 
			{ "bSortable": false, "aTargets": [ 0 ] },
			{ "bSortable": false, "aTargets": [ 7 ] },
			{ "bSortable": false, "aTargets": [ 8 ] },
			{ "bSortable": false, "aTargets": [ 9 ] },
			{ "bSortable": false, "aTargets": [ 12 ] },
			/*{ "bSortable": false, className: "fldCls", "targets": [ 13 ] },
			{ "bSortable": false, className: "fldCls", "targets": [ 14 ] },
			{ "bSortable": false, className: "fldCls", "targets": [ 15 ] },
			{ "bSortable": false, className: "fldCls", "targets": [ 16 ] },
			{ "bSortable": false, className: "fldCls", "targets": [ 17 ] },*/
		]
	} );
	vtable.buttons().container().appendTo( '#booking-grid_wrapper .elm-row' );
	$('.dt-buttons').css('display','none') ;
	
	/*$('.exportBtn').on('click',function(event){ 
		var SITEURL1 = "<?php echo base_url_site;?>";
		event.preventDefault();
		$('#download_img').show(); 
		var stdLocId = $(this).attr('customattr'); 
		var customBookType = $(this).attr('customBookType');
		$.ajax({
			type: "POST",
			url: "<?php echo base_url_site;?>bookinglistexport?stdLocId="+stdLocId+"&bookingType="+customBookType,
			success: function(msg){
				$('#download_img').hide(); 
				var data = JSON.parse(msg);
				console.log(data.filename);
				//window.open(SITEURL1+"reports/"+data.filename, '_blank','width='+windowWidth+', height='+windowHeigth+'');
				window.open(SITEURL1+"reports/"+data.filename, '_self');
			}
		});
	});	*/
	
	/*$('.exportBtn').on('click',function(event){ 
		var SITEURL1 = "<?php echo base_url_site;?>";
		event.preventDefault();
		$('#download_img').show(); 
		var stdLocId = $(this).attr('customattr'); 
		var customBookType = $(this).attr('customBookType');
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url_site;?>ajax?stdLocId="+stdLocId+"&bookingType="+customBookType,
			success: function(msg){
				$('#download_img').hide(); 
				var data = JSON.parse(msg);
				window.open(SITEURL1+"reports/"+data.filename, '_self');
			}
		});
	});	*/
	
	
	$(document).on('click','.delBooking',function(){  
		var bookingId = $(this).attr('customAtr');
		var bookingStatus = $(this).attr('customStatusAtr');
		var elm = $(this);
		var dlg ='<p>Would you like to delete this Booking</p>';
		doConfirm(dlg, function yes() {
			$.ajax({
				url : '<?php echo base_url_site; ?>addon/ajaxDeleteBooking',
				type: 'post',
				data: {bookingId:bookingId,bookingStatus:bookingStatus},
				success: function(data) {
				    var odata = $.parseJSON(data);
					vtable.row( elm.parents('tr') ).remove().draw(false);
					$('html, body').animate({scrollTop: '0px'}, 300);//Scroll top
					$('.booking-del-msg').css("display","block");
					$('.booking-del-msg').html(odata.msg);
					setTimeout(function(){ 
						$('.booking-del-msg').css("display","none");
						$('.booking-del-msg').html(""); 
					}, 2000);
				},
				error: function() {
					alert('There has been an error.');
				}
			});
		}, function no() {
			return false;
		});
    });
	
	$(document).on('click','.pdelBooking',function(){  
		var bookingId = $(this).attr('customAtr');
		var bookingStatus = $(this).attr('customStatusAtr');
		var elm = $(this);
		var dlg ='<p>Would you like to permanently delete this Booking</p>';
		doConfirm(dlg, function yes() {
			$.ajax({
				url : '<?php echo base_url_site; ?>addon/ajaxPermanentDeleteBooking',
				type: 'post',
				data: {bookingId:bookingId,bookingStatus:bookingStatus},
				success: function(data) {
				    var odata = $.parseJSON(data);
					vtable.row( elm.parents('tr') ).remove().draw(false);
					$('html, body').animate({scrollTop: '0px'}, 300);//Scroll top
					$('.booking-del-msg').css("display","block");
					$('.booking-del-msg').html(odata.msg);
					setTimeout(function(){ 
						$('.booking-del-msg').css("display","none");
						$('.booking-del-msg').html(""); 
					}, 2000);
				},
				error: function() {
					alert('There has been an error.');
				}
			});
		}, function no() {
			return false;
		});
    });
	
	$(document).on('click','.restoreBooking',function(){  
		var bookingId = $(this).attr('customAtr');
		var bookingStatus = $(this).attr('customStatusAtr');
		var elm = $(this);
		var dlg ='<p>Would you like to restore this Booking</p>';
		doConfirm(dlg, function yes() {
			$.ajax({
				url : '<?php echo base_url_site; ?>addon/ajaxRestoreBooking',
				type: 'post',
				data: {bookingId:bookingId,bookingStatus:bookingStatus},
				success: function(data) {
				    var odata = $.parseJSON(data);
					vtable.row( elm.parents('tr') ).remove().draw(false);
					$('html, body').animate({scrollTop: '0px'}, 300);//Scroll top
					$('.booking-del-msg').css("display","block");
					$('.booking-del-msg').html(odata.msg);
					setTimeout(function(){ 
						$('.booking-del-msg').css("display","none");
						$('.booking-del-msg').html(""); 
					}, 2000);
				},
				error: function() {
					alert('There has been an error.');
				}
			});
		}, function no() {
			return false;
		});
    });
});
</script>
<!-- left nagivation end-->
<!-- left content area start-->
<div class="right-wrapper">
	<script>
	var type = '<?php echo $_REQUEST["type"];?>';
	if(type == 'needatten') {
		$('.needatten').addClass("active");
	} else if(type == 'future') {
		$('.current').addClass("active");
	} else if(type == 'today') {
		$('.today').addClass("active");
	} else if(type == 'tomorrow') {
		$('.tomorrow').addClass("active");
	} else if(type == 'past') {
		$('.past').addClass("active");
	} else if(type == 'trash') {
		$('.trash').addClass("active");
	} else if(type == '') { //all boking list
		$('.allbooking').addClass("active");
	}
	
	$(document).on("click","#schStDate",function(){
		$(this).datepicker({showOn:"focus",autoclose:true,changeMonth: true,changeYear: true, yearRange: '1950:2050' }).focus();
		$(this).attr("autocomplete", "off"); 
		$(this).datepicker({minDate: "0",showOn:"focus",changeMonth: true,changeYear: true, yearRange: '1950:2050'}).on("change", function() {
			$(this).valid(); 
		});
	});

	$(document).on("click","#schEndDate",function(){
		$(this).datepicker({showOn:"focus",autoclose:true,changeMonth: true,changeYear: true, yearRange: '1950:2050'}).focus();
		$(this).attr("autocomplete", "off");
		$(this).datepicker({minDate: "0",showOn:"focus",changeMonth: true,changeYear: true, yearRange: '1950:2050'}).on("change", function() {
			$(this).valid(); 
		});
	});
	</script>
	<?php
	if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="needatten") {
		$heading = "Needs Attention Booking Listing";
	} else if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="future") {
		$heading = "Future Booking Listing";
	} else if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="today") {
		$heading = "Today Booking Listing";
	} else if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="tomorrow") {
		$heading = "Tomorrow Booking Listing";
	} else if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="past") {
		$heading = "Past Booking Listing";
	} else if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="trash") {
		$heading = "Trash Booking Listing";
	} else {
		$heading = "All";
	}
	?>
	<h1 class="pageheading"><?php echo fnMultiStudioTitle($_SESSION["stId"]);?><?php echo $heading;?></h1>
	<style>
	.booking-msg,.booking-del-msg{color:#6fcf00; font-weight:bold; text-align:center;margin-bottom: 20px;}
	.fldCls{display:none;}
	.dataTables_processing { position:absolute; z-index: 1000; width:100%; top:40%;}
	.loader-control{ position:relative;}
	</style>
	<?php
	if(isset($_SESSION['msg']) && $_SESSION['msg']!="") {?>
		<script>
		setInterval(function(){ $('.booking-msg').hide(); }, 3000);
		</script>
		<div class="booking-msg" style="display:block;"><?php echo $_SESSION['msg'];?></div>
	<?php }?>
	<div class="booking-del-msg" style="display:none;"></div>
	<div id="download_img" style="text-align: center; position: fixed; display:none; z-index: 99999; left: 0; right: 0; top: 0; bottom: 0;" >
		<img src="<?php echo base_url_site;?>images/ajax-loader.gif"  style="position: relative;  top: 50%;"/ >
	</div>
		
	<div class="form-area">
		<div class="event-listing-module">
			<div class="elm-control">
				<div class="dwnaction" style="width:100%; float:left; margin-bottom:15px;">
					<?php 
					if(isset($_REQUEST['type']) && $_REQUEST['type'] =="needatten") { 
						$bookingType = "needatten";
						$orderbyLoad =" order by id desc";
						$stwhreLoad.= "  and isDeleted=0 and status=3 ";
					} 
					else if(isset($_REQUEST['type']) && $_REQUEST['type'] =="future") {
						$bookingType = "future";
						$orderbyLoad=" order by bookingCreationDate desc";
						$stwhreLoad.= "  and isDeleted=0 and boEventDate >= CURDATE() ";
					} 
					else if(isset($_REQUEST['type']) && $_REQUEST['type'] =="today") { 
						$bookingType = "today";
						$orderbyLoad=" order by bookingCreationDate desc";
						$stwhreLoad.= "  and isDeleted=0 and boEventDate = CURDATE() ";
					} 
					else if(isset($_REQUEST['type']) && $_REQUEST['type'] =="tomorrow") { 
						$bookingType = "tomorrow";
						$orderbyLoad=" order by bookingCreationDate desc";
						$stwhreLoad.= "  and isDeleted=0 and date(boEventDate) = date_add(CURDATE(), interval 1 day) "; 
					} 
					else if(isset($_REQUEST['type']) && $_REQUEST['type'] =="past") { 
						$bookingType = "past";
						$orderbyLoad=" order by bookingCreationDate desc";
						$stwhreLoad.= "  and isDeleted=0 and boEventDate < CURDATE() ";
					} 
					else if(isset($_REQUEST['type']) && $_REQUEST['type'] =="trash") { 
						$bookingType = "trash";
						$orderbyLoad=" order by id desc";
						$stwhreLoad.= "  and isDeleted=1";
					}
					else { 
						$bookingType = "all";
						$orderbyLoad=" order by id desc";
						$stwhreLoad.= "  and isDeleted=0 ";
					} 
					
					//Getting total number records at page load
					$sql656 = "SELECT * FROM `bb_booking` where eventLocationId IN(".$_SESSION["stId"].") $stwhreLoad $orderbyLoad";
					$cRev656 = $db->rawQuery($sql656, array(''));
					if(count($cRev656)>0){ ?>
						<!--<button class="exportBtn grey-btn" customAttr="<?php //echo $_SESSION['stId'];?>" customBookType="<?php //echo $bookingType;?>">Download </button>-->
						
						<form role="form" method="post" action="" id="addDtRangeFrm">
							<div class="floatingBox">
								<input type="text" name="schStDate" value="" id="schStDate" placeholder="" autocomplete="off" />
								<label class="floating-lab">Start Date (MM/DD/YYYY)</label>
							</div>
							
							<div class="floatingBox" style="width:200px!important;">
								<input type="text" name="schEndDate" value="" id="schEndDate" placeholder="" autocomplete="off"/>
								<label class="floating-lab">End Date (MM/DD/YYYY)</label>
							</div>
							<input type="hidden" name="stdLocId" value="<?php echo $_SESSION['stId'];?>" id="stdLocId" />
							<input type="hidden" name="bookingType" value="<?php echo $bookingType;?>" id="bookingType" />
							<button class="exportBtn grey-btn" type="submit">Download</button>
						</form>
					<?php } ?>
				</div>
				
				<style>
				#addDtRangeFrm .floatingBox{width:208px!important; margin-right:10px; }
				#addDtRangeFrm .floatingBox .floating-lab{top:8px;}
				#addDtRangeFrm .floatingBox input[type="text"]{padding:7px 10px;}
				#addDtRangeFrm{text-align:right;}
				.float{top:-16px!important;}
				.tooltip-inner {
					max-width: 200px;
					padding: 3px 8px;
					color: #fff;
					text-align: center;
					background-color: #000;
					border-radius: 4px;
				}
				.tooltip {
					font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
					font-size: 12px;
					font-style: normal;
					font-weight: 400;
					line-height: 1.42857143;
					text-align: left;
					text-align: start;
					text-shadow: none;
					text-transform: none;
					letter-spacing: normal;
					word-break: normal;
					word-spacing: normal;
					word-wrap: normal;
					white-space: normal;
				}
				::after, ::before {
					-webkit-box-sizing: border-box;
					-moz-box-sizing: border-box;
					box-sizing: border-box;
				}
				.tooltip.top .tooltip-arrow {
					bottom: 0;
					left: 50%;
					margin-left: -5px;
					border-width: 5px 5px 0;
					border-top-color: #000;
				}
				.tooltip-arrow {
					position: absolute;
					width: 0;
					height: 0;
					border-color: transparent;
						border-top-color: transparent;
					border-style: solid;
				}
				.tooltip.top {
					padding: 5px 0;
					margin-top: -3px;
				}
				.fade.in {
					opacity: 1;
				}
				.tooltip.in {
					filter: alpha(opacity=90);
					opacity: .9;
				}
				.tooltip {
					position: absolute;
					z-index: 1070;
					display: block;
					font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
					font-size: 12px;
					font-style: normal;
					font-weight: 400;
					line-height: 1.42857143;
					text-align: left;
					text-align: start;
					text-decoration: none;
					text-shadow: none;
					text-transform: none;
					letter-spacing: normal;
					word-break: normal;
					word-spacing: normal;
					word-wrap: normal;
					white-space: normal;
					filter: alpha(opacity=0);
					opacity: 0;
					line-break: auto;
				}
				.fade {
					opacity: 0;
					-webkit-transition: opacity .15s linear;
					-o-transition: opacity .15s linear;
					transition: opacity .15s linear;
				}
				</style>
				
				<div class="elm-row loader-control" style="overflow:auto; overflow-y:hidden;">
					<table class="elm-table" id="booking-grid">
						<thead>
						<tr>
							<th>Action</th>
							<th>Status</th>
							<th>Booking ID</th>
							<th>Booking Date</th>
							<th>Customer Name</th>
							<th>Customer Email</th>
							<th>Customer Phone</th>
							<th>Seats</th>
							<th>Project</th>
							<th>Personalizations</th>
							<th>Coupon Code</th>
							<th>Total</th>
							<th>Gateway</th>
							<!--<th>Address</th>
							<th>City</th>
							<th>State</th>
							<th>Zip Code</th>
							<th>Event Date</th>-->
						</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
	<script>
	$.validator.addMethod("greaterThan",function(value, element, param) {
		if($(param).val() != ''){
			if(Date.parse(value) >= Date.parse($(param).val())){
				if( $('#schStDate').hasClass('error')) {
					$('#schStDate').removeClass('error')
					$('#schStDate').addClass('valid')
					$('#schStDate').next('.tooltip').remove();
				}
				return this.optional(element) || Date.parse(value) >= Date.parse($(param).val());
			} else {
				return false;
			}
		} else {
			return true;
		}
	},'Must be greater than or equal to booking start date.');
	
	$.validator.addMethod('lessThan', function(value, element, param) {
		if($(param).val() != ''){
			if(Date.parse(value) <= Date.parse($(param).val())){
				if( $('#schEndDate').hasClass('error')) {
					$('#schEndDate').removeClass('error')
					$('#schEndDate').addClass('valid')
					$('#schEndDate').next('.tooltip').remove();
				}
				return this.optional(element) || Date.parse(value) <= Date.parse($(param).val());
			} else {
				return false;
			}
		} else {
			return true;
		}
	}, 'Must be less than or equal to booking end date.');
	
	$.validator.addMethod("checkDateFormat",function(value, element) {
		return value.match(/^(0[1-9]|1[012])[\/|-](0[1-9]|[12][0-9]|3[01])[\/|-](19\d\d|2\d\d\d)$/);
	},"Please enter a date in the format mm/dd/yyyy");

	$("form[id='addDtRangeFrm']").validate({
		rules: {
			schStDate: {
				required :true,
				lessThan: "#schEndDate",
				checkDateFormat: true,
			},
			schEndDate: {
				required :true,
				greaterThan: "#schStDate",
				checkDateFormat: true,
			}
		},
		messages: {
			schStDate: {required: "Please enter start date",date: "Please enter valid date"},
			schEndDate: {required: "Please enter end date" ,date: "Please enter valid date"},
		},
		submitHandler: function(e) { 
			$('#download_img').show();
			$.ajax({
				url : '<?php echo base_url_site;?>ajax',
				type: 'post',
				data: $('#addDtRangeFrm').serialize(),
				success: function(msg) {
					$('#download_img').hide(); 
					var data = JSON.parse(msg);
					//var url = "<?php echo base_url_site;?>"+"reports/"+data.filename;
					//window.open(SITEURL1+"reports/"+data.filename, '_blank','width='+windowWidth+', height='+windowHeigth+'');
					window.open("<?php echo base_url_site;?>"+"reports/"+data.filename, '_self');
					return false;
				},
				error: function() {
					alert('There has been an error.');
				}
			});
		}
	});		
	</script>
</div>

<!-- left content area end-->
<?php
include('includes/footer.php');
?>