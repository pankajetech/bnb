<?php include('includes/header.php');
$menuClssUsers = "downarrow";
$menuSbClssUsers = "sub";
$menuSbClssUsers2 = "active";
$menuSbClssStyleUsers = "style='display:block;'";

if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 
if(isset($_SESSION['urole']) && $_SESSION['urole']!=1){ header("location: ".base_url_site."dashboard");    exit; } 


if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") {    
//$numOrders = $result['Count'];
$params = array('');
$result = $db->rawQueryOne("SELECT * FROM bb_users WHERE id='".$_REQUEST["id"]."' ", $params);
$ritems = (array)$result;

if(!empty($ritems)) $ritems=$ritems; 
} else {
$ritems=array();
$ritems=$ritems;
}




	 ?>

<!-- Header end-->
<style>
.token-input-list-facebook{
display:none;
}
</style>

<!-- left nagivation start-->

<?php include('includes/left-sidebar.php');?>
<script type="text/javascript" src="<?php echo base_url_js?>jquery.tokeninput.js"></script>
    <link rel="stylesheet" href="<?php echo base_url_css?>token-input.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url_css?>token-input-facebook.css" type="text/css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>select-style.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>selectize.default.css">
<script type="text/javascript" src="<?php echo base_url_js?>selectize.js" /></script>
<!-- left nagivation end-->
<!-- left nagivation end-->

<!-- left content area start-->
<div class="loading" style="display:none;">Loading&#8230;</div>
<div class="right-wrapper" id="frmChanged">
<h1 class="pageheading"><?php if(!isset($_REQUEST["id"]))  echo "Add";  else  echo "Update" ?>  User</h1>
<div class="form-area">
 <form role="form" method="post" action="" enctype="multipart/form-data" name="frmaddupUser" id="frmaddupUser">
<input type="hidden" name="id" id="id" value="<?php echo $_REQUEST["id"]; ?>" />
<input type="hidden" name="did" id="did" value="<?php echo $_REQUEST["did"]; ?>" />
<?php if((isset($_GET["stid"]) && $_GET["stid"]!="") && (!isset($_GET["did"]) && $_GET["did"]=="") && (!isset($_GET["id"]) && $_GET["id"]=="")) {?>
<div class="input-row">
<input type="checkbox" class="uexist" id="useExist" name="useExist" value="" onclick="javascript:valueChanged();" /> Use Existing User
</div>
<div class="input-row uEid" style="display:none;">
<div class="control-group">
						<select id="txtExistU" name="txtExistU" class="left49">
							<option value="">Existing User</option>
								<?php echo fnMDropDownList("bb_users","id","email",@fnCheckEmpty($ritems['id']),"isActive=0 and isDeleted=0 and role=3");?>
						</select>
					</div>
					<script>
					$('#txtExistU').selectize();
					</script>

</div>
<?php }?>
<div class="user-info">
<div class="input-row">
<div class="floatingBox left49"><input class="pdf-text myElements remspace" type="text" id="txtFirtName" name="txtFirtName" placeholder="" value="<?php echo @fnCheckEmpty($ritems['firstName']); ?>" /><label class="floating-lab">First Name</label></div>
<div class="floatingBox right49"><input class="pdf-text myElements remspace" type="text" id="txtLastName" name="txtLastName" placeholder="" value="<?php echo @fnCheckEmpty($ritems['lastName']); ?>" /><label class="floating-lab">Last Name</label></div>
</div>

<div class="input-row">
<div class="floatingBox left49"><input class="pdf-text myElements remspace" type="text" id="txtEmail" name="txtEmail" value="<?php echo @fnCheckEmpty($ritems['email']); ?>" <?php //if(isset($_REQUEST["id"])) echo 'readonly="readonly"';?> placeholder="" /><label class="floating-lab">Email</label></div>
<select class="right49 pdf-text myElements" id="isSend" name="isSend">
<option value="">Select Invite</option>
<option value="1" <?php if(isset($ritems['isInvite']) && $ritems['isInvite']==1) echo "selected"; ?>>Send Invite</option>
<option value="2" <?php if(isset($ritems['isInvite']) && $ritems['isInvite']==2) echo "selected"; ?>>Save for Later</option>
</select>
</div>

<div class="input-row">
<?php 
$roleid="";
$stid="";
if(isset($_GET["stid"]) && $_GET["stid"]!="") { 
$roleid=3; 
		if(isset($_GET["id"]) && $_GET["id"]!="") { 
		$stid1=$_GET["stid"];
		$uinfo=getUserStudio($_GET["id"],$stid1);
		//print_r($uinfo);
		//die();
				if($uinfo) {
				$stid=$uinfo['studioLocation'].",".$stid1;
				}
		} else {
		$stid=$_GET["stid"]; 
		$strDis='readonly="readonly"';
		}
} else { $roleid=@fnCheckEmpty($ritems['role']); $stid=@fnCheckEmpty($ritems['studioLocation']); $strDis=''; }

?>
<select class="left49 pdf-text myElements" id="lstRole" name="lstRole">
<option value="">Select User Type</option>
<?php echo fnDropDownList("bb_userroles","id","roleName",$roleid,"");?>
</select>
<?php
if(isset($stid) && $stid!="") { 
//echo "SELECT id, stName FROM `bb_studiolocation` where id IN(".$stid.") and isDeleted=0 order by id asc";
$stlist = $db->rawQuery("SELECT id, stName FROM `bb_studiolocation` where id IN(".$stid.") and isDeleted=0 order by id asc", $params);
$studiolist=array();
$studiojlist=array();
foreach ($stlist as $key=>$item) {
$studiolist['id']=$item['id'];
$studiolist['stName']=$item['stName'];
$studiojlist[]=$studiolist;
}

}


?>


<input type="text" class="right49 myElements" id="lstLocations" name="lstLocations"  paceholder="Studio Locations" />
<script type="text/javascript">
        $(document).ready(function() {
            $("#lstLocations").tokenInput("addon/lstlocations.php", {
			propertyToSearch: "stName",
                theme: "facebook",
				tokenDelimiter: ",",
				preventDuplicates: true,
				<?php  if(isset($stid) && $stid!="") { ?> 
				prePopulate: <?php echo json_encode($studiojlist);?>
			<?php }?>	
            });
        });
        </script>
</div>
<?php if(isset($_GET["id"]) && $_GET["id"]!="") { ?>
<div class="floatingBox left49" style="margin-top:10px;"><input class="left49 pdf-text myElements remspace" type="password" id="txtRstPwd" name="txtRstPwd" placeholder="" value="" /><label class="floating-lab">Reset Password</label></div>
<?php }?>





<div class="bottom-btn"><input style="padding-left:30px; padding-right:30px;" class="yellow-btn" id="btnSave" name="btnSave" type="submit" value="Save" /></div>
<input type="hidden" name="txtChanged" id="txtChanged" value="0" />
</form>
</div>

</div>

<!-- left content area end-->
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>bootstrap-3.3.5.min.css">
    <script type="text/javascript" src="<?php echo base_url_js?>bootstrap-3.3.5.min.js" /></script>
    <script type="text/javascript" src="<?php echo base_url_js?>jquery-validate.bootstrap-tooltip.js" /></script>
<script>
function valueChanged()
{
    if($(".uexist").is(":checked"))   
        $(".uEid").show();
    else
        $(".uEid").hide();
}
/*
$(document).on('click', function(event) {
if($("#txtChanged").val()==1) {
//alert("outside");
} else {
return true;
}
});*/

$(document).on("blur",".remspace",function() {
var str = $(this).val();
var str = $.trim(str);
var newStr = str.replace(/\s+/g,' ').trim();
$(this).val(newStr);
});


$(document).ready(function() {




/*$(document).on('click', function(event) {
alert("outside");
});*/

/*$('#div3').on('click', function() {
	return false;
});*/

/*$(window).click(function () {
if($("#txtChanged").val()==1) {
    var dlg='<p>You have un-saved changes on page, Are you sure want to leave?</p>';
		
			doConfirm(dlg, function yes() {
            
            }, function no() {
			
			});
} else {
return false;
}	
});


$('#frmChanged').click(function (event) {
   return false;
    event.stopPropagation();
});*/


$('.myElements').each(function() {
   var elem = $(this);

   // Save current value of element
   elem.data('oldVal', elem.val());

   // Look for changes in the value
   elem.bind("propertychange change click keyup input paste", function(event){
      // If value has changed...
      if (elem.data('oldVal') != elem.val()) {
       // Updated stored value
       elem.data('oldVal', elem.val());

	
	$("#txtChanged").val(1);
       // Do action
       //alert("test");
     }
   });
 });


$("#txtExistU").on('change',function(){
sid=$(this).val();
window.location.href="<?php echo base_url_site?>addupduser?stid=<?php echo $_REQUEST['stid'];?>&id="+sid;
 });
$("#lstRole").on('change',function(){
var sid="";
sid=$(this).val();
if(sid==3){
$(".token-input-list-facebook").show();
//$("#lstLocations").show();
} else {
$(".token-input-list-facebook").hide();
//$("#lstLocations").hide();
}

     });
<?php if((isset($_GET["stid"]) && $_GET["stid"]!="") || (isset($roleid) && $roleid!="")) {?>
$('#lstRole').change();
<?php }?>
	$.validator.addMethod("customemail", 
		function(value, element) {
			return /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
		}, 
		"Please enter a valid email address"
	);	 
$("#frmaddupUser").validate({
			ignore: [],
			debug: false,
			rules: {
			txtFirtName: "required",
			txtLastName:"required",
			txtEmail: {
			required: true,
			customemail:true,
			<?php // if(!isset($_REQUEST["id"]) && $_REQUEST["id"]==""){?>  
			remote: {
			url: "<?php echo base_url_site?>chkExist.php",
			type: "post",
			data:
			{
			ptitle: function()
			{
			return $('#frmaddupUser :input[name="txtEmail"]').val();
			},
			tblname: function()
			{
			return "bb_users";
			},
			coln: function()
			{
			return "email";
			},
			colid: function()
			{
			return "<?php echo $_REQUEST["id"]?>";
			},
			colid: function()
			{
			return "<?php echo $_REQUEST["id"]?>";
			},
			isDeleted: function()
			{
			return "0";
			}
			},
			complete: function(data) {
		
			if(data.responseText=="false") {
			var dlg='<p>User already exists, is this a multi-studio user?</p>';
		
			doConfirm(dlg, function yes() {
			var emailid = $('#txtEmail').val();
			$.post("<?php echo base_url_site; ?>addon/chkMEmailUser", {emailid:emailid}, function(response) {
			var data = JSON.parse(response);
			<?php if(isset($_REQUEST['stid']) && $_REQUEST['stid']!="") {?>
			window.location.href="<?php echo base_url_site?>addupduser?stid=<?php echo $_REQUEST['stid'];?>&id="+data.uid;
			<?php } else {?>
			window.location.href="<?php echo base_url_site?>addupduser?id="+data.uid;
			<?php } ?>
			});
			}, function no() {
			
			});
			
			}
			
			}
			} 
			<?php //}?>
			},
			isSend: "required",
			lstRole: "required",
			lstLocations: {
            required: function(element){
						if($("#lstRole").val()==3) {
						$("ul.token-input-list-facebook").addClass("error");
                         return true;
						 } else {
						 $("ul.token-input-list-facebook").removeClass("error");
						 return false;
						 }
                        },

             },
			
			<?php if(isset($_GET["id"]) && $_GET["id"]!="") { ?>
			txtRstPwd: {
							 minlength : 5
                        },
			<?php }?>			
			},			
			messages: {
				txtEmail: {
				required: "Please enter a valid email.",
				<?php if(!isset($_REQUEST["id"])){?> 
				remote: jQuery.validator.format("{0} is already taken.")
				<?php }?>     
				},
			txtFirtName: "Please enter first name.",
			txtLastName:"Please enter last name.",	
			isSend:"Please select invite.",
			lstRole:"Please select role.",
			lstLocations:"Please select location.",
			},
		submitHandler: function(form) {
		
		<?php if(isset($_REQUEST["id"])){?> 
		var dlg='<p>Are you sure you would like to update user info</p>';
		
		doConfirm(dlg, function yes() {
		
		jQuery('.loading').show(); 
			var dataf = new FormData();
			var form_data = $('#frmaddupUser').serializeArray();
           // alert (form_data); 
			
			$.each(form_data, function() {
			dataf.append(this.name, this.value);
			});

			

			
			jQuery.ajax({
			url : '<?php echo base_url_site; ?>addon/ajaxSaveUser', 
			type : 'POST',              // Assuming creation of an entity
			mimeType: "multipart/form-data",
			contentType : false,        // To force multipart/form-data
			data : dataf,
			processData : false,
			success : function(response) {

						var data = JSON.parse(response);
						console.log(data);
						///$('.event-msg').html("");
						if(data.result==1){
						jQuery('.loading').hide(); 
						
						window.location.href="<?php echo base_url_site?>users";
						
						
						}
			
			
			}
    		});

        }, function no() {

        });
		
		<?php } else {?>
		
		jQuery('.loading').show(); 
			var dataf = new FormData();
			var form_data = $('#frmaddupUser').serializeArray();
           // alert (form_data); 
			
			$.each(form_data, function() {
			dataf.append(this.name, this.value);
			});

			

			
			jQuery.ajax({
			url : '<?php echo base_url_site; ?>addon/ajaxSaveUser', 
			type : 'POST',              // Assuming creation of an entity
			mimeType: "multipart/form-data",
			contentType : false,        // To force multipart/form-data
			data : dataf,
			processData : false,
			success : function(response) {

						var data = JSON.parse(response);
						console.log(data);
						///$('.event-msg').html("");
						if(data.result==1){
						jQuery('.loading').hide(); 
						
						window.location.href="<?php echo base_url_site?>users";
						
						
						}
			
			
			}
    		});
		
		<?php } ?>
		
		

			
		
			
				
		}
		});	 
	 
 }); 
</script>

<?php
include('includes/footer.php');
?>