<?php 
error_reporting(E_ALL & ~E_NOTICE);
include('includes/header.php');
$menuClssStudio = "downarrow";
$menuSbClssStudio = "sub";
$menuSbClssStudio1 = "active";
$menuSbClssStyleStudio = "style='display:block;'";

if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 
//if(isset($_SESSION['urole']) && $_SESSION['urole']!=1){ header("location: ".base_url_site."dashboard"); exit; } 
if(isset($_SESSION['urole']) && $_SESSION['urole']== 3 ){ header("location: ".base_url_site."dashboard"); exit; } 

if(isset($_REQUEST["delid"]) and $_REQUEST["delid"]!="") { 
$dataD=array("isDeleted"=>1);
$db->where('id',$_REQUEST["delid"]);
$db->update('bb_studiolocation',$dataD);

$_SESSION["msg"]="Successfully deleted studio location info";
header("location: studiolocs.php");
exit;
}
	 ?>

<!-- Header end-->


<!-- left nagivation start-->

<?php include('includes/left-sidebar.php');?>
<link href="<?php echo base_url_css?>dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<!-- left nagivation end-->
<!-- left nagivation end-->

<!-- left content area start-->
<div class="loading" style="display:none;">Loading&#8230;</div>
<div class="right-wrapper">
<h1 class="pageheading">Studio Locations</h1>
<h3>  
<?php 
echo '<a href="'.base_url_site.'migration-script/events">Studio Migration Script</a>';
?>
 </h3>
<div class="form-area">
<div class="listing-wrapper">
<div class="listing-table">
<table width="100%" cellpadding="0" cellspacing="0" id="dataTables-example">
<thead>
<tr>
<th>Action</th>
<th>No.</th>
<th>Name</th>
<th>Email</th>
<th>Phone</th>
<th>Studio Status</th>


</tr>
</thead>

</table>
</div>

</div>
</div>
</div>

<!-- left content area end-->



<link href="<?php echo base_url_css?>dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<script src="<?php echo base_url_css?>dataTables/buttons/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url_css?>dataTables/buttons/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url_css?>dataTables/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url_css?>dataTables/buttons/1.5.2/js/buttons.html5.min.js"></script>
 
<script>
$(document).ready(function() {

/*vtable = $('#dataTables-example').DataTable( {
dom: '<"elm-row"<"bubbleInfo"<"fl spage"<"paging"pl><"clear">><"fr page-text"i>>>',
pagingType: "full_numbers",
sortable: false,
paginate: true,
pageLength: 50,
info: true,
bSort: true,
bFilter: false,
"aaSorting": [],
"aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0 ] } ]
} );
*/


vtable = $('#dataTables-example').DataTable( {
		"dom": '<"elm-row"<"bubbleInfo"<"fl spage"<"paging"pl><"clear">><"fr page-text"i>>>rt',
		"processing": true,
		"language": {
			processing: "<div style='text-align:center'><img src='<?php echo base_url_site;?>images/ajax-loader.gif'></div>",
		},
		"serverSide": true,
		"ajax":{
			url :"<?php echo base_url_site; ?>addon/studio-grid-data", // json datasource
			data: function (d) {
                d.estatus = "";
            },
			type: "post",  //method,by default get
			error: function(){  //error handling
				$(".dataTables-example-error").html("");
				$("#dataTables-example").append('<tbody class="dataTables-example-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
				$("#dataTables-example_processing").css("display","none");
			}
		},
		order: [[ 0, "desc" ]],
		pagingType: "full_numbers",
		sortable: false,
		paginate: true,
		pageLength: 50,
		info: true,
		bSort: true,
		bFilter: false,
		"aaSorting": [],
		"aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0 ] } ]
	} );

//$(".vslogin").on('click',function(){
$(document).on("click",".vslogin",function() {
jQuery('.loading').show();
var stid=$(this).attr('id');
//alert(stid);
$.post("<?php echo base_url_site; ?>addon/ajaxStudioLogin.php", {studioId:stid}, function(response) {
	var data = JSON.parse(response);
	if(data.result==0) {
		jQuery('.loading').hide();
		window.location.href="<?php echo base_url_site; ?>edit-locationinfo?stId="+stid;
	} else {
		window.location.href="<?php echo base_url_site; ?>logout";
	}
	 });


});

$('.del').click(function(){
var delid=$(this).attr('id');
var dlg='<p>Would you like to delete studio location info</p>';

var dialog = $(dlg).dialog({
            width : "400",
            height: "200",
            modal : true,
                    buttons: {
                        "Yes": function() {
						window.location.href="<?php echo base_url_site?>studiolocs?delid="+delid;
						dialog.dialog('close');
						},
                        "No":  function() {
						dialog.dialog('close');
						}
                    }
                });

});
});
</script>

<?php
include('includes/footer.php');
?>
