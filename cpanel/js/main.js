// JavaScript Document

function doConfirm(msg, yesFn, noFn) {
    var confirmBox = $("#confirmBox");
	confirmBox.find(".modal-wrapper").width(400).height(200);
    confirmBox.find(".message").html(msg);
    confirmBox.find(".yes,.no,.clsclose").unbind().click(function () {
        confirmBox.hide();
    });
    confirmBox.find(".yes").click(yesFn);
    confirmBox.find(".no").click(noFn);
    confirmBox.show();
}

$(document).ready(function(){
	jQuery('.down').click(function(){
		if( $(this).hasClass("downarrow")){
			$(this).removeClass("downarrow");
			$(this).parents("li").siblings().children("a").removeClass("downarrow");
			$(this).parents("li").siblings().children("ul").slideUp(350);
			$(this).next().removeClass("sub");
			$(this).next().slideUp(350);
		}
		else{
			$(this).addClass("downarrow");
			$(this).parents("li").siblings().children("a").removeClass("downarrow");
			$(this).parents("li").siblings().children("ul").slideUp(350);
			$(this).next().addClass("sub");
			$(this).next().slideDown(350);
		}
	});
	
	$(".left-navigation ul li .submenu li").click(function(){
		$(this).addClass("active");
		$(this).siblings().removeClass("active");
	});
	
	
	
	
	$(".pdf-text").focus(function(){
	if( !$(this).val() ) {
	$(this).siblings(":last-child").addClass('floating-label');
	}
    
    });
	
	
	
	$(".pdf-text").blur(function(){
		if ($(this).attr('id') == 'label_date'){
			setTimeout(delayCount, 100);
		}else{
			if( !$(this).val() ) {
				$(this).siblings(":last-child").removeClass('floating-label');
			}
		}
		
    });
	
	
//	function delayCount(){
//		var str = $('#label_date').attr('class');
//		var res = str.split(" ");
//		if (res.length == 4) {
//			$('#label_date').siblings(":last-child").addClass('floating-label');
//		}else{
//			$('#label_date').siblings(":last-child").removeClass('floating-label');
//		}
//	}
	

	function loadFunction(){
	//$('form').find('.pdf-text').each(function(){
	$('.pdf-row').find('.pdf-text').each(function(){
		if( $(this).val() ) {
			$(this).next().addClass('floating-label');
			
		}
	});
	}
	loadFunction();
	
	
	
	
	
	
});






