<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
include('includes/header.php');
$menuClssBookingTemp = "downarrow";
$menuSbClssBookingTemp = "sub";
$menuSbClssStyleBookingTemp = "style='display:block;'";
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php"); exit; } 

/* strpos that takes an array of values to match against a string
 * note the stupid argument order (to match strpos)
 */
/*function strpos_arr_check($haystack, $needle) {
    $accommodList = preg_split('/\r\n|\r|\n/', $haystack);
	//echo "<pre>accommodList==";print_r($accommodList);
	
	$accommodList_str_separated = implode("!@#$,", $accommodList);
	//echo "<pre>accommodList_str_separated==";print_r($accommodList_str_separated);
	//$match = 'zip:';
	if(stripos($accommodList_str_separated, $needle) !== false) {
		$used1 = explode($needle,$accommodList_str_separated);
		//echo "<pre>used1==";print_r($used1);
		$used11 = explode('!@#$',$used1[1]);
		//echo "<pre>used11==";print_r($used11);
		$czip = $used11[0];
	} else  {
		$czip = '';
	}	
	return $czip;	
}*/

if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") {    
	$params = array('');
	$result = $db->rawQueryOne("SELECT * FROM bb_booking WHERE id='".$_REQUEST["id"]."' and isDeleted=0 ", $params);
	$ritems = (array)$result;
	if(!empty($ritems)) {
		$ritems = $ritems;
		//Get booking address,city,state,zip from ci_transaction_detail tbl!
		if(!empty($ritems)){
			$tansInfo = getBookingTransactioninfo($ritems['id']);
			//echo "<pre>tansInfo==";print_r($tansInfo);die;
			
			if($tansInfo['caddress'] != ''){
				$boAddress = $tansInfo['caddress'];
			} else {
				$boAddress = $ritems['boAddress'];
			}
			
			if($tansInfo['czip'] != ''){
				$czip = $tansInfo['czip'];
			} else { 
				$czip = strpos_arr_check($ritems['boAccommodationsNotes'],'Zip:');
			}
			
			if($tansInfo['ccity'] != ''){
				$ccity = $tansInfo['ccity'];
			} else {
				$ccity = strpos_arr_check($ritems['boAccommodationsNotes'],'City:');
			}
			
			if($tansInfo['cstate'] != ''){
				$cstate = $tansInfo['cstate'];
			} else {
				$cstate = strpos_arr_check($ritems['boAccommodationsNotes'],'State:');
			}
		}
	} else {
		header("location: ".base_url_site."bookinglist"); exit;
	}
} else { 
	$ritems = array();
	$ritems = $ritems;
	header("location: ".base_url_site."bookinglist"); exit;
}

//print_r($_SESSION);

?>
<!-- Header end-->
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<!-- left nagivation end-->
<!-- ToolTip Type Validation-->
<link href="<?php echo base_url_css?>datepicker/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url_js?>datepicker/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url_js?>additional-methods.js" /></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>bootstrap-3.3.5.min.css">
<script type="text/javascript" src="<?php echo base_url_js?>bootstrap-3.3.5.min.js" /></script>
<script type="text/javascript" src="<?php echo base_url_js?>jquery-validate.bootstrap-tooltip.min.js" /></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>select2.min.css">
<script type="text/javascript" src="<?php echo base_url_js?>select2.min.js" /></script>
<script type="text/javascript" src="<?php echo base_url_js?>jquery.maskedinput-1.3.1.min_.js" /></script>

<!-- left content area start-->
<div class="loading" style="display:none;">Loading&#8230;</div>

<!-- left nagivation end-->

<!-- left content area start-->
<div class="right-wrapper">
	<h1 class="pageheading"><?php echo fnMultiStudioTitle($_SESSION["stId"]);?>Booking Record</h1>
	<style>
	.booking-resend-msg{color:#6fcf00; font-weight:bold; text-align:center;margin-bottom: 20px;}
	</style>
	<script>
		setInterval(function(){ $('.booking-resend-msg').hide(); }, 3000);
	</script>
	<div class="booking-resend-msg" style="display:none;"></div>
	
	<script>
	var type = '<?php echo $_REQUEST["type"];?>';
	if(type == 'customer') {
		$('.customer').addClass("active");
	} 
	else if(type == 'needatten') {
		$('.needatten').addClass("active");
	} 
	else if(type == 'future') {
		$('.current').addClass("active");
	} 
	else if(type == 'today') {
		$('.today').addClass("active");
	} 
	else if(type == 'tomorrow') {
		$('.tomorrow').addClass("active");
	} 
	else if(type == 'past') {
		$('.past').addClass("active");
	} 
	else if(type == '') { //all boking list
		$('.allbooking').addClass("active");
	}
	</script>
	<div class="form-area">
	<form role="form" method="post" action="" id="addUpdBookingFrm">
		<!-- 0=>Completed,1=>Unapprove,2=>Cancel,3=>Need Attention,4=>Change Project Pending,5=>Reject -->
		<div class="bnt-row">
			<div class="enbdis">
				<?php
				//edit case
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="" ){ 
					if($ritems["status"] != "0" ){ 
				?>
					<button class="yellow-btn actionEvent" id="approveBoookingAction" type="submit">Approve</button>
				<?php 
				} else{ 
					?>
					<button class="dsbl-btn actionEvent" style="display:none;" id="approveBoookingAction" disabled alt="Needs attention or cancelled booking,can use this" title="Needs attention or cancelled booking,can use this">Approve</button>
				<?php	
					}
				}?>
			</div>
			
			<div class="enbdis">
				<?php
				//edit case
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="" ){ 
					if($ritems["status"] == "2" || $ritems["status"] == "3" || $ritems["status"] == "4"){
				?>
					<button class="yellow-btn actionEvent" id="rejectBoookingAction" type="submit">Reject</button>
				<?php 
					} else if($ritems["status"] == "5"){
					?>
					<button class="dsbl-btn actionEvent" style="display:none;" id="rejectBoookingAction" disabled alt="Needs attention or cancelled booking,can use this" title="Needs attention or cancelled booking,can use this">Reject</button>
				<?php	
					}
				}?>
			</div>
			
			<div class="enbdis">
				<?php
				//edit case
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="" ){ 
					if($ritems["status"] == "0"){
				?>
					<button class="yellow-btn actionEvent" id="unapproveBoookingAction" type="submit">Unapprove</button>
				<?php 
					} 
					elseif($ritems["status"] == "1"){ ?>
					<button class="dsbl-btn actionEvent" style="display:none;" id="unapproveBoookingAction" disabled alt="Only approved booking,can use this" title="Only approved booking,can use this">Unapprove</button>
				<?php
					}
				}?>
			</div>
			
			<div class="enbdis">
				<?php
				//edit case
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="" ){ 
					if($ritems["status"] == "0"){
				?>
					<button class="yellow-btn actionEvent" id="resendmail" type="button">Resend Confirmation</button>
				<?php 
					} 
				}?>
			</div>
			
			<?php
			//edit case
			if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="" ){ 
				if($ritems["status"] == "1" || $ritems["status"] == "2" || $ritems["status"] == "5"){
			?>
				<button class="dsbl-btn actionEvent" style="display:none;" id="cancelBoookingAction" disabled alt="You can use this in active order" title="You can use this in active order">Cancel Booking</button>
			<?php 
			} else { //not cancelled
				?>
				<button class="yellow-btn actionEvent" id="cancelBoookingAction" type="submit">Cancel Booking</button>
			<?php	
				}
			}?>
		</div>
		<div class="bnt-row">
			<div class="btn-left">
				<?php 
				//update case
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") 
				{
					//update changes in needs attention	action	
					if($ritems["status"] == "3" ){ ?>
						<button class="yellow-btn actionEvent" id="updateNeedAttentionAction" type="submit">Update Changes</button>
					<?php
					}
					//update changes in change Project action
					else if($ritems["status"] == "4" ){ ?>
						<button class="yellow-btn actionEvent" id="updateChangeProjectAction" type="submit">Update Changes</button>
					<?php
					}
					//update changes in cancelled Project action
					else if($ritems["status"] == "1" || $ritems["status"] == "2" || $ritems["status"] == "5"){?>
						<button class="dsbl-btn actionEvent" id="updateCancelAction" disabled alt="You can use this in active order" title="You can use this in active order">Update Changes</button>
					<?php
					}
					//update changes in completed action
					else { ?>
						<button class="yellow-btn actionEvent" id="updateAction" type="submit">Update Changes</button>
					<?php
					}
				} 
				else 
				{ //add case?>
					<button class="yellow-btn actionEvent" id="addAction" type="submit">Save</button>
				<?php 
				} ?>
			</div>
			<div class="btn-right"> 
				<button type="reset" id="btnCancel" class="grey-btn btn-mrg-right dsbl-btn">Cancel</button>
				<!--<button type="reset" id="btnCancel" class="grey-btn">Exit</button>-->
				<button type="reset" id="btnExit" class="grey-btn">Exit</button>
			</div>
		</div>
		
		<div class="template-info">
			<div class="booking-left">
				<?php 
				//edit case
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { ?>
					<input type="hidden" name="actionType" value="updateAction" id="actionType">
				<?php 
				} 
				if(!isset($_REQUEST["id"])) { //add case?>
					<input type="hidden" name="actionType" value="addAction" id="actionType">
				<?php } ?>
				
				<input type="hidden" name="userId" value="<?php echo $_SESSION['usrid'];?>">
				
				<?php 
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") { 
					//echo "<pre>SESSION====";print_r($_SESSION);
				?>
				<input type="hidden" name="bookingId" id="bookingId" value="<?php echo $_REQUEST["id"];?>">
				<input type="hidden" name="pageType" id="pageType" value="<?php echo $_REQUEST["type"];?>">
				<input type="hidden" name="bookingStatus" id="bookingStatus" value="<?php echo $ritems["status"];?>">
				<input type="hidden" name="studioId" id="studioId" value="<?php echo $_SESSION['stId'];?>">
				
				<input type="hidden" name="pageAction" id="pageAction" value="normal">
				
				<input type="hidden" name="oldEventId" id="oldEventId" value="<?php echo $ritems["eventId"];?>">
				<?php } ?>
				
				<div class="input-row">
					<div class="floatingBox"><input class="textfull tempname etname" type="text" name="boEventName" value="<?php echo fnCheckEmpty($ritems['boEventName']);?>" placeholder="" /><label class="floating-lab">Event Name</label></div>
					<select id="BoEventName" class="TPPNEventDp" style="display:none; width:100%;"></select>
				</div>
				<div class="input-row">
					<?php
					if( strpos($ritems['eventStTime'], ':00 ') !== false ) { 
						$evST = str_replace(':00 ','',$ritems['eventStTime']);
					} else { 
						$evST = preg_replace('/\s+/', '', $ritems['eventStTime']);
					}

					if( strpos($ritems['eventEndTime'], ':00 ') !== false ) {
						$evET = str_replace(':00 ','',$ritems['eventEndTime']);
					} else {
						$evET = preg_replace('/\s+/', '', $ritems['eventEndTime']);
					}
					
					//Event Date time
					$eventDtTime="";
					if(isset($ritems['boEventDate']) && $ritems['boEventDate']!="") {
						$evDateFormated = strtotime($ritems['boEventDate']);
						if($evDateFormated >0){
							$eventDtTime.= "".date("m/d/y",strtotime($ritems['boEventDate']));
						}
					}
					if($evST!="") {
						if($evDateFormated >0) { $eventDtTime.=" - ".$evST; } else { $eventDtTime.= $evST;}
					}
					if($evET!="") {
						$eventDtTime.=" - ".$evET."";
					}
					?>
					<div class="floatingBox evt-date tempname"><input class="evt-date tempname" type="text" name="boEventDateTime" id="selEventDtTimeInDisabledFld" value="<?php echo $eventDtTime;?>" placeholder="" /><label class="floating-lab">Event Date/Time</label></div>
					
					<div class="floatingBox bkproject"><input class="bkproject tempname" type="text" placeholder="" name="boProjects" value="<?php echo fnCheckEmpty($ritems['boProjects']);?>"/><label class="floating-lab">#Projects</label> </div>
					
					
					<input type="hidden" name="boEventIdHidden" id="selEventId" value="<?php echo fnCheckEmpty($ritems['eventId']);?>" />
					
					<input type="hidden" name="boEventNameHidden" id="selEventName" value="<?php echo fnCheckEmpty($ritems['boEventName']);?>" />
					
					<input type="hidden" name="boEventDateHidden" id="selEventDt" value="<?php echo $ritems['boEventDate'];?>" />
					
					<input type="hidden" name="boEventStartTimeHidden" id="selEventStartTime" value="<?php echo $ritems['eventStTime'];?>" />
					
					<input type="hidden" name="boEventEndTimeHidden" id="selEventEndTime" value="<?php echo $ritems['eventEndTime'];?>" />
					
					
				</div>

				<div class="input-row">
					<div class="floatingBox width20"><input class="width20 cancelBtnEnble" type="text" placeholder="" name="boFullName" value="<?php echo fnCheckEmpty($ritems['boFullName']);?>" /><label class="floating-lab">Full Name</label></div>
					<div class="floatingBox width20"><input class="width20 cancelBtnEnble" type="text" placeholder="" id="boPhone" name="boPhone" value="<?php echo fnCheckEmpty($ritems['boPhone']);?>"/><label class="floating-lab">Phone</label></div>
					<div class="floatingBox bkemail"><input class="bkemail" type="text" placeholder="" name="boEmail" id="boEmail" value="<?php echo fnCheckEmpty($ritems['boEmail']);?>"/><label class="floating-lab">Email</label></div>
					<input type="hidden" name="customerEmail" id="customerEmail" value="<?php echo fnCheckEmpty($ritems['boEmail']);?>"/>
				</div>
				<div class="input-row">
					<!--<input class="textfull cancelBtnEnble" type="text" placeholder="Billing Address" name="boAddress" value="<?php //echo fnCheckEmpty($ritems['boAddress']);?>"/>-->  	
					<div class="floatingBox textfull">
						<input class="textfull cancelBtnEnble" type="text" placeholder="" name="caddress" id="caddress" value="<?php echo $boAddress;?>"/>
						<label class="floating-lab">Billing Address</label>
					</div>
				</div>
				
				<div class="input-row">
					<div class="floatingBox width33"><input class="width33 cancelBtnEnble" type="text" placeholder="" name="ccity" id="ccity" value="<?php echo $ccity;?>"/><label class="floating-lab">City</label></div>
					<div class="floatingBox width33"><input class="width33 cancelBtnEnble" type="text" placeholder="" name="cstate" id="cstate" value="<?php echo $cstate;?>"/><label class="floating-lab">State</label></div>
					<div class="floatingBox width33" style="margin-right:0px;"><input class="width33 cancelBtnEnble" style="margin-right:0px;" type="text" placeholder="" name="czip" id="czip" value="<?php echo $czip;?>"/><label class="floating-lab">Zip</label></div>
                </div>
				
				<div class="input-row">
					<input class="yellow-btn" value="Customer History" type="button" onclick="redirectToPage('<?php echo base64_encode($ritems['boEmail']);?>','<?php echo $ritems['id'];?>')">
				</div>
			</div>
			<div class="booking-right"><button class="<?php if($ritems["status"] == 1 || $ritems["status"] == 2 || $ritems["status"] == 5){ echo "dsbl-btn";} else { echo "yellow-btn rschedule"; }?>" type="button">Reschedule</button></div>
		</div>
		
		<div class="ticketpackage">
			<h2 class="section-heading">Ticket Package</h2>
			<?php
			if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") {    
				$params = array('');
				$result2 = $db->rawQuery("SELECT * FROM bb_booktcktinfo WHERE bookingId='".$_REQUEST["id"]."' ", $params);
				$ritems2 = (array)$result2;
				if(!empty($ritems2)) {
					foreach($ritems2 as $key=>$val)
					{
				?>
				<div class="tk-section">
					<div class="input-row">
						<div class="floatingBox width78"><input class="width78 tempname" type="text" placeholder="" name="boTicketName" value="<?php echo fnCheckEmpty($val['boTicketName']);?>"/><label class="floating-lab">Ticket Name</label></div>
						<div class="floatingBox width20 mrgright0"><input class="width20 mrgright0 tempname" type="text" placeholder="" name="boTicketPrice" value="<?php echo fnCheckEmpty($val['boTicketPrice']);?>" /><label class="floating-lab">Price</label></div>
						<input type="hidden" name="boTicketId" id="boTicketId" value="<?php echo fnCheckEmpty($val['id']);?>"/>
					</div>
					
					<div class="tk-inner" id="<?php echo $key;?>">
						<div class="input-row">
							<!--<div class="loading" style="display:none;">Loading&#8230;</div>-->
							<div class="floatingBox"><input class="width57 tempname TPPN1 flref" type="text" placeholder="" name="boTicketProjectName" value="<?php echo fnCheckEmpty($val['boTicketProjectName']);?>" customProjectId="<?php echo fnCheckEmpty($val['boProjectId']);?>"/><label class="floating-lab">Project Name</label>
							<select class="width57 tempname TPPN" style="display:none;"></select>
							<input class="width20 tempname" type="text" placeholder="SKU" name="boTicketSku" id="boTicketSkuMain" value="<?php echo fnCheckEmpty($val['boTicketSku']);?>" style="display:none;"/>
							
							<input type="hidden" name="boTicketProjectId[<?php echo $val['id'];?>]" id="boTicketProjectId" value="<?php echo fnCheckEmpty($val['boProjectId']);?>"/>
							<input type="hidden" name="boTicketProjectNameTxt[<?php echo $val['id'];?>]" id="boTicketProjectNameTxt" value="<?php echo fnCheckEmpty($val['boTicketProjectName']);?>"/>
							<input type="hidden" name="boTicketSkuTxt[<?php echo $val['id'];?>]" id="boTicketSkuTxt" value="<?php echo fnCheckEmpty($val['boTicketSku']);?>"/>
							
							<input type="hidden" name="boTicketProjectSize[<?php echo $val['id'];?>]" id="boTicketProjectSize" value="<?php echo fnCheckEmpty($val['boTicketProjectSize']);?>"/>
							<input type="hidden" name="boTicketProjectImage[<?php echo $val['id'];?>]" id="boTicketProjectImage" value="<?php echo fnCheckEmpty($val['boTicketProjectImage']);?>"/>
							
							<button class="<?php if($ritems["status"] == 1 || $ritems["status"] == 2 || $ritems["status"] == 5){ echo "dsbl-btn";} else { echo "yellow-btn  tp_action"; }?> " type="button" customBookingTicketId="<?php echo $val["id"];?>">Change Project</button><label class="floating-lab">Project Name</label></div>
						
						</div>						
						<?php
						//Get ticket personalize info	
						$result3 = $db->rawQuery("SELECT * FROM bb_booktcktpersinfo WHERE bookingId='".$_REQUEST["id"]."' and bookingTicketId='".$val["id"]."' ", $params);
						$ritems3 = (array)$result3;
						if(!empty($ritems3)) {
							foreach($ritems3 as $key1=>$val1){
								//Get project id from 'bb_booktcktinfo' tbl
								$boPersOptName = strtolower($val1['boPersOptName']);
								?>	
								<!--<div class="loading" style="display:none;">Loading&#8230;</div>-->
								<div class="input-row">
									<label class="labelwdh"><?php echo fnCheckEmpty($val1['boPersOptName']);?></label>
									<input type="hidden" name="boPersOptName[<?php echo $val['id'];?>][]" id="boPersOptName" value="<?php echo fnCheckEmpty($val1['boPersOptName']);?>"/>
									<input type="hidden" name="boPersOptType[<?php echo $val['id'];?>][]" id="boPersOptType" value="<?php echo fnCheckEmpty($val1['boPersOptType']);?>"/>
									<input type="hidden" name="boPersOptHoverTxt[<?php echo $val['id'];?>][]" id="boPersOptHoverTxt" value="<?php echo fnCheckEmpty($val1['boPersOptHoverTxt']);?>"/>
									<input type="hidden" name="boPersOptTypeFormat[<?php echo $val['id'];?>][]" id="boPersOptTypeFormat" value="<?php echo fnCheckEmpty($val1['boPersOptTypeFormat']);?>"/>
									
									
									<?php
									if($val1['boPersOptType']==1) { 
										$boPersOptNameEscape = StringEscapeSlug($val1['boPersOptName']);	
										if($val1['boPersOptTypeFormat']=="All Lower Case") {
										?>
											<script>
											//$(document).on('keyup','#<?php echo str_replace(" ","_",$val1['boPersOptName']); ?>',function(){
											$(document).on('keyup','#<?php echo $boPersOptNameEscape; ?>',function(){
												this.value = this.value.toLowerCase();
											});
											</script>
										<?php 
										} 
										
										if($val1['boPersOptTypeFormat']=="All Caps") { 
											?>
											<script>
											//$(document).on('keyup','#<?php echo str_replace(" ","_",$val1['boPersOptName']); ?>',function(){
											$(document).on('keyup','#<?php echo $boPersOptNameEscape;?>',function(){
												this.value = this.value.toUpperCase();
											});
											</script>
										<?php 
										} 
										
										if($val1['boPersOptTypeFormat']== "Title Case") { 
											?>
											<script>
											//$(document).on('keyup','#<?php echo str_replace(" ","_",$val1['boPersOptName']); ?>',function(){
											$(document).on('keyup','#<?php echo $boPersOptNameEscape;?>',function(){
												this.value = toTitleCase(this.value);
											});
											</script>
										<?php 
										} ?>
										<!--<input class="width20 validate_class" type="text" name="boPersOptVal[<?php //echo $val['id'];?>][]" value="<?php //echo fnCheckEmpty($val1['boPersOptVal']);?>" id="<?php //echo str_replace(" ","_",$val1['boPersOptName']); ?>" alt="<?php //echo ucwords($val1['boPersOptHoverTxt']); ?>" title="<?php //echo ucwords($val1['boPersOptHoverTxt']); ?>"/>-->
										<!--<input class="width20 validate_class" type="text" name="boPersOptVal[<?php //echo $val['id'];?>][]" value="<?php //echo fnCheckEmpty($val1['boPersOptVal']);?>" id="<?php //echo str_replace(" ","_",$val1['boPersOptName']); ?>" alt="<?php //echo ucwords($val1['boPersOptHoverTxt']); ?>" title=""/>-->
										<input class="width20 validate_class" type="text" name="boPersOptVal[<?php echo $val['id'];?>][]" value="<?php echo fnCheckEmpty($val1['boPersOptVal']);?>" id="<?php echo $boPersOptNameEscape;?>" alt="<?php echo ucwords($val1['boPersOptHoverTxt']); ?>" title=""/>
										<?php 
									}
										
									if($val1['boPersOptType']==2) {
										$boPersOptNameEscape = StringEscapeSlug($val1['boPersOptName']);	
										if($val1['boPersOptTypeFormat']=="MM-DD-YYYY") {
											$dtval="mm-dd-yy";
										} else if($val1['boPersOptTypeFormat']=="MM-DD-YY") {
											$dtval="mm-dd-y";
										}  else if($val1['boPersOptTypeFormat']=="YYYY") {
											$dtval="yy";
										} else if($val1['boPersOptTypeFormat']=="MM-YY") {
											$dtval="mm-y";
										} else if($val1['boPersOptTypeFormat']=="YY") {
											$dtval="y";
										} else {
											$dtval=strtolower($val1['boPersOptTypeFormat']);
										}
										?>
										<!--<input class="width20 input_date validate_class" type="text" name="boPersOptVal[<?php //echo $val['id'];?>][]" value="<?php //if($val1['boPersOptVal'] != "" ) { //echo $val1['boPersOptVal']; } else { //echo "";} ?>" id="<?php //echo str_replace(" ","_",$val1['boPersOptName']); ?>" alt="<?php //echo ucwords($val1['boPersOptHoverTxt']); ?>" placeholder="<?php //echo $val1['boPersOptTypeFormat'];?>"/>-->
										<input class="width20 input_date validate_class" type="text" name="boPersOptVal[<?php echo $val['id'];?>][]" value="<?php if($val1['boPersOptVal'] != "" ) { echo $val1['boPersOptVal']; } else { echo "";} ?>" id="<?php echo $boPersOptNameEscape;?>" alt="<?php echo ucwords($val1['boPersOptHoverTxt']); ?>" placeholder="<?php echo $val1['boPersOptTypeFormat'];?>"/>
										<!--<script>
										var date = $('.input_date').datepicker({ dateFormat: '<?php //echo $dtval; ?>',showOn:'focus' }).val();
										console.log('date==='+date);
										$('#datepicker').attr("autocomplete", "off"); 
										$('.input_date').datepicker({dateFormat: '<?php //echo $dtval; ?>',showOn:'focus'}).on('change', function() {
											$(this).valid();
										});
										</script>-->
									<?php 
									}
					
									if($val1['boPersOptType']==3) {	
										$boPersOptNameEscape = StringEscapeSlug($val1['boPersOptName']);	
										if($val1['boPersOptTypeFormat']=="All Lower Case") {
											?>
											<script>
											//$(document).on('keyup','#<?php echo str_replace(" ","_",$val1['boPersOptName']); ?>',function(){
											$(document).on('keyup','#<?php echo $boPersOptNameEscape;?>',function(){
												this.value = this.value.toLowerCase();
											});
											</script>
										<?php 
										} 
										if($val1['boPersOptTypeFormat']=="All Caps") { 
											$inputVal=strtoupper($val1[str_replace(" ","_",$val1['boPersOptName'])]);
											?>
											<script>
											//$(document).on('keyup','#<?php echo str_replace(" ","_",$val1['boPersOptName']); ?>',function(){
											$(document).on('keyup','#<?php echo $boPersOptNameEscape;?>',function(){
												this.value = this.value.toUpperCase();
											});
											</script>
										<?php 
										} 
										if($val1['boPersOptTypeFormat']== "Title Case") { 
											?>
											<script>
											//$(document).on('keyup','#<?php echo str_replace(" ","_",$val1['boPersOptName']); ?>',function(){
											$(document).on('keyup','#<?php echo $boPersOptNameEscape;?>',function(){
												this.value = toTitleCase(this.value);
											});
											</script>
										<?php 
										}?>
										<!--<textarea class="validate_class" style="width:100%; margin-bottom:2%;" type="text" name="boPersOptVal[<?php //echo $val['id'];?>][]" id="<?php //echo str_replace(" ","_",$val1['boPersOptName']); ?>" alt="<?php //echo ucwords($val1['boPersOptHoverTxt']); ?>" title="<?php //echo ucwords($item['proHoverTxt']); ?>" /><?php //echo fnCheckEmpty($val1['boPersOptVal']);?></textarea>-->
										<textarea class="validate_class" style="width:100%; margin-bottom:2%;" type="text" name="boPersOptVal[<?php echo $val['id'];?>][]" id="<?php echo $boPersOptNameEscape;?>" alt="<?php echo ucwords($val1['boPersOptHoverTxt']); ?>" title="<?php echo ucwords($item['proHoverTxt']); ?>" /><?php echo fnCheckEmpty($val1['boPersOptVal']);?></textarea>
									<?php 
									} ?>
								</div>
							<?php 
							}
						}?>
						
						<!--<div class="ph2Upgrades">
							<h2 class="section-heading">Upgrades</h2>
							<div class="ph2uprow"><label><input id ="noCheck" type="checkbox"> Component Option None</label></div>
							<div class="ph2uprow">
								<ul class="ph2ckop">
									<li><label><input type="checkbox"> ($X) Component Option 1</label></li>
									<li><label><input type="checkbox"> ($X) Component Option 2</label></li>
									<li><label><input type="checkbox"> ($X) Component Option 3</label></li>
									<li><label><input type="checkbox"> ($X) Component Option 4</label></li>
									<li><label><input type="checkbox"> ($X) Component Option 1</label></li>
									<li><label><input type="checkbox"> ($X) Component Option 2</label></li>
									<li><label><input type="checkbox"> ($X) Component Option 3</label></li>
									<li><label><input type="checkbox"> ($X) Component Option 4</label></li>
								</ul>
							</div>
						</div>-->
						
						<input type="hidden" id="bookproject_<?php echo $key ?>" value="<?php echo $val['id'];?>">				
						
						<div class="ph2Upgrades" id="ph2Upgrades_replace_<?php echo $key;?>">
						<?php
						$sql = 'SELECT isPriceOpp  FROM  bb_compstudio WHERE userId="'.$_SESSION['usrid'].'"';
						$optinfo = $db->rawQueryone($sql, array(''));
						 
						
			      $sql="SELECT a1.proId,b1.id,b1.proComName,b1.proComPrice,c1.compSprice,c1.toption FROM bb_projcompused as a1 LEFT JOIN bb_component b1 ON a1.compId=b1.id LEFT JOIN bb_compstudio c1 ON b1.id=c1.compId WHERE a1.proId=".$val['boProjectId']." and b1.status=0 and b1.isDeleted=0 and b1.isArchive=0 ";
						
						
						
						$results =  $db->rawQuery($sql);
						
						$arraySelId=[];
						 
						if($ritems2[$key]['boComDesc']=='None'){
							$checkedNone='checked="checked"';
							$disabled = 'disabled="disabled"';
						}
						else {
							$disabled='';
							$checkedNone='';
							$explodeValue = explode("|",$ritems2[$key]['boComDesc']);
							 
							foreach($explodeValue as $value){
								$explodeExpression= explode("!^",$value);
								$arraySelId[]=$explodeExpression[0];
							}
						}
						
						//Get project max value
						$boComMaxDescArr = explode("!^",$ritems2[$key]['boComMaxDesc']);
						if(!empty($boComMaxDescArr)){
							$compMaxLimit = $boComMaxDescArr[2];
						}
						$response = array();	
						if(!empty($results) || !empty($arraySelId[0]) ){ ?>
							<h2 class="section-heading">Upgrades</h2>
							<div class="ph2uprow">
								<label>
									<input name="com_options_<?php echo $ritems2[$key]['id']; ?>[]" <?php echo $checkedNone; ?> id="noCheck" nocheckval="<?php echo $key; ?>" type="checkbox" > Component Option None
								</label>
							</div>
							<div class="ph2uprow">
								<ul class="ph2ckop" id="ph2ckop_<?php echo $key;?>">
									<?php 
									$count=0;
									 
									
									foreach($results as $value){
											 
										$checked='';
										$booked=0;
										if(in_array($value['id'],$arraySelId)){ 
											$checked='checked="checked"';
											$booked='1';  //1 for booked
										}

										/*echo "<pre>";
										 print_r($value);
										echo "</pre>"; 
										echo "booked is".$booked;*/
										if(isset($value['compSprice']) && ($value['compSprice']!='0.00') && !empty($value['compSprice'])){
												$compUpPrice =$value['compSprice'];	
											}
											else {
											$compUpPrice =$value['proComPrice'];	
											}
										
									
									if($value['toption']==0 && ($booked==0 || $booked==1))
										{
										?>
	<li>
		<label>
			<input <?php echo $checked; ?> type="checkbox" class="chkCompGrp_<?php echo $key;?>"  value="<?php echo $value['id'] ?>" id="com_options_<?php echo $ritems2[$key]['id'];?>_<?php echo $value['id'];?>" name="com_options_<?php echo $ritems2[$key]['id']; ?>[]" compMaxLimit="<?php echo $compMaxLimit;?>"> ($<?php echo $compUpPrice; ?>) <?php echo $value['proComName'];?>
		</label>
	</li>
									<?php 
									}
									 else if($value['toption']==1 && $booked==1)
										{
										?>
					<li>
						<label>
							<input <?php echo $checked; ?> type="checkbox" class="chkCompGrp_<?php echo $key;?>"  value="<?php echo $value['id'] ?>" id="com_options_<?php echo $ritems2[$key]['id'];?>_<?php echo $value['id'];?>" name="com_options_<?php echo $ritems2[$key]['id']; ?>[]" compMaxLimit="<?php echo $compMaxLimit;?>"> ($<?php echo $compUpPrice; ?>) <?php echo $value['proComName'];?>
						</label>
					</li>
									<?php } 



								}
									
									//get active component array
									$activeCompArr = array();
									if(!empty($results)){
										foreach($results as $value){
											$activeCompArr[] = $value['id'];
										}
									}  
									$diffArr = array_diff($activeCompArr, $arraySelId);
									$diffArr1 = array_diff($arraySelId,$activeCompArr);
									
									/*echo "<pre>activeCompArr=";print_r($activeCompArr);
									echo "<pre>diffArr=";print_r($diffArr);
									echo "<pre>diffArr1=";print_r($diffArr1);*/
									
									
									if(!empty($diffArr)){
										foreach($diffArr as $key1=>$val1){ 
											//get component price and name from booking ticket info tbl
											$compInfo = getCompInfoWRTTcktProjectInfo($val1,$ritems2[$key]['id']);
											//echo "<pre>compInfo=";print_r($compInfo);
											if($compInfo != ""){
												$compInfoArr= explode("!^",$compInfo);
												if(!empty($compInfoArr)){
													$proComName = $compInfoArr[1];
													$proComPrice = $compInfoArr[2];
													$checked1='checked="checked"';
													?>
													<li>
														<label>
															<input <?php echo $checked1; ?> <?php echo $disabled; ?>  type="checkbox" class="chkCompGrp_<?php echo $key;?>"  value="<?php echo $val1;?>" id="com_options_<?php echo $ritems2[$key]['id'];?>_<?php echo $val1;?>" name="com_options_<?php echo $ritems2[$key]['id']; ?>[]" compMaxLimit="<?php echo $compMaxLimit;?>"> ($<?php echo $proComPrice;?>) <?php echo $proComName;?>
														</label>
													</li>
												<?php
												}
											}
										}
									}
									
									if(!empty($diffArr1)){
										foreach($diffArr1 as $key2=>$val2){ 
											//get component price and name from booking ticket info tbl
											$compInfo1 = getCompInfoWRTTcktProjectInfo($val2,$ritems2[$key]['id']);
											if($compInfo1 != ""){
												$compInfo1Arr= explode("!^",$compInfo1);
												if(!empty($compInfo1Arr)){
													$proComName1 = $compInfo1Arr[1];
													$proComPrice1 = $compInfo1Arr[2];
													$checked2='checked="checked"';
													?>
													<li>
														<label>
															<input <?php echo $checked2;?> <?php echo $disabled; ?>  type="checkbox" class="chkCompGrp_<?php echo $key;?>"  value="<?php echo $val2;?>" id="com_options_<?php echo $ritems2[$key]['id'];?>_<?php echo $val2;?>" name="com_options_<?php echo $ritems2[$key]['id']; ?>[]" compMaxLimit="<?php echo $compMaxLimit;?>"> ($<?php echo $proComPrice1; ?>) <?php echo $proComName1;?>
														</label>
													</li>
												<?php
												}
											}
										} //end loop
									} //end if ?> 
								</ul>
							</div>
						<?php 
						} ?>
						</div>
						
					
					
					<style>
					.ph2Upgrades{float:left; width:100%;}
					.ph2uprow{float:left; width:100%;}
					.ph2uprow label{margin-bottom:15px; display:inline-block;}
					.ph2ckop{margin:0px; padding:0px; list-style:none;}
					.ph2ckop li{display:inline-block; width:225px;}
					</style>
					
					<script>
					$(document).ready(function(){
						$(document).on("click", "#noCheck", function(){
							var id = $(this).attr('nocheckval');
							//alert(id);
							if($(this).is(":checked")){
								$("#ph2ckop_"+id+" li").each(function(){
									//$(this).find("input[type='checkbox']").attr("disabled", true);
									$(this).find("input[type='checkbox']").prop('checked', false);
									$(this).find("input[type='checkbox']").attr('disabled', false);
								});
							}else{
								$(".ph2ckop li").each(function(){
									$(this).find("input[type='checkbox']").attr("disabled", false);
									//$(this).find("input[type='checkbox']").attr('checked', true);
								});
							}
						});
						var key = '<?php echo $key;?>'; 
						/*$(document).on("click", ".chkCompGrp_"+key, function(){ 
							var compMaxLimit = $(this).attr('compMaxLimit');
							var checkedLength = $(this).parents(".ph2ckop").find("input:checkbox.chkCompGrp_"+key+":checked").length;
							//alert('compMaxLimit='+compMaxLimit);
							//alert('checkedLength='+checkedLength);
							//alert($(this).attr('id'));
							if(checkedLength >compMaxLimit){
								var checkedId = $(this).attr('id');
								$('#'+checkedId).attr("disabled",true);
							} 
						});*/
						
						
						$(document).on("click", ".chkCompGrp_"+key, function(){
							var compMaxLimit = $(this).attr('compMaxLimit');
							
							if($(".chkCompGrp_"+key+":checked").length > 0){
								//alert("a");
								$(this).parents(".ph2uprow").siblings(".ph2uprow").find("#noCheck").prop('checked', false);
							}else{
								//alert("b=");
								$(this).parents(".ph2uprow").siblings(".ph2uprow").find("#noCheck").prop('checked', true);
							}
							
							if($(".chkCompGrp_"+key+":checked").length > compMaxLimit){
								$(this).prop("checked", false);
								$(".chkCompGrp_"+key).each(function(){
									if(!$(this).is(':checked')){
										$(this).attr("disabled", true);
									}
								});
							}else{
								$(".chkCompGrp_"+key).each(function(){
									if(!$(this).is(':checked')){
										$(this).attr("disabled", false);
									}
								});
							}
							 
							
						});
						
						
						
					});
					</script>
					
					<div class="input-row sitby">
						<label class="labelwdh">Sit By</label>
						<input class="width93 cancelBtnEnble" type="text" placeholder="Sit By" name="boTicketSitBy[<?php echo $val['id'];?>]" value="<?php echo fnCheckEmpty($val['boTicketSitBy']);?>" />
					</div>
				</div>
			<?php
				}
			}
		}
		?>
		</div>

		<div class="accommodaation">
			<?php
			$accommdatArr = getAccommodatWRTCustEmail($ritems['boEmail'],$_SESSION['stId']);
			//echo "<pre>accommdatArr==";print_R($accommdatArr);die;
			if(!empty($accommdatArr)){
				$boAccommodations = $accommdatArr['boAccommodations'];
				$boAccommodationsNotes = $accommdatArr['boAccommodationsNotes'];
			} else {
				$boAccommodations = "";
				$boAccommodationsNotes = "";
			}?>
			<h2 class="section-heading">Accommodation</h2>
			<div class="input-row">
				<div class="floatingBox">
					<!--<input class="textfull cancelBtnEnble tempname" type="text" placeholder="" name="boAccommodations" value="<?php //echo fnCheckEmpty($ritems['boAccommodations']);?>"/>-->
					<input class="textfull cancelBtnEnble tempname" type="text" placeholder="" name="boAccommodations" value="<?php echo $boAccommodations;?>"/>
				
					<label class="floating-lab">Accommodation</label>
				</div>
			</div>
			<div class="input-row">
				<label class="note">Note</label>
				<!--<textarea class="tarea90 cancelBtnEnble tempname" name="boAccommodationsNotes"><?php //echo fnCheckEmpty($ritems['boAccommodationsNotes']);?></textarea>-->
				
				<textarea class="tarea90 cancelBtnEnble tempname" name="boAccommodationsNotes"><?php echo $boAccommodationsNotes;?></textarea>
			</div>
		</div>


		<div class="transaction">
			<h2 class="section-heading">Transaction</h2>
			<div class="listing-table">
			<table width="100%" cellpadding="0" cellspacing="0">
				<thead>
				<tr>
					<th>Transaction ID</th>
					<th>Booking Date</th>
					<th>Booking ID</th>
					<th>Coupon Code</th>
					<th>Amount</th>
					<th>Gateway</th>
					<th>Status</th>
				</tr>
				</thead>
				<tbody>
				<?php
				//echo $_REQUEST["id"];
				if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") {    
					$params = array('');
					$sql789 = "SELECT transactionId,cOrderNo,id,promocode,bookingCreationDate FROM bb_booking WHERE id='".$_REQUEST["id"]."' and isDeleted=0 ";
					$result11 = $db->rawQueryOne($sql789,$params);
					//echo "<pre>result11===";print_R($result11);
					if(!empty($result11) && is_array($result11)) {
						$transactionId = $result11['transactionId'];
						$cOrderNo = $result11['cOrderNo'];
						$promocode = $result11['promocode'];
						$bookingCreationDate = $result11['bookingCreationDate'];
						
						$query243 = "SELECT * FROM bb_transactiondetails WHERE bookingId='".$result11['id']."' and isDeleted=0 and ClientDbTbl= '' ";
						$result21 = $db->rawQuery($query243,$params);
						$ritems21 = (array)$result21;
						//echo "<pre>ritems21===";print_R($ritems21);die;
						if(!empty($ritems21) && is_array($ritems21)) {
							foreach($ritems21 as $key11=>$val21){ ?>
							<tr>
								<td><?php if($val21['transactionId']!= ""){ echo $val21['transactionId'];} else { echo "NA";}?></td>
								<td>
									<?php 
									if($bookingCreationDate != ""){
										echo  date("m/d/y",strtotime($bookingCreationDate));
									} else {
										echo "NA";
									}?>
								</td>
								<td><?php echo $cOrderNo;?></td>
								<td><?php if($promocode != ""){echo $promocode;} else {echo "NA";}?></td>
								<td><?php echo "$".$val21['bookedAmount'];?></td>
								<td><?php echo getPaymentGateway($result11['id']);?></td>
								<td><?php echo getBookingStatus($result11['id']);?></td>
							</tr>
							<?php
							}
						}
					} 
					else { ?>
						<tr>
							<td colspan="5">No record found</td>
						</tr>
					<?php
					}
				}
				?>
				</tbody>
			</table>
			</div>
		</div>
	</form>
	</div>
</div>
<!-- left content area end-->
<style>
#ui-datepicker-div{z-index:1080!important;}
</style>
<script>
$(document).on("change blur", "#boEmail", function(){
	$("#customerEmail").val($(this).val());
});

$.validator.prototype.checkForm = function() {
    //overriden in a specific page
    this.prepareForm();
    for (var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
        if (this.findByName(elements[i].name).length !== undefined && this.findByName(elements[i].name).length > 1) {
            for (var cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
                this.check(this.findByName(elements[i].name)[cnt]);
            }
        } else {
            this.check(elements[i]);
        }
    }
    return this.valid();
};

//Add method for validating date picker field in personalize option
var dateType ="";
$.validator.addMethod("input_date",function(value, element) {
    var dateType = $(element).attr("placeholder"); //console.log(dateType);
    var valid = true;
    if(dateType == "MM-DD-YYYY") {
        //if (!value.match(/^(?:(0[1-9]|1[012])[\/.-](0[1-9]|[12][0-9]|3[01])[\/.-](19|20)[0-9]{2})$/)) {
        //if (!value.match(/^(?:(0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])[-](19|20)[0-9]{2})$/)) {
		if (!value.match(/^(?:(0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])[-][12][0-9]{3})$/)) {
            $.validator.messages.input_date = "Please enter date in MM-DD-YYYY format";
            valid = false;
        }
    }
    else if(dateType == "MM-DD-YY") {
        //if (!value.match(/^(?:(0[1-9]|1[012])[\/.-](0[1-9]|[12][0-9]|3[01])[\/.-][0-9]{2})$/)) {
        if (!value.match(/^(?:(0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])[-][0-9]{2})$/)) {
            $.validator.messages.input_date = "Please enter date in MM-DD-YY format";
            valid = false;
        }
    }
    else if(dateType == "YYYY") {
        //if (!value.match(/^(?:(19|20)[0-9]{2})$/)) {
		if (!value.match(/^(?:[12][0-9]{3})$/)) {
            $.validator.messages.input_date = "Please enter date in YYYY format";
            valid = false;
        }
    }
    else if(dateType == "MM-YY") {
        //if (!value.match(/^(?:(0[1-9]|1[012])[\/.-][0-9]{2})$/)) {
        if (!value.match(/^(?:(0[1-9]|1[012])[-][0-9]{2})$/)) {
            $.validator.messages.input_date = "Please enter date in MM-YY format";
            valid = false;
        }
    }
    else if(dateType == "YY") {
        if (!value.match(/^(?:[0-9]{2})$/)) {
            $.validator.messages.input_date = "Please enter date in YY format";
            valid = false;
        }
    }
    //console.log(valid);
    return valid;    
},$.validator.messages.input_date);


//Function is used to convert string into title case
function toTitleCase(str) {
    return str.replace(
        /\w\S*/g,
        function(txt) { 
			return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
}

$("#boPhone").mask("?(999) 999-9999");

var array = [];
var aid = 0;
$('#addUpdBookingFrm').find('.cancelBtnEnble').each(function(){
	$(this).attr('id', aid);
	array.push($(this).val());
	aid++;
});



$(document).on('change','.cancelBtnEnble',function(){
	var aidTg = 0;
	var getthisid;
	$('#addUpdBookingFrm').find('.cancelBtnEnble').each(function(){
		getthisid = $(this).attr('id');
		if ($(this).val() == array[getthisid]) {
			aidTg = 0;
		}else {
			aidTg = 1;
			return false;
		}
	});
	if (aidTg != 0){
		$("#btnCancel").removeClass('dsbl-btn');
	}else {
		$("#btnCancel").addClass('dsbl-btn');	
	}
});

$(document).on('click','#btnCancel',function(){ 
	if($(this).hasClass('dsbl-btn')){
		return false;
	} else {
		var pageType = '<?php echo $_REQUEST['type'];?>';
		if(pageType != ''){
			var url = '<?php echo base_url_site;?>bookinglist?type='+pageType;
		} else {
			var url = '<?php echo base_url_site;?>bookinglist';
		}
		window.location = url;
		return false;
	}
});

$(document).on('click','#btnExit',function(){
	var redirectevId = '<?php echo $_REQUEST['evId'];?>'; 
	if(redirectevId != ''){
		var url = '<?php echo base_url_site;?>single-event?evid='+redirectevId;
	} else {
		var pageType = '<?php echo $_REQUEST['type'];?>';
		if(pageType != ''){
			var url = '<?php echo base_url_site;?>bookinglist?type='+pageType;
		} else {
			var url = '<?php echo base_url_site;?>bookinglist';
		}
	}
	window.location = url;
	return false;
});

function redirectToPage(CR,bookingId) {
	var pageType = '<?php echo $_REQUEST['type'];?>';
	var url = '<?php echo base_url_site;?>customerRecord?CR='+CR+'&bookingId='+bookingId+'&type='+pageType;
	window.location = url;
	return false;
}

$(document).on('click','.actionEvent',function(){ 
	var actionEventId = $(this).attr('id');
	$("#actionType").val(actionEventId);
});
<?php 
//Edit Case
if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "") { ?>
	var bookingId = $("#bookingId").val();
<?php 
} else { // Add Case ?>
	var bookingId = "";
<?php }?>
<?php 
//Edit Case  when preview is true
if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "" ) { ?>
	$('.tempname').attr('disabled', true);
<?php 
} else { // Add Case ?>
	$('.tempname').attr('disabled', false);
<?php }?>

$.validator.addMethod("zipcode", function(value, element) {
  return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
}, "Please provide a valid zipcode.");

$.validator.addClassRules("validate_class", {required: true});
$.validator.addClassRules("TPPN", {required: true});
$.validator.addClassRules("TPPNEventDp", {required: true});

$("form[id='addUpdBookingFrm']").validate({
	rules: {
	    boFullName: { required :true },
		//boPhone : { required: true, phoneUS: true },
		boPhone : { required: true},
		boEmail : { required: true,email: true},
		caddress: { required :true },
		ccity: { required :true },
		cstate: { required :true },
		czip: { required :true ,zipcode: true },
	},
	messages: {
		boFullName: { required : "Please enter customer name" },
		//boPhone : { required: "Please enter phone name", phoneUS: "Please enter valid phone name" },
		boPhone : { required: "Please enter phone name"},
		boEmail : { required: "Please enter email address"},
		caddress: { required : "Please enter address" },
		ccity: { required : "Please enter city" },
		cstate: { required : "Please enter state" },
		czip: { required : "Please enter zip" },
	},
	submitHandler: function(form) { 
		$('.loading').show(); 
		$.ajax({
			url : '<?php echo base_url_site; ?>addon/ajaxSaveBookingData',
			type : 'POST', 
			data :$('#addUpdBookingFrm').serialize(),
			success : function(response) {
				var odata = $.parseJSON(response); 
				var pageType = odata.pageType; 
				var actionType = odata.actionType;
				var customerEmail = odata.customerEmail;
				var bookingId = odata.bookingId;
				var studioId = odata.studioId;
				//alert('actionType=='+actionType);
				//alert('customerEmail=='+customerEmail);
				//alert('bookingId=='+bookingId);
				//alert('studioId=='+studioId);
				if(actionType == 'rejectBoookingAction' || actionType == 'cancelBoookingAction' || actionType == 'unapproveBoookingAction'){
					$('.loading').hide();
					bookingRejectionEmail(customerEmail,bookingId,studioId,actionType);
					setTimeout(function(){ 
						$('.loading').hide();
						if(pageType == ''){
							window.location.href = "<?php echo base_url_site;?>bookinglist";
						} else {
							window.location.href = "<?php echo base_url_site;?>bookinglist?type="+pageType;
						}
					}, 3000);
				} 
				else if(actionType == 'approveBoookingAction'){
					$('.loading').hide();
					bookingApprovalEmail(customerEmail,bookingId,studioId,actionType);
					setTimeout(function(){ 
						$('.loading').hide();
						if(pageType == ''){
							window.location.href = "<?php echo base_url_site;?>bookinglist";
						} else {
							window.location.href = "<?php echo base_url_site;?>bookinglist?type="+pageType;
						}
					}, 3000);
				} 
				else {
					$('.loading').hide();
					if(pageType == ''){
						window.location.href = "<?php echo base_url_site;?>bookinglist";
					} else {
						window.location.href = "<?php echo base_url_site;?>bookinglist?type="+pageType;
					}
				}
			}
		});
	}
});


$(document).on('click','#resendmail',function(){ 
	$('.loading').show(); 
	var boEmail = $('#boEmail').val();
	$.ajax({
		url : '<?php echo base_url_site; ?>addon/ajaxBookingReConfirmationEmail',
		type : 'POST', 
		data :{customerEmail: boEmail, bookingId: '<?php echo $_REQUEST['id'];?>',studioId: '<?php echo $_SESSION['stId'];?>',actionType: 'resendconfirm'},
		success : function(response) {
			$('.loading').hide(); 
			var odata = $.parseJSON(response); 
			var mailstatus = odata.mailstatus;
			if(mailstatus == '1'){
				console.log('mail is successfully sent');
				$('.booking-resend-msg').css("display","block");
				$('.booking-resend-msg').html(odata.mailmsg);   
			} else {
				console.log('mail is not sent');
				$('.booking-resend-msg').css("display","block");
				$('.booking-resend-msg').html(odata.mailmsg);   
			}
		}
	});
});

function bookingRejectionEmail(customerEmail,bookingId,studioId,actionType){ 
	$.ajax({
		url : '<?php echo base_url_site; ?>addon/ajaxBookingRejectionEmail',
		type : 'POST', 
		data :{customerEmail: customerEmail, bookingId: bookingId, studioId: studioId,actionType: actionType},
		success : function(response) {
			var odata = $.parseJSON(response); 
			var mailstatus = odata.mailstatus;
			if(mailstatus == '1'){
				console.log('mail is successfully sent');
			} else {
				console.log('mail is not sent');
			}
		}
	});
}

function bookingApprovalEmail(customerEmail,bookingId,studioId,actionType){ 
	$.ajax({
		url : '<?php echo base_url_site; ?>addon/ajaxBookingReConfirmationEmail',
		type : 'POST', 
		data :{customerEmail: customerEmail, bookingId: bookingId, studioId: studioId,actionType:'approval'},
		success : function(response) {
			var odata = $.parseJSON(response); 
			var mailstatus = odata.mailstatus;
			if(mailstatus == '1'){
				console.log('mail is successfully sent');
			} else {
				console.log('mail is not sent');
			}
		}
	});
}


$(document).on('click','.tp_action',function(){
	$(this).siblings('select').attr("disabled", false);
	$(this).siblings('select').css("display", "block");
	$(this).siblings('.TPPN1').css("display", "none");
	var customProjectId = $(this).siblings('.TPPN1').attr("customProjectId");
	var elem = $(this);
	$.ajax({
        url: "<?php echo base_url_site;?>getActiveProjectList",
        type: "post",
        data : {tblName:'bb_project',fldName1:'id',fldName2:'proName',fldName3:customProjectId},
        success: function (response) {
            var jsonData = $.parseJSON(response); 
			var optionListLength  = jsonData.optionList.length;
			if(optionListLength > 0) {
			    var optionListHtml = "<option value=''>-Select Project-</option>";
				for(var k=0;k<optionListLength; k++){
				    optionListHtml += "<option value="+jsonData.optionList[k].id+">"+jsonData.optionList[k].proName+"</option>";
				}
				elem.parent().find("select").html(optionListHtml);
			    //add select2 js on change project drop down
				elem.parent().find(".TPPN").select2();
						
			}
		}
    });
});

$(document).on('change','.TPPN',function(){
	var parentDivId = $(this).parent().parent().parent().attr('id');
	//alert(parentDivId);
	var bookProjectId = $("#bookproject_"+parentDivId).val();
	//alert(bookProjectId);
	//return false;
	
	var thisElm = $(this);
	var selectedProjId = $(this).val();
	var boTicketId = $(this).parents(".tk-section").find('#boTicketId').val();//alert('boTicketId='+boTicketId);
	//Get Selected Project Info
	$.ajax({
        url: "<?php echo base_url_site;?>getSelProjectInfo",
        type: "post",
        data : {tblName:'bb_project',fldName1:'id',fldName2:'proName',fldName3:'proSku',fldName4:'proSku',fldName5:'proGalImg',fldName6:selectedProjId},
        success: function (response) {
            var jsonData = $.parseJSON(response); 
			var optionListLength  = jsonData.optionList.length;
			if(optionListLength > 0) {
				thisElm.siblings("#boTicketProjectId").val(jsonData.optionList[0].id);
				thisElm.siblings("#boTicketProjectNameTxt").val(jsonData.optionList[0].proName);
				thisElm.siblings("#boTicketSkuMain").val(jsonData.optionList[0].proSku);
				thisElm.siblings("#boTicketSkuTxt").val(jsonData.optionList[0].proSku);
				thisElm.siblings("#boTicketProjectSize").val(jsonData.optionList[0].boardSize);
				thisElm.siblings("#boTicketProjectImage").val(jsonData.optionList[0].proGalImg);
			}
		}
	});
	
	//Get Project Personalize Data
	$.ajax({
        url: "<?php echo base_url_site;?>getProjPersonalizeList",
        type: "post",
        data : {tblName:'bb_propersoptions',fldName1:'id',fldName2:'proOptionVal',fldName3:'proLabel',fldName4:'proHoverTxt',fldName5:'proOptVal',fldName6:selectedProjId},
        success: function (response) { 
			//Remove Existing Personalize Data Div
			thisElm.parent().parent().siblings(".input-row").remove(); 
            var jsonData = $.parseJSON(response);  
			var optionListLength  = jsonData.optionList.length;
			if(optionListLength > 0) {
			    var optionListHtml = "";
				for(var k=0;k<optionListLength; k++){
					var nameStr = "boPersOptVal["+boTicketId+"][]";
					
					if(jsonData.optionList[k].proOptionVal == 1 ){
						var proLabelStr1 = jsonData.optionList[k].proLabel;
						var proLabelStr = proLabelStr1.replace(/ /g, "_"); 
						if(jsonData.optionList[k].proOptVal == "All Lower Case") {
							$(document).on('keyup','#'+proLabelStr,function(){
								this.value = this.value.toLowerCase();
							});
						} 
						else if(jsonData.optionList[k].proOptVal == "All Caps") {
							$(document).on('keyup','#'+proLabelStr,function(){
								this.value = this.value.toUpperCase();
							});
						}  
						else if(jsonData.optionList[k].proOptVal== "Title Case") {
							$(document).on('keyup','#'+proLabelStr,function(){
								this.value = toTitleCase(this.value);
							});
						} 
						
						//var inputStr = "<input class='width20 validate_class' type='text' name='"+nameStr+"' value='' id='"+proLabelStr+"' alt='"+jsonData.optionList[k].proHoverTxt+"' title='"+jsonData.optionList[k].proHoverTxt+"' >";
						var inputStr = "<input class='width20 validate_class' type='text' name='"+nameStr+"' value='' id='"+proLabelStr+"' alt='"+jsonData.optionList[k].proHoverTxt+"' title='' >";
					}
					
					if(jsonData.optionList[k].proOptionVal == 2 ){
						var proLabelStr1 = jsonData.optionList[k].proLabel;
						var proLabelStr = proLabelStr1.replace(/ /g, "_");
						 
						if(jsonData.optionList[k].proOptVal =="MM-DD-YYYY") {
							var dtval = "mm-dd-yy";
						} else if(jsonData.optionList[k].proOptVal=="MM-DD-YY") {
							var dtval = "mm-dd-y";
						}  else if(jsonData.optionList[k].proOptVal=="YYYY") {
							var dtval = "yy";
						} else if(jsonData.optionList[k].proOptVal=="MM-YY") {
							var dtval = "mm-y";
						} else if(jsonData.optionList[k].proOptVal=="YY") {
							var dtval = "y";
						} 
							
						var inputStr = "<input class='width20 input_date validate_class' type='text' name='"+nameStr+"' id='"+proLabelStr+"' alt='"+jsonData.optionList[k].proHoverTxt+"' placeholder='"+jsonData.optionList[k].proOptVal+"'>";

						/*$("body").on("click", ".input_date:not(.hasDatepicker)", function(){
							$('.input_date').datepicker({dateFormat: dtval});
						});
						var dateType = jsonData.optionList[k].proOptVal;*/
					}
					
					
					if(jsonData.optionList[k].proOptionVal == 3 ){
						var proLabelStr1 = jsonData.optionList[k].proLabel;
						var proLabelStr = proLabelStr1.replace(/ /g, "_");
						if(jsonData.optionList[k].proOptVal == "All Lower Case") {
							$(document).on('keyup','#'+proLabelStr,function(){
								this.value = this.value.toLowerCase();
							});
						} 
						else if(jsonData.optionList[k].proOptVal == "All Caps") {
							$(document).on('keyup','#'+proLabelStr,function(){
								this.value = this.value.toUpperCase();
							});
						}  
						
						else if(jsonData.optionList[k].proOptVal== "Title Case") { 
							$(document).on('keyup','#'+proLabelStr,function(){ 
								this.value = toTitleCase(this.value);
							});
						} 
						//var inputStr = "<textarea class='validate_class' style='width:100%;margin-bottom:2%;' type='text' name='"+nameStr+"' id='"+proLabelStr+"' alt='"+jsonData.optionList[k].proHoverTxt+"' title='"+jsonData.optionList[k].proHoverTxt+"'></textarea>";
						var inputStr = "<textarea class='validate_class' style='width:100%;margin-bottom:2%;' type='text' name='"+nameStr+"' id='"+proLabelStr+"' alt='"+jsonData.optionList[k].proHoverTxt+"' title=''></textarea>";
					}
					
					var boPersOptNameStr = "boPersOptName["+boTicketId+"][]";
					var boPersOptTypeStr = "boPersOptType["+boTicketId+"][]";
					var boPersOptHoverTxtStr = "boPersOptHoverTxt["+boTicketId+"][]";
					var boPersOptTypeFormatStr = "boPersOptTypeFormat["+boTicketId+"][]";
					
					optionListHtml += "<div class='input-row'><input type='hidden' name='"+boPersOptNameStr+"' id='boPersOptName' value='"+jsonData.optionList[k].proLabel+"'><input type='hidden' name='"+boPersOptTypeStr+"' id='boPersOptType' value='"+jsonData.optionList[k].proOptionVal+"'><input type='hidden' name='"+boPersOptHoverTxtStr+"' id='boPersOptHoverTxt' value='"+jsonData.optionList[k].proHoverTxt+"'><input type='hidden' name='"+boPersOptTypeFormatStr+"' id='boPersOptTypeFormat' value='"+jsonData.optionList[k].proOptVal+"'><label class='labelwdh'>"+jsonData.optionList[k].proLabel+"</label>"+inputStr+"</div>";
				} // end loop
				//alert(optionListHtml);
				thisElm.parent().parent().after(optionListHtml);
				
				/*var dtval;
				thisElm.parents(".tk-section").find('.input_date').each(function(){
					if ($(this).hasClass( "input_date" )){
						if ($(this).attr("placeholder") == "MM-DD-YYYY"){
							dtval = "mm-dd-yy";
						} else if ($(this).attr("placeholder") == "MM-DD-YY"){
							dtval = "mm-dd-y";
						} else if ($(this).attr("placeholder") == "YYYY"){
							dtval = "yy";
						} else if ($(this).attr("placeholder") == "MM-YY"){
							dtval = "mm-y";
						} else if ($(this).attr("placeholder") == "YY"){
							dtval = "y";
						}
					}
					console.log('dtval='+dtval);
					$(this).datepicker({dateFormat: dtval});
					$(this).datepicker({dateFormat: dtval,showOn:'focus'}).on('change', function() {
						$(this).valid();
					});
				});*/
			}
		}
    });
	
	//Get Project copnent date from booking ticket info and siapky in check boxes Data added by pankaj
	//alert('boTicketId='+boTicketId);
	//Get Selected Project copone with price and name  Info
	$.ajax({
        url: "<?php echo base_url_site;?>getCompProjectInfo",
        type: "post",
        data : {tblName1:'bb_projcompused',tblName2:'bb_component',fldName1:'boComDesc',fldName2:'boComMaxDesc',fldName3:selectedProjId},
        success: function (response) {
			var str='';
            var jsonData = $.parseJSON(response); 
			var optionListLength  = jsonData.optionList.length;
				//alert(optionListLength);
				//return false;
			
			if(optionListLength > 0) {
				 str+='<h2 class="section-heading">Upgrades</h2><div class="ph2uprow"><label><input name="com_options_'+bookProjectId+'[]" id ="noCheck" nocheckval="'+parentDivId+'" type="checkbox" >Component Option None</label></div><div class="ph2uprow"><ul class="ph2ckop" id="ph2ckop_'+parentDivId+'">';
				for(var k=0;k<optionListLength; k++){
					//console.log("name is"+jsonData.optionList[k].id);
					str+='<li><label><input id="com_options_'+bookProjectId+'_'+jsonData.optionList[k].id+'" class="chkCompGrp_'+parentDivId+'" compmaxlimit="'+jsonData.optionList[k].compMax+'" value="'+jsonData.optionList[k].id+'" type="checkbox" name="com_options_'+bookProjectId+'[]"> ($'+jsonData.optionList[k].price+') '+jsonData.optionList[k].name+'</label></li>';
				}
				str+='</ul></div>';
			} 
			$("#ph2Upgrades_replace_"+parentDivId).html(str);
		}
	});
});

$(document).on('click','.rschedule',function(){
	$(".etname").hide();
	$("#BoEventName").show();
	var customEventId = $("#selEventId").val();
	var studioLocationId = '<?php echo $_SESSION['stId'];?>';
	var elem = $(this);
	$.ajax({
        url: "<?php echo base_url_site;?>getActiveEventList",
        type: "post",
        data : {tblName:'bb_event',fldName1:'id',fldName2:'evTitle',fldName3:customEventId,fldName4:studioLocationId},
        success: function (response) {
            var jsonData = $.parseJSON(response); 
			var optionListLength  = jsonData.optionList.length;
			if(optionListLength > 0) {
			    var optionListHtml = "<option value=''>-Select Event-</option>";
				for(var k=0;k<optionListLength; k++){
					 //optionListHtml += "<option value="+jsonData.optionList[k].id+">"+jsonData.optionList[k].evTitle+"</option>";
					optionListHtml += "<option value="+jsonData.optionList[k].id+">"+jsonData.optionList[k].evDateTimeFormated+"</option>";
				}
			} else {
				var optionListHtml = "<option value=''>-No Event Found-</option>";
			}
			$("#BoEventName").html(optionListHtml);
		}
    });
	
	
});

//Check event is already over booked or not
function checkIsEventAlreadyBooked(selectedEventId){
	$.ajax({
        url: "<?php echo base_url_site;?>checkIsEventAlreadyBooked",
        type: "post",
        data : {tblName:'bb_event',fldName1:'id',fldName2:'evTitle',fldName3:'evNoOfTickets',fldName4:'	evBookTicket',fldName5:selectedEventId},
        success: function (response) {
            var jsonData = $.parseJSON(response); 
			var optionListLength  = jsonData.optionList.length;
			if(optionListLength > 0) {
				if(jsonData.optionList[0].overBooked == 1){ //over or equal booked case
					var dlg = '<p>'+jsonData.optionList[0].evTitle+ 'is already booked. Do you want to reschedule?</p>'; 
					doConfirm(dlg, function yes() {
						//Get selected event info
						getSelEventInfo(selectedEventId);
					}, function no() {
						console.log('Select other event');
						$("#BoEventName").val('');
						return false;
					});
				} 
				else if(jsonData.optionList[0].overBooked == 0){ //less booked case
					//Get selected event info
					getSelEventInfo(selectedEventId);
				}
			}
		}
	});
}

function getSelEventInfo(selectedEventId) {
	var thisElm = $(this);
	$.ajax({
		url: "<?php echo base_url_site;?>getSelEventInfo",
		type: "post",
		data : {tblName:'bb_event',fldName1:'id',fldName2:'evTitle',fldName3:'evDate',fldName4:'evStTime',fldName5:'evEndTime',fldName6:selectedEventId},
		success: function (response) {
			$("#pageAction").val('reschedule');
			var jsonData = $.parseJSON(response); 
			//$(".tk-section").remove(); //remove ticket package section div
			var optionListLength  = jsonData.optionList.length; //alert('optionListLength=='+optionListLength);
			if(optionListLength > 0) { 
				$("#selEventId").val(jsonData.optionList[0].evId);
				$("#selEventName").val(jsonData.optionList[0].evTitle);
				$("#selEventDt").val(jsonData.optionList[0].evDate);
				$("#selEventStartTime").val(jsonData.optionList[0].evStTime);
				$("#selEventEndTime").val(jsonData.optionList[0].evEndTime);
				
				$("#selEventDtTimeInDisabledFld").val(jsonData.optionList[0].eventDtTime);
				
				var optionListHtml = "";
				
				/*for(var k=0;k<optionListLength; k++){
					var evTicketName = jsonData.optionList[k].evTicketName;
					var evTicketPrice = jsonData.optionList[k].evTicketPrice;
					var evTicketProjectIds = jsonData.optionList[k].evTicketProjectIds; 
					var fprojectId = jsonData.optionList[k].fprojectId;
					var fprojectName = jsonData.optionList[k].fprojectName;
					var fprojectSku = jsonData.optionList[k].fprojectSku;
					var fprojectGalImg = jsonData.optionList[k].fprojectGalImg;
					
					if(evTicketProjectIds != '') {
						var optionListHtml1 = "";
						var projPersInfoLength = jsonData.optionList[k].projPersInfo.length;
						//alert('projPersInfoLength=='+projPersInfoLength);
						if(projPersInfoLength > 0) {
							for(var kk=0;kk<projPersInfoLength; kk++){
								var persId = jsonData.optionList[k].projPersInfo[kk].persId;
								var proOptionVal = jsonData.optionList[k].projPersInfo[kk].proOptionVal;
								var proLabel = jsonData.optionList[k].projPersInfo[kk].proLabel;
								var proHoverTxt = jsonData.optionList[k].projPersInfo[kk].proHoverTxt;
								var proOptVal = jsonData.optionList[k].projPersInfo[kk].proOptVal;
								
								if(proOptionVal == 1 ){
									var proLabelStr1 = proLabel;
									var proLabelStr = proLabelStr1.replace(/ /g, "_"); 
									if(proOptVal == "All Lower Case") {
										$(document).on('keyup','#'+proLabelStr,function(){
											this.value = this.value.toLowerCase();
										});
									} 
									else if(proOptVal == "All Caps") {
										$(document).on('keyup','#'+proLabelStr,function(){
											this.value = this.value.toUpperCase();
										});
									}  
									else if(proOptVal == "Title Case") {
										$(document).on('keyup','#'+proLabelStr,function(){
											this.value = toTitleCase(this.value);
										});
									} 
									
									var inputStr = "<input class='width20 validate_class' type='text' name='boPersOptVal["+k+"][]' value='' id='"+proLabelStr+"' alt='"+proHoverTxt+"' title='' >";
								}
								
								if(proOptionVal == 2 ){
									var proLabelStr1 = proLabel;
									var proLabelStr = proLabelStr1.replace(/ /g, "_");
									 
									if(proOptVal =="MM-DD-YYYY") {
										var dtval = "mm-dd-yy";
									} else if(proOptVal=="MM-DD-YY") {
										var dtval = "mm-dd-y";
									}  else if(proOptVal=="YYYY") {
										var dtval = "yy";
									} else if(proOptVal=="MM-YY") {
										var dtval = "mm-y";
									} else if(proOptVal=="YY") {
										var dtval = "y";
									} 
										
									var inputStr = "<input class='width20 input_date validate_class' type='text' name='boPersOptVal["+k+"][]' value='' id='"+proLabelStr+"' alt='"+proHoverTxt+"' placeholder='"+proOptVal+"'>";
								}
								
								if(proOptionVal == 3 ){
									var proLabelStr1 = proLabel;
									var proLabelStr = proLabelStr1.replace(/ /g, "_");
									if(proOptVal == "All Lower Case") {
										$(document).on('keyup','#'+proLabelStr,function(){
											this.value = this.value.toLowerCase();
										});
									} 
									else if(proOptVal == "All Caps") {
										$(document).on('keyup','#'+proLabelStr,function(){
											this.value = this.value.toUpperCase();
										});
									}  
									
									else if(proOptVal== "Title Case") { 
										$(document).on('keyup','#'+proLabelStr,function(){ 
											this.value = toTitleCase(this.value);
										});
									} 
									var inputStr = "<textarea class='validate_class' style='width:100%;margin-bottom:2%;' type='text' name='boPersOptVal["+k+"][]' id='"+proLabelStr+"' alt='"+proHoverTxt+"' title=''></textarea>";
								}
								
								optionListHtml1 += "<div class='input-row'><input type='hidden' name='boPersOptName["+k+"][]' id='boPersOptName' value='"+proLabel+"'><input type='hidden' name='boPersOptType["+k+"][]' id='boPersOptType' value='"+proOptionVal+"'><input type='hidden' name='boPersOptHoverTxt["+k+"][]' id='boPersOptHoverTxt' value='"+proHoverTxt+"'><input type='hidden' name='boPersOptTypeFormat["+k+"][]' id='boPersOptTypeFormat' value='"+proOptVal+"'><label class='labelwdh'>"+proLabel+"</label>"+inputStr+"</div>";
							
							} //end for loop
							
							//alert(optionListHtml1);
						}
						
						optionListHtml += "<div class='tk-section'><div class='input-row'><input class='width78' type='text' placeholder='Ticket Name' value='"+evTicketName+"' disabled='disabled'><input class='width20 mrgright0' type='text' placeholder='Price' value='"+evTicketPrice+"' disabled='disabled'><input class='width78' type='hidden' placeholder='Ticket Name' name='boTicketName[]' value='"+evTicketName+"' ><input class='width20 mrgright0' type='hidden' placeholder='Price' name='boTicketPrice[]' value='"+evTicketPrice+"'></div><div class='tk-inner'><div class='input-row'><input class='width57 TPPN1 tempname' type='text' placeholder='Project Name' name='boTicketProjectName' value='"+fprojectName+"' customProjectId='"+fprojectId+"' disabled='disabled'/><select class='width57 tempname TPPN' style='display:none;'></select><input class='width20 tempname' type='text' placeholder='SKU' name='boTicketSku' value='"+fprojectSku+"' disabled='disabled'/><input type='hidden' name='boTicketProjectId[]' id='boTicketProjectId' value='"+fprojectId+"'/><input type='hidden' name='boTicketProjectNameTxt[]' id='boTicketProjectNameTxt' value='"+fprojectName+"'/><input type='hidden' name='boTicketSkuTxt[]' id='boTicketSkuTxt' value='"+fprojectSku+"'/><input type='hidden' name='boTicketProjectSize[]' id='boTicketProjectSize' value='"+fprojectGalImg+"'/><input type='hidden' name='boTicketProjectImage[]' id='boTicketProjectImage' value='"+fprojectGalImg+"'/></div>"+optionListHtml1+"</div><div class='input-row sitby'><label class='labelwdh'>Sit By</label><input class='width93' type='text' placeholder='Sit By' name='boTicketSitBy[]' value=''></div></div>";
					} 
					else {
						optionListHtml += "<div class='tk-section'><div class='input-row'><input class='width78' type='text' placeholder='Ticket Name' value='"+evTicketName+"' disabled='disabled'><input class='width20 mrgright0' type='text' placeholder='Price' value='"+evTicketPrice+"' disabled='disabled'><input class='width78' type='hidden' placeholder='Ticket Name' name='boTicketName[]' value='"+evTicketName+"' ><input class='width20 mrgright0' type='hidden' placeholder='Price' name='boTicketPrice[]' value='"+evTicketPrice+"'></div><input type='hidden' name='boTicketProjectId[]' id='boTicketProjectId' value=''/><input type='hidden' name='boTicketProjectNameTxt[]' id='boTicketProjectNameTxt' value=''/><input type='hidden' name='boTicketSkuTxt[]' id='boTicketSkuTxt' value=''/><input type='hidden' name='boTicketProjectSize[]' id='boTicketProjectSize' value=''/><input type='hidden' name='boTicketProjectImage[]' id='boTicketProjectImage' value=''/><div class='input-row sitby'><label class='labelwdh'>Sit By</label><input class='width93' type='text' placeholder='Sit By' name='boTicketSitBy[]' value=''></div></div>";
					}
				} 
				//end for loop
				
				$('.ticketpackage .section-heading').after(optionListHtml);
				
				var dtval;
				$(".tk-section").find('.input_date').each(function(){
					if ($(this).hasClass( "input_date" )){
						if ($(this).attr("placeholder") == "MM-DD-YYYY"){
							dtval = "mm-dd-yy";
						} else if ($(this).attr("placeholder") == "MM-DD-YY"){
							dtval = "mm-dd-y";
						} else if ($(this).attr("placeholder") == "YYYY"){
							dtval = "yy";
						} else if ($(this).attr("placeholder") == "MM-YY"){
							dtval = "mm-y";
						} else if ($(this).attr("placeholder") == "YY"){
							dtval = "y";
						}
					}
					$(this).datepicker({dateFormat: dtval});
					$(this).datepicker({dateFormat: dtval,showOn:'focus'}).on('change', function() {
						$(this).valid();
					});
				});*/
			}
		}
	});
}
$(document).on('change','#BoEventName',function(){
	var selectedEventId = $(this).val();  //alert(selectedEventId);
	//First check event is already over booked or not
	checkIsEventAlreadyBooked(selectedEventId);
});
</script>
<?php
include('includes/footer.php');
?>