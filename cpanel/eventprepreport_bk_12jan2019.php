<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('memory_limit', '12048M');
ob_start();
include('includes/functions.php');
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: ".base_url_site."index.php"); exit; } 
global $db;
$eventId = $_REQUEST['eventId'];

define('SITEURL', base_url_site);
//$path = $_SERVER['DOCUMENT_ROOT'].'/bnbdemo/cpanel/';
$path = $_SERVER['DOCUMENT_ROOT'].'/cpanel/';
define('SITEPATH', str_replace('\\', '/', $path));


if (!class_exists('S3'))require_once('includes/S3.php');

//Some  bullshit to prevent php from throwing notices related to the s3 class 
date_default_timezone_set('America/Los_Angeles');
// defined('CURL_SSLVERSION_TLSv1')   || define('CURL_SSLVERSION_TLSv1', 1);

// AWS access info
if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAIDJP7L7AROVO5ISQ');
if (!defined('awsSecretKey')) define('awsSecretKey', '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');

define('bucketName', 'cupload');
define('sPath', $_SERVER['DOCUMENT_ROOT'].'/bnbdemo/cpanel/');

$s3 = new S3(awsAccessKey, awsSecretKey); 


require_once SITEPATH . 'PHPExcel/Classes/PHPExcel.php';

$objPHPExcel = new PHPExcel(); 
$objPHPExcel->getProperties()
		->setCreator("user")
		->setLastModifiedBy("user")
		->setTitle("Office 2007 XLSX Test Document")
		->setSubject("Office 2007 XLSX Test Document")
		->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
		->setKeywords("office 2007 openxml php")
		->setCategory("Test result file");

// Set the active Excel worksheet to sheet 0
$objPHPExcel->setActiveSheetIndex(0); 

//Get studio city
$studioCity = getStudioCityWRTEventId($eventId);

$objPHPExcel->getActiveSheet()->mergeCells('A1:H2');
$objPHPExcel->getActiveSheet()->setCellValue('A1','Board & Brush - '.$studioCity);

// Initialise the Excel row number
$rowCount = 0; 

// Sheet cells
$cell_definition = array(
	'A' => 'Event Name',
	'B' => 'Event Date',
	'C' => 'Event Start Time',
	'D' => 'Total Seats',
	'E' => 'Total Projects'
);

$BStyle = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '000000')
		)
	)
);

$BStyle2 = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
 	)
);
	
$border_right = array(
	'borders' => array(
		'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '000000')
		)
	)
);
	
$border_left = array(
	'borders' => array(
		'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		//'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		//'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '000000')
		//)
	)
);

$border_acmd = array(
	'borders' => array(
		'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		//'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		//'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => 'FFFFFF')
		//)
	)
);
	 
$white_fill = array(
	'type' => PHPExcel_Style_Fill::FILL_SOLID,
	'startcolor' => array('rgb' => 'FFFFFF')
);
	
$border_leftright = array(
	'borders' => array(
		'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '000000')
		)
	)
);
	
$border_style = array(
	'borders' => array(
		'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => 'FFFFFF')
		)
	)
);

$border_style5 = array(
	'borders' => array(
		'top' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '000000')
		)
	)
);
	
$last_cell_border_style = array(
	'borders' => array(
		'top' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '000000')
		),
	)
);

$last_row_border = array(
	'borders' => array(
		'bottom' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '000000')
		),
	)
);
	
$boarder_right = array(
	'borders' => array(
		'RIGHT' => array(
			'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
			'color' => array('rgb' => 'FFFFFF')
		),
	)
);

$cell_border_collapsein = array(
	'borders' => array(
		'bottom' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => 'FFFFFF')
		),
	)
);

// Build headers
foreach( $cell_definition as $column => $value ) {
	$objPHPExcel->getActiveSheet()->getColumnDimension("{$column}")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue( "{$column}3", $value ); 
	
	$objPHPExcel->getActiveSheet()->getStyle("{$column}3", $value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle("{$column}4", $value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('A1:B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold( true );
	$objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setSize(20);
	$objPHPExcel->getActiveSheet()->getStyle('A3:I4')->applyFromArray($BStyle);
}
	
$objPHPExcel->getActiveSheet()->getStyle('A6:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A6:I6')->getFont()->setBold( true );
//Get event info wrt event id
$eventdetails = getEventinfoWRTEvntId($eventId);
// Build cells
while( $rowCount < 1 ){ 
	$cell=4;
	foreach( $cell_definition as $column => $value ) {
		$objPHPExcel->getActiveSheet()->getRowDimension(4)->setRowHeight(35); 
		$objPHPExcel->getActiveSheet()->setCellValue($column.$cell, $eventdetails[$value] ); 
		$objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFont()->setBold( true );
	}
	$rowCount++; 
} 
	
//Sheet cells
$cell_definition1 = array(
	'A' => 'Customer',
	'B' => 'Contact',
	'C' => 'Project Selection',
	'D' => 'Personalization',
	'E' => 'Seating Request',
	'F' => 'Supplies',
	'G' => 'Qty',
	'H' => 'Sample',
	'I' => 'Accommodation'
);

// Build headers
foreach( $cell_definition1 as $column1 => $value1 ){
	$objPHPExcel->getActiveSheet()->getColumnDimension("{$column1}")->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue( "{$column1}6", $value1 ); 
}
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);

  
$objPHPExcel->getActiveSheet()->getStyle('A1:I1000')->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('A7:I1000')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
	
//Get booking info wrt event id
$bookingdetails = getBookinginfoWRTEvntId($eventId);
//echo "<prE>bookingdetails==";print_R($bookingdetails);die;
// Build cells
$rowCount1=0;
while( $rowCount1 < count($bookingdetails) ) { 
	//echo "rowCount1==".$rowCount1;
	if($rowCount1<1) {
		$cell1= $rowCount1+7;
		//For Cell Counting
		$persInfoArr = getTicketProjectPersonalizeInfoWRTBookingId($bookingdetails[$rowCount1]['bookingIdVal'],$bookingdetails[$rowCount1]['bookingTicketId']);
		$persInfoArrCnt = count($persInfoArr);
		
		$specInfoArr = getprojectSpecInfoWRTBookingId($bookingdetails[$rowCount1]['boProjectId']);
		$specInfoArrCnt = count($specInfoArr['supplies']);
		
		//For Merging Cell
		if($persInfoArrCnt >=5 || $specInfoArrCnt>=5){ 
			if($persInfoArrCnt >= $specInfoArrCnt){
				$cellCnt = $cell1+$persInfoArrCnt;
			} else {
				$cellCnt = $cell1+$specInfoArrCnt;
			}
		} else { 
			$cellCnt = $cell1+5;
		}
		$cellCnt1 = $cellCnt-1;
		//echo "cellCnt1==".$cellCnt1;
		$firstCellLastRow = $cellCnt-2;
		
		$objPHPExcel->getActiveSheet()->mergeCells("H7:H{$cellCnt1}");
		//$objPHPExcel->getActiveSheet()->mergeCells("I7:I{$cellCnt1}");
		//new added
		$objPHPExcel->getActiveSheet()->getStyle("H7:H{$cellCnt1}")->applyFromArray($border_leftright);
		$objPHPExcel->getActiveSheet()->getStyle("F7:H{$cellCnt1}")->applyFromArray($border_right);
		$objPHPExcel->getActiveSheet()->getStyle("B7:B{$cellCnt1}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		
		$objPHPExcel->getActiveSheet()->getStyle("B12:B{$cellCnt1}")->applyFromArray($border_left);
		$objPHPExcel->getActiveSheet()->getStyle("C12:C{$cellCnt1}")->applyFromArray($border_left);
		$objPHPExcel->getActiveSheet()->getStyle("D12:D{$cellCnt1}")->applyFromArray($border_left);
		$objPHPExcel->getActiveSheet()->getStyle("E12:E{$cellCnt1}")->applyFromArray($border_left);
		
		$objPHPExcel->getActiveSheet()->getStyle("B7:B{$cellCnt1}")->getFill()->applyFromArray($white_fill);
		$objPHPExcel->getActiveSheet()->getStyle("C7:C{$cellCnt1}")->getFill()->applyFromArray($white_fill);
		$objPHPExcel->getActiveSheet()->getStyle("D7:D{$cellCnt1}")->getFill()->applyFromArray($white_fill);
		$objPHPExcel->getActiveSheet()->getStyle("E7:E{$cellCnt1}")->getFill()->applyFromArray($white_fill);
		$objPHPExcel->getActiveSheet()->getStyle("I7:I{$cellCnt1}")->getFill()->applyFromArray($white_fill);
		
		
			
		$objPHPExcel->getActiveSheet()->getStyle("B7:B{$cellCnt1}")->applyFromArray($border_left);
		$objPHPExcel->getActiveSheet()->getStyle("C7:C{$cellCnt1}")->applyFromArray($border_left);
		$objPHPExcel->getActiveSheet()->getStyle("D7:D{$cellCnt1}")->applyFromArray($border_left);
		$objPHPExcel->getActiveSheet()->getStyle("E7:E{$cellCnt1}")->applyFromArray($border_left);
		
			
		//new added
		$objPHPExcel->getActiveSheet()->getStyle("A6:I6")->applyFromArray($BStyle);
		$objPHPExcel->getActiveSheet()->getStyle("A6:I{$cellCnt1}")->applyFromArray($BStyle2);
		
		$objPHPExcel->getActiveSheet()->getStyle("B7:B{$firstCellLastRow}")->applyFromArray($cell_border_collapsein);
		
		$objPHPExcel->getActiveSheet()->getStyle("C7:C{$firstCellLastRow}")->applyFromArray($cell_border_collapsein); 
		$objPHPExcel->getActiveSheet()->getStyle("D7:D{$firstCellLastRow}")->applyFromArray($cell_border_collapsein); 
		$objPHPExcel->getActiveSheet()->getStyle("E7:E{$firstCellLastRow}")->applyFromArray($cell_border_collapsein);
		
	} 
	else {
		//For Cell Counting
		$persInfoArr = getTicketProjectPersonalizeInfoWRTBookingId($bookingdetails[$rowCount1-1]['bookingIdVal'],$bookingdetails[$rowCount1-1]['bookingTicketId']);
		$persInfoArrCnt = count($persInfoArr);
		
		$specInfoArr = getprojectSpecInfoWRTBookingId($bookingdetails[$rowCount1-1]['boProjectId']);
		$specInfoArrCnt = count($specInfoArr['supplies']);
		
		if($persInfoArrCnt <5 && $specInfoArrCnt<5){  
			$cell1= $cell1+5;
		} 
		else if($persInfoArrCnt >=5 || $specInfoArrCnt>=5){ 
			if($persInfoArrCnt == $specInfoArrCnt){
				$cell1= $cell1+$persInfoArrCnt;
			}
			if($persInfoArrCnt > $specInfoArrCnt){
				$cell1= $cell1+$persInfoArrCnt;
			}
			if($persInfoArrCnt < $specInfoArrCnt){
				$cell1= $cell1+$specInfoArrCnt;
			}
		} 
		else {
			$cell1= $cell1+5;
		}
		//For Merging Cell
		$persInfoArr1 = getTicketProjectPersonalizeInfoWRTBookingId($bookingdetails[$rowCount1]['bookingIdVal'],$bookingdetails[$rowCount1]['bookingTicketId']);
		$persInfoArrCnt1 = count($persInfoArr1);
		$specInfoArr1 = getprojectSpecInfoWRTBookingId($bookingdetails[$rowCount1]['boProjectId']);
		$specInfoArrCnt1 = count($specInfoArr1['supplies']);
		if($persInfoArrCnt1 >=5 || $specInfoArrCnt1>=5){  
			if($persInfoArrCnt1 >= $specInfoArrCnt1){
				$cellCnt = $cell1+$persInfoArrCnt1;
			} else {
				$cellCnt = $cell1+$specInfoArrCnt1;
			}
		} else { 
			$cellCnt = $cell1+5;
		}
		$cellCnt1 = $cellCnt-1;
			
		//echo $cell1." ===".$cellCnt."===".$cellCnt1."<br/>";
		$objPHPExcel->getActiveSheet()->mergeCells("H{$cell1}:H{$cellCnt1}");
		//$objPHPExcel->getActiveSheet()->mergeCells("I{$cell1}:I{$cellCnt1}");
		
		//new added
		$objPHPExcel->getActiveSheet()->getStyle("A7:A{$cellCnt1}")->applyFromArray($border_right);
		
		$objPHPExcel->getActiveSheet()->getStyle("H{$cell1}:H{$cellCnt1}")->applyFromArray($border_leftright);
		$objPHPExcel->getActiveSheet()->getStyle("F7:H{$cellCnt1}")->applyFromArray($border_right);
		$objPHPExcel->getActiveSheet()->getStyle("B12:B{$cellCnt1}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		
		$objPHPExcel->getActiveSheet()->getStyle("B12:B{$cellCnt1}")->applyFromArray($border_left);
		$objPHPExcel->getActiveSheet()->getStyle("C12:C{$cellCnt1}")->applyFromArray($border_left);
		$objPHPExcel->getActiveSheet()->getStyle("D12:D{$cellCnt1}")->applyFromArray($border_left);
		$objPHPExcel->getActiveSheet()->getStyle("E12:E{$cellCnt1}")->applyFromArray($border_left);
		
		$objPHPExcel->getActiveSheet()->getStyle("B12:B{$cellCnt1}")->getFill()->applyFromArray($white_fill);
		$objPHPExcel->getActiveSheet()->getStyle("C12:C{$cellCnt1}")->getFill()->applyFromArray($white_fill);
		$objPHPExcel->getActiveSheet()->getStyle("D12:D{$cellCnt1}")->getFill()->applyFromArray($white_fill);
		$objPHPExcel->getActiveSheet()->getStyle("E12:E{$cellCnt1}")->getFill()->applyFromArray($white_fill);
		$objPHPExcel->getActiveSheet()->getStyle("I12:I{$cellCnt1}")->getFill()->applyFromArray($white_fill);
		
		//new added
		$objPHPExcel->getActiveSheet()->getStyle("A{$cell1}:I{$cellCnt1}")->applyFromArray($BStyle2);
		$objPHPExcel->getActiveSheet()->getStyle("B{$cell1}:B{$cellCnt1}")->applyFromArray($cell_border_collapsein);
		$objPHPExcel->getActiveSheet()->getStyle("C{$cell1}:C{$cellCnt1}")->applyFromArray($cell_border_collapsein); 
		$objPHPExcel->getActiveSheet()->getStyle("D{$cell1}:D{$cellCnt1}")->applyFromArray($cell_border_collapsein); 
		$objPHPExcel->getActiveSheet()->getStyle("E{$cell1}:E{$cellCnt1}")->applyFromArray($cell_border_collapsein);
		$objPHPExcel->getActiveSheet()->getStyle("I{$cell1}:I{$cellCnt1}")->applyFromArray($cell_border_collapsein);
	}
	//echo "cell1===".$cell1."===".$cellCnt1."<br/>";
	
	//Table H Column Set Width
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20); 
	
	//Table I Column Set Width
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18); 
	
	foreach( $cell_definition1 as $column1 => $value1 ) {
		$objPHPExcel->getActiveSheet()->getRowDimension(4)->setRowHeight(35); 
		switch ($value1) {
			case 'Sample':
				if($column1 == "H"){
					// your filename
					$filePathName = "uploads/projects/700X600/".$bookingdetails[$rowCount1]['boTicketProjectImage'];
					$info = $s3->getObjectInfo(bucketName, $filePathName);
					/*echo "<pre>";print_r($info);
					die; */
					if ($info){
						$objDrawing = new PHPExcel_Worksheet_Drawing();
						$objDrawing->setName('Customer Signature');
						$objDrawing->setDescription('Customer Signature');
						
						//Path to signature .jpg file
						$signature = s3_url_uimages."uploads/projects/700X600/".$bookingdetails[$rowCount1]['boTicketProjectImage'];
						
						$objDrawing->setPath($signature, false);
						$objDrawing->setOffsetX(10);                     //setOffsetX works properly
						$objDrawing->setOffsetY(10);                     //setOffsetY works properly
						$objDrawing->setCoordinates($column1.$cell1);    //set image to cell 
						$objDrawing->setWidth(17);  
						$objDrawing->setHeight(7);                     //signature height
						
						//$objDrawing->setResizeProportional(false);
						$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());  //save 
					} 
					else {
						$objPHPExcel->getActiveSheet()->setCellValue($column1.$cell1, "Image not found" ); 
					}
				}
				break;
			
			default:
				
				if($column1 == "A"){
					$objPHPExcel->getActiveSheet()->setCellValue($column1.$cell1, $bookingdetails[$rowCount1]['boFullName'] ); 
					if($bookingdetails[$rowCount1]['seatNo'] != ''){
						$updCell12345 = $cell1;
						$updCell123456 = $updCell12345+1;
						$updCell1234567 = $updCell12345+2;
						$updCell12345678 = $updCell12345+3;
						$updCell123456789 = $updCell12345+4;
						
						$objPHPExcel->getActiveSheet()->setCellValue($column1.$updCell123456, $bookingdetails[$rowCount1]['seatNo']);
						
						if($bookingdetails[$rowCount1]['totalTickets']>1){
							$objPHPExcel->getActiveSheet()->setCellValue($column1.$updCell1234567,$bookingdetails[$rowCount1]['seatNo'].' of '.$bookingdetails[$rowCount1]['totalTickets']);
						} else {
							$objPHPExcel->getActiveSheet()->setCellValue($column1.$updCell1234567,$bookingdetails[$rowCount1]['seatNo']);
						}
						
						$objPHPExcel->getActiveSheet()->setCellValue($column1.$updCell12345678, $bookingdetails[$rowCount1]['cOrderNo']);
						
						if($bookingdetails[$rowCount1]['bookingCreationDate'] != '' ) { 
							$bookingCreationDate = date('n-j-Y',strtotime($bookingdetails[$rowCount1]['bookingCreationDate']) ) ;
						} else {
							$bookingCreationDate = '';
						}
						$objPHPExcel->getActiveSheet()->setCellValue($column1.$updCell123456789, $bookingCreationDate);
					}
				}
				if($column1 == "B"){
					$objPHPExcel->getActiveSheet()->setCellValue($column1.$cell1, $bookingdetails[$rowCount1]['boPhone'] ); 
					if($bookingdetails[$rowCount1]['boEmail'] != ''){
						$updCell123 = $cell1;
						$updCell123 = $updCell123+1;
						$objPHPExcel->getActiveSheet()->setCellValue($column1.$updCell123, $bookingdetails[$rowCount1]['boEmail']);
					}
				}
				if($column1 == "C"){
					$objPHPExcel->getActiveSheet()->setCellValue($column1.$cell1, $bookingdetails[$rowCount1]['boTicketProjectName'] ); 
				}
				if($column1 == "D"){
					$persInfoArr = getTicketProjectPersonalizeInfoWRTBookingId($bookingdetails[$rowCount1]['bookingIdVal'],$bookingdetails[$rowCount1]['bookingTicketId']);
					$persInfoArrCnt = count($persInfoArr);
					if($persInfoArrCnt >=1){
						for($k=0;$k<=$persInfoArrCnt;$k++){
							if($k==0){
								$updCell1 = $cell1;
							} else{
								$updCell1 = $updCell1+1;
							} 
							$objPHPExcel->getActiveSheet()->setCellValue($column1.$updCell1, $persInfoArr[$k] );
						}
					}
				}
				if($column1 == "E"){
					$objPHPExcel->getActiveSheet()->setCellValue($column1.$cell1, $bookingdetails[$rowCount1]['boTicketSitBy'] ); 
				}
				if($column1 == "F"){
					$specInfoArr = getprojectSpecInfoWRTBookingId($bookingdetails[$rowCount1]['boProjectId']);
					$specInfoArrCnt = count($specInfoArr['supplies']);
					if($specInfoArrCnt >=1){
						for($j=0;$j<=$specInfoArrCnt;$j++){
							if($j==0){
								$updCell = $cell1;
							} else{
								$updCell = $updCell+1;
							} 
							$objPHPExcel->getActiveSheet()->setCellValue($column1.$updCell, $specInfoArr['supplies'][$j] );
						}
					}
				}
				if($column1 == "G"){
					$specInfoArr = getprojectSpecInfoWRTBookingId($bookingdetails[$rowCount1]['boProjectId']);
					$specInfoArrCnt = count($specInfoArr['supplies']);
					if($specInfoArrCnt >=1){
						for($j=0;$j<=$specInfoArrCnt;$j++){
							if($j==0){
								$updCell = $cell1;
							} else{
								$updCell = $updCell+1;
							} 
							$objPHPExcel->getActiveSheet()->setCellValue($column1.$updCell, $specInfoArr['qty'][$j] );
						}
					}
				}
				
				if($column1 == "I"){
					$objPHPExcel->getActiveSheet()->setCellValue($column1.$cell1, $bookingdetails[$rowCount1]['boAccommodationsNotes'] ); 
				}
			break;
		}
	}
	//Table Last Row Styling
	if($rowCount1 == (count($bookingdetails)-1)){
		$tableLastCellRow = $cellCnt-1;
		$objPHPExcel->getActiveSheet()->getStyle("A{$tableLastCellRow}:I{$tableLastCellRow}")->applyFromArray($last_row_border);
		$objPHPExcel->getActiveSheet()->getStyle("A8:A{$tableLastCellRow}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
	}
	
	//Table each section First Row Border Styling
	if($rowCount1 >0){
		$objPHPExcel->getActiveSheet()->getStyle("A{$cell1}:I{$cell1}")->applyFromArray($last_cell_border_style);
	} 
	$rowCount1++; 
} //die;
	
$woodSummryRowNo = $tableLastCellRow+2;
//echo "<prE>woodSummryRowNo==";print_R($woodSummryRowNo); die;
$woodSummaryTbl1 = getWoodPlanSummaryWRTEventId($eventId);
//echo "<prE>woodSummaryTbl1==";print_R($woodSummaryTbl1); 

$alpArr = array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F',6=>'G',7=>'H',8=>'I',9=>'J',10=>'K',11=>'L',12=>'M',13=>'N',14=>'O',15=>'P',16=>'Q',17=>'R',18=>'S',19=>'T',20=>'U',21=>'V',22=>'W',23=>'X',24=>'Y',25=>'Z');
$woodSummaryTbl = array();
foreach($woodSummaryTbl1 as $keyW=>$valW){
	$woodSummaryTbl[$keyW]['event']= $valW['event'];
	$woodSummaryTbl[$keyW]['budget']= $valW['budget'];
	$woodSummaryTbl[$keyW]['ColWood']= $alpArr[$keyW];
}
//echo "<prE>woodSummaryTbl==";print_R($woodSummaryTbl); 

// Build headers
if(isset($woodSummaryTbl) && !empty($woodSummaryTbl) ) {
	//WoordsummaryRow Label Upto Value
	$woodSummaryTblRowUpto = count($woodSummaryTbl)-1;
	$woodSummaryTblRowUptoVal = $alpArr[$woodSummaryTblRowUpto];
	$objPHPExcel->getActiveSheet()->mergeCells("A{$woodSummryRowNo}:B{$woodSummryRowNo}");
	$objPHPExcel->getActiveSheet()->setCellValue("A{$woodSummryRowNo}","Wood Planning Summary :");
	$objPHPExcel->getActiveSheet()->getStyle("A{$woodSummryRowNo}:{$woodSummaryTblRowUptoVal}{$woodSummryRowNo}")->getFont()->setBold( true );
	
	foreach( $woodSummaryTbl as $key5 => $val5 ){
		$newWoordRow = $woodSummryRowNo+1;
		$ColWood = $val5['ColWood'];
		$objPHPExcel->getActiveSheet()->setCellValue( "{$ColWood}{$newWoordRow}", $val5['event'] ); 
		$objPHPExcel->getActiveSheet()->getStyle("{$ColWood}{$newWoordRow}:{$woodSummaryTblRowUptoVal}{$newWoordRow}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle("{$ColWood}{$newWoordRow}:{$woodSummaryTblRowUptoVal}{$newWoordRow}")->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle("{$ColWood}{$newWoordRow}:{$woodSummaryTblRowUptoVal}{$newWoordRow}")->applyFromArray($BStyle);
	}
	foreach( $woodSummaryTbl as $key5 => $val5 ) {
		$newWoordRow1 = $woodSummryRowNo+2;
		$ColWood = $val5['ColWood'];
		$objPHPExcel->getActiveSheet()->setCellValue( "{$ColWood}{$newWoordRow1}", $val5['budget'] ); 
		$objPHPExcel->getActiveSheet()->getStyle("{$ColWood}{$newWoordRow1}:{$woodSummaryTblRowUptoVal}{$newWoordRow1}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle("{$ColWood}{$newWoordRow1}:{$woodSummaryTblRowUptoVal}{$newWoordRow1}")->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle("{$ColWood}{$newWoordRow1}:{$woodSummaryTblRowUptoVal}{$newWoordRow1}")->applyFromArray($BStyle);
	}
}
	
/*$rand = rand(1234, 9898);
$presentDate = date('YmdHis');
.xls: application/vnd.ms-excel
.xlsx: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet*/
/*$fileName = "report_" . $rand . "_" . $presentDate . ".xlsx";
$fileName = "report_" . $rand . "_" . $presentDate . ".xls";*/

//Get Event date
$eventdetails1 = getEventDetailWRTEventId($eventId);
$eventDate1 = $eventdetails1['evDate'];
//echo "<pre>eventdetail1==";print_R($eventdetails1['evDate']);die;
$fileName = "Prepsheet_" .$eventDate1.".xls";    //Prepsheet_2018-07-16 6_30-16

header('Content-Type: application/vnd.ms-excel');
//header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); //Excel2007

/*ob_clean();
$objWriter->save('php://output');
exit;*/

$saveExcelToLocalFile = saveExcelToLocalFile($objWriter,$eventId);
$response = array(
	 'success' => true,
	 'filename' => $saveExcelToLocalFile['filename'],
	 'url' => $saveExcelToLocalFile['filePath']
);
echo json_encode($response);
die();

function saveExcelToLocalFile($objWriter,$eventId) {

	$rand = rand(1234, 9898);
	$presentDate = date('YmdHis');
	//$fileName = "report_" . $rand . "_" . $presentDate . ".xls";
	//Get Event date
	$eventdetails1 = getEventDetailWRTEventId($eventId);
	$eventDate1 = $eventdetails1['evDate'];
	$fileName = "Prepsheet_" .$eventDate1.".xls";    //Prepsheet_2018-07-16 6_30-16 
	
    // make sure you have permission to write to directory
    $filePath = SITEPATH . 'reports/' . $fileName;
    $objWriter->save($filePath);
    $data = array(
    	'filename' => $fileName,
    	'filePath' => $filePath
	);
    return $data;
}
?>