<?php
include("includes/functions.php");
global $db;
$tbl = $_POST["tblName"];
$col1 = $_POST["fldName1"];
$col2 = $_POST["fldName2"];
$col3 = $_POST["fldName3"];
$col4 = $_POST["fldName4"];

$fldName5 = $_POST["fldName5"];
$fldName6 = $_POST["fldName6"];
$fldName7 = $_POST["fldName7"];

$evEndTimeSt = $_POST["evEndTimeSt"];
$type = $_POST["type"];

$sql = "SELECT $col1,$col2,$col3,$col4,evEndTime, 
	cast(concat(`evDate`,' ',`evStTime`) as datetime) as sortdate,
	TIMESTAMP(`evDate`, STR_TO_DATE(`evStTime`, '%h:%i %p')) as  sortTime
	FROM $tbl 
	WHERE evStatus=0 and isDeleted=0 and  evLocationId IN(".$fldName5.")
	and TIMESTAMP(`evDate`, STR_TO_DATE(`evStTime`, '%h:%i %p')) >= NOW()
	order by sortTime asc";
$results =  $db->rawQuery($sql);
//echo "<pre>results=";print_r($results);die;
if(count($results)>0){
	foreach($results as $key=>$val){
		$day = date("D",strtotime($val['evDate']));
		$evStTimeFormated = $val['evStTime'];
		if(!empty($evStTimeFormated)){
			$evStTimeFormatedArr = explode(" ",$evStTimeFormated);
			$evStTime1Arr = explode(":",$evStTimeFormatedArr[0]);
			
			if($evStTime1Arr[1] == "00" || $evStTime1Arr[1] == "0"){
				$evStTime1Str = $evStTime1Arr[0];
				$evStTime1StrForAMPM =  str_replace("m","",$evStTimeFormatedArr[1]);
				$eventFGinalFormatedStr = $evStTime1Str.$evStTime1StrForAMPM."&nbsp;&nbsp;&nbsp;";
			} else{
				$evStTime1Str = $evStTimeFormatedArr[0];
				$evStTime1StrForAMPM =  str_replace("m","",$evStTimeFormatedArr[1]);
				$eventFGinalFormatedStr = $evStTime1Str.$evStTime1StrForAMPM;
			}
		} 
		else {
			$eventFGinalFormatedStr = "";
		}
		$evDateFormated = date("m/d/Y",strtotime($val['evDate']));
		$results[$key]['evDateTimeFormated'] =  $day."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$eventFGinalFormatedStr."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$evDateFormated."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$val['evTitle'];
	}
	//echo "<pre>results==";print_r($results);die;
	$response["optionList"] = $results;
} else {
	$response["optionList"] = array();
}
echo json_encode($response);
die;
?>