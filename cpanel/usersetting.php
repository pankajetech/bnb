<?php include('includes/header.php');
$menuClssFinePrint = "downarrow";
$menuSbClssPr = "sub";
$menuSbClssStylePr = "style='display:block;'";

if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 

if(isset($_SESSION["usrid"]) and $_SESSION["usrid"]!="") {    
//$numOrders = $result['Count'];
$params = array('');
$whs="";
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"]!="") {
$whs="   id='".$_SESSION["loggedin"]."'";
//$whs="  CONCAT(',', `studioLocation`, ',') REGEXP ',(".$_SESSION["stId"]."),'";
} else {
$whs="   id='".$_SESSION["usrid"]."'";
}

/*echo "SELECT * FROM bb_users WHERE id='".$_SESSION["usrid"]."' $whs ";
die();*/


$result = $db->rawQueryOne("SELECT * FROM bb_users WHERE  $whs ", $params);
$ritems = (array)$result;

if(!empty($ritems)) $ritems=$ritems; 
} else {
$ritems=array();
$ritems=$ritems;
}

	 ?>

<!-- Header end-->


<!-- left nagivation start-->

<?php include('includes/left-sidebar.php');?>

<!-- left nagivation end-->

<!-- left content area start-->

<div class="right-wrapper">
<h1 class="pageheading"><?php echo getNamebyPara("bb_studiolocation","stName","id",$_SESSION["stidloc"]);?></h1>
<div class="form-area">
 <form role="form" method="post" action="" enctype="multipart/form-data" name="frmaddupUser" id="frmaddupUser">
<input type="hidden" name="id" id="id" value="<?php echo @fnCheckEmpty($ritems['id']); ?>" />
<?php

 if(isset($_SESSION['msg']) && $_SESSION['msg']!="") {?>
<script>
setInterval(function(){ $('.event-msg').hide(); }, 3000);
</script>
<div class="event-msg" style="display:block;"><?php echo $_SESSION['msg']?></div>
<?php }?>
<div class="preferences-module">
<div class="preferences-box">
<div class="preferences-center">
<div class="preferences-row">
<div class="floatingBox">
<input id="txtFirtName" name="txtFirtName" class="remspace" placeholder="" value="<?php echo @fnCheckEmpty($ritems['firstName']); ?>" type="text">
<label class="floating-lab">First Name</label>
</div>
</div>
<div class="preferences-row">
<div class="floatingBox">
<input id="txtLastName" name="txtLastName" class="remspace" placeholder="" value="<?php echo @fnCheckEmpty($ritems['lastName']); ?>" type="text">
<label class="floating-lab">Last Name</label>
</div>
</div>
<div class="preferences-row">
<div class="floatingBox">
<input id="txtEmail" class="remspace" name="txtEmail" placeholder="" readonly="readonly" value="<?php echo @fnCheckEmpty($ritems['email']); ?>" <?php if(isset($_REQUEST["id"])) echo 'readonly="readonly"';?> type="text">
<label class="floating-lab">Email</label>
</div>
</div>
<div class="preferences-row">
<div class="floatingBox">
<?php if(isset($_SESSION["usrid"]) && $_SESSION["usrid"]!="") { ?>
<input class="remspace" type="password" id="txtRstPwd" name="txtRstPwd" placeholder="" value="" />
<?php }?>
<label class="floating-lab">Reset Password</label>
</div>
</div>
<div class="preferences-row"><input style="padding-left:30px; padding-right:30px;" class="yellow-btn" id="btnSave" name="btnSave" type="submit" value="Change Password" /></div>
</div>
</div>
</div>
</form>
</div>

</div>

<!-- left content area end-->
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>bootstrap-3.3.5.min.css">
    <script type="text/javascript" src="<?php echo base_url_js?>bootstrap-3.3.5.min.js" /></script>
    <script type="text/javascript" src="<?php echo base_url_js?>jquery-validate.bootstrap-tooltip.js" /></script>
    <script>
$(document).on("blur",".remspace",function() {
var str = $(this).val();
var str = $.trim(str);
var newStr = str.replace(/\s+/g,' ').trim();
$(this).val(newStr);
});
	
	
	$(document).ready(function() {
		$.validator.addMethod("customemail", 
		function(value, element) {
			return /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
		}, 
		"Sorry, Ive enabled very strict email validation"
	);	 
	$("#frmaddupUser").validate({
			ignore: [],
			debug: false,
			rules: {
			txtFirtName: "required",
			txtLastName:"required",
			txtRstPwd: {
							 minlength : 5
                        },

			},			
			messages: {
			txtFirtName : "Please enter first name",
			txtLastName : "Please enter last name",
			},
		submitHandler: function(form) {
		jQuery('.loading').show(); 
			var dataf = new FormData();
			var form_data = $('#frmaddupUser').serializeArray();
           // alert (form_data); 
			
			$.each(form_data, function() {
			dataf.append(this.name, this.value);
			});

			

			
			jQuery.ajax({
			url : '<?php echo base_url_site; ?>addon/ajaxSaveUser', 
			type : 'POST',              // Assuming creation of an entity
			mimeType: "multipart/form-data",
			contentType : false,        // To force multipart/form-data
			data : dataf,
			processData : false,
			success : function(response) {

						var data = JSON.parse(response);
						console.log(data);
						///$('.event-msg').html("");
						if(data.result==1){
						jQuery('.loading').hide(); 
						
						window.location.href="<?php echo base_url_site?>usersetting";
						
						
						}
			
			
			}
    		});

			
		
			
				
		}
		});	 
	 
 }); 
	</script>
<?php
include('includes/footer.php');
?>
