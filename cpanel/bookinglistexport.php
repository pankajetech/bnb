<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ob_start();
include('includes/functions.php');
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: ".base_url_site."index.php"); exit; } 
global $db;
$eventId = $_REQUEST['stdLocId'];
$bookingType = $_REQUEST['bookingType'];

define('SITEURL', base_url_site);
$path = $_SERVER['DOCUMENT_ROOT'].'/cpanel/';
define('SITEPATH', str_replace('\\', '/', $path));
ini_set('max_execution_time', -1);
ini_set('memory_limit', '35000M');
require_once SITEPATH . 'PHPExcel/Classes/PHPExcel.php';
	
$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
$cacheSettings = array( 'memoryCacheSize' => '8000MB');
PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

$objPHPExcel = new PHPExcel(); 
$objPHPExcel->getProperties()
		->setCreator("user")
		->setLastModifiedBy("user")
		->setTitle("Office 2007 XLSX Test Document")
		->setSubject("Office 2007 XLSX Test Document")
		->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
		->setKeywords("office 2007 openxml php")
		->setCategory("Test result file");

// Set the active Excel worksheet to sheet 0
$objPHPExcel->setActiveSheetIndex(0); 
	
//Initialise the Excel row number
$rowCount = 0; 

// Sheet cells
$cell_definition = array(
	'A' => 'Status',
	'B' => 'BookingID',
	'C' => 'BookingDate',
	'D' => 'CustomerName',
	'E' => 'CustomerEmail',
	'F' => 'CustomerPhone',
	'G' => 'Seats',
	'H' => 'Project',
	'I' => 'Personalizations',
	'J' => 'PromoCode',
	'K' => 'Total',
	'L' => 'Gateway',
	'M' => 'Address',
	'N' => 'City',
	'O' => 'State',
	'P' => 'ZipCode',
	'Q' => 'EventDate',
);
//Build headers
foreach( $cell_definition as $column => $value ) {
	$objPHPExcel->getActiveSheet()->setCellValue( "{$column}1", $value ); 
}
//Get Booking info wrt event location id
$eventdetails = getBookinginfoWRTBookingId($eventId,$bookingType);
//echo "<pre>eventdetails==";print_R($eventdetails);die;
$rowCount =0;
//Build cells
while( $rowCount < count($eventdetails) ) {
	$cell=$rowCount+2;
	foreach( $cell_definition as $column => $value ) {
		$objPHPExcel->getActiveSheet()->setCellValue($column.$cell, $eventdetails[$rowCount][$value] ); 
	}
	$rowCount++; 
} 

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); //Excel2007
ob_clean();
/*ob_clean();
$objWriter->save('php://output');
exit;*/
$saveExcelToLocalFile = saveExcelToLocalFile($objWriter);
$response = array(
	 'success' => true,
	 'filename' => $saveExcelToLocalFile['filename'],
	 'url' => $saveExcelToLocalFile['filePath']
);
echo json_encode($response);
die();

function saveExcelToLocalFile($objWriter) {
	$rand = rand(1234, 9898);
	$presentDate = date('Y-m-d H:i:s');
	//$fileName = "report_" . $rand . "_" . $presentDate . ".xls";
	$fileName = "Booking_".$presentDate.".xls";
    //make sure you have permission to write to directory
    $filePath = SITEPATH .'reports/'.$fileName;
    $objWriter->save($filePath);
	$data = array(
    	'filename' => $fileName,
    	'filePath' => $filePath
	);
    return $data;
}
?>