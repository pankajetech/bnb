<?php 
include('includes/header.php');
$menuClssSetting = "downarrow";
$menuSbClssSetting = "sub";
$menuSbClssStyleSetting = "style='display:block;'";
$menuClssSetting1 = "active";

if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 
?>
<!-- Header end-->
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<!-- ToolTip Type Validation-->
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>bootstrap-3.3.5.min.css">
<script type="text/javascript" src="<?php echo base_url_js?>bootstrap-3.3.5.min.js" /></script>
<script type="text/javascript" src="<?php echo base_url_js?>jquery-validate.bootstrap-tooltip.min.js" /></script>


<!-- left nagivation end-->
<!-- left content area start-->
<div class="right-wrapper">
	<h1 class="pageheading"><?php echo fnMultiStudioTitle($_SESSION["stId"]);?><?php echo $heading = "Project Fine Print";?></h1>
	<style>
	.fineprint-msg{color:#6fcf00; font-weight:bold; text-align:center;margin-bottom: 20px;}
	.pfp-outer{float:left; width:100%;}
	.pfp-outer ul{float:left; width:100%; margin:0; padding:0; list-style:none;}
	.pfp-heading{float:left; width:100%; margin-bottom: 15px; background-color: #434343; font-size: 16px; padding: 8px 5px; text-align: left; color: #fff;}
	.pfp-heading label{width:15%; padding: 6px 0px; float:left; margin-right:1.5%;}
	.pfp-outer ul li{float:left; width:100%; margin-bottom:15px;}
	.pfp-heading{float:left; width:100%;}
	.width-70{float:left; width:70%;}
	.width-70 input{width:100%;}
	.pfp-save{background-color: #6fcf00; float:right; font-size: 16px; color: #fff; border: none; border-radius: 5px; padding: 8px 17px;}
	.pfp-save:hover { background-color: #62b700;}
	.pfp-save:active { transform: translateY(2px);}
	</style>
	<?php
	if(isset($_SESSION['msg']) && $_SESSION['msg']!="") {?>
		<script>
		setInterval(function(){ $('.fineprint-msg').hide(); }, 3000);
		</script>
		<div class="fineprint-msg" style="display:block;"><?php echo $_SESSION['msg'];?></div>
	<?php }?>
	<div class="form-area">
		<div class="event-listing-module">
			<div class="elm-control">
				<div class="pfp-outer">
					<form method="post" action="" role="form" id="ProjectFinePrintForm">
						<div class="pfp-heading"><label><b>Label</b></label><label><b>Name</b></label>
						<button type="submit" class="pfp-save" id="bbb">Save</button></div>
						<ul>
						<?php
						$params = array('');
						$sql = "SELECT * FROM `bb_profineprintoptions` where status=0 order by id asc";
						$proFinePrints = $db->rawQuery($sql,$params);
						if(isset($proFinePrints) && !empty($proFinePrints)) {
							foreach ($proFinePrints as $key=>$item) { 
							?>
							<li>
								<div class="ticket-close-dropdown">
									<a href='javascript:void(0);'><i class='fa fa-times-circle removeDbVal' style='font-size:19px;color:red'></i></a>
									<input readonly name="ProjectFormLabelDataDb[<?php echo $item['id'];?>]" type="text" customDbItem="<?php echo $item['id'];?>" value="Fine Print <?php if(isset($item['id']) && $item['id']!=""){ echo $key+1; } else { echo "NA";}?>" class="template_pe_dropdown">
								</div>
								<div class="width-70">
									<input type="text" name="ProjectFormNameDataDb[]" id="ProjectFormNameData_<?php echo $key+1;?>"
									value="<?php if(isset($item['proFinePrintName']) && $item['proFinePrintName']!=""){ echo $item['proFinePrintName']; }?>"  >
								</div>
							</li>
						<?php 
							}
						} ?>
						</ul>
						<input type="hidden" id="removeDbValue" name="removeDbValue" value="" />
						<div class="insert-row add-more-li"><a href="javascript:void(0);"><i class="fa fa-plus-circle"></i></a></div>
					</form>
					</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){ 
	var lilength = 0;
	var liInputVal;
	var liInputVal = '<?php echo count($proFinePrints);?>';
	$(document).on('click', '.add-more-li', function(){
		var liInputVal1 = $(this).siblings('ul').children('li:last-child').find('.template_pe_dropdown').val();
		var liInputValArr = liInputVal1.split("Fine Print");
		liInputVal = liInputValArr[1];
		lilength = $(this).siblings('ul').children('li').length; 
		if (lilength < 20) {
			liInputVal++;
			$(this).siblings('ul').children('li:last-child').after("<li><div class='ticket-close-dropdown'><a href='javascript:void(0);'><i class='fa fa-times-circle remove' style='font-size:19px;color:red'></i></a><input readonly name='ProjectFormLabelData[]' type='text' value='Fine Print "+liInputVal+"' class='template_pe_dropdown'></div><div class='width-70'><input id='ProjectFormNameData_"+liInputVal+"' name='ProjectFormNameData[]' type='text' value=''></div></li>");
			if (lilength == 19) {
				$(this).hide();
			}else {
				$(this).show();
			}
		}else {
			alert("No More Then 20");
		}
	});
	var totalRemove = "";
	//Remove click on values fetch from Db
	$(document).on('click', '.removeDbVal', function(){ 
		//get custom attr value 
		var liInputCustomDbVal = $(this).parent().parent().find('.template_pe_dropdown').attr('customDbItem');
		totalRemove = totalRemove + parseInt(liInputCustomDbVal) + ",";
    	$('#removeDbValue').val(totalRemove); //set value in hidden field
		
		$(this).parent().parent().parent().nextAll('li').each(function(){
			var liInputVal11 = $(this).find('.template_pe_dropdown').val();
			var liinputSplitArr = liInputVal11.split("Fine Print");
			liInputVal = liinputSplitArr[1]; 
			liInputVal--;
			$(this).find('.template_pe_dropdown').val("Fine Print"+" "+liInputVal);
		});
		console.log('db-added='+$(this).parent().parent().parent().html());
		$(this).parent().parent().parent().remove();
		$(".add-more-li").show();
	});
	
	//Remove click on newly click values
	$(document).on('click', '.remove', function(){
		$(this).parent().parent().parent().nextAll('li').each(function(){
			var liInputVal11 = $(this).find('.template_pe_dropdown').val();
			var liinputSplitArr = liInputVal11.split("Fine Print");
			liInputVal = liinputSplitArr[1]; 
			liInputVal--;
			$(this).find('.template_pe_dropdown').val("Fine Print"+" "+liInputVal);
		});
		console.log('new-added='+$(this).parent().parent().parent().html());
		$(this).parent().parent().parent().remove();
		$(".add-more-li").show();
	});
});

$.validator.prototype.checkForm = function() {
    //overriden in a specific page
    this.prepareForm();
    for (var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
        if (this.findByName(elements[i].name).length !== undefined && this.findByName(elements[i].name).length > 1) {
            for (var cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
                this.check(this.findByName(elements[i].name)[cnt]);
            }
        } else {
            this.check(elements[i]);
        }
    }
    return this.valid();
};

$.validator.addMethod("noSpace", function(value, element) { 
  //return value.indexOf(" ") < 0 && value != "";
  return $.trim(value) != "";
}, "No space please and don't leave it empty");


$("form[id='ProjectFinePrintForm']").validate({
	rules: {
		"ProjectFormNameData[]": { required :true, noSpace: true},
		"ProjectFormNameDataDb[]": { required :true, noSpace: true},
	},
	messages: {
		"ProjectFormNameData[]": { required: "Enter Fine Print Name" },
		"ProjectFormNameDataDb[]": { required :"Enter Fine Print Name"},
	},
	submitHandler: function(form) {
	    $('.loading').show();
		$('.fineprint-msg').html("");
		$.ajax({
			url : '<?php echo base_url_site; ?>addon/ajaxSaveProjectFinePrint',
			type : 'POST', 
			data :$('#ProjectFinePrintForm').serialize(),
			success : function(response) {
				$('.loading').hide(); 
				var odata = $.parseJSON(response); 
				window.location.href = "<?php echo base_url_site;?>projectFinePrintlist";
			}
		});	
	}
});
		
</script>
<!-- left content area end-->
<?php
include('includes/footer.php');
?>