<?php 
include('includes/header.php');
$menuClssProject = "downarrow";
$menuSbClssProject = "sub";
$menuSbClssStyleProject = "style='display:block;'";
$menuClssProjecttem6 = "active";
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 
?>
<!-- Header end-->
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<!-- left nagivation end-->
<!-- left content area start-->
<div class="right-wrapper">
	<h1 class="pageheading"><?php echo fnMultiStudioTitle($_SESSION["stId"]);?>Project Schedule</h1>
	<div class="form-area">
		<div class="event-listing-module">
			<div class="elm-control">
				<div class="elm-row">
					<table class="elm-table" id="example1">
						<thead>
						<tr>
							<th>Status</th>
							<th>Project Name</th>
							<th>Schedule Date</th>
							<th>Schedule Time</th>
						</tr>
						</thead>
						<tbody>
						<?php
						$params = array('');
						$sql = "SELECT * FROM `bb_project` where status=0 and isDeleted=0 and proSchDtOptional=0 and proSchStDtTime >= NOW() order by proSchStDtTime desc";
						$projects = $db->rawQuery($sql,$params);
						$projectCnt = count($projects);
						if(isset($projects) && !empty($projects)) {
							foreach ($projects as $key=>$item) {
							?>
							<tr>
								<td>
									<?php 
									/*if($item['status'] == '0'){ 
										echo "Scheduled"; 
									} else if($item['status'] == '2') { 
										echo "Draft"; 
									} else if($item['status'] == '3') { 
										echo "Archive"; 
									} else { 
										echo "InActive"; 
									}*/ 
									if($item['proSchDtOptional'] == 1){
										echo "Draft"; 
									} else {
										if(strtotime($item['proSchStDtTime'])<= strtotime("now")){
											echo "Published"; 
										} else {
											echo "Scheduled"; 
										}
									}?>
								</td>
								
								<td><?php if(isset($item['proName']) && $item['proName']!=""){ echo $item['proName']; } else { echo "NA";}?></td>
								<td><?php if(isset($item['proSchStDate']) && $item['proSchStDate']!=""){ echo date("m/d/Y",strtotime($item['proSchStDate'])); } else { echo "NA";}?></td>
								<td><?php if(isset($item['proSchStTime']) && $item['proSchStTime']!=""){ echo $item['proSchStTime'];} else { echo "NA";}?></td>
							</tr>
							<?php 
							}
						} ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<link href="<?php echo base_url_css?>dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<script src="<?php echo base_url_css?>dataTables/jquery.dataTables_project.js"></script>
<script src="<?php echo base_url_css?>dataTables/dataTables.bootstrap.js"></script>
<script>
$(document).ready(function(){ 
	vtable =  $('#example1').DataTable( {
		dom: '<"elm-row"<"bubbleInfo"<"fl spage"<"paging"pl><"clear">><"fr page-text"i>>>',
		pagingType: "full_numbers",
		sortable: false,
		paginate: true,
		pageLength: 50,
		info: true,
		bSort: true,
		bFilter: true,
		"aaSorting": [],
	} );
});
</script>
<!-- left content area end-->
<?php
include('includes/footer.php');
?>