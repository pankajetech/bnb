<?php 
include('includes/header.php');
$menuClssPromocode = "downarrow";
$menuSbClssPromocode = "sub";
$menuSbClssStylePromocode = "style='display:block;'";

if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 
?>
<!-- Header end-->
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<!-- left nagivation end-->
<!-- left content area start-->
<div class="right-wrapper">
	<?php 
	//echo "<pre>";print_r($_SESSION);
	$params = array('');
	/*$sql = "SELECT NOW(),CURDATE()";
	$crnt = $db->rawQuery($sql,$params);
	echo "<pre>crnt==";print_r($crnt);*/
	if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="archive") {
		$heading = "Archive Coupon code Listing";
	} 
	else if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="current") {
		$heading = "Current Coupon code Listing";
	}
	else if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="expired") {
		$heading = "Expired Coupon code Listing";
	}
	else {
		$heading = "Coupon code Listing";
	} ?>
    
	<h1 class="pageheading"><?php echo fnMultiStudioTitle($_SESSION["stId"]);?><?php echo $heading;?></h1>
	<script>
	var type = '<?php echo $_REQUEST["type"];?>';
	if(type == 'archive') {
		$('.archivePromocode').addClass("active");
	} else if(type == 'current') {
		$('.currentPromocode').addClass("active");
	} else if(type == 'expired') {
		$('.expirePromocode').addClass("active");
	} else if(type == '') {
		$('.allPromocode').addClass("active");
	}
	</script>
	<style>
	.promo-msg,.promo-del-msg{color:#6fcf00; font-weight:bold; text-align:center;margin-bottom: 20px;}
	.fldCls{display:none;}
	</style>
	<?php
	if(isset($_SESSION['msg']) && $_SESSION['msg']!="") {?>
		<script>
		setInterval(function(){ $('.promo-msg').hide(); }, 3000);
		</script>
		<div class="promo-msg" style="display:block;"><?php echo $_SESSION['msg'];?></div>
	<?php }?>
	<div class="promo-del-msg" style="display:none;"></div>
	<div id="download_img" style="text-align: center; position: fixed; display:none; z-index: 99999; left: 0; right: 0; top: 0; bottom: 0;" >
		<img src="<?php echo base_url_site;?>images/ajax-loader.gif"  style="position: relative;  top: 50%;"/ >
	</div>
	<div class="form-area">
		<div class="event-listing-module">
			<div class="elm-control">
				<div class="elm-row">
					<table class="elm-table" id="example">
						<thead>
						<tr>
							<?php 
							/*if( ( isset($_REQUEST["type"]) && $_REQUEST["type"] =="archive" ) || ( !isset($_REQUEST["type"]) && $_REQUEST["type"] =="" )  ) {*/ ?>
							<th>Action</th>
							<?php //} ?>
							<th>Status</th>
							<th>Name</th>
							<th>Code</th>
							<th>Description</th>
							<th>Discount</th>
							<th>Uses</th>
							<th>Creator</th>
							<th>promoSchStDtTime</th>
							<th>promoSchEndDtTime</th>
						</tr>
						</thead>
						
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<link href="<?php echo base_url_css?>dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<script src="<?php echo base_url_css?>dataTables/jquery.dataTables_project.js"></script>
<script src="<?php echo base_url_css?>dataTables/dataTables.bootstrap.js"></script>

<script>
$(document).ready(function(){ 
	vtable = $('#example').DataTable( {
		dom: '<"elm-row"<"bubbleInfo"<"fl spage"<"paging"pl><"clear">><"fr page-text"i>>>',
		"processing": true,
		"language": {
			processing: "<div style='text-align:center'><img src='<?php echo base_url_site;?>images/ajax-loader.gif'></div>"
		},
		"serverSide": true,
		"ajax":{
			url :"<?php echo base_url_site; ?>addon/promocode-grid-data", // json datasource
			data: function ( d ) {
                d.type = "<?php echo $_REQUEST["type"];?>";
            },
			type: "post",  // method  , by default get
			error: function(){  // error handling
				$(".example-error").html("");
				$("#example-grid").append('<tbody class="example-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
				$("#example_processing").css("display","none");
			}
		},
		
		pagingType: "full_numbers",
		sortable: false,
		paginate: true,
		pageLength: 50,
		info: true,
		bSort: true,
		
		<?php if($_REQUEST['type'] == ''){ ?>
			order: [[ 0, "desc" ]],
		<?php } if($_REQUEST['type'] == 'archive'){ ?>
			order: [[ 0, "desc" ]],
		<?php } if($_REQUEST['type'] == 'current'){ ?>
			order: [[ 8, "asc" ]],
		<?php } if($_REQUEST['type'] == 'expired'){ ?>
			order: [[9, "asc" ]],
		<?php } ?>
		bFilter: true,
		"aaSorting": [],
		"aoColumnDefs": [ 
			<?php if($_REQUEST['type'] == 'current' || $_REQUEST['type'] == 'expired' ){ ?>
				{ "bSortable": false, className: "fldCls", "aTargets": [ 0 ] } ,
			<?php } else {?>
				{ "bSortable": false, "aTargets": [ 0 ] } ,
			<?php }?>
			{ "bSortable": false, className: "fldCls", "targets": [ 8 ] },
			{ "bSortable": false, className: "fldCls", "targets": [ 9 ] },
		]
	} );
	
	$(document).on('click','.delPromocode',function(){  
		var promoId = $(this).attr('customAtr');
		var elm = $(this);
		var dlg ='<p>Are you sure you like to delete this Coupon?</p>';
		doConfirm(dlg, function yes() {
			$.ajax({
				url : '<?php echo base_url_site; ?>addon/ajaxDeletePromocode',
				type: 'post',
				data: {promoId:promoId},
				success: function(data) {
				    var odata = $.parseJSON(data);
					vtable.row( elm.parents('tr') ).remove().draw(false);
					$('html, body').animate({scrollTop: '0px'}, 300);//Scroll top
					$('.promo-del-msg').css("display","block");
					$('.promo-del-msg').html(odata.msg);
					setTimeout(function(){ 
						$('.promo-del-msg').css("display","none");
						$('.promo-del-msg').html(""); 
					}, 2000);
				},
				error: function() {
					alert('There has been an error.');
				}
			});
		}, function no() {
			return false;
		});
    });
});
</script>
<!-- left content area end-->
<?php
include('includes/footer.php');
?>