<?php include('includes/header.php');
$menuClssUsers = "downarrow";
$menuSbClssUsers = "sub";
$menuSbClssUsers1 = "active";
$menuSbClssStyleUsers = "style='display:block;'";

if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 
//if(isset($_SESSION['urole']) && $_SESSION['urole']!=1){ header("location: ".base_url_site."dashboard"); exit; } 
if(isset($_SESSION['urole']) && $_SESSION['urole']== 3 ){ header("location: ".base_url_site."dashboard"); exit; }

if(isset($_REQUEST["delid"]) and $_REQUEST["delid"]!="") { 
if(isset($_REQUEST["studioIds"]) and $_REQUEST["studioIds"]!="") { 
$dataD=array("isActive"=>1);
} else {
$dataD=array("isDeleted"=>1,"studioLocation"=>"");
}
$db->where('id',$_REQUEST["delid"]);
$db->update('bb_users',$dataD);

$_SESSION["msg"]="Successfully deleted user info";
header("location: users.php");
exit;
}
	 ?>

<!-- Header end-->


<!-- left nagivation start-->

<?php include('includes/left-sidebar.php');?>
<link href="<?php echo base_url_css?>dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<!-- left nagivation end-->
<!-- left nagivation end-->

<!-- left content area start-->

<div class="right-wrapper">
<h1 class="pageheading">Users</h1>
<div class="form-area">
<div class="listing-wrapper">
<div class="listing-table">
<table width="100%" cellpadding="0" cellspacing="0" id="dataTables-example">
<thead>
<tr>
<th>Action</th>
<th>Name</th>
<th>Email</th>
<th>Role</th>
<th>Is Invite</th>
<th>Status</th>

</tr>
</thead>

</table>
</div>
</div>
</div>

</div>



<!-- left content area end-->

<?php /*?> <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <?php */?>

<script src="<?php echo base_url_css?>dataTables/buttons/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url_css?>dataTables/buttons/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url_css?>dataTables/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url_css?>dataTables/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script>


$(document).ready(function() {

/*vtable = $('#dataTables-example').DataTable( {
dom: '<"elm-row"<"bubbleInfo"<"fl spage"<"paging"pl><"clear">><"fr page-text"i>>>',
pagingType: "full_numbers",
sortable: false,
paginate: true,
pageLength: 50,
info: true,
bSort: true,
bFilter: false,
"aaSorting": [],
"aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0, 5 ] } ]
} );*/


vtable = $('#dataTables-example').DataTable( {
		"dom": '<"elm-row"<"bubbleInfo"<"fl spage"<"paging"pl><"clear">><"fr page-text"i>>>rt',
		"processing": true,
		"language": {
			processing: "<div style='text-align:center'><img src='<?php echo base_url_site;?>images/ajax-loader.gif'></div>",
		},
		"serverSide": true,
		"ajax":{
			url :"<?php echo base_url_site; ?>addon/user-grid-data", // json datasource
			data: function (d) {
                d.estatus = "";
            },
			type: "post",  //method,by default get
			error: function(){  //error handling
				$(".dataTables-example-error").html("");
				$("#dataTables-example").append('<tbody class="dataTables-example-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
				$("#dataTables-example_processing").css("display","none");
			}
		},
		order: [[ 1, "desc" ]],
		pagingType: "full_numbers",
		sortable: false,
		paginate: true,
		pageLength: 50,
		info: true,
		bSort: true,
		bFilter: false,
		"aaSorting": [],
		"aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0,5 ] } ]
	} );




$(document).on("click",".del",function() {
var delid=$(this).attr('id');


/*if(studio!="") {
var dlg="<p>This user cannot be deleted until a new user is assigned to the location <strong>"+studio+"'</strong>. If you would like to revoke access for this user, set the users status to inactive. 'Create New User' or 'Cancel'.</p>";
} else {*/
var dlg='<p>Would you like to delete user info</p>';
//}


      
        doConfirm(dlg, function yes() {

						window.location.href="<?php echo base_url_site?>users?delid="+delid;

        }, function no() {
          
						//dialog.dialog('close');

        });



});
$(document).on("click",".clsLogin",function() {
var cur=$(this);
			var uid=$(this).attr('id');
			var st=$(this).attr('data-status');
			
			$.post("<?php echo base_url_site; ?>addon/updateloginstatus.php", {uid:uid,st:st}, function(response) {
			var data = JSON.parse(response);
			cur.attr("title",data.tit);
			cur.attr("alt",data.tit);
			cur.attr("data-status",data.sta);
			cur.children("img").attr("src",data.imname);
			});
});
});
</script>


<?php
include('includes/footer.php');
?>