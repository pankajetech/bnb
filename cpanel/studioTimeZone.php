<?php 
include('includes/header.php');
$menuClssSetting = "downarrow";
$menuSbClssSetting = "sub";
$menuSbClssStyleSetting = "style='display:block;'";
$menuClssTzone2 = "active";

if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 

$timezones = array(
'America/New_York' => 'Eastern Time Zone',
'America/Chicago' => 'Central Time Zone',
'America/Denver' => 'Mountain Time Zone',
'America/Phoenix' => 'Mountain Time Zone (No-DTS)',
'America/Los_Angeles' => 'Pacific Time Zone'
);
?>
<!-- Header end-->
<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<!-- ToolTip Type Validation-->
<link type="text/css" rel="stylesheet" href="<?php echo base_url_site?>css/bootstrap-3.3.5.min.css">
<script type="text/javascript" src="<?php echo base_url_site?>js/bootstrap-3.3.5.min.js" /></script>
<script type="text/javascript" src="<?php echo base_url_site?>js/jquery-validate.bootstrap-tooltip.min.js" /></script>

<style>
.dyanmicRadio input[type="checkbox"]{margin-right:15px; width:auto;}
.dynamicBox input[type="text"]{width:35%; margin-right:15px; margin-top:15px; float:none;}
.dynamicBox button{background-color: #6fcf00; font-size: 16px; color: #fff; border: none; border-radius: 5px; padding: 8px 17px;}
.dynamicBox button:focus {background-color: #62b700; transform: translateY(2px); }
.dynamicBox button:hover{background-color: #62b700;}
.studiochr-msg{color:#6fcf00; font-weight:bold; text-align:center;margin-bottom: 20px;}
</style>

<!-- left nagivation end-->
<!-- left content area start-->
<div class="right-wrapper">
	<h1 class="pageheading"><?php echo fnMultiStudioTitle($_SESSION["stId"]);?>Studio Time Zone Setting</h1>
	<?php
	if(isset($_SESSION['msg']) && $_SESSION['msg']!="") {?>
		<script>
		setInterval(function(){ $('.studiochr-msg').hide(); }, 3000);
		</script>
		<div class="studiochr-msg" style="display:block;"><?php echo $_SESSION['msg'];?></div>
	<?php }?>
	<?php // echo "<pre>";print_r($_SESSION);?>
	<?php
	
	if($_SESSION["urole"] == 3 && empty($_SESSION["curRole"])){
		$instyle="disabled='disabled'";
		$btnstyle="style='display:none;'";
	}
	
	$params = array('');
	//if($_SESSION["urole"] == 1 || $_SESSION["urole"] == 2){
		$sql = "SELECT stimeZone,id FROM `bb_studiolocation` where id=".$_SESSION["stId"];
		$studioIds=$_SESSION["stId"];
		
		
		
	/*}
	if($_SESSION["urole"] == 3){
		$sql = "SELECT * FROM `bb_timezonestudio` where studioLocId IN(".$_SESSION["studioIds"].") "; 
		$studioIds=$_SESSION["studioIds"];
	}*/
	$stdTax = $db->rawQueryone($sql,$params);
	if(isset($stdTax) && !empty($stdTax)) {
		$timezone = $stdTax["stimeZone"];
		//$status = $stdTax["status"];
	} ?>	
	<div class="form-area">
		<div class="event-listing-module">
			<div class="elm-control">
				<div class="pfp-outer">
					<form method="post" action="" role="form" id="studioZoneForm">
							<div class="dyanmicRadio">
								Set Studio TimeZone
							</div>
							<div class="dynamicBox">
						<select name="lstTimezone" id="lstTimezone" class="slectfull" style="width:80%;" <?php echo $instyle?>>
                        <option value="">Select Time Zone</option>
                        <?php 
						foreach($timezones as $ky=>$kval) {
						if(isset($timezone) && $timezone==$ky) {
						echo '<option value="'.$ky.'" selected>'.$kval.'</option>';
						} else {
							if($ky=='America/Chicago' && $timezone=="") {
							echo '<option value="'.$ky.'" selected>'.$kval.'</option>';
							} else {
							echo '<option value="'.$ky.'">'.$kval.'</option>';
							}
						}
						
						}
						?>
                        </select>

								<button type="submit" class="pfp-save" <?php echo $btnstyle?>>Save</button>

							</div>
                      
                      	<div class="dyanmicRadio" id="tzid" style="display:none;">
								
							</div>      
							<input name="updateaction" type="hidden" value="updateaction">
						<input name="id" type="hidden" value="<?php echo $stdTax["id"] ;?>">

					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

$(document).on('change', '#lstTimezone', function(){
  //alert("dsfsd");
  $.ajax({
			url : '<?php echo base_url_site; ?>addon/ajaxGetDST.php',
			type : 'POST', 
			data :{tzone:this.value},
			success : function(response) {
				$("#tzid").show();
				$("#tzid").html(response);
				
				//$('.loading').hide(); 
				
			}
		});	
  
  
});

$(document).ready(function() {

$("#lstTimezone").trigger("change");
 




$("#studioZoneForm").validate({
	rules: {
		lstTimezone:"required"
	},
	messages: {
		lstTimezone: { required: "Please select timezone" },
	},
	submitHandler: function(form) {
	    $('.loading').show();
		$('.studiochr-msg').html("");
		$.ajax({
			url : '<?php echo base_url_site; ?>addon/ajaxSaveStudioTimeZone',
			type : 'POST', 
			data :$('#studioZoneForm').serialize(),
			success : function(response) {
				$('.loading').hide(); 
				var odata = $.parseJSON(response); 
				window.location.href = "<?php echo base_url_site;?>studioTimeZone";
			}
		});	
	}
});
});
</script>
<!-- left content area end-->
<?php
include('includes/footer.php');
?>