<?php
include("../includes/functions.php");
global $db;
$userId     = isset($_POST['userId'])?$_POST['userId']:"";
$proSku     = isset($_POST['proSku'])?$_POST['proSku']:"";
$proDetailedDesc = isset($_POST['proDetailedDesc'])?StringInputCleaner($_POST['proDetailedDesc']):"";
$proName     = isset($_POST['proName'])?StringInputCleaner($_POST['proName']):"";
$proTypeId     = isset($_POST['proTypeId'])?$_POST['proTypeId']:"";
$proClassId     = isset($_POST['proClassId'])?$_POST['proClassId']:"";
$proPrice = isset($_POST['proPrice'])?$_POST['proPrice']:"";
$proPrice1 = str_replace("$", "", $proPrice);
$proPriCatId = isset($_POST['proPriCatId'])?$_POST['proPriCatId']:"";
//pr($_POST);die;
//If schedule date is optional then 
if($_POST['proSchStDate'] == ''){ 
	$proSchDtOptional = 1;
} else {
	$proSchDtOptional = 0;
}
	
if($_POST['proSchStDate'] != ''){
	$proSchStDateVal = date('Y-m-d', strtotime($_POST['proSchStDate']));
} else {
	//$proSchStDateVal = '';
	$proSchStDateVal = date('Y-m-d');  
}

if($_POST['proSchStTime'] != ''){
 $proSchStTime = $_POST['proSchStTime'];
} else {
 //$proSchStTime = '';
 $proSchStTime = date('h:i a');
}

if($proSchStDateVal == '' && $proSchStTime == ''){
	$dtFinal = '';
} else {
	$dt = $proSchStDateVal.''.$proSchStTime;
	$dtFinal = date( 'Y-m-d H:i:s', strtotime($dt) );
}

$actionType = $_POST['actionType'];
if($actionType == 'updateAction'){
	if($proSchDtOptional ==1){ //If Project schedule is optional then it will saved as draft
		$status = 2;
		$actionvar = "Project modified and drafted";
	} else {
		if(strtotime($dtFinal)<= strtotime("now")){
			$actionvar = "Project modified and published";
			//$status = 0; //published
		} else {
			$actionvar = "Project modified and scheduled";
			//$status = 2; //scheduled
		}
		$status = 0;
	}
} 
if($actionType == 'updatedraftAction'){
	$actionvar = "Project drafted";
	$status = 2; //draft
}

if($actionType == 'updatepreviewAction'){
	$actionvar = "Project previewed";
	//get project status 
	$params7 = array('');
	$result7 = $db->rawQuery("SELECT status FROM bb_project WHERE id='".$_POST["projectId"]."' ", $params7);
	$ritems7 = (array)$result7;
	if(!empty($ritems7)){
		$status = $ritems7[0]['status'];
	}
}
/*echo "proSchDtOptional===".$proSchDtOptional;
echo "status=".$status; die;*/

$proAltCat = isset($_POST['proAltCat'])?$_POST['proAltCat']:"";
if($_POST['proFpPrintList'] != ''){
  $proFpPrintList = implode(',',$_POST['proFpPrintList']);
} else {
  $proFpPrintList = '';
}

$proFpPrintCustom = isset($_POST['proFpPrintCustom'])?StringInputCleaner($_POST['proFpPrintCustom']):"";
$proSpecWi = isset($_POST['proSpecWi'])?IntegerInputCleaner($_POST['proSpecWi']):"";
$proSpecHi = isset($_POST['proSpecHi'])?IntegerInputCleaner($_POST['proSpecHi']):"";
$proSpecDe = isset($_POST['proSpecDe'])?IntegerInputCleaner($_POST['proSpecDe']):"";
$proSpecWe = isset($_POST['proSpecWe'])?IntegerInputCleaner($_POST['proSpecWe']):"";

/*$images_arr = array();
if(!empty($_FILES) && is_array($_FILES)){
	$fileName = $_FILES['proGalImg']['name'];
	$fileTempName = $_FILES['proGalImg']['tmp_name'];
	$fldname="uploads/projects/";
	$resizefldname="700X600/";
	$flg="";
	$md5filename="pid!".md5(time());
	$type = strtolower(substr(strrchr($fileName,"."),1));
	if($type == 'jpeg') $type = 'jpg';
	if($type == 'JPEG') $type = 'jpg';
	if($type == 'JPG') $type = 'jpg';
	if($type == 'GIF') $type = 'gif';
	if($type == 'BMP') $type = 'bmp';
	if($type == 'PNG') $type = 'png';
	$images_arr[0]=$md5filename.".".$type;
	upload_image_to_s3project($md5filename,$fileName,$fileTempName,$fldname,$resizefldname,$flg,@$_POST['flpup']);	
} else {
  $images_arr[0] = @$_POST['flpup'];
}*/
//echo '<pre>'; print_r($images_arr); die;
$data = array (
	'userId' => $userId,
	'proName' => $db->escape($proName),
	'proTypeId' => $proTypeId,
	'proSku' => $proSku,
	'proClassId' => $proClassId,
	'proDetailedDesc' => $db->escape($proDetailedDesc),
	'proPrice' =>  $proPrice1,
	'proPriCatId' => $proPriCatId,
	'proAltCat' => $proAltCat,
	'proSchType' => $_POST['proSchType'],
	'proSchStDate' => $proSchStDateVal,
	'proSchStTime' => $proSchStTime,  
	'proSchStDtTime' => $dtFinal,
	'proSchDtOptional' => $proSchDtOptional,
	//'proGalImg' => $images_arr[0],
	'proFpPrintList' => $proFpPrintList,
	'proFpPrintCustom' => $db->escape($proFpPrintCustom),
	'proSpecWi' =>  $db->escape($proSpecWi),
	'proSpecHi' =>  $db->escape($proSpecHi),
	'proSpecDe' =>  $db->escape($proSpecDe),
	'proSpecWe' =>  $db->escape($proSpecWe),
	'status' => $status,
	'isDeleted' => 0,
	'updatedDate' => date('Y-m-d H:i:s')
);
$db->where ('id', $_POST["projectId"]);
$update = $db->update("bb_project",$data);
$last_insert_id = $_POST["projectId"];	
if($update) {
	
	if(!empty($_POST['proOptionVal'][0]) && is_array($_POST['proOptionVal'])) {
		
		$params1 = array('');
		$result1 = $db->rawQuery("SELECT id,proId FROM bb_propersoptions WHERE proId='".$last_insert_id."' ", $params1);
		$ritems1 = (array)$result1;
		if(!empty($ritems1)) {
		    foreach($ritems1 as $key=>$val) {
				$db->where ('id', $val['id']);
				$db->where ('proId', $val['proId']);
				$db->delete("bb_propersoptions");
				//delete value from bb_prooptionadd 
				//$db->where ('id', $val['id']);
				
				
			}
		}
		$db->where ('proId', $last_insert_id);
		$db->delete("bb_prooptionadd");
		// pr($_POST);die;
		$count_check=0;
		 foreach($_POST['proOptionVal'] as $value){
			//echo "value is".$value;
			 
			 $data1 = array (
					'proUserId' => $_POST['userId'],
					'proId' => $last_insert_id,
					'proOptionVal' => $value,
				    'proLabel' => $db->escape(StringInputCleaner($_POST['proLabel'][$count_check])),
					'proHoverTxt' => $db->escape(StringInputCleaner($_POST['proHoverTxt'][$count_check])),
					
				);
				if(!empty($_POST['proOptVal_'.$count_check])){
					$data1['proOptVal']=$_POST['proOptVal_'.$count_check][0];
					
				}
				if(!empty($_POST['description_'.$count_check])){
					$data1['description']=$_POST['description_'.$count_check][0];
				}
				if(!empty($_POST['min_'.$count_check])){
					$data1['min']=$_POST['min_'.$count_check][0];
					$data1['max']=$_POST['max_'.$count_check][0];
				}
				///pr($data1);
				$ins1 = $db->insert("bb_propersoptions",$data1);
				if(!empty($_POST['option_per_'.$count_check])){
						
					
				foreach($_POST['option_per_'.$count_check] as $value_option_per){
						$data21 = array (
						'propersoptions_id' =>$ins1,
						'proId' => $last_insert_id,
						'proOptionVal' => $value,
						'option_value' => $db->escape(StringInputCleaner($value_option_per))
						);
						$last_insert_id_option_add =$db->insert("bb_prooptionadd",$data21);
					} 
				}  
			 $count_check++;
		 }
		
	}  
	else { 
		$params1 = array('');
		$result1 = $db->rawQuery("SELECT id,proId FROM bb_propersoptions WHERE proId='".$last_insert_id."' ", $params1);
		$ritems1 = (array)$result1;
		if(!empty($ritems1)) {
		    foreach($ritems1 as $key=>$val) {
				$db->where ('id', $val['id']);
				$db->where ('proId', $val['proId']);
				$db->delete("bb_propersoptions");
			}
		}
	}
	
	if(!empty($_POST['specOptionVal'][0]) && is_array($_POST['specOptionVal'])) {
	    $params3 = array('');
		$result3 = $db->rawQuery("SELECT id,proId FROM bb_prospecifications WHERE proId='".$last_insert_id."' ", $params3);
		$ritems3 = (array)$result3;
		if(!empty($ritems3)) {
			foreach($ritems3 as $key1=>$val1) {
			    $db->where ('id', $val1['id']);
				$db->where ('proId', $val1['proId']);
				$db->delete("bb_prospecifications");
			} 
		}
		for($j=0;$j<count($_POST['specOptionVal']); $j++) {
			$data3 = array (
				'userId' => $_POST['userId'],
				'proId' => $last_insert_id,
				'specOptionVal' => $_POST['specOptionVal'][$j],
				'specType' => $_POST['specType'][$j],
				'specTypeVal' => $_POST['specTypeVal'][$j]
			);
			$ins2 = $db->insert("bb_prospecifications",$data3);
			
		}
	}
	
	//save record of Restrication to table bb_prorestrictions for phase 2 pankaj
	
	$db->where('proId', $last_insert_id);
	$db->delete("bb_prorestrictions");
	$res_option_count=0;
	//pr($_POST);
	if(!empty($_POST['resOptionVal'][0]) && is_array($_POST['resOptionVal'])) {
			
		foreach($_POST['resOptionVal'] as $value){
			
			$data3 = array (
				//'userId' => $_POST['userId'],
				'proId' => $last_insert_id,
				'rsOptionVal' => $value,
			);
		   if($value==1){
				$data3['rsCouponType']=$_POST['resCouponType'][0];
				
				if(!empty($_POST['code_select_val'])){
					$explodeCode = explode(",",$_POST['code_select_val']);
					$implodeCodeId= implode(",",$explodeCode);
					$data3['code_id']=$implodeCodeId;
					 
				}
				if(!empty($_POST['amount'][0])){
					$data3['couponAmount']=$_POST['amount'][0];
				}
				$db->insert("bb_prorestrictions",$data3);
				
			}
			
			else if($value==2){
				$explodeStudioId = explode(",",$_POST['studio_select_val']);
				$implodeStudioid = implode(",",$explodeStudioId);
				$data3['userId']=$implodeStudioid;
				$db->insert("bb_prorestrictions",$data3);
			}
			
			$res_option_count++;
		}
		//die;
		
	}
	//code to save image in gallery for 2nd phase by pankaj
   // $db->where('proId', $last_insert_id);
	//$db->delete("bb_progallery");
	
	// image code start//
	
	$i=0;
if(!empty($_FILES) && is_array($_FILES)){
		foreach ($_FILES as $filename) 
            {
				$fileName = $filename['name'];
				$fileTempName = $filename['tmp_name'];
				$fldname="uploads/projects/";
				$resizefldname="700X600/";
				$flg="";
				$md5filename="cid!".md5(time());
				$type = strtolower(substr(strrchr($fileName,"."),1));
				if($type == 'jpeg') $type = 'jpg';
				if($type == 'JPEG') $type = 'jpg';
				if($type == 'JPG') $type = 'jpg';
				if($type == 'GIF') $type = 'gif';
				if($type == 'BMP') $type = 'bmp';
				if($type == 'PNG') $type = 'png';
				$images_arr[$i]=$md5filename.".".$type;
				if($fileName==$firstimg)
				{
					$firstimg=$md5filename.".".$type;
				}
				
				upload_image_to_s3project($md5filename,$fileName,$fileTempName,$fldname,$resizefldname,$flg,@$_POST['flpup']);
			$dataGallery = array (
			'proUserId' => $_POST['userId'],
			'proImgName' =>$images_arr[$i],
			'proId' => $last_insert_id,
			
		);
		
		
		$db->insert("bb_progallery",$dataGallery);
				$i++;
            }
		}
	else{
			$firstimg = "";
		
		}
	
	
	//Project History insertion
	$data4 = array (
		'lgUserId' => $_POST['userId'],
		'lgProId' => $last_insert_id,
		'lgDescription' => $actionvar,
		'lgDate' => date('Y-m-d'),
		'lgTime' => date('H:i:s')
	);
	$db->insert("bb_proactivitylog",$data4);
	
	//$response["msg"] = "Project data is successfully updated"; //$db->getLastQuery();
	//$response["last_insert_id"] = $last_insert_id;
	//$_SESSION["msg"]= "Successfully updated Project data";
	if($actionType == 'updateAction'){
		$_SESSION["msg"]= "Successfully updated project data";
		$response["status"] = 0;
	} 
	if($actionType == 'updatedraftAction'){
		$_SESSION["msg"]= "Successfully updated draft project data";
		$response["status"] = 2;
	}
	if($actionType == 'updatepreviewAction'){
		$_SESSION["msg"]= "Successfully previewed project data";
		$response["status"] = 5;
		$response["last_insert_id"] = $last_insert_id;
	}
} 
else {
	//$response["msg"] = "Project data is not successfully updated";//$db->getLastError();
	//$response["last_insert_id"] = '';
	if($actionType == 'updateAction'){
		$_SESSION["msg"]= "Not successfully updated project data";
	} 
	if($actionType == 'updatedraftAction'){
		$_SESSION["msg"]= "Not successfully updated draft project data";
	}
	if($actionType == 'updatepreviewAction'){
		$_SESSION["msg"]= "Not successfully previewed project data";
	}
}
echo json_encode($response);
die;
?>