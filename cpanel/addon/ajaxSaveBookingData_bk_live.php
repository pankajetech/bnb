<?php
include("../includes/functions.php");
global $db;
$response = array();
echo '<pre>=='; print_r($_POST);
die;

$pageAction = $_POST['pageAction'];


$bookingId = $_POST['bookingId']; //for update case
$actionType = $_POST['actionType'];
$bookingStatus = $_POST['bookingStatus'];
if( $bookingStatus == 2 || $bookingStatus == 5){  //2=>Cancel,5=>Reject
} else {
	if( $actionType == 'unapproveBoookingAction' || $actionType == 'rejectBoookingAction' || $actionType == 'cancelBoookingAction' ){
		decreaseSeatsFromBookedEvent($bookingId);
	}
} 
//1=>Unapprove,2=>Cancel,5=>Reject
if( $bookingStatus == 1 || $bookingStatus == 2 || $bookingStatus == 5){  
	if($actionType == 'approveBoookingAction') {
		increaseSeatsFromBookedEvent($bookingId);
	}
} 

if($actionType == 'updateAction'){ $status = 0; }  //Update changes in completed booking action
if($actionType == 'updateCancelAction'){ $status = 2; }  //Update changes in cancel booking action
if($actionType == 'updateNeedAttentionAction'){ $status = 3; }  //Update changes in needs attention booking action
if($actionType == 'updateChangeProjectAction'){ $status = 4; }  //Update changes in change project booking action
if($actionType == 'approveBoookingAction'){ $status = 0; }  //approve booking in case of needs attention or cancelled booking
if($actionType == 'cancelBoookingAction'){ $status = 2;  }  //cancel booking
if($actionType == 'rejectBoookingAction'){ $status = 5;  }  //reject booking
if($actionType == 'unapproveBoookingAction'){ $status = 1;  }  //unapprove booking

$userId     = isset($_POST['userId'])?$_POST['userId']:"";
$boFullName = isset($_POST['boFullName'])?StringInputCleaner($_POST['boFullName']):"";
$boPhone    = isset($_POST['boPhone'])?$_POST['boPhone']:"";
$boAddress  = isset($_POST['boAddress'])?StringInputCleaner($_POST['boAddress']):"";
$boAccommodations  = isset($_POST['boAccommodations'])?StringInputCleaner($_POST['boAccommodations']):"";
$boAccommodationsNotes  = isset($_POST['boAccommodationsNotes'])?StringInputCleaner($_POST['boAccommodationsNotes']):"";

$boEventIdHidden = isset($_POST['boEventIdHidden'])?$_POST['boEventIdHidden']:"";
$boEventNameHidden = isset($_POST['boEventNameHidden'])?StringInputCleaner($_POST['boEventNameHidden']):"";
$boEventDateHidden = isset($_POST['boEventDateHidden'])?$_POST['boEventDateHidden']:"";
$boEventStartTimeHidden = isset($_POST['boEventStartTimeHidden'])?$_POST['boEventStartTimeHidden']:"";
$boEventEndTimeHidden = isset($_POST['boEventEndTimeHidden'])?$_POST['boEventEndTimeHidden']:"";

$caddress = isset($_POST['caddress'])?StringInputCleaner($_POST['caddress']):"";
$ccity = isset($_POST['ccity'])?StringInputCleaner($_POST['ccity']):"";
$cstate = isset($_POST['cstate'])?StringInputCleaner($_POST['cstate']):"";
$czip = isset($_POST['czip'])?IntegerInputCleaner($_POST['czip']):"";

$data = array (
	'eventId' => $boEventIdHidden,
	'boEventName' => $db->escape($boEventNameHidden),
	'boEventDate' => $boEventDateHidden,
	'eventStTime' => $boEventStartTimeHidden,
	'eventEndTime' => $boEventEndTimeHidden,
	
	'boFullName' => $db->escape($boFullName),
	'boPhone' => $boPhone,
	'boAddress' => $db->escape($boAddress),
	'boAccommodations' =>  $db->escape($boAccommodations),
	'boAccommodationsNotes' => $db->escape($boAccommodationsNotes),
	'status' => $status,
	'bookingUpdationDate' => date('Y-m-d H:i:s'),
);

if($bookingId != ''){ //update case
	$db->where ('id', $bookingId);
	$update = $db->update("bb_booking",$data);
	
	//update bb_transaction details tbl with address info
	$data999 = array (
		'caddress' => $db->escape($caddress),
		'ccity' => $db->escape($ccity),
		'cstate' => $db->escape($cstate),
		'czip' => $db->escape($czip)
	);
	
	$db->where ('bookingId', $bookingId );
	$db->update("bb_transactiondetails",$data999);
	
	if($update) {
		
		/*******************************************************************************************
		Add/Update ticket Projecte Info :- bb_booktcktinfo
		************************************************************************************/
		/*if($pageAction == 'reschedule'){
			if(!empty($_POST['boTicketName']) && is_array($_POST['boTicketName'])) {
				foreach($_POST['boTicketName'] as $key=>$val){
					$bookingTicketNewArr[$key]['boTicketName'] = $val;
				}
			}
			if(!empty($_POST['boTicketPrice']) && is_array($_POST['boTicketPrice'])) {
				foreach($_POST['boTicketPrice'] as $key=>$val){
					$bookingTicketNewArr[$key]['boTicketPrice'] = $val;
				}
			}
		}*/
		
		if(!empty($_POST['boTicketProjectId']) && is_array($_POST['boTicketProjectId'])) {
			foreach($_POST['boTicketProjectId'] as $key=>$val){
				$bookingTicketNewArr[$key]['boProjectId'] = $val;
			}
		}
		
		if(!empty($_POST['boTicketProjectNameTxt']) && is_array($_POST['boTicketProjectNameTxt'])) {
			foreach($_POST['boTicketProjectNameTxt'] as $key=>$val){
				$bookingTicketNewArr[$key]['boTicketProjectName'] = $db->escape(StringInputCleaner($val));
			}
		}
		
		if(!empty($_POST['boTicketSkuTxt']) && is_array($_POST['boTicketSkuTxt'])) {
			foreach($_POST['boTicketSkuTxt'] as $key=>$val){
				$bookingTicketNewArr[$key]['boTicketSku'] = $val;
			}
		}
		
		if(!empty($_POST['boTicketSitBy']) && is_array($_POST['boTicketSitBy'])) {
			foreach($_POST['boTicketSitBy'] as $key=>$val){
				$bookingTicketNewArr[$key]['boTicketSitBy'] = $db->escape(StringInputCleaner($val));
			}
		}
		
		if(!empty($_POST['boTicketProjectSize']) && is_array($_POST['boTicketProjectSize'])) {
			foreach($_POST['boTicketProjectSize'] as $key=>$val){
				$bookingTicketNewArr[$key]['boTicketProjectSize'] = $val;
			}
		}
		
		if(!empty($_POST['boTicketProjectImage']) && is_array($_POST['boTicketProjectImage'])) {
			foreach($_POST['boTicketProjectImage'] as $key=>$val){
				$bookingTicketNewArr[$key]['boTicketProjectImage'] = $val;
			}
		}
		
		/*echo '<pre>bookingTicketNewArr=='; print_r($bookingTicketNewArr);
		die;*/
		
		/*if($pageAction == 'reschedule'){
			
			//Delete  bb_booktcktinfo Tbl Data
			$params1 = array('');
			$result15 = $db->rawQuery("SELECT * FROM bb_booktcktinfo WHERE bookingId='".$bookingId."' ", $params1);
			$ritems15 = (array)$result15;
			if(!empty($ritems15)) {
				foreach($ritems15 as $key15=>$val15) {
					$db->where ('id', $val15['id']);
					$db->where ('bookingId', $val15['bookingId']);
					$db->delete("bb_booktcktinfo");
				}
			}	
			//Insert  bb_booktcktinfo Tbl Data	
			if(!empty($bookingTicketNewArr) && is_array($bookingTicketNewArr)) {
				foreach($bookingTicketNewArr as $keyId=>$valArr){
					$data111 = array (
						'bookingId' => $bookingId,
						'boTicketName' => $valArr['boTicketName'],
						'boTicketPrice' => $valArr['boTicketPrice'],
						'boProjectId' => $valArr['boProjectId'],
						'boTicketProjectName' => $valArr['boTicketProjectName'],
						'boTicketSku' => $valArr['boTicketSku'],
						'boTicketSitBy' => $valArr['boTicketSitBy'],
						'boTicketProjectSize' => $valArr['boTicketProjectSize'],
						'boTicketProjectImage' => $valArr['boTicketProjectImage']
					);
					$last_inserted_ticket_id[] = $db->insert("bb_booktcktinfo",$data111);
				}
			}
		} 
		else {*/
		if($pageAction != 'reschedule'){
			if(!empty($bookingTicketNewArr) && is_array($bookingTicketNewArr)) {
				foreach($bookingTicketNewArr as $keyId=>$valArr){
					$db->where ('id', $keyId);
					$db->update("bb_booktcktinfo",$valArr);
				}
			}
		}
		//}
		
		
		/*****************************************************************************************
		Add/Update ticket Projecte Info :- bb_booktcktpersinfo
		***********************************************************************************************/
		/*[boPersOptName] => Array
        (
            [24] => Array
                (
                    [0] => Label Multi Line
                    [1] => Label Text
                    [2] => Label Date
                )
			[25] => Array
                (
                    [0] => label text
                    [1] => label multi-line
                )
		)*/
		//echo '<pre>last_inserted_ticket_id=='; print_r($last_inserted_ticket_id);
		//echo '<pre>boPersOptVal=='; print_r($_POST['boPersOptVal']);//die;
		if(!empty($_POST['boPersOptName']) && is_array($_POST['boPersOptName'])) {
			foreach($_POST['boPersOptName'] as $keyy=>$vall){
				if(!empty($vall)){
					for($j=0;$j<count($vall);$j++){
						/*if($pageAction == 'reschedule'){
							$bookingTickPersNewArr[$last_inserted_ticket_id[$keyy]][$j]['boPersOptName'] = $vall[$j];
						} 
						else{*/
							$bookingTickPersNewArr[$keyy][$j]['boPersOptName'] = $vall[$j];
						//}
					}
				}
			}
		}
		
		if(!empty($_POST['boPersOptType']) && is_array($_POST['boPersOptType'])) {
			foreach($_POST['boPersOptType'] as $keyy=>$vall){
				if(!empty($vall)){
					for($j=0;$j<count($vall);$j++){
						/*if($pageAction == 'reschedule'){
							$bookingTickPersNewArr[$last_inserted_ticket_id[$keyy]][$j]['boPersOptType'] = $vall[$j];
						} 
						else{*/
							$bookingTickPersNewArr[$keyy][$j]['boPersOptType'] = $vall[$j];
						//}
					}
				}
			}
		}
		
		if(!empty($_POST['boPersOptHoverTxt']) && is_array($_POST['boPersOptHoverTxt'])) {
			foreach($_POST['boPersOptHoverTxt'] as $keyy=>$vall){
				if(!empty($vall)){
					for($j=0;$j<count($vall);$j++){
						/*if($pageAction == 'reschedule'){
							$bookingTickPersNewArr[$last_inserted_ticket_id[$keyy]][$j]['boPersOptHoverTxt'] = $vall[$j];
						} 
						else{*/
							$bookingTickPersNewArr[$keyy][$j]['boPersOptHoverTxt'] = $vall[$j];
						//}
					}
				}
			}
		}
		
		if(!empty($_POST['boPersOptTypeFormat']) && is_array($_POST['boPersOptTypeFormat'])) {
			foreach($_POST['boPersOptTypeFormat'] as $keyy=>$vall){
				if(!empty($vall)){
					for($j=0;$j<count($vall);$j++){
						/*if($pageAction == 'reschedule'){
							$bookingTickPersNewArr[$last_inserted_ticket_id[$keyy]][$j]['boPersOptTypeFormat'] = $vall[$j];
						} 
						else{*/
							$bookingTickPersNewArr[$keyy][$j]['boPersOptTypeFormat'] = $vall[$j];
						//}
					}
				}
			}
		}
		
		if(!empty($_POST['boPersOptVal']) && is_array($_POST['boPersOptVal'])) {
			foreach($_POST['boPersOptVal'] as $keyy=>$vall){
				if(!empty($vall)){
					for($j=0;$j<count($vall);$j++){
						/*/if($pageAction == 'reschedule'){
							$bookingTickPersNewArr[$last_inserted_ticket_id[$keyy]][$j]['boPersOptVal'] = $vall[$j];
						} 
						else{*/
							$bookingTickPersNewArr[$keyy][$j]['boPersOptVal'] = $vall[$j];
						//}
					}
				}
			}
		}
		//echo '<pre>bookingTickPersNewArr=='; print_r($bookingTickPersNewArr);die;
		
		if(!empty($_POST['boPersOptVal']) && is_array($_POST['boPersOptVal'])) {  
			$params1 = array('');
			$result1 = $db->rawQuery("SELECT id,bookingId,bookingTicketId FROM bb_booktcktpersinfo WHERE bookingId='".$bookingId."' ", $params1);
			$ritems1 = (array)$result1;
			if(!empty($ritems1)) {
				foreach($ritems1 as $key=>$val) {
					$db->where ('id', $val['id']);
					$db->where ('bookingId', $val['bookingId']);
					$db->where ('bookingTicketId', $val['bookingTicketId']);
					$db->delete("bb_booktcktpersinfo");
				}
			}
			
			foreach($bookingTickPersNewArr as $key7=>$val7) {
				for($k=0;$k<count($val7); $k++) {
					$data1 = array (
						'bookingId' => $bookingId,
						'bookingTicketId' => $key7,
						'boPersOptName' => $db->escape(StringInputCleaner($val7[$k]['boPersOptName'])),
						'boPersOptVal' => $db->escape(StringInputCleaner($val7[$k]['boPersOptVal'])),
						'boPersOptType' => $val7[$k]['boPersOptType'],
						'boPersOptHoverTxt' => $db->escape(StringInputCleaner($val7[$k]['boPersOptHoverTxt'])),
						'boPersOptTypeFormat' => $val7[$k]['boPersOptTypeFormat']
					);
					$ins1 = $db->insert("bb_booktcktpersinfo",$data1);
				}
			}
		}  
		else {
			$params1 = array('');
			$result1 = $db->rawQuery("SELECT id,bookingId,bookingTicketId FROM bb_booktcktpersinfo WHERE bookingId='".$bookingId."' ", $params1);
			$ritems1 = (array)$result1;
			if(!empty($ritems1)) {
				foreach($ritems1 as $key=>$val) {
					$db->where ('id', $val['id']);
					$db->where ('bookingId', $val['bookingId']);
					$db->where ('bookingTicketId', $val['bookingTicketId']);
					$db->delete("bb_booktcktpersinfo");
				}
			}
		}
		
		if($pageAction == 'reschedule'){
			//Get 'totalTickets' field from bb_booking tbl
			/*$params1 = array('');
			$result78 = $db->rawQueryone("SELECT totalTickets FROM bb_booking WHERE id='".$bookingId."' ", $params1);
			$ritems78 = (array)$result78;
			if(!empty($ritems78)) {
				$totalSeatsAsPerOldEventID = $ritems78['totalTickets'];
			} else{
				$totalSeatsAsPerOldEventID = 0;
			}*/
			$totalSeatsAsPerOldEventID = getTotalTicketWRTBookingId($bookingId);
			
			//Set/Update 'totalTickets' field from bb_booking tbl
			/*if(!empty($_POST['boTicketName']) && is_array($_POST['boTicketName'])) {
				$totlSeatsInAnyBooking = count($_POST['boTicketName']);
			} else{
				$totlSeatsInAnyBooking = 0;
			}
			$data65 = array ('totalTickets' => $totlSeatsInAnyBooking);	
			$db->where ('id', $bookingId);
			$db->update("bb_booking",$data65);*/
			
			//Increase 'evBookTicket' from 'bb_event' tbl from new eventID
			$newEventId = $boEventIdHidden;
			//Fetch Total booked seats in new event till now
			$totBookedSeatsTillNow = cntEventBookedSeats($newEventId);
			//Increase Seats after reschedule
			//$updTotSeats = $totBookedSeatsTillNow + $totlSeatsInAnyBooking;
			$updTotSeats = $totBookedSeatsTillNow + $totalSeatsAsPerOldEventID;
			
			updateIncreaseSeatsInNewEvent($updTotSeats,$newEventId);
				
			//Decrease 'evBookTicket' from 'bb_event' tbl from old eventID
			$oldEventId = $_POST['oldEventId'];
			
			//Fetch Total booked seats in old event till now
			$totBookedSeatsTillNow1 = cntEventBookedSeats($oldEventId);
			
			//Decrease Seats after reschedule
			$updTotSeatsInNewEvent = $totBookedSeatsTillNow1 - $totalSeatsAsPerOldEventID;
			
			if($updTotSeatsInNewEvent>=0) {
				$updTotSeatsInNewEvent1 =  $updTotSeatsInNewEvent;
			} else{
				$updTotSeatsInNewEvent1 = 0;
			}
			//echo "updTotSeatsInNewEvent==".$updTotSeatsInNewEvent1;die;
			updateDecreaseSeatsFromOldEvent($updTotSeatsInNewEvent1,$oldEventId);
		}
		
		
		if($actionType == 'updateAction'){
			//$_SESSION["msg"]= "Successfully updated completed booking data";
			$response["status"] = 0; //Completed
		} 
		if($actionType == 'updateCancelAction'){
			//$_SESSION["msg"]= "Successfully updated cancel booking data";
			$response["status"] = 2; //Cancelled
		}
		if($actionType == 'updateNeedAttentionAction'){
			//$_SESSION["msg"]= "Successfully updated needs attention booking data";
			$response["status"] = 3; //Needs attention
		}
		if($actionType == 'updateChangeProjectAction'){
			$_SESSION["msg"]= "Successfully updated change project booking data";
			$response["status"] = 4; //Update change project
		}
		if($actionType == 'approveBoookingAction'){
			$_SESSION["msg"]= "Successfully approved booking mail sent to user";
			$response["status"] = 0; //Complete needs attention booking
		}
		if($actionType == 'cancelBoookingAction'){
			$_SESSION["msg"]= "Successfully cancelled booking and mail sent to user";
			$response["status"] = 2; //Cancellled booking
		} 
		if($actionType == 'rejectBoookingAction'){
			$_SESSION["msg"]= "Successfully rejected booking and mail sent to user";
			$response["status"] = 5; //Rejected booking
		}
		if($actionType == 'unapproveBoookingAction'){
			$_SESSION["msg"]= "Successfully unapproved booking and mail sent to user";
			$response["status"] = 1; //Unapproved booking
		}
		
		//$_SESSION["msg"]= "Successfully updated booking";
		
	} 
	else {
		if($actionType == 'updateAction'){
			$_SESSION["msg"]= "Not successfully updated completed booking data";
		} 
		if($actionType == 'updateCancelAction'){
			$_SESSION["msg"]= "Not successfully updated cancel booking data";
		}
		if($actionType == 'updateNeedAttentionAction'){
			$_SESSION["msg"]= "Not successfully updated needs attention booking data";
		}
		if($actionType == 'updateChangeProjectAction'){
			$_SESSION["msg"]= "Not successfully updated change project booking data";
		}
		if($actionType == 'approveBoookingAction'){
			$_SESSION["msg"]= "Not successfully approved booking";
		}
		if($actionType == 'cancelBoookingAction'){
			$_SESSION["msg"]= "Not successfully cancelled booking";
		} 
		if($actionType == 'rejectBoookingAction'){
			$_SESSION["msg"]= "Not successfully rejected booking";
		}
		if($actionType == 'unapproveBoookingAction'){
			$_SESSION["msg"]= "Not successfully unapproved booking";
		}
	}
	$response["pageType"] = $_REQUEST['pageType'];
	$response["actionType"] = $actionType; 
	$response["customerEmail"] = $_POST['customerEmail']; 
	$response["bookingId"] = $bookingId; 
	$response["studioId"] = $_POST['studioId']; 
} 
echo json_encode($response);
die;
?>