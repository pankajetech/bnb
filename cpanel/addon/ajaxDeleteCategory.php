<?php
include("../includes/functions.php");
global $db;

$categoryId = urldecode($_POST['categoryId']);
$categoryId = trim($categoryId);
$response = array();
$data = array ( "status"=>1);
$db->where ('id',$categoryId);
$update = $db->update("bb_procategory",$data);
if($update == 1){ 
	$response['msg'] = "Successfully deleted category";
	$response['status'] = 1;
} else {
	$response['msg'] = "Not successfully deleted category";
	$response['status'] = 2;
}
echo json_encode($response);
die;
?>