<?php
include("../includes/functions.php");
global $db;

//storing  request (ie, get/post) global array to a variable  
$requestData= $_REQUEST;
$columns = array( 
// datatable column index  => database column name
	0 =>'proSchStDtTime', 
	1 => 'projectstatus', //status
	3 => 'proName',
	4 => 'opName',
	5 => 'proTypeName',
	6 => 'proCatName',
	7 => 'createdDate',
	8 => 'updatedDate',
	9 => 'lgDescription',
);

if($requestData['order'][0]['column'] == 4){
	$tblAlias = "bproCls".".";
} 
else if($requestData['order'][0]['column'] == 5){
	$tblAlias = "bproType".".";
}
else if($requestData['order'][0]['column'] == 6){
	$tblAlias = "bproCat".".";
} 
else if($requestData['order'][0]['column'] == 9){
	$tblAlias = "bproAct".".";
} 
else if($requestData['order'][0]['column'] == 1){
	$tblAlias = "";
}
else {
	$tblAlias = "bpro".".";
}

//echo "====".$tblAlias."==".$requestData['order'][0]['column'];die;
$stwhre = "";
$stwhre1 = "";
if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="archive") {
	$orderby =" order by bpro.proSchStDtTime desc";
	$stwhre.= " bpro.status=3 and bpro.isDeleted=0";
} 
else if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="draft") {
	$orderby =" order by bpro.proSchStDtTime desc";
	$stwhre.= " bpro.isDeleted=0 and ( 
		(bpro.status=0 AND bpro.proSchStDtTime > (NOW() + INTERVAL 330 Minute) AND bpro.proSchDtOptional=0 ) 
		OR 
		( bpro.status=0 AND bpro.proSchStDtTime < (NOW() + INTERVAL 330 Minute) AND bpro.proSchDtOptional=1)  
		OR bpro.status=2 
		) ";
}
else { 
	$orderby =" order by bpro.proSchStDtTime desc";
	$stwhre.= " (bpro.status=0 or bpro.status=1 or bpro.status=2) and bpro.isDeleted=0";
}


//getting records as per search parameters
if( !empty($requestData['columns'][6]['search']['value']) ){   //category name
    //$stwhre1.= " AND bproCat.proCatName LIKE '%".$requestData['columns'][6]['search']['value']."%' ";
	$stwhre1.= " AND bpro.proPriCatId = '".$requestData['columns'][6]['search']['value']."' ";
	//Getting total number records without any search
	$sql1 = "SELECT * 
			FROM `bb_project` as bpro 
			inner join `bb_procategory` as bproCat
			on bpro.proPriCatId = bproCat.id
			where $stwhre $stwhre1 $orderby";
} else {
	//Getting total number records without any search
	$sql1 = "SELECT * FROM `bb_project` as bpro where $stwhre $stwhre1 $orderby";
}

$cRev = $db->rawQuery($sql1, array(''));
if(count($cRev)>0){
	$totalData = $db->count; //count($cRev)
	$totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
} 
//echo $sql1; die;
$sql = "SELECT 
	bproAct.id as activityLogId,bproAct.lgDescription,
	bpro.id,bpro.proName,
	bpro.proSchStDtTime,bpro.proSchDtOptional,
	bpro.status,bpro.proGalImg,
	bpro.updatedDate,bpro.createdDate,
	bproCls.opName,bproType.proTypeName,bproCat.proCatName,bpro.proPriCatId,
	
	(
		CASE 
		WHEN ( bpro.status = '0' AND bpro.proSchDtOptional = '1') THEN 'Draft'
		WHEN ( bpro.status = '0' AND bpro.proSchDtOptional != '1' AND bpro.proSchStDtTime <= NOW() ) THEN 'Published'
		WHEN ( bpro.status = '0' AND bpro.proSchDtOptional != '1' AND bpro.proSchStDtTime > NOW() ) THEN 'Scheduled'
		
		WHEN bpro.status = '1' THEN 'Disabled'
		WHEN bpro.status = '2' THEN 'Draft' 
		WHEN bpro.status = '3' THEN 'Archived'
		END
	) as projectstatus
	
	FROM `bb_project` as bpro
	left join `bb_proclassoptions` as bproCls
	on bpro.proClassId = bproCls.id
	left join `bb_protype` as bproType
	on bpro.proTypeId = bproType.id
	left join `bb_procategory` as bproCat
	on bpro.proPriCatId = bproCat.id
	
	INNER JOIN  
	(
        SELECT    MAX(id) max_id, lgDescription,lgProId
        FROM      `bb_proactivitylog` 
        GROUP BY  lgProId
    ) c_max ON (c_max.lgProId = bpro.id)
	INNER JOIN  `bb_proactivitylog` AS bproAct ON (bproAct.id = c_max.max_id)
	where $stwhre $stwhre1
	ORDER BY ". ''.$tblAlias.''.$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	
//echo $sql;die;
/* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc  */	
$query = $db->rawQuery($sql, array(''));
//echo "<pre>query=";print_R($query);die;
$data = array();
if(isset($query) && !empty($query)) {
	foreach($query as $key=>$item){ // preparing an array
		$nestedData = array(); 
		
		//If user type= "studio"
		//stId
		if($_SESSION['urole'] == 3){
			$actionBtnstr = '<a href="'.base_url_site.'projectpreview?id='.$item['id'].'">View</a>';
		} 
		//If user type= "corp_admin"
		else {
			if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="draft") { 
				$actionBtnstr1 = '<a href="'.base_url_site.'addupdproject?id='.$item['id'].'&type=draft">Edit</a>';
			} 
			else if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="archive") { 
				$actionBtnstr1 = '<a href="'.base_url_site.'addupdproject?id='.$item['id'].'&type=archive">Edit</a>';
			} 
			else { 
				$actionBtnstr1 = '<a href="'.base_url_site.'addupdproject?id='.$item['id'].'">Edit</a>';
			}
			
			$actionBtnstr2 = ' | <a href="javascript:void(0);" class="delProject" customAtr="'.$item['id'].'">Delete</a>';
			$actionBtnstr = $actionBtnstr1.$actionBtnstr2;
		} // end user level block
		
		/*if($item['status'] == 0){
			if($item['proSchDtOptional'] == 1){
				$statusType = "Draft"; 
			} else {
				if(strtotime($item['proSchStDtTime'])<= strtotime("now")){
					$statusType = "Published"; 
				} else {
					$statusType = "Scheduled"; 
				}
			}
		} 
		else if($item['status'] == '1') { 
			$statusType = "Disabled"; 
		} 
		else if($item['status'] == '2') { 
			$statusType = "Draft"; 
		} 
		else if($item['status'] == '3') { 
			$statusType = "Archived"; 
		} */
		
		if(isset($item['proGalImg']) && $item['proGalImg']!=""){ 
			$imgSrcUrl = s3_url_uimages.'uploads/projects/700X600/'.$item["proGalImg"];
		} else {  
			$imgSrcUrl = base_url_images.'placeholder.jpg';
		}
		$imgSrc = '<img src="'.$imgSrcUrl.'" height="80" width="80" />';
		
		if(isset($item['proName']) && $item['proName']!=""){ 
			$proName = $item['proName']; 
		} else { 
			$proName ="NA";
		} 
		
		if(isset($item['opName']) && $item['opName']!=""){ 
			$proClassId = $item['opName'];
		} else { 
			$proClassId = "NA";
		}
		
		if(isset($item['proTypeName']) && $item['proTypeName']!=""){ 
			$proTypeName = $item['proTypeName'];
		} else { 
			$proTypeName = "NA";
		}
		
		if(isset($item['proCatName']) && $item['proCatName']!=""){ 
			$proCatName = $item['proCatName'];
		} else { 
			$proCatName = "NA";
		}
		
		
		if(isset($item['createdDate']) && $item['createdDate']!=""){ 
			$createdDate = date("m/d/Y",strtotime($item['createdDate'])); 
		} else { 
			$createdDate = "NA";
		}
		
		if(isset($item['updatedDate']) && $item['updatedDate']!=""){ 
			$updatedDate = date("m/d/Y",strtotime($item['updatedDate'])); 
		} else { 
			$updatedDate = "NA";
		}
		
		$nestedData[] = $actionBtnstr;
		$nestedData[] = $item['projectstatus'];
		
		$nestedData[] = $imgSrc;
		$nestedData[] = $proName;
		$nestedData[] = $proClassId;
		
		$nestedData[] = $proTypeName;
		$nestedData[] = $proCatName;
		
		$nestedData[] = $createdDate;
		$nestedData[] = $updatedDate;
		
		$nestedData[] = $item['lgDescription'];  //lgDescriptionInfo($item['id']);
		$data[] = $nestedData;
	} 
	//echo "<pre>data===".print_r($data);die;
}
$json_data = array(
	"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
	"recordsTotal"    => intval( $totalData ),  // total number of records
	"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
	"data"            => $data,   // total data array
);
echo json_encode($json_data);  // send data as json format
?>