<?php 
include("../includes/functions.php");
global $db;

$reponse=array();
$strHtml="";

if(isset($_REQUEST["tempid"]) && $_REQUEST["tempid"]!="") {
	$params = array('');
	$u=0;
	if(isset($_SESSION["urole"]) && ($_SESSION["urole"]==1 || $_SESSION["urole"]==2) ) {
		if(isset($_SESSION["stId"]) && $_SESSION["stId"]) {
			$u=0;
		}
	} else {
		if(isset($_SESSION["stId"]) && $_SESSION["stId"]) {
			if(checkUserEvent($_REQUEST["evid"],$_SESSION["stId"])){
				$u=1;
				//$evLocId=$_SESSION["studid"];
				$evLocId=$_SESSION["stId"];
			}
		} else {
			if(checkUserEvent($_REQUEST["evid"],$_SESSION["studioIds"])){
				$u=1;
				$evLocId=$_SESSION["studioIds"];
			}
		}
	}
	
	$strIsClose="";
	$strIsCloseAttr="";
	$strIsCloseVal="";
	if($u==1) { 
		$result = $db->rawQueryOne("SELECT * FROM bb_event WHERE id='".$_REQUEST["evid"]."' ", $params);
		$strIsClose="yellow-btn ";
	} else { 
		if(isset($_SESSION["stId"]) && $_SESSION["stId"]!="") { 
			$u=1;
			if(isset($_REQUEST["evid"]) && $_REQUEST["evid"]!="") {
				$result = $db->rawQueryOne("SELECT * FROM bb_event WHERE id='".$_REQUEST["evid"]."' ", $params);
				$strIsClose="yellow-btn ";
			} else {
				$u=0;
				$strIsClose="dsbl-btn ";
				$result = $db->rawQueryOne("SELECT * FROM bb_eventtemplates WHERE id='".$_REQUEST["tempid"]."' ", $params);
			}
		} else {
			if(isset($_REQUEST["evid"]) && $_REQUEST["evid"]!="") {
				$u=1;
				$result = $db->rawQueryOne("SELECT * FROM bb_event WHERE id='".$_REQUEST["evid"]."' ", $params);
				$strIsClose="yellow-btn ";
			} else {
				$u=0;
				$strIsClose="dsbl-btn ";
				$result = $db->rawQueryOne("SELECT * FROM bb_eventtemplates WHERE id='".$_REQUEST["tempid"]."' ", $params);
			}
		}
	}
	$ritems = (array)$result;
	
	if(!empty($ritems)) { 
		$ritems=$ritems; 
		if($u==1) {
			$evId=@fnCheckEmpty($ritems['id']);
			$evTempId=@fnCheckEmpty($ritems['evTempId']);
			$slug_old=@fnCheckEmpty($ritems['slug']);
			$studioIdT=@fnCheckEmpty($ritems['studioId']);
			$evType=@fnCheckEmpty($ritems['evType']);
			$evTempName=@fnCheckEmpty($ritems['evTempName']);
			$evCalColor=@fnCheckEmpty($ritems['evCalenderColor']);
			$evCalLabel=@fnCheckEmpty($ritems['evCalenderColorLabel']);
			
			$evImage=@fnCheckEmpty($ritems['evImage']);
			$evDate=fnDisplayDate(@fnCheckEmpty($ritems['evDate']));
			$evStTime=@fnCheckEmpty($ritems['evStTime']);
			$evEndTime=@fnCheckEmpty($ritems['evEndTime']);
			$evLocationId=@fnCheckEmpty($ritems['evLocationId']);
			$evTitle=@fnCheckEmpty($ritems['evTitle']);
			$evFullDescription=@fnCheckEmpty($ritems['evFullDescription']);
			$evDesAddition=@fnCheckEmpty($ritems['evDesAddition']);
			$evAccessCode=@fnCheckEmpty($ritems['evAccessCode']);
			$evScheduleType=$ritems['evScheduleType'];
			$evSPubDate=@fnCheckEmpty($ritems['evSPubDate']);
			$evSPubTime=@fnCheckEmpty($ritems['evSPubTime']);
			$evsDays=@fnCheckEmpty($ritems['evsDays']);
			$evScheduleHrs=$ritems['evScheduleHrs'];
			$evRegEnd=@fnCheckEmpty($ritems['evRegEnd']);
			$evNoOfTickets=@fnCheckEmpty($ritems['evNoOfTickets']);
			$evBookTicket=@fnCheckEmpty($ritems['evBookTicket']);
			$evStatus=@fnCheckEmpty($ritems['evStatus']);
			$evTypeStatus=@fnCheckEmpty($ritems['evTypeStatus']);
			$isCancel1=@fnCheckEmpty($ritems['isCancel']);
			$isCancel=$ritems['evIsClose'];
			$isPublish=$ritems['isPublish'];
			if($isCancel==1) {
				$strIsClose="yellow-btn ";
				$strIsCloseAttr='';
				$clsestatus=0;
				$strIsCloseVal="Open Registration";
			} else {
				$strIsClose="yellow-btn ";
				$strIsCloseAttr='';
				$clsestatus=1;
				$strIsCloseVal="Close Registration";
			}
		
			if($isPublish==1) {
				$evSPubDate="";
				$evSPubTime="";
				$strIsPubAttr='disabled="disabled';
			}
		
			$totalTicket=@countTicketP("",$evId,$studioIdT);

			$strDis="";
			$strDis1="";
			$ischkStype="";
			$ischkStype1="";
			$evScheduleTypeR1="";
			$evScheduleTypeR2="";

			if(isset($evScheduleType)) {
				if($evScheduleType==0) {
					$evScheduleTypeR1=0;
					$evScheduleTypeR2=1;
					$ischkStype='checked="checked"';
					$strDis="";
					$strDis1='disabled="disabled"';
				} if($evScheduleType==1) {
					$evScheduleTypeR1=0;
					$evScheduleTypeR2=1;
					$ischkStype1='checked="checked"';
					$strDis='disabled="disabled"';
					$strDis1="";
				}
			} else {
				$ischkStype="";
				$ischkStype1="";
				$strDis='disabled="disabled"';
			}
		} 
		else {

			$strDis='disabled="disabled"';
			$strDis1='disabled="disabled"';
			$ischkStype="";
			$ischkStype1="";
			$evScheduleTypeR1=0;
			$evScheduleTypeR2=1;
			$strIsClose="yellow-btn ";

			$strIsCloseAttr='';

			$strIsCloseVal="Close Registration";

			$evType=@fnCheckEmpty($ritems['evType']);
			$evTempName=@fnCheckEmpty($ritems['evTempName']);
			$evCalColor=@fnCheckEmpty($ritems['evCalColor']);
			$evCalLabel=@fnCheckEmpty($ritems['evCalLabel']);
			
			$slug_old="";
			

			$evScheduleType="";
			$evDate="";
			$evStTime="";
			$evEndTime="";
			$evLocationId="";
			$evAccessCode="";
			$evSPubDate="";
			$evSPubTime="";
			$evRegEnd="";
			$isCancel1="";
			$isPublish=0;
			$evImage=@fnCheckEmpty($ritems['evTempImage']);
			$evCalColor=@fnCheckEmpty($ritems['evCalColor']);
			$evCalLabel=@fnCheckEmpty($ritems['evCalLabel']);
			$evTitle=@fnCheckEmpty($ritems['evTempTitle']);
			$evFullDescription=@fnCheckEmpty($ritems['evTempDescription']);
			$evScheduleHrs=$ritems['evScheduleHrs'];
			$evNoOfTickets=@fnCheckEmpty($ritems['evNoOfTickets']);
			$evBookTicket=0;
			$evStatus=@fnCheckEmpty($ritems['evisStatus']);
			$evTypeStatus=@fnCheckEmpty($ritems['evTypeStatus']);
			

			$totalTicket=@countTicketP(@fnCheckEmpty($ritems['id']),"","");
			
			//$totalTicket=@countTicketPA($_REQUEST["tempid"],1);
		}
		
		$params1 = array('');
		if($u==1) {
			if(isset($_SESSION["urole"]) && ($_SESSION["urole"]==1|| $_SESSION["urole"]==2) ) {
				$result1 = $db->rawQuery("SELECT * FROM bb_evticketpackage WHERE  eventID='".$evId."' and FIND_IN_SET(".$studioIdT.",`studioId`) order by id asc ", $params1);
			} else {
				$result1 = $db->rawQuery("SELECT * FROM bb_evticketpackage WHERE  eventID='".$evId."' and studioId IN(".$studioIdT.") order by id asc ", $params1);
			}
		} else { 
			if(isset($_SESSION["stId"]) && !empty($_SESSION["stId"])) {
				$locId=$_SESSION["stId"];
			} else {
				$locId=$_SESSION["studioIds"];
			}
			//$stwhr = " and studioId IN(".$locId.") ";
			$stwhr = " and studioId= '' ";
			$sql55 = "SELECT * FROM bb_evticketpackage WHERE evtempID='".$_REQUEST["tempid"]."' and eventID=0 $stwhr order by id asc ";
			$result1 = $db->rawQuery($sql55, $params1);
			//echo "<pre>sessiopn===";print_r($_SESSION);
			//echo "<pre>result1===";print_r($result1);die;
		}
		//$ritems1 = (array)$result1;
		
	}
 
	$locationIds=$_SESSION["studioIds"];
	$strHtml.='<link type="text/css" rel="stylesheet" href="'.base_url_css.'bootstrap-3.3.5.min.css"><script type="text/javascript" src="'.base_url_js.'bootstrap-3.3.5.min.js" /></script><script type="text/javascript" src="'.base_url_js.'jquery-validate.bootstrap-tooltip.js" /></script>';

	if(isset($_SESSION["studioIds"]) && $_SESSION["studioIds"]!="") {
		$locationIds=$_SESSION["studioIds"];
		if(isset($_REQUEST["evid"]) && $_REQUEST["evid"]!="") {
			$stwhr = " and FIND_IN_SET(".$evLocationId.",`id`) ";
		} else {
			if(isset($_SESSION["stId"]) && !empty($_SESSION["stId"])) {
				$locId=$_SESSION["stId"];
			} else {
				$locId=$_SESSION["studioIds"];
			}
			$stwhr = " and id IN(".$locId.") ";
		}
	} else {
		$locationIds=$_SESSION["studid"];
		if(isset($_REQUEST["evid"]) && $_REQUEST["evid"]!="") {
			$stwhr = " and FIND_IN_SET(".$evLocationId.",`id`) ";
		} else {
			$stwhr = " and id IN(".$locationIds.") ";
		}
		//$stwhr = " and FIND_IN_SET(".$locationIds.",`id`) ";
	}


	$strHtml.='<input type="hidden" name="evTempName" id="evTempName" value="'.checkSpace($evTempName).'" />
	<input type="hidden" name="evType" id="evType" value="'.$evType.'" />
	<input type="hidden" name="slug_old" id="slug_old" value="'.$slug_old.'" />';
	
	if(!isset($_REQUEST["evid"]) && $_REQUEST["evid"]=="") {
	$strHtml.='<input type="hidden" name="evCalenderColor" id="evCalenderColor" value="'.$evCalColor.'" />';
	
	}
	
	$strHtml.='<div class="template-info">
	<div class="template-info-left">
	<div class="input-row">
	<div class="floatingBox eventdate-dim">
	<input class="eventdate input_date" type="text" name="evDate" id="evDate" placeholder="" value="'.$evDate.'" />
	<label class="floating-lab">MM/DD/YYYY</label>
	</div>
	<div class="floatingBox eventdate-dim">
	<input class="eventdate timepicker" type="text" name="evStTime" id="evStTime" placeholder="" value="'.$evStTime.'"   readonly="readonly"/>
	<label class="floating-lab">Start Time</label>
	</div>
	<div class="floatingBox eventdate-dim">
	<input class="eventdate timepicker" type="text" name="evEndTime" id="evEndTime" placeholder="" value="'.$evEndTime.'"  readonly="readonly" />
	<label class="floating-lab">End Time</label>
	</div>';

	$strHtml.='<select class="evlocation" id="evLocationId" name="evLocationId">
	<option value="">Location</option>'.fnMDropDownListST("bb_studiolocation","id","stName","","(stStatus=4 or stStatus=3 or stStatus=1 or stStatus=2) $stwhr",$evLocationId).'</select>
	</div>';

	$strHtml.='<div class="input-row">
	<div class="floatingBox">
	<input class="textfull remspace" type="text" name="evTitle" id="evTitle" placeholder="" value="'.checkSpace($evTitle).'" />
	<label class="floating-lab">Event Title</label>
	</div>
	</div>
	<div class="input-row">
	<div class="floatingBox">
	<textarea class="textareafull remspace" name="evFullDescription" id="evFullDescription" placeholder="">'. checkSpace($evFullDescription).'</textarea>
	<label class="floating-lab">Event Full Description</label>
	</div>
	</div>
	<div class="input-row">';
//	if(isset($_REQUEST["evid"]) && $_REQUEST["evid"]!="") {
	$strHtml.='<select class="clendar-color" name="evCalenderColor" id="evCalenderColor">
	<option value="">Calendar Color</option>
'.fnDropDownListC("bb_calcolor","caCode","caTitle",$evCalColor,"").'
</select>';
//}
	$strHtml.='<div class="floatingBox clendar-lavel-dim">
<input class="clendar-lavel remspace" type="text" name="evCalenderColorLabel" id="evCalenderColorLabel" placeholder="" value="'.checkSpace($evCalLabel).'" />
<label class="floating-lab">Calendar Label</label>
</div>
	</div>
	
	<div class="input-row">
	<div class="floatingBox">
	<textarea  style="height:70px;" class="textareafull remspace" name="evDesAddition" id="evDesAddition" placeholder="">'. checkSpace($evDesAddition).'</textarea>
	<label class="floating-lab">Description Additions</label>
	</div>
	</div>';
	if($evType==2) {
	$strHtml.='<div class="input-row"><span>Access Code</span> <input class="accesscode remspace" type="text" id="evAccessCode" name="evAccessCode" value="'.checkSpace($evAccessCode).'"  /></div>';
	}
	$strHtml.='</div>';
	

	
 $s3 = new S3(awsAccessKey, awsSecretKey);  	
	
	$filePathName = "uploads/events/".$evImage;
     $info = $s3->getObjectInfo(bucketName, $filePathName);


	
	if(isset($info) && !empty($info)){
		$imageurl=s3_url_uimages."uploads/events/".$evImage;
		$imageName=$evImage;
		$strHtml.='<input type="hidden" name="isfile" id="isfile" value="1">';
		$isVimage=1;
	}  else  {
		$imageurl=base_url_site."images/placeholder.jpg";
		$strHtml.='<input type="hidden" name="isfile" id="isfile" value="0">';
		$imageName="";
		$isVimage=0;
	}

	if(isset($evSPubDate) && $evSPubDate!="0000-00-00" && $evSPubDate!="") {
		$evSPubDate=fnDisplayDate($evSPubDate);
	} else {
		$evSPubDate="";
	}	
			 
	$strHtml.='<input type="hidden" name="flpup" value="'.$imageName.'" />
	<div class="gallery-img"><label id="upload-label"><i class="fa fa-plus-circle"></i><input onchange="readURL(this);" name="evImage" id="img-upload" type="file"/></label><img id="blah" src="'.$imageurl.'" alt="" style="height:300px;" /></div>
	</div>

	<div class="schedule-section">
	<h2 class="section-heading">Schedule</h2>
	<div class="schedule-row-top"><span style="margin-right:10px;">Publish</span>
	<div class="floatingBox pushdate-dim">
	<input class="pushdate" type="hidden" name="evScheduleType" id="evScheduleType" value="1" />
	<input class="pushdate input_date" type="text" name="evSPubDate" id="evSPubDate" '.$strIsPubAttr.' placeholder=""   value="'.$evSPubDate.'"  />
	<label class="floating-lab">MM/DD/YYYY</label>
	</div>
	
	<input class="pushdate timepicker" name="evSPubTime" id="evSPubTime"  type="text" placeholder="" value="'.$evSPubTime.'" '.$strIsPubAttr.'"  /> </div>

	<div class="schedule-row"><span>Registration End</span><input class="tempname remspace number-only" name="evScheduleHrs" id="evScheduleHrs" type="text" value="'.$evScheduleHrs.'" /> <span>Hours before event</span> <button type="button" class="'.$strIsClose.' cose-reg" '.$strIsCloseAttr.' id="btnCReg" name="btnCReg" data-status="'.$clsestatus.'" >'.$strIsCloseVal.'</button>&nbsp;&nbsp;<span id="cmsg" style="color: #6fcf00;font-weight: bold;"></span></div>
	</div>

	<div class="seats-section">
	<h2 class="section-heading">Seats & Ticket Packages</h2>
	<div class="input-row">
	<div class="floatingBox">
	<input  type="hidden" name="evBookedTickets" id="evBookedTickets" value="'.$evBookTicket.'" />
	<input style="width:300px!important;" class="numseats remspace number-only" type="text" placeholder="" name="evNoOfTickets" id="evNoOfTickets" value="'.$evNoOfTickets.'" />
	<label class="floating-lab">Number of seats</label>
	</div>
	</div>

	<div id="customFields">
	<input type="hidden" id="totalTicketPackId" name="totalTicketPackId" value="'.$totalTicket.'" />
	<input type="hidden" id="rdids" name="rdids" value="" />';

	//echo "<pre>result1===";print_r($result1);die;
	$h=1;
	
	if(isset($result1) && !empty($result1)) {
	foreach($result1 as $k=>$tval) {

	if($tval['isManual']==1) $ischk='checked="checked"'; else $ischk='';

	$strHtml.='<div class="ticket-wrapper">
	<div class="ticket-left">';
	 if($h!=1) {
	$strHtml.='<a href="javascript:void(0);" class="remCF" data-val="'.$tval['id'].'"><img src="'.base_url_site.'images/cross-icon.png" /></a>';
	 }
	$strHtml.='<input type="hidden" name="ticketId" class="tcount" id="ticketId" value="'.$totalTicket.'">
	<input type="hidden" name="ticketIdT[]" id="ticketIdT" value="'.$tval['id'].'">
	</div>
	<div class="ticket-right" id="t_'.$h.'">
	<div class="ticket-right-row">
	<div class="floatingBox ticket-name-dim">
	<input class="ticket-name remspace" type="text" placeholder="" name="evTicketName[]" id="evTicketName'.$h.'"  value="'. @fnCheckEmpty(checkSpace($tval['evTicketName'])).'" />
	<label class="floating-lab">Ticket Name</label>
	</div>
	<div class="floatingBox ticket-price-dim">
	<input class="ticket-price-small2 remspace number-only" type="hidden" placeholder="Seat Value" name="evTicketSeatVal[]" id="evTicketSeatVal'.$h.'" value="'.@fnCheckEmpty($tval['evSeatValue']).'" readonly="readonly" />
	<input class="thprice" type="hidden" placeholder="" name="evHTicketPrice[]" id="evHTicketPrice'.$h.'" value="'.number_format($tval['evTicketPrice'],0).'" />
	<input class="ticket-price tprice number-only numeric" type="text" placeholder="" name="evTicketPrice[]" id="evTicketPrice'.$h.'" value="'.number_format($tval['evTicketPrice'],0).'" />
	<label class="floating-lab">Price</label>
	</div>
	</div>
	<div class="ticket-right-row">
	<div class="floatingBox ticket-name-small-dim">
	<input class="ticket-name-small remspace" type="text" placeholder="" name="evTicketButtonLabel[]" id="evTicketButtonLabel'.$h.'" value="'.@fnCheckEmpty(checkSpace($tval['evTicketButtonLabel'])).'" />
	<label class="floating-lab">Add button label (eg “bring a friend for”) (limit space)</label>
	</div>
	<div class="floatingBox ticket-price-small-dim">
	<input class="ticket-price-small number-only" type="text" id="ticketLimit'.$h.'" name="ticketLimit[]" '.$isdis.' placeholder="" value="'.@fnCheckEmpty($tval['evTicketLImit']).'" />
	<label class="floating-lab">Ticket Limit</label>
	</div>
	</div>
	<div id="customSFields">';

	if($tval['isManual']==0) {
		$strHtml.='<div class="ticket-right-row" id="to_'.$h.'" >
		<div class="append-row">
		<div class="append-row-close">';
		if($h!=1) {
		//$strHtml.='<a href="javascript:void(0);" class="remSCF"><i class="fa fa-times-circle" style="font-size:19px;color:red"></i></a>';
		}
		if($tval['evTicketOptionId']!="") {
		$strHtml.='<style> .proOptions .add-cat {display:inline-block;}</style>';
		}

		$strHtml.='</div>
		<div class="append-row-element">
		<div class="proOptions">
		<select class="template_pe_dropdown proOpt" id="lstPProjectOptions" name="lstPProjectOptionsT[]">
		<option value="">Options</option>
		'.fnDropDownListProOptions("bb_proclassoptions","id","opName","opPrice",@fnCheckEmpty($tval['evTicketOptionId']),"").'
		</select>';

		$cattemp="";
		$typetemp="";
		$protemp="";
		$cattempIds="";
		$typetempIds="";
		$protempIds="";


		if(isset($tval['evTicketCatIds']) && !empty($tval['evTicketCatIds'])) {
			$cattempIds=$tval['evTicketCatIds'];
			$oids=explode(",",$tval['evTicketCatIds']);
			foreach($oids as $co=>$oval) {
				$cattemp.='<div class="template_pe_dropdown-box"><input placeholder="'.getNamebyPara("bb_procategory","proCatName","id",$oval).'" type="text" readonly><a href="javascript:void(0);" class="rcat" id="'.$oval.'" data-id="1"><i class="fa fa-times-circle" style="font-size:19px;color:red"></i></a></div>';
			}
		}

		if(isset($tval['evTicketTypeIds']) && !empty($tval['evTicketTypeIds'])) {
			$typetempIds=$tval['evTicketTypeIds'];
			$oids=explode(",",$tval['evTicketTypeIds']);
			foreach($oids as $co=>$oval) {
				$typetemp.='<div class="template_pe_dropdown-box"><input placeholder="'.getNamebyPara("bb_protype","proTypeName","id",$oval).'" type="text" readonly><a href="javascript:void(0);" class="rcat" id="'.$oval.'" data-id="2"><i class="fa fa-times-circle" style="font-size:19px;color:red"></i></a></div>';
			}
		}

		if(isset($tval['evTicketProjectIds']) && !empty($tval['evTicketProjectIds'])) {
			$protempIds=$tval['evTicketProjectIds'];
			$oids=explode(",",$tval['evTicketProjectIds']);
			foreach($oids as $co=>$oval) {
				$protemp.='<div class="template_pe_dropdown-box"><input placeholder="'.getNamebyPara("bb_project","proName","id",$oval).'" type="text" readonly><a href="javascript:void(0);" class="rcat" id="'.$oval.'" data-id="3"><i class="fa fa-times-circle" style="font-size:19px;color:red"></i></a></div>';
			}
		}
		

if($tval['evTicketCatIds']=="0"){
$sty="style=display:none";
$sty1="style=display:inline-block";
$sty5="style=display:none";
$strHtml.='<div class="template_pe_dropdown-box"><input placeholder="Catch All" type="text" readonly><a href="javascript:void(0);" class="rcat" id="0" data-id="1"><i class="fa fa-times-circle" style="font-size:19px;color:red"></i></a></div>';
$cattempIds=0;
}  if($tval['evTicketCatIds']==""){
$sty="style=display:block";
$sty1="style=display:none";
$sty5="style=display:inline-block";
echo $cattemp; 
} if($tval['evTicketCatIds']!="0"){
$sty="style=display:none";
$sty5="style=display:inline-block";
$sty1="style=display:inline-block";
$strHtml.=$cattemp; 
if($cattemp=="") {
$sty5="style=display:inline-block";
$sty1="style=display:none";
$sty2="style=display:none";
}
}
if($tval['evTicketCatIds']==""){
$sty="style=display:block";
}


		$strHtml.='<a href="javascript:void(0);" class="add-cat padd crc" id="1" '.$sty5.'><span class="alert_msg" '.$sty.'>Type and Project will show after selecting category</span><i class="fa fa-plus-circle"></i><span>Add Category<input type="hidden" id="catids" name="catidsT[]" value="'.$cattempIds.'" /></span></a>';


if($tval['evTicketTypeIds']=="0"){
$sty2="style=display:inline-block";
$sty1="style=display:none";
$strHtml.='<div class="template_pe_dropdown-box"><input placeholder="Catch All" type="text" readonly><a href="javascript:void(0);" class="rcat" id="0" data-id="2"><i class="fa fa-times-circle" style="font-size:19px;color:red"></i></a></div>';
$typetempIds=0;
} else if($tval['evTicketTypeIds']==""){
//$sty1="style=display:none";
$sty2="style=display:none";
$strHtml.=$typetemp; 
}  else if($tval['evTicketTypeIds']!="0"){
//$sty1="style=display:none";
$sty2="style=display:inline-block";
$strHtml.=$typetemp; 
} else {
$strHtml.=$typetemp; 
}

$strHtml.='<a href="javascript:void(0);" class="add-cat padd rc" id="2" '.$sty1.'><i class="fa fa-plus-circle"></i><span>Add Type<input type="hidden" id="typeids" name="typeidsT[]" value="'.$typetempIds.'" /></span></a>
		'.$protemp.'
		<a href="javascript:void(0);" class="add-cat padd rc pc" id="3" '.$sty2.'><i class="fa fa-plus-circle"></i><span>Add Project<input type="hidden" id="projectids" name="projectidsT[]" value="'.$protempIds.'" /></span></a>
		</div>


		</div>
		</div>
		</div>';
	} else {
		$strHtml.='<div class="ticket-right-row" id="to_'.$h.'" style="display:none;">
		<div class="append-row">
		<div class="append-row-close"></div>
		<div class="append-row-element">
		<div class="proOptions">
		<select class="template_pe_dropdown proOpt" id="lstPProjectOptions'.$h.'" name="lstPProjectOptionsT[]">
		<option value="">Options</option>
		'.fnDropDownListProOptions("bb_proclassoptions","id","opName","opPrice","","").'
		</select>
		<a href="javascript:void(0);" class="add-cat padd crc" id="1"><span class="alert_msg">Type and Project will show after selecting category</span><i class="fa fa-plus-circle"></i><span>Add Category<input type="hidden" id="catids" name="catidsT'.$h.'" value="" /></span></a>
		<a href="javascript:void(0);" class="add-cat padd rc" id="2"><i class="fa fa-plus-circle"></i><span>Add Type<input type="hidden" id="typeids" name="typeidsT[]" value="" /></span></a>
		<a href="javascript:void(0);" class="add-cat padd rc" id="3"><i class="fa fa-plus-circle"></i><span>Add Project<input type="hidden" id="projectids" name="projectidsT[]" value="" /></span></a>
		</div>


		</div>
		</div>


		</div>';
	}
	$strHtml.='</div>

	</div>


	</div><input type="hidden" id="totalTicketPackIdE" name="totalTicketPackIdE" value="'.$h.'" />';

	$h++;
}
 } else { 
$strHtml.='<div class="ticket-wrapper">
<div class="ticket-left"><input type="hidden" class="tcount" name="ticketId" id="ticketId" value="1"></div>
<div class="ticket-right" id="t_1">
<div class="ticket-right-row"><div class="floatingBox ticket-name-dim"><input class="ticket-name remspace" type="text" placeholder="" name="evTicketName[]" id="evTicketName1" /><label class="floating-lab">Ticket Name</label></div><div class="floatingBox ticket-price-dim"><input class="ticket-price-small2 remspace number-only" type="hidden" placeholder="Seat Value" name="evTicketSeatVal[]" id="evTicketSeatVal1" value="1" readonly="readonly" /><input class="ticket-price tprice number-only numeric" type="text" placeholder="" name="evTicketPrice[]" id="evTicketPrice1" /><input class="thprice" type="hidden" placeholder="" name="evHTicketPrice[]" id="evHTicketPrice1" /><label class="floating-lab">Price</label></div></div>
<div class="ticket-right-row"><div class="floatingBox ticket-name-small-dim"><input class="ticket-name-small remspace" type="text" placeholder="" name="evTicketButtonLabel[]" id="evTicketButtonLabel1" /><label class="floating-lab">Add button label (eg "bring a friend for") (limit space)</label></div><div class="floatingBox ticket-price-small-dim"><input class="ticket-price-small number-only" type="text" id="ticketLimit1" name="ticketLimit[]" placeholder="" value="" /><label class="floating-lab">Ticket Limit</label></div></div>
<div id="customSFields">
<div class="ticket-right-row" id="to_1">
<div class="append-row">
<div class="append-row-close"></div>
<div class="append-row-element">
<div class="proOptions">
<select class="template_pe_dropdown proOpt" id="lstPProjectOptions1" name="lstPProjectOptionsT[]">
<option value="">Options</option>
'.fnDropDownListProOptions("bb_proclassoptions","id","opName","opPrice","","").'
</select>
<a href="javascript:void(0);" class="add-cat padd crc" id="1"><span class="alert_msg">Type and Project will show after selecting category</span><i class="fa fa-plus-circle"></i><span>Add Category<input type="hidden" id="catids" name="catidsT[]" value="" /></span></a>
<a href="javascript:void(0);" class="add-cat padd rc" id="2"><i class="fa fa-plus-circle"></i><span>Add Type<input type="hidden" id="typeids" name="typeidsT[]" value="" /></span></a>
<a href="javascript:void(0);" class="add-cat padd rc" id="3"><i class="fa fa-plus-circle"></i><span>Add Project<input type="hidden" id="projectids" name="projectidsT[]" value="" /></span></a>
</div>


</div>
</div>


</div>
</div>
<!--<div class="insert-row"><a href="javascript:void(0);" class="addSCF"><i class="fa fa-plus-circle"></i></a></div>-->
</div>


</div>';
 }
$strHtml.='</div>';
if($h<=4) {
$strHtml.='<div class="insert-ticket"><a href="javascript:void(0);" class="addCF"><i class="fa fa-plus-circle"></i></a></div>';
}

$strHtml.='</div><input type="hidden" name="isVimage" id="isVimage" value="'.$isVimage.'" />
<script>
 function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            (charCode != 45 || $(element).val().indexOf("-") != -1) &&    
            (charCode != 46 || $(element).val().indexOf(".") != -1) &&     
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    } 
	var specialKeys = new Array();
        specialKeys.push(8); //Backspace
		
		
		
	
		
		
$("document").ready(function(){
	
	/*Input field take only numaric and none decimal value*/
	

	
/*
$(".number-only").keypress(function (event) {
return isNumber(event, this)
});
*/

$(".numeric").bind("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
               // $(".error").css("display", ret ? "none" : "inline");
                return ret;
            });
            $(".numeric").bind("paste", function (e) {
                return false;
            });
            $(".numeric").bind("drop", function (e) {
                return false;
            });
';

if(intval($evBookTicket)>0){
$strHtml.=' $("#evScheduleHrs").attr("readonly","readonly"); ';
$strHtml.=' $("#evDate").attr("readonly","readonly"); ';
$strHtml.=' $("#evStTime").attr("readonly","readonly"); ';
$strHtml.=' $("#evSPubDate").attr("readonly","readonly"); ';
$strHtml.=' $("#evSPubTime").attr("readonly","readonly"); ';

}
if(intval($evBookTicket)==0){
$strHtml.=' 







';
}
$strHtml.='

});
</script>
';

$response['eventhtml']=$strHtml;
}
echo json_encode($response);
exit;