<?php
/* echo "<pre>";
 print_r($_POST);
 die; */
include("../includes/functions.php");
global $db;
$options=[];
$response = array();
$userId     = isset($_POST['userId'])?$_POST['userId']:"";
$proName     = isset($_POST['proName'])?StringInputCleaner($_POST['proName']):"";
$proTypeId     = isset($_POST['proTypeId'])?$_POST['proTypeId']:"";
$proSku     = isset($_POST['proSku'])?$_POST['proSku']:"";
$proClassId     = isset($_POST['proClassId'])?$_POST['proClassId']:"";
$proDetailedDesc = isset($_POST['proDetailedDesc'])?StringInputCleaner($_POST['proDetailedDesc']):"";
$proPrice = isset($_POST['proPrice'])?$_POST['proPrice']:"";
$proPrice1 = str_replace("$", "", $proPrice);
$proPriCatId = isset($_POST['proPriCatId'])?$_POST['proPriCatId']:"";
$proAltCat = isset($_POST['proAltCat'])?$_POST['proAltCat']:"";

//If schedule date is optional then 
if($_POST['proSchStDate'] == ''){ 
	$proSchDtOptional = 1;
} else {
	$proSchDtOptional = 0;
}

if($_POST['proSchStDate'] != ''){ 
	$proSchStDateVal = date('Y-m-d', strtotime($_POST['proSchStDate']));  
} else { 
	//$proSchStDateVal = '';
	$proSchStDateVal = date('Y-m-d');  
}

if($_POST['proSchStTime'] != ''){
	$proSchStTime = $_POST['proSchStTime'];
} else {
	//$proSchStTime = '';
	$proSchStTime = date('h:i a');
}
if($proSchStDateVal == '' && $proSchStTime == ''){
	$dtFinal = '';
} else {
	$dt = $proSchStDateVal.''.$proSchStTime;
	$dtFinal = date( 'Y-m-d H:i:s', strtotime($dt) );
}

//pr($_POST);
//pr($_FILES);
//die;


$actionType = $_POST['actionType'];
if($actionType == 'addAction'){
	if($proSchDtOptional ==1){  //If Project schedule is optional then it will saved as draft
		$status = 2; 
		$actionvar = "Project drafted";
	} else {
		if(strtotime($dtFinal)<= strtotime("now")){
			$actionvar = "Project published";
			//$status = 0; //published
		} else {
			$actionvar = "Project scheduled";
			//$status = 2; //scheduled
		}
		$status = 0;
	}
}
 
if($actionType == 'draftAction'){
	$actionvar = "Project drafted";
	$status = 2; //draft
}
if($actionType == 'previewAction'){
	$actionvar = "Project previewed";
	$status = 0; //published or scheduled
}

if($_POST['proFpPrintList'] != ''){
  $proFpPrintList = implode(',',$_POST['proFpPrintList']);
} else {
  $proFpPrintList = '';
}

$proFpPrintCustom = isset($_POST['proFpPrintCustom'])?StringInputCleaner($_POST['proFpPrintCustom']):"";
$proSpecWi = isset($_POST['proSpecWi'])?IntegerInputCleaner($_POST['proSpecWi']):"";
$proSpecHi = isset($_POST['proSpecHi'])?IntegerInputCleaner($_POST['proSpecHi']):"";
$proSpecDe = isset($_POST['proSpecDe'])?IntegerInputCleaner($_POST['proSpecDe']):"";
$proSpecWe = isset($_POST['proSpecWe'])?IntegerInputCleaner($_POST['proSpecWe']):"";
$data = array (
	'userId' => $userId,
	'proName' => $db->escape($proName),
	'proTypeId' => $proTypeId,
	'proSku' => $proSku,
	'proClassId' => $proClassId,
	'proDetailedDesc' => $db->escape($proDetailedDesc),
	'proPrice' =>  $proPrice1,
	'proPriCatId' => $proPriCatId,
	'proAltCat' => $proAltCat,
	'proSchType' => $_POST['proSchType'],
	'proSchStDate' => $proSchStDateVal,
	'proSchStTime' => $proSchStTime,
	'proSchStDtTime' => $dtFinal,
	'proSchDtOptional' => $proSchDtOptional,
	//'proGalImg' =>$_FILES["proGalImg"]["name"][0],
	'proFpPrintList' => $proFpPrintList,
	'proFpPrintCustom' => $db->escape($proFpPrintCustom),
	'proSpecWi' =>  $db->escape($proSpecWi),
	'proSpecHi' =>  $db->escape($proSpecHi),
	'proSpecDe' =>  $db->escape($proSpecDe),
	'proSpecWe' =>  $db->escape($proSpecWe),
	'status' => $status,
	'isDeleted' => 0,
	'createdDate' => date('Y-m-d H:i:s'),
	'updatedDate' => date('Y-m-d H:i:s')
);

$last_insert_id = $db->insert("bb_project",$data);
if($last_insert_id) {
	//pr($_POST);die;
	if(!empty($_POST['proOptionVal'][0]) && is_array($_POST['proOptionVal'])) {
	    $count_check=0;
		 foreach($_POST['proOptionVal'] as $value){
			//echo "value is".$value;
			 
			 $data1 = array (
					'proUserId' => $_POST['userId'],
					'proId' => $last_insert_id,
					'proOptionVal' => $value,
				    'proLabel' => $db->escape(StringInputCleaner($_POST['proLabel'][$count_check])),
					'proHoverTxt' => $db->escape(StringInputCleaner($_POST['proHoverTxt'][$count_check])),
					
				);
				if(!empty($_POST['proOptVal_'.$count_check])){
					$data1['proOptVal']=$_POST['proOptVal_'.$count_check][0];
					
				}
				if(!empty($_POST['description_'.$count_check])){
					$data1['description']=$_POST['description_'.$count_check][0];
				}
				if(!empty($_POST['min_'.$count_check])){
					$data1['min']=$_POST['min_'.$count_check][0];
					$data1['max']=$_POST['max_'.$count_check][0];
				}
				///pr($data1);
				$ins1 = $db->insert("bb_propersoptions",$data1);
				if(!empty($_POST['option_per_'.$count_check])){
						
					
				foreach($_POST['option_per_'.$count_check] as $value_option_per){
						$data21 = array (
						'propersoptions_id' =>$ins1,
						'proId' => $last_insert_id,
						'proOptionVal' => $value,
						'option_value' => $db->escape(StringInputCleaner($value_option_per))
						);
						$last_insert_id_option_add =$db->insert("bb_prooptionadd",$data21);
					} 
				}  
			 $count_check++;
		 }
		
	} 
	
	if(!empty($_POST['specOptionVal'][0]) && is_array($_POST['specOptionVal'])) {
		for($i=0;$i<count($_POST['specOptionVal']); $i++) {
			$data3 = array (
				'userId' => $_POST['userId'],
				'proId' => $last_insert_id,
				'specOptionVal' => $_POST['specOptionVal'][$i],
				'specType' => $_POST['specType'][$i],
				'specTypeVal' => $_POST['specTypeVal'][$i]
			);
			$db->insert("bb_prospecifications",$data3);
		}
	}
	
	//save record of Restrication to table bb_prorestrictions
	$res_option_count=0;
		
	if(!empty($_POST['resOptionVal'][0]) && is_array($_POST['resOptionVal'])) {
		
		foreach($_POST['resOptionVal'] as $value){
			
			$data3 = array (
				//'userId' => $_POST['userId'],
				'proId' => $last_insert_id,
				'rsOptionVal' => $value,
			);
			//pr($_POST);
			if($value==1){
				$data3['rsCouponType']=$_POST['resCouponType'][0];
				
				if(!empty($_POST['code_select_val'])){
					$explodeCode = explode(",",$_POST['code_select_val']);
					$implodeCodeId= implode(",",$explodeCode);
					$data3['code_id']=$implodeCodeId;
					 
				}
				if(!empty($_POST['amount'][0])){
					$data3['couponAmount']=$_POST['amount'][0];
				}
				
				
			}
			
			else if($value==2){
				$explodeStudioId = explode(",",$_POST['studio_select_val']);
				$implodeStudioid = implode(",",$explodeStudioId);
				$data3['userId']=$implodeStudioid;
			}
			
		
			 $db->insert("bb_prorestrictions",$data3);
			//die;
			$res_option_count++;
		}
		
	}
	

	//code to save image in gallery for 2nd phase by pankaj
	// image code start//
	
	$i=0;
if(!empty($_FILES) && is_array($_FILES)){
		foreach ($_FILES as $filename) 
            {
				$fileName = $filename['name'];
				$fileTempName = $filename['tmp_name'];
				$fldname="uploads/projects/";
				$resizefldname="700X600/";
				$flg="";
				$md5filename="cid!".md5(time());
				$type = strtolower(substr(strrchr($fileName,"."),1));
				if($type == 'jpeg') $type = 'jpg';
				if($type == 'JPEG') $type = 'jpg';
				if($type == 'JPG') $type = 'jpg';
				if($type == 'GIF') $type = 'gif';
				if($type == 'BMP') $type = 'bmp';
				if($type == 'PNG') $type = 'png';
				$images_arr[$i]=$md5filename.".".$type;
				if($fileName==$firstimg)
				{
					$firstimg=$md5filename.".".$type;
				}
				
				upload_image_to_s3project($md5filename,$fileName,$fileTempName,$fldname,$resizefldname,$flg,@$_POST['flpup']);
			$dataGallery = array (
			'proUserId' => $_POST['userId'],
			'proImgName' =>$images_arr[$i],
			'proId' => $last_insert_id,
			
		);
		if($i=0){
			$dataGallery['proImgStatus']=1;
		}
		
		$db->insert("bb_progallery",$dataGallery);
				$i++;
            }
		}
	else{
			$firstimg = "";
		
		}

	//code_select_val
	if(!empty($_POST['code_select_val'])){
		$explodeCode = explode(" ",$_POST['code_select_val']);
		
		/* echo "<pre>";
		 print_r($explodeCode);
		 die; */
		 foreach($explodeCode as $valueCode){
			$dataInsertCode = array (
			'lgUserId' => $_POST['userId'],
			'lgProId' => $last_insert_id,
			'code_id' => $valueCode
			);
			//pr($dataInsertCode);
			$db->insert("bb_procode",$dataInsertCode);
			
		 }
		
	}
	
	
	//die;
	//Project History insertion
	$data4 = array (
		'lgUserId' => $_POST['userId'],
		'lgProId' => $last_insert_id,
		'lgDescription' => $actionvar,
		'lgDate' => date('Y-m-d'),
		'lgTime' => date('H:i:s')
	);
	$db->insert("bb_proactivitylog",$data4);
	

	//die;
	//end of code to save image in gallery
	
	//$response["msg"] = "Project data is successfully inserted"; //$db->getLastQuery();
	//$response["last_insert_id"] = $last_insert_id;
	if($actionType == 'addAction'){
		$_SESSION["msg"]= "Successfully saved project data";
		$response["status"] = 0;
	} 
	if($actionType == 'draftAction'){
		$_SESSION["msg"]= "Successfully saved draft project data";
		$response["status"] = 2;
	}
    if($actionType == 'previewAction'){
		$_SESSION["msg"]= "Successfully previewed project";
		$response["status"] = 5;
		$response["last_insert_id"] = $last_insert_id;
	}	
} 
else {
	//$response["msg"] = "Project data is not successfully inserted";//$db->getLastError();
	//$response["last_insert_id"] = '';
	if($actionType == 'addAction'){
		$_SESSION["msg"]= "Not successfully saved project data";
	} 
	if($actionType == 'draftAction'){
		$_SESSION["msg"]= "Not successfully saved draft project data";
	}
	if($actionType == 'previewAction'){
		$_SESSION["msg"]= "Not Successfully previewed project";
	}
}
echo json_encode($response);
die;
?>