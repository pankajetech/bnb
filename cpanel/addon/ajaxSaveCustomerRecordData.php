<?php
include("../includes/functions.php");
global $db;
$response = array();
/*echo '<pre>post=='; print_r($_POST);
die;*/

$pageType = $_POST['pageType'];
$actionType = $_POST['actionType'];
if($actionType == 'updateAction'){ $status = 0; } 

$userId     = isset($_POST['userId'])?$_POST['userId']:"";
$boFullName = isset($_POST['boFullName'])?$_POST['boFullName']:"";
$boProjects = isset($_POST['boProjects'])?$_POST['boProjects']:"";
$boPhone    = isset($_POST['boPhone'])?$_POST['boPhone']:"";
$boAddress  = isset($_POST['boAddress'])?$_POST['boAddress']:"";
$boAccommodations  = isset($_POST['boAccommodations'])?$_POST['boAccommodations']:"";
$boAccommodationsNotes  = isset($_POST['boAccommodationsNotes'])?$_POST['boAccommodationsNotes']:"";


$caddress = isset($_POST['caddress'])?StringInputCleaner($_POST['caddress']):"";
$ccity = isset($_POST['ccity'])?StringInputCleaner($_POST['ccity']):"";
$cstate = isset($_POST['cstate'])?StringInputCleaner($_POST['cstate']):"";
$czip = isset($_POST['czip'])?IntegerInputCleaner($_POST['czip']):"";

$data = array (
	'boFullName' => $boFullName,
	'boPhone' => $boPhone,
	'boAddress' => $boAddress,
	'boProjects' => $boProjects,
	'boAccommodations' =>  $boAccommodations,
	'boAccommodationsNotes' => $boAccommodationsNotes,
);

$bookingId = $_POST['bookingId']; //for update case
if($bookingId != ''){ //update case
	$db->where ('id', $bookingId);
	$update = $db->update("bb_booking",$data);
	
	//update bb_transaction details tbl with address info
	$data999 = array (
		'caddress' => $db->escape($caddress),
		'ccity' => $db->escape($ccity),
		'cstate' => $db->escape($cstate),
		'czip' => $db->escape($czip)
	);
	
	$db->where ('bookingId', $bookingId );
	$db->update("bb_transactiondetails",$data999);
	
	if($update) {
		if($actionType == 'updateAction'){
			$_SESSION["msg"]= "Successfully updated customer record data";
			$response["status"] = 0;
			$response["pageType"] = $pageType;
		} 
	} 
	else {
		if($actionType == 'updateAction'){
			$_SESSION["msg"]= "Not successfully updated customer record data";
			$response["pageType"] = $pageType;
		} 
	}
} 
echo json_encode($response);
die;
?>