<?php
include("../includes/functions.php");
global $db;

// storing  request (ie, get/post) global array to a variable  
$requestData= $_REQUEST;
//echo "<pre>REQUEST===";print_r($_REQUEST);die;

$columns = array( 
// datatable column index  => database column name
	0 => 'id',
	1 => 'firstName',
	2 => 'email', 
	3 => 'role',
	4 => 'isInvite',
	5 => 'isActive'
);


/*$requestData['order'][0]['column'] = 0;
$requestData['order'][0]['dir'] = "DESC";
$requestData['start']=0;
$requestData['length']= 50;*/

$stwhre="";
$orderby="";


//Getting total number records without any search
$params = array('');
$sql1= "SELECT * FROM `bb_users` where isDeleted=0 order by id desc";
$users = $db->rawQuery($sql1, $params);
//echo "<pre>users==";print_r($users);//die;
if(count($users)>0){
	$totalData = $db->count;
	//$totalData = count($users);
	$totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
} 


	
$sql = "SELECT * FROM `bb_users` where isDeleted=0	ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." 
		LIMIT ".$requestData['start']." ,".$requestData['length']." ";

$users = $db->rawQuery($sql, $params);
//echo "<pre>users1==";print_r($users);die;

$data = array();
if(count($users)>0){
$displayName="";
foreach ($users as $key=>$item) {
$nestedData=array(); 
if(isset($item['id'])) { $userid=$item['id']; } else { $userid=$item['id']; }

if($item['role']!="") {
		if(isset($_SESSION['urole']) && $_SESSION['urole']==$item['role'] && $item['role']==1) {
		$delstr='';
		} else {
		if(isset($item['studioLocation']) && $item['studioLocation']!="") {
		
		$studioLoc=substr(getStudioName($item['studioLocation']),0,-2);
		
		 $studioLoc=$studioLoc;
		  } else { $studioLoc=""; }
	
		$delstr='/ <a href="javascript:void(0);" id="'.$userid.'" class="del" data-studio="'.$studioLoc.'" data-Ids="'.$item['studioLocation'].'">Delete</a>';
	//users.php?delid='.$userid.'	
		}
} else {
$delstr='';
}
if($item['isInvite']=="1") {
$status='Yes';
} else {
$status='No';
}



if(isset($_SESSION['urole']) && $_SESSION['urole']== 1 ){
	$isactive='';
	if($item['isActive']==0) {
		$isactive='<a href="javascript:void(0);" class="clsLogin" id="'.$userid.'" data-status="1" alt="Click for In-Active" title="Click for In-Active"><img src="'.base_url_images.'green-icon.png" height="10" width="10"></a>';
	} else {
		$isactive='<a href="javascript:void(0);" class="clsLogin" id="'.$userid.'" data-status="0" alt="Click for Active" title="Click for Active"><img src="'.base_url_images.'red-icon.png" height="10" width="10"></a>';
	}
}

if(isset($_SESSION['urole']) && $_SESSION['urole']== 2 ){
	$isactive='';
	if($item['isActive']==0) {
		$isactive='<img src="'.base_url_images.'green-icon.png" height="10" width="10" title="Active">';
	} else {
		$isactive='<img src="'.base_url_images.'red-icon.png" height="10" width="10" title="In-Active">';
	}
}


if(isset($item['firstName']) && $item['firstName']!="") {
$displayName=$item['firstName'];
}
if(isset($item['lastName']) && $item['lastName']!="") {
$displayName=$item['lastName'];
}
if((isset($item['lastName']) && $item['lastName']!="") && isset($item['firstName']) && $item['firstName']!="") {
$displayName=$item['firstName']." ".$item['lastName'];
}


		//$i++;
		
		//$nestedData[] = $editstr.' '.$delstr.' '.$viewstr;
		$editstr = '<a href="addupduser.php?id='.$userid.'">Edit</a> ';
		
		if($_SESSION['urole'] == 1){
			$nestedData[] = $editstr.' '.$delstr;
		}
		else if($_SESSION['urole'] == 2){
			$nestedData[] = '<a href="viewuser.php?id='.$userid.'">View</a>';
			//$nestedData[] = $viewstr1;
		}
		$nestedData[] = $displayName;
		$nestedData[] = $item['email'];
		$nestedData[] = getNamebyPara("bb_userroles","roleName","id",$item['role']);
		$nestedData[] = $status;
		$nestedData[] = $isactive;
		//$nestedData[] = getNamebyPara("bb_studiostatus","stName","id",$item['stStatus']);
		
		$data[] = $nestedData;
	}
	
}

	//echo "<pre>data==";print_r($data);die;

	
$json_data = array(
	"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
	"recordsTotal"    => intval( $totalData ),  // total number of records
	"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
	"data"            => $data,   // total data array
);
echo json_encode($json_data);
?>	
