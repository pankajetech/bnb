<?php 
include('includes/header.php');
$menuClssProject = "downarrow";
$menuSbClssProject = "sub";
$menuSbClssStyleProject = "style='display:block;'";
$menuClssProjecttem5 = "active";

if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 

if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") {    
	$params = array('');
	$result = $db->rawQueryOne("SELECT * FROM bb_procategory WHERE id='".$_REQUEST["id"]."' ", $params);
	$ritems = (array)$result;
	if(!empty($ritems)) $ritems=$ritems; 
} else {
	$ritems=array();
	$ritems=$ritems;
}
?>

<!-- Header end-->


<!-- left nagivation start-->

<?php include('includes/left-sidebar.php');?>

<!-- left nagivation end-->
<!-- left content area start-->
<div class="loading" style="display:none;">Loading&#8230;</div>
<div class="right-wrapper">
<h1 class="pageheading"><?php if(!isset($_REQUEST["id"]))  echo "Add";  else  echo "Update" ?>  Category</h1>
<div class="form-area">
 <form role="form" method="post" action="" name="frmaddupCat" id="frmaddupCat">
	<input type="hidden" name="catid" id="catid" value="<?php echo $_REQUEST["id"]; ?>" />
	<div class="user-info">
	<div class="input-row">
	<div class="floatingBox"><input style="width:49% !important;" class="left49" type="text" id="proCatName" name="proCatName" placeholder="" value="<?php echo @fnCheckEmpty($ritems['proCatName']); ?>" /><label class="floating-lab">Category Name</label></div>
	</div>
    </div>
	<div class="bottom-btn"><input style="padding-left:30px; padding-right:30px;" class="yellow-btn" id="btnSave" name="btnSave" type="submit" value="Save" /></div>

</form>
</div>

</div>

<!-- left content area end-->
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>bootstrap-3.3.5.min.css">
<script type="text/javascript" src="<?php echo base_url_js?>bootstrap-3.3.5.min.js" /></script>
<script type="text/javascript" src="<?php echo base_url_js?>jquery-validate.bootstrap-tooltip.js" /></script>
<script>
<?php 
//Edit Case
if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "") { ?>
	var catid = $("#catid").val();
<?php 
} else { // Add Case ?>
	var catid = "";
<?php }?>


/*$.validator.addMethod("lettersonlys", function(value, element) {
  return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
}, "Please enter only letters"); */

$.validator.addMethod("noSpace", function(value, element) { 
  //return value.indexOf(" ") < 0 && value != "";
  return $.trim(value) != "";
}, "No space please and don't leave it empty");

$("#frmaddupCat").validate({
	onkeyup: false,
    onblur :true,
	rules: {
		proCatName: { 
			required :true, maxlength: 40,noSpace: true,
			remote: {
				url: '<?php echo base_url_site; ?>addon/ajaxValidateCatName',
				type: "post",
				data: { catid:catid}
			}
		},
	},
	messages: {
		proCatName: {required: "Please enter category name", remote: "category name is already exist"},
	},
	submitHandler: function() {
		$('.loading').show(); 
		$.ajax({
			url : '<?php echo base_url_site; ?>addon/ajaxSaveCategory', 
			type : 'POST',
			data: $('#frmaddupCat').serialize(),
			success : function(response) {
				var data = JSON.parse(response);
				//alert(data.result);
				if(data.result==1){
					$('.loading').hide(); 
					window.location.href="<?php echo base_url_site?>categorylist";
				}
			}
		});
	}
}); 
</script>
<?php
include('includes/footer.php');
?>