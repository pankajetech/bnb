<?php
include("../includes/functions.php");
global $db;

//echo "<pre>POST==";print_r($_POST);die;

$bookingId = urldecode($_POST['bookingId']);
$bookingId = trim($bookingId);
$response = array();

$data = array ( "isDeleted"=>0);
$db->where ('id',$bookingId);
if($db->update("bb_booking",$data)){  //restore booking
	$update =1;
} else {
	$update =2;
}

$bookingStatus = $_POST['bookingStatus'];

//0=>Completed,3=>Need Attention,4=>Change Project Pending
if($bookingStatus == 0 || $bookingStatus == 3 || $bookingStatus == 4){
	//Function is used to increase seats on restore booking
	increaseSeatsFromBookedEvent($bookingId);
}


//Update bb_transactiondetails status = deleted
$params3 = array('');
$result3 = $db->rawQuery("SELECT id,bookingId FROM bb_transactiondetails WHERE bookingId='".$bookingId."' ", $params3);
$ritems3 = (array)$result3;
if(!empty($ritems3)) {
	foreach($ritems3 as $key3=>$val3) {
		$data3 = array("isDeleted"=>0);
		$db->where ('id', $val3['id']);
		$db->where ('bookingId', $val3['bookingId']);
		$update3 = $db->update("bb_transactiondetails",$data3);
	}
}

if($update == 1){ 
	$response['msg'] = "Booking is successfully Restored";
	$response['status'] = 1;
} else {
	$response['msg'] = "Booking is not successfully Restored";
	$response['status'] = 2;
}
echo json_encode($response);
die;
?>