<?php
include("../includes/functions.php");
global $db;

if($_SERVER['REQUEST_METHOD'] == "POST")  {
	$response=array();
	/*print_r($_POST);
	die();*/

	$firstName     = isset($_POST['txtFirtName'])?$_POST['txtFirtName']:"";
	$lastName     = isset($_POST['txtLastName'])?$_POST['txtLastName']:"";
	$email     = isset($_POST['txtEmail'])?$_POST['txtEmail']:""; 
	//$pwd     = isset($_POST['txtPwd'])?$_POST['txtPwd']:""; 
	$userType     = isset($_POST['lstRole'])?$_POST['lstRole']:""; 
	$isInvite     = isset($_POST['isSend'])?$_POST['isSend']:"";  
	$stdlocationsg     = isset($_POST['lstLocations'])?$_POST['lstLocations']:"";  


	if(isset($userType) and $userType!="") {
		$userType=$userType;
	} else {
		if(isset($_SESSION["curRole"]) && !empty($_SESSION["curRole"])) {
			$userType=$_SESSION["curRole"];
		} else {
			$userType=$_SESSION['urole'];
		}
	}

	if(isset($stdlocationsg) and $stdlocationsg!="") {
		$stdlocationsg=$stdlocationsg;
	} else {
		$stdlocationsg=$_SESSION['studioIds'];
	}



	/*$lstLocations="";
	if(isset($_POST['lstLocations']) && !empty($_POST['lstLocations'])) {
	foreach($stdlocationsg as $kl=>$vall) { 
	$lstLocations.=$vall.",";
	}
	}
	if(isset($lstLocations)) {
	$stdlocations=substr($lstLocations,0,-1);
	} else {
	$stdlocations="";
	}*/
	$stdlocations=$stdlocationsg;
	if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") {

		$sslug1 = $db->rawQueryOne ("SELECT isInvite FROM bb_users where id='".$_REQUEST["id"]."' ", array(''));
		if(count($sslug1)>0) {
			$isInvite1 = $sslug1['isInvite'];
		} 

		if(isset($_POST["txtRstPwd"]) and $_POST["txtRstPwd"]!="") {
			$txtRstPwd     = isset($_POST['txtRstPwd'])?md5($_POST['txtRstPwd']):"";  
			$data = array ('firstName' => $firstName,'lastName' => $lastName,'email' => $email,'pwd' => $txtRstPwd,'role' => $userType,'studioLocation' => $stdlocations,'isInvite' => $isInvite,'ipaddr' => get_ip_address());
		} else {
			$data = array ('firstName' => $firstName,'lastName' => $lastName,'email' => $email,'role' => $userType,'studioLocation' => $stdlocations,'isInvite' => $isInvite,'ipaddr' => get_ip_address());
		}

		$db->where ('id', $_REQUEST["id"]);
		$update = $db->update("bb_users",$data);
		$_SESSION["msg"]="Successfully updated user info";

		//echo "SELECT isInvite FROM bb_users where id='".$_REQUEST["id"]"' ";


		if(isset($isInvite) && $isInvite==1 && $isInvite1==2) {
			$to = $email;
			//$to = "prabhat.thakur@effectualtech.com";
			$subject = 'Board & Brush Access Invitation';
			$headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
			//$headers .= "From:hello@boardandbrush.com \r\n";
			$headers .= "From: ".From_Name." <".From_Email."> \r\n";
			$header .= "Content-type: text/html\r\n";
			$invitationurl = base_url_site.'invitation?eid='.$_REQUEST["id"];
			$addURLS = base_url_site;
			
			$message = '
			<html>
			<head>
			<title>Board & Brush</title>
			<style>
				body{ background-color:#f1f1f1; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333; font-size:15px;}
				p,h1,h2,h3,h4,h5,h6{ margin:0; padding:0}
				.email-main{ max-width:680px; margin:auto;}
				.sizeX {font-size: 16px;}
			</style>
			</head>
			<body>
			<div class="email-main">
				<table width="98%" cellpadding="0" cellspacing="0" border="0" style="padding:8px 1% 8px 1%; background-color:#eaeaea; width:680px; max-width:680px;">
					<tr>
					<td align="center" style="background-color:#FFFFFF; padding-top:15px; padding-bottom:15px;"><img width="300" src="'.base_url_images.'boardandbrush-logo-email.png" /></td>
					</tr>

					<tr>
					<td align="center" style="background-color:#FFFFFF;"><img width="97%" src="'.base_url_images.'hr-line-email.jpg" /></td>
					</tr>

					<tr>
					<td style="background-color:#FFFFFF;" height="10"></td></tr>
					<tr>
				
					<tr>
						<td style="background-color:#FFFFFF;">
							<table style=" width:100%; padding-top:15px; padding-bottom:15px; padding-left:2%; padding-right:2%;">
								<tr><td style="color:#000;" valign="top" colspan="3">
								Hello '.$firstName.',<br><br>
								Welcome to the Board &amp; Brush Registration Portal. Please use the links and information below to access the site!
								<br><br>
						
								Your user name is: '.$email.'
								<br><b><a style="color:0000ee;" href="'.$invitationurl.'">Click here to set your password</a></b>
								<br><small><em>This link will expire in 48 hours</em></small>
								<br><br>
								Once you reset your password you will be directed to the login page. Before you login, be sure to bookmark the page!
								<br><br>
								If the link above did not work for you, copy and paste the following link into your browser:
								<b><a style="color:0000ee;" href="'.$invitationurl.'">'.$invitationurl.'</a></b>
								<br><br>
								For additional questions and support check out the <b><a style="color:0000ee;" href="https://studiosupport.boardandbrush.com">Studio Support</a></b> site.
								<br><br>
								</td></tr>
								
								<tr>
								<td style="color:#000;" colspan="3">
								The Board & Brush Team
								</td>
								</tr>
							</table>
						</td>
					</tr>
					</table>
					<table width="98%" cellpadding="0" cellspacing="0" border="0" style="padding:0 1%; margin-top:10px; margin-bottom:10px;">
					<tr>
					<td  style="color:#000;" align="center">Copyrighted Board & Brush, LLC. All Rights Reserved © '.date("Y").' </td>
					</tr>
				</table>
			</div>
			</body>
			</html>'; 
			
			mail($to, $subject, $message, $headers);
		}
	} 
	else {
		$data = array ('firstName' => $firstName,'lastName' => $lastName,'email' => $email,'role' => $userType,'studioLocation' => $stdlocations,'isInvite' => $isInvite,'isActive' => 0,'createdDate' => date("Y-m-d H:i:s"),'createdBy' => $_SESSION["usrid"],'ipaddr' => get_ip_address());
		$lastInsertedId = $db->insert("bb_users",$data);
		/* echo "Last executed query was ". $db->getLastQuery();
		   die();*/
		$_SESSION["msg"]="Successfully added user info";

		if(isset($_REQUEST["did"]) and $_REQUEST["did"]!="") {
			$dataD=array("isDeleted"=>1);
			$db->where('id',$_REQUEST["did"]);
			$db->update('bb_users',$dataD);
		}


		if(isset($isInvite) && $isInvite==1) {
			$to = $email;
			//$to = "prabhat.thakur@effectualtech.com";
			$subject = 'Board & Brush Access Invitation';
			$headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
			//$headers .= "From:hello@boardandbrush.com \r\n";
			$headers .= "From: ".From_Name." <".From_Email."> \r\n";
			$header .= "Content-type: text/html\r\n";
			$invitationurl = base_url_site.'invitation?eid='.$lastInsertedId;
			$addURLS = base_url_site;

			$message = '
			<html>
			<head>
			<title>Board & Brush</title>
			<style>
				body{ background-color:#f1f1f1; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333; font-size:15px;}
				p,h1,h2,h3,h4,h5,h6{ margin:0; padding:0}
				.email-main{ max-width:680px; margin:auto;}
				.sizeX {font-size: 16px;}
			</style>
			</head>
			<body>
			<div class="email-main">
				<table width="98%" cellpadding="0" cellspacing="0" border="0" style="padding:8px 1% 8px 1%; background-color:#eaeaea; width:680px; max-width:680px;">
					<tr>
					<td align="center" style="background-color:#FFFFFF; padding-top:15px; padding-bottom:15px;"><img width="300" src="'.base_url_images.'boardandbrush-logo-email.png" /></td>
					</tr>

					<tr>
					<td align="center" style="background-color:#FFFFFF;"><img width="97%" src="'.base_url_images.'hr-line-email.jpg" /></td>
					</tr>

					<tr>
					<td style="background-color:#FFFFFF;" height="10"></td></tr>
					<tr>
				
					<tr>
						<td style="background-color:#FFFFFF;">
							<table style=" width:100%; padding-top:15px; padding-bottom:15px; padding-left:2%; padding-right:2%;">
								<tr><td   style="color:#000;" valign="top" colspan="3">
								Hello '.$firstName.',<br><br>
								Welcome to the Board &amp; Brush Registration Portal. Please use the links and information below to access the site!
								<br><br>
						
								Your user name is: '.$email.'
								<br><b><a style="color:0000ee;" href="'.$invitationurl.'">Click here to set your password</a></b>
								<br><small><em>This link will expire in 48 hours</em></small>
								<br><br>
								Once you reset your password you will be directed to the login page. Before you login, be sure to bookmark the page!
								<br><br>
								If the link above did not work for you, copy and paste the following link into your browser:
								<b><a style="color:0000ee;" href="'.$invitationurl.'">'.$invitationurl.'</a></b>
								<br><br>
								For additional questions and support check out the <b><a style="color:0000ee;" href="https://studiosupport.boardandbrush.com">Studio Support</a></b> site.
								<br><br>
								</td></tr>
								
								<tr>
								<td   style="color:#000;" colspan="3">
								The Board & Brush Team
								</td>
								</tr>
							</table>
						</td>
					</tr>
					</table>
					<table width="98%" cellpadding="0" cellspacing="0" border="0" style="padding:0 1%; margin-top:10px; margin-bottom:10px;">
					<tr>
					<td   style="color:#000;" align="center">Copyrighted Board & Brush, LLC. All Rights Reserved © '.date("Y").' </td>
					</tr>
				</table>
			</div>
			</body>
			</html>'; 
			mail($to, $subject, $message, $headers);
		}
	}
	$response["result"]="1";
	echo json_encode($response); 	
	exit;
	// header('Location: '.base_url_site.'users.php');        
}