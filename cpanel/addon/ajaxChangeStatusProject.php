<?php
include("../includes/functions.php");
global $db;

//echo "<pre>==#####";print_r($_POST); 
//echo "<pre>==#####";print_r($_FILES); die;

$userId       = isset($_POST['userId'])?$_POST['userId']:"";
$proName      = isset($_POST['proName'])?StringInputCleaner($_POST['proName']):"";
$proTypeId    = isset($_POST['proTypeId'])?$_POST['proTypeId']:"";
$proClassId   = isset($_POST['proClassId'])?$_POST['proClassId']:"";
$proPrice     = isset($_POST['proPrice'])?$_POST['proPrice']:"";
$proPrice1    = str_replace("$", "", $proPrice);
$proPriCatId  = isset($_POST['proPriCatId'])?$_POST['proPriCatId']:"";

//If schedule date is optional then 
if($_POST['proSchStDate'] == ''){ 
	$proSchDtOptional = 1;
} else {
	$proSchDtOptional = 0;
}

if($_POST['proSchStDate'] != ''){
 $proSchStDateVal = date('Y-m-d', strtotime($_POST['proSchStDate']));
} else {
 //$proSchStDateVal = '';
 $proSchStDateVal = date('Y-m-d');  
}

if($_POST['proSchStTime'] != ''){
	$proSchStTime = $_POST['proSchStTime'];
} else {
	//$proSchStTime = '';
	$proSchStTime = date('h:i a');
}
if($proSchStDateVal == '' && $proSchStTime == ''){
	$dtFinal = '';
} else {
	$dt = $proSchStDateVal.''.$proSchStTime;
	$dtFinal = date( 'Y-m-d H:i:s', strtotime($dt) );
}


$actionType = $_POST['actionType'];
//echo $actionType."====".$proSchDtOptional;die;
if($actionType == 'enableAction'){
	if($proSchDtOptional ==1){ //If Project schedule is optional then it will saved as draft
		//$status = 2; 
		//$actionvar = "Project drafted";
		
		$status = 0; 
		$actionvar = "Project enabled";
	} else {
		if(strtotime($dtFinal)<= strtotime("now")){
			$actionvar = "Project enabled and published";
			//$status = 0; //published
		} else {
			$actionvar = "Project enabled and scheduled";
			//$status = 2; //scheduled
		}
		$status = 0; //published
	}
} 


if($actionType == 'unarchiveAction'){
	if($proSchDtOptional ==1){ //If Project schedule is optional then it will saved as draft
		$status = 0; 
		$actionvar = "Project unarchived";
	} else {
		if(strtotime($dtFinal)<= strtotime("now")){
			$actionvar = "Project unarchived and published";
		} else {
			$actionvar = "Project unarchived and scheduled";
		}
		$status = 0; //published
	}
} 

if($actionType == 'disableAction'){
	$actionvar = "Project disabled";
	$status = 1; //unpublish
}

if($actionType == 'archiveAction'){
	$actionvar = "Project archived";
	$status = 3; //archived
}

$proSku     = isset($_POST['proSku'])?$_POST['proSku']:"";
$proDetailedDesc = isset($_POST['proDetailedDesc'])?StringInputCleaner($_POST['proDetailedDesc']):"";
$proAltCat = isset($_POST['proAltCat'])?$_POST['proAltCat']:"";

if($_POST['proFpPrintList'] != ''){
  $proFpPrintList = implode(',',$_POST['proFpPrintList']);
} else {
  $proFpPrintList = '';
}

$proFpPrintCustom = isset($_POST['proFpPrintCustom'])?StringInputCleaner($_POST['proFpPrintCustom']):"";
$proSpecWi = isset($_POST['proSpecWi'])?IntegerInputCleaner($_POST['proSpecWi']):"";
$proSpecHi = isset($_POST['proSpecHi'])?IntegerInputCleaner($_POST['proSpecHi']):"";
$proSpecDe = isset($_POST['proSpecDe'])?IntegerInputCleaner($_POST['proSpecDe']):"";
$proSpecWe = isset($_POST['proSpecWe'])?IntegerInputCleaner($_POST['proSpecWe']):"";

$images_arr = array();
if(!empty($_FILES) && is_array($_FILES)){
	$fileName = $_FILES['proGalImg']['name'];
	$fileTempName = $_FILES['proGalImg']['tmp_name'];
	$fldname="uploads/projects/";
	$resizefldname="700X600/";
	$flg="";
	$md5filename="pid!".md5(time());
	$type = strtolower(substr(strrchr($fileName,"."),1));
	if($type == 'jpeg') $type = 'jpg';
	if($type == 'JPEG') $type = 'jpg';
	if($type == 'JPG') $type = 'jpg';
	if($type == 'GIF') $type = 'gif';
	if($type == 'BMP') $type = 'bmp';
	if($type == 'PNG') $type = 'png';

	$images_arr[0]=$md5filename.".".$type;
	upload_image_to_s3project($md5filename,$fileName,$fileTempName,$fldname,$resizefldname,$flg,@$_POST['flpup']);
} else {
  $images_arr[0] = $_POST['flpup'];
}
//echo '<pre>'; print_r($images_arr); die;
$data = array (
	'userId' => $userId,
	'proName' => $db->escape($proName),
	'proTypeId' => $proTypeId,
	'proSku' => $proSku,
	'proClassId' => $proClassId,
	'proDetailedDesc' => $db->escape($proDetailedDesc),
	'proPrice' =>  $proPrice1,
	'proPriCatId' => $proPriCatId,
	'proAltCat' => $proAltCat,
	'proSchType' => $_POST['proSchType'],
	'proSchStDate' => $proSchStDateVal,
	'proSchStTime' => $proSchStTime, 
	'proSchStDtTime' => $dtFinal,
	'proSchDtOptional' => $proSchDtOptional,	
	'proGalImg' => $images_arr[0],
	'proFpPrintList' => $proFpPrintList,
	'proFpPrintCustom' => $db->escape($proFpPrintCustom),
	'proSpecWi' =>  $db->escape($proSpecWi),
	'proSpecHi' =>  $db->escape($proSpecHi),
	'proSpecDe' =>  $db->escape($proSpecDe),
	'proSpecWe' =>  $db->escape($proSpecWe),
	'status' => $status, //0=>Active, 1=> DeActive
	'isDeleted' => 0,
	'updatedDate' => date('Y-m-d H:i:s')
);
$db->where ('id', $_POST["projectId"]);
$update = $db->update("bb_project",$data);
$last_insert_id = $_POST["projectId"];		
if($update) {
    if(!empty($_POST['proOptionVal'][0]) && is_array($_POST['proOptionVal'])) {
	    $params1 = array('');
		$result1 = $db->rawQuery("SELECT id,proId FROM bb_propersoptions WHERE proId='".$last_insert_id."' ", $params1);
		$ritems1 = (array)$result1;
		if(!empty($ritems1)) {
		    foreach($ritems1 as $key=>$val) {
				$db->where ('id', $val['id']);
				$db->where ('proId', $val['proId']);
				$db->delete("bb_propersoptions");
			}
		}
		
		for($i=0;$i<count($_POST['proOptionVal']); $i++) {
			if($_POST['proOptionVal'][$i] != ''){
				$data1 = array (
					'proUserId' => $_POST['userId'],
					'proId' => $last_insert_id,
					'proOptionVal' => $_POST['proOptionVal'][$i],
					'proLabel' => $db->escape(StringInputCleaner($_POST['proLabel'][$i])),
					'proHoverTxt' => $db->escape(StringInputCleaner($_POST['proHoverTxt'][$i])),
					//'proOptVal' => $_POST['proOptVal_'.$i][0]
					'proOptVal' => $_POST['proOptValHidden'][$i]
				);
				$ins1 = $db->insert("bb_propersoptions",$data1);
				/*if($ins1){
					echo "query=".$db->getLastQuery();
				} else {
					echo "issue=".$db->getLastError();
				}*/
			}
		}
	} 
	else {
		$params1 = array('');
		$result1 = $db->rawQuery("SELECT id,proId FROM bb_propersoptions WHERE proId='".$last_insert_id."' ", $params1);
		$ritems1 = (array)$result1;
		if(!empty($ritems1)) {
		    foreach($ritems1 as $key=>$val) {
				$db->where ('id', $val['id']);
				$db->where ('proId', $val['proId']);
				$db->delete("bb_propersoptions");
			}
		}
	}
	
	if(!empty($_POST['specOptionVal'][0]) && is_array($_POST['specOptionVal'])) {
	    $params3 = array('');
		$result3 = $db->rawQuery("SELECT id,proId FROM bb_prospecifications WHERE proId='".$last_insert_id."' ", $params3);
		$ritems3 = (array)$result3;
		if(!empty($ritems3)) {
			foreach($ritems3 as $key1=>$val1) {
			    $db->where ('id', $val1['id']);
				$db->where ('proId', $val1['proId']);
				$db->delete("bb_prospecifications");
			} 
		}
		
		for($j=0;$j<count($_POST['specOptionVal']); $j++) {
			$data3 = array (
				'userId' => $_POST['userId'],
				'proId' => $last_insert_id,
				'specOptionVal' => $_POST['specOptionVal'][$j],
				'specType' => $_POST['specType'][$j],
				'specTypeVal' => $_POST['specTypeVal'][$j]
			);
			$ins2 = $db->insert("bb_prospecifications",$data3);
			/*if($ins2){
				echo "query2=".$db->getLastQuery();
			} else {
				echo "issue2=".$db->getLastError();
			}*/
		}
	}
	
	//Project History insertion
	$data4 = array (
		'lgUserId' => $_POST['userId'],
		'lgProId' => $last_insert_id,
		'lgDescription' => $actionvar,
		'lgDate' => date('Y-m-d'),
		'lgTime' => date('H:i:s')
	);
	$db->insert("bb_proactivitylog",$data4);
	
	//$response["msg"] = "Project is successfully".$msg.$db->getLastQuery(); //$db->getLastQuery();
	//$response["last_insert_id"] = $last_insert_id;
	if($actionType == 'enableAction'){
		$_SESSION["msg"]= "Project is successfully enabled";
		$response["status"] = 0;
	} 
	if($actionType == 'disableAction'){
		$_SESSION["msg"]= "Project is successfully disabled";
		$response["status"] = 1;
	}
	if($actionType == 'archiveAction'){
		$_SESSION["msg"]= "Project is successfully archived";
		$response["status"] = 3;
	}
	
	if($actionType == 'unarchiveAction'){
		$_SESSION["msg"]= "Project is successfully unarchived";
		$response["status"] = 0;
	}
} 
else {
	//$response["msg"] = "Project is successfully disabled".$db->getLastError(); //$db->getLastError();
	//$response["last_insert_id"] = '';
	if($actionType == 'enableAction'){
		$_SESSION["msg"]= "Project is not successfully enabled";
		$response["status"] = 0;
	} 
	if($actionType == 'disableAction'){
		$_SESSION["msg"]= "Project is not successfully disabled";
		$response["status"] = 1;
	}
	if($actionType == 'archiveAction'){
		$_SESSION["msg"]= "Project is not successfully archived";
		$response["status"] = 3;
	}
	
	if($actionType == 'unarchiveAction'){
		$_SESSION["msg"]= "Project is not successfully unarchived";
		$response["status"] = 0;
	}
}
echo json_encode($response);
die;
?>