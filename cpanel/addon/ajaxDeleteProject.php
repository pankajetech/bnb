<?php
include("../includes/functions.php");
global $db;

$projectId = urldecode($_POST['projectId']);
$projectId = trim($projectId);
$response = array();

//Update Project Table Data
$data = array ( "isDeleted"=>1);
$db->where ('id',$projectId);
$update = $db->update("bb_project",$data);

//Delete Project Personalize Data
$params1 = array('');
$result1 = $db->rawQuery("SELECT id,proId FROM bb_propersoptions WHERE proId='".$projectId."' ", $params1);
$ritems1 = (array)$result1;
if(!empty($ritems1)) {
	foreach($ritems1 as $key=>$val) {
		$db->where ('id', $val['id']);
		$db->where ('proId', $val['proId']);
		$db->delete("bb_propersoptions");
	}
}

//Delete Project Specification Data 
$params3 = array('');
$result3 = $db->rawQuery("SELECT id,proId FROM bb_prospecifications WHERE proId='".$projectId."' ", $params3);
$ritems3 = (array)$result3;
if(!empty($ritems3)) {
	foreach($ritems3 as $key1=>$val1) {
		$db->where ('id', $val1['id']);
		$db->where ('proId', $val1['proId']);
		$db->delete("bb_prospecifications");
	} 
}

if($update == 1){ 
	$response['msg'] = "Project is Successfully Deleted";
	$response['status'] = 1;
} else {
	$response['msg'] = "Project is not Successfully Deleted";
	$response['status'] = 2;
}
echo json_encode($response);
die;
?>