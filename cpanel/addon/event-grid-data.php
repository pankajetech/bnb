<?php

include("../includes/functions.php");
global $db;

// storing  request (ie, get/post) global array to a variable  
$requestData= $_REQUEST;

$columns = array( 
// datatable column index  => database column name
	0 => 'id', 
	1 => 'evStatus', 
	4 => 'evDate',
	5 => 'evStTime',
	6 => 'evTitle'
);

	
//echo "<pre>requestData===".print_r($requestData); die;

$stwhre="";
$ck=0;
$ckurl="";
$orderby="";
if(isset($requestData["estatus"]) && $requestData["estatus"]=='draft') {
	$stwhre.= " and evStatus=1 and isDeleted=0";
	$orderby=" order by evDate desc";
} else if(isset($requestData["estatus"]) && $requestData["estatus"]=='trash') {
	$ck=1;
	$stwhre.= " and isDeleted=1 ";
	$orderby=" order by evDate desc";
} else if(isset($requestData["estatus"]) && $requestData["estatus"]=='current') {
	$ck=2;
	$orderby=" order by evDate asc";
	$stwhre.= "  and evDate>='".date('Y-m-d')."' and evStatus=0 and isDeleted=0 ";
} else if(isset($requestData["estatus"]) && $requestData["estatus"]=='past') {
	$ck=4;
	$orderby=" order by evDate desc";
	$stwhre.= "  and evDate<'".date('Y-m-d')."' and evStatus=0 and isDeleted=0 ";
} else {
	$ck=3;
	$orderby=" order by evDate desc";
	$stwhre.= "  and isDeleted=0 ";
}
//and evStatus=0

//Getting total number records without any search
$params = array('');
if(isset($_SESSION["urole"]) && ($_SESSION["urole"]==1 || $_SESSION["urole"]==2) ) {
	/*echo "SELECT * FROM `bb_event` where evLocationId='".$_SESSION["stidloc"]."'  $stwhre $orderby";
	die();*/
	$evlist = $db->rawQuery("SELECT * FROM `bb_event` where evLocationId='".$_SESSION["stidloc"]."'  $stwhre $orderby", $params);
} else {
	/*echo "SELECT * FROM `bb_event` where studioId IN(".$_SESSION["studioIds"].")  $stwhre order by id desc";
	die();*/
	if(isset($_SESSION["stId"]) && !empty($_SESSION["stId"])) {
		$locId=$_SESSION["stId"];
	} else {
		$locId=$_SESSION["studioIds"];
	}
	$evlist = $db->rawQuery("SELECT * FROM `bb_event` where FIND_IN_SET(".$locId.",`evLocationId`)  $stwhre $orderby", $params);
}	
if(count($evlist)>0){
	$totalData = $db->count;
	$totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
} 
	
	
if(isset($_SESSION["urole"]) && ($_SESSION["urole"]==1 || $_SESSION["urole"]==2) ) {
/*	echo "SELECT * FROM `bb_event` where evLocationId='".$_SESSION["stidloc"]."'  $stwhre ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."'" ;
	die();*/
	$evlist = $db->rawQuery("SELECT id,userid,evIsClose,isCancel,evDate,evStTime,evTitle,evScheduleHrs,evStatus,evImage,evBookTicket,evNoOfTickets,evEndTime FROM `bb_event` where evLocationId='".$_SESSION["stidloc"]."'  $stwhre ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']." ", $params);
} else {
	/*echo "SELECT * FROM `bb_event` where studioId IN(".$_SESSION["studioIds"].")  $stwhre order by id desc";
	die();*/
	if(isset($_SESSION["stId"]) && !empty($_SESSION["stId"])) {
		$locId=$_SESSION["stId"];
	} else {
		$locId=$_SESSION["studioIds"];
	}
	$sql = "SELECT * FROM `bb_event` where FIND_IN_SET(".$locId.",`evLocationId`)  $stwhre ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']." ";
	
/*	print_r($_REQUEST);
	
	echo $sql;
	die();*/
	
	$evlist = $db->rawQuery($sql, $params);
	
}
/* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc  */	
//echo "<pre>evlist===".print_r($evlist); die;
$data = array();
if(isset($evlist) && !empty($evlist)) {

$curDate=strtotime(date('Y-m-d H:i:s'));

	foreach($evlist as $key=>$item){ // preparing an array
		$nestedData=array(); 

$publishDate=strtotime(date('Y-m-d H:i:s',strtotime($item['evSPubDate']." ".$item['evSPubTime'])));
$evIsClose=$item['evIsClose'];
$isCancel=$item['isCancel'];

$date = new DateTime($item['evDate']." ".$item['evStTime']);
$startDate = $date->format('Y-m-d H:i:s');
//echo date('d-m-Y h:i:s',$curDate).'--'.$startDate1."--1<Br>";

$startWDate=date('Y-m-d',strtotime($item['evDate']." ".$item['evStTime']));

$evDTime=strtotime(date('Y-m-d H:i:s',strtotime($item['evDate']." ".$item['evStTime'])));
$evBDTime=$item['evDate']." ".$item['evStTime'];
if(isset($item['evScheduleHrs']) && $item['evScheduleHrs']>0) {
$evsDays=strtotime("-".$item['evScheduleHrs']." hours",strtotime($evBDTime));
} else {
$evsDays=$evDTime;
}



		
		if(isset($item['id'])) { $evntid=$item['id']; } else { $evntid=$item['id']; }

		if(isset($_SESSION['urole']) && ($_SESSION['urole']==1 || $_SESSION['urole']==2 || $_SESSION['urole']==3)) {
			if($ck==1){
				$resstr=' <a href="javascript:void(0);" class="del" id="'.$evntid.'" data-type="r">Restore</a> ';
				$delstr=' <a href="javascript:void(0);" class="del" id="'.$evntid.'" data-type="p">Delete</a> /';
			} else {
				$resstr='';
				$delstr=' <a href="javascript:void(0);" class="del" id="'.$evntid.'" data-type="">Delete</a>';
			}
		} else {
			$delstr='';
		}
		if($item['evDate']!="0000-00-00") {
		if($curDate>=$publishDate) {
			if($evIsClose==1 && $isCancel==0 && $item['evStatus']==0 && $evsDays>=$curDate) {
				$status='Closed';
			} if($isCancel==1 && $evIsClose==0 && $item['evStatus']==0 && $evsDays>=$curDate) {
				$status='Cancelled';
			} if($isCancel==1 && $evIsClose==1 && $item['evStatus']==0 && $evsDays>=$curDate) {
				$status='Cancelled and Closed';
			} else if($item['evStatus']==0 && $isCancel==0 && $evIsClose==0 && $evsDays>=$curDate) {
				$status='Published';
			} /*else if($item['evStatus']==0 && $evIsClose==0 && $evsDays>=$curDate) {
				$status='Published';
			} */ else if($item['evStatus']==1 && ($isCancel==0 || $evIsClose==0) && $evsDays>=$curDate) {
				$status='Draft';
			} 
			else {
				if($curDate>=$evsDays) {
					$status='Closed';
				} 
			}
		} 
		else if($curDate<$publishDate) {
			if($evIsClose==1 && $isCancel==0 && $item['evStatus']==0 && $evsDays>=$curDate) {
				$status='Closed';
			} if($isCancel==1 && $evIsClose==0 && $item['evStatus']==0 && $evsDays>=$curDate) {
				$status='Cancelled';
			} if($isCancel==1 && $evIsClose==1 && $item['evStatus']==0 && $evsDays>=$curDate) {
				$status='Cancelled and Closed';
			} else if($item['evStatus']==0 && $isCancel==0 && $evIsClose==0 && $evsDays>=$curDate) {
				$status='Scheduled';
			} /*else if($item['evStatus']==0 && $evIsClose==0 && $evsDays>=$curDate) {
				$status='Published';
			} */ else if($item['evStatus']==1 && ($isCancel==0 || $evIsClose==0) && $evsDays>=$curDate) {
				$status='Draft';
			} 
			else {
				if($curDate>=$evsDays) {
					$status='Closed';
				} 
			}
		}  
		} else {
		$status ="Draft";
		}


		/*else if($curDate<=$publishDate && $publishDate>=$evDTime && $item['evStatus']==0) {
			$status='Scheduled';
		} else if($curDate<=$publishDate && $publishDate<=$evDTime && $item['evStatus']==0) {
			$status='Published';
		}*/

		
		/*if($item['evStatus']=="0") {
			$status='Published';
		} else {
			$status='Draft';
		}*/
		
		if($ck!=1){
			$editstr='<a href="addupdevent.php?evid='.$evntid.'">Edit</a> / ';
		}
		$vtstr='<a href="'.base_url_site."single-event?evid=".$item['id'].'">View</a> ';
		
	
		
		if(isset($item['evImage']) && $item['evImage']!=""){
			$imageurl='<img src="'.s3_url_uimages."uploads/events/".$item['evImage'].'" height="80" width="80">';
		}  else  {
			$imageurl='<img src="'.s3_url_uimages.'placeholder.jpg" height="80" width="80">';
		}

		$evWeekDay=date('D',strtotime($item['evDate']));
		
		if($item['evBookTicket'] >0){
			$downloadAsCsv1 = '<button class="green-btn exportBtn1" style="padding:3px 4px;" customAttr="'.$evntid.'">Download </button>';
		} else {
			$downloadAsCsv1 = '';
		}
		
		if($item['evBookTicket'] != ""){
			$evBookTicket = $item['evBookTicket'];
		} else {
			$evBookTicket = 0;
		}
		
		$bookedTickets='Booked:<strong>'.$evBookTicket.'/'.$item['evNoOfTickets'].'</strong>';
		$bookedPTickets='Pending:<strong>'.countPBookingEventTicketListing($item['id'],$item['evLocationId']).'</strong>';
		
		/*if($requestData["estatus"] == "trash"){
			if($i%2==0) {
				echo '<tr class="even gradeC"><td class="center"> '.$editstr.' '.$delstr.' '.$resstr.'</td><td>'.$status.'</td><td>'.$imageurl.'</td><td>'.$evWeekDay.'</td><td>'.date('m/d/Y',strtotime($item['evDate'])).'</td><td>'.$item['evStTime']."-".$item['evEndTime'].'</td><td>'.$item['evTitle'].'</td><td>'.getNamebyPara("bb_users","firstName","id",$item['userid']).' '.getNamebyPara("bb_users","lastName","id",$item['userid']).'</td><td>'.$bookedTickets.'<br>'.$bookedPTickets.'</td><td class="center"></td></tr>';
			} else {
				echo '<tr class="odd gradeX"><td class="center"> '.$editstr.' '.$delstr.' '.$resstr.'</td><td>'.$status.'</td><td>'.$imageurl.'</td><td>'.$evWeekDay.'</td><td>'.date('m/d/Y',strtotime($item['evDate'])).'</td><td>'.$item['evStTime']."-".$item['evEndTime'].'</td><td>'.$item['evTitle'].'</td><td>'.getNamebyPara("bb_users","firstName","id",$item['userid']).' '.getNamebyPara("bb_users","lastName","id",$item['userid']).'</td><td>'.$bookedTickets.'<br>'.$bookedPTickets.'</td><td class="center"></td></tr>';
			}
		} else {
			if($i%2==0) {
				echo '<tr class="even gradeC"><td class="center"> '.$editstr.' '.$delstr.' '.$resstr.'</td><td>'.$status.'</td><td>'.$imageurl.'</td><td>'.$evWeekDay.'</td><td>'.date('m/d/Y',strtotime($item['evDate'])).'</td><td>'.$item['evStTime']."-".$item['evEndTime'].'</td><td>'.$item['evTitle'].'</td><td>'.getNamebyPara("bb_users","firstName","id",$item['userid']).' '.getNamebyPara("bb_users","lastName","id",$item['userid']).'</td><td>'.$bookedTickets.'<br>'.$bookedPTickets.'</td><td class="center">'.$downloadAsCsv1.'</td></tr>';
			} else {
				echo '<tr class="odd gradeX"><td class="center"> '.$editstr.' '.$delstr.' '.$resstr.'</td><td>'.$status.'</td><td>'.$imageurl.'</td><td>'.$evWeekDay.'</td><td>'.date('m/d/Y',strtotime($item['evDate'])).'</td><td>'.$item['evStTime']."-".$item['evEndTime'].'</td><td>'.$item['evTitle'].'</td><td>'.getNamebyPara("bb_users","firstName","id",$item['userid']).' '.getNamebyPara("bb_users","lastName","id",$item['userid']).'</td><td>'.$bookedTickets.'<br>'.$bookedPTickets.'</td><td class="center">'.$downloadAsCsv1.'</td></tr>';
			}
		}*/
		
		$nestedData[] = $editstr.' '.$vtstr.' '.$delstr.' '.$resstr;
		$nestedData[] = $status;
		//$nestedData[] = $vtstr;
		
		$nestedData[] = $imageurl;
		if($item['evDate']!="0000-00-00") {
		$nestedData[] = $evWeekDay;
		} else {
		$nestedData[] = "-";
		}
		if($item['evDate']!="0000-00-00") {
		$nestedData[] = date('m/d/Y',strtotime($item['evDate']));
		} else {
		$nestedData[] = "-";
		}
		$nestedData[] = $item['evStTime']."-".$item['evEndTime'];
		$nestedData[] = checkSpace($item['evTitle']);
		$nestedData[] = getNamebyPara("bb_users","firstName","id",$item['userid']).' '.getNamebyPara("bb_users","lastName","id",$item['userid']);
		$nestedData[] = $bookedTickets.'<br>'.$bookedPTickets;
		$nestedData[] = $downloadAsCsv1;
		
		$data[] = $nestedData;
	}
	//echo "<pre>data===".print_r($data); die;
}

$json_data = array(
	"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
	"recordsTotal"    => intval( $totalData ),  // total number of records
	"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
	"data"            => $data,   // total data array
);
echo json_encode($json_data);  // send data as json format
?>