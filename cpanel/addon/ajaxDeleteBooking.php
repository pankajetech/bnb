<?php
include("../includes/functions.php");
global $db;

$bookingId = urldecode($_POST['bookingId']);
$bookingId = trim($bookingId);
$response = array();

$data = array ( "isDeleted"=>1);
$db->where ('id',$bookingId);
$update = $db->update("bb_booking",$data);

$bookingStatus = $_POST['bookingStatus'];
//0=>Completed,3=>Need Attention,4=>Change Project Pending
if($bookingStatus == 0 || $bookingStatus == 3 || $bookingStatus == 4){
	//Function is used to decrease seats on delete booking
	decreaseSeatsFromBookedEvent($bookingId);
}


//Delete bb_booktcktinfo Data
/*$params1 = array('');
$result1 = $db->rawQuery("SELECT id,bookingId FROM bb_booktcktinfo WHERE bookingId='".$bookingId."' ", $params1);
$ritems1 = (array)$result1;
if(!empty($ritems1)) {
	foreach($ritems1 as $key1=>$val1) {
		$db->where ('id', $val1['id']);
		$db->where ('bookingId', $val1['bookingId']);
		$db->delete("bb_booktcktinfo");
	}
}*/


//Delete bb_booktcktpersinfo Data
/*$params2 = array('');
$result2 = $db->rawQuery("SELECT id,bookingId FROM bb_booktcktpersinfo WHERE bookingId='".$bookingId."' ", $params2);
$ritems2 = (array)$result2;
if(!empty($ritems2)) {
	foreach($ritems2 as $key2=>$val2) {
		$db->where ('id', $val2['id']);
		$db->where ('bookingId', $val2['bookingId']);
		$db->delete("bb_booktcktpersinfo");
	}
}*/

//Update bb_transactiondetails status = deleted
$params3 = array('');
$result3 = $db->rawQuery("SELECT id,bookingId FROM bb_transactiondetails WHERE bookingId='".$bookingId."' ", $params3);
$ritems3 = (array)$result3;
if(!empty($ritems3)) {
	foreach($ritems3 as $key3=>$val3) {
		$data3 = array("isDeleted"=>1);
		$db->where ('id', $val3['id']);
		$db->where ('bookingId', $val3['bookingId']);
		$update3 = $db->update("bb_transactiondetails",$data3);
	}
}

if($update == 1){ 
	$response['msg'] = "Booking is successfully Deleted";
	$response['status'] = 1;
} else {
	$response['msg'] = "Booking is not successfully Deleted";
	$response['status'] = 2;
}
echo json_encode($response);
die;
?>