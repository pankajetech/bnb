<?php
include("../includes/functions.php");
global $db;

$promoId = urldecode($_POST['promoId']);
$promoId = trim($promoId);
$response = array();
$data = array ( "isDeleted"=>1);
$db->where ('id',$promoId);
$update = $db->update("bb_promocode",$data);
if($update == 1){ 
	$response['msg'] = "Successfully deleted coupon code";
	$response['status'] = 1;
} else {
	$response['msg'] = "Not successfully deleted coupon code";
	$response['status'] = 2;
}
echo json_encode($response);
die;
?>