<?php
include("../includes/functions.php");
global $db;

if($_SERVER['REQUEST_METHOD'] == "POST")  {
	$response = array();
	$proCatName = isset($_POST['proCatName'])?StringInputCleaner($_POST['proCatName']):"";
	$data = array ('proCatName' => $db->escape($proCatName));
	if(isset($_POST["catid"]) and $_POST["catid"]!="") { 
		$db->where ('id', $_POST["catid"]);
		$update = $db->update("bb_procategory",$data);
		$_SESSION["msg"] = "Successfully updated category";
	} 
	else { 
		$update = $db->insert("bb_procategory",$data);
		/* echo "Last executed query was ". $db->getLastQuery();
		   die();*/
		$_SESSION["msg"]= "Successfully added category";
	}
	$response["result"]="1";
	echo json_encode($response); 	
	exit;
}
?>