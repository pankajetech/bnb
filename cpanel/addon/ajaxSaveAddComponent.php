<?php

include("../includes/functions.php");
global $db;

if(isset($_POST['actionType']) && $_POST['actionType']=="saveAction" )
{

	///----------save code goes here--------------
$response = array();
$userId     = isset($_POST['userId'])?$_POST['userId']:"";
$compName     = isset($_POST['compName'])?StringInputCleaner($_POST['compName']):"";
$ComFpPrint     = isset($_POST['ComFpPrint'])?StringInputCleaner($_POST['ComFpPrint']):"";
$proComSku    = isset($_POST['proComSku'])?$_POST['proComSku']:"";
$ComPriceOption     = isset($_POST['ComPriceOptionHidden'])?$_POST['ComPriceOptionHidden']:"0";
$ComPrice = isset($_POST['ComPrice'])?$_POST['ComPrice']:"0";

$proComSpecWi = isset($_POST['proComSpecWi'])?IntegerInputCleaner($_POST['proComSpecWi']):"";
$proComSpecHi= isset($_POST['proComSpecHi'])?IntegerInputCleaner($_POST['proComSpecHi']):"";
$proComSpecDe = isset($_POST['proComSpecDe'])?IntegerInputCleaner($_POST['proComSpecDe']):"";
$proComSpecWe = isset($_POST['proComSpecWe'])?IntegerInputCleaner($_POST['proComSpecWe']):"";
$firstimg = isset($_POST['firstimg'])?StringInputCleaner($_POST['firstimg']):"";
$i;
$i=0;

////restrction code---------start


$fixedprice = isset($_POST['fixprice'])?$_POST['fixprice']:"";
$percentage= isset($_POST['Percentpr'])?$_POST['Percentpr']:"";
$studioes = isset($_POST['studio'])?$_POST['studio']:"";

$stud=implode(',', $studioes);






////restrction code---------end
// image code start//
if(!empty($_FILES) && is_array($_FILES)){
foreach ($_FILES as $filename) 
            {
    $fileName = $filename['name'];
	$fileTempName = $filename['tmp_name'];
	$fldname="uploads/projects/";
	$resizefldname="700X600/";
	$flg="";
	$md5filename="cid!".md5(time());
	$type = strtolower(substr(strrchr($fileName,"."),1));
    if($type == 'jpeg') $type = 'jpg';
    if($type == 'JPEG') $type = 'jpg';
    if($type == 'JPG') $type = 'jpg';
    if($type == 'GIF') $type = 'gif';
    if($type == 'BMP') $type = 'bmp';
    if($type == 'PNG') $type = 'png';
	$images_arr[$i]=$md5filename.".".$type;
	if($fileName==$firstimg)
	{
		$firstimg=$md5filename.".".$type;
	}
	
	upload_image_to_s3project($md5filename,$fileName,$fileTempName,$fldname,$resizefldname,$flg,@$_POST['flpup']);
	$i++;
            }
}
else{
	 $firstimg = "";
	
}


$data = array (
	'userId' => $userId,
	'proComName' => $db->escape($compName),
	'proComSku' => $proComSku,
	'proComPrice' => $ComPrice,
	'proComGalImg' => $firstimg,
	'proComFpPrint' => $db->escape($ComFpPrint),
	'proComFpPrintCustom' =>  "",
	'proComSpecWi' => $proComSpecWi,
	'proComSpecHi' => $proComSpecHi,
	'proComSpecDe' => $proComSpecDe,
	'proComSpecWe' => $proComSpecWe,
	'status' => 0,
	'isArchive'=> 0,
	'isDeleted' => 0,
	'createdDate' => date('Y-m-d H:i:s'),
	'updatedDate' => date('Y-m-d H:i:s'),
	'proComPriceOption'=> $ComPriceOption
);

$last_insert_id = $db->insert("bb_component",$data);
if($last_insert_id) {
	
	
	if($fixedprice!="")
{
	$data6 = array (
	'restoption' => '1',
	'restvalues' => $fixedprice,
	'userId' => $userId,
	'compId' => $last_insert_id,
	'restname' => 'Coupon',
	'restoptname' => 'Fixed Amount',
	'restvalueId' => '1',
	);

	
	$db->insert("bb_comprestriction",$data6);
	
	
}

	if($percentage!="")
{
	$data7 = array (
	'restoption' => '1',
	'restvalues' => $percentage,
	'userId' => $userId,
	'compId' => $last_insert_id,
	'restname' => 'Coupon',
	'restoptname' => 'Percent',
	'restvalueId' => '2',
	);

	
	$db->insert("bb_comprestriction",$data7);
	
	
}	
	
	
	if($studioes!="")
{
	$data8 = array (
	'restoption' => '2',
	'restvalues' => $stud,
	'userId' => $userId,
	'compId' => $last_insert_id,
	'restname' => 'Studio',
	'restoptname' => 'studioes',
	'restvalueId' => '3',
	);

	
	$db->insert("bb_comprestriction",$data8);
	
	
}	
		foreach ($images_arr as $images) {
			$data3 = array (
				'compId' => $last_insert_id,
				'compImgName' => $images,
				'compUserId' => $_POST['userId'],
				'compImgStatus' => "1",

			);
			$db->insert("bb_compgallery",$data3);
		}
	
	
	//Project History insertion
	$data4 = array (
		'lgUserId' => $_POST['userId'],
		'lgComId' => $last_insert_id,
		'lgDescription' => "Record Inserted",
		'lgDate' => date('Y-m-d'),
		'lgTime' => date('H:i:s')
	);
	$db->insert("bb_compactivitylog",$data4);
	

		$_SESSION["msg"]= "Successfully saved component data";
		$response["status"] = 0;
		
} 
else {
	
		$_SESSION["msg"]= "Not successfully saved component data";
	
}
///----------save code ends here--------------

}
else
{
	
	if(isset($_POST['actionType']) && $_POST['actionType']=="updateAction" )
	{
		
		
	//	<!-------         Update form code goes here                       -------->
		
		
		
		$response = array();
		$compId=isset($_POST['compId'])?$_POST['compId']:"";
$userId     = isset($_POST['userId'])?$_POST['userId']:"";
$compName     = isset($_POST['compName'])?StringInputCleaner($_POST['compName']):"";
$ComFpPrint     = isset($_POST['ComFpPrint'])?StringInputCleaner($_POST['ComFpPrint']):"";
$proComSku    = isset($_POST['proComSku'])?$_POST['proComSku']:"";
$ComPriceOption     = isset($_POST['ComPriceOptionHidden'])?$_POST['ComPriceOptionHidden']:"0";
$ComPrice = isset($_POST['ComPrice'])?$_POST['ComPrice']:"0";

$proComSpecWi = isset($_POST['proComSpecWi'])?IntegerInputCleaner($_POST['proComSpecWi']):"";
$proComSpecHi= isset($_POST['proComSpecHi'])?IntegerInputCleaner($_POST['proComSpecHi']):"";
$proComSpecDe = isset($_POST['proComSpecDe'])?IntegerInputCleaner($_POST['proComSpecDe']):"";
$proComSpecWe = isset($_POST['proComSpecWe'])?IntegerInputCleaner($_POST['proComSpecWe']):"";
$firstimg = isset($_POST['firstimg'])?StringInputCleaner($_POST['firstimg']):"";
$imgdel=isset($_POST['delimg'])?StringInputCleaner($_POST['delimg']):"";
$i;
$i=0;

/////----------restrction code start
		$fixedprice = isset($_POST['fixprice'])?$_POST['fixprice']:"";
$percentage= isset($_POST['Percentpr'])?$_POST['Percentpr']:"";
$studioes = isset($_POST['studio'])?$_POST['studio']:"";

$stud=implode(',', $studioes);
		
/////----------restrction code ends		
		
if(!empty($_FILES) && is_array($_FILES)){
foreach ($_FILES as $filename) 
            {
    $fileName = $filename['name'];
	$fileTempName = $filename['tmp_name'];
	$fldname="uploads/projects/";
	$resizefldname="700X600/";
	$flg="";
	$md5filename="cid!".md5(time());
	$type = strtolower(substr(strrchr($fileName,"."),1));
    if($type == 'jpeg') $type = 'jpg';
    if($type == 'JPEG') $type = 'jpg';
    if($type == 'JPG') $type = 'jpg';
    if($type == 'GIF') $type = 'gif';
    if($type == 'BMP') $type = 'bmp';
    if($type == 'PNG') $type = 'png';
	$images_arr[$i]=$md5filename.".".$type;
	if($fileName==$firstimg)
	{
		$firstimg=$md5filename.".".$type;
	}
	
	upload_image_to_s3project($md5filename,$fileName,$fileTempName,$fldname,$resizefldname,$flg,@$_POST['flpup']);
	$i++;
            }
			
			
			
			
}




$data = array (
	'userId' => $userId,
	'proComName' => $db->escape($compName),
	'proComSku' => $proComSku,
	'proComPrice' => $ComPrice,
	'proComGalImg' => $firstimg,
	'proComFpPrint' => $db->escape($ComFpPrint),
	'proComFpPrintCustom' =>  "",
	'proComSpecWi' => $proComSpecWi,
	'proComSpecHi' => $proComSpecHi,
	'proComSpecDe' => $proComSpecDe,
	'proComSpecWe' => $proComSpecWe,
	'updatedDate' => date('Y-m-d H:i:s'),
	'proComPriceOption'=> $ComPriceOption
);

$db->where ('id', $compId);
$update = $db->update("bb_component",$data);

		if($update)
		{
		
		
		
		       $db->where ('compId', $compId);
				$db->delete("bb_comprestriction");
				if($imgdel!="")
		{
		
		$params = array('');
	$db->rawQuery("delete from bb_compgallery where id in ($imgdel) and compId=$compId", $params);
		}
		
		
		
		
		
		$data4 = array (
		'lgUserId' => $_POST['userId'],
		'lgComId' => $compId,
		'lgDescription' => "Record Updated",
		'lgDate' => date('Y-m-d'),
		'lgTime' => date('H:i:s')
	);
	$db->insert("bb_compactivitylog",$data4);
	
		
		
		foreach ($images_arr as $images) {
			$data3 = array (
				'compId' => $compId,
				'compImgName' => $images,
				'compUserId' => $_POST['userId'],
				'compImgStatus' => "1",

			);
			$db->insert("bb_compgallery",$data3);
		}
	
	if($fixedprice!="")
{
	$data6 = array (
	'restoption' => '1',
	'restvalues' => $fixedprice,
	'userId' => $userId,
	'compId' => $compId,
	'restname' => 'Coupon',
	'restoptname' => 'Fixed Amount',
	'restvalueId' => '1',
	);

	
	$db->insert("bb_comprestriction",$data6);
	
	
}

	if($percentage!="")
{
	$data7 = array (
	'restoption' => '1',
	'restvalues' => $percentage,
	'userId' => $userId,
	'compId' => $compId,
	'restname' => 'Coupon',
	'restoptname' => 'Percent',
	'restvalueId' => '2',
	);

	
	$db->insert("bb_comprestriction",$data7);
	
	
}	
	
	
	if($studioes!="")
{
	$data8 = array (
	'restoption' => '2',
	'restvalues' => $stud,
	'userId' => $userId,
	'compId' => $compId,
	'restname' => 'Studio',
	'restoptname' => 'studioes',
	'restvalueId' => '3',
	);

	
	$db->insert("bb_comprestriction",$data8);
	
	
}	
	
	
	
		
		
		
		
		
		
		
		
		
		
		
		
		
	$_SESSION["msg"]= "Successfully Updated Component data";
		$response["status"] = 0;	
		
		}
		else
		{
			
			$_SESSION["msg"]= "Not successfully updated Component data";
		
		}
			//	<!-------         Update form code ends here                       -------->
		
		
	}
	
	else
	{
	
	
	
	
	
	
	
	
	
	
	
	    if(isset($_POST['flag']) && $_POST['flag']=="deletecomp" )
       {
	
	                  ///----------delete code goes here--------------
	        $id=$_POST['compid'];
	        $response = array();
            $data = array ( "isDeleted"=>1);
            $db->where ('id',$id);
            $update = $db->update("bb_component",$data);
            if($update == 1){ 
	        $response['msg'] = "Successfully deleted component";
	        $response['status'] = 1;
	        $_SESSION["msg"]= "Successfully deleted component";
            } else {
	        $response['msg'] = "Not successfully deleted component";
	        $response['status'] = 2;
             }
		            ///----------delete code ends here--------------
	
	
         }
         else{
	
	
	              if(isset($_POST['flag']) && $_POST['flag']=="archiveAction" )
                  {
	
	                   ///----------archiveAction code goes here--------------
	                   $id=$_POST['compid'];
	                   $response = array();
                       $data = array ( "isArchive"=>1);
                       $db->where ('id',$id);
                       $update = $db->update("bb_component",$data);
                       if($update == 1){ 
	                   $response['msg'] = "Successfully archived component";
	                   $response['status'] = 1;
	                   $_SESSION["msg"]= "Successfully archive component";
                       } else {
	                   $response['msg'] = "Not successfully archive component";
	                   $response['status'] = 2;
                       }
		               ///----------archiveAction code ends here--------------
	
	
                  }
                  else{
	
	                       if(isset($_POST['flag']) && $_POST['flag']=="unarchiveAction" )
                             {
	
	                               ///----------unarchiveAction code goes here--------------
	                              $id=$_POST['compid'];
	                              $response = array();
                                  $data = array ( "isArchive"=>0);
                                  $db->where ('id',$id);
                                  $update = $db->update("bb_component",$data);
                                  if($update == 1){ 
	                              $response['msg'] = "Successfully unarchived component";
	                              $response['status'] = 1;
	                              $_SESSION["msg"]= "Successfully unarchive component";
                                  } else {
	                              $response['msg'] = "Not successfully unarchive component";
	                              $response['status'] = 2;
                                  }
		                          ///----------unarchiveAction code ends here--------------
	
	
	
	
	
                             }
							 else
							 {
								         if(isset($_POST['flag']) && $_POST['flag']=="enabledAction" )
                                         {
	
	                                           ///----------enabledAction code goes here--------------
	                                           $id=$_POST['compid'];
	                                           $response = array();
                                               $data = array ( "status"=>0);
                                               $db->where ('id',$id);
                                               $update = $db->update("bb_component",$data);
                                               if($update == 1){ 
	                                           $response['msg'] = "Successfully enabled component";
	                                           $response['status'] = 1;
	                                           $_SESSION["msg"]= "Successfully enabled component";
                                               } else {
	                                           $response['msg'] = "Not successfully enabled component";
	                                           $response['status'] = 2;
                                               }
		                                       ///----------enabledAction code ends here--------------
	
	
	
	
	
                                          }
								          else
								          {
											             if(isset($_POST['flag']) && $_POST['flag']=="disableAction" )
                                                           {
	
	                                                           ///----------disableAction code goes here--------------
	                                                           $id=$_POST['compid'];
	                                                           $response = array();
                                                               $data = array ( "status"=>1);
                                                               $db->where ('id',$id);
                                                               $update = $db->update("bb_component",$data);
                                                               if($update == 1){ 
	                                                           $response['msg'] = "Successfully disabled component";
	                                                           $response['status'] = 1;
	                                                           $_SESSION["msg"]= "Successfully disabled component";
                                                               } else {
	                                                           $response['msg'] = "Not successfully disabled component";
	                                                           $response['status'] = 2;
                                                               }
		                                                       ///----------disableAction code ends here--------------
	
	
	
	
	
                                                            }
															else
															{
																
																if(isset($_POST['myname']) && isset($_POST['myprice']))
																$params = array('');
	                                                            $result = $db->rawQueryOne("SELECT * FROM  bb_component WHERE proComName='".$_POST['myname']."'  and proComPrice='".$_POST['myprice']."' ", $params);
	                                                            $ritems = (array)$result;
	                                                            if(empty($ritems)) {
																
																		
																		$response['status']=0;
																	
																
	                                                            }
																else{
																	
																	$response['status']=1;
																}
                                                               
																
																
																
																
																
																
																
															}
									  
								          }
								  
								 
							 }
	
	
                         }
	
	
                }
	
	
	///----------update code goes here--------------
	
	
	///----------update code ends here--------------
	}
}
echo json_encode($response);
die;
?>