<?php
include("../includes/functions.php");
global $db;
//storing  request (ie, get/post) global array to a variable  
$requestData= $_REQUEST;

$columns = array( 
// datatable column index  => database column name
	0 =>'id', 
	1 =>'status', 
	2 => 'cOrderNo',
	3=> 'bookingCreationDate',
	4=> 'boFullName',
	5=> 'boEmail',
	6=> 'boPhone',
	7=> 'totalSeats',
	8=> 'Project',
	9=> 'Personalizations',
	10=> 'promocode',
	11=> 'totalAmt',
	12=> 'paymentGateway'
);

	
//echo "<pre>requestData===".print_r($requestData); die;
if(isset($requestData['type']) && $requestData['type'] =="needatten") {
	$orderby =" order by bk.id desc";
	$stwhre.= "  and bk.isDeleted=0 and bk.status=3 ";
}
else if(isset($requestData['type']) && $requestData['type'] =="future") {
	$orderby=" order by bk.bookingCreationDate desc";
	$stwhre.= "  and bk.isDeleted=0 and bk.boEventDate >= CURDATE() ";
}
else if(isset($requestData['type']) && $requestData['type'] =="today") {
	$orderby=" order by bk.bookingCreationDate desc";
	$stwhre.= "  and bk.isDeleted=0 and bk.boEventDate = CURDATE() ";
}
else if(isset($requestData['type']) && $requestData['type'] =="tomorrow") {
	$orderby=" order by bk.bookingCreationDate desc";
	$stwhre.= "  and bk.isDeleted=0 and date(boEventDate) = date_add(CURDATE(), interval 1 day) "; 
}
else if(isset($requestData['type']) && $requestData['type'] =="past") {
	$orderby=" order by bk.bookingCreationDate desc";
	$stwhre.= "  and bk.isDeleted=0 and bk.boEventDate < CURDATE() ";
}
else if(isset($requestData['type']) && $requestData['type'] =="trash") {
	$orderby =" order by bk.id desc";
	$stwhre.= "  and bk.isDeleted=1 ";
}
else {
	$orderby=" order by bk.id desc";
	$stwhre.= "  and bk.isDeleted=0 ";
}
	
//Getting total number records without any search
$sql = "SELECT * FROM `bb_booking` as bk where bk.eventLocationId IN(".$_SESSION["stId"].") $stwhre $orderby";
$cRev = $db->rawQuery($sql, array(''));
if(count($cRev)>0){
	$totalData = $db->count; //count($cRev)
	$totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
} 


/*$sql = "SELECT * FROM `bb_booking` 
	where eventLocationId IN(".$_SESSION["stId"].") $stwhre
	ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
*/
/*$sql = "SELECT bk.id,bk.status,bk.cOrderNo,bk.bookingCreationDate,
		bk.boFullName,bk.boEmail,bk.boPhone,bk.boProjects,
		bk.promocode,bk.totalAmt,bk.boEventDate,
		(
			CASE 
			WHEN bk.status = '0' THEN 'Completed'
			WHEN bk.status = '1' THEN 'Unapproved'
			WHEN bk.status = '2' THEN 'Cancelled' 
			WHEN bk.status = '3' THEN 'Needs Attention'
			WHEN bk.status = '4' THEN 'Change Project'
			WHEN bk.status = '5' THEN 'Rejected'
			END
		) as bookingStatus,
		(
			CASE 
			WHEN trans.ptype = '0' THEN 'Authorize .net'
			WHEN trans.ptype = '1' THEN 'Gift Card'
			WHEN trans.ptype = '2' THEN 'Offline' 
			WHEN trans.ptype = '3' THEN 'Paypal'
			WHEN trans.ptype = '4' THEN 'Manual'
			WHEN trans.ptype = '' THEN 'NA'
			END
		) as paymentType,
		(
			CASE 
			WHEN trans.caddress != '' THEN trans.caddress
			WHEN trans.caddress = '' THEN 'NA'
			END
		) as caddress1,
		(
			CASE 
			WHEN trans.ccity != '' THEN trans.ccity
			WHEN trans.ccity = '' THEN 'NA'
			END
		) as ccity1,
		(
			CASE 
			WHEN trans.cstate != '' THEN trans.cstate
			WHEN trans.cstate = '' THEN 'NA'
			END
		) as cstate1,
		(
			CASE 
			WHEN trans.czip != '' THEN trans.czip
			WHEN trans.czip = '' THEN 'NA'
			END
		) as czip1,
		GROUP_CONCAT(bkTckt.boTicketProjectName) as boTicketProjectName1 
		
		
	FROM `bb_booking` as bk
	inner join `bb_booktcktinfo` as bkTckt
	on bk.id = bkTckt.bookingId
	left join `bb_transactiondetails` as trans
	on bkTckt.bookingId = trans.bookingId
	where bk.eventLocationId IN(".$_SESSION["stId"].") $stwhre
	GROUP BY bkTckt.bookingId
	ORDER BY ". 'bk.'.$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
*/

$sql = "SELECT bk.id,bk.status,bk.cOrderNo,bk.bookingCreationDate,
		bk.boFullName,bk.boEmail,bk.boPhone,bk.boProjects,
		bk.promocode,bk.totalAmt,bk.boEventDate,
		(
			CASE 
			WHEN bk.status = '0' THEN 'Completed'
			WHEN bk.status = '1' THEN 'Unapproved'
			WHEN bk.status = '2' THEN 'Cancelled' 
			WHEN bk.status = '3' THEN 'Needs Attention'
			WHEN bk.status = '4' THEN 'Change Project'
			WHEN bk.status = '5' THEN 'Rejected'
			END
		) as bookingStatus,
		(
			CASE 
			WHEN trans.ptype = '0' THEN 'Authorize .net'
			WHEN trans.ptype = '1' THEN 'Gift Card'
			WHEN trans.ptype = '2' THEN 'Offline' 
			WHEN trans.ptype = '3' THEN 'Paypal'
			WHEN trans.ptype = '4' THEN 'None'
			WHEN trans.ptype = '' THEN 'NA'
			END
		) as paymentType,
		GROUP_CONCAT(bkTckt.boTicketProjectName) as boTicketProjectName1 
		
	FROM `bb_booking` as bk
	inner join `bb_booktcktinfo` as bkTckt
	on bk.id = bkTckt.bookingId
	left join `bb_transactiondetails` as trans
	on bkTckt.bookingId = trans.bookingId
	where bk.eventLocationId IN(".$_SESSION["stId"].") $stwhre
	GROUP BY bkTckt.bookingId
	ORDER BY ". 'bk.'.$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	
//echo $sql;die;

/* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc  */	
$query = $db->rawQuery($sql, array(''));
$data = array();
if(isset($query) && !empty($query)) {
	foreach($query as $key=>$row){ // preparing an array
		$nestedData=array(); 
		
		if(isset($requestData["type"]) && $requestData["type"] =="needatten") { 
			//$actionBtnstr = '<a href="'.base_url_site.'addupdbooking?id='.$row['id'].'&type=needatten">Edit</a>|<a href="javascript:void(0);" class="delBooking"  customAtr="'.$row['id'].'" customStatusAtr="'.$row['status'].'">Delete</a>|<a href="javascript:void(0);" onclick="bookingApprovalEmail('.$row['boEmail'].','.$item['id'].','.$_SESSION['stId'].',"approval");">Approve</a>|<a href="javascript:void(0);" onclick="bookingRejectionEmail('.$item['boEmail'].','.$item['id'].','.$_SESSION['stId'].',"rejectBoookingAction");">Reject</a>';
			$actionBtnstr = '<a href="'.base_url_site.'addupdbooking?id='.$row['id'].'&type=needatten">Edit</a>|<a href="javascript:void(0);" class="delBooking"  customAtr="'.$row['id'].'" customStatusAtr="'.$row['status'].'">Delete</a>|<a href="javascript:void(0);" class="bookingApprovalEmail" data-cus-email="'.$row['boEmail'].'" data-bookingId="'.$row['id'].'" data-studioId="'.$_SESSION['stId'].'" data-actType="approval">Approve</a>|<a href="javascript:void(0);" class="bookingRejectionEmail" data-cus-email="'.$row['boEmail'].'" data-bookingId="'.$row['id'].'" data-studioId="'.$_SESSION['stId'].'" data-actType="rejectBoookingAction" >Reject</a>';
		}
		else if(isset($requestData["type"]) && $requestData["type"] =="future") {
			$actionBtnstr = '<a href="'.base_url_site.'addupdbooking?id='.$row['id'].'&type=future">Edit</a>|<a href="javascript:void(0);" class="delBooking"  customAtr="'.$row['id'].'" customStatusAtr="'.$row['status'].'">Delete</a>';
		}
		else if(isset($requestData["type"]) && $requestData["type"] =="today") { 
		    $actionBtnstr = '<a href="'.base_url_site.'addupdbooking?id='.$row['id'].'&type=today">Edit</a>|<a href="javascript:void(0);" class="delBooking"  customAtr="'.$row['id'].'" customStatusAtr="'.$row['status'].'">Delete</a>';
		}
		else if(isset($requestData["type"]) && $requestData["type"] =="tomorrow") { 
			$actionBtnstr = '<a href="'.base_url_site.'addupdbooking?id='.$row['id'].'&type=tomorrow">Edit</a>|<a href="javascript:void(0);" class="delBooking"  customAtr="'.$row['id'].'" customStatusAtr="'.$row['status'].'">Delete</a>';
		}
		else if(isset($requestData["type"]) && $requestData["type"] =="past") { 
			$actionBtnstr = '<a href="'.base_url_site.'addupdbooking?id='.$row['id'].'&type=past">Edit</a>|<a href="javascript:void(0);" class="delBooking"  customAtr="'.$row['id'].'" customStatusAtr="'.$row['status'].'">Delete</a>';
		} 
		else if(isset($requestData["type"]) && $requestData["type"] =="trash") { 
			$actionBtnstr = '<a href="javascript:void(0);" class="pdelBooking"  customAtr="'.$row['id'].'" customStatusAtr="'.$row['status'].'">Delete</a>|<a href="javascript:void(0);" class="restoreBooking"  customAtr="'.$row['id'].'" customStatusAtr="'.$row['status'].'">Restore</a>';
		} 
		else {
			$actionBtnstr = '<a href="'.base_url_site.'addupdbooking?id='.$row['id'].'">Edit</a>|<a href="javascript:void(0);" class="delBooking"  customAtr="'.$row['id'].'" customStatusAtr="'.$row['status'].'">Delete</a>';
		}
		
		//$bookingStatus = getBookingStatus($row['id']);
		$bookingStatus = $row['bookingStatus'];
		
		if(isset($row['cOrderNo']) && $row['cOrderNo']!=""){ 
			$cOrderNo = $row['cOrderNo'];
		} else { 
			$cOrderNo = "NA";
		}
		
		if(isset($row['bookingCreationDate']) && $row['bookingCreationDate']!=""){ 
			$bookingCreationDate =  date("m/d/Y",strtotime($row['bookingCreationDate']));
		} else { 
			$bookingCreationDate =  "NA";
		}
		
		if(isset($row['boFullName']) && $row['boFullName']!=""){ 
			$boFullName = $row['boFullName']; 
		} else { 
			$boFullName =  "NA";
		}
		
		if(isset($row['boEmail']) && $row['boEmail']!=""){ 
			$boEmail = $row['boEmail'];
		} else { 
			$boEmail = "NA";
		}
		
		if(isset($row['boPhone']) && $row['boPhone']!=""){ 
			$boPhone = $row['boPhone']; 
		} else { 
			$boPhone = "NA";
		}
		
		if(isset($row['boProjects']) && $row['boProjects']!=""){ 
			$totalSeats =  $row['boProjects']; 
		} else { 
			$totalSeats = "NA";
		}
		//$project = getTicketProjectName($row['id']);
		$project = $row['boTicketProjectName1'];
		
		$personalizations = getTicketProjectPersonalizeInfo($row['id']);
		if(isset($row['promocode']) && $row['promocode']!=""){ 
			$promocode = $row['promocode']; 
		} else { 
			$promocode = "NA";
		}
		if(isset($row['totalAmt']) && $row['totalAmt']!=""){ 
			$totalAmt =  "$".$row['totalAmt']; 
		} else { 
			$totalAmt = "NA";
		}
		//$paymentGateway = getPaymentGateway($row['id']);
		$paymentGateway = $row['paymentType'];
		
		/*$tansInfo = getBookingTransactioninfo($row['id']);
		if(isset($tansInfo) && !empty($tansInfo)){
			$caddress = $tansInfo['caddress'];
			$ccity = $tansInfo['ccity'];
			$cstate = $tansInfo['cstate'];
			$czip = $tansInfo['czip'];
		} else {
			$caddress =  "NA";
			$ccity = "NA";
			$cstate = "NA";
			$czip = "NA";
		}
		
		if(isset($row['boEventDate']) && $row['boEventDate']!=""){ 
			$boEventDate =  date("m/d/Y",strtotime($row['boEventDate']));
		} else { 
			$boEventDate =  "NA";
		}*/
		
		$nestedData[] = $actionBtnstr;
		$nestedData[] = $bookingStatus;
		$nestedData[] = $cOrderNo;
		$nestedData[] = $bookingCreationDate;
		$nestedData[] = $boFullName;
		$nestedData[] = $boEmail;
		$nestedData[] = $boPhone;
		
		$nestedData[] = $totalSeats;
		$nestedData[] = $project;
		$nestedData[] = $personalizations;
		
		$nestedData[] = $promocode;
		$nestedData[] = $totalAmt;
		$nestedData[] = $paymentGateway;
		
		/*$nestedData[] = $caddress;
		$nestedData[] = $ccity;
		$nestedData[] = $cstate;
		$nestedData[] = $czip;
		$nestedData[] = $boEventDate;*/
		
		$data[] = $nestedData;
	}
	//echo "<pre>data===".print_r($data); die;
}
$json_data = array(
	"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
	"recordsTotal"    => intval( $totalData ),  // total number of records
	"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
	"data"            => $data,   // total data array
);
echo json_encode($json_data);  // send data as json format
?>