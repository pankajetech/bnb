<?php
include("../includes/functions.php");
global $db;
//storing  request (ie, get/post) global array to a variable  
$requestData= $_REQUEST;
$columns = array( 
// datatable column index  => database column name
	0 =>'id', 
	1 => 'status',
	2 => 'promoName',
	3 => 'promoCode',
	4 => 'promoDescription',
	5 => 'promoRDiscountVal',
	6 => 'promoCodeUsed',
	7 => 'firstName',
	8 => 'promoSchStDtTime', 
	9 => 'promoSchEndDtTime',
);

if($requestData['order'][0]['column'] == 7){
	$tblAlias = "busers";
} else {
	$tblAlias = "bpromo";
}

$params = array('');
$stwhre="";
$orderby="";
if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="archive") {
	$orderby =" order by bpromo.id desc";
	$stwhre.= "  and bpromo.isDeleted=0 and bpromo.status=3 ";
}
else if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="current") {
	$orderby=" order by bpromo.promoSchStDtTime asc";
	$stwhre.= "  and bpromo.status=0 and bpromo.isDeleted=0 and bpromo.promoSchEndDtTime >= NOW() ";
}
else if(isset($_REQUEST["type"]) && $_REQUEST["type"] =="expired") {
	$orderby=" order by bpromo.promoSchEndDtTime asc";
	$stwhre.= "  and bpromo.status=0 and bpromo.isDeleted=0 and bpromo.promoSchEndDtTime < NOW()";
}
else {
	$orderby=" order by bpromo.id desc";
	$stwhre.= "  and (bpromo.status=0 or bpromo.status=1) and bpromo.isDeleted=0 ";
}

//Getting total number records without any search
if(!isset($_SESSION['curRole']) ) {  //studio user
	if(isset($_SESSION["stId"]) && !empty($_SESSION["stId"])) {
		$studioIdsStr = $_SESSION["stId"];
	} else {
		$studioIdsStr = str_replace(",","|",$_SESSION["studioIds"]);
	}
	$sql = "SELECT * from `bb_promocode` as bpromo WHERE CONCAT(',', `studioId`, ',') REGEXP ',(".$studioIdsStr."),' $stwhre $orderby "; 
} 
else {
	$sql = "SELECT * FROM `bb_promocode` as bpromo WHERE FIND_IN_SET(".$_SESSION["stId"].",`studioId`) $stwhre $orderby";
	
}

$cRev = $db->rawQuery($sql, array(''));
if(count($cRev)>0){
	$totalData = $db->count; //count($cRev)
	$totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
} 

if(!isset($_SESSION['curRole']) ) {  //studio user
	if(isset($_SESSION["stId"]) && !empty($_SESSION["stId"])) {
		$studioIdsStr = $_SESSION["stId"];
	} else {
		$studioIdsStr = str_replace(",","|",$_SESSION["studioIds"]);
	}
	$sql = "SELECT bpromo.id,bpromo.status,
	bpromo.promoName,bpromo.promoCode,bpromo.promoDescription,
	bpromo.promoRDiscountType,bpromo.promoRDiscountVal,
	bpromo.promoQty,bpromo.promoCreatedBy,
	bpromo.promoSchStDtTime,bpromo.promoSchEndDtTime,bpromo.promoCodeUsed,
	(
		CASE 
		WHEN bpromo.status = '0' THEN 'Active'
		WHEN bpromo.status = '2' THEN 'Draft'
		WHEN bpromo.status = '3' THEN 'Archive' 
		WHEN bpromo.status = '1' THEN 'InActive'
		END
	) as promostatus,
	busers.firstName 
	from `bb_promocode` as bpromo
	left join `bb_users` as busers
	on busers.id = bpromo.promoCreatedBy
	WHERE CONCAT(',', `studioId`, ',') REGEXP ',(".$studioIdsStr."),' $stwhre 
	ORDER BY ". ''.$tblAlias.'.'.$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";; 
} 
else {
	$sql = "SELECT bpromo.id,bpromo.status,
	bpromo.promoName,bpromo.promoCode,bpromo.promoDescription,
	bpromo.promoRDiscountType,bpromo.promoRDiscountVal,
	bpromo.promoQty,bpromo.promoCreatedBy,
	bpromo.promoSchStDtTime,bpromo.promoSchEndDtTime,bpromo.promoCodeUsed,
	(
		CASE 
		WHEN bpromo.status = '0' THEN 'Active'
		WHEN bpromo.status = '2' THEN 'Draft'
		WHEN bpromo.status = '3' THEN 'Archive' 
		WHEN bpromo.status = '1' THEN 'InActive'
		END
	) as promostatus,
	busers.firstName
	FROM `bb_promocode` as bpromo 
	left join `bb_users` as busers
	on 	busers.id = bpromo.promoCreatedBy
	WHERE FIND_IN_SET(".$_SESSION["stId"].",`studioId`) $stwhre 
	ORDER BY ". ''.$tblAlias.'.'.$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
}

//echo $sql; die;
/* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc  */	
$query = $db->rawQuery($sql, array(''));
$data = array();
if(isset($query) && !empty($query)) {
	foreach($query as $key=>$item){ // preparing an array
		$nestedData = array(); 
		
		if(!isset($_SESSION['curRole']) ) {  //studio user
			$editUrl = base_url_site."addupdpromocode?id=".$item['id'];
			if( isset($_REQUEST["type"]) && ($_REQUEST["type"] =="archive") ) { 
				$actionBtnstr = '<a href="'.$editUrl.'&type=archive">Edit</a> | <a href="javascript:void(0);" class="delPromocode" customAtr="'.$item['id'].'">Delete</a>';
			} 
			if( $_REQUEST["type"] == "" ) {
				$actionBtnstr = '<a href="'.$editUrl.'">Edit</a> | <a href="javascript:void(0);" class="delPromocode" customAtr="'.$item['id'].'">Delete</a>';
			} 
		}
		else 
		{  
			//if user is corp admin
			$editUrl1 = base_url_site."addupdpromocode?studid=".$_SESSION['stId']."&id=".$item['id'];
			if( isset($_REQUEST["type"]) && ($_REQUEST["type"] =="archive") ) { 
				$actionBtnstr = '<a href="'.$editUrl1.'&type=archive">Edit</a> | <a href="javascript:void(0);" class="delPromocode" customAtr="'.$item['id'].'">Delete</a>';	
			}  
			if( $_REQUEST["type"] == "" ) { 
				$actionBtnstr = '<a href="'.$editUrl1.'">Edit</a> | <a href="javascript:void(0);" class="delPromocode" customAtr="'.$item['id'].'">Delete</a>';	
			}
		}
		
		if(isset($item['promoName']) && $item['promoName']!=""){ 
			$promoName1 = $item['promoName'];
		} else { 
			$promoName1 = "NA";
		}
		
		if(isset($item['promoCode']) && $item['promoCode']!=""){ 
			$promoCode1 = $item['promoCode'];
		} else { 
			$promoCode1 = "NA";
		}
		
		if(isset($item['promoDescription']) && $item['promoDescription']!=""){ 
			$promoDescription1 = $item['promoDescription'];
		} else { 
			$promoDescription1 = "NA";
		}
		
		if(isset($item['promoRDiscountVal']) && $item['promoRDiscountVal']!=""){ 
			if($item['promoRDiscountType'] == '2'){ 
				$promoRDiscountVal1 = $item['promoRDiscountVal']."%";
			} else {
				$promoRDiscountVal1 = "$".$item['promoRDiscountVal'];
			}
		} else { 
			$promoRDiscountVal1 = "NA";
		}
		
		if(isset($item['promoQty']) && $item['promoQty']!=""){ 
			$promoQty1 = $item['promoCodeUsed']."/".$item['promoQty'];
		} else { 
			$promoQty1 = "NA";
		}
		
		if(isset($item['promoCreatedBy']) && $item['promoCreatedBy']!=""){ 
			if($item['firstName']!= ""){
				$creator = $item['firstName']; 
			} else {
				$creator = "NA";
			}
		} else { 
			$creator = "NA";
		}
		
		if(isset($item['promoSchStDtTime']) && $item['promoSchStDtTime']!=""){ 
			$promoSchStDtTime = $item['promoSchStDtTime']; 
		} else { 
			$promoSchStDtTime = "NA";
		}
		
		if(isset($item['promoSchEndDtTime']) && $item['promoSchEndDtTime']!=""){ 
			$promoSchEndDtTime = $item['promoSchEndDtTime']; 
		} else { 
			$promoSchEndDtTime = "NA";
		}
		
		$nestedData[] = $actionBtnstr;
		$nestedData[] = $item['promostatus'];
		
		$nestedData[] = $promoName1;
		$nestedData[] = $promoCode1;
		
		$nestedData[] = $promoDescription1;
		$nestedData[] = $promoRDiscountVal1;
		
		$nestedData[] = $promoQty1;
		$nestedData[] = $creator;
		
		$nestedData[] = $promoSchStDtTime;
		$nestedData[] = $promoSchEndDtTime;
		
		$data[] = $nestedData;
	} 
	//echo "<pre>data===".print_r($data);die;
}
$json_data = array(
	"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
	"recordsTotal"    => intval( $totalData ),  // total number of records
	"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
	"data"            => $data,   // total data array
);
echo json_encode($json_data);  // send data as json format
?>