<?php
include("../includes/functions.php");
global $db;

//echo "<pre>==#####";print_r($_POST); 

//echo "<pre>==#####";print_r($_FILES); die;

//File upload configuration
$targetDir = "../uploads/projects/";
$allowTypes = array('jpg','png','jpeg','gif');
//echo '<pre>'; print_r($_FILES);
$images_arr = array();
if(!empty($_FILES) && is_array($_FILES)){
	$image_name = $_FILES['proGalImg']['name'];
	$tmp_name   = $_FILES['proGalImg']['tmp_name'];
	$size       = $_FILES['proGalImg']['size'];
	$type       = $_FILES['proGalImg']['type'];
	$error      = $_FILES['proGalImg']['error'];
		
	// File upload path
	$fileName = basename($_FILES['proGalImg']['name']);
	$targetFilePath = $targetDir.$fileName;
	
	// Check whether file type is valid
	$fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
	if(in_array($fileType, $allowTypes)){    
		// Store images on the server
		if(move_uploaded_file($_FILES['proGalImg']['tmp_name'],$targetFilePath)){
			$images_arr[] = $fileName ; //$targetFilePath
		}
	}	
} else {
   $images_arr[0] = "";
}

if($_POST['proPrice'] != ''){
	$proPrice = str_replace("$", "", $_POST['proPrice']);
} else {
	$proPrice = '';
}
$proTypeId = isset($_POST['proTypeId'])?$_POST['proTypeId']:"";
$proSku = isset($_POST['proSku'])?$_POST['proSku']:"";
$proClassId = isset($_POST['proClassId'])?$_POST['proClassId']:"";
$proDetailedDesc = isset($_POST['proDetailedDesc'])?$_POST['proDetailedDesc']:"";
$proPriCatId = isset($_POST['proPriCatId'])?$_POST['proPriCatId']:"";
$proAltCat = isset($_POST['proAltCat'])?$_POST['proAltCat']:"";
$proSchType = isset($_POST['proSchType'])?$_POST['proSchType']:"";
$proSchStDateVal = isset($_POST['proSchStDate'])?$_POST['proSchStDate']:"";
$proSchStTime = isset($_POST['proSchStTime'])?$_POST['proSchStTime']:"";
if($proSchStDateVal == '' && $proSchStTime == ''){
	$dtFinal = '';
} else {
	$dt = $proSchStDateVal.''.$proSchStTime;
	$dtFinal = date( 'Y-m-d H:i:s', strtotime($dt) );
}
if(!empty($images_arr) && is_array($images_arr)){
	$image_first = $images_arr[0];
} else {
	$image_first = '';
}
if($_POST['proFpPrintList'] != ''){
	$proFpPrintList = implode(',',$_POST['proFpPrintList']);
} else {
	$proFpPrintList = '';
}
$proFpPrintCustom = isset($_POST['proFpPrintCustom'])?$_POST['proFpPrintCustom']:"";
$proSpecWi = isset($_POST['proSpecWi'])?$_POST['proSpecWi']:"";
$proSpecHi = isset($_POST['proSpecHi'])?$_POST['proSpecHi']:"";
$proSpecDe = isset($_POST['proSpecDe'])?$_POST['proSpecDe']:"";
$proSpecWe = isset($_POST['proSpecWe'])?$_POST['proSpecWe']:"";

$data = array (
	'userId' => $_POST['userId'],
	'proName' => $_POST['proName'],
	'proTypeId' => $proTypeId,
	'proSku' => $proSku,
	'proClassId' => $proClassId,
	'proDetailedDesc' => $proDetailedDesc,
	'proPrice' =>  $proPrice,
	'proPriCatId' => $proPriCatId,
	'proAltCat' => $proAltCat,
	'proSchType' => $proSchType,
	'proSchStDate' => $proSchStDateVal,
	'proSchStTime' => $proSchStTime, 
	'proSchStDtTime' => $dtFinal,	
	'proGalImg' => $image_first,
	'proFpPrintList' => $proFpPrintList,
	'proFpPrintCustom' => $proFpPrintCustom,
	'proSpecWi' =>  $proSpecWi,
	'proSpecHi' =>  $proSpecHi,
	'proSpecDe' =>  $proSpecDe,
	'proSpecWe' =>  $proSpecWe,
	'status' => 2, //for draft
	'isDeleted' => 0,
	'createdDate' => date('Y-m-d H:i:s'),
	'updatedDate' => date('Y-m-d H:i:s')
);
$last_insert_id = $db->insert("bb_project",$data);

if($last_insert_id) {
    if(!empty($_POST['proOptionVal'][0]) && is_array($_POST['proOptionVal'])) {
	    for($i=0;$i<count($_POST['proOptionVal']); $i++) {
		    $data1 = array (
				'proUserId' => $_POST['userId'],
				'proId' => $last_insert_id,
				'proOptionVal' => $_POST['proOptionVal'][$i],
				'proLabel' => $_POST['proLabel'][$i],
				'proHoverTxt' => $_POST['proHoverTxt'][$i],
				'proOptVal' => $_POST['proOptVal_'.$i][0]
			);
			$db->insert("bb_propersoptions",$data1);
		}
	} 
	
	if(!empty($_POST['specOptionVal'][0]) && is_array($_POST['specOptionVal'])) {
	    for($i=0;$i<count($_POST['specOptionVal']); $i++) {
			$data3 = array (
				'userId' => $_POST['userId'],
				'proId' => $last_insert_id,
				'specOptionVal' => $_POST['specOptionVal'][$i],
				'specType' => $_POST['specType'][$i],
				'specTypeVal' => $_POST['specTypeVal'][$i]
			);
			$db->insert("bb_prospecifications",$data3);
		}
	}
	
	//Project History insertion
	$actionType = $_POST['actionType'];
	if($actionType == 'draftAction'){
		$actionvar = "Project Drafted";
	}
	$data4 = array (
		'lgUserId' => $_POST['userId'],
		'lgProId' => $last_insert_id,
		'lgDescription' => $actionvar,
		'lgDate' => date('Y-m-d'),
		'lgTime' => date('H:i:s')
	);
	$db->insert("bb_proactivitylog",$data4);
	
	//$response["msg"] = "Project is successfully draft.".$db->getLastQuery(); //$db->getLastQuery();
	//$response["last_insert_id"] = $last_insert_id;
	$_SESSION["msg"]= "Successfully saved draft Project data";
} 
else {
	//$response["msg"] = "Project is not successfully draft".$db->getLastError(); //$db->getLastError();
	//$response["last_insert_id"] = '';
	$_SESSION["msg"]= "Not successfully saved draft Project data";
}
echo json_encode($response);
die;
?>