<?php
include("../includes/functions.php");
global $db;
$response = array();

/*echo '<pre>post=='; print_r($_POST);
die;*/

//Delete remove ids
if(isset($_POST['removeDbValue']) && !empty($_POST['removeDbValue'])) {
	$remDids = explode(",",substr($_POST['removeDbValue'],0,-1));
	for($p=0;$p<count($remDids);$p++) {
		if(isset($remDids[$p]) && !empty($remDids[$p])) {
			$db->where('id',$remDids[$p]);
			$db->delete('bb_profineprintoptions');
		}
	}
}
	
//Update fine print name which is already in Db 
if(!empty($_POST['ProjectFormNameDataDb']) && is_array($_POST['ProjectFormNameDataDb'])) {
	$ProjectFormLabelDataDb = array_flip($_POST['ProjectFormLabelDataDb']);
	$ProjectFormNameDataDb = $_POST['ProjectFormNameDataDb'];
	$projFinePrintDbArr = array_combine($ProjectFormLabelDataDb,$ProjectFormNameDataDb);
	foreach($projFinePrintDbArr as $key=>$val) {
		$data = array ('proFinePrintName' => $val);
		$db->where ('id',$key);
		$db->update("bb_profineprintoptions",$data);
	}
}

//insert add more values
if(!empty($_POST['ProjectFormNameData'][0]) && is_array($_POST['ProjectFormNameData'])) {
	for($i=0;$i<count($_POST['ProjectFormNameData']); $i++) {
		$data = array ('proFinePrintName' => $_POST['ProjectFormNameData'][$i]);
		$insert = $db->insert("bb_profineprintoptions",$data);
	}
}
//if($insert) {
	$_SESSION["msg"]= "Successfully Saved Fine Print Data";
//} 
echo json_encode($response);
die;
?>