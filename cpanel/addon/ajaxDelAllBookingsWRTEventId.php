<?php
include("../includes/functions.php");
global $db;

$eventId = trim($_POST['eventId']);
$dtype = trim($_POST['dtype']);

if($dtype == ''){
	//Soft Delete all bookings as per eventId
	$params1 = array('');
	$result1 = $db->rawQuery("SELECT id,eventId FROM bb_booking WHERE eventId='".$eventId."' ", $params1);
	$ritems1 = (array)$result1;
	if(!empty($ritems1)) {
		foreach($ritems1 as $key1=>$val1) {
			$data1 = array ('isDeleted' =>1);	
			$db->where ('id', $val1['id']);
			$db->where ('eventId', $val1['eventId']);
			$db->update("bb_booking",$data1);
		}
	} 
	//Update booked seats=0
	$data2 = array ('evBookTicket' =>0);	
	$db->where ('id', $eventId);
	$db->update("bb_event",$data2);
}

if($dtype == 'p'){ //permanent delete
	//Hard Delete all bookings as per eventId
	$params1 = array('');
	$result1 = $db->rawQuery("SELECT id,eventId FROM bb_booking WHERE eventId='".$eventId."' ", $params1);
	$ritems1 = (array)$result1;
	if(!empty($ritems1)) {
		foreach($ritems1 as $key1=>$val1) {
			$db->where ('id', $val1['id']);
			$db->where ('eventId', $val1['eventId']);
			$db->delete("bb_booking");
		}
	}
	//Update booked seats=0
	$data2 = array ('evBookTicket' =>0);	
	$db->where ('id', $eventId);
	$db->update("bb_event",$data2);
}

if($dtype == 'r'){ //restore
	//Restore all bookings as per eventId
	$params1 = array('');
	$result1 = $db->rawQuery("SELECT id,eventId,totalTickets FROM bb_booking WHERE eventId='".$eventId."' and isDeleted=1", $params1);
	$ritems1 = (array)$result1;
	if(!empty($ritems1)) {
		$totalTicketsPerBooking =0;
		foreach($ritems1 as $key1=>$val1) {
			$totalTicketsPerBooking += $val1['totalTickets'];
			$data1 = array ('isDeleted' =>0);	
			$db->where ('id', $val1['id']);
			$db->where ('eventId', $val1['eventId']);
			$db->update("bb_booking",$data1);
		}
	} else {
		$totalTicketsPerBooking = 0;
	}
	//echo '===='.$totalTicketsPerBooking;die;
	$data2 = array ('evBookTicket' =>$totalTicketsPerBooking);	
	$db->where ('id', $eventId);
	$db->update("bb_event",$data2);
}

echo json_encode($response);
die;
?>