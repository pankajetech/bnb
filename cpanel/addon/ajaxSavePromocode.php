<?php
include("../includes/functions.php");
global $db;
$response = array();
/*echo '<pre>post=='; print_r($_POST);
die;*/

$actionType = $_POST['actionType'];
//publish
if($actionType == 'addAction'){ $status = 0;  } 
if($actionType == 'updateAction'){ $status = 0; } 
if($actionType == 'enableAction'){ $status = 0; } 
if($actionType == 'disableAction'){ $status = 1; } 
if($actionType == 'archiveAction'){ $status = 3; } 
if($actionType == 'updateArchiveAction'){ $status = 3; } 

$userId     = isset($_POST['userId'])?$_POST['userId']:"";
//$studid     = isset($_POST['studid'])?$_POST['studid']:"";
$userRole     = isset($_POST['userRole'])?$_POST['userRole']:"";

$promoName     = isset($_POST['promoName'])?StringInputCleaner($_POST['promoName']):"";
$promoCode     = isset($_POST['promoCode'])?StringInputCleaner($_POST['promoCode']):"";
$promoDescription  = isset($_POST['promoDescription'])?StringInputCleaner($_POST['promoDescription']):"";

$promoQty     = isset($_POST['promoQty'])?IntegerInputCleaner($_POST['promoQty']):"";

$promoIsApproRequired = isset($_POST['promoIsApproRequired'])?$_POST['promoIsApproRequired']:"";
$promoRApplyTo = isset($_POST['promoRApplyTo'])?$_POST['promoRApplyTo']:"";
$promoRDiscountType = isset($_POST['promoRDiscountType'])?$_POST['promoRDiscountType']:"";
$promoRTaxType = isset($_POST['promoRTaxType'])?$_POST['promoRTaxType']:"";

$promoRDiscountVal = isset($_POST['promoRDiscountVal'])?$_POST['promoRDiscountVal']:"";

if($_POST['schStDate'] != ''){ $schStDate = date('Y-m-d', strtotime($_POST['schStDate'])); } else { $schStDate = ''; }
if($_POST['schStTime'] != ''){ $schStTime = $_POST['schStTime']; } else { $schStTime = '';}
if($schStDate == '' && $schStTime == ''){
	$schStDateFinal = '';
} else {
	$dt = $schStDate.''.$schStTime;
	$schStDateFinal = date( 'Y-m-d H:i:s', strtotime($dt) );
}

if($_POST['schEndDate'] != ''){ $schEndDate = date('Y-m-d', strtotime($_POST['schEndDate'])); } else { $schEndDate = ''; }
if($_POST['schEndTime'] != ''){ $schEndTime = $_POST['schEndTime']; } else { $schEndTime = ''; }
if($schEndDate == '' && $schEndTime == ''){
	$schEndDateFinal = '';
} else {
	$dt1 = $schEndDate.''.$schEndTime;
	$schEndDateFinal = date( 'Y-m-d H:i:s', strtotime($dt1) );
}

if($_POST['eventAssociated'] != ''){
  $eventAssociated = implode(',',$_POST['eventAssociated']);
} else {
  $eventAssociated = '';
}

//Get studioId from studio sessionId
$studSessId = $_POST['studid']; 

//Update case
$promoId = $_POST['promoId'];
if($promoId != '') { 
	//update studio id when user role = 3 "studio user"
	if($userRole ==3) { 
		$params = array('');
		if(!empty($_POST['eventAssociated'])){
			$evLocationIdArr = array();
			//echo "<prE>eventAssociated==";print_R($_POST['eventAssociated']); 
			foreach($_POST['eventAssociated'] as $key=>$val){
				//Get locationId wrt to eventId
				$evLocationIdArr[] = getEventLocIdWRTEventId($val);
			}
			//echo "<pre>evLocationIdArr==";print_r($evLocationIdArr);die;
			if(!empty($evLocationIdArr)){
				$evLocationIdArr = array_unique($evLocationIdArr);
				$studid = implode(',',$evLocationIdArr);
			} else {
			    $studid = '';
			}
			//echo "<pre>studid==";print_r($studid);die;
		} 
		else { //if associated event is blank
			$studid = $_POST['studid']; 
		} 
	} 
	//studio id when user role = 1 "corp admin"
	else {
		$studid = getStdLocationIdWRTPromoId($promoId);
		if($_POST['diffArrStr'] == ''){
			$eventAssociated = $eventAssociated; 
		} else if($eventAssociated == ''){
			$eventAssociated = $_POST['diffArrStr'];
		} else {
			$eventAssociated = $_POST['diffArrStr'].",".$eventAssociated; 
		}
	}
	
	$data = array (
		'studioId' => $studid,
		'promoName' => $db->escape($promoName),
		'promoCode' => $db->escape($promoCode),
		'promoDescription' => $db->escape($promoDescription),
		'promoQty' => $db->escape($promoQty),
		'promoIsApproRequired' => $promoIsApproRequired,
		'promoRApplyTo' =>  $promoRApplyTo,
		'promoRTaxType' => $promoRTaxType,
		'promoRDiscountType' => $promoRDiscountType,
		'promoRDiscountVal' => $db->escape($promoRDiscountVal),
		'schStDate' => $schStDate,
		'schStTime' => $schStTime,
		'schEndDate' => $schEndDate,
		'schEndTime' => $schEndTime,
		'promoSchStDtTime' => $schStDateFinal,
		'promoSchEndDtTime' => $schEndDateFinal,
		'eventAssociated' => $eventAssociated,
		'status' => $status,
		'promoUpdatedBy' => $userId,
		'promoUpdatedDate' => date('Y-m-d H:i:s'),
	);
} 
else { //add case
	$studid = isset($_POST['studid'])?$_POST['studid']:"";
	$data = array (
		'studioId' => $studid,
		'promoName' => $db->escape($promoName),
		'promoCode' => $db->escape($promoCode),
		'promoDescription' => $db->escape($promoDescription),
		'promoQty' => $db->escape($promoQty),
		'promoIsApproRequired' => $promoIsApproRequired,
		'promoRApplyTo' =>  $promoRApplyTo,
		'promoRTaxType' => $promoRTaxType,
		'promoRDiscountType' => $promoRDiscountType,
		'promoRDiscountVal' => $db->escape($promoRDiscountVal),
		'schStDate' => $schStDate,
		'schStTime' => $schStTime,
		'schEndDate' => $schEndDate,
		'schEndTime' => $schEndTime,
		'promoSchStDtTime' => $schStDateFinal,
		'promoSchEndDtTime' => $schEndDateFinal,
		'eventAssociated' => $eventAssociated,
		'status' => $status,
		'promoCreatedBy' => $userId, 
		'promoCreatedDate' => date('Y-m-d H:i:s'),
		'promoUpdatedBy' => $userId,
		'promoUpdatedDate' => date('Y-m-d H:i:s'),
	);
}

if($promoId != ''){ //update case
	$db->where ('id', $promoId);
	$update = $db->update("bb_promocode",$data);
	if($update) {
		if($actionType == 'updateAction'){
			$_SESSION["msg"]= "Successfully updated Coupon code";
			$response["studid"] = $studSessId; //$studid
			$response["status"] = 0;
			$response["userRole"] = $userRole; //user role
		} 
		if($actionType == 'enableAction'){
			$_SESSION["msg"]= "Successfully enabled Coupon code";
			$response["studid"] = $studSessId; //$studid
			$response["status"] = 0;
			$response["userRole"] = $userRole; //user role
		} 
		if($actionType == 'disableAction'){
			$_SESSION["msg"]= "Successfully disabled Coupon code";
			$response["studid"] = $studSessId; //$studid
			$response["status"] = 1;
			$response["userRole"] = $userRole; //user role
		} 
		if($actionType == 'archiveAction'){
			$_SESSION["msg"]= "Successfully archived Coupon code";
			$response["studid"] = $studSessId; //$studid
			$response["status"] = 3;
			$response["userRole"] = $userRole; //user role
		} 
		if($actionType == 'updateArchiveAction'){
			$_SESSION["msg"]= "Successfully updated archived Coupon code";
			$response["studid"] = $studSessId; //$studid
			$response["status"] = 3;
			$response["userRole"] = $userRole; //user role
		}
	} 
	else {
		if($actionType == 'updateAction'){
			$_SESSION["msg"]= "Not successfully updated Coupon code";
		} 
	}
} 
else 
{ 	//add case
	$last_insert_id = $db->insert("bb_promocode",$data);
	if($last_insert_id) {
		if($actionType == 'addAction'){
			$_SESSION["msg"]= "Successfully saved Coupon code"; //$db->getLastQuery();
			$response["status"] = 0;
			$response["studid"] = $studSessId; //$studid
			$response["last_insert_id"] = $last_insert_id;
			$response["userRole"] = $userRole; //user role
		} 
	} 
	else {
		if($actionType == 'addAction'){
			$_SESSION["msg"]= "Not successfully saved Coupon code"; //$db->getLastError();
			$response["studid"] = $studSessId; //$studid
			$response["userRole"] = $userRole; //user role
		} 
	}
}
echo json_encode($response);
die;
?>