<?php
include("../includes/functions.php");
global $db;

//echo "<pre>post=";print_R($_REQUEST['projType']);die;

if(isset($_REQUEST["projType"]) && $_REQUEST["projType"] =="archive") {
	$stwhre = " bpro.status=3";
} 
else if(isset($_REQUEST["projType"]) && $_REQUEST["projType"] =="draft") {
	$stwhre = " ( 
		(bpro.status=0 AND bpro.proSchStDtTime > (NOW() + INTERVAL 330 Minute) AND bpro.proSchDtOptional=0 ) 
		OR 
		( bpro.status=0 AND bpro.proSchStDtTime < (NOW() + INTERVAL 330 Minute) AND bpro.proSchDtOptional=1)  
		OR bpro.status=2 
		)";
}
else { 
	$stwhre =" (bpro.status=0 or bpro.status=1 or bpro.status=2)";
}

$query = "SELECT bproCat.proCatName,bproCat.id
FROM `bb_project` as bpro 
left join `bb_procategory` as bproCat on bpro.proPriCatId = bproCat.id 
where $stwhre and bpro.isDeleted=0
group by  bproCat.proCatName
ORDER BY bproCat.proCatName asc";
$results =  $db->rawQuery($query);
$response = array();
if(count($results)>0){
	$response["catList"] = $results;
}	else {
	$response["catList"] = array();
}
echo json_encode($response);
die;
?>