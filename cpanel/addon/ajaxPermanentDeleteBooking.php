<?php
include("../includes/functions.php");
global $db;

//echo "<pre>POST==";print_r($_POST);die;

$bookingId = urldecode($_POST['bookingId']);
$bookingId = trim($bookingId);
$response = array();

//Permanent delete bb_booking
$db->where ('id',$bookingId);
$db->where ('isDeleted',1);
if($db->delete("bb_booking")){
	$delete =1;
} else {
	$delete =2;
}

//Permanent Delete bb_booktcktinfo Data
$params1 = array('');
$result1 = $db->rawQuery("SELECT id,bookingId FROM bb_booktcktinfo WHERE bookingId='".$bookingId."' ", $params1);
$ritems1 = (array)$result1;
if(!empty($ritems1)) {
	foreach($ritems1 as $key1=>$val1) {
		$db->where ('id', $val1['id']);
		$db->where ('bookingId', $val1['bookingId']);
		$db->delete("bb_booktcktinfo");
	}
}

//Permanent Delete bb_booktcktpersinfo Data
$params2 = array('');
$result2 = $db->rawQuery("SELECT id,bookingId FROM bb_booktcktpersinfo WHERE bookingId='".$bookingId."' ", $params2);
$ritems2 = (array)$result2;
if(!empty($ritems2)) {
	foreach($ritems2 as $key2=>$val2) {
		$db->where ('id', $val2['id']);
		$db->where ('bookingId', $val2['bookingId']);
		$db->delete("bb_booktcktpersinfo");
	}
}

//Permanent delete bb_transactiondetails
$params3 = array('');
$result3 = $db->rawQuery("SELECT id,bookingId FROM bb_transactiondetails WHERE bookingId='".$bookingId."' ", $params3);
$ritems3 = (array)$result3;
if(!empty($ritems3)) {
	foreach($ritems3 as $key3=>$val3) {
		$db->where ('isDeleted', 1);
		$db->where ('id', $val3['id']);
		$db->where ('bookingId', $val3['bookingId']);
		$db->delete("bb_transactiondetails");
	}
}

if($delete == 1){ 
	$response['msg'] = "Booking is Permanently Deleted";
	$response['status'] = 1;
} else {
	$response['msg'] = "Booking is not Permanently Deleted";
	$response['status'] = 2;
}
echo json_encode($response);
die;
?>