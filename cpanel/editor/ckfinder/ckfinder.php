<?php
/*
 * CKFinder
 * ========
 * http://cksource.com/ckfinder
 * Copyright (C) 2007-2013, CKSource - Frederico Knabben. All rights reserved.
 *
 * The software, this file and its contents are subject to the CKFinder
 * License. Please read the license.txt file before using, installing, copying,
 * modifying or distribute this file or part of its contents. The contents of
 * this file is part of the Source Code of CKFinder.
 */

require_once 'core/ckfinder_php5.php' ;


function CheckAuthentication() {
	return true;
}
$baseUrl = 'http://localhost/idillionaire-admin/editor/ckeditor/uploads/';
$enabled = true;
$config['SecureImageUploads'] = false;
$config['ChmodFolders'] = 0777 ;
