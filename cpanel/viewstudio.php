<?php 
include('includes/header.php');
$menuClssStudio = "downarrow";
$menuSbClssStudio = "sub";
$menuSbClssStudio1 = "active";
$menuSbClssStyleStudio = "style='display:block;'";
if(!isset($_SESSION['usrid']) && $_SESSION['usrid']==0){ header("location: index.php");    exit; } 
if(isset($_SESSION['urole']) && $_SESSION['urole']!=2){ header("location: ".base_url_site."dashboard");  exit; } 

if(isset($_REQUEST["id"]) and $_REQUEST["id"]!="") {    
	$params = array('');
	$result = $db->rawQueryOne("SELECT * FROM bb_studiolocation WHERE id='".$_REQUEST["id"]."' ", $params);
	$ritems = (array)$result;
	if(!empty($ritems)) $ritems=$ritems; 
} else {
	$ritems=array();
	$ritems=$ritems;
} ?>
<script type="text/javascript" src="<?php echo base_url_site?>editor/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url_site?>editor/ckfinder/ckfinder.js"></script>
<!-- Header end-->

<!-- left nagivation start-->
<?php include('includes/left-sidebar.php');?>
<style>
.tempname {float:none;}
</style>
<!-- left nagivation end-->
<!-- left content area start-->
<div class="loading" style="display:none;">Loading&#8230;</div>
<div class="right-wrapper">
	<h1 class="pageheading">Studio-<?php echo @fnCheckEmpty($ritems['stName']);?></h1>
	<div class="form-area">
		<input type="hidden" name="id" id="id" value="<?php echo $_REQUEST["id"]; ?>" />
		<form role="form" method="post" action="" name="frmaddupStudio" id="frmaddupStudio">
		<div class="template-info">
			<div class="template-info-left">
				<div class="input-row">
					<div class="floatingBox">
						<input class="textfull tempname" type="text" name="stName" id="stName" required  value="<?php echo @fnCheckEmpty($ritems['stName']); ?>" placeholder="" />
						<label class="floating-lab">Studio Name</label>
					</div>
				</div>
                
                <div class="input-row"><div class="floatingBox"><input class="textfull tempname" type="text" required  name="stEmail" id="stEmail" value="<?php echo @fnCheckEmpty($ritems['stEmail']); ?>" placeholder="" /><label class="floating-lab">Studio Email</label></div></div>
				<div class="input-row"><div class="floatingBox"><input class="textfull tempname" type="text" name="stStreetAddress"   id="stStreetAddress"  placeholder="" value="<?php echo @fnCheckEmpty($ritems['stStreetAddress']); ?>" /><label class="floating-lab">Address Line 1</label></div></div>
                <div class="input-row"><div class="floatingBox"><input class="textfull tempname" type="text" name="stAddress1"  id="stAddress1"  placeholder="" value="<?php echo @fnCheckEmpty($ritems['stAddress1']); ?>" /><label class="floating-lab">Address Line 2</label></div></div>
                <div class="input-row"><div class="floatingBox"><input class="textfull tempname" type="text" name="stCity" required  id="stCity"  placeholder="" value="<?php echo @fnCheckEmpty($ritems['stCity']); ?>" /><label class="floating-lab">City</label></div></div>
                <div class="input-row">
					<select class="slectfull tempname" name="stState" required  id="stState"  placeholder="Select an Option">
						<option value="">Select State</option>
						<?php 
						$states=getStates("224");
						foreach($states as $k=>$v) {
							if($ritems['stState']==$v['state_name']) {
								echo '<option value="'.$v['state_name'].'" selected>'.$v['state_name'].' </option>';
							} else {
								echo '<option value="'.$v['state_name'].'">'.$v['state_name'].' </option>';
							}
						}?> 
					</select>
				</div>
                <div class="input-row"><div class="floatingBox"><input class="textfull number-only tempname" type="text" name="stZip" required  id="stZip"  placeholder="" value="<?php echo @fnCheckEmpty($ritems['stZip']); ?>" maxlength="5" /><label class="floating-lab">Zip</label></div></div>
				<div class="input-row"><div class="floatingBox"><input class="textfull tempname" type="text" required  name="stPhone" id="stPhone" value="<?php echo @fnCheckEmpty($ritems['stPhone']); ?>" placeholder="" /><label class="floating-lab">Studio Phone</label></div></div>
            </div>
			
            <?php 
			if(isset($ritems['stOwnerPic']) && $ritems['stOwnerPic']!=""){
				$imageurl=base_url_site."uploads/studio/".$ritems['stOwnerPic'];
				$imageName=$ritems['stOwnerPic'];
				$isVimage=1;
			}  else  {
				//$imageurl=base_url_site."images/placeholder.jpg";
				$imageurl=base_url_images."placeholder.jpg";
				$imageName="";
				$isVimage=0;
			}?>
			<input type="hidden" name="flpup" id="flpup" value="<?php echo $imageName;?>" />
            <div class="gallery-img">
				<label id="upload-label"><i class="fa fa-plus-circle"></i><input onchange="readURL(this);" name="stImage" id="img-upload" disabled="disabled" name="stImage" type="file"/></label>
				<img id="blah" src="<?php echo $imageurl?>" alt="" />
            </div>
		</div>

		<div class="abtstudio">
			<h2 class="section-heading">About Studio</h2>
			<div class="wysiwyg-editor">
				<textarea name="stAboutStudo" class="tempname" id="stAboutStudo" rows="3" required data-placement="right"  placeholder="Please enter about studio."><?php echo @fnCheckEmpty($ritems['stAboutStudo']); ?></textarea>
				<script type="text/javascript">
				var editor = CKEDITOR.replace( 'stAboutStudo', {
				filebrowserBrowseUrl : 'editor/ckfinder/ckfinder.html',
				filebrowserImageBrowseUrl : 'editor/ckfinder/ckfinder.html?type=Images',
				filebrowserFlashBrowseUrl : 'editor/ckfinder/ckfinder.html?type=Flash',
				filebrowserUploadUrl : 'editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
				filebrowserImageUploadUrl : 'editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
				filebrowserFlashUploadUrl : 'editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
				});
				CKFinder.setupCKEditor( editor, '../' );
				</script>
			</div>
		</div>

		<div class="abtstudio">
			<h2 class="section-heading">Bio & Description</h2>
			<div class="wysiwyg-editor">
				<textarea class="tempname" name="stBioDesc" id="stBioDesc" rows="3" required  data-placement="bottom"  placeholder="Please enter studio description."><?php echo @fnCheckEmpty($ritems['stBioDesc']); ?></textarea>
				<script type="text/javascript">
				var editor = CKEDITOR.replace( 'stBioDesc', {
				filebrowserBrowseUrl : 'editor/ckfinder/ckfinder.html',
				filebrowserImageBrowseUrl : 'editor/ckfinder/ckfinder.html?type=Images',
				filebrowserFlashBrowseUrl : 'editor/ckfinder/ckfinder.html?type=Flash',
				filebrowserUploadUrl : 'editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
				filebrowserImageUploadUrl : 'editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
				filebrowserFlashUploadUrl : 'editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
				});
				CKFinder.setupCKEditor( editor, '../' );
				</script>
			</div>
		</div>

		<div class="sociallink-section">
			<h2 class="section-heading">Social Links</h2>
			<div class="sociallink-row"><span>Facebook</span><input class="width70  tempname" type="text" name="stFbLink" id="stFbLink" value="<?php echo @fnCheckEmpty($ritems['stFbLink']); ?>" /> </div>
			<div class="sociallink-row"><span>Instagram</span><input class="width70  tempname" type="text" name="stInstaLink" id="stInstaLink" value="<?php echo @fnCheckEmpty($ritems['stInstaLink']); ?>" /> </div>
			<div class="sociallink-row"><span>Twitter link</span><input class="width70  tempname" type="text" name="stTwtLink" id="stTwtLink" value="<?php echo @fnCheckEmpty($ritems['stTwtLink']); ?>" /> </div>
			<div class="sociallink-row"><span>Pinterest link</span><input class="width70  tempname" type="text" name="stPinLink" id="stPinLink" value="<?php echo @fnCheckEmpty($ritems['stPinLink']); ?>" /> </div>
		</div>
		
		<div class="sociallink-row"><span>Studio</span>
            <select class="clendar-color tempname" name="stStatus" id="stStatus">
				<option value="">Studio Status</option>
				<?php echo fnDropDownList("bb_studiostatus","id","stName",@fnCheckEmpty($ritems['stStatus']),"");?>
            </select>
        </div>
		<input type="hidden" name="isVimage" id="isVimage" value="<?php echo $isVimage;?>" />
		<input class="width70" type="hidden" name="isconfirm" id="isconfirm" value="0" />
		<input class="width70" type="hidden" name="ccode" id="ccode" value="<?php echo @fnCheckEmpty($ritems['ccode']); ?>" />
		<input class="width70" type="hidden" name="isusa" id="isusa" value="<?php echo @fnCheckEmpty($ritems['isusa']); ?>" />
		<input class="width70" type="hidden" name="latitude" id="latitude" value="<?php echo @fnCheckEmpty($ritems['lat']); ?>" />
		<input class="width70" type="hidden" name="longitude" id="longitude" value="<?php echo @fnCheckEmpty($ritems['lng']); ?>" />  
	</form>
</div>
</div>
<!-- left content area end-->
<link type="text/css" rel="stylesheet" href="<?php echo base_url_css?>bootstrap-3.3.5.min.css">
<script type="text/javascript" src="<?php echo base_url_js?>bootstrap-3.3.5.min.js" /></script>
<script type="text/javascript" src="<?php echo base_url_js?>jquery-validate.bootstrap-tooltip.js" /></script>
<script type="text/javascript" src="<?php echo base_url_js?>jquery.maskedinput-1.3.1.min_.js"></script>
<script type="text/javascript" src="<?php echo base_url_js?>additional-methods.js" /></script>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyDF2l9JmpBPo_y0JdK8ZA-rxOZ9cudCb-g"></script>     
<script>
function showResult(result) {
	document.getElementById('latitude').value = result.geometry.location.lat();
    document.getElementById('longitude').value = result.geometry.location.lng();
	var position = new google.maps.LatLng(result.geometry.location.lat(), result.geometry.location.lng());
	geocoder.geocode({'location': position}, function(results, status) {
        if(status === "OK"){
			var adrFormat = results[0].formatted_address;
			var adrLangth = adrFormat.split(", ").length - 1;
			var adrAry = adrFormat.split(", ");
			if(adrAry[adrLangth] == "USA"){
				document.getElementById('isusa').value = 1;
				document.getElementById('ccode').value = adrAry[adrLangth];
			} else {
				document.getElementById('isusa').value = 0;
				document.getElementById('ccode').value = adrAry[adrLangth];
			}
        }
    });
}

function getLatitudeLongitude(callback, address) {
    // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
    address = address || 'Ferrol, Galicia, Spain';
    // Initialize the Geocoder
    geocoder = new google.maps.Geocoder();
    if (geocoder) {
        geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                callback(results[0]);
            }
        });
    }
}

var button1 = document.getElementById('stZip');
var address="";
button1.addEventListener("blur", function () {
	if($("#stZip").val()!="") {
		if($("#stStreetAddress").val()!="") {
			address=address+$("#stStreetAddress").val();
		}
		if($("#stAddress1").val()!="") {
			address=address+" "+$("#stAddress1").val();
		}
		if($("#stCity").val()!="") {
			address=address+","+$("#stCity").val();
		}
		if($("#stState").val()!="") {
			address=address+","+$("#stState").val();
		}
		if($("#stZip").val()!="") {
			address=address+" "+$("#stZip").val();
		}
		getLatitudeLongitude(showResult, address)
	}	
});
</script>
    
<script>
<?php 
//Disabled Class
if(isset($_REQUEST["id"]) && $_REQUEST["id"] != "" ) { ?>
	$('.tempname').attr('disabled', true);
	$('.tempname').css("background", "#e7e7e7");
<?php 
} ?>
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#blah').attr('src', e.target.result);
			var image = new Image();
			image.src = e.target.result;
			
			image.onload = function () {
				//Determine the Height and Width.
				var height = this.height;
				var width = this.width;
				if (height <= 600 || width <= 700) {
				   $("#isVimage").val(0);
				   $("#upload-label").css({'visibility' : 'visible'});
				} else {
					$("#isVimage").val(1);
					$("#upload-label").css({'visibility' : 'hidden'});
				}
			};
		};
        reader.readAsDataURL(input.files[0]);
	}
}

CKEDITOR.on('instanceReady', function () {
    $.each(CKEDITOR.instances, function (instance) {
        CKEDITOR.instances[instance].document.on("keyup", CK_jQ);
        CKEDITOR.instances[instance].document.on("paste", CK_jQ);
        CKEDITOR.instances[instance].document.on("keypress", CK_jQ);
        CKEDITOR.instances[instance].document.on("blur", CK_jQ);
        CKEDITOR.instances[instance].document.on("change", CK_jQ);
    });
});

function CK_jQ() {
    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
    }
}
</script>
<?php
include('includes/footer.php');
?>